//******************************************************************************
/// @file vv_msg.h
///
/// @brief QNPL Common Messaging Header File
///
/// This file contains a suite of message macros used to output 
/// DV and VI messages in a consistent manner.  They allow 'C' based
/// tests to output messages using a minimum amount of CPU overhead
/// when running in simulation.  This allows the test developer to 
/// insert messages into the program to show progress and/or to flag errors.
/// 
/// Code running on real HW during HW bringup could use ANSI 'C'
/// string functions (such as printf()), but these same functions
/// when executed on the chip in simulation would require too many
/// simulation cycles.  Thus, these simple APIs allow messages to be
/// outputted in both platforms without the extreme slow down in
/// simulation.
/// 
/// @note
///   All of the message macros require the specification of a severity and a 
///   state enumeration.   These enumerations together with a file number and
///   the line number that the macro is specified on, form a unique message
///   code.  This message code is used when running in simulation to specify
///   the message that will be outputted.
/// 
/// <pre>
/// 
/// Message Code Format:
/// 
/// +------+----------+-----------------+---------------------------------+
/// | SEV  |  STATE   |   FILE_NUMBER   |           LINE_NUMBER           |
/// +------+----------+-----------------+---------------------------------+
/// 31   29 28      24 23             16 15                              0 
/// 
/// 
/// SEV         - 3-bit message severity (8 possible values; see enum).
/// 
/// STATE       - 5-bit message state is five bits (32 possible values; see 
///               enum).
/// 
/// FILE_NUMBER - 8-bit file number (256 possible files).  The '#define'
///               VV_MSG_FILE_NUMBER MUST exist in each source code file
///               that uses vv_msg().
/// 
///               File numbers are grouped into the following categories:
/// 
///               Values 0-127 are for use by test code (128 files).
///
///               Values 128-191 are reserved for common test code (64 files).
///
///               Values 192-255 are reserved for QNPL code (64 files).
/// 
/// LINE_NUMBER - 16-bit line number (allows files up to 64K lines).
///  
/// </pre>
///  
/// @b File @b Number
///
/// To use the common messaging, each file needs to specify a special @#define 
/// named, VV_MSG_FILE_NUMBER.
///
/// <code>
///   @#define VV_MSG_FILE_NUMBER file_number
/// </code>
///
/// This #define allows messaging from multiple files when the test consists of 
/// more than one file.  It needs to be defined local to the 'C' file (i.e. not
/// in the header file).  file_number must be an integer value between 0 and 239.
/// Each file must use a unique file_number.  file_numbers above 239 are reserved
/// for QNPL files ('vv_main.c' for instance).
//******************************************************************************


#ifndef _VV_MSG_INCLUDED_
#define _VV_MSG_INCLUDED_

#if !defined(VERILOG_STRING) && !defined(SIMULATION_CODING) || defined(CDVI_SVTB)
#include <stdio.h>
#endif 

#ifdef SIMULATION_CODING
#include "main.h"
#endif

#ifndef PROC_ID
#define PROC_ID 0
#endif

//******************************************************************************
/// @brief QNPL Message Severities
///  
/// The maximum number of severities supported is 8 (0-7) since the cooresponding
/// field in the message code is only 3-bits wide.
//******************************************************************************
typedef enum
{     
    SEV_ERROR       = 0,
    SEV_WARNING     = 1,
    SEV_INFO        = 2,
    SEV_FATAL       = 3,
    SEV_DEBUG       = 4

} VV_MSG_SEVERITY;

//******************************************************************************
/// @brief QNPL Message States
///  
/// The maximum number of states supported is 32 (0-31) since the cooresponding
/// field in the message code is only 5-bits wide.
//******************************************************************************
typedef enum
{
    ST_BOOT        = 0,
    ST_INIT        = 1,
    ST_FUNCTION    = 2,
    ST_SUBFUNCTION = 3,
    ST_IRQ         = 4,
    ST_FIQ         = 5,
    ST_EXCEPTION   = 6

} VV_MSG_STATE;

//******************************************************************************
// Shift values for building the message code.
//******************************************************************************
/// @cond SKIP
#define MSG_SEV_SHIFT   29
#define MSG_STATE_SHIFT 24
#define MSG_FILE_SHIFT  16
/// @endcond

//******************************************************************************
/// @brief QNPL Message macro
///  
/// @parm severity      - An enumeration that specifies the message severity.
/// 
/// @parm state         - An enumeration that specifies the message state.
/// 
/// @parm format_string - Message string to output.
/// 
/// @parm fmt_arg       - Optionally, one or more format arguments (current 
///                       maximum is 4).
///
/// @return none
///
/// @b Description:
///
/// This macro provides a mechanism for outputting of an unformatted message or 
/// a formated message having from 1 to 4 format arguments.
///
/// The format for vv_msg() is:
/// @code
///   vv_msg(severity, state, format_string [, fmt_arg, ..., fmt_arg]);
/// @endcode
/// @n
/// The format_string is identical to the 'C' printf format string with the 
/// following exceptions:
///
/// @li It can ONLY contain the following format specifiers: %i, %d, %x, %X, %u, %o, %c.
/// @li From zero to a maximum of four format arguments are allowed.
/// @li Format flags and format width is supported (see 4.1.6 Format Tags below).
///
/// How do I use vv_msg()?@n
/// 
/// @li Decide on a message severity:
///    SEV_ERROR, SEV_WARNING, SEV_INFO, SEV_FATAL, SEV_DEBUG
/// @li Decide on a message state:
///    ST_BOOT, ST_INIT, ST_FUNCTION, ST_IRQ, ST_FIQ, ST_EXCEPTION
/// @li Determine a file number to use and add this #define to your C source code:
///    #define VV_MSG_FILE_NUMBER file_number
/// @li Create your message
/// @n
///
/// @b Example:
/// @code
/// // read 'My Status' register and print its value
/// unsigned long reg_value;
///
/// mmio_read(MY_STATUS_REG_ADDRESS, reg_value);
///
/// vv_msg(SEV_INFO, ST_FUNCTION,
///       "My Status Register = %#08x\n", reg_value);
/// @endcode
///
//******************************************************************************

//******************************************************************************
// With CDVI_SVTB, compiler is GCC and also kernel is simulator, which supports
// standard C libraries on the host. So can easily use printf.
//******************************************************************************
#if defined(CDVI_SVTB)
    #define vv_msg(severity, state, ...) \
      printf ("[%s] [%s] ", (severity == SEV_INFO ? "SEV_INFO" : \
                            severity == SEV_DEBUG ? "SEV_DEBUG" : \
                            severity == SEV_ERROR ? "SEV_ERROR" : \
                            severity == SEV_WARNING ? "SEV_WARNING" : \
                            "SEV_FATAL") ,\
                           (state == ST_BOOT        ? "ST_BOOT" : \
                            state == ST_INIT        ? "ST_INIT" : \
                            state == ST_FUNCTION    ? "ST_FUNCTION" : \
                            state == ST_SUBFUNCTION ? "ST_SUBFUNCTION" : \
                            state == ST_IRQ         ? "ST_IRQ" : \
                            state == ST_FIQ         ? "ST_FIQ" : \
                            "ST_EXCEPTION")); \
     printf (__VA_ARGS__)


//******************************************************************************
// HMS uses the GNU gcc compiler which is smarter than the ARM compiler.  
// gcc can properly process a macro when less than the number of arguments are 
// specified.  Hence, the reason for this separation.
//******************************************************************************
#elif defined(HMS)

  //------------------------------------------------------------------------------
  // Vera implementation:
  //  
  // To compile for Vera, use: -E -DVERA_MESSAGE.
  //  
  // This produces a preprocessor output file.  To extract the Vera code from it, 
  // use: egrep "message_table[" preprocessor_output_file.
  //------------------------------------------------------------------------------
  #if defined(VERA_MESSAGE)
    #define vv_msg(severity, state, msg_string, ...) \
        message_table[( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ )] = msg_string; \
        file_name_table[ VV_MSG_FILE_NUMBER ] = __FILE__
  
  //------------------------------------------------------------------------------
  // 'C' code in simulation
  //------------------------------------------------------------------------------
  #elif defined(SVTB_MESSAGE)
    #define vv_msg(severity, state, msg_string, ...) \
        message_table[( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ )] = __FILE__; \
        msg_string;
  
  //------------------------------------------------------------------------------
  // 'C' code in simulation
  //------------------------------------------------------------------------------
  #elif defined(SIMULATION_CODING) 
     #define vv_msg(severity, state, msg_string, ...) \
       outp( NATIVE_BUS_MESSAGE, ( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ ) ); \
       { \
         unsigned long format_args[4] = { __VA_ARGS__ }; \
         outp( NATIVE_BUS_MESSAGE+4, format_args[0] ); \
         outp( NATIVE_BUS_MESSAGE+4, format_args[1] ); \
         outp( NATIVE_BUS_MESSAGE+4, format_args[2] ); \
         outp( NATIVE_BUS_MESSAGE+4, format_args[3] ); \
       }
  
  #endif

//******************************************************************************
// non-HMS compile.
//******************************************************************************
#else

  //------------------------------------------------------------------------------
  // Vera implementation:
  //  
  // To compile for Vera, use: -E -DVERA_MESSAGE.
  //  
  // This produces a preprocessor output file.  To extract the Vera code from it, 
  // use: egrep "message_table[" preprocessor_output_file.
  //------------------------------------------------------------------------------
  #if defined(VERA_MESSAGE)
    #define vv_msg(severity, state, msg_string, ...) \
        message_table[( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ )] = msg_string; \
        file_name_table[ VV_MSG_FILE_NUMBER ] = __FILE__

  //------------------------------------------------------------------------------
  // 'C' code in simulation
  //------------------------------------------------------------------------------
  #elif defined(SVTB_MESSAGE)
    #define vv_msg(severity, state, msg_string, ...) \
        message_table[( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ )] = __FILE__; \
        msg_string;
  
  //------------------------------------------------------------------------------
  // 'C' code in simulation
  //------------------------------------------------------------------------------
  #elif defined(SIMULATION_CODING) 

    // the below defines help us determine how many arguments are in __VA_ARGS__
    // and call the appropriate vv_msg_* macro based on this number
    // Reference:
    // http://groups.google.com/group/comp.std.c/browse_thread/thread/77ee8c8f92e4a3fb/346fc464319b1ee5?pli=1
    
    // this set of macros will determine the number of arguments in
    // __VA__ARGS__; it can count up to 9 arguments, in case we ever need
    // this many arguments in the future (this can easily be extended by
    // adding more numbers below)
    #define PP_NARG( ...) PP_NARG_(__VA_ARGS__,PP_RSEQ_N())
    #define PP_NARG_(...) PP_ARG_N(__VA_ARGS__)
    #define PP_ARG_N(_1,_2,_3,_4,_5,_6,_7,_8,_9,N,...) N
    #define PP_RSEQ_N() 9,8,7,6,5,4,3,2,1,0 
    
    // this set of macros concatenates "vv_msg_" with the # of arguments in
    // __VA_ARGS__, resulting in vv_msg_1, vv_msg_2, etc.
    // later, we just call vv_msg_n with the entire format string (which
    // includes the message string and all of the arguments), and this set
    // of macros figures out which vv_msg_* macro needs to be called
    #define CONCATENATE(arg1, arg2)  arg1##arg2
    #define vv_msg_(N, ...) CONCATENATE(vv_msg_, N)(__VA_ARGS__)
    #define vv_msg_n(...) vv_msg_(PP_NARG(__VA_ARGS__), __VA_ARGS__)

    // appropriate vv_msg_* macro is used depending on how many arguments
    // make up the format string; the message string is counted as one of
    // the arguments
    // Ex: 
    //                               |-------------- format string ------------|
    //                               |------- msg_string ------------|
    //  vv_msg(SEV_INFO, ST_FUNCTION, "print a=%i b=%i c=%i d=%i\n", a, b, c, d);
    //
    //  In the above example, there are 5 arguments including the message
    //  string. This is the maximum number of allowed arguments.

    // vv_msg_1 - called when the only argument is the msg_string
    #define vv_msg_1(msg_string) \
         ; \

    // vv_msg_2 - called when we have 2 arguments: msg_string and another
    // argument
    #define vv_msg_2(msg_string, a) \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, a ); \

    // vv_msg_3 - called when we have 3 arguments including the message string
    #define vv_msg_3(msg_string, a, b) \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, a ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, b ); \

    // vv_msg_4 - called when we have 4 arguments including the message string
    #define vv_msg_4(msg_string, a, b, c) \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, a ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, b ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, c ); \

    // vv_msg_5 - called when we have 5 arguments including the message string
    #define vv_msg_5(msg_string, a, b, c, d) \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, a ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, b ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, c ); \
         outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) +4, d ); \

    // vv_msg - macro called by users
    #define vv_msg(severity, state, ...) \
       outp( NATIVE_BUS_MESSAGE + (PROC_ID*8) , ( (severity << MSG_SEV_SHIFT) | (state << MSG_STATE_SHIFT) | (VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT) | __LINE__ ) ); \
         vv_msg_n(__VA_ARGS__) \

  //------------------------------------------------------------------------------
  // 'C' code on RUMI.
  //------------------------------------------------------------------------------
  #elif defined(VVPRINTF)
    #define vv_msg(severity, state, ...) \
       VVPRINTF("%08X: ", \
              ( severity << MSG_SEV_SHIFT | state << MSG_STATE_SHIFT  | \
                VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT | __LINE__         ) ); \
       VVPRINTF(__VA_ARGS__)
         
  #elif defined(RUMI)
    #define vv_msg(severity, state, ...) \
       printf("%08X: ", \
              ( severity << MSG_SEV_SHIFT | state << MSG_STATE_SHIFT  | \
                VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT | __LINE__         ) ); \
       printf(__VA_ARGS__)
  
  //------------------------------------------------------------------------------
  // 'C' code on the 'real' thing.
  //------------------------------------------------------------------------------
  #else
#if 0
     #define vv_msg(severity, state, msg_string, ...) \
       msg(VV_MSG_SEVERITY, VV_MSG_STATE,  msg_string, __VA_ARGS__)
#endif
     #define vv_msg(severity, state, msg_string, ...)


  #endif


#endif


//******************************************************************************
/// @brief QNPL Message String Macro
///  
/// @parm severity      - An enumeration that specifies the message severity.
/// 
/// @parm state         - An enumeration that specifies the message state.
/// 
/// @parm format_string - A formatted message string to output.
/// 
/// @parm fmt_arg       - A character string variable that is no longer than 
///                       16 characters not including the termination character.
///
/// @return none
///
/// @b Description:
///
/// This macro provides a mechanism for outputting formated messages that contain
/// a single string specifier, "%s".
/// 
/// @code
///   vv_msg_string(severity, state, format_string, fmt_arg);
/// @endcode
/// 
/// The format_string is identical to the 'C' printf format string with the 
/// following exceptions:
///
/// @li It MUST contain one and only one format specifier: %s.
/// @li A single format argument must be a variable of type 'unsigned char'.
/// @li The length of the character string ('unsigned char' variable) must not 
/// exceed 16 characters excluding the string terminating character, '\0'.
///
/// Format flags and format width are supported
///
/// How do I use vv_msg_sttring()?@n
/// 
/// @li Decide on a message severity:
///    SEV_ERROR, SEV_WARNING, SEV_INFO, SEV_FATAL, SEV_DEBUG
/// @li Decide on a message state:
///    ST_BOOT, ST_INIT, ST_FUNCTION, ST_IRQ, ST_FIQ, ST_EXCEPTION
/// @li Determine a file number to use and add this #define to your C source code:
///    #define VV_MSG_FILE_NUMBER file_number.
/// @li Create your message.
/// @n
///
/// @b Example:
/// @code
/// // Read the specified register and print its value
/// unsigned long Read_Register(unsigned char* reg_name,
///                             unsigned int reg_address) {
///   unsigned long reg_value;
///   mmio_read(reg_address, &reg_value);
///
///   vv_msg_string(SEV_INFO, ST_FUNCTION,
///          "%s Register = ", reg_name);
///
///   vv_msg(SEV_INFO, ST_FUNCTION,
///          "%#08x\n", reg_value);
///
///   return(reg_value);
/// }
/// @endcode
//******************************************************************************


//------------------------------------------------------------------------------
// Vera and 'C' implementation:
//------------------------------------------------------------------------------
#if defined(VERA_MESSAGE) || defined(SIMULATION_CODING) || defined(SVTB_MESSAGE)
#define vv_msg_string(severity, state, msg_string, fmt_arg) \
  vv_msg(severity, state, msg_string, \
         *( (unsigned int*) &fmt_arg[0]  ), \
         *( (unsigned int*) &fmt_arg[4]  ), \
         *( (unsigned int*) &fmt_arg[8]  ), \
         *( (unsigned int*) &fmt_arg[12] )  )

//------------------------------------------------------------------------------
// 'C' code on RUMI.
//------------------------------------------------------------------------------
#elif defined(VVPRINTF)
  #define vv_msg_string(severity, state, msg_string, fmt_arg) \
     VVPRINTF("%08X: ", \
            ( severity << MSG_SEV_SHIFT | state << MSG_STATE_SHIFT  | \
              VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT | __LINE__         ) ); \
     VVPRINTF(msg_string, fmt_arg)

             
             
#elif defined(RUMI)
  #define vv_msg_string(severity, state, msg_string, fmt_arg) \
     printf("%08X: ", \
            ( severity << MSG_SEV_SHIFT | state << MSG_STATE_SHIFT  | \
              VV_MSG_FILE_NUMBER << MSG_FILE_SHIFT | __LINE__         ) ); \
     printf(msg_string, fmt_arg)

//------------------------------------------------------------------------------
// 'C' code on the 'real' thing.
//------------------------------------------------------------------------------
#else
#if 0
   #define vv_msg_string(severity, state, msg_string, fmt_arg) \
     msg(VV_MSG_SEVERITY, VV_MSG_STATE,  msg_string, fmt_arg)
#endif
   #define vv_msg_string(severity, state, msg_string, fmt_arg)

#endif

#endif // _VV_MSG_INCLUDED_
