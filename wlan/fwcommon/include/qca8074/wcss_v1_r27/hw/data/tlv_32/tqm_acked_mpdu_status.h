// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_ACKED_MPDU_STATUS_H_
#define _TQM_ACKED_MPDU_STATUS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_status_header.h"
#include "buffer_addr_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct uniform_tqm_status_header status_header;
//	2	removed_mpdu_count[8:0], removed_msdu_count[24:9], mpdu_head_is_fw_tx_notify_frame[25], reserved_2a[31:26]
//	3	removed_mpdu_byte_count[31:0]
//	4-5	struct buffer_addr_info tx_notify_frame_addr_info;
//	6	remaining_mpdu_queue_byte_count[31:0]
//	7	remaining_mpdu_count[15:0], prev_mpdu_start_sequence_number[27:16], reserved_7a[31:28]
//	8	tx_mpdu_queue_number[15:0], new_mpdu_start_sequence_number[27:16], gen_mpdu_list_control[29:28], gen_list_stopped_for_tx_notify[30], reserved_8a[31]
//	9	false_mpdu_ack_count[7:0], tid[11:8], reserved_9a[15:12], sw_peer_id[31:16]
//	10	mpdu_last_sequence_number[11:0], mpdu_list_count[20:12], reserved_10a[27:21], looping_count[31:28]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_ACKED_MPDU_STATUS 11

struct tqm_acked_mpdu_status {
    struct            uniform_tqm_status_header                       status_header;
             uint32_t removed_mpdu_count              :  9, //[8:0]
                      removed_msdu_count              : 16, //[24:9]
                      mpdu_head_is_fw_tx_notify_frame :  1, //[25]
                      reserved_2a                     :  6; //[31:26]
             uint32_t removed_mpdu_byte_count         : 32; //[31:0]
    struct            buffer_addr_info                       tx_notify_frame_addr_info;
             uint32_t remaining_mpdu_queue_byte_count : 32; //[31:0]
             uint32_t remaining_mpdu_count            : 16, //[15:0]
                      prev_mpdu_start_sequence_number : 12, //[27:16]
                      reserved_7a                     :  4; //[31:28]
             uint32_t tx_mpdu_queue_number            : 16, //[15:0]
                      new_mpdu_start_sequence_number  : 12, //[27:16]
                      gen_mpdu_list_control           :  2, //[29:28]
                      gen_list_stopped_for_tx_notify  :  1, //[30]
                      reserved_8a                     :  1; //[31]
             uint32_t false_mpdu_ack_count            :  8, //[7:0]
                      tid                             :  4, //[11:8]
                      reserved_9a                     :  4, //[15:12]
                      sw_peer_id                      : 16; //[31:16]
             uint32_t mpdu_last_sequence_number       : 12, //[11:0]
                      mpdu_list_count                 :  9, //[20:12]
                      reserved_10a                    :  7, //[27:21]
                      looping_count                   :  4; //[31:28]
};

/*

struct uniform_tqm_status_header status_header
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			Details that can link this status with the original
			command. It also contains info on how long TQM took to
			execute this command.

removed_mpdu_count
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MPDUs removed from the queue
			
			<legal 0-256>

removed_msdu_count
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MSDUs removed from the queue
			
			<legal all>

mpdu_head_is_fw_tx_notify_frame
			
			When set, the MPDU frame at the head of the MPDU queue
			is a FW_tx_notify_frame 
			
			<legal all>

reserved_2a
			
			<legal 0>

removed_mpdu_byte_count
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The SUM of all MPDU lengths of the removed MPDUs
			
			<legal all>

struct buffer_addr_info tx_notify_frame_addr_info
			
			Consumer: FW
			
			Producer: TQM
			
			
			
			Field only valid when 'MPDU_head_is_fw_tx_notify_frame'
			is set.
			
			Details of the physical address of the MSDU buffer +
			source buffer owner +  some SW meta data corresponding to
			the (first) MSDU related to the MPDU at the head of the
			queue.
			
			
			
			This is the BUFFER_ADDR_INFO from within the MPDU link
			descriptor.

remaining_mpdu_queue_byte_count
			
			The sum of all the remaining MPDU lengths in the queue
			after other MPDUs have been removed
			
			<legal all>

remaining_mpdu_count
			
			The number of MPDUs remaining in the queue.
			
			<legal all>

prev_mpdu_start_sequence_number
			
			Value of 'MPDU_last_sequence_number' from the
			MPDU_QUEUE_HEAD STRUCT at the time that this command starts
			being processed.
			
			
			
			<legal all>

reserved_7a
			
			<legal 0>

tx_mpdu_queue_number
			
			Consumer: TQM/Debug
			
			Producer: SW
			
			
			
			Indicates the MPDU queue ID to which this MPDU link
			descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>

new_mpdu_start_sequence_number
			
			Sequence number of  the first MPDU remaining in the
			queue (after all acknowledged ones have been removed). 
			
			
			
			If the queue is empty, this number will be set equal to
			the field MPDU_last_sequence_number field.
			
			
			
			<legal all>

gen_mpdu_list_control
			
			The setting of the gen_mpdu_list_control field in the
			TQM_ACKED_MPDU TLV
			
			
			
			<legal all>

gen_list_stopped_for_tx_notify
			
			Field only valid when gen_mpdu_list_control > 0
			
			
			
			When set, TQM encountered a frame for which the
			FW_tx_notify_frame field is set, which resulted in stopping
			adding more MPDUs to the MPDU length list
			
			
			
			<legal all>

reserved_8a
			
			<legal 0>

false_mpdu_ack_count
			
			The number of MPDUs that were indicated as properly
			received in the ACK bitmap, but that never got transmitted
			
			Field saturates as 255
			
			<legal all>

tid
			
			TID from the TX_MPDU_QUEUE descriptor.
			
			<legal all>

reserved_9a
			
			<legal 0>

sw_peer_id
			
			Sw_peer_id from the TX_MPDU_QUEUE descriptor.
			
			<legal all>

mpdu_last_sequence_number
			
			Sequence number assigned to the last MPDU in the queue.
			
			
			
			If the queue is empty, this number shall be equal to
			MPDU_start_sequence_number, and represents the value that
			was assigned to the last generated MPDU.
			
			<legal all>

mpdu_list_count
			
			The number of MPDUs that were listed.
			
			<legal all>

reserved_10a
			
			<legal 0>

looping_count
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/

#define TQM_ACKED_MPDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_OFFSET 0x00000000
#define TQM_ACKED_MPDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_LSB 28
#define TQM_ACKED_MPDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_MASK 0xffffffff
#define TQM_ACKED_MPDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_OFFSET 0x00000004
#define TQM_ACKED_MPDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_LSB 28
#define TQM_ACKED_MPDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_MASK 0xffffffff

/* Description		TQM_ACKED_MPDU_STATUS_2_REMOVED_MPDU_COUNT
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MPDUs removed from the queue
			
			<legal 0-256>
*/
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MPDU_COUNT_OFFSET            0x00000008
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MPDU_COUNT_LSB               0
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MPDU_COUNT_MASK              0x000001ff

/* Description		TQM_ACKED_MPDU_STATUS_2_REMOVED_MSDU_COUNT
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MSDUs removed from the queue
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MSDU_COUNT_OFFSET            0x00000008
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MSDU_COUNT_LSB               9
#define TQM_ACKED_MPDU_STATUS_2_REMOVED_MSDU_COUNT_MASK              0x01fffe00

/* Description		TQM_ACKED_MPDU_STATUS_2_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME
			
			When set, the MPDU frame at the head of the MPDU queue
			is a FW_tx_notify_frame 
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_2_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_OFFSET 0x00000008
#define TQM_ACKED_MPDU_STATUS_2_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_LSB  25
#define TQM_ACKED_MPDU_STATUS_2_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_MASK 0x02000000

/* Description		TQM_ACKED_MPDU_STATUS_2_RESERVED_2A
			
			<legal 0>
*/
#define TQM_ACKED_MPDU_STATUS_2_RESERVED_2A_OFFSET                   0x00000008
#define TQM_ACKED_MPDU_STATUS_2_RESERVED_2A_LSB                      26
#define TQM_ACKED_MPDU_STATUS_2_RESERVED_2A_MASK                     0xfc000000

/* Description		TQM_ACKED_MPDU_STATUS_3_REMOVED_MPDU_BYTE_COUNT
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The SUM of all MPDU lengths of the removed MPDUs
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_3_REMOVED_MPDU_BYTE_COUNT_OFFSET       0x0000000c
#define TQM_ACKED_MPDU_STATUS_3_REMOVED_MPDU_BYTE_COUNT_LSB          0
#define TQM_ACKED_MPDU_STATUS_3_REMOVED_MPDU_BYTE_COUNT_MASK         0xffffffff
#define TQM_ACKED_MPDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_OFFSET 0x00000010
#define TQM_ACKED_MPDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_LSB 0
#define TQM_ACKED_MPDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_MASK 0xffffffff
#define TQM_ACKED_MPDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_OFFSET 0x00000014
#define TQM_ACKED_MPDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_LSB 0
#define TQM_ACKED_MPDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_MASK 0xffffffff

/* Description		TQM_ACKED_MPDU_STATUS_6_REMAINING_MPDU_QUEUE_BYTE_COUNT
			
			The sum of all the remaining MPDU lengths in the queue
			after other MPDUs have been removed
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_6_REMAINING_MPDU_QUEUE_BYTE_COUNT_OFFSET 0x00000018
#define TQM_ACKED_MPDU_STATUS_6_REMAINING_MPDU_QUEUE_BYTE_COUNT_LSB  0
#define TQM_ACKED_MPDU_STATUS_6_REMAINING_MPDU_QUEUE_BYTE_COUNT_MASK 0xffffffff

/* Description		TQM_ACKED_MPDU_STATUS_7_REMAINING_MPDU_COUNT
			
			The number of MPDUs remaining in the queue.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_7_REMAINING_MPDU_COUNT_OFFSET          0x0000001c
#define TQM_ACKED_MPDU_STATUS_7_REMAINING_MPDU_COUNT_LSB             0
#define TQM_ACKED_MPDU_STATUS_7_REMAINING_MPDU_COUNT_MASK            0x0000ffff

/* Description		TQM_ACKED_MPDU_STATUS_7_PREV_MPDU_START_SEQUENCE_NUMBER
			
			Value of 'MPDU_last_sequence_number' from the
			MPDU_QUEUE_HEAD STRUCT at the time that this command starts
			being processed.
			
			
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_7_PREV_MPDU_START_SEQUENCE_NUMBER_OFFSET 0x0000001c
#define TQM_ACKED_MPDU_STATUS_7_PREV_MPDU_START_SEQUENCE_NUMBER_LSB  16
#define TQM_ACKED_MPDU_STATUS_7_PREV_MPDU_START_SEQUENCE_NUMBER_MASK 0x0fff0000

/* Description		TQM_ACKED_MPDU_STATUS_7_RESERVED_7A
			
			<legal 0>
*/
#define TQM_ACKED_MPDU_STATUS_7_RESERVED_7A_OFFSET                   0x0000001c
#define TQM_ACKED_MPDU_STATUS_7_RESERVED_7A_LSB                      28
#define TQM_ACKED_MPDU_STATUS_7_RESERVED_7A_MASK                     0xf0000000

/* Description		TQM_ACKED_MPDU_STATUS_8_TX_MPDU_QUEUE_NUMBER
			
			Consumer: TQM/Debug
			
			Producer: SW
			
			
			
			Indicates the MPDU queue ID to which this MPDU link
			descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_8_TX_MPDU_QUEUE_NUMBER_OFFSET          0x00000020
#define TQM_ACKED_MPDU_STATUS_8_TX_MPDU_QUEUE_NUMBER_LSB             0
#define TQM_ACKED_MPDU_STATUS_8_TX_MPDU_QUEUE_NUMBER_MASK            0x0000ffff

/* Description		TQM_ACKED_MPDU_STATUS_8_NEW_MPDU_START_SEQUENCE_NUMBER
			
			Sequence number of  the first MPDU remaining in the
			queue (after all acknowledged ones have been removed). 
			
			
			
			If the queue is empty, this number will be set equal to
			the field MPDU_last_sequence_number field.
			
			
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_8_NEW_MPDU_START_SEQUENCE_NUMBER_OFFSET 0x00000020
#define TQM_ACKED_MPDU_STATUS_8_NEW_MPDU_START_SEQUENCE_NUMBER_LSB   16
#define TQM_ACKED_MPDU_STATUS_8_NEW_MPDU_START_SEQUENCE_NUMBER_MASK  0x0fff0000

/* Description		TQM_ACKED_MPDU_STATUS_8_GEN_MPDU_LIST_CONTROL
			
			The setting of the gen_mpdu_list_control field in the
			TQM_ACKED_MPDU TLV
			
			
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_8_GEN_MPDU_LIST_CONTROL_OFFSET         0x00000020
#define TQM_ACKED_MPDU_STATUS_8_GEN_MPDU_LIST_CONTROL_LSB            28
#define TQM_ACKED_MPDU_STATUS_8_GEN_MPDU_LIST_CONTROL_MASK           0x30000000

/* Description		TQM_ACKED_MPDU_STATUS_8_GEN_LIST_STOPPED_FOR_TX_NOTIFY
			
			Field only valid when gen_mpdu_list_control > 0
			
			
			
			When set, TQM encountered a frame for which the
			FW_tx_notify_frame field is set, which resulted in stopping
			adding more MPDUs to the MPDU length list
			
			
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_8_GEN_LIST_STOPPED_FOR_TX_NOTIFY_OFFSET 0x00000020
#define TQM_ACKED_MPDU_STATUS_8_GEN_LIST_STOPPED_FOR_TX_NOTIFY_LSB   30
#define TQM_ACKED_MPDU_STATUS_8_GEN_LIST_STOPPED_FOR_TX_NOTIFY_MASK  0x40000000

/* Description		TQM_ACKED_MPDU_STATUS_8_RESERVED_8A
			
			<legal 0>
*/
#define TQM_ACKED_MPDU_STATUS_8_RESERVED_8A_OFFSET                   0x00000020
#define TQM_ACKED_MPDU_STATUS_8_RESERVED_8A_LSB                      31
#define TQM_ACKED_MPDU_STATUS_8_RESERVED_8A_MASK                     0x80000000

/* Description		TQM_ACKED_MPDU_STATUS_9_FALSE_MPDU_ACK_COUNT
			
			The number of MPDUs that were indicated as properly
			received in the ACK bitmap, but that never got transmitted
			
			Field saturates as 255
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_9_FALSE_MPDU_ACK_COUNT_OFFSET          0x00000024
#define TQM_ACKED_MPDU_STATUS_9_FALSE_MPDU_ACK_COUNT_LSB             0
#define TQM_ACKED_MPDU_STATUS_9_FALSE_MPDU_ACK_COUNT_MASK            0x000000ff

/* Description		TQM_ACKED_MPDU_STATUS_9_TID
			
			TID from the TX_MPDU_QUEUE descriptor.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_9_TID_OFFSET                           0x00000024
#define TQM_ACKED_MPDU_STATUS_9_TID_LSB                              8
#define TQM_ACKED_MPDU_STATUS_9_TID_MASK                             0x00000f00

/* Description		TQM_ACKED_MPDU_STATUS_9_RESERVED_9A
			
			<legal 0>
*/
#define TQM_ACKED_MPDU_STATUS_9_RESERVED_9A_OFFSET                   0x00000024
#define TQM_ACKED_MPDU_STATUS_9_RESERVED_9A_LSB                      12
#define TQM_ACKED_MPDU_STATUS_9_RESERVED_9A_MASK                     0x0000f000

/* Description		TQM_ACKED_MPDU_STATUS_9_SW_PEER_ID
			
			Sw_peer_id from the TX_MPDU_QUEUE descriptor.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_9_SW_PEER_ID_OFFSET                    0x00000024
#define TQM_ACKED_MPDU_STATUS_9_SW_PEER_ID_LSB                       16
#define TQM_ACKED_MPDU_STATUS_9_SW_PEER_ID_MASK                      0xffff0000

/* Description		TQM_ACKED_MPDU_STATUS_10_MPDU_LAST_SEQUENCE_NUMBER
			
			Sequence number assigned to the last MPDU in the queue.
			
			
			
			If the queue is empty, this number shall be equal to
			MPDU_start_sequence_number, and represents the value that
			was assigned to the last generated MPDU.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LAST_SEQUENCE_NUMBER_OFFSET    0x00000028
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LAST_SEQUENCE_NUMBER_LSB       0
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LAST_SEQUENCE_NUMBER_MASK      0x00000fff

/* Description		TQM_ACKED_MPDU_STATUS_10_MPDU_LIST_COUNT
			
			The number of MPDUs that were listed.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LIST_COUNT_OFFSET              0x00000028
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LIST_COUNT_LSB                 12
#define TQM_ACKED_MPDU_STATUS_10_MPDU_LIST_COUNT_MASK                0x001ff000

/* Description		TQM_ACKED_MPDU_STATUS_10_RESERVED_10A
			
			<legal 0>
*/
#define TQM_ACKED_MPDU_STATUS_10_RESERVED_10A_OFFSET                 0x00000028
#define TQM_ACKED_MPDU_STATUS_10_RESERVED_10A_LSB                    21
#define TQM_ACKED_MPDU_STATUS_10_RESERVED_10A_MASK                   0x0fe00000

/* Description		TQM_ACKED_MPDU_STATUS_10_LOOPING_COUNT
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/
#define TQM_ACKED_MPDU_STATUS_10_LOOPING_COUNT_OFFSET                0x00000028
#define TQM_ACKED_MPDU_STATUS_10_LOOPING_COUNT_LSB                   28
#define TQM_ACKED_MPDU_STATUS_10_LOOPING_COUNT_MASK                  0xf0000000


#endif // _TQM_ACKED_MPDU_STATUS_H_
