// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _PHYRX_L_SIG_A_H_
#define _PHYRX_L_SIG_A_H_
#if !defined(__ASSEMBLER__)
#endif

#include "l_sig_a_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct l_sig_a_info phyrx_l_sig_a_info_details;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_PHYRX_L_SIG_A 1

struct phyrx_l_sig_a {
    struct            l_sig_a_info                       phyrx_l_sig_a_info_details;
};

/*

struct l_sig_a_info phyrx_l_sig_a_info_details
			
			See detailed description of the STRUCT
*/

#define PHYRX_L_SIG_A_0_L_SIG_A_INFO_PHYRX_L_SIG_A_INFO_DETAILS_OFFSET 0x00000000
#define PHYRX_L_SIG_A_0_L_SIG_A_INFO_PHYRX_L_SIG_A_INFO_DETAILS_LSB  0
#define PHYRX_L_SIG_A_0_L_SIG_A_INFO_PHYRX_L_SIG_A_INFO_DETAILS_MASK 0xffffffff


#endif // _PHYRX_L_SIG_A_H_
