// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RX_START_PARAM_H_
#define _RX_START_PARAM_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	pkt_type[3:0], reserved_0a[15:4], remaining_rx_time[31:16]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RX_START_PARAM 1

struct rx_start_param {
             uint32_t pkt_type                        :  4, //[3:0]
                      reserved_0a                     : 12, //[15:4]
                      remaining_rx_time               : 16; //[31:16]
};

/*

pkt_type
			
			Packet type:
			
			<enum 0 dot11a>802.11a PPDU type
			
			<enum 1 dot11b>802.11b PPDU type
			
			<enum 2 dot11n_mm>802.11n Mixed Mode PPDU type
			
			<enum 3 dot11ac>802.11ac PPDU type
			
			<enum 4 dot11ax>802.11ax PPDU type

reserved_0a
			
			<legal 0>

remaining_rx_time
			
			Remaining time (in us) for the current frame in the
			medium.
			
			(received from PHY in TLV:
			PHYRX_COMMON_USER_INFO.Receive_duration)
			
			<legal all>
*/


/* Description		RX_START_PARAM_0_PKT_TYPE
			
			Packet type:
			
			<enum 0 dot11a>802.11a PPDU type
			
			<enum 1 dot11b>802.11b PPDU type
			
			<enum 2 dot11n_mm>802.11n Mixed Mode PPDU type
			
			<enum 3 dot11ac>802.11ac PPDU type
			
			<enum 4 dot11ax>802.11ax PPDU type
*/
#define RX_START_PARAM_0_PKT_TYPE_OFFSET                             0x00000000
#define RX_START_PARAM_0_PKT_TYPE_LSB                                0
#define RX_START_PARAM_0_PKT_TYPE_MASK                               0x0000000f

/* Description		RX_START_PARAM_0_RESERVED_0A
			
			<legal 0>
*/
#define RX_START_PARAM_0_RESERVED_0A_OFFSET                          0x00000000
#define RX_START_PARAM_0_RESERVED_0A_LSB                             4
#define RX_START_PARAM_0_RESERVED_0A_MASK                            0x0000fff0

/* Description		RX_START_PARAM_0_REMAINING_RX_TIME
			
			Remaining time (in us) for the current frame in the
			medium.
			
			(received from PHY in TLV:
			PHYRX_COMMON_USER_INFO.Receive_duration)
			
			<legal all>
*/
#define RX_START_PARAM_0_REMAINING_RX_TIME_OFFSET                    0x00000000
#define RX_START_PARAM_0_REMAINING_RX_TIME_LSB                       16
#define RX_START_PARAM_0_REMAINING_RX_TIME_MASK                      0xffff0000


#endif // _RX_START_PARAM_H_
