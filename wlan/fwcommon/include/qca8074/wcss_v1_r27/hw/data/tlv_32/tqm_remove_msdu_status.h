// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_REMOVE_MSDU_STATUS_H_
#define _TQM_REMOVE_MSDU_STATUS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_status_header.h"
#include "buffer_addr_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct uniform_tqm_status_header status_header;
//	2	remove_msdu_cmd_type[3:0], removed_msdu_count[19:4], msdu_head_is_fw_tx_notify_frame[20], reserved_2a[31:21]
//	3	removed_msdu_byte_count[31:0]
//	4-5	struct buffer_addr_info tx_notify_frame_addr_info;
//	6	remaining_flow_byte_count[31:0]
//	7	remaining_msdu_count[15:0], tx_flow_number[31:16]
//	8	reserved_8a[31:0]
//	9	reserved_9a[31:0]
//	10	reserved_10a[27:0], looping_count[31:28]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_REMOVE_MSDU_STATUS 11

struct tqm_remove_msdu_status {
    struct            uniform_tqm_status_header                       status_header;
             uint32_t remove_msdu_cmd_type            :  4, //[3:0]
                      removed_msdu_count              : 16, //[19:4]
                      msdu_head_is_fw_tx_notify_frame :  1, //[20]
                      reserved_2a                     : 11; //[31:21]
             uint32_t removed_msdu_byte_count         : 32; //[31:0]
    struct            buffer_addr_info                       tx_notify_frame_addr_info;
             uint32_t remaining_flow_byte_count       : 32; //[31:0]
             uint32_t remaining_msdu_count            : 16, //[15:0]
                      tx_flow_number                  : 16; //[31:16]
             uint32_t reserved_8a                     : 32; //[31:0]
             uint32_t reserved_9a                     : 32; //[31:0]
             uint32_t reserved_10a                    : 28, //[27:0]
                      looping_count                   :  4; //[31:28]
};

/*

struct uniform_tqm_status_header status_header
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			Details that can link this status with the original
			command. It also contains info on how long TQM took to
			execute this command.

remove_msdu_cmd_type
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			<enum 0 remove_head_msdus> Remove MSDUs starting from
			the head of the queue. These are the TLVs that have been the
			longest in the flow queue)
			
			
			
			<enum 1 remove_aged_msdus> Remove MSDUs that have been
			in the transmit queue for too long. The
			Aged_Reference_timestamp field indicates the reference time
			value: All frames with a timestamp older then this one shall
			be removed
			
			
			
			<legal 0-1>

removed_msdu_count
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MSDUs removed from the queue
			
			<legal all>

msdu_head_is_fw_tx_notify_frame
			
			When set, the MSDU frame at the head of the MSDU queue
			(after the remove command has finished) is a
			FW_tx_notify_frame 
			
			<legal 0>

reserved_2a
			
			<legal 0>

removed_msdu_byte_count
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The SUM of all MSDU lengths of the removed MPDUs
			
			<legal all>

struct buffer_addr_info tx_notify_frame_addr_info
			
			Consumer: FW
			
			Producer: TQM
			
			
			
			Field only valid when 'MSDU_head_is_fw_tx_notify_frame'
			is set.
			
			
			
			Details of the physical address of the MSDU buffer +
			source buffer owner +  some SW meta data at the head of the
			queue.

remaining_flow_byte_count
			
			The sum of all the MSDU lengths remaining in the flow
			after selected MSDUs have been removed.
			
			<legal all>

remaining_msdu_count
			
			The number of MSDUs remaining in the flow after selected
			MSDUs have been removed.
			
			<legal all>

tx_flow_number
			
			Indicates the flow ID for which MSDUs were removed
			
			<legal all>

reserved_8a
			
			<legal 0>

reserved_9a
			
			<legal 0>

reserved_10a
			
			<legal 0>

looping_count
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/

#define TQM_REMOVE_MSDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_OFFSET 0x00000000
#define TQM_REMOVE_MSDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_LSB 28
#define TQM_REMOVE_MSDU_STATUS_0_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_MASK 0xffffffff
#define TQM_REMOVE_MSDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_OFFSET 0x00000004
#define TQM_REMOVE_MSDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_LSB 28
#define TQM_REMOVE_MSDU_STATUS_1_UNIFORM_TQM_STATUS_HEADER_STATUS_HEADER_MASK 0xffffffff

/* Description		TQM_REMOVE_MSDU_STATUS_2_REMOVE_MSDU_CMD_TYPE
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			<enum 0 remove_head_msdus> Remove MSDUs starting from
			the head of the queue. These are the TLVs that have been the
			longest in the flow queue)
			
			
			
			<enum 1 remove_aged_msdus> Remove MSDUs that have been
			in the transmit queue for too long. The
			Aged_Reference_timestamp field indicates the reference time
			value: All frames with a timestamp older then this one shall
			be removed
			
			
			
			<legal 0-1>
*/
#define TQM_REMOVE_MSDU_STATUS_2_REMOVE_MSDU_CMD_TYPE_OFFSET         0x00000008
#define TQM_REMOVE_MSDU_STATUS_2_REMOVE_MSDU_CMD_TYPE_LSB            0
#define TQM_REMOVE_MSDU_STATUS_2_REMOVE_MSDU_CMD_TYPE_MASK           0x0000000f

/* Description		TQM_REMOVE_MSDU_STATUS_2_REMOVED_MSDU_COUNT
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The number of MSDUs removed from the queue
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_2_REMOVED_MSDU_COUNT_OFFSET           0x00000008
#define TQM_REMOVE_MSDU_STATUS_2_REMOVED_MSDU_COUNT_LSB              4
#define TQM_REMOVE_MSDU_STATUS_2_REMOVED_MSDU_COUNT_MASK             0x000ffff0

/* Description		TQM_REMOVE_MSDU_STATUS_2_MSDU_HEAD_IS_FW_TX_NOTIFY_FRAME
			
			When set, the MSDU frame at the head of the MSDU queue
			(after the remove command has finished) is a
			FW_tx_notify_frame 
			
			<legal 0>
*/
#define TQM_REMOVE_MSDU_STATUS_2_MSDU_HEAD_IS_FW_TX_NOTIFY_FRAME_OFFSET 0x00000008
#define TQM_REMOVE_MSDU_STATUS_2_MSDU_HEAD_IS_FW_TX_NOTIFY_FRAME_LSB 20
#define TQM_REMOVE_MSDU_STATUS_2_MSDU_HEAD_IS_FW_TX_NOTIFY_FRAME_MASK 0x00100000

/* Description		TQM_REMOVE_MSDU_STATUS_2_RESERVED_2A
			
			<legal 0>
*/
#define TQM_REMOVE_MSDU_STATUS_2_RESERVED_2A_OFFSET                  0x00000008
#define TQM_REMOVE_MSDU_STATUS_2_RESERVED_2A_LSB                     21
#define TQM_REMOVE_MSDU_STATUS_2_RESERVED_2A_MASK                    0xffe00000

/* Description		TQM_REMOVE_MSDU_STATUS_3_REMOVED_MSDU_BYTE_COUNT
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			The SUM of all MSDU lengths of the removed MPDUs
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_3_REMOVED_MSDU_BYTE_COUNT_OFFSET      0x0000000c
#define TQM_REMOVE_MSDU_STATUS_3_REMOVED_MSDU_BYTE_COUNT_LSB         0
#define TQM_REMOVE_MSDU_STATUS_3_REMOVED_MSDU_BYTE_COUNT_MASK        0xffffffff
#define TQM_REMOVE_MSDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_OFFSET 0x00000010
#define TQM_REMOVE_MSDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_LSB 0
#define TQM_REMOVE_MSDU_STATUS_4_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_MASK 0xffffffff
#define TQM_REMOVE_MSDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_OFFSET 0x00000014
#define TQM_REMOVE_MSDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_LSB 0
#define TQM_REMOVE_MSDU_STATUS_5_BUFFER_ADDR_INFO_TX_NOTIFY_FRAME_ADDR_INFO_MASK 0xffffffff

/* Description		TQM_REMOVE_MSDU_STATUS_6_REMAINING_FLOW_BYTE_COUNT
			
			The sum of all the MSDU lengths remaining in the flow
			after selected MSDUs have been removed.
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_6_REMAINING_FLOW_BYTE_COUNT_OFFSET    0x00000018
#define TQM_REMOVE_MSDU_STATUS_6_REMAINING_FLOW_BYTE_COUNT_LSB       0
#define TQM_REMOVE_MSDU_STATUS_6_REMAINING_FLOW_BYTE_COUNT_MASK      0xffffffff

/* Description		TQM_REMOVE_MSDU_STATUS_7_REMAINING_MSDU_COUNT
			
			The number of MSDUs remaining in the flow after selected
			MSDUs have been removed.
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_7_REMAINING_MSDU_COUNT_OFFSET         0x0000001c
#define TQM_REMOVE_MSDU_STATUS_7_REMAINING_MSDU_COUNT_LSB            0
#define TQM_REMOVE_MSDU_STATUS_7_REMAINING_MSDU_COUNT_MASK           0x0000ffff

/* Description		TQM_REMOVE_MSDU_STATUS_7_TX_FLOW_NUMBER
			
			Indicates the flow ID for which MSDUs were removed
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_7_TX_FLOW_NUMBER_OFFSET               0x0000001c
#define TQM_REMOVE_MSDU_STATUS_7_TX_FLOW_NUMBER_LSB                  16
#define TQM_REMOVE_MSDU_STATUS_7_TX_FLOW_NUMBER_MASK                 0xffff0000

/* Description		TQM_REMOVE_MSDU_STATUS_8_RESERVED_8A
			
			<legal 0>
*/
#define TQM_REMOVE_MSDU_STATUS_8_RESERVED_8A_OFFSET                  0x00000020
#define TQM_REMOVE_MSDU_STATUS_8_RESERVED_8A_LSB                     0
#define TQM_REMOVE_MSDU_STATUS_8_RESERVED_8A_MASK                    0xffffffff

/* Description		TQM_REMOVE_MSDU_STATUS_9_RESERVED_9A
			
			<legal 0>
*/
#define TQM_REMOVE_MSDU_STATUS_9_RESERVED_9A_OFFSET                  0x00000024
#define TQM_REMOVE_MSDU_STATUS_9_RESERVED_9A_LSB                     0
#define TQM_REMOVE_MSDU_STATUS_9_RESERVED_9A_MASK                    0xffffffff

/* Description		TQM_REMOVE_MSDU_STATUS_10_RESERVED_10A
			
			<legal 0>
*/
#define TQM_REMOVE_MSDU_STATUS_10_RESERVED_10A_OFFSET                0x00000028
#define TQM_REMOVE_MSDU_STATUS_10_RESERVED_10A_LSB                   0
#define TQM_REMOVE_MSDU_STATUS_10_RESERVED_10A_MASK                  0x0fffffff

/* Description		TQM_REMOVE_MSDU_STATUS_10_LOOPING_COUNT
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/
#define TQM_REMOVE_MSDU_STATUS_10_LOOPING_COUNT_OFFSET               0x00000028
#define TQM_REMOVE_MSDU_STATUS_10_LOOPING_COUNT_LSB                  28
#define TQM_REMOVE_MSDU_STATUS_10_LOOPING_COUNT_MASK                 0xf0000000


#endif // _TQM_REMOVE_MSDU_STATUS_H_
