// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_GET_MSDU_FLOW_STATS_H_
#define _TQM_GET_MSDU_FLOW_STATS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_cmd_header.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct uniform_tqm_cmd_header cmd_header;
//	1	flow_desc_addr_31_0[31:0]
//	2	flow_desc_addr_39_32[7:0], clear_stats[8], reserved_2a[31:9]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_GET_MSDU_FLOW_STATS 3

struct tqm_get_msdu_flow_stats {
    struct            uniform_tqm_cmd_header                       cmd_header;
             uint32_t flow_desc_addr_31_0             : 32; //[31:0]
             uint32_t flow_desc_addr_39_32            :  8, //[7:0]
                      clear_stats                     :  1, //[8]
                      reserved_2a                     : 23; //[31:9]
};

/*

struct uniform_tqm_cmd_header cmd_header
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Details for command execution tracking purposes.

flow_desc_addr_31_0
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Address (lower 32 bits) of the flow descriptor
			
			<legal all>

flow_desc_addr_39_32
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Address (upper 8 bits) of the flow descriptor
			
			<legal all>

clear_stats
			
			Clear stat settings....
			
			
			
			<enum 0 no_clear> Do NOT clear the stats after
			generating the status
			
			<enum 1 clear_the_stats> Clear the stats after
			generating the status. 
			
			
			
			The stats actually cleared are:
			
			Total_processed_MSDU_count
			
			Total_dropped_MSDU_count
			
			Total_processed_Flow_byte_count_31_0
			
			Total_processed_Flow_byte_count_48_32
			
			
			
			<legal 0-1>

reserved_2a
			
			<legal 0>
*/

#define TQM_GET_MSDU_FLOW_STATS_0_UNIFORM_TQM_CMD_HEADER_CMD_HEADER_OFFSET 0x00000000
#define TQM_GET_MSDU_FLOW_STATS_0_UNIFORM_TQM_CMD_HEADER_CMD_HEADER_LSB 9
#define TQM_GET_MSDU_FLOW_STATS_0_UNIFORM_TQM_CMD_HEADER_CMD_HEADER_MASK 0xffffffff

/* Description		TQM_GET_MSDU_FLOW_STATS_1_FLOW_DESC_ADDR_31_0
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Address (lower 32 bits) of the flow descriptor
			
			<legal all>
*/
#define TQM_GET_MSDU_FLOW_STATS_1_FLOW_DESC_ADDR_31_0_OFFSET         0x00000004
#define TQM_GET_MSDU_FLOW_STATS_1_FLOW_DESC_ADDR_31_0_LSB            0
#define TQM_GET_MSDU_FLOW_STATS_1_FLOW_DESC_ADDR_31_0_MASK           0xffffffff

/* Description		TQM_GET_MSDU_FLOW_STATS_2_FLOW_DESC_ADDR_39_32
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Address (upper 8 bits) of the flow descriptor
			
			<legal all>
*/
#define TQM_GET_MSDU_FLOW_STATS_2_FLOW_DESC_ADDR_39_32_OFFSET        0x00000008
#define TQM_GET_MSDU_FLOW_STATS_2_FLOW_DESC_ADDR_39_32_LSB           0
#define TQM_GET_MSDU_FLOW_STATS_2_FLOW_DESC_ADDR_39_32_MASK          0x000000ff

/* Description		TQM_GET_MSDU_FLOW_STATS_2_CLEAR_STATS
			
			Clear stat settings....
			
			
			
			<enum 0 no_clear> Do NOT clear the stats after
			generating the status
			
			<enum 1 clear_the_stats> Clear the stats after
			generating the status. 
			
			
			
			The stats actually cleared are:
			
			Total_processed_MSDU_count
			
			Total_dropped_MSDU_count
			
			Total_processed_Flow_byte_count_31_0
			
			Total_processed_Flow_byte_count_48_32
			
			
			
			<legal 0-1>
*/
#define TQM_GET_MSDU_FLOW_STATS_2_CLEAR_STATS_OFFSET                 0x00000008
#define TQM_GET_MSDU_FLOW_STATS_2_CLEAR_STATS_LSB                    8
#define TQM_GET_MSDU_FLOW_STATS_2_CLEAR_STATS_MASK                   0x00000100

/* Description		TQM_GET_MSDU_FLOW_STATS_2_RESERVED_2A
			
			<legal 0>
*/
#define TQM_GET_MSDU_FLOW_STATS_2_RESERVED_2A_OFFSET                 0x00000008
#define TQM_GET_MSDU_FLOW_STATS_2_RESERVED_2A_LSB                    9
#define TQM_GET_MSDU_FLOW_STATS_2_RESERVED_2A_MASK                   0xfffffe00


#endif // _TQM_GET_MSDU_FLOW_STATS_H_
