// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _EXPECTED_RESPONSE_H_
#define _EXPECTED_RESPONSE_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	tx_ad2_31_0[31:0]
//	1	tx_ad2_47_32[15:0], expected_response_type[20:16], response_to_response[23:21], su_ba_user_number[24], reserved_1[31:25]
//	2	ndp_sta_partial_aid_2_8_0[10:0], reserved_2[20:11], ndp_sta_partial_aid1_8_0[31:21]
//	3	ast_index[15:0], capture_ack_ba_sounding[16], capture_sounding_1str_20mhz[17], capture_sounding_1str_40mhz[18], capture_sounding_1str_80mhz[19], capture_sounding_1str_160mhz[20], reserved_3a[31:21]
//	4	fcs[8:0], reserved_4a[9], crc[13:10], scrambler_seed[20:14], reserved_4b[31:21]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_EXPECTED_RESPONSE 5

struct expected_response {
             uint32_t tx_ad2_31_0                     : 32; //[31:0]
             uint32_t tx_ad2_47_32                    : 16, //[15:0]
                      expected_response_type          :  5, //[20:16]
                      response_to_response            :  3, //[23:21]
                      su_ba_user_number               :  1, //[24]
                      reserved_1                      :  7; //[31:25]
             uint32_t ndp_sta_partial_aid_2_8_0       : 11, //[10:0]
                      reserved_2                      : 10, //[20:11]
                      ndp_sta_partial_aid1_8_0        : 11; //[31:21]
             uint32_t ast_index                       : 16, //[15:0]
                      capture_ack_ba_sounding         :  1, //[16]
                      capture_sounding_1str_20mhz     :  1, //[17]
                      capture_sounding_1str_40mhz     :  1, //[18]
                      capture_sounding_1str_80mhz     :  1, //[19]
                      capture_sounding_1str_160mhz    :  1, //[20]
                      reserved_3a                     : 11; //[31:21]
             uint32_t fcs                             :  9, //[8:0]
                      reserved_4a                     :  1, //[9]
                      crc                             :  4, //[13:10]
                      scrambler_seed                  :  7, //[20:14]
                      reserved_4b                     : 11; //[31:21]
};

/*

tx_ad2_31_0
			
			Lower 32 bits of the transmitter address (AD2) of the
			last packet which was transmitted, which is used by RXPCU in
			Proxy STA mode.

tx_ad2_47_32
			
			Upper 16 bits of the transmitter address (AD2) of the
			last packet which was transmitted, which is used by RXPCU in
			Proxy STA mode.

expected_response_type
			
			Provides insight for RXPCU of what type of response is
			expected in the medium. 
			
			
			
			Mainly used for debugging purposes.
			
			
			
			No matter what RXPCU receives, it shall always report it
			to TXPCU.
			
			
			
			Only special scenario where RXPCU will have to generate
			a RECEIVED_RESPONSE_INFO TLV , even when no actual MPDU with
			passing FCS was received is when the response_type is set
			to: frameless_phyrx_response_accepted
			
			
			
			<enum 0 No_response_expected>After transmission of this
			frame, no response in SIFS time is expected
			
			
			
			When TXPCU sees this setting, it shall not generated the
			EXPECTED_RESPONSE TLV.
			
			
			
			RXPCU should never see this setting
			
			<enum 1 ACK_expected>An ACK frame is expected as
			response
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 2 BA_64_bitmap_expected>BA with 64 bitmap is
			expected.
			
			
			
			PDG uses the size info to calculated response duration
			
			
			
			For TXPCU only the fact that it is a BA is important.
			Actual received BA size is not important
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 3 BA_256_expected>BA with 256 bitmap is expected.
			
			
			
			PDG uses the size info to calculated response duration
			
			
			
			For TXPCU only the fact that it is a BA is important.
			Actual received BA size is not important
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 4 ActionNoAck_expected>SW sets this after sending
			NDP or BR-Poll. 
			
			
			
			As PDG has no idea on how long the reception is going to
			be, the reception time of the response will have to be
			programmed by SW in the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 5 ACK_BA_expected>PDG uses the size info and
			assumes single BA format with ACK and 64 bitmap embedded. 
			
			If SW expects more bitmaps in case of multi-TID, is
			shall program the 'Extend_duration_value_bw...' field for
			additional duration time.
			
			For TXPCU only the fact that an ACK and/or BA is
			received is important. Reception of only ACK or BA is also
			considered a success.
			
			SW also typically sets this when sending VHT single
			MPDU. Some chip vendors might send BA rather than ACK in
			response to VHT single MPDU but still we want to accept BA
			as well. 
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 6 CTS_expected>SW sets this after queuing RTS
			frame as standalone packet and sending it.
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 7 ACK_Data_expected>SW sets this after sending
			PS-Poll. 
			
			
			
			For TXPCU either ACK and/or data reception is considered
			success.
			
			PDG basis it's response duration calculation on an ACK.
			For the data portion, SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 8 NDP_ACK_Expected>Reserved for 11ah usage. 
			
			<enum 9 NDP_MODIFIED_Ack>Reserved for 11ah usage 
			
			<enum 10 NDP_BA_Expected>Reserved for 11ah usage. 
			
			<enum 11 NDP_CTS_Expected>Reserved for 11ah usage
			
			<enum 12 NDP_ACK_or_NDP_MODIFIED_ACK_Expected>Reserved
			for 11ah usage
			
			<enum 13 UL_MU_BA_expected>TXPCU expects UL MU OFDMA or
			UL MU MIMO reception.
			
			As PDG does not know how RUs are assigned for the uplink
			portion, PDG can not calculate the uplink duration. Therefor
			SW shall program the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU It is TXPCUs responsibility to
			distinguish between the UL MU or SU
			
			<enum 14 UL_MU_BA_and_data_expected>TXPCU expects UL MU
			OFDMA or UL MU MIMO reception.
			
			As PDG does not know how RUs are assigned for the uplink
			portion, PDG can not calculate the uplink duration. Therefor
			SW shall program the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU It is TXPCUs responsibility to
			distinguish between the UL MU or SU
			
			<enum 15 any_response_to_this_device>Any response
			expected to be send to this device in SIFS time is
			acceptable. 
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 
			
			
			
			For TXPCU, UL MU or SU is both acceptable.
			
			
			
			Can be used for complex OFDMA scenarios. PDG can not
			calculate the uplink duration. Therefor SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 16 any_response_accepted>Any frame in the medium
			to this or any other device, is acceptable as response. 
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 
			
			
			
			For TXPCU, UL MU or SU is both acceptable.
			
			
			
			Can be used for complex OFDMA scenarios. PDG can not
			calculate the uplink duration. Therefor SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 17 frameless_phyrx_response_accepted>Any MU
			frameless reception generated by the PHY is acceptable. 
			
			
			
			PHY indicates this type of reception explicitly in TLV
			PHYRX_RSSI_LEGACY, field Reception_type ==
			reception_is_frameless
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU.
			
			
			
			This can be used for complex MU-MIMO or OFDMA scenarios,
			like receiving MU-CTS.
			
			
			
			PDG can not calculate the uplink duration. Therefor SW
			shall program the 'Extend_duration_value_bw...' field
			
			<enum 18 MU_CBF_expected>When selected, CBF frames are
			expected to be received in MU reception (uplink OFDMA or
			uplink MIMO)
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 

response_to_response
			
			Field indicates if after receiving the PPDU response
			(indicated in the field above), TXPCU is expected to
			generate a reponse to the response
			
			
			
			In case a response to response is expected, RXPCU shall
			first acknowledge the proper reception of the received
			frames, so that TXPCU can first wrapup that portion of the
			FES.
			
			
			
			<enum 0 None> No response after response allowed.
			
			<enum 1 SU_BA> The response after response that TXPCU is
			allowed to generate is a single BA. Even if RXPCU is
			indicating that multiple users are received, TXPCU shall
			only send a BA for 1 STA. Response_to_response rates can be
			found in fields 'response_to_response_rate_info_bw...'
			
			<enum 2 MU_BA> The response after response that TXPCU is
			allowed to generate is only Multi Destination Multi User BA.
			Response_to_response rates can be found in fields
			'response_to_response_rate_info_bw...'
			
			
			
			<enum 3 RESPONSE_TO_RESPONSE_CMD> A response to response
			is expected to be generated. In other words, RXPCU will
			likely indicate to TXPCU at the end of upcoming reception
			that a response is needed. TXPCU is however to ignore this
			indication from RXPCU, and assume for a moment that no
			response to response is needed, as all the details on how to
			handle this is provided in the next scheduling command,
			which is marked as a 'response_to_response' type.
			
			
			
			<legal    0-3>

su_ba_user_number
			
			Field only valid when Response_to_response is SU_BA
			
			
			
			Indicates the user number of which the BA will be send
			after receiving the uplink OFDMA.

reserved_1
			
			<legal 0>

ndp_sta_partial_aid_2_8_0
			
			This field is applicable only in 11ah mode of operation.
			This field carries the information needed for RxPCU to
			qualify valid NDP-CTS
			
			
			
			When an RTS is being transmitted, this field  provides
			the partial AID of STA/BSSID of the transmitter,so the
			received RA/BSSID of the NDP CTS response frame can be
			compared to validate it. This value is provided by SW for
			valiadating the NDP CTS. 
			
			
			
			This filed also carries information for TA of the NDP
			Modified ACK when an NDP PS-Poll is transmitted. 

reserved_2
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>

ndp_sta_partial_aid1_8_0
			
			This field is applicable only in 11ah mode of operation.
			This field carries the information needed for RxPCU to
			qualify valid NDP Modified ACK
			
			
			
			TxPCU provides the partial AID (RA) of the NDP PS-Poll
			frame. 

ast_index
			
			The AST index of the receive Ack/BA.  This information
			is provided from the TXPCU to the RXPCU for receive Ack/BA.

capture_ack_ba_sounding
			
			If set enables capture of sounding on Ack or BA as long
			as the corresponding capture_sounding_1str_##mhz bits is
			set.  If clear the capture of sounding on Ack or BA is
			disabled even if the corresponding
			capture_sounding_1str_##mhz is set.

capture_sounding_1str_20mhz
			
			Capture sounding for 1 stream 20 MHz receive packets

capture_sounding_1str_40mhz
			
			Capture sounding for 1 stream 40 MHz receive packets

capture_sounding_1str_80mhz
			
			Capture sounding for 1 stream 80 MHz receive packets

capture_sounding_1str_160mhz
			
			Capture sounding for 1 stream 160 MHz receive packets

reserved_3a
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>

fcs
			
			Tx Frame's FCS[31:23]
			
			
			
			TODO: describe what this is used for ...
			
			
			
			For aggregates and NDP frames, this field is reserved
			and TxPCU should populate this to Zero.

reserved_4a
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>

crc
			
			TODO: describe what this is used for ...
			
			
			
			Tx SIG's CRC[3:0]

scrambler_seed
			
			TODO: describe what this is used for ...
			
			
			
			Tx Frames SERVICE[6:0]

reserved_4b
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>
*/


/* Description		EXPECTED_RESPONSE_0_TX_AD2_31_0
			
			Lower 32 bits of the transmitter address (AD2) of the
			last packet which was transmitted, which is used by RXPCU in
			Proxy STA mode.
*/
#define EXPECTED_RESPONSE_0_TX_AD2_31_0_OFFSET                       0x00000000
#define EXPECTED_RESPONSE_0_TX_AD2_31_0_LSB                          0
#define EXPECTED_RESPONSE_0_TX_AD2_31_0_MASK                         0xffffffff

/* Description		EXPECTED_RESPONSE_1_TX_AD2_47_32
			
			Upper 16 bits of the transmitter address (AD2) of the
			last packet which was transmitted, which is used by RXPCU in
			Proxy STA mode.
*/
#define EXPECTED_RESPONSE_1_TX_AD2_47_32_OFFSET                      0x00000004
#define EXPECTED_RESPONSE_1_TX_AD2_47_32_LSB                         0
#define EXPECTED_RESPONSE_1_TX_AD2_47_32_MASK                        0x0000ffff

/* Description		EXPECTED_RESPONSE_1_EXPECTED_RESPONSE_TYPE
			
			Provides insight for RXPCU of what type of response is
			expected in the medium. 
			
			
			
			Mainly used for debugging purposes.
			
			
			
			No matter what RXPCU receives, it shall always report it
			to TXPCU.
			
			
			
			Only special scenario where RXPCU will have to generate
			a RECEIVED_RESPONSE_INFO TLV , even when no actual MPDU with
			passing FCS was received is when the response_type is set
			to: frameless_phyrx_response_accepted
			
			
			
			<enum 0 No_response_expected>After transmission of this
			frame, no response in SIFS time is expected
			
			
			
			When TXPCU sees this setting, it shall not generated the
			EXPECTED_RESPONSE TLV.
			
			
			
			RXPCU should never see this setting
			
			<enum 1 ACK_expected>An ACK frame is expected as
			response
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 2 BA_64_bitmap_expected>BA with 64 bitmap is
			expected.
			
			
			
			PDG uses the size info to calculated response duration
			
			
			
			For TXPCU only the fact that it is a BA is important.
			Actual received BA size is not important
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 3 BA_256_expected>BA with 256 bitmap is expected.
			
			
			
			PDG uses the size info to calculated response duration
			
			
			
			For TXPCU only the fact that it is a BA is important.
			Actual received BA size is not important
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 4 ActionNoAck_expected>SW sets this after sending
			NDP or BR-Poll. 
			
			
			
			As PDG has no idea on how long the reception is going to
			be, the reception time of the response will have to be
			programmed by SW in the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 5 ACK_BA_expected>PDG uses the size info and
			assumes single BA format with ACK and 64 bitmap embedded. 
			
			If SW expects more bitmaps in case of multi-TID, is
			shall program the 'Extend_duration_value_bw...' field for
			additional duration time.
			
			For TXPCU only the fact that an ACK and/or BA is
			received is important. Reception of only ACK or BA is also
			considered a success.
			
			SW also typically sets this when sending VHT single
			MPDU. Some chip vendors might send BA rather than ACK in
			response to VHT single MPDU but still we want to accept BA
			as well. 
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 6 CTS_expected>SW sets this after queuing RTS
			frame as standalone packet and sending it.
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received.
			
			<enum 7 ACK_Data_expected>SW sets this after sending
			PS-Poll. 
			
			
			
			For TXPCU either ACK and/or data reception is considered
			success.
			
			PDG basis it's response duration calculation on an ACK.
			For the data portion, SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 8 NDP_ACK_Expected>Reserved for 11ah usage. 
			
			<enum 9 NDP_MODIFIED_Ack>Reserved for 11ah usage 
			
			<enum 10 NDP_BA_Expected>Reserved for 11ah usage. 
			
			<enum 11 NDP_CTS_Expected>Reserved for 11ah usage
			
			<enum 12 NDP_ACK_or_NDP_MODIFIED_ACK_Expected>Reserved
			for 11ah usage
			
			<enum 13 UL_MU_BA_expected>TXPCU expects UL MU OFDMA or
			UL MU MIMO reception.
			
			As PDG does not know how RUs are assigned for the uplink
			portion, PDG can not calculate the uplink duration. Therefor
			SW shall program the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU It is TXPCUs responsibility to
			distinguish between the UL MU or SU
			
			<enum 14 UL_MU_BA_and_data_expected>TXPCU expects UL MU
			OFDMA or UL MU MIMO reception.
			
			As PDG does not know how RUs are assigned for the uplink
			portion, PDG can not calculate the uplink duration. Therefor
			SW shall program the 'Extend_duration_value_bw...' field
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU It is TXPCUs responsibility to
			distinguish between the UL MU or SU
			
			<enum 15 any_response_to_this_device>Any response
			expected to be send to this device in SIFS time is
			acceptable. 
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 
			
			
			
			For TXPCU, UL MU or SU is both acceptable.
			
			
			
			Can be used for complex OFDMA scenarios. PDG can not
			calculate the uplink duration. Therefor SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 16 any_response_accepted>Any frame in the medium
			to this or any other device, is acceptable as response. 
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 
			
			
			
			For TXPCU, UL MU or SU is both acceptable.
			
			
			
			Can be used for complex OFDMA scenarios. PDG can not
			calculate the uplink duration. Therefor SW shall program the
			'Extend_duration_value_bw...' field
			
			<enum 17 frameless_phyrx_response_accepted>Any MU
			frameless reception generated by the PHY is acceptable. 
			
			
			
			PHY indicates this type of reception explicitly in TLV
			PHYRX_RSSI_LEGACY, field Reception_type ==
			reception_is_frameless
			
			
			
			RXPCU will report any frame received, irrespective of it
			having been UL MU or SU.
			
			
			
			This can be used for complex MU-MIMO or OFDMA scenarios,
			like receiving MU-CTS.
			
			
			
			PDG can not calculate the uplink duration. Therefor SW
			shall program the 'Extend_duration_value_bw...' field
			
			<enum 18 MU_CBF_expected>When selected, CBF frames are
			expected to be received in MU reception (uplink OFDMA or
			uplink MIMO)
			
			
			
			RXPCU is just expecting any response. It is TXPCU who
			checks that the right response was received 
*/
#define EXPECTED_RESPONSE_1_EXPECTED_RESPONSE_TYPE_OFFSET            0x00000004
#define EXPECTED_RESPONSE_1_EXPECTED_RESPONSE_TYPE_LSB               16
#define EXPECTED_RESPONSE_1_EXPECTED_RESPONSE_TYPE_MASK              0x001f0000

/* Description		EXPECTED_RESPONSE_1_RESPONSE_TO_RESPONSE
			
			Field indicates if after receiving the PPDU response
			(indicated in the field above), TXPCU is expected to
			generate a reponse to the response
			
			
			
			In case a response to response is expected, RXPCU shall
			first acknowledge the proper reception of the received
			frames, so that TXPCU can first wrapup that portion of the
			FES.
			
			
			
			<enum 0 None> No response after response allowed.
			
			<enum 1 SU_BA> The response after response that TXPCU is
			allowed to generate is a single BA. Even if RXPCU is
			indicating that multiple users are received, TXPCU shall
			only send a BA for 1 STA. Response_to_response rates can be
			found in fields 'response_to_response_rate_info_bw...'
			
			<enum 2 MU_BA> The response after response that TXPCU is
			allowed to generate is only Multi Destination Multi User BA.
			Response_to_response rates can be found in fields
			'response_to_response_rate_info_bw...'
			
			
			
			<enum 3 RESPONSE_TO_RESPONSE_CMD> A response to response
			is expected to be generated. In other words, RXPCU will
			likely indicate to TXPCU at the end of upcoming reception
			that a response is needed. TXPCU is however to ignore this
			indication from RXPCU, and assume for a moment that no
			response to response is needed, as all the details on how to
			handle this is provided in the next scheduling command,
			which is marked as a 'response_to_response' type.
			
			
			
			<legal    0-3>
*/
#define EXPECTED_RESPONSE_1_RESPONSE_TO_RESPONSE_OFFSET              0x00000004
#define EXPECTED_RESPONSE_1_RESPONSE_TO_RESPONSE_LSB                 21
#define EXPECTED_RESPONSE_1_RESPONSE_TO_RESPONSE_MASK                0x00e00000

/* Description		EXPECTED_RESPONSE_1_SU_BA_USER_NUMBER
			
			Field only valid when Response_to_response is SU_BA
			
			
			
			Indicates the user number of which the BA will be send
			after receiving the uplink OFDMA.
*/
#define EXPECTED_RESPONSE_1_SU_BA_USER_NUMBER_OFFSET                 0x00000004
#define EXPECTED_RESPONSE_1_SU_BA_USER_NUMBER_LSB                    24
#define EXPECTED_RESPONSE_1_SU_BA_USER_NUMBER_MASK                   0x01000000

/* Description		EXPECTED_RESPONSE_1_RESERVED_1
			
			<legal 0>
*/
#define EXPECTED_RESPONSE_1_RESERVED_1_OFFSET                        0x00000004
#define EXPECTED_RESPONSE_1_RESERVED_1_LSB                           25
#define EXPECTED_RESPONSE_1_RESERVED_1_MASK                          0xfe000000

/* Description		EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID_2_8_0
			
			This field is applicable only in 11ah mode of operation.
			This field carries the information needed for RxPCU to
			qualify valid NDP-CTS
			
			
			
			When an RTS is being transmitted, this field  provides
			the partial AID of STA/BSSID of the transmitter,so the
			received RA/BSSID of the NDP CTS response frame can be
			compared to validate it. This value is provided by SW for
			valiadating the NDP CTS. 
			
			
			
			This filed also carries information for TA of the NDP
			Modified ACK when an NDP PS-Poll is transmitted. 
*/
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID_2_8_0_OFFSET         0x00000008
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID_2_8_0_LSB            0
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID_2_8_0_MASK           0x000007ff

/* Description		EXPECTED_RESPONSE_2_RESERVED_2
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>
*/
#define EXPECTED_RESPONSE_2_RESERVED_2_OFFSET                        0x00000008
#define EXPECTED_RESPONSE_2_RESERVED_2_LSB                           11
#define EXPECTED_RESPONSE_2_RESERVED_2_MASK                          0x001ff800

/* Description		EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID1_8_0
			
			This field is applicable only in 11ah mode of operation.
			This field carries the information needed for RxPCU to
			qualify valid NDP Modified ACK
			
			
			
			TxPCU provides the partial AID (RA) of the NDP PS-Poll
			frame. 
*/
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID1_8_0_OFFSET          0x00000008
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID1_8_0_LSB             21
#define EXPECTED_RESPONSE_2_NDP_STA_PARTIAL_AID1_8_0_MASK            0xffe00000

/* Description		EXPECTED_RESPONSE_3_AST_INDEX
			
			The AST index of the receive Ack/BA.  This information
			is provided from the TXPCU to the RXPCU for receive Ack/BA.
*/
#define EXPECTED_RESPONSE_3_AST_INDEX_OFFSET                         0x0000000c
#define EXPECTED_RESPONSE_3_AST_INDEX_LSB                            0
#define EXPECTED_RESPONSE_3_AST_INDEX_MASK                           0x0000ffff

/* Description		EXPECTED_RESPONSE_3_CAPTURE_ACK_BA_SOUNDING
			
			If set enables capture of sounding on Ack or BA as long
			as the corresponding capture_sounding_1str_##mhz bits is
			set.  If clear the capture of sounding on Ack or BA is
			disabled even if the corresponding
			capture_sounding_1str_##mhz is set.
*/
#define EXPECTED_RESPONSE_3_CAPTURE_ACK_BA_SOUNDING_OFFSET           0x0000000c
#define EXPECTED_RESPONSE_3_CAPTURE_ACK_BA_SOUNDING_LSB              16
#define EXPECTED_RESPONSE_3_CAPTURE_ACK_BA_SOUNDING_MASK             0x00010000

/* Description		EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_20MHZ
			
			Capture sounding for 1 stream 20 MHz receive packets
*/
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_20MHZ_OFFSET       0x0000000c
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_20MHZ_LSB          17
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_20MHZ_MASK         0x00020000

/* Description		EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_40MHZ
			
			Capture sounding for 1 stream 40 MHz receive packets
*/
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_40MHZ_OFFSET       0x0000000c
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_40MHZ_LSB          18
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_40MHZ_MASK         0x00040000

/* Description		EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_80MHZ
			
			Capture sounding for 1 stream 80 MHz receive packets
*/
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_80MHZ_OFFSET       0x0000000c
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_80MHZ_LSB          19
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_80MHZ_MASK         0x00080000

/* Description		EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_160MHZ
			
			Capture sounding for 1 stream 160 MHz receive packets
*/
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_160MHZ_OFFSET      0x0000000c
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_160MHZ_LSB         20
#define EXPECTED_RESPONSE_3_CAPTURE_SOUNDING_1STR_160MHZ_MASK        0x00100000

/* Description		EXPECTED_RESPONSE_3_RESERVED_3A
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>
*/
#define EXPECTED_RESPONSE_3_RESERVED_3A_OFFSET                       0x0000000c
#define EXPECTED_RESPONSE_3_RESERVED_3A_LSB                          21
#define EXPECTED_RESPONSE_3_RESERVED_3A_MASK                         0xffe00000

/* Description		EXPECTED_RESPONSE_4_FCS
			
			Tx Frame's FCS[31:23]
			
			
			
			TODO: describe what this is used for ...
			
			
			
			For aggregates and NDP frames, this field is reserved
			and TxPCU should populate this to Zero.
*/
#define EXPECTED_RESPONSE_4_FCS_OFFSET                               0x00000010
#define EXPECTED_RESPONSE_4_FCS_LSB                                  0
#define EXPECTED_RESPONSE_4_FCS_MASK                                 0x000001ff

/* Description		EXPECTED_RESPONSE_4_RESERVED_4A
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>
*/
#define EXPECTED_RESPONSE_4_RESERVED_4A_OFFSET                       0x00000010
#define EXPECTED_RESPONSE_4_RESERVED_4A_LSB                          9
#define EXPECTED_RESPONSE_4_RESERVED_4A_MASK                         0x00000200

/* Description		EXPECTED_RESPONSE_4_CRC
			
			TODO: describe what this is used for ...
			
			
			
			Tx SIG's CRC[3:0]
*/
#define EXPECTED_RESPONSE_4_CRC_OFFSET                               0x00000010
#define EXPECTED_RESPONSE_4_CRC_LSB                                  10
#define EXPECTED_RESPONSE_4_CRC_MASK                                 0x00003c00

/* Description		EXPECTED_RESPONSE_4_SCRAMBLER_SEED
			
			TODO: describe what this is used for ...
			
			
			
			Tx Frames SERVICE[6:0]
*/
#define EXPECTED_RESPONSE_4_SCRAMBLER_SEED_OFFSET                    0x00000010
#define EXPECTED_RESPONSE_4_SCRAMBLER_SEED_LSB                       14
#define EXPECTED_RESPONSE_4_SCRAMBLER_SEED_MASK                      0x001fc000

/* Description		EXPECTED_RESPONSE_4_RESERVED_4B
			
			Reserved: Generator should set to 0, consumer shall
			ignore <legal 0>
*/
#define EXPECTED_RESPONSE_4_RESERVED_4B_OFFSET                       0x00000010
#define EXPECTED_RESPONSE_4_RESERVED_4B_LSB                          21
#define EXPECTED_RESPONSE_4_RESERVED_4B_MASK                         0xffe00000


#endif // _EXPECTED_RESPONSE_H_
