// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MACTX_MU_UPLINK_USER_SETUP_H_
#define _MACTX_MU_UPLINK_USER_SETUP_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uplink_user_setup_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct uplink_user_setup_info uplink_user_setup_info_details_bw20;
//	1	struct uplink_user_setup_info uplink_user_setup_info_details_bw40;
//	2	struct uplink_user_setup_info uplink_user_setup_info_details_bw80;
//	3	struct uplink_user_setup_info uplink_user_setup_info_details_bw160;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_MACTX_MU_UPLINK_USER_SETUP 4

struct mactx_mu_uplink_user_setup {
    struct            uplink_user_setup_info                       uplink_user_setup_info_details_bw20;
    struct            uplink_user_setup_info                       uplink_user_setup_info_details_bw40;
    struct            uplink_user_setup_info                       uplink_user_setup_info_details_bw80;
    struct            uplink_user_setup_info                       uplink_user_setup_info_details_bw160;
};

/*

struct uplink_user_setup_info uplink_user_setup_info_details_bw20
			
			Field Contains details for this user in case trigger
			frame went out in 20 MHz

struct uplink_user_setup_info uplink_user_setup_info_details_bw40
			
			Field Contains details for this user in case trigger
			frame went out in 40 MHz

struct uplink_user_setup_info uplink_user_setup_info_details_bw80
			
			Field Contains details for this user in case trigger
			frame went out in 160 MHz

struct uplink_user_setup_info uplink_user_setup_info_details_bw160
			
			Field Contains details for this user in case trigger
			frame went out in 160 MHz
*/

#define MACTX_MU_UPLINK_USER_SETUP_0_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW20_OFFSET 0x00000000
#define MACTX_MU_UPLINK_USER_SETUP_0_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW20_LSB 0
#define MACTX_MU_UPLINK_USER_SETUP_0_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW20_MASK 0xffffffff
#define MACTX_MU_UPLINK_USER_SETUP_1_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW40_OFFSET 0x00000004
#define MACTX_MU_UPLINK_USER_SETUP_1_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW40_LSB 0
#define MACTX_MU_UPLINK_USER_SETUP_1_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW40_MASK 0xffffffff
#define MACTX_MU_UPLINK_USER_SETUP_2_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW80_OFFSET 0x00000008
#define MACTX_MU_UPLINK_USER_SETUP_2_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW80_LSB 0
#define MACTX_MU_UPLINK_USER_SETUP_2_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW80_MASK 0xffffffff
#define MACTX_MU_UPLINK_USER_SETUP_3_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW160_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_USER_SETUP_3_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW160_LSB 0
#define MACTX_MU_UPLINK_USER_SETUP_3_UPLINK_USER_SETUP_INFO_UPLINK_USER_SETUP_INFO_DETAILS_BW160_MASK 0xffffffff


#endif // _MACTX_MU_UPLINK_USER_SETUP_H_
