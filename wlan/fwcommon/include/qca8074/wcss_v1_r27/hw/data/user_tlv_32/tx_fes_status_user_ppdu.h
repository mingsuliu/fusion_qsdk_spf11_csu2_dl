// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TX_FES_STATUS_USER_PPDU_H_
#define _TX_FES_STATUS_USER_PPDU_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	underflow_mpdu_count[8:0], data_underflow_warning[10:9], bw_drop_underflow_warning[11], qc_eosp_setting[12], fc_more_data_setting[13], fc_pwr_mgt_setting[14], mpdu_tx_count[23:15], user_blocked[24], reserved_0[31:25]
//	1	underflow_byte_count[15:0], coex_abort_mpdu_count_valid[16], coex_abort_mpdu_count[25:17], reserved_1[31:26]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TX_FES_STATUS_USER_PPDU 2

struct tx_fes_status_user_ppdu {
             uint32_t underflow_mpdu_count            :  9, //[8:0]
                      data_underflow_warning          :  2, //[10:9]
                      bw_drop_underflow_warning       :  1, //[11]
                      qc_eosp_setting                 :  1, //[12]
                      fc_more_data_setting            :  1, //[13]
                      fc_pwr_mgt_setting              :  1, //[14]
                      mpdu_tx_count                   :  9, //[23:15]
                      user_blocked                    :  1, //[24]
                      reserved_0                      :  7; //[31:25]
             uint32_t underflow_byte_count            : 16, //[15:0]
                      coex_abort_mpdu_count_valid     :  1, //[16]
                      coex_abort_mpdu_count           :  9, //[25:17]
                      reserved_1                      :  6; //[31:26]
};

/*

underflow_mpdu_count
			
			The MPDU count correctly received from TX DMA when the
			first underrun condition was detected
			
			<legal 0-256>

data_underflow_warning
			
			Mac data underflow warning for this user
			
			
			
			<enum 0 no_data_underrun> No data underflow
			
			<enum 1 data_underrun_between_mpdu> PCU experienced data
			underflow in between MPDUs
			
			<enum 2 data_underrun_within_mpdu> PCU experienced data
			underflow within an MPDU
			
			<legal 0-2>

bw_drop_underflow_warning
			
			When set, data underflow happened while TXPCU was busy
			with dropping a frame that is only allowed to go out at
			certain BW, which is not the BW of the current transmission
			
			<legal all>

qc_eosp_setting
			
			This field indicates if TX PCU set the eosp bit in the
			QoS control field for this user (indicated in field
			User_Id.)
			
			0: No action
			
			1: eosp bit is set in all the transmitted frames. This
			is done upon request of the PDG.
			
			<legal all>

fc_more_data_setting
			
			This field indicates if TX PCU set the More data bit in
			the Frame control field for this user (indicated in field
			User_Id.)
			
			
			
			0: No action
			
			1: More Data bit is set in all the transmitted frames.
			This is done upon request of the PDG.
			
			<legal all>

fc_pwr_mgt_setting
			
			This field indicates what the setting was of the pwr bit
			in the Frame control field for this user (indicated in field
			User_Id.)
			
			
			
			Note that TXPCU never manipulates the pwr bit in the FC
			field. Generating the correct setting is all managed by TX
			OLE.
			
			TXPCU just reports here what the pwr setting was of the
			(last) transmitted MPDU.
			
			
			
			0: pwr_mgt bit NOT set
			
			1: pwr_mgt bit is set
			
			<legal all>

mpdu_tx_count
			
			Number of MPDU frames transmitted
			
			
			
			Note: MPDUs that had an underrun during transmission
			will still be listed here. The assumption is that underrun
			is a very rare occasion, and any miscounting can therefor be
			accepted. If underrun happens too often, SW should change
			the density settings.
			
			<legal 0-256>

user_blocked
			
			When set, TXPCU received the TX_PEER_ENTRY TLV with bit
			'Block_this_user' set. As a result TXDMA did not push in any
			MPDUs for this user and non were expected to be transmitted.
			TXPCU will therefor NOT report any underrun conditions for
			this user
			
			<legal all>

reserved_0
			
			Reserved and not used by HW
			
			<legal 0>

underflow_byte_count
			
			The number of bytes correctly received for this MPDU
			when the first underrun condition was detected

coex_abort_mpdu_count_valid
			
			When set to 1, the (A-MPDU) transmission was silently
			aborted in the middle of transmission. The PHY faked the
			remaining transmission on the medium, so that TX PCU is
			still waiting for the BA frame to be received.  

coex_abort_mpdu_count
			
			Field only valid when 'Coex_abort_mpdu_count_valid' is
			set.
			
			The number of MPDU frames that were properly sent before
			the coex transmit abort request was received
			
			<legal 0-256>

reserved_1
			
			Reserved and not used by HW
			
			<legal 0>
*/


/* Description		TX_FES_STATUS_USER_PPDU_0_UNDERFLOW_MPDU_COUNT
			
			The MPDU count correctly received from TX DMA when the
			first underrun condition was detected
			
			<legal 0-256>
*/
#define TX_FES_STATUS_USER_PPDU_0_UNDERFLOW_MPDU_COUNT_OFFSET        0x00000000
#define TX_FES_STATUS_USER_PPDU_0_UNDERFLOW_MPDU_COUNT_LSB           0
#define TX_FES_STATUS_USER_PPDU_0_UNDERFLOW_MPDU_COUNT_MASK          0x000001ff

/* Description		TX_FES_STATUS_USER_PPDU_0_DATA_UNDERFLOW_WARNING
			
			Mac data underflow warning for this user
			
			
			
			<enum 0 no_data_underrun> No data underflow
			
			<enum 1 data_underrun_between_mpdu> PCU experienced data
			underflow in between MPDUs
			
			<enum 2 data_underrun_within_mpdu> PCU experienced data
			underflow within an MPDU
			
			<legal 0-2>
*/
#define TX_FES_STATUS_USER_PPDU_0_DATA_UNDERFLOW_WARNING_OFFSET      0x00000000
#define TX_FES_STATUS_USER_PPDU_0_DATA_UNDERFLOW_WARNING_LSB         9
#define TX_FES_STATUS_USER_PPDU_0_DATA_UNDERFLOW_WARNING_MASK        0x00000600

/* Description		TX_FES_STATUS_USER_PPDU_0_BW_DROP_UNDERFLOW_WARNING
			
			When set, data underflow happened while TXPCU was busy
			with dropping a frame that is only allowed to go out at
			certain BW, which is not the BW of the current transmission
			
			<legal all>
*/
#define TX_FES_STATUS_USER_PPDU_0_BW_DROP_UNDERFLOW_WARNING_OFFSET   0x00000000
#define TX_FES_STATUS_USER_PPDU_0_BW_DROP_UNDERFLOW_WARNING_LSB      11
#define TX_FES_STATUS_USER_PPDU_0_BW_DROP_UNDERFLOW_WARNING_MASK     0x00000800

/* Description		TX_FES_STATUS_USER_PPDU_0_QC_EOSP_SETTING
			
			This field indicates if TX PCU set the eosp bit in the
			QoS control field for this user (indicated in field
			User_Id.)
			
			0: No action
			
			1: eosp bit is set in all the transmitted frames. This
			is done upon request of the PDG.
			
			<legal all>
*/
#define TX_FES_STATUS_USER_PPDU_0_QC_EOSP_SETTING_OFFSET             0x00000000
#define TX_FES_STATUS_USER_PPDU_0_QC_EOSP_SETTING_LSB                12
#define TX_FES_STATUS_USER_PPDU_0_QC_EOSP_SETTING_MASK               0x00001000

/* Description		TX_FES_STATUS_USER_PPDU_0_FC_MORE_DATA_SETTING
			
			This field indicates if TX PCU set the More data bit in
			the Frame control field for this user (indicated in field
			User_Id.)
			
			
			
			0: No action
			
			1: More Data bit is set in all the transmitted frames.
			This is done upon request of the PDG.
			
			<legal all>
*/
#define TX_FES_STATUS_USER_PPDU_0_FC_MORE_DATA_SETTING_OFFSET        0x00000000
#define TX_FES_STATUS_USER_PPDU_0_FC_MORE_DATA_SETTING_LSB           13
#define TX_FES_STATUS_USER_PPDU_0_FC_MORE_DATA_SETTING_MASK          0x00002000

/* Description		TX_FES_STATUS_USER_PPDU_0_FC_PWR_MGT_SETTING
			
			This field indicates what the setting was of the pwr bit
			in the Frame control field for this user (indicated in field
			User_Id.)
			
			
			
			Note that TXPCU never manipulates the pwr bit in the FC
			field. Generating the correct setting is all managed by TX
			OLE.
			
			TXPCU just reports here what the pwr setting was of the
			(last) transmitted MPDU.
			
			
			
			0: pwr_mgt bit NOT set
			
			1: pwr_mgt bit is set
			
			<legal all>
*/
#define TX_FES_STATUS_USER_PPDU_0_FC_PWR_MGT_SETTING_OFFSET          0x00000000
#define TX_FES_STATUS_USER_PPDU_0_FC_PWR_MGT_SETTING_LSB             14
#define TX_FES_STATUS_USER_PPDU_0_FC_PWR_MGT_SETTING_MASK            0x00004000

/* Description		TX_FES_STATUS_USER_PPDU_0_MPDU_TX_COUNT
			
			Number of MPDU frames transmitted
			
			
			
			Note: MPDUs that had an underrun during transmission
			will still be listed here. The assumption is that underrun
			is a very rare occasion, and any miscounting can therefor be
			accepted. If underrun happens too often, SW should change
			the density settings.
			
			<legal 0-256>
*/
#define TX_FES_STATUS_USER_PPDU_0_MPDU_TX_COUNT_OFFSET               0x00000000
#define TX_FES_STATUS_USER_PPDU_0_MPDU_TX_COUNT_LSB                  15
#define TX_FES_STATUS_USER_PPDU_0_MPDU_TX_COUNT_MASK                 0x00ff8000

/* Description		TX_FES_STATUS_USER_PPDU_0_USER_BLOCKED
			
			When set, TXPCU received the TX_PEER_ENTRY TLV with bit
			'Block_this_user' set. As a result TXDMA did not push in any
			MPDUs for this user and non were expected to be transmitted.
			TXPCU will therefor NOT report any underrun conditions for
			this user
			
			<legal all>
*/
#define TX_FES_STATUS_USER_PPDU_0_USER_BLOCKED_OFFSET                0x00000000
#define TX_FES_STATUS_USER_PPDU_0_USER_BLOCKED_LSB                   24
#define TX_FES_STATUS_USER_PPDU_0_USER_BLOCKED_MASK                  0x01000000

/* Description		TX_FES_STATUS_USER_PPDU_0_RESERVED_0
			
			Reserved and not used by HW
			
			<legal 0>
*/
#define TX_FES_STATUS_USER_PPDU_0_RESERVED_0_OFFSET                  0x00000000
#define TX_FES_STATUS_USER_PPDU_0_RESERVED_0_LSB                     25
#define TX_FES_STATUS_USER_PPDU_0_RESERVED_0_MASK                    0xfe000000

/* Description		TX_FES_STATUS_USER_PPDU_1_UNDERFLOW_BYTE_COUNT
			
			The number of bytes correctly received for this MPDU
			when the first underrun condition was detected
*/
#define TX_FES_STATUS_USER_PPDU_1_UNDERFLOW_BYTE_COUNT_OFFSET        0x00000004
#define TX_FES_STATUS_USER_PPDU_1_UNDERFLOW_BYTE_COUNT_LSB           0
#define TX_FES_STATUS_USER_PPDU_1_UNDERFLOW_BYTE_COUNT_MASK          0x0000ffff

/* Description		TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_VALID
			
			When set to 1, the (A-MPDU) transmission was silently
			aborted in the middle of transmission. The PHY faked the
			remaining transmission on the medium, so that TX PCU is
			still waiting for the BA frame to be received.  
*/
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_VALID_OFFSET 0x00000004
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_VALID_LSB    16
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_VALID_MASK   0x00010000

/* Description		TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT
			
			Field only valid when 'Coex_abort_mpdu_count_valid' is
			set.
			
			The number of MPDU frames that were properly sent before
			the coex transmit abort request was received
			
			<legal 0-256>
*/
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_OFFSET       0x00000004
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_LSB          17
#define TX_FES_STATUS_USER_PPDU_1_COEX_ABORT_MPDU_COUNT_MASK         0x03fe0000

/* Description		TX_FES_STATUS_USER_PPDU_1_RESERVED_1
			
			Reserved and not used by HW
			
			<legal 0>
*/
#define TX_FES_STATUS_USER_PPDU_1_RESERVED_1_OFFSET                  0x00000004
#define TX_FES_STATUS_USER_PPDU_1_RESERVED_1_LSB                     26
#define TX_FES_STATUS_USER_PPDU_1_RESERVED_1_MASK                    0xfc000000


#endif // _TX_FES_STATUS_USER_PPDU_H_
