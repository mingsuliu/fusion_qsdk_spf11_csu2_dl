// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MACTX_VHT_SIG_B_MU20_H_
#define _MACTX_VHT_SIG_B_MU20_H_
#if !defined(__ASSEMBLER__)
#endif

#include "vht_sig_b_mu20_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct vht_sig_b_mu20_info mactx_vht_sig_b_mu20_info_details;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_MACTX_VHT_SIG_B_MU20 1

struct mactx_vht_sig_b_mu20 {
    struct            vht_sig_b_mu20_info                       mactx_vht_sig_b_mu20_info_details;
};

/*

struct vht_sig_b_mu20_info mactx_vht_sig_b_mu20_info_details
			
			See detailed description of the STRUCT
*/

#define MACTX_VHT_SIG_B_MU20_0_VHT_SIG_B_MU20_INFO_MACTX_VHT_SIG_B_MU20_INFO_DETAILS_OFFSET 0x00000000
#define MACTX_VHT_SIG_B_MU20_0_VHT_SIG_B_MU20_INFO_MACTX_VHT_SIG_B_MU20_INFO_DETAILS_LSB 0
#define MACTX_VHT_SIG_B_MU20_0_VHT_SIG_B_MU20_INFO_MACTX_VHT_SIG_B_MU20_INFO_DETAILS_MASK 0xffffffff


#endif // _MACTX_VHT_SIG_B_MU20_H_
