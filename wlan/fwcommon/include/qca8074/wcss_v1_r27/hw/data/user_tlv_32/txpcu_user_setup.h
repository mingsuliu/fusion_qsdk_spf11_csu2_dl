// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TXPCU_USER_SETUP_H_
#define _TXPCU_USER_SETUP_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	check_with_rxpcu_for_ack_ba[1:0], user_request_type[2], user_number[8:3], sw_peer_id[24:9], tid_specific_request[25], requested_tid[29:26], reserved_0a[31:30]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TXPCU_USER_SETUP 1

struct txpcu_user_setup {
             uint32_t check_with_rxpcu_for_ack_ba     :  2, //[1:0]
                      user_request_type               :  1, //[2]
                      user_number                     :  6, //[8:3]
                      sw_peer_id                      : 16, //[24:9]
                      tid_specific_request            :  1, //[25]
                      requested_tid                   :  4, //[29:26]
                      reserved_0a                     :  2; //[31:30]
};

/*

check_with_rxpcu_for_ack_ba
			
			Used in response to response schedule command to
			indicate that TXPCU shall first generate an ACK or BA frame
			
			
			
			Note that when the RX_FRAME_BITMAP_ACK comes back and
			bit SW_Response_tlv_from_crypto is set, TXPCU shall not
			generate any other bitmap requests as MAC micro controller
			will generate the required BA frame(s) and push them to
			TXPCU through the CRYPTO TLV interface.
			
			
			
			<enum 0  NO_ACK_BA_check> no need to do any checks for
			ACKs or BAs (for this user) with RXPCU.
			
			
			
			<enum 1 Single_ACK_BA_check> Only check once for this
			user if an ACK of BA response is needed
			
			<enum 2 multiple_ACK_BA_check> This feature allows multi
			TID BA acknowledgement generation. TXPCU will check multiple
			times if for this user an ACK of BA response is needed. 
			
			
			
			<legal 0-2>

user_request_type
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			
			
			<enum 0 bitmap_req_user_number_based> The request is
			based on a user_number. This method is typically used in
			case of SIFS response for Multi User BA
			
			
			
			<enum 1 bitmap_req_sw_peer_id_based> The request is
			based on the sw_peer_id.  This method is typically used in
			the response to response scenario where TXPCU got a new
			scheduling command for the response to response part, and SW
			now explicitly indicates for which STAs a BA shall be
			requested.
			
			<legal all>

user_number
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			and 
			
			
			
			
			The user number for which the bitmap is requested.
			
			<legal all>

sw_peer_id
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			and 
			
			User_request_type is set to bitmap_req_sw_peer_id_based
			
			
			
			The sw_peer_id for which the bitmap is requested. 
			
			<legal all>

tid_specific_request
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			
			
			When set, the request is going out for a specific TID,
			indicated in field TID
			
			
			
			When clear, it is up to RXPCU to determine in which
			order it wants to return bitmaps to TXPCU. Note that these
			bitmaps do need to all belong the the requested user, as
			Explicit_user_request has also been set. 
			
			<legal all>

requested_tid
			
			Field only valid when Tid_specific_request is set
			
			
			
			The TID for which a BA bitmap is requested
			
			<legal all>

reserved_0a
			
			<legal 0>
*/


/* Description		TXPCU_USER_SETUP_0_CHECK_WITH_RXPCU_FOR_ACK_BA
			
			Used in response to response schedule command to
			indicate that TXPCU shall first generate an ACK or BA frame
			
			
			
			Note that when the RX_FRAME_BITMAP_ACK comes back and
			bit SW_Response_tlv_from_crypto is set, TXPCU shall not
			generate any other bitmap requests as MAC micro controller
			will generate the required BA frame(s) and push them to
			TXPCU through the CRYPTO TLV interface.
			
			
			
			<enum 0  NO_ACK_BA_check> no need to do any checks for
			ACKs or BAs (for this user) with RXPCU.
			
			
			
			<enum 1 Single_ACK_BA_check> Only check once for this
			user if an ACK of BA response is needed
			
			<enum 2 multiple_ACK_BA_check> This feature allows multi
			TID BA acknowledgement generation. TXPCU will check multiple
			times if for this user an ACK of BA response is needed. 
			
			
			
			<legal 0-2>
*/
#define TXPCU_USER_SETUP_0_CHECK_WITH_RXPCU_FOR_ACK_BA_OFFSET        0x00000000
#define TXPCU_USER_SETUP_0_CHECK_WITH_RXPCU_FOR_ACK_BA_LSB           0
#define TXPCU_USER_SETUP_0_CHECK_WITH_RXPCU_FOR_ACK_BA_MASK          0x00000003

/* Description		TXPCU_USER_SETUP_0_USER_REQUEST_TYPE
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			
			
			<enum 0 bitmap_req_user_number_based> The request is
			based on a user_number. This method is typically used in
			case of SIFS response for Multi User BA
			
			
			
			<enum 1 bitmap_req_sw_peer_id_based> The request is
			based on the sw_peer_id.  This method is typically used in
			the response to response scenario where TXPCU got a new
			scheduling command for the response to response part, and SW
			now explicitly indicates for which STAs a BA shall be
			requested.
			
			<legal all>
*/
#define TXPCU_USER_SETUP_0_USER_REQUEST_TYPE_OFFSET                  0x00000000
#define TXPCU_USER_SETUP_0_USER_REQUEST_TYPE_LSB                     2
#define TXPCU_USER_SETUP_0_USER_REQUEST_TYPE_MASK                    0x00000004

/* Description		TXPCU_USER_SETUP_0_USER_NUMBER
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			and 
			
			
			
			
			The user number for which the bitmap is requested.
			
			<legal all>
*/
#define TXPCU_USER_SETUP_0_USER_NUMBER_OFFSET                        0x00000000
#define TXPCU_USER_SETUP_0_USER_NUMBER_LSB                           3
#define TXPCU_USER_SETUP_0_USER_NUMBER_MASK                          0x000001f8

/* Description		TXPCU_USER_SETUP_0_SW_PEER_ID
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			and 
			
			User_request_type is set to bitmap_req_sw_peer_id_based
			
			
			
			The sw_peer_id for which the bitmap is requested. 
			
			<legal all>
*/
#define TXPCU_USER_SETUP_0_SW_PEER_ID_OFFSET                         0x00000000
#define TXPCU_USER_SETUP_0_SW_PEER_ID_LSB                            9
#define TXPCU_USER_SETUP_0_SW_PEER_ID_MASK                           0x01fffe00

/* Description		TXPCU_USER_SETUP_0_TID_SPECIFIC_REQUEST
			
			Field only valid when Check_with_rxpcu_for_ACK_BA is set
			to Single_ACK_BA_check or multiple_ACK_BA_check
			
			
			
			When set, the request is going out for a specific TID,
			indicated in field TID
			
			
			
			When clear, it is up to RXPCU to determine in which
			order it wants to return bitmaps to TXPCU. Note that these
			bitmaps do need to all belong the the requested user, as
			Explicit_user_request has also been set. 
			
			<legal all>
*/
#define TXPCU_USER_SETUP_0_TID_SPECIFIC_REQUEST_OFFSET               0x00000000
#define TXPCU_USER_SETUP_0_TID_SPECIFIC_REQUEST_LSB                  25
#define TXPCU_USER_SETUP_0_TID_SPECIFIC_REQUEST_MASK                 0x02000000

/* Description		TXPCU_USER_SETUP_0_REQUESTED_TID
			
			Field only valid when Tid_specific_request is set
			
			
			
			The TID for which a BA bitmap is requested
			
			<legal all>
*/
#define TXPCU_USER_SETUP_0_REQUESTED_TID_OFFSET                      0x00000000
#define TXPCU_USER_SETUP_0_REQUESTED_TID_LSB                         26
#define TXPCU_USER_SETUP_0_REQUESTED_TID_MASK                        0x3c000000

/* Description		TXPCU_USER_SETUP_0_RESERVED_0A
			
			<legal 0>
*/
#define TXPCU_USER_SETUP_0_RESERVED_0A_OFFSET                        0x00000000
#define TXPCU_USER_SETUP_0_RESERVED_0A_LSB                           30
#define TXPCU_USER_SETUP_0_RESERVED_0A_MASK                          0xc0000000


#endif // _TXPCU_USER_SETUP_H_
