// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RX_PEER_ENTRY_H_
#define _RX_PEER_ENTRY_H_
#if !defined(__ASSEMBLER__)
#endif

#include "rx_peer_entry_details.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-68	struct rx_peer_entry_details rx_peer_entry;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RX_PEER_ENTRY 69

struct rx_peer_entry {
    struct            rx_peer_entry_details                       rx_peer_entry;
};

/*

struct rx_peer_entry_details rx_peer_entry
			
			See definition of rx_peer_entry_details
*/

#define RX_PEER_ENTRY_0_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000000
#define RX_PEER_ENTRY_0_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_0_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_1_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000004
#define RX_PEER_ENTRY_1_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_1_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_2_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000008
#define RX_PEER_ENTRY_2_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_2_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_3_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x0000000c
#define RX_PEER_ENTRY_3_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_3_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_4_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000010
#define RX_PEER_ENTRY_4_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_4_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_5_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000014
#define RX_PEER_ENTRY_5_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_5_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_6_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000018
#define RX_PEER_ENTRY_6_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_6_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_7_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x0000001c
#define RX_PEER_ENTRY_7_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_7_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_8_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000020
#define RX_PEER_ENTRY_8_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_8_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_9_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET   0x00000024
#define RX_PEER_ENTRY_9_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB      0
#define RX_PEER_ENTRY_9_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK     0xffffffff
#define RX_PEER_ENTRY_10_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000028
#define RX_PEER_ENTRY_10_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_10_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_11_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000002c
#define RX_PEER_ENTRY_11_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_11_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_12_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000030
#define RX_PEER_ENTRY_12_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_12_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_13_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000034
#define RX_PEER_ENTRY_13_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_13_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_14_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000038
#define RX_PEER_ENTRY_14_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_14_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_15_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000003c
#define RX_PEER_ENTRY_15_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_15_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_16_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000040
#define RX_PEER_ENTRY_16_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_16_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_17_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000044
#define RX_PEER_ENTRY_17_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_17_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_18_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000048
#define RX_PEER_ENTRY_18_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_18_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_19_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000004c
#define RX_PEER_ENTRY_19_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_19_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_20_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000050
#define RX_PEER_ENTRY_20_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_20_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_21_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000054
#define RX_PEER_ENTRY_21_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_21_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_22_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000058
#define RX_PEER_ENTRY_22_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_22_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_23_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000005c
#define RX_PEER_ENTRY_23_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_23_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_24_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000060
#define RX_PEER_ENTRY_24_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_24_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_25_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000064
#define RX_PEER_ENTRY_25_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_25_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_26_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000068
#define RX_PEER_ENTRY_26_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_26_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_27_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000006c
#define RX_PEER_ENTRY_27_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_27_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_28_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000070
#define RX_PEER_ENTRY_28_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_28_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_29_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000074
#define RX_PEER_ENTRY_29_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_29_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_30_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000078
#define RX_PEER_ENTRY_30_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_30_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_31_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000007c
#define RX_PEER_ENTRY_31_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_31_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_32_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000080
#define RX_PEER_ENTRY_32_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_32_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_33_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000084
#define RX_PEER_ENTRY_33_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_33_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_34_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000088
#define RX_PEER_ENTRY_34_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_34_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_35_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000008c
#define RX_PEER_ENTRY_35_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_35_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_36_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000090
#define RX_PEER_ENTRY_36_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_36_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_37_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000094
#define RX_PEER_ENTRY_37_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_37_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_38_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000098
#define RX_PEER_ENTRY_38_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_38_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_39_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000009c
#define RX_PEER_ENTRY_39_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_39_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_40_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000a0
#define RX_PEER_ENTRY_40_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_40_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_41_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000a4
#define RX_PEER_ENTRY_41_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_41_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_42_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000a8
#define RX_PEER_ENTRY_42_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_42_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_43_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000ac
#define RX_PEER_ENTRY_43_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_43_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_44_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000b0
#define RX_PEER_ENTRY_44_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_44_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_45_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000b4
#define RX_PEER_ENTRY_45_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_45_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_46_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000b8
#define RX_PEER_ENTRY_46_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_46_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_47_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000bc
#define RX_PEER_ENTRY_47_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_47_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_48_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000c0
#define RX_PEER_ENTRY_48_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_48_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_49_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000c4
#define RX_PEER_ENTRY_49_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_49_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_50_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000c8
#define RX_PEER_ENTRY_50_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_50_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_51_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000cc
#define RX_PEER_ENTRY_51_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_51_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_52_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000d0
#define RX_PEER_ENTRY_52_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_52_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_53_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000d4
#define RX_PEER_ENTRY_53_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_53_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_54_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000d8
#define RX_PEER_ENTRY_54_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_54_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_55_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000dc
#define RX_PEER_ENTRY_55_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_55_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_56_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000e0
#define RX_PEER_ENTRY_56_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_56_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_57_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000e4
#define RX_PEER_ENTRY_57_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_57_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_58_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000e8
#define RX_PEER_ENTRY_58_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_58_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_59_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000ec
#define RX_PEER_ENTRY_59_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_59_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_60_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000f0
#define RX_PEER_ENTRY_60_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_60_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_61_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000f4
#define RX_PEER_ENTRY_61_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_61_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_62_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000f8
#define RX_PEER_ENTRY_62_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_62_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_63_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x000000fc
#define RX_PEER_ENTRY_63_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_63_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_64_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000100
#define RX_PEER_ENTRY_64_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_64_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_65_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000104
#define RX_PEER_ENTRY_65_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_65_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_66_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000108
#define RX_PEER_ENTRY_66_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_66_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_67_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x0000010c
#define RX_PEER_ENTRY_67_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_67_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff
#define RX_PEER_ENTRY_68_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_OFFSET  0x00000110
#define RX_PEER_ENTRY_68_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_LSB     0
#define RX_PEER_ENTRY_68_RX_PEER_ENTRY_DETAILS_RX_PEER_ENTRY_MASK    0xffffffff


#endif // _RX_PEER_ENTRY_H_
