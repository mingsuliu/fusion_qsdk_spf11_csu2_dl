// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_ENTRANCE_RING_H_
#define _TQM_ENTRANCE_RING_H_
#if !defined(__ASSEMBLER__)
#endif

#include "tx_msdu_details.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-3	struct tx_msdu_details msdu_0;
//	4	flow_queue_addr_31_0[31:0]
//	5	flow_queue_addr_39_32[7:0], tqm_add_cmd_number[31:8]
//	6	tqm_status_required[0], tqm_drop_frame[1], tqm_status_ring[2], reserved_6[31:3]
//	7	reserved_7[19:0], ring_id[27:20], looping_count[31:28]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_ENTRANCE_RING 8

struct tqm_entrance_ring {
    struct            tx_msdu_details                       msdu_0;
             uint32_t flow_queue_addr_31_0            : 32; //[31:0]
             uint32_t flow_queue_addr_39_32           :  8, //[7:0]
                      tqm_add_cmd_number              : 24; //[31:8]
             uint32_t tqm_status_required             :  1, //[0]
                      tqm_drop_frame                  :  1, //[1]
                      tqm_status_ring                 :  1, //[2]
                      reserved_6                      : 29; //[31:3]
             uint32_t reserved_7                      : 20, //[19:0]
                      ring_id                         :  8, //[27:20]
                      looping_count                   :  4; //[31:28]
};

/*

struct tx_msdu_details msdu_0
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Details of MSDU to be added to the MSDU flow queue
			
			
			
			NOTE: 
			
			The field last_msdu_in_mpdu_flag is reserved when used
			in this RING and shall be set to 0

flow_queue_addr_31_0
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_Drop_frame' is NOT set.
			
			
			
			Address (lower 32 bits) of the flow queue descriptor. 
			
			<legal all>

flow_queue_addr_39_32
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_Drop_frame' is NOT set.
			
			
			
			Address (upper 8 bits) of the flow queue descriptor. 
			
			<legal all>

tqm_add_cmd_number
			
			Consumer: TQM/SW/DEBUG
			
			Producer: SW/FW/TCL
			
			
			
			This number can be used by SW to track, identify and
			link the created commands with the command statusses
			
			
			
			<legal all> 

tqm_status_required
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			<enum 0 NoStatus> TQM does not need to generate a status
			TLV for the execution of this command
			
			<enum 1 StatusRequired> TQM shall generate a status TLV
			for the execution of this command
			
			
			
			<legal all>

tqm_drop_frame
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			When set, TQM shall not push this frame into the flow
			queue, but immediately push this frame to the release ring.
			
			<legal all>

tqm_status_ring
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_status_required' is set.
			
			
			
			<enum 0 TQM_STATUS_RING_0> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 0
			
			<enum 1 TQM_STATUS_RING_1> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 1
			
			
			
			<legal all>

reserved_6
			
			<legal 0>

reserved_7
			
			<legal 0>

ring_id
			
			Consumer: DEBUG
			
			Producer: SW/TCL/SWITCH
			
			
			
			Ring ID.
			
			Every source has a unique ring id.
			
			
			
			Helps with debugging when dumping ring contents.
			
			<legal all>

looping_count
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/

#define TQM_ENTRANCE_RING_0_TX_MSDU_DETAILS_MSDU_0_OFFSET            0x00000000
#define TQM_ENTRANCE_RING_0_TX_MSDU_DETAILS_MSDU_0_LSB               28
#define TQM_ENTRANCE_RING_0_TX_MSDU_DETAILS_MSDU_0_MASK              0xffffffff
#define TQM_ENTRANCE_RING_1_TX_MSDU_DETAILS_MSDU_0_OFFSET            0x00000004
#define TQM_ENTRANCE_RING_1_TX_MSDU_DETAILS_MSDU_0_LSB               28
#define TQM_ENTRANCE_RING_1_TX_MSDU_DETAILS_MSDU_0_MASK              0xffffffff
#define TQM_ENTRANCE_RING_2_TX_MSDU_DETAILS_MSDU_0_OFFSET            0x00000008
#define TQM_ENTRANCE_RING_2_TX_MSDU_DETAILS_MSDU_0_LSB               28
#define TQM_ENTRANCE_RING_2_TX_MSDU_DETAILS_MSDU_0_MASK              0xffffffff
#define TQM_ENTRANCE_RING_3_TX_MSDU_DETAILS_MSDU_0_OFFSET            0x0000000c
#define TQM_ENTRANCE_RING_3_TX_MSDU_DETAILS_MSDU_0_LSB               28
#define TQM_ENTRANCE_RING_3_TX_MSDU_DETAILS_MSDU_0_MASK              0xffffffff

/* Description		TQM_ENTRANCE_RING_4_FLOW_QUEUE_ADDR_31_0
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_Drop_frame' is NOT set.
			
			
			
			Address (lower 32 bits) of the flow queue descriptor. 
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_4_FLOW_QUEUE_ADDR_31_0_OFFSET              0x00000010
#define TQM_ENTRANCE_RING_4_FLOW_QUEUE_ADDR_31_0_LSB                 0
#define TQM_ENTRANCE_RING_4_FLOW_QUEUE_ADDR_31_0_MASK                0xffffffff

/* Description		TQM_ENTRANCE_RING_5_FLOW_QUEUE_ADDR_39_32
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_Drop_frame' is NOT set.
			
			
			
			Address (upper 8 bits) of the flow queue descriptor. 
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_5_FLOW_QUEUE_ADDR_39_32_OFFSET             0x00000014
#define TQM_ENTRANCE_RING_5_FLOW_QUEUE_ADDR_39_32_LSB                0
#define TQM_ENTRANCE_RING_5_FLOW_QUEUE_ADDR_39_32_MASK               0x000000ff

/* Description		TQM_ENTRANCE_RING_5_TQM_ADD_CMD_NUMBER
			
			Consumer: TQM/SW/DEBUG
			
			Producer: SW/FW/TCL
			
			
			
			This number can be used by SW to track, identify and
			link the created commands with the command statusses
			
			
			
			<legal all> 
*/
#define TQM_ENTRANCE_RING_5_TQM_ADD_CMD_NUMBER_OFFSET                0x00000014
#define TQM_ENTRANCE_RING_5_TQM_ADD_CMD_NUMBER_LSB                   8
#define TQM_ENTRANCE_RING_5_TQM_ADD_CMD_NUMBER_MASK                  0xffffff00

/* Description		TQM_ENTRANCE_RING_6_TQM_STATUS_REQUIRED
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			<enum 0 NoStatus> TQM does not need to generate a status
			TLV for the execution of this command
			
			<enum 1 StatusRequired> TQM shall generate a status TLV
			for the execution of this command
			
			
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_6_TQM_STATUS_REQUIRED_OFFSET               0x00000018
#define TQM_ENTRANCE_RING_6_TQM_STATUS_REQUIRED_LSB                  0
#define TQM_ENTRANCE_RING_6_TQM_STATUS_REQUIRED_MASK                 0x00000001

/* Description		TQM_ENTRANCE_RING_6_TQM_DROP_FRAME
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			When set, TQM shall not push this frame into the flow
			queue, but immediately push this frame to the release ring.
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_6_TQM_DROP_FRAME_OFFSET                    0x00000018
#define TQM_ENTRANCE_RING_6_TQM_DROP_FRAME_LSB                       1
#define TQM_ENTRANCE_RING_6_TQM_DROP_FRAME_MASK                      0x00000002

/* Description		TQM_ENTRANCE_RING_6_TQM_STATUS_RING
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			Field only valid when 'TQM_status_required' is set.
			
			
			
			<enum 0 TQM_STATUS_RING_0> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 0
			
			<enum 1 TQM_STATUS_RING_1> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 1
			
			
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_6_TQM_STATUS_RING_OFFSET                   0x00000018
#define TQM_ENTRANCE_RING_6_TQM_STATUS_RING_LSB                      2
#define TQM_ENTRANCE_RING_6_TQM_STATUS_RING_MASK                     0x00000004

/* Description		TQM_ENTRANCE_RING_6_RESERVED_6
			
			<legal 0>
*/
#define TQM_ENTRANCE_RING_6_RESERVED_6_OFFSET                        0x00000018
#define TQM_ENTRANCE_RING_6_RESERVED_6_LSB                           3
#define TQM_ENTRANCE_RING_6_RESERVED_6_MASK                          0xfffffff8

/* Description		TQM_ENTRANCE_RING_7_RESERVED_7
			
			<legal 0>
*/
#define TQM_ENTRANCE_RING_7_RESERVED_7_OFFSET                        0x0000001c
#define TQM_ENTRANCE_RING_7_RESERVED_7_LSB                           0
#define TQM_ENTRANCE_RING_7_RESERVED_7_MASK                          0x000fffff

/* Description		TQM_ENTRANCE_RING_7_RING_ID
			
			Consumer: DEBUG
			
			Producer: SW/TCL/SWITCH
			
			
			
			Ring ID.
			
			Every source has a unique ring id.
			
			
			
			Helps with debugging when dumping ring contents.
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_7_RING_ID_OFFSET                           0x0000001c
#define TQM_ENTRANCE_RING_7_RING_ID_LSB                              20
#define TQM_ENTRANCE_RING_7_RING_ID_MASK                             0x0ff00000

/* Description		TQM_ENTRANCE_RING_7_LOOPING_COUNT
			
			Consumer: TQM
			
			Producer: SW/FW/TCL
			
			
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/
#define TQM_ENTRANCE_RING_7_LOOPING_COUNT_OFFSET                     0x0000001c
#define TQM_ENTRANCE_RING_7_LOOPING_COUNT_LSB                        28
#define TQM_ENTRANCE_RING_7_LOOPING_COUNT_MASK                       0xf0000000


#endif // _TQM_ENTRANCE_RING_H_
