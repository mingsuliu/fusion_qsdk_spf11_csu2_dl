// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _VHT_SIG_B_MU20_INFO_H_
#define _VHT_SIG_B_MU20_INFO_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	length[15:0], mcs[19:16], tail[25:20], mu_user_number[28:26], reserved_0[31:29]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_VHT_SIG_B_MU20_INFO 1

struct vht_sig_b_mu20_info {
             uint32_t length                          : 16, //[15:0]
                      mcs                             :  4, //[19:16]
                      tail                            :  6, //[25:20]
                      mu_user_number                  :  3, //[28:26]
                      reserved_0                      :  3; //[31:29]
};

/*

length
			
			VHT-SIG-B Length (in units of 4 octets) = ceiling
			(LENGTH/4)  
			
			<legal all>

mcs
			
			Modulation as described in vht_sig_a mcs field 
			
			<legal 0-11>

tail
			
			Used to terminate the trellis of the convolutional
			decoder.
			
			<legal all>

mu_user_number
			
			Not part of VHT-SIG-B.
			
			Mapping from user number (BFer hardware specific) to
			mu_user_number. The reader is directed to the previous
			chapter (User Number) for a definition of the terms user and
			mu_user.  
			
			<legal 0-3>

reserved_0
			
			<legal 0>
*/


/* Description		VHT_SIG_B_MU20_INFO_0_LENGTH
			
			VHT-SIG-B Length (in units of 4 octets) = ceiling
			(LENGTH/4)  
			
			<legal all>
*/
#define VHT_SIG_B_MU20_INFO_0_LENGTH_OFFSET                          0x00000000
#define VHT_SIG_B_MU20_INFO_0_LENGTH_LSB                             0
#define VHT_SIG_B_MU20_INFO_0_LENGTH_MASK                            0x0000ffff

/* Description		VHT_SIG_B_MU20_INFO_0_MCS
			
			Modulation as described in vht_sig_a mcs field 
			
			<legal 0-11>
*/
#define VHT_SIG_B_MU20_INFO_0_MCS_OFFSET                             0x00000000
#define VHT_SIG_B_MU20_INFO_0_MCS_LSB                                16
#define VHT_SIG_B_MU20_INFO_0_MCS_MASK                               0x000f0000

/* Description		VHT_SIG_B_MU20_INFO_0_TAIL
			
			Used to terminate the trellis of the convolutional
			decoder.
			
			<legal all>
*/
#define VHT_SIG_B_MU20_INFO_0_TAIL_OFFSET                            0x00000000
#define VHT_SIG_B_MU20_INFO_0_TAIL_LSB                               20
#define VHT_SIG_B_MU20_INFO_0_TAIL_MASK                              0x03f00000

/* Description		VHT_SIG_B_MU20_INFO_0_MU_USER_NUMBER
			
			Not part of VHT-SIG-B.
			
			Mapping from user number (BFer hardware specific) to
			mu_user_number. The reader is directed to the previous
			chapter (User Number) for a definition of the terms user and
			mu_user.  
			
			<legal 0-3>
*/
#define VHT_SIG_B_MU20_INFO_0_MU_USER_NUMBER_OFFSET                  0x00000000
#define VHT_SIG_B_MU20_INFO_0_MU_USER_NUMBER_LSB                     26
#define VHT_SIG_B_MU20_INFO_0_MU_USER_NUMBER_MASK                    0x1c000000

/* Description		VHT_SIG_B_MU20_INFO_0_RESERVED_0
			
			<legal 0>
*/
#define VHT_SIG_B_MU20_INFO_0_RESERVED_0_OFFSET                      0x00000000
#define VHT_SIG_B_MU20_INFO_0_RESERVED_0_LSB                         29
#define VHT_SIG_B_MU20_INFO_0_RESERVED_0_MASK                        0xe0000000


#endif // _VHT_SIG_B_MU20_INFO_H_
