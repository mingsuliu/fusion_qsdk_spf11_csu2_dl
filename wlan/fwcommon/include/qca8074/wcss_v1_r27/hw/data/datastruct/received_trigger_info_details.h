// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RECEIVED_TRIGGER_INFO_DETAILS_H_
#define _RECEIVED_TRIGGER_INFO_DETAILS_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	trigger_type[3:0], trigger_is_wildcard[4], trigger_source_sta_full_aid[17:5], frame_control_valid[18], qos_control_valid[19], he_control_trigger_info_valid[20], reserved_0b[31:21]
//	1	phy_ppdu_id[15:0], response_length[27:16], reserved_1a[31:28]
//	2	frame_control[15:0], qos_control[31:16]
//	3	sw_peer_id[15:0], reserved_3a[31:16]
//	4	he_control[31:0]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RECEIVED_TRIGGER_INFO_DETAILS 5

struct received_trigger_info_details {
             uint32_t trigger_type                    :  4, //[3:0]
                      trigger_is_wildcard             :  1, //[4]
                      trigger_source_sta_full_aid     : 13, //[17:5]
                      frame_control_valid             :  1, //[18]
                      qos_control_valid               :  1, //[19]
                      he_control_trigger_info_valid   :  1, //[20]
                      reserved_0b                     : 11; //[31:21]
             uint32_t phy_ppdu_id                     : 16, //[15:0]
                      response_length                 : 12, //[27:16]
                      reserved_1a                     :  4; //[31:28]
             uint32_t frame_control                   : 16, //[15:0]
                      qos_control                     : 16; //[31:16]
             uint32_t sw_peer_id                      : 16, //[15:0]
                      reserved_3a                     : 16; //[31:16]
             uint32_t he_control                      : 32; //[31:0]
};

/*

trigger_type
			
			This field indicates for what type of trigger has been
			received
			
			
			
			<enum 0 SCH_Qboost_trigger> 
			
			<enum 1 SCH_PSPOLL_trigger>
			
			<enum 2 SCH_UAPSD_trigger>
			
			<enum 3 SCH_11ax_basic_trigger> 
			
			<enum 4 SCH_11ax_buf_size_trigger> 
			
			<enum 5 SCH_11ax_brpoll_trigger> 
			
			<enum 6 SCH_11ax_mu_bar_trigger> 
			
			<enum 7 SCH_11ax_mu_rts_trigger> 
			
			
			
			<enum 8 SCH_11ax_basic_or_buf_size_trigger> 
			
			NOTE: this field will never be set by RXPCU, but is
			there for SW programming of the SCH response handling ....
			
			
			
			<legal 0-8>

trigger_is_wildcard
			
			Field Only valid when Trigger_type  is an 11ax related
			trigger of type SCH_11ax_basic_trigger or
			SCH_11ax_buf_size_trigger
			
			
			
			When set, the received trigger is based on a wildcard. 
			
			<legal all>

trigger_source_sta_full_aid
			
			The sta_full_aid of the sta/ap that generated the
			trigger.
			
			Comes from the address_search_entry
			
			
			
			<legal all>

frame_control_valid
			
			When set, the 'frame_control' field contains valid info
			
			<legal all>

qos_control_valid
			
			When set, the 'QoS_control' field contains valid info
			
			<legal all>

he_control_trigger_info_valid
			
			When set, the HE trigger is indicated by an HE-control
			field.
			
			<legal all>

reserved_0b
			
			<legal 0>

phy_ppdu_id
			
			A ppdu counter value that PHY increments for every PPDU
			received. The counter value wraps around  
			
			<legal all>

response_length
			
			Field only valid in case of OFDMA trigger
			
			
			
			Indicates the value of the L-SIG Length field of the HE
			trigger-based PPDU that is the response to the Trigger frame
			
			
			
			<legal all>

reserved_1a
			
			<legal 0>

frame_control
			
			frame control field of the received frame
			
			<legal 0>

qos_control
			
			frame control field of the received frame (if present)
			
			<legal 0>

sw_peer_id
			
			A unique identifier for this STA. Extracted from the
			Address_Search_Entry
			
			
			
			Used by the SCH to find linkage between this trigger and
			potentially pre-programmed responses.
			
			<legal all>

reserved_3a
			
			<legal 0>

he_control
			
			Field only valid when HE_control_trigger_info_valid is
			set
			
			
			
			This is the 'RAW HE_CONTROL field' that indicated it
			included an UL trigger.
			
			<legal all>
*/


/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_TYPE
			
			This field indicates for what type of trigger has been
			received
			
			
			
			<enum 0 SCH_Qboost_trigger> 
			
			<enum 1 SCH_PSPOLL_trigger>
			
			<enum 2 SCH_UAPSD_trigger>
			
			<enum 3 SCH_11ax_basic_trigger> 
			
			<enum 4 SCH_11ax_buf_size_trigger> 
			
			<enum 5 SCH_11ax_brpoll_trigger> 
			
			<enum 6 SCH_11ax_mu_bar_trigger> 
			
			<enum 7 SCH_11ax_mu_rts_trigger> 
			
			
			
			<enum 8 SCH_11ax_basic_or_buf_size_trigger> 
			
			NOTE: this field will never be set by RXPCU, but is
			there for SW programming of the SCH response handling ....
			
			
			
			<legal 0-8>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_TYPE_OFFSET          0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_TYPE_LSB             0
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_TYPE_MASK            0x0000000f

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_IS_WILDCARD
			
			Field Only valid when Trigger_type  is an 11ax related
			trigger of type SCH_11ax_basic_trigger or
			SCH_11ax_buf_size_trigger
			
			
			
			When set, the received trigger is based on a wildcard. 
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_IS_WILDCARD_OFFSET   0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_IS_WILDCARD_LSB      4
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_IS_WILDCARD_MASK     0x00000010

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_SOURCE_STA_FULL_AID
			
			The sta_full_aid of the sta/ap that generated the
			trigger.
			
			Comes from the address_search_entry
			
			
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_SOURCE_STA_FULL_AID_OFFSET 0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_SOURCE_STA_FULL_AID_LSB 5
#define RECEIVED_TRIGGER_INFO_DETAILS_0_TRIGGER_SOURCE_STA_FULL_AID_MASK 0x0003ffe0

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_FRAME_CONTROL_VALID
			
			When set, the 'frame_control' field contains valid info
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_FRAME_CONTROL_VALID_OFFSET   0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_FRAME_CONTROL_VALID_LSB      18
#define RECEIVED_TRIGGER_INFO_DETAILS_0_FRAME_CONTROL_VALID_MASK     0x00040000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_QOS_CONTROL_VALID
			
			When set, the 'QoS_control' field contains valid info
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_QOS_CONTROL_VALID_OFFSET     0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_QOS_CONTROL_VALID_LSB        19
#define RECEIVED_TRIGGER_INFO_DETAILS_0_QOS_CONTROL_VALID_MASK       0x00080000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_HE_CONTROL_TRIGGER_INFO_VALID
			
			When set, the HE trigger is indicated by an HE-control
			field.
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_HE_CONTROL_TRIGGER_INFO_VALID_OFFSET 0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_HE_CONTROL_TRIGGER_INFO_VALID_LSB 20
#define RECEIVED_TRIGGER_INFO_DETAILS_0_HE_CONTROL_TRIGGER_INFO_VALID_MASK 0x00100000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_0_RESERVED_0B
			
			<legal 0>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_0_RESERVED_0B_OFFSET           0x00000000
#define RECEIVED_TRIGGER_INFO_DETAILS_0_RESERVED_0B_LSB              21
#define RECEIVED_TRIGGER_INFO_DETAILS_0_RESERVED_0B_MASK             0xffe00000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_1_PHY_PPDU_ID
			
			A ppdu counter value that PHY increments for every PPDU
			received. The counter value wraps around  
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_1_PHY_PPDU_ID_OFFSET           0x00000004
#define RECEIVED_TRIGGER_INFO_DETAILS_1_PHY_PPDU_ID_LSB              0
#define RECEIVED_TRIGGER_INFO_DETAILS_1_PHY_PPDU_ID_MASK             0x0000ffff

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_1_RESPONSE_LENGTH
			
			Field only valid in case of OFDMA trigger
			
			
			
			Indicates the value of the L-SIG Length field of the HE
			trigger-based PPDU that is the response to the Trigger frame
			
			
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESPONSE_LENGTH_OFFSET       0x00000004
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESPONSE_LENGTH_LSB          16
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESPONSE_LENGTH_MASK         0x0fff0000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_1_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESERVED_1A_OFFSET           0x00000004
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESERVED_1A_LSB              28
#define RECEIVED_TRIGGER_INFO_DETAILS_1_RESERVED_1A_MASK             0xf0000000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_2_FRAME_CONTROL
			
			frame control field of the received frame
			
			<legal 0>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_2_FRAME_CONTROL_OFFSET         0x00000008
#define RECEIVED_TRIGGER_INFO_DETAILS_2_FRAME_CONTROL_LSB            0
#define RECEIVED_TRIGGER_INFO_DETAILS_2_FRAME_CONTROL_MASK           0x0000ffff

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_2_QOS_CONTROL
			
			frame control field of the received frame (if present)
			
			<legal 0>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_2_QOS_CONTROL_OFFSET           0x00000008
#define RECEIVED_TRIGGER_INFO_DETAILS_2_QOS_CONTROL_LSB              16
#define RECEIVED_TRIGGER_INFO_DETAILS_2_QOS_CONTROL_MASK             0xffff0000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_3_SW_PEER_ID
			
			A unique identifier for this STA. Extracted from the
			Address_Search_Entry
			
			
			
			Used by the SCH to find linkage between this trigger and
			potentially pre-programmed responses.
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_3_SW_PEER_ID_OFFSET            0x0000000c
#define RECEIVED_TRIGGER_INFO_DETAILS_3_SW_PEER_ID_LSB               0
#define RECEIVED_TRIGGER_INFO_DETAILS_3_SW_PEER_ID_MASK              0x0000ffff

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_3_RESERVED_3A
			
			<legal 0>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_3_RESERVED_3A_OFFSET           0x0000000c
#define RECEIVED_TRIGGER_INFO_DETAILS_3_RESERVED_3A_LSB              16
#define RECEIVED_TRIGGER_INFO_DETAILS_3_RESERVED_3A_MASK             0xffff0000

/* Description		RECEIVED_TRIGGER_INFO_DETAILS_4_HE_CONTROL
			
			Field only valid when HE_control_trigger_info_valid is
			set
			
			
			
			This is the 'RAW HE_CONTROL field' that indicated it
			included an UL trigger.
			
			<legal all>
*/
#define RECEIVED_TRIGGER_INFO_DETAILS_4_HE_CONTROL_OFFSET            0x00000010
#define RECEIVED_TRIGGER_INFO_DETAILS_4_HE_CONTROL_LSB               0
#define RECEIVED_TRIGGER_INFO_DETAILS_4_HE_CONTROL_MASK              0xffffffff


#endif // _RECEIVED_TRIGGER_INFO_DETAILS_H_
