// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TX_MSDU_LINK_ENTRY_PTR_H_
#define _TX_MSDU_LINK_ENTRY_PTR_H_
#if !defined(__ASSEMBLER__)
#endif

#include "buffer_addr_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct buffer_addr_info msdu_link_desc_addr_info;
//	2	msdu_index[3:0], reserved_2a[31:4]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TX_MSDU_LINK_ENTRY_PTR 3

struct tx_msdu_link_entry_ptr {
    struct            buffer_addr_info                       msdu_link_desc_addr_info;
             uint32_t msdu_index                      :  4, //[3:0]
                      reserved_2a                     : 28; //[31:4]
};

/*

struct buffer_addr_info msdu_link_desc_addr_info
			
			Details of the physical address of the msdu link
			descriptor.

msdu_index
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			MSDU Index within MSDU link descriptor
			
			<legal 0-6>

reserved_2a
			
			<legal 0>
*/

#define TX_MSDU_LINK_ENTRY_PTR_0_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_OFFSET 0x00000000
#define TX_MSDU_LINK_ENTRY_PTR_0_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_LSB 4
#define TX_MSDU_LINK_ENTRY_PTR_0_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_MASK 0xffffffff
#define TX_MSDU_LINK_ENTRY_PTR_1_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_OFFSET 0x00000004
#define TX_MSDU_LINK_ENTRY_PTR_1_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_LSB 4
#define TX_MSDU_LINK_ENTRY_PTR_1_BUFFER_ADDR_INFO_MSDU_LINK_DESC_ADDR_INFO_MASK 0xffffffff

/* Description		TX_MSDU_LINK_ENTRY_PTR_2_MSDU_INDEX
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			MSDU Index within MSDU link descriptor
			
			<legal 0-6>
*/
#define TX_MSDU_LINK_ENTRY_PTR_2_MSDU_INDEX_OFFSET                   0x00000008
#define TX_MSDU_LINK_ENTRY_PTR_2_MSDU_INDEX_LSB                      0
#define TX_MSDU_LINK_ENTRY_PTR_2_MSDU_INDEX_MASK                     0x0000000f

/* Description		TX_MSDU_LINK_ENTRY_PTR_2_RESERVED_2A
			
			<legal 0>
*/
#define TX_MSDU_LINK_ENTRY_PTR_2_RESERVED_2A_OFFSET                  0x00000008
#define TX_MSDU_LINK_ENTRY_PTR_2_RESERVED_2A_LSB                     4
#define TX_MSDU_LINK_ENTRY_PTR_2_RESERVED_2A_MASK                    0xfffffff0


#endif // _TX_MSDU_LINK_ENTRY_PTR_H_
