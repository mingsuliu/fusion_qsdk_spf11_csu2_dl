// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RX_MPDU_LINK_H_
#define _RX_MPDU_LINK_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_descriptor_header.h"
#include "rx_mpdu_details.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct uniform_descriptor_header descriptor_header;
//	1	start_mpdu_sequence_number[11:0], reserved_1a[15:12], receive_mpdu_queue_number[31:16]
//	2	reserved_2a[31:0]
//	3	reserved_3a[31:0]
//	4-7	struct rx_mpdu_details mpdu_0;
//	8-11	struct rx_mpdu_details mpdu_1;
//	12-15	struct rx_mpdu_details mpdu_2;
//	16-19	struct rx_mpdu_details mpdu_3;
//	20-23	struct rx_mpdu_details mpdu_4;
//	24-27	struct rx_mpdu_details mpdu_5;
//	28-31	struct rx_mpdu_details mpdu_6;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RX_MPDU_LINK 32

struct rx_mpdu_link {
    struct            uniform_descriptor_header                       descriptor_header;
             uint32_t start_mpdu_sequence_number      : 12, //[11:0]
                      reserved_1a                     :  4, //[15:12]
                      receive_mpdu_queue_number       : 16; //[31:16]
             uint32_t reserved_2a                     : 32; //[31:0]
             uint32_t reserved_3a                     : 32; //[31:0]
    struct            rx_mpdu_details                       mpdu_0;
    struct            rx_mpdu_details                       mpdu_1;
    struct            rx_mpdu_details                       mpdu_2;
    struct            rx_mpdu_details                       mpdu_3;
    struct            rx_mpdu_details                       mpdu_4;
    struct            rx_mpdu_details                       mpdu_5;
    struct            rx_mpdu_details                       mpdu_6;
};

/*

struct uniform_descriptor_header descriptor_header
			
			Details about which module owns this struct.
			
			Note that sub field Buffer_type shall be set to
			Receive_MPDU_Link_descriptor

start_mpdu_sequence_number
			
			The sequence number of MPDU_0.
			
			If MPDU_0 field is not yet filled in, this number will
			be the expected sequence number of this frame.
			
			Used for tracking and debugging
			
			<legal all>

reserved_1a
			
			<legal 0>

receive_mpdu_queue_number
			
			Indicates the receive MPDU queue ID to which this MPDU
			link descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>

reserved_2a
			
			<legal 0>

reserved_3a
			
			<legal 0>

struct rx_mpdu_details mpdu_0
			
			Details of first MPDU in this linked list

struct rx_mpdu_details mpdu_1
			
			Details of next MPDU in this linked list

struct rx_mpdu_details mpdu_2
			
			Details of next MPDU in this linked list

struct rx_mpdu_details mpdu_3
			
			Details of next MPDU in this linked list

struct rx_mpdu_details mpdu_4
			
			Details of next MPDU in this linked list

struct rx_mpdu_details mpdu_5
			
			Details of next MPDU in this linked list

struct rx_mpdu_details mpdu_6
			
			Details of next MPDU in this linked list
*/

#define RX_MPDU_LINK_0_UNIFORM_DESCRIPTOR_HEADER_DESCRIPTOR_HEADER_OFFSET 0x00000000
#define RX_MPDU_LINK_0_UNIFORM_DESCRIPTOR_HEADER_DESCRIPTOR_HEADER_LSB 0
#define RX_MPDU_LINK_0_UNIFORM_DESCRIPTOR_HEADER_DESCRIPTOR_HEADER_MASK 0xffffffff

/* Description		RX_MPDU_LINK_1_START_MPDU_SEQUENCE_NUMBER
			
			The sequence number of MPDU_0.
			
			If MPDU_0 field is not yet filled in, this number will
			be the expected sequence number of this frame.
			
			Used for tracking and debugging
			
			<legal all>
*/
#define RX_MPDU_LINK_1_START_MPDU_SEQUENCE_NUMBER_OFFSET             0x00000004
#define RX_MPDU_LINK_1_START_MPDU_SEQUENCE_NUMBER_LSB                0
#define RX_MPDU_LINK_1_START_MPDU_SEQUENCE_NUMBER_MASK               0x00000fff

/* Description		RX_MPDU_LINK_1_RESERVED_1A
			
			<legal 0>
*/
#define RX_MPDU_LINK_1_RESERVED_1A_OFFSET                            0x00000004
#define RX_MPDU_LINK_1_RESERVED_1A_LSB                               12
#define RX_MPDU_LINK_1_RESERVED_1A_MASK                              0x0000f000

/* Description		RX_MPDU_LINK_1_RECEIVE_MPDU_QUEUE_NUMBER
			
			Indicates the receive MPDU queue ID to which this MPDU
			link descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>
*/
#define RX_MPDU_LINK_1_RECEIVE_MPDU_QUEUE_NUMBER_OFFSET              0x00000004
#define RX_MPDU_LINK_1_RECEIVE_MPDU_QUEUE_NUMBER_LSB                 16
#define RX_MPDU_LINK_1_RECEIVE_MPDU_QUEUE_NUMBER_MASK                0xffff0000

/* Description		RX_MPDU_LINK_2_RESERVED_2A
			
			<legal 0>
*/
#define RX_MPDU_LINK_2_RESERVED_2A_OFFSET                            0x00000008
#define RX_MPDU_LINK_2_RESERVED_2A_LSB                               0
#define RX_MPDU_LINK_2_RESERVED_2A_MASK                              0xffffffff

/* Description		RX_MPDU_LINK_3_RESERVED_3A
			
			<legal 0>
*/
#define RX_MPDU_LINK_3_RESERVED_3A_OFFSET                            0x0000000c
#define RX_MPDU_LINK_3_RESERVED_3A_LSB                               0
#define RX_MPDU_LINK_3_RESERVED_3A_MASK                              0xffffffff
#define RX_MPDU_LINK_4_RX_MPDU_DETAILS_MPDU_0_OFFSET                 0x00000010
#define RX_MPDU_LINK_4_RX_MPDU_DETAILS_MPDU_0_LSB                    0
#define RX_MPDU_LINK_4_RX_MPDU_DETAILS_MPDU_0_MASK                   0xffffffff
#define RX_MPDU_LINK_5_RX_MPDU_DETAILS_MPDU_0_OFFSET                 0x00000014
#define RX_MPDU_LINK_5_RX_MPDU_DETAILS_MPDU_0_LSB                    0
#define RX_MPDU_LINK_5_RX_MPDU_DETAILS_MPDU_0_MASK                   0xffffffff
#define RX_MPDU_LINK_6_RX_MPDU_DETAILS_MPDU_0_OFFSET                 0x00000018
#define RX_MPDU_LINK_6_RX_MPDU_DETAILS_MPDU_0_LSB                    0
#define RX_MPDU_LINK_6_RX_MPDU_DETAILS_MPDU_0_MASK                   0xffffffff
#define RX_MPDU_LINK_7_RX_MPDU_DETAILS_MPDU_0_OFFSET                 0x0000001c
#define RX_MPDU_LINK_7_RX_MPDU_DETAILS_MPDU_0_LSB                    0
#define RX_MPDU_LINK_7_RX_MPDU_DETAILS_MPDU_0_MASK                   0xffffffff
#define RX_MPDU_LINK_8_RX_MPDU_DETAILS_MPDU_1_OFFSET                 0x00000020
#define RX_MPDU_LINK_8_RX_MPDU_DETAILS_MPDU_1_LSB                    0
#define RX_MPDU_LINK_8_RX_MPDU_DETAILS_MPDU_1_MASK                   0xffffffff
#define RX_MPDU_LINK_9_RX_MPDU_DETAILS_MPDU_1_OFFSET                 0x00000024
#define RX_MPDU_LINK_9_RX_MPDU_DETAILS_MPDU_1_LSB                    0
#define RX_MPDU_LINK_9_RX_MPDU_DETAILS_MPDU_1_MASK                   0xffffffff
#define RX_MPDU_LINK_10_RX_MPDU_DETAILS_MPDU_1_OFFSET                0x00000028
#define RX_MPDU_LINK_10_RX_MPDU_DETAILS_MPDU_1_LSB                   0
#define RX_MPDU_LINK_10_RX_MPDU_DETAILS_MPDU_1_MASK                  0xffffffff
#define RX_MPDU_LINK_11_RX_MPDU_DETAILS_MPDU_1_OFFSET                0x0000002c
#define RX_MPDU_LINK_11_RX_MPDU_DETAILS_MPDU_1_LSB                   0
#define RX_MPDU_LINK_11_RX_MPDU_DETAILS_MPDU_1_MASK                  0xffffffff
#define RX_MPDU_LINK_12_RX_MPDU_DETAILS_MPDU_2_OFFSET                0x00000030
#define RX_MPDU_LINK_12_RX_MPDU_DETAILS_MPDU_2_LSB                   0
#define RX_MPDU_LINK_12_RX_MPDU_DETAILS_MPDU_2_MASK                  0xffffffff
#define RX_MPDU_LINK_13_RX_MPDU_DETAILS_MPDU_2_OFFSET                0x00000034
#define RX_MPDU_LINK_13_RX_MPDU_DETAILS_MPDU_2_LSB                   0
#define RX_MPDU_LINK_13_RX_MPDU_DETAILS_MPDU_2_MASK                  0xffffffff
#define RX_MPDU_LINK_14_RX_MPDU_DETAILS_MPDU_2_OFFSET                0x00000038
#define RX_MPDU_LINK_14_RX_MPDU_DETAILS_MPDU_2_LSB                   0
#define RX_MPDU_LINK_14_RX_MPDU_DETAILS_MPDU_2_MASK                  0xffffffff
#define RX_MPDU_LINK_15_RX_MPDU_DETAILS_MPDU_2_OFFSET                0x0000003c
#define RX_MPDU_LINK_15_RX_MPDU_DETAILS_MPDU_2_LSB                   0
#define RX_MPDU_LINK_15_RX_MPDU_DETAILS_MPDU_2_MASK                  0xffffffff
#define RX_MPDU_LINK_16_RX_MPDU_DETAILS_MPDU_3_OFFSET                0x00000040
#define RX_MPDU_LINK_16_RX_MPDU_DETAILS_MPDU_3_LSB                   0
#define RX_MPDU_LINK_16_RX_MPDU_DETAILS_MPDU_3_MASK                  0xffffffff
#define RX_MPDU_LINK_17_RX_MPDU_DETAILS_MPDU_3_OFFSET                0x00000044
#define RX_MPDU_LINK_17_RX_MPDU_DETAILS_MPDU_3_LSB                   0
#define RX_MPDU_LINK_17_RX_MPDU_DETAILS_MPDU_3_MASK                  0xffffffff
#define RX_MPDU_LINK_18_RX_MPDU_DETAILS_MPDU_3_OFFSET                0x00000048
#define RX_MPDU_LINK_18_RX_MPDU_DETAILS_MPDU_3_LSB                   0
#define RX_MPDU_LINK_18_RX_MPDU_DETAILS_MPDU_3_MASK                  0xffffffff
#define RX_MPDU_LINK_19_RX_MPDU_DETAILS_MPDU_3_OFFSET                0x0000004c
#define RX_MPDU_LINK_19_RX_MPDU_DETAILS_MPDU_3_LSB                   0
#define RX_MPDU_LINK_19_RX_MPDU_DETAILS_MPDU_3_MASK                  0xffffffff
#define RX_MPDU_LINK_20_RX_MPDU_DETAILS_MPDU_4_OFFSET                0x00000050
#define RX_MPDU_LINK_20_RX_MPDU_DETAILS_MPDU_4_LSB                   0
#define RX_MPDU_LINK_20_RX_MPDU_DETAILS_MPDU_4_MASK                  0xffffffff
#define RX_MPDU_LINK_21_RX_MPDU_DETAILS_MPDU_4_OFFSET                0x00000054
#define RX_MPDU_LINK_21_RX_MPDU_DETAILS_MPDU_4_LSB                   0
#define RX_MPDU_LINK_21_RX_MPDU_DETAILS_MPDU_4_MASK                  0xffffffff
#define RX_MPDU_LINK_22_RX_MPDU_DETAILS_MPDU_4_OFFSET                0x00000058
#define RX_MPDU_LINK_22_RX_MPDU_DETAILS_MPDU_4_LSB                   0
#define RX_MPDU_LINK_22_RX_MPDU_DETAILS_MPDU_4_MASK                  0xffffffff
#define RX_MPDU_LINK_23_RX_MPDU_DETAILS_MPDU_4_OFFSET                0x0000005c
#define RX_MPDU_LINK_23_RX_MPDU_DETAILS_MPDU_4_LSB                   0
#define RX_MPDU_LINK_23_RX_MPDU_DETAILS_MPDU_4_MASK                  0xffffffff
#define RX_MPDU_LINK_24_RX_MPDU_DETAILS_MPDU_5_OFFSET                0x00000060
#define RX_MPDU_LINK_24_RX_MPDU_DETAILS_MPDU_5_LSB                   0
#define RX_MPDU_LINK_24_RX_MPDU_DETAILS_MPDU_5_MASK                  0xffffffff
#define RX_MPDU_LINK_25_RX_MPDU_DETAILS_MPDU_5_OFFSET                0x00000064
#define RX_MPDU_LINK_25_RX_MPDU_DETAILS_MPDU_5_LSB                   0
#define RX_MPDU_LINK_25_RX_MPDU_DETAILS_MPDU_5_MASK                  0xffffffff
#define RX_MPDU_LINK_26_RX_MPDU_DETAILS_MPDU_5_OFFSET                0x00000068
#define RX_MPDU_LINK_26_RX_MPDU_DETAILS_MPDU_5_LSB                   0
#define RX_MPDU_LINK_26_RX_MPDU_DETAILS_MPDU_5_MASK                  0xffffffff
#define RX_MPDU_LINK_27_RX_MPDU_DETAILS_MPDU_5_OFFSET                0x0000006c
#define RX_MPDU_LINK_27_RX_MPDU_DETAILS_MPDU_5_LSB                   0
#define RX_MPDU_LINK_27_RX_MPDU_DETAILS_MPDU_5_MASK                  0xffffffff
#define RX_MPDU_LINK_28_RX_MPDU_DETAILS_MPDU_6_OFFSET                0x00000070
#define RX_MPDU_LINK_28_RX_MPDU_DETAILS_MPDU_6_LSB                   0
#define RX_MPDU_LINK_28_RX_MPDU_DETAILS_MPDU_6_MASK                  0xffffffff
#define RX_MPDU_LINK_29_RX_MPDU_DETAILS_MPDU_6_OFFSET                0x00000074
#define RX_MPDU_LINK_29_RX_MPDU_DETAILS_MPDU_6_LSB                   0
#define RX_MPDU_LINK_29_RX_MPDU_DETAILS_MPDU_6_MASK                  0xffffffff
#define RX_MPDU_LINK_30_RX_MPDU_DETAILS_MPDU_6_OFFSET                0x00000078
#define RX_MPDU_LINK_30_RX_MPDU_DETAILS_MPDU_6_LSB                   0
#define RX_MPDU_LINK_30_RX_MPDU_DETAILS_MPDU_6_MASK                  0xffffffff
#define RX_MPDU_LINK_31_RX_MPDU_DETAILS_MPDU_6_OFFSET                0x0000007c
#define RX_MPDU_LINK_31_RX_MPDU_DETAILS_MPDU_6_LSB                   0
#define RX_MPDU_LINK_31_RX_MPDU_DETAILS_MPDU_6_MASK                  0xffffffff


#endif // _RX_MPDU_LINK_H_
