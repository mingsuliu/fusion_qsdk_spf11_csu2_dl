// Copyright (c) 2016 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _WHO_CLASSIFY_INFO_H_
#define _WHO_CLASSIFY_INFO_H_
#if !defined(__ASSEMBLER__)
#endif

#include "txpt_classify_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-2	struct txpt_classify_info tx_non_qos;
//	3-5	struct txpt_classify_info tx_tid0;
//	6-8	struct txpt_classify_info tx_tid1;
//	9-11	struct txpt_classify_info tx_tid2;
//	12-14	struct txpt_classify_info tx_tid3;
//	15-17	struct txpt_classify_info tx_tid4;
//	18-20	struct txpt_classify_info tx_tid5;
//	21-23	struct txpt_classify_info tx_tid6;
//	24-26	struct txpt_classify_info tx_tid7;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_WHO_CLASSIFY_INFO 27

struct who_classify_info {
    struct            txpt_classify_info                       tx_non_qos;
    struct            txpt_classify_info                       tx_tid0;
    struct            txpt_classify_info                       tx_tid1;
    struct            txpt_classify_info                       tx_tid2;
    struct            txpt_classify_info                       tx_tid3;
    struct            txpt_classify_info                       tx_tid4;
    struct            txpt_classify_info                       tx_tid5;
    struct            txpt_classify_info                       tx_tid6;
    struct            txpt_classify_info                       tx_tid7;
};

/*

struct txpt_classify_info tx_non_qos
			
			Tx classification info. for non-QoS-data raw WiFi SDUs

struct txpt_classify_info tx_tid0
			
			Tx classification info. for SDUs with TID 0

struct txpt_classify_info tx_tid1
			
			Tx classification info. for SDUs with TID 1

struct txpt_classify_info tx_tid2
			
			Tx classification info. for SDUs with TID 2

struct txpt_classify_info tx_tid3
			
			Tx classification info. for SDUs with TID 3

struct txpt_classify_info tx_tid4
			
			Tx classification info. for SDUs with TID 4

struct txpt_classify_info tx_tid5
			
			Tx classification info. for SDUs with TID 5

struct txpt_classify_info tx_tid6
			
			Tx classification info. for SDUs with TID 6

struct txpt_classify_info tx_tid7
			
			Tx classification info. for SDUs with TID 7
*/

#define WHO_CLASSIFY_INFO_0_TXPT_CLASSIFY_INFO_TX_NON_QOS_OFFSET     0x00000000
#define WHO_CLASSIFY_INFO_0_TXPT_CLASSIFY_INFO_TX_NON_QOS_LSB        0
#define WHO_CLASSIFY_INFO_0_TXPT_CLASSIFY_INFO_TX_NON_QOS_MASK       0xffffffff
#define WHO_CLASSIFY_INFO_1_TXPT_CLASSIFY_INFO_TX_NON_QOS_OFFSET     0x00000004
#define WHO_CLASSIFY_INFO_1_TXPT_CLASSIFY_INFO_TX_NON_QOS_LSB        0
#define WHO_CLASSIFY_INFO_1_TXPT_CLASSIFY_INFO_TX_NON_QOS_MASK       0xffffffff
#define WHO_CLASSIFY_INFO_2_TXPT_CLASSIFY_INFO_TX_NON_QOS_OFFSET     0x00000008
#define WHO_CLASSIFY_INFO_2_TXPT_CLASSIFY_INFO_TX_NON_QOS_LSB        0
#define WHO_CLASSIFY_INFO_2_TXPT_CLASSIFY_INFO_TX_NON_QOS_MASK       0xffffffff
#define WHO_CLASSIFY_INFO_3_TXPT_CLASSIFY_INFO_TX_TID0_OFFSET        0x0000000c
#define WHO_CLASSIFY_INFO_3_TXPT_CLASSIFY_INFO_TX_TID0_LSB           0
#define WHO_CLASSIFY_INFO_3_TXPT_CLASSIFY_INFO_TX_TID0_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_4_TXPT_CLASSIFY_INFO_TX_TID0_OFFSET        0x00000010
#define WHO_CLASSIFY_INFO_4_TXPT_CLASSIFY_INFO_TX_TID0_LSB           0
#define WHO_CLASSIFY_INFO_4_TXPT_CLASSIFY_INFO_TX_TID0_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_5_TXPT_CLASSIFY_INFO_TX_TID0_OFFSET        0x00000014
#define WHO_CLASSIFY_INFO_5_TXPT_CLASSIFY_INFO_TX_TID0_LSB           0
#define WHO_CLASSIFY_INFO_5_TXPT_CLASSIFY_INFO_TX_TID0_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_6_TXPT_CLASSIFY_INFO_TX_TID1_OFFSET        0x00000018
#define WHO_CLASSIFY_INFO_6_TXPT_CLASSIFY_INFO_TX_TID1_LSB           0
#define WHO_CLASSIFY_INFO_6_TXPT_CLASSIFY_INFO_TX_TID1_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_7_TXPT_CLASSIFY_INFO_TX_TID1_OFFSET        0x0000001c
#define WHO_CLASSIFY_INFO_7_TXPT_CLASSIFY_INFO_TX_TID1_LSB           0
#define WHO_CLASSIFY_INFO_7_TXPT_CLASSIFY_INFO_TX_TID1_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_8_TXPT_CLASSIFY_INFO_TX_TID1_OFFSET        0x00000020
#define WHO_CLASSIFY_INFO_8_TXPT_CLASSIFY_INFO_TX_TID1_LSB           0
#define WHO_CLASSIFY_INFO_8_TXPT_CLASSIFY_INFO_TX_TID1_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_9_TXPT_CLASSIFY_INFO_TX_TID2_OFFSET        0x00000024
#define WHO_CLASSIFY_INFO_9_TXPT_CLASSIFY_INFO_TX_TID2_LSB           0
#define WHO_CLASSIFY_INFO_9_TXPT_CLASSIFY_INFO_TX_TID2_MASK          0xffffffff
#define WHO_CLASSIFY_INFO_10_TXPT_CLASSIFY_INFO_TX_TID2_OFFSET       0x00000028
#define WHO_CLASSIFY_INFO_10_TXPT_CLASSIFY_INFO_TX_TID2_LSB          0
#define WHO_CLASSIFY_INFO_10_TXPT_CLASSIFY_INFO_TX_TID2_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_11_TXPT_CLASSIFY_INFO_TX_TID2_OFFSET       0x0000002c
#define WHO_CLASSIFY_INFO_11_TXPT_CLASSIFY_INFO_TX_TID2_LSB          0
#define WHO_CLASSIFY_INFO_11_TXPT_CLASSIFY_INFO_TX_TID2_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_12_TXPT_CLASSIFY_INFO_TX_TID3_OFFSET       0x00000030
#define WHO_CLASSIFY_INFO_12_TXPT_CLASSIFY_INFO_TX_TID3_LSB          0
#define WHO_CLASSIFY_INFO_12_TXPT_CLASSIFY_INFO_TX_TID3_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_13_TXPT_CLASSIFY_INFO_TX_TID3_OFFSET       0x00000034
#define WHO_CLASSIFY_INFO_13_TXPT_CLASSIFY_INFO_TX_TID3_LSB          0
#define WHO_CLASSIFY_INFO_13_TXPT_CLASSIFY_INFO_TX_TID3_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_14_TXPT_CLASSIFY_INFO_TX_TID3_OFFSET       0x00000038
#define WHO_CLASSIFY_INFO_14_TXPT_CLASSIFY_INFO_TX_TID3_LSB          0
#define WHO_CLASSIFY_INFO_14_TXPT_CLASSIFY_INFO_TX_TID3_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_15_TXPT_CLASSIFY_INFO_TX_TID4_OFFSET       0x0000003c
#define WHO_CLASSIFY_INFO_15_TXPT_CLASSIFY_INFO_TX_TID4_LSB          0
#define WHO_CLASSIFY_INFO_15_TXPT_CLASSIFY_INFO_TX_TID4_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_16_TXPT_CLASSIFY_INFO_TX_TID4_OFFSET       0x00000040
#define WHO_CLASSIFY_INFO_16_TXPT_CLASSIFY_INFO_TX_TID4_LSB          0
#define WHO_CLASSIFY_INFO_16_TXPT_CLASSIFY_INFO_TX_TID4_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_17_TXPT_CLASSIFY_INFO_TX_TID4_OFFSET       0x00000044
#define WHO_CLASSIFY_INFO_17_TXPT_CLASSIFY_INFO_TX_TID4_LSB          0
#define WHO_CLASSIFY_INFO_17_TXPT_CLASSIFY_INFO_TX_TID4_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_18_TXPT_CLASSIFY_INFO_TX_TID5_OFFSET       0x00000048
#define WHO_CLASSIFY_INFO_18_TXPT_CLASSIFY_INFO_TX_TID5_LSB          0
#define WHO_CLASSIFY_INFO_18_TXPT_CLASSIFY_INFO_TX_TID5_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_19_TXPT_CLASSIFY_INFO_TX_TID5_OFFSET       0x0000004c
#define WHO_CLASSIFY_INFO_19_TXPT_CLASSIFY_INFO_TX_TID5_LSB          0
#define WHO_CLASSIFY_INFO_19_TXPT_CLASSIFY_INFO_TX_TID5_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_20_TXPT_CLASSIFY_INFO_TX_TID5_OFFSET       0x00000050
#define WHO_CLASSIFY_INFO_20_TXPT_CLASSIFY_INFO_TX_TID5_LSB          0
#define WHO_CLASSIFY_INFO_20_TXPT_CLASSIFY_INFO_TX_TID5_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_21_TXPT_CLASSIFY_INFO_TX_TID6_OFFSET       0x00000054
#define WHO_CLASSIFY_INFO_21_TXPT_CLASSIFY_INFO_TX_TID6_LSB          0
#define WHO_CLASSIFY_INFO_21_TXPT_CLASSIFY_INFO_TX_TID6_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_22_TXPT_CLASSIFY_INFO_TX_TID6_OFFSET       0x00000058
#define WHO_CLASSIFY_INFO_22_TXPT_CLASSIFY_INFO_TX_TID6_LSB          0
#define WHO_CLASSIFY_INFO_22_TXPT_CLASSIFY_INFO_TX_TID6_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_23_TXPT_CLASSIFY_INFO_TX_TID6_OFFSET       0x0000005c
#define WHO_CLASSIFY_INFO_23_TXPT_CLASSIFY_INFO_TX_TID6_LSB          0
#define WHO_CLASSIFY_INFO_23_TXPT_CLASSIFY_INFO_TX_TID6_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_24_TXPT_CLASSIFY_INFO_TX_TID7_OFFSET       0x00000060
#define WHO_CLASSIFY_INFO_24_TXPT_CLASSIFY_INFO_TX_TID7_LSB          0
#define WHO_CLASSIFY_INFO_24_TXPT_CLASSIFY_INFO_TX_TID7_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_25_TXPT_CLASSIFY_INFO_TX_TID7_OFFSET       0x00000064
#define WHO_CLASSIFY_INFO_25_TXPT_CLASSIFY_INFO_TX_TID7_LSB          0
#define WHO_CLASSIFY_INFO_25_TXPT_CLASSIFY_INFO_TX_TID7_MASK         0xffffffff
#define WHO_CLASSIFY_INFO_26_TXPT_CLASSIFY_INFO_TX_TID7_OFFSET       0x00000068
#define WHO_CLASSIFY_INFO_26_TXPT_CLASSIFY_INFO_TX_TID7_LSB          0
#define WHO_CLASSIFY_INFO_26_TXPT_CLASSIFY_INFO_TX_TID7_MASK         0xffffffff


#endif // _WHO_CLASSIFY_INFO_H_
