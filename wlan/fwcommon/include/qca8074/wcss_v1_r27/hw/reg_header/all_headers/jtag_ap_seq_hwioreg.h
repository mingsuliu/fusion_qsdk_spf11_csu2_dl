///////////////////////////////////////////////////////////////////////////////////////////////
//
// QUALCOMM Proprietary Design Data
// Copyright (c) 2011, Qualcomm Technologies Incorporated. All rights reserved.
//
// All data and information contained in or disclosed by this document are confidential and
// proprietary information of Qualcomm Technologies Incorporated, and all rights therein are expressly
// reserved. By accepting this material, the recipient agrees that this material and the
// information contained therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in any manner to others
// without the express written permission of Qualcomm Technologies Incorporated.
//
// This technology was exported from the United States in accordance with the Export
// Administration Regulations. Diversion contrary to U. S. law prohibited.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// jtag_ap_seq_hwioreg.h : automatically generated by Autoseq  3.6 3/21/2017 
// User Name:pgohil
//
// !! WARNING !!  DO NOT MANUALLY EDIT THIS FILE.
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __JTAG_AP_SEQ_REG_H__
#define __JTAG_AP_SEQ_REG_H__

#include "seq_hwio.h"
#include "jtag_ap_seq_hwiobase.h"
#ifdef SCALE_INCLUDES
	#include "HALhwio.h"
#else
	#include "msmhwio.h"
#endif


///////////////////////////////////////////////////////////////////////////////////////////////
// Register Data for Block JTAG_AP
///////////////////////////////////////////////////////////////////////////////////////////////

//// Register CSW ////

#define HWIO_JTAG_AP_CSW_ADDR(x)                                     (x+0x00000000)
#define HWIO_JTAG_AP_CSW_PHYS(x)                                     (x+0x00000000)
#define HWIO_JTAG_AP_CSW_RMSK                                        0xf700000f
#define HWIO_JTAG_AP_CSW_SHFT                                                 0
#define HWIO_JTAG_AP_CSW_IN(x)                                       \
	in_dword_masked ( HWIO_JTAG_AP_CSW_ADDR(x), HWIO_JTAG_AP_CSW_RMSK)
#define HWIO_JTAG_AP_CSW_INM(x, mask)                                \
	in_dword_masked ( HWIO_JTAG_AP_CSW_ADDR(x), mask) 
#define HWIO_JTAG_AP_CSW_OUT(x, val)                                 \
	out_dword( HWIO_JTAG_AP_CSW_ADDR(x), val)
#define HWIO_JTAG_AP_CSW_OUTM(x, mask, val)                          \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_CSW_ADDR(x), mask, val, HWIO_JTAG_AP_CSW_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_CSW_SERACTV_BMSK                                0x80000000
#define HWIO_JTAG_AP_CSW_SERACTV_SHFT                                      0x1f

#define HWIO_JTAG_AP_CSW_WFIFOCNT_BMSK                               0x70000000
#define HWIO_JTAG_AP_CSW_WFIFOCNT_SHFT                                     0x1c

#define HWIO_JTAG_AP_CSW_RFIFOCNT_BMSK                               0x07000000
#define HWIO_JTAG_AP_CSW_RFIFOCNT_SHFT                                     0x18

#define HWIO_JTAG_AP_CSW_PORTCNCTD_BMSK                              0x00000008
#define HWIO_JTAG_AP_CSW_PORTCNCTD_SHFT                                     0x3

#define HWIO_JTAG_AP_CSW_SRSTCNCTD_BMSK                              0x00000004
#define HWIO_JTAG_AP_CSW_SRSTCNCTD_SHFT                                     0x2

#define HWIO_JTAG_AP_CSW_TRST_OUT_BMSK                               0x00000002
#define HWIO_JTAG_AP_CSW_TRST_OUT_SHFT                                      0x1

#define HWIO_JTAG_AP_CSW_SRST_OUT_BMSK                               0x00000001
#define HWIO_JTAG_AP_CSW_SRST_OUT_SHFT                                      0x0

//// Register PORTSEL ////

#define HWIO_JTAG_AP_PORTSEL_ADDR(x)                                 (x+0x00000004)
#define HWIO_JTAG_AP_PORTSEL_PHYS(x)                                 (x+0x00000004)
#define HWIO_JTAG_AP_PORTSEL_RMSK                                    0x000000ff
#define HWIO_JTAG_AP_PORTSEL_SHFT                                             0
#define HWIO_JTAG_AP_PORTSEL_IN(x)                                   \
	in_dword_masked ( HWIO_JTAG_AP_PORTSEL_ADDR(x), HWIO_JTAG_AP_PORTSEL_RMSK)
#define HWIO_JTAG_AP_PORTSEL_INM(x, mask)                            \
	in_dword_masked ( HWIO_JTAG_AP_PORTSEL_ADDR(x), mask) 
#define HWIO_JTAG_AP_PORTSEL_OUT(x, val)                             \
	out_dword( HWIO_JTAG_AP_PORTSEL_ADDR(x), val)
#define HWIO_JTAG_AP_PORTSEL_OUTM(x, mask, val)                      \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_PORTSEL_ADDR(x), mask, val, HWIO_JTAG_AP_PORTSEL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_PORTSEL_PORTSEL_BMSK                            0x000000ff
#define HWIO_JTAG_AP_PORTSEL_PORTSEL_SHFT                                   0x0

//// Register PSTA ////

#define HWIO_JTAG_AP_PSTA_ADDR(x)                                    (x+0x00000008)
#define HWIO_JTAG_AP_PSTA_PHYS(x)                                    (x+0x00000008)
#define HWIO_JTAG_AP_PSTA_RMSK                                       0x000000ff
#define HWIO_JTAG_AP_PSTA_SHFT                                                0
#define HWIO_JTAG_AP_PSTA_IN(x)                                      \
	in_dword_masked ( HWIO_JTAG_AP_PSTA_ADDR(x), HWIO_JTAG_AP_PSTA_RMSK)
#define HWIO_JTAG_AP_PSTA_INM(x, mask)                               \
	in_dword_masked ( HWIO_JTAG_AP_PSTA_ADDR(x), mask) 
#define HWIO_JTAG_AP_PSTA_OUT(x, val)                                \
	out_dword( HWIO_JTAG_AP_PSTA_ADDR(x), val)
#define HWIO_JTAG_AP_PSTA_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_PSTA_ADDR(x), mask, val, HWIO_JTAG_AP_PSTA_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_PSTA_PSTA_BMSK                                  0x000000ff
#define HWIO_JTAG_AP_PSTA_PSTA_SHFT                                         0x0

//// Register BFIFO0 ////

#define HWIO_JTAG_AP_BFIFO0_ADDR(x)                                  (x+0x00000010)
#define HWIO_JTAG_AP_BFIFO0_PHYS(x)                                  (x+0x00000010)
#define HWIO_JTAG_AP_BFIFO0_RMSK                                     0xffffffff
#define HWIO_JTAG_AP_BFIFO0_SHFT                                              0
#define HWIO_JTAG_AP_BFIFO0_IN(x)                                    \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO0_ADDR(x), HWIO_JTAG_AP_BFIFO0_RMSK)
#define HWIO_JTAG_AP_BFIFO0_INM(x, mask)                             \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO0_ADDR(x), mask) 
#define HWIO_JTAG_AP_BFIFO0_OUT(x, val)                              \
	out_dword( HWIO_JTAG_AP_BFIFO0_ADDR(x), val)
#define HWIO_JTAG_AP_BFIFO0_OUTM(x, mask, val)                       \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_BFIFO0_ADDR(x), mask, val, HWIO_JTAG_AP_BFIFO0_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_BFIFO0_DATA_BMSK                                0xffffffff
#define HWIO_JTAG_AP_BFIFO0_DATA_SHFT                                       0x0

//// Register BFIFO1 ////

#define HWIO_JTAG_AP_BFIFO1_ADDR(x)                                  (x+0x00000014)
#define HWIO_JTAG_AP_BFIFO1_PHYS(x)                                  (x+0x00000014)
#define HWIO_JTAG_AP_BFIFO1_RMSK                                     0xffffffff
#define HWIO_JTAG_AP_BFIFO1_SHFT                                              0
#define HWIO_JTAG_AP_BFIFO1_IN(x)                                    \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO1_ADDR(x), HWIO_JTAG_AP_BFIFO1_RMSK)
#define HWIO_JTAG_AP_BFIFO1_INM(x, mask)                             \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO1_ADDR(x), mask) 
#define HWIO_JTAG_AP_BFIFO1_OUT(x, val)                              \
	out_dword( HWIO_JTAG_AP_BFIFO1_ADDR(x), val)
#define HWIO_JTAG_AP_BFIFO1_OUTM(x, mask, val)                       \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_BFIFO1_ADDR(x), mask, val, HWIO_JTAG_AP_BFIFO1_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_BFIFO1_DATA_BMSK                                0xffffffff
#define HWIO_JTAG_AP_BFIFO1_DATA_SHFT                                       0x0

//// Register BFIFO2 ////

#define HWIO_JTAG_AP_BFIFO2_ADDR(x)                                  (x+0x00000018)
#define HWIO_JTAG_AP_BFIFO2_PHYS(x)                                  (x+0x00000018)
#define HWIO_JTAG_AP_BFIFO2_RMSK                                     0xffffffff
#define HWIO_JTAG_AP_BFIFO2_SHFT                                              0
#define HWIO_JTAG_AP_BFIFO2_IN(x)                                    \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO2_ADDR(x), HWIO_JTAG_AP_BFIFO2_RMSK)
#define HWIO_JTAG_AP_BFIFO2_INM(x, mask)                             \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO2_ADDR(x), mask) 
#define HWIO_JTAG_AP_BFIFO2_OUT(x, val)                              \
	out_dword( HWIO_JTAG_AP_BFIFO2_ADDR(x), val)
#define HWIO_JTAG_AP_BFIFO2_OUTM(x, mask, val)                       \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_BFIFO2_ADDR(x), mask, val, HWIO_JTAG_AP_BFIFO2_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_BFIFO2_DATA_BMSK                                0xffffffff
#define HWIO_JTAG_AP_BFIFO2_DATA_SHFT                                       0x0

//// Register BFIFO3 ////

#define HWIO_JTAG_AP_BFIFO3_ADDR(x)                                  (x+0x0000001c)
#define HWIO_JTAG_AP_BFIFO3_PHYS(x)                                  (x+0x0000001c)
#define HWIO_JTAG_AP_BFIFO3_RMSK                                     0xffffffff
#define HWIO_JTAG_AP_BFIFO3_SHFT                                              0
#define HWIO_JTAG_AP_BFIFO3_IN(x)                                    \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO3_ADDR(x), HWIO_JTAG_AP_BFIFO3_RMSK)
#define HWIO_JTAG_AP_BFIFO3_INM(x, mask)                             \
	in_dword_masked ( HWIO_JTAG_AP_BFIFO3_ADDR(x), mask) 
#define HWIO_JTAG_AP_BFIFO3_OUT(x, val)                              \
	out_dword( HWIO_JTAG_AP_BFIFO3_ADDR(x), val)
#define HWIO_JTAG_AP_BFIFO3_OUTM(x, mask, val)                       \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_BFIFO3_ADDR(x), mask, val, HWIO_JTAG_AP_BFIFO3_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_BFIFO3_DATA_BMSK                                0xffffffff
#define HWIO_JTAG_AP_BFIFO3_DATA_SHFT                                       0x0

//// Register IDR ////

#define HWIO_JTAG_AP_IDR_ADDR(x)                                     (x+0x000000fc)
#define HWIO_JTAG_AP_IDR_PHYS(x)                                     (x+0x000000fc)
#define HWIO_JTAG_AP_IDR_RMSK                                        0xffff00ff
#define HWIO_JTAG_AP_IDR_SHFT                                                 0
#define HWIO_JTAG_AP_IDR_IN(x)                                       \
	in_dword_masked ( HWIO_JTAG_AP_IDR_ADDR(x), HWIO_JTAG_AP_IDR_RMSK)
#define HWIO_JTAG_AP_IDR_INM(x, mask)                                \
	in_dword_masked ( HWIO_JTAG_AP_IDR_ADDR(x), mask) 
#define HWIO_JTAG_AP_IDR_OUT(x, val)                                 \
	out_dword( HWIO_JTAG_AP_IDR_ADDR(x), val)
#define HWIO_JTAG_AP_IDR_OUTM(x, mask, val)                          \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_JTAG_AP_IDR_ADDR(x), mask, val, HWIO_JTAG_AP_IDR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_JTAG_AP_IDR_REVISION_BMSK                               0xf0000000
#define HWIO_JTAG_AP_IDR_REVISION_SHFT                                     0x1c

#define HWIO_JTAG_AP_IDR_JEDECBANK_BMSK                              0x0f000000
#define HWIO_JTAG_AP_IDR_JEDECBANK_SHFT                                    0x18

#define HWIO_JTAG_AP_IDR_JEDECCODE_BMSK                              0x00fe0000
#define HWIO_JTAG_AP_IDR_JEDECCODE_SHFT                                    0x11

#define HWIO_JTAG_AP_IDR_MEMAP_BMSK                                  0x00010000
#define HWIO_JTAG_AP_IDR_MEMAP_SHFT                                        0x10

#define HWIO_JTAG_AP_IDR_IDENVAL_BMSK                                0x000000ff
#define HWIO_JTAG_AP_IDR_IDENVAL_SHFT                                       0x0


#endif

