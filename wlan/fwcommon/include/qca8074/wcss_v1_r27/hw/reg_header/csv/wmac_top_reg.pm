
// ---------------------------
%addr_decode = {
       "0x00000000" => "MAC_PDG_REG", 
       "0x00003000" => "MAC_TXDMA_REG", 
       "0x00006000" => "MAC_RXDMA_REG", 
       "0x00009000" => "MAC_MCMN_REG", 
       "0x0000c000" => "MAC_RXPCU_REG", 
       "0x0000f000" => "MAC_TXPCU_REG", 
       "0x00012000" => "MAC_AMPI_REG", 
       "0x00015000" => "MAC_RXOLE_REG", 
       "0x00018000" => "MAC_RXOLE_PARSER_REG", 
       "0x0001b000" => "MAC_CCE_REG", 
       "0x0001e000" => "MAC_TXOLE_REG", 
       "0x00021000" => "MAC_TXOLE_PARSER_REG", 
       "0x00024000" => "MAC_RRI_REG", 
       "0x00027000" => "MAC_CRYPTO_REG", 
       "0x0002a000" => "MAC_HWSCH_REG", 
       "0x00030000" => "MAC_MXI_REG", 
       "0x00033000" => "MAC_SFM_REG", 
       "0x00036000" => "MAC_RXDMA1_REG", 
       "0x00039000" => "MAC_LPEC_REG", 
       "0x0003efff" => "ADDRESS_END"
};
