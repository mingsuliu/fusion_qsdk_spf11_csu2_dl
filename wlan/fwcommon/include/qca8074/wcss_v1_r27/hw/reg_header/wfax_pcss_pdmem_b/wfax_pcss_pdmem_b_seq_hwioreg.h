///////////////////////////////////////////////////////////////////////////////////////////////
//
// QUALCOMM Proprietary Design Data
// Copyright (c) 2011, Qualcomm Technologies Incorporated. All rights reserved.
//
// All data and information contained in or disclosed by this document are confidential and
// proprietary information of Qualcomm Technologies Incorporated, and all rights therein are expressly
// reserved. By accepting this material, the recipient agrees that this material and the
// information contained therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in any manner to others
// without the express written permission of Qualcomm Technologies Incorporated.
//
// This technology was exported from the United States in accordance with the Export
// Administration Regulations. Diversion contrary to U. S. law prohibited.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// wfax_pcss_pdmem_b_seq_hwioreg.h : automatically generated by Autoseq  3.6 3/21/2017 
// User Name:pgohil
//
// !! WARNING !!  DO NOT MANUALLY EDIT THIS FILE.
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WFAX_PCSS_PDMEM_B_SEQ_REG_H__
#define __WFAX_PCSS_PDMEM_B_SEQ_REG_H__

#include "seq_hwio.h"
#include "wfax_pcss_pdmem_b_seq_hwiobase.h"
#ifdef SCALE_INCLUDES
	#include "HALhwio.h"
#else
	#include "msmhwio.h"
#endif


///////////////////////////////////////////////////////////////////////////////////////////////
// Register Data for Block WFAX_PCSS_PDMEM_B
///////////////////////////////////////////////////////////////////////////////////////////////

//// Register PDMEM_L_n ////

#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_ADDR(base, n)               (base+0x8*n)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_PHYS(base, n)               (base+0x8*n)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_RMSK                        0xffffffff
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_SHFT                                 0
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_MAXn                                 0
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_INI(base, n)                \
	in_dword_masked ( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_ADDR(base, n), HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_RMSK)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_INMI(base, n, mask)         \
	in_dword_masked ( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_ADDR(base, n), mask) 
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_OUTI(base, n, val)          \
	out_dword( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_ADDR(base, n), val)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_OUTMI(base, n, mask, val)   \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_ADDR(base, n), mask, val, HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_INI(base, n)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_PDMEM_LOW_BMSK              0xffffffff
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_L_n_PDMEM_LOW_SHFT                     0x0

//// Register PDMEM_U_n ////

#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_ADDR(base, n)               (base+0x4+0x8*n)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_PHYS(base, n)               (base+0x4+0x8*n)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_RMSK                        0xffffffff
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_SHFT                                 0
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_MAXn                                 0
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_INI(base, n)                \
	in_dword_masked ( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_ADDR(base, n), HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_RMSK)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_INMI(base, n, mask)         \
	in_dword_masked ( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_ADDR(base, n), mask) 
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_OUTI(base, n, val)          \
	out_dword( HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_ADDR(base, n), val)
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_OUTMI(base, n, mask, val)   \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_ADDR(base, n), mask, val, HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_INI(base, n)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_PDMEM_HIGH_BMSK             0xffffffff
#define HWIO_WFAX_PCSS_PDMEM_B_PDMEM_U_n_PDMEM_HIGH_SHFT                    0x0


#endif

