#!/bin/sh

import os
from subprocess import Popen, PIPE


cur_version = os.getcwd()
del_files = open("deleted_files.txt", "a")
print "********* Deleting files in PWD:"+cur_version
file_ext = ['*.svh', '*.sv', '*.log', '*.flat', '*.json', "*.csv", "mex_*", "*.log*"]
print "********* Deleting following files:"
print file_ext


for ext in file_ext:
    print ext
    cmd_string = "find . -name "+ext+" -type f"
    out = os.popen(cmd_string)
    out = out.read()
    del_files.write(out)
    cmd_string = "find . -name "+ext+" -type f -delete"
    out = os.popen(cmd_string)

print "********* Check deleted_files.txt for junk files deleted"

