
//-----------------------------------------------------------------------------
// Qualcomm Proprietary
// Copyright (c) Qualcomm Technologies Inc.
// All rights reserved.
//
//
// All data and information contained in or disclosed by this document
// are confidential and proprietary information of Qualcomm Technologies Incorporated,
// and all rights therein are expressly reserved. By accepting this
// material, the recipient agrees that this material and the information
// contained therein are held in confidence and in trust and will not be
// used, copied, reproduced in whole or in part, nor its contents
// revealed in any manner to others without the express written
// permission of Qualcomm Technologies Incorporated.
//
// This technology was exported from the United States in accordance with
// the Export Administration Regulations. Diversion contrary to U.S. law
// prohibited.
//-----------------------------------------------------------------------------


/**
 * Generated file ... Do not hand edit ...
 */

#ifndef _TLV_ENUM_H_
#define _TLV_ENUM_H_

//-----------------------------------------------------------------------------
// "ack_report" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFICTS_FRAME_E                                    = 0,
  WIFIACK_FRAME_E                                    = 1,
  WIFIBA_FRAME_E                                     = 2,
  WIFIQBOOST_TRIGGER_E                               = 3,
  WIFIPSPOLL_TRIGGER_E                               = 4,
  WIFIUAPSD_TRIGGER_E                                = 5,
  WIFICBF_FRAME_E                                    = 6,
  WIFIAX_SU_TRIGGER_E                                = 7,
  WIFIAX_WILDCARD_TRIGGER_E                          = 8,
  WIFIAX_UNASSOC_WILDCARD_TRIGGER_E                  = 9,
  WIFIMU_UL_RESPONSE_TO_RESPONSE_E                   = 10
} ack_report__selfgen_response_reason__e;            ///< selfgen_response_reason Enum Type

//-----------------------------------------------------------------------------
// "addr_search_entry" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIDL_UL_FLAG_IS_DL_OR_TDLS_E                     = 0,
  WIFIDL_UL_FLAG_IS_UL_E                             = 1
} addr_search_entry__dot11ax_dl_ul_flag__e;          ///< dot11ax_dl_ul_flag Enum Type
typedef enum {
  WIFIWEP_40_E                                       = 0,
  WIFIWEP_104_E                                      = 1,
  WIFITKIP_NO_MIC_E                                  = 2,
  WIFIWEP_128_E                                      = 3,
  WIFITKIP_WITH_MIC_E                                = 4,
  WIFIWAPI_E                                         = 5,
  WIFIAES_CCMP_128_E                                 = 6,
  WIFINO_CIPHER_E                                    = 7,
  WIFIAES_CCMP_256_E                                 = 8,
  WIFIAES_GCMP_128_E                                 = 9,
  WIFIAES_GCMP_256_E                                 = 10,
  WIFIWAPI_GCM_SM4_E                                 = 11,
  WIFIWEP_VARIED_WIDTH_E                             = 12
} addr_search_entry__key_type__e;                    ///< key_type Enum Type
typedef enum {
  WIFI1_SPATIAL_STREAM_E                             = 0,
  WIFI2_SPATIAL_STREAMS_E                            = 1,
  WIFI3_SPATIAL_STREAMS_E                            = 2,
  WIFI4_SPATIAL_STREAMS_E                            = 3,
  WIFI5_SPATIAL_STREAMS_E                            = 4,
  WIFI6_SPATIAL_STREAMS_E                            = 5,
  WIFI7_SPATIAL_STREAMS_E                            = 6,
  WIFI8_SPATIAL_STREAMS_E                            = 7
} addr_search_entry__ftm_pe_nss__e;                  ///< ftm_pe_nss Enum Type
typedef enum {
  WIFICHANNEL_CAPTURE_TYPE_IS_CV_E                   = 0,
  WIFICHANNEL_CAPTURE_TYPE_IS_H_E                    = 1
} addr_search_entry__capture_channel_type__e;        ///< capture_channel_type Enum Type

//-----------------------------------------------------------------------------
// "buffer_addr_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIWBM_IDLE_BUF_LIST_E                            = 0,
  WIFIWBM_IDLE_DESC_LIST_E                           = 1,
  WIFIFW_BM_E                                        = 2,
  WIFISW0_BM_E                                       = 3,
  WIFISW1_BM_E                                       = 4,
  WIFISW2_BM_E                                       = 5,
  WIFISW3_BM_E                                       = 6,
  WIFISW4_BM_E                                       = 7
} buffer_addr_info__return_buffer_manager__e;        ///< return_buffer_manager Enum Type

//-----------------------------------------------------------------------------
// "cce_rule" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFICCE_EXACT_4B_MATCH_E                           = 0,
  WIFICCE_MASKED_2B_BE_COMPARE_E                     = 1,
  WIFICCE_MASKED_2B_MATCH_E                          = 2,
  WIFICCE_MASKED_3B_MATCH_E                          = 3
} cce_rule__match_mode__e;                           ///< match_mode Enum Type
typedef enum {
  WIFIWHO_ANCHOR_NOP_E                               = 0,
  WIFIWHO_ANCHOR_MPDU_E                              = 1,
  WIFIWHO_ANCHOR_NON_AMSDU_MPDU_E                    = 2,
  WIFIWHO_ANCHOR_AMSDU_MPDU_E                        = 3,
  WIFIWHO_ANCHOR_MSDU_E                              = 4,
  WIFIWHO_ANCHOR_LLC_E                               = 5,
  WIFIWHO_ANCHOR_L2_TYPE_E                           = 6,
  WIFIWHO_ANCHOR_DA_E                                = 7,
  WIFIWHO_ANCHOR_SA_E                                = 8,
  WIFIWHO_ANCHOR_S_VLAN_E                            = 9,
  WIFIWHO_ANCHOR_C_VLAN_E                            = 10,
  WIFIWHO_ANCHOR_L3_E                                = 11,
  WIFIWHO_ANCHOR_IPV4_E                              = 12,
  WIFIWHO_ANCHOR_IPV6_E                              = 13,
  WIFIWHO_ANCHOR_L4_E                                = 14,
  WIFIWHO_ANCHOR_TCP_E                               = 15,
  WIFIWHO_ANCHOR_UDP_E                               = 16,
  WIFIWHO_ANCHOR_AH_E                                = 17,
  WIFIWHO_ANCHOR_ESP_E                               = 18,
  WIFIWHO_ANCHOR_IPV6_EXT_HDR_E                      = 19,
  WIFIWHO_ANCHOR_TCP_OPTS_E                          = 20,
  WIFIWHO_ANCHOR_IP_PROTO_E                          = 21,
  WIFIWHO_ANCHOR_MESH_CTRL_E                         = 22
} cce_rule__anchor_type__e;                          ///< anchor_type Enum Type

//-----------------------------------------------------------------------------
// "he_sig_a_mu_dl_info" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__dot11ax_dl_ul_flag__e     he_sig_a_mu_dl_info__dl_ul_flag__e;
typedef enum {
  WIFIHE_SIG_A_MU_DL_BW20_E                          = 0,
  WIFIHE_SIG_A_MU_DL_BW40_E                          = 1,
  WIFIHE_SIG_A_MU_DL_BW80_E                          = 2,
  WIFIHE_SIG_A_MU_DL_BW160_E                         = 3,
  WIFIHE_SIG_A_MU_DL_BW80_SEC_20_PUNC_E              = 4,
  WIFIHE_SIG_A_MU_DL_BW80_20_PUNC_IN_SEC_40_E        = 5,
  WIFIHE_SIG_A_MU_DL_BW160_SEC_20_PUNC_E             = 6,
  WIFIHE_SIG_A_MU_DL_BW160_SEC_40_80_PUNC_E          = 7
} he_sig_a_mu_dl_info__transmit_bw__e;               ///< transmit_bw Enum Type
typedef enum {
  WIFIMU_FOURX_LTF_0_8CP_E                           = 0,
  WIFIMU_TWOX_LTF_0_8CP_E                            = 1,
  WIFIMU_TWOX_LTF_1_6CP_E                            = 2,
  WIFIMU_FOURX_LTF_3_2CP_E                           = 3
} he_sig_a_mu_dl_info__cp_ltf_size__e;               ///< cp_ltf_size Enum Type
typedef enum {
  WIFIA_FACTOR_4_E                                   = 0,
  WIFIA_FACTOR_1_E                                   = 1,
  WIFIA_FACTOR_2_E                                   = 2,
  WIFIA_FACTOR_3_E                                   = 3
} he_sig_a_mu_dl_info__packet_extension_a_factor__e; ///< packet_extension_a_factor Enum Type

//-----------------------------------------------------------------------------
// "he_sig_a_mu_ul_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIHE_SIGA_FORMAT_HE_TRIG_E                       = 0,
  WIFIHE_SIGA_FORMAT_SU_OR_EXT_SU_E                  = 1
} he_sig_a_mu_ul_info__format_indication__e;         ///< format_indication Enum Type
typedef enum {
  WIFIHE_SIG_A_MU_UL_BW20_E                          = 0,
  WIFIHE_SIG_A_MU_UL_BW40_E                          = 1,
  WIFIHE_SIG_A_MU_UL_BW80_E                          = 2,
  WIFIHE_SIG_A_MU_UL_BW160_E                         = 3
} he_sig_a_mu_ul_info__transmit_bw__e;               ///< transmit_bw Enum Type

//-----------------------------------------------------------------------------
// "he_sig_a_su_info" enums
//-----------------------------------------------------------------------------
typedef he_sig_a_mu_ul_info__format_indication__e    he_sig_a_su_info__format_indication__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     he_sig_a_su_info__dl_ul_flag__e;
typedef enum {
  WIFIHE_SIG_A_BW20_E                                = 0,
  WIFIHE_SIG_A_BW40_E                                = 1,
  WIFIHE_SIG_A_BW80_E                                = 2,
  WIFIHE_SIG_A_BW160_E                               = 3
} he_sig_a_su_info__transmit_bw__e;                  ///< transmit_bw Enum Type
typedef enum {
  WIFIONEX_LTF_0_8CP_E                               = 0,
  WIFITWOX_LTF_0_8CP_E                               = 1,
  WIFITWOX_LTF_1_6CP_E                               = 2,
  WIFIFOURX_LTF_0_8CP_3_2CP_E                        = 3
} he_sig_a_su_info__cp_ltf_size__e;                  ///< cp_ltf_size Enum Type
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e he_sig_a_su_info__packet_extension_a_factor__e;

//-----------------------------------------------------------------------------
// "he_sig_b2_mu_info" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             he_sig_b2_mu_info__nsts__e;

//-----------------------------------------------------------------------------
// "he_sig_b2_ofdma_info" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             he_sig_b2_ofdma_info__nsts__e;

//-----------------------------------------------------------------------------
// "ht_sig_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIHT_20_MHZ_E                                    = 0,
  WIFIHT_40_MHZ_E                                    = 1
} ht_sig_info__cbw__e;                               ///< cbw Enum Type
typedef enum {
  WIFINO_SMOOTHING_E                                 = 1
} ht_sig_info__smoothing__e;                         ///< smoothing Enum Type
typedef enum {
  WIFINO_SOUNDING_E                                  = 1
} ht_sig_info__not_sounding__e;                      ///< not_sounding Enum Type
typedef enum {
  WIFIMPDU_E                                         = 0,
  WIFIA_MPDU_E                                       = 1
} ht_sig_info__aggregation__e;                       ///< aggregation Enum Type
typedef enum {
  WIFINO_STBC_E                                      = 0,
  WIFI1_STR_STBC_E                                   = 1
} ht_sig_info__stbc__e;                              ///< stbc Enum Type
typedef enum {
  WIFIHT_BCC_E                                       = 0,
  WIFIHT_LDPC_E                                      = 1
} ht_sig_info__fec_coding__e;                        ///< fec_coding Enum Type
typedef enum {
  WIFIHT_NORMAL_GI_E                                 = 0,
  WIFIHT_SHORT_GI_E                                  = 1
} ht_sig_info__short_gi__e;                          ///< short_gi Enum Type
typedef enum {
  WIFI0_EXT_SP_STR_E                                 = 0
} ht_sig_info__num_ext_sp_str__e;                    ///< num_ext_sp_str Enum Type

//-----------------------------------------------------------------------------
// "l_sig_a_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIOFDM_48_MBPS_E                                 = 8,
  WIFIOFDM_24_MBPS_E                                 = 9,
  WIFIOFDM_12_MBPS_E                                 = 10,
  WIFIOFDM_6_MBPS_E                                  = 11,
  WIFIOFDM_54_MBPS_E                                 = 12,
  WIFIOFDM_36_MBPS_E                                 = 13,
  WIFIOFDM_18_MBPS_E                                 = 14,
  WIFIOFDM_9_MBPS_E                                  = 15
} l_sig_a_info__rate__e;                             ///< rate Enum Type
typedef enum {
  WIFIDOT11A_E                                       = 0,
  WIFIDOT11B_E                                       = 1,
  WIFIDOT11N_MM_E                                    = 2,
  WIFIDOT11AC_E                                      = 3,
  WIFIDOT11AX_E                                      = 4
} l_sig_a_info__pkt_type__e;                         ///< pkt_type Enum Type

//-----------------------------------------------------------------------------
// "l_sig_b_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIDSSS_1_MPBS_LONG_E                             = 1,
  WIFIDSSS_2_MBPS_LONG_E                             = 2,
  WIFICCK_5_5_MBPS_LONG_E                            = 3,
  WIFICCK_11_MBPS_LONG_E                             = 4,
  WIFIDSSS_2_MBPS_SHORT_E                            = 5,
  WIFICCK_5_5_MBPS_SHORT_E                           = 6,
  WIFICCK_11_MBPS_SHORT_E                            = 7
} l_sig_b_info__rate__e;                             ///< rate Enum Type

//-----------------------------------------------------------------------------
// "mactx_abort_request_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMACTX_ABORT_SW_INITIATED_E                     = 0,
  WIFIMACTX_TX_FLUSH_RECEIVED_E                      = 1,
  WIFIMACTX_TX_FLUSH_CBF_UNDERRUN_E                  = 2,
  WIFIMACTX_ABORT_SPARE3_E                           = 3,
  WIFIMACTX_ABORT_SPARE4_E                           = 4,
  WIFIMACTX_ABORT_OTHER_E                            = 5
} mactx_abort_request_info__mactx_abort_reason__e;   ///< mactx_abort_reason Enum Type

//-----------------------------------------------------------------------------
// "mimo_control_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIVHT_MIMO_CONTROL_E                             = 0,
  WIFIHE_MIMO_CONTROL_E                              = 1
} mimo_control_info__vht_or_he_setting__e;           ///< vht_or_he_setting Enum Type

//-----------------------------------------------------------------------------
// "no_ack_report" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_ACK_FCS_ERRORS_E                            = 0,
  WIFIUNICAST_NO_ACK_FRAME_RECEIVED_E                = 1,
  WIFINO_ACK_BROADCAST_E                             = 2,
  WIFINO_ACK_MULTICAST_E                             = 3,
  WIFINOT_DIRECTED_E                                 = 4,
  WIFIAST_NO_ACK_E                                   = 5,
  WIFIPHY_GID_MISMATCH_E                             = 6,
  WIFIPHY_AID_MISMATCH_E                             = 7,
  WIFINO_ACK_PHY_ERROR_E                             = 8,
  WIFIRTS_BW_NOT_AVAILABLE_E                         = 9,
  WIFINDPA_FRAME_E                                   = 10,
  WIFINDP_FRAME_E                                    = 11,
  WIFITRIGGER_NAV_BLOCKED_E                          = 12,
  WIFITRIGGER_NO_AID_E                               = 13,
  WIFINO_ACK_MAC_ABORT_REQ_E                         = 14,
  WIFINO_RESPONSE_OTHER_E                            = 15
} no_ack_report__no_ack_transmit_reason__e;          ///< no_ack_transmit_reason Enum Type

//-----------------------------------------------------------------------------
// "pcu_ppdu_setup_end_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_COEX_TX_TIME_LIMITATION_E                   = 0,
  WIFICOEX_BASED_TX_TIME_LIMITATION_E                = 1
} pcu_ppdu_setup_end_info__coex_limited_fes__e;      ///< coex_limited_fes Enum Type
typedef enum {
  WIFIUSER_REDUCTION_NONE_E                          = 0,
  WIFIUSER_REDUCTION_ALL_E                           = 1,
  WIFIUSER_REDUCTION_PARTIAL_E                       = 2
} pcu_ppdu_setup_end_info__md_user_reduction_type__e; ///< md_user_reduction_type Enum Type
typedef enum {
  WIFIRESPONSE_20BW_E                                = 0,
  WIFIRESPONSE_40BW_E                                = 1,
  WIFIRESPONSE_80BW_E                                = 2,
  WIFIRESPONSE_160BW_E                               = 3,
  WIFIRESPONSE_11AH_1MHZ_E                           = 4,
  WIFIRESPONSE_11AH_2MHZ_E                           = 5,
  WIFIRESPONSE_11AH_4MHZ_E                           = 6,
  WIFIRESPONSE_11AH_8MHZ_E                           = 7
} pcu_ppdu_setup_end_info__pdg_sifs_response_bw__e;  ///< pdg_sifs_response_bw Enum Type

//-----------------------------------------------------------------------------
// "pdg_response_rate_setting" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    pdg_response_rate_setting__pkt_type__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_response_rate_setting__alt_nss__e;
typedef enum {
  WIFI20_MHZ_E                                       = 0,
  WIFI40_MHZ_E                                       = 1,
  WIFI80_MHZ_E                                       = 2,
  WIFI160_MHZ_E                                      = 3
} pdg_response_rate_setting__alt_bw__e;              ///< alt_bw Enum Type
typedef addr_search_entry__ftm_pe_nss__e             pdg_response_rate_setting__nss__e;
typedef enum {
  WIFIDPD_OFF_E                                      = 0,
  WIFIDPD_ON_E                                       = 1
} pdg_response_rate_setting__dpd_enable__e;          ///< dpd_enable Enum Type
typedef enum {
  WIFI0_8_US_SGI_E                                   = 0,
  WIFI0_4_US_SGI_E                                   = 1,
  WIFI1_6_US_SGI_E                                   = 2,
  WIFI3_2_US_SGI_E                                   = 3
} pdg_response_rate_setting__sgi__e;                 ///< sgi Enum Type
typedef ht_sig_info__aggregation__e                  pdg_response_rate_setting__aggregation__e;
typedef he_sig_a_su_info__cp_ltf_size__e             pdg_response_rate_setting__dot11ax_cp_ltf_size__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_response_rate_setting__dot11ax_pe_nss__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     pdg_response_rate_setting__dot11ax_dl_ul_flag__e;

//-----------------------------------------------------------------------------
// "phyrx_abort_request_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIPHYRX_ERR_PHY_OFF_E                            = 0,
  WIFIPHYRX_ERR_SYNTH_OFF_E                          = 1,
  WIFIPHYRX_ERR_OFDMA_TIMING_E                       = 2,
  WIFIPHYRX_ERR_OFDMA_SIGNAL_PARITY_E                = 3,
  WIFIPHYRX_ERR_OFDMA_RATE_ILLEGAL_E                 = 4,
  WIFIPHYRX_ERR_OFDMA_LENGTH_ILLEGAL_E               = 5,
  WIFIPHYRX_ERR_OFDMA_RESTART_E                      = 6,
  WIFIPHYRX_ERR_OFDMA_SERVICE_E                      = 7,
  WIFIPHYRX_ERR_PPDU_OFDMA_POWER_DROP_E              = 8,
  WIFIPHYRX_ERR_CCK_BLOKKER_E                        = 9,
  WIFIPHYRX_ERR_CCK_TIMING_E                         = 10,
  WIFIPHYRX_ERR_CCK_HEADER_CRC_E                     = 11,
  WIFIPHYRX_ERR_CCK_RATE_ILLEGAL_E                   = 12,
  WIFIPHYRX_ERR_CCK_LENGTH_ILLEGAL_E                 = 13,
  WIFIPHYRX_ERR_CCK_RESTART_E                        = 14,
  WIFIPHYRX_ERR_CCK_SERVICE_E                        = 15,
  WIFIPHYRX_ERR_CCK_POWER_DROP_E                     = 16,
  WIFIPHYRX_ERR_HT_CRC_ERR_E                         = 17,
  WIFIPHYRX_ERR_HT_LENGTH_ILLEGAL_E                  = 18,
  WIFIPHYRX_ERR_HT_RATE_ILLEGAL_E                    = 19,
  WIFIPHYRX_ERR_HT_ZLF_E                             = 20,
  WIFIPHYRX_ERR_FALSE_RADAR_EXT_E                    = 21,
  WIFIPHYRX_ERR_GREEN_FIELD_E                        = 22,
  WIFIPHYRX_ERR_BW_GT_DYN_BW_E                       = 23,
  WIFIPHYRX_ERR_LEG_HT_MISMATCH_E                    = 24,
  WIFIPHYRX_ERR_VHT_CRC_ERROR_E                      = 25,
  WIFIPHYRX_ERR_VHT_SIGA_UNSUPPORTED_E               = 26,
  WIFIPHYRX_ERR_VHT_LSIG_LEN_INVALID_E               = 27,
  WIFIPHYRX_ERR_VHT_NDP_OR_ZLF_E                     = 28,
  WIFIPHYRX_ERR_VHT_NSYM_LT_ZERO_E                   = 29,
  WIFIPHYRX_ERR_VHT_RX_EXTRA_SYMBOL_MISMATCH_E       = 30,
  WIFIPHYRX_ERR_VHT_RX_SKIP_GROUP_ID0_E              = 31,
  WIFIPHYRX_ERR_VHT_RX_SKIP_GROUP_ID1TO62_E          = 32,
  WIFIPHYRX_ERR_VHT_RX_SKIP_GROUP_ID63_E             = 33,
  WIFIPHYRX_ERR_OFDM_LDPC_DECODER_DISABLED_E         = 34,
  WIFIPHYRX_ERR_DEFER_NAP_E                          = 35,
  WIFIPHYRX_ERR_FDOMAIN_TIMEOUT_E                    = 36,
  WIFIPHYRX_ERR_LSIG_REL_CHECK_E                     = 37,
  WIFIPHYRX_ERR_BT_COLLISION_E                       = 38,
  WIFIPHYRX_ERR_UNSUPPORTED_MU_FEEDBACK_E            = 39,
  WIFIPHYRX_ERR_PPDU_TX_INTERRUPT_RX_E               = 40,
  WIFIPHYRX_ERR_UNSUPPORTED_CBF_E                    = 41,
  WIFIPHYRX_ERR_OTHER_E                              = 42,
  WIFIPHYRX_ERR_HE_SIGA_UNSUPPORTED_E                = 43,
  WIFIPHYRX_ERR_HE_CRC_ERROR_E                       = 44,
  WIFIPHYRX_ERR_HE_SIGB_UNSUPPORTED_E                = 45,
  WIFIPHYRX_ERR_HE_MU_MODE_UNSUPPORTED_E             = 46,
  WIFIPHYRX_ERR_HE_NDP_OR_ZLF_E                      = 47,
  WIFIPHYRX_ERR_HE_NSYM_LT_ZERO_E                    = 48,
  WIFIPHYRX_ERR_HE_RU_PARAMS_UNSUPPORTED_E           = 49,
  WIFIPHYRX_ERR_HE_NUM_USERS_UNSUPPORTED_E           = 50,
  WIFIPHYRX_ERR_HE_SOUNDING_PARAMS_UNSUPPORTED_E     = 51,
  WIFIPHYRX_ERR_MU_UL_NO_POWER_DETECTED_E            = 52
} phyrx_abort_request_info__phyrx_abort_reason__e;   ///< phyrx_abort_reason Enum Type

//-----------------------------------------------------------------------------
// "ppdu_rate_setting" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    ppdu_rate_setting__pkt_type__e;
typedef pdg_response_rate_setting__dpd_enable__e     ppdu_rate_setting__dpd_enable__e;
typedef pdg_response_rate_setting__sgi__e            ppdu_rate_setting__sgi__e;
typedef pdg_response_rate_setting__alt_bw__e         ppdu_rate_setting__alt_bw__e;
typedef enum {
  WIFILTF_1X_E                                       = 0,
  WIFILTF_2X_E                                       = 1,
  WIFILTF_4X_E                                       = 2
} ppdu_rate_setting__ltf_size__e;                    ///< ltf_size Enum Type

//-----------------------------------------------------------------------------
// "prot_rate_setting" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIGI_0_8_US_E                                    = 0,
  WIFIGI_0_4_US_E                                    = 1,
  WIFIGI_1_6_US_E                                    = 2,
  WIFIGI_3_2_US_E                                    = 3
} prot_rate_setting__sgi__e;                         ///< sgi Enum Type
typedef addr_search_entry__ftm_pe_nss__e             prot_rate_setting__alt_nss__e;
typedef pdg_response_rate_setting__alt_bw__e         prot_rate_setting__alt_bw__e;
typedef l_sig_a_info__pkt_type__e                    prot_rate_setting__pkt_type__e;
typedef addr_search_entry__ftm_pe_nss__e             prot_rate_setting__nss__e;
typedef pdg_response_rate_setting__dpd_enable__e     prot_rate_setting__dpd_enable__e;
typedef ppdu_rate_setting__ltf_size__e               prot_rate_setting__ltf_size__e;

//-----------------------------------------------------------------------------
// "receive_user_info" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    receive_user_info__pkt_type__e;
typedef enum {
  WIFIRECEPTION_TYPE_SU_E                            = 0,
  WIFIRECEPTION_TYPE_MU_MIMO_E                       = 1,
  WIFIRECEPTION_TYPE_MU_OFDMA_E                      = 2,
  WIFIRECEPTION_TYPE_MU_OFDMA_MIMO_E                 = 3,
  WIFIRECEPTION_TYPE_UL_MU_MIMO_E                    = 4,
  WIFIRECEPTION_TYPE_UL_MU_OFDMA_E                   = 5,
  WIFIRECEPTION_TYPE_UL_MU_OFDMA_MIMO_E              = 6
} receive_user_info__reception_type__e;              ///< reception_type Enum Type
typedef prot_rate_setting__sgi__e                    receive_user_info__sgi__e;
typedef enum {
  WIFIFULL_RX_BW_20_MHZ_E                            = 0,
  WIFIFULL_RX_BW_40_MHZ_E                            = 1,
  WIFIFULL_RX_BW_80_MHZ_E                            = 2,
  WIFIFULL_RX_BW_160_MHZ_E                           = 3
} receive_user_info__receive_bandwidth__e;           ///< receive_bandwidth Enum Type
typedef enum {
  WIFICONTENT_CHANNEL_1_E                            = 0,
  WIFICONTENT_CHANNEL_2_E                            = 1
} receive_user_info__ofdma_content_channel__e;       ///< ofdma_content_channel Enum Type

//-----------------------------------------------------------------------------
// "received_trigger_info_details" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFISCH_QBOOST_TRIGGER_E                           = 0,
  WIFISCH_PSPOLL_TRIGGER_E                           = 1,
  WIFISCH_UAPSD_TRIGGER_E                            = 2,
  WIFISCH_11AX_TRIGGER_E                             = 3,
  WIFISCH_11AX_WILDCARD_TRIGGER_E                    = 4,
  WIFISCH_11AX_UNASSOC_WILDCARD_TRIGGER_E            = 5
} received_trigger_info_details__trigger_type__e;    ///< trigger_type Enum Type
typedef enum {
  WIFI11AX_TRIGGER_FRAME_E                           = 0,
  WIFIHE_CONTROL_BASED_TRIGGER_E                     = 1
} received_trigger_info_details__ax_trigger_source__e; ///< ax_trigger_source Enum Type

//-----------------------------------------------------------------------------
// "reo_destination_ring" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMSDU_BUF_ADDRESS_E                             = 0,
  WIFIMSDU_LINK_DESC_ADDRESS_E                       = 1
} reo_destination_ring__reo_dest_buffer_type__e;     ///< reo_dest_buffer_type Enum Type
typedef enum {
  WIFIREO_ERROR_DETECTED_E                           = 0,
  WIFIREO_ROUTING_INSTRUCTION_E                      = 1
} reo_destination_ring__reo_push_reason__e;          ///< reo_push_reason Enum Type
typedef enum {
  WIFIREO_QUEUE_DESC_ADDR_ZERO_E                     = 0,
  WIFIREO_QUEUE_DESC_NOT_VALID_E                     = 1,
  WIFIAMPDU_IN_NON_BA_E                              = 2,
  WIFINON_BA_DUPLICATE_E                             = 3,
  WIFIBA_DUPLICATE_E                                 = 4,
  WIFIREGULAR_FRAME_2K_JUMP_E                        = 5,
  WIFIBAR_FRAME_2K_JUMP_E                            = 6,
  WIFIREGULAR_FRAME_OOR_E                            = 7,
  WIFIBAR_FRAME_OOR_E                                = 8,
  WIFIBAR_FRAME_NO_BA_SESSION_E                      = 9,
  WIFIBAR_FRAME_SN_EQUALS_SSN_E                      = 10,
  WIFIPN_CHECK_FAILED_E                              = 11,
  WIFI2K_ERROR_HANDLING_FLAG_SET_E                   = 12,
  WIFIPN_ERROR_HANDLING_FLAG_SET_E                   = 13,
  WIFIQUEUE_DESCRIPTOR_BLOCKED_SET_E                 = 14
} reo_destination_ring__reo_error_code__e;           ///< reo_error_code Enum Type
typedef enum {
  WIFIINVALID_E                                      = 0,
  WIFIFWDCUR_FWDBUF_E                                = 1,
  WIFIFWDBUF_FWDCUR_E                                = 2,
  WIFIQCUR_E                                         = 3,
  WIFIFWDBUF_QCUR_E                                  = 4,
  WIFIFWDBUF_DROP_E                                  = 5,
  WIFIFWDALL_DROP_E                                  = 6,
  WIFIFWDALL_QCUR_E                                  = 7,
  WIFIRESERVED_REO_OPCODE_1_E                        = 8,
  WIFIDROPCUR_E                                      = 9,
  WIFIRESERVED_REO_OPCODE_2_E                        = 10,
  WIFIRESERVED_REO_OPCODE_3_E                        = 11,
  WIFIRESERVED_REO_OPCODE_4_E                        = 12,
  WIFIRESERVED_REO_OPCODE_5_E                        = 13,
  WIFIRESERVED_REO_OPCODE_6_E                        = 14,
  WIFIRESERVED_REO_OPCODE_7_E                        = 15
} reo_destination_ring__reorder_opcode__e;           ///< reorder_opcode Enum Type

//-----------------------------------------------------------------------------
// "reo_entrance_ring" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREO_DESTINATION_TCL_E                          = 0,
  WIFIREO_DESTINATION_SW1_E                          = 1,
  WIFIREO_DESTINATION_SW2_E                          = 2,
  WIFIREO_DESTINATION_SW3_E                          = 3,
  WIFIREO_DESTINATION_SW4_E                          = 4,
  WIFIREO_DESTINATION_RELEASE_E                      = 5,
  WIFIREO_DESTINATION_FW_E                           = 6,
  WIFIREO_DESTINATION_7_E                            = 7,
  WIFIREO_DESTINATION_8_E                            = 8,
  WIFIREO_DESTINATION_9_E                            = 9,
  WIFIREO_DESTINATION_10_E                           = 10,
  WIFIREO_DESTINATION_11_E                           = 11,
  WIFIREO_DESTINATION_12_E                           = 12,
  WIFIREO_DESTINATION_13_E                           = 13,
  WIFIREO_DESTINATION_14_E                           = 14,
  WIFIREO_DESTINATION_15_E                           = 15,
  WIFIREO_DESTINATION_16_E                           = 16,
  WIFIREO_DESTINATION_17_E                           = 17,
  WIFIREO_DESTINATION_18_E                           = 18,
  WIFIREO_DESTINATION_19_E                           = 19,
  WIFIREO_DESTINATION_20_E                           = 20,
  WIFIREO_DESTINATION_21_E                           = 21,
  WIFIREO_DESTINATION_22_E                           = 22,
  WIFIREO_DESTINATION_23_E                           = 23,
  WIFIREO_DESTINATION_24_E                           = 24,
  WIFIREO_DESTINATION_25_E                           = 25,
  WIFIREO_DESTINATION_26_E                           = 26,
  WIFIREO_DESTINATION_27_E                           = 27,
  WIFIREO_DESTINATION_28_E                           = 28,
  WIFIREO_DESTINATION_29_E                           = 29,
  WIFIREO_DESTINATION_30_E                           = 30,
  WIFIREO_DESTINATION_31_E                           = 31
} reo_entrance_ring__reo_destination_indication__e;  ///< reo_destination_indication Enum Type
typedef enum {
  WIFIRXDMA_ERROR_DETECTED_E                         = 0,
  WIFIRXDMA_ROUTING_INSTRUCTION_E                    = 1,
  WIFIRXDMA_RX_FLUSH_E                               = 2
} reo_entrance_ring__rxdma_push_reason__e;           ///< rxdma_push_reason Enum Type
typedef enum {
  WIFIRXDMA_OVERFLOW_ERR_E                           = 0,
  WIFIRXDMA_MPDU_LENGTH_ERR_E                        = 1,
  WIFIRXDMA_FCS_ERR_E                                = 2,
  WIFIRXDMA_DECRYPT_ERR_E                            = 3,
  WIFIRXDMA_TKIP_MIC_ERR_E                           = 4,
  WIFIRXDMA_UNECRYPTED_ERR_E                         = 5,
  WIFIRXDMA_MSDU_LEN_ERR_E                           = 6,
  WIFIRXDMA_MSDU_LIMIT_ERR_E                         = 7,
  WIFIRXDMA_WIFI_PARSE_ERR_E                         = 8,
  WIFIRXDMA_AMSDU_PARSE_ERR_E                        = 9,
  WIFIRXDMA_SA_TIMEOUT_ERR_E                         = 10,
  WIFIRXDMA_DA_TIMEOUT_ERR_E                         = 11,
  WIFIRXDMA_FLOW_TIMEOUT_ERR_E                       = 12,
  WIFIRXDMA_FLUSH_REQUEST_E                          = 13
} reo_entrance_ring__rxdma_error_code__e;            ///< rxdma_error_code Enum Type

//-----------------------------------------------------------------------------
// "response_rate_setting" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    response_rate_setting__pkt_type__e;
typedef addr_search_entry__ftm_pe_nss__e             response_rate_setting__nss__e;
typedef pdg_response_rate_setting__sgi__e            response_rate_setting__sgi__e;
typedef addr_search_entry__ftm_pe_nss__e             response_rate_setting__alt_nss__e;
typedef ppdu_rate_setting__ltf_size__e               response_rate_setting__ltf_size__e;

//-----------------------------------------------------------------------------
// "rx_flow_search_entry" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIICMPV4_E                                       = 1,
  WIFITCP_E                                          = 6,
  WIFIUDP_E                                          = 17,
  WIFIESP_E                                          = 50,
  WIFIAH_E                                           = 51,
  WIFIICMPV6_E                                       = 58
} rx_flow_search_entry__l4_protocol__e;              ///< l4_protocol Enum Type
typedef enum {
  WIFIRXFT_USE_FT_E                                  = 0,
  WIFIRXFT_USE_ASPT_E                                = 1,
  WIFIRXFT_USE_FT2_E                                 = 2,
  WIFIRXFT_USE_CCE_E                                 = 3
} rx_flow_search_entry__reo_destination_handler__e;  ///< reo_destination_handler Enum Type
typedef reo_entrance_ring__reo_destination_indication__e rx_flow_search_entry__reo_destination_indication__e;

//-----------------------------------------------------------------------------
// "rx_location_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFILOCATION_FAC_LEGACY_STATUS_NOT_VALID_E         = 0,
  WIFILOCATION_FAC_LEGACY_STATUS_VALID_E             = 1
} rx_location_info__rtt_fac_legacy_status__e;        ///< rtt_fac_legacy_status Enum Type
typedef enum {
  WIFILOCATION_FAC_LEGACY_EXT80_STATUS_NOT_VALID_E   = 0,
  WIFILOCATION_FAC_LEGACY_EXT80_STATUS_VALID_E       = 1
} rx_location_info__rtt_fac_legacy_ext80_status__e;  ///< rtt_fac_legacy_ext80_status Enum Type
typedef enum {
  WIFILOCATION_FAC_VHT_STATUS_NOT_VALID_E            = 0,
  WIFILOCATION_FAC_VHT_STATUS_VALID_E                = 1
} rx_location_info__rtt_fac_vht_status__e;           ///< rtt_fac_vht_status Enum Type
typedef enum {
  WIFILOCATION_FAC_VHT_EXT80_STATUS_NOT_VALID_E      = 0,
  WIFILOCATION_FAC_VHT_EXT80_STATUS_VALID_E          = 1
} rx_location_info__rtt_fac_vht_ext80_status__e;     ///< rtt_fac_vht_ext80_status Enum Type
typedef enum {
  WIFILOCATION_CFR_DUMP_NOT_VALID_E                  = 0,
  WIFILOCATION_CFR_DUMP_VALID_E                      = 1
} rx_location_info__rtt_cfr_status__e;               ///< rtt_cfr_status Enum Type
typedef enum {
  WIFILOCATION_CIR_DUMP_NOT_VALID_E                  = 0,
  WIFILOCATION_CIR_DUMP_VALID_E                      = 1
} rx_location_info__rtt_cir_status__e;               ///< rtt_cir_status Enum Type
typedef enum {
  WIFILOCATION_SW_IFFT_MODE_E                        = 0,
  WIFILOCATION_HW_IFFT_MODE_E                        = 1
} rx_location_info__rtt_hw_ifft_mode__e;             ///< rtt_hw_ifft_mode Enum Type
typedef enum {
  WIFILOCATION_NOT_BTCF_BASED_TS_E                   = 0,
  WIFILOCATION_BTCF_BASED_TS_E                       = 1
} rx_location_info__rtt_btcf_status__e;              ///< rtt_btcf_status Enum Type
typedef enum {
  WIFILOCATION_PREAMBLE_TYPE_LEGACY_E                = 0,
  WIFILOCATION_PREAMBLE_TYPE_HT_E                    = 1,
  WIFILOCATION_PREAMBLE_TYPE_VHT_E                   = 2,
  WIFILOCATION_PREAMBLE_TYPE_HE_SU_4XLTF_E           = 3,
  WIFILOCATION_PREAMBLE_TYPE_HE_SU_2XLTF_E           = 4,
  WIFILOCATION_PREAMBLE_TYPE_HE_SU_1XLTF_E           = 5,
  WIFILOCATION_PREAMBLE_TYPE_HE_TRIGGER_BASED_UL_4XLTF_E = 6,
  WIFILOCATION_PREAMBLE_TYPE_HE_TRIGGER_BASED_UL_2XLTF_E = 7,
  WIFILOCATION_PREAMBLE_TYPE_HE_TRIGGER_BASED_UL_1XLTF_E = 8,
  WIFILOCATION_PREAMBLE_TYPE_HE_MU_4XLTF_E           = 9,
  WIFILOCATION_PREAMBLE_TYPE_HE_MU_2XLTF_E           = 10,
  WIFILOCATION_PREAMBLE_TYPE_HE_MU_1XLTF_E           = 11,
  WIFILOCATION_PREAMBLE_TYPE_HE_EXTENDED_RANGE_SU_4XLTF_E = 12,
  WIFILOCATION_PREAMBLE_TYPE_HE_EXTENDED_RANGE_SU_2XLTF_E = 13,
  WIFILOCATION_PREAMBLE_TYPE_HE_EXTENDED_RANGE_SU_1XLTF_E = 14
} rx_location_info__rtt_preamble_type__e;            ///< rtt_preamble_type Enum Type
typedef enum {
  WIFILOCATION_PKT_BW_20MHZ_E                        = 0,
  WIFILOCATION_PKT_BW_40MHZ_E                        = 1,
  WIFILOCATION_PKT_BW_80MHZ_E                        = 2,
  WIFILOCATION_PKT_BW_160MHZ_E                       = 3
} rx_location_info__rtt_pkt_bw_leg__e;               ///< rtt_pkt_bw_leg Enum Type
typedef rx_location_info__rtt_pkt_bw_leg__e          rx_location_info__rtt_pkt_bw_vht__e;
typedef prot_rate_setting__sgi__e                    rx_location_info__rtt_gi_type__e;
typedef enum {
  WIFILOCATION_STRONGEST_CHAIN_IS_0_E                = 0,
  WIFILOCATION_STRONGEST_CHAIN_IS_1_E                = 1,
  WIFILOCATION_STRONGEST_CHAIN_IS_2_E                = 2,
  WIFILOCATION_STRONGEST_CHAIN_IS_3_E                = 3,
  WIFILOCATION_STRONGEST_CHAIN_IS_4_E                = 4,
  WIFILOCATION_STRONGEST_CHAIN_IS_5_E                = 5,
  WIFILOCATION_STRONGEST_CHAIN_IS_6_E                = 6,
  WIFILOCATION_STRONGEST_CHAIN_IS_7_E                = 7
} rx_location_info__rtt_strongest_chain__e;          ///< rtt_strongest_chain Enum Type
typedef rx_location_info__rtt_strongest_chain__e     rx_location_info__rtt_strongest_chain_ext80__e;
typedef enum {
  WIFITIMING_BACKOFF_LOW_RSSI_E                      = 0,
  WIFITIMING_BACKOFF_MID_RSSI_E                      = 1,
  WIFITIMING_BACKOFF_HIGH_RSSI_E                     = 2,
  WIFIRESERVED_E                                     = 3
} rx_location_info__rtt_timing_backoff_sel__e;       ///< rtt_timing_backoff_sel Enum Type
typedef enum {
  WIFIRX_LOCATION_INFO_IS_NOT_VALID_E                = 0,
  WIFIRX_LOCATION_INFO_IS_VALID_E                    = 1
} rx_location_info__rx_location_info_valid__e;       ///< rx_location_info_valid Enum Type

//-----------------------------------------------------------------------------
// "rx_mpdu_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRXPCU_FILTER_PASS_E                            = 0,
  WIFIRXPCU_MONITOR_CLIENT_E                         = 1,
  WIFIRXPCU_MONITOR_OTHER_E                          = 2
} rx_mpdu_info__rxpcu_mpdu_filter_in_category__e;    ///< rxpcu_mpdu_filter_in_category Enum Type
typedef enum {
  WIFISW_FRAME_GROUP_NDP_FRAME_E                     = 0,
  WIFISW_FRAME_GROUP_MULTICAST_DATA_E                = 1,
  WIFISW_FRAME_GROUP_UNICAST_DATA_E                  = 2,
  WIFISW_FRAME_GROUP_NULL_DATA_E                     = 3,
  WIFISW_FRAME_GROUP_MGMT_0000_E                     = 4,
  WIFISW_FRAME_GROUP_MGMT_0001_E                     = 5,
  WIFISW_FRAME_GROUP_MGMT_0010_E                     = 6,
  WIFISW_FRAME_GROUP_MGMT_0011_E                     = 7,
  WIFISW_FRAME_GROUP_MGMT_0100_E                     = 8,
  WIFISW_FRAME_GROUP_MGMT_0101_E                     = 9,
  WIFISW_FRAME_GROUP_MGMT_0110_E                     = 10,
  WIFISW_FRAME_GROUP_MGMT_0111_E                     = 11,
  WIFISW_FRAME_GROUP_MGMT_1000_E                     = 12,
  WIFISW_FRAME_GROUP_MGMT_1001_E                     = 13,
  WIFISW_FRAME_GROUP_MGMT_1010_E                     = 14,
  WIFISW_FRAME_GROUP_MGMT_1011_E                     = 15,
  WIFISW_FRAME_GROUP_MGMT_1100_E                     = 16,
  WIFISW_FRAME_GROUP_MGMT_1101_E                     = 17,
  WIFISW_FRAME_GROUP_MGMT_1110_E                     = 18,
  WIFISW_FRAME_GROUP_MGMT_1111_E                     = 19,
  WIFISW_FRAME_GROUP_CTRL_0000_E                     = 20,
  WIFISW_FRAME_GROUP_CTRL_0001_E                     = 21,
  WIFISW_FRAME_GROUP_CTRL_0010_E                     = 22,
  WIFISW_FRAME_GROUP_CTRL_0011_E                     = 23,
  WIFISW_FRAME_GROUP_CTRL_0100_E                     = 24,
  WIFISW_FRAME_GROUP_CTRL_0101_E                     = 25,
  WIFISW_FRAME_GROUP_CTRL_0110_E                     = 26,
  WIFISW_FRAME_GROUP_CTRL_0111_E                     = 27,
  WIFISW_FRAME_GROUP_CTRL_1000_E                     = 28,
  WIFISW_FRAME_GROUP_CTRL_1001_E                     = 29,
  WIFISW_FRAME_GROUP_CTRL_1010_E                     = 30,
  WIFISW_FRAME_GROUP_CTRL_1011_E                     = 31,
  WIFISW_FRAME_GROUP_CTRL_1100_E                     = 32,
  WIFISW_FRAME_GROUP_CTRL_1101_E                     = 33,
  WIFISW_FRAME_GROUP_CTRL_1110_E                     = 34,
  WIFISW_FRAME_GROUP_CTRL_1111_E                     = 35,
  WIFISW_FRAME_GROUP_UNSUPPORTED_E                   = 36,
  WIFISW_FRAME_GROUP_PHY_ERROR_E                     = 37
} rx_mpdu_info__sw_frame_group_id__e;                ///< sw_frame_group_id Enum Type
typedef addr_search_entry__key_type__e               rx_mpdu_info__encrypt_type__e;
typedef enum {
  WIFIWEP_VARIED_WIDTH_40_E                          = 0,
  WIFIWEP_VARIED_WIDTH_104_E                         = 1,
  WIFIWEP_VARIED_WIDTH_128_E                         = 2
} rx_mpdu_info__wep_key_width_for_variable_key__e;   ///< wep_key_width_for_variable_key Enum Type
typedef enum {
  WIFIRAW_E                                          = 0,
  WIFINATIVE_WIFI_E                                  = 1,
  WIFIETHERNET_E                                     = 2,
  WIFI802_3_E                                        = 3
} rx_mpdu_info__decap_type__e;                       ///< decap_type Enum Type

//-----------------------------------------------------------------------------
// "rx_msdu_desc_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINOT_FIRST_MSDU_E                               = 0,
  WIFIFIRST_MSDU_E                                   = 1
} rx_msdu_desc_info__first_msdu_in_mpdu_flag__e;     ///< first_msdu_in_mpdu_flag Enum Type
typedef enum {
  WIFINOT_LAST_MSDU_E                                = 0,
  WIFILAST_MSDU_E                                    = 1
} rx_msdu_desc_info__last_msdu_in_mpdu_flag__e;      ///< last_msdu_in_mpdu_flag Enum Type
typedef reo_entrance_ring__reo_destination_indication__e rx_msdu_desc_info__reo_destination_indication__e;

//-----------------------------------------------------------------------------
// "rx_peer_entry_details" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__key_type__e               rx_peer_entry_details__key_type__e;
typedef rx_mpdu_info__decap_type__e                  rx_peer_entry_details__decap_type__e;
typedef rx_mpdu_info__wep_key_width_for_variable_key__e rx_peer_entry_details__wep_key_width_for_keyid_0__e;
typedef rx_mpdu_info__wep_key_width_for_variable_key__e rx_peer_entry_details__wep_key_width_for_keyid_1__e;
typedef rx_mpdu_info__wep_key_width_for_variable_key__e rx_peer_entry_details__wep_key_width_for_keyid_2__e;
typedef rx_mpdu_info__wep_key_width_for_variable_key__e rx_peer_entry_details__wep_key_width_for_keyid_3__e;

//-----------------------------------------------------------------------------
// "rx_reo_queue" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIPN_SIZE_24_E                                   = 0,
  WIFIPN_SIZE_48_E                                   = 1,
  WIFIPN_SIZE_128_E                                  = 2
} rx_reo_queue__pn_size__e;                          ///< pn_size Enum Type

//-----------------------------------------------------------------------------
// "rxole_cce_classify_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRXCCE_USE_CCE_E                                = 0,
  WIFIRXCCE_USE_ASPT_E                               = 1,
  WIFIRXCCE_USE_FT_E                                 = 2,
  WIFIRXCCE_USE_CCE2_E                               = 3
} rxole_cce_classify_info__reo_destination_handler__e; ///< reo_destination_handler Enum Type

//-----------------------------------------------------------------------------
// "rxpt_classify_info" enums
//-----------------------------------------------------------------------------
typedef reo_entrance_ring__reo_destination_indication__e rxpt_classify_info__reo_destination_indication__e;
typedef enum {
  WIFIWBM2RXDMA_BUF_SOURCE_RING_E                    = 0,
  WIFIFW2RXDMA_BUF_SOURCE_RING_E                     = 1,
  WIFISW2RXDMA_BUF_SOURCE_RING_E                     = 2,
  WIFINO_BUFFER_RING_E                               = 3
} rxpt_classify_info__rxdma0_source_ring_selection__e; ///< rxdma0_source_ring_selection Enum Type
typedef enum {
  WIFIRXDMA_RELEASE_RING_E                           = 0,
  WIFIRXDMA2FW_RING_E                                = 1,
  WIFIRXDMA2SW_RING_E                                = 2,
  WIFIRXDMA2REO_RING_E                               = 3
} rxpt_classify_info__rxdma0_destination_ring_selection__e; ///< rxdma0_destination_ring_selection Enum Type

//-----------------------------------------------------------------------------
// "scheduler_cmd" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIBO_TRANSMISSION_ONLY_E                         = 0,
  WIFITRIGGER_TRANSMISSION_ONLY_E                    = 1,
  WIFIBO_OR_TRIGGER_TRANSMISSION_E                   = 2
} scheduler_cmd__bo_or_response_setting__e;          ///< bo_or_response_setting Enum Type
typedef enum {
  WIFINO_SIFS_BURSTING_E                             = 0,
  WIFIALWAYS_SIFS_BURSTING_E                         = 1
} scheduler_cmd__sifs_burst_continuation__e;         ///< sifs_burst_continuation Enum Type
typedef enum {
  WIFISIFS_E                                         = 0,
  WIFIPIFS_E                                         = 1
} scheduler_cmd__burst_continuation_ifs_time__e;     ///< burst_continuation_ifs_time Enum Type
typedef scheduler_cmd__burst_continuation_ifs_time__e scheduler_cmd__burst_continuation_ifs_time_prev_cmd_overwrite__e;
typedef enum {
  WIFISCHEDULER_TLV_SOURCE_E                         = 0,
  WIFITQM_SOURCE_E                                   = 1
} scheduler_cmd__mpdu_info_source__e;                ///< mpdu_info_source Enum Type
typedef enum {
  WIFISCH_CMD_STATUS_RING0_E                         = 0,
  WIFISCH_CMD_STATUS_RING1_E                         = 1
} scheduler_cmd__cmd_status_ring_to_use__e;          ///< cmd_status_ring_to_use Enum Type
typedef enum {
  WIFISW_TRANSMIT_MODE_E                             = 0,
  WIFIPDG_TRANSMIT_MODE_E                            = 1
} scheduler_cmd__fes_control_mode__e;                ///< fes_control_mode Enum Type
typedef enum {
  WIFIBASIC_E                                        = 0,
  WIFISTART_AFTER_BO_OR_ONGOING_E                    = 1
} scheduler_cmd__txop_time_ctrl__e;                  ///< txop_time_ctrl Enum Type
typedef enum {
  WIFITSF1_E                                         = 0,
  WIFITSF2_E                                         = 1,
  WIFIWB_TIMER_E                                     = 2,
  WIFIQUEUE_TIMER0_E                                 = 3,
  WIFIQUEUE_TIMER1_E                                 = 4
} scheduler_cmd__start_time_reference__e;            ///< start_time_reference Enum Type
typedef scheduler_cmd__start_time_reference__e       scheduler_cmd__end_time_reference__e;
typedef enum {
  WIFICMD_FOR_QBOOST_TRIGGER_E                       = 0,
  WIFICMD_FOR_PSPOLL_TRIGGER_E                       = 1,
  WIFICMD_FOR_UAPSD_TRIGGER_E                        = 2,
  WIFICMD_FOR_11AX_TRIGGER_E                         = 3,
  WIFICMD_FOR_11AX_UNASSOC_WILDCARD_TRIGGER_E        = 4
} scheduler_cmd__trigger_type__e;                    ///< trigger_type Enum Type

//-----------------------------------------------------------------------------
// "tcl_cce_classify_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITXCCE_TO_FW_E                                  = 0,
  WIFITXCCE_USE_ASPT_E                               = 1,
  WIFITXCCE_USE_FT_E                                 = 2,
  WIFITXCCE_TO_TQM_E                                 = 3
} tcl_cce_classify_info__tqm_flow_handler__e;        ///< tqm_flow_handler Enum Type
typedef enum {
  WIFITXFT_LOOP_TO_FW_E                              = 0,
  WIFITXFT_LOOP_TO_TQM_E                             = 1
} tcl_cce_classify_info__tqm_flow_loop_handler__e;   ///< tqm_flow_loop_handler Enum Type

//-----------------------------------------------------------------------------
// "tcl_exit_base" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMSDU_BUFFER_E                                  = 0,
  WIFIEXTENSION_DESCRIPTOR_E                         = 1
} tcl_exit_base__buf_or_ext_desc_type__e;            ///< buf_or_ext_desc_type Enum Type
typedef rx_mpdu_info__decap_type__e                  tcl_exit_base__encap_type__e;
typedef enum {
  WIFISW2TCL1_E                                      = 0,
  WIFISW2TCL2_E                                      = 1,
  WIFISW2TCL3_E                                      = 2,
  WIFISW2TCL_CMD_E                                   = 3,
  WIFIFW2TCL1_E                                      = 4
} tcl_exit_base__msdu_input_ring__e;                 ///< msdu_input_ring Enum Type
typedef enum {
  WIFITO_FW_E                                        = 1,
  WIFITO_TQM_FW_M0_E                                 = 2,
  WIFITO_TQM_FW_E                                    = 3
} tcl_exit_base__tcl_hndlr__e;                       ///< tcl_hndlr Enum Type

//-----------------------------------------------------------------------------
// "tcl_status_ring" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRD_STAT_E                                      = 0,
  WIFISRCH_DIS_E                                     = 1,
  WIFIWR_BK_SINGLE_E                                 = 2,
  WIFIWR_BK_ALL_E                                    = 3,
  WIFIINVAL_SINGLE_E                                 = 4,
  WIFIINVAL_ALL_E                                    = 5,
  WIFIWR_BK_INVAL_SINGLE_E                           = 6,
  WIFIWR_BK_INVAL_ALL_E                              = 7,
  WIFICLR_STAT_SINGLE_E                              = 8
} tcl_status_ring__gse_ctrl__e;                      ///< gse_ctrl Enum Type
typedef enum {
  WIFIOP_DONE_E                                      = 0,
  WIFINOT_FND_E                                      = 1,
  WIFITIMEOUT_ER_E                                   = 2
} tcl_status_ring__cache_op_res__e;                  ///< cache_op_res Enum Type
typedef enum {
  WIFIINDEX_BASED_CMD_DISABLE_E                      = 0,
  WIFIINDEX_BASED_CMD_ENABLE_E                       = 1
} tcl_status_ring__index_search_en__e;               ///< index_search_en Enum Type

//-----------------------------------------------------------------------------
// "tqm_entrance_ring" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINOSTATUS_E                                     = 0,
  WIFISTATUSREQUIRED_E                               = 1
} tqm_entrance_ring__tqm_status_required__e;         ///< tqm_status_required Enum Type
typedef enum {
  WIFITQM_STATUS_RING_0_E                            = 0,
  WIFITQM_STATUS_RING_1_E                            = 1
} tqm_entrance_ring__tqm_status_ring__e;             ///< tqm_status_ring Enum Type

//-----------------------------------------------------------------------------
// "tx_flow_search_entry" enums
//-----------------------------------------------------------------------------
typedef rx_flow_search_entry__l4_protocol__e         tx_flow_search_entry__l4_protocol__e;
typedef enum {
  WIFITXFT_TO_FW_E                                   = 0,
  WIFITXFT_USE_ASPT_E                                = 1,
  WIFITXFT_TO_TQM_E                                  = 2,
  WIFITXFT_USE_CCE_E                                 = 3
} tx_flow_search_entry__tqm_flow_handler__e;         ///< tqm_flow_handler Enum Type
typedef tcl_cce_classify_info__tqm_flow_loop_handler__e tx_flow_search_entry__tqm_flow_loop_handler__e;

//-----------------------------------------------------------------------------
// "tx_mpdu_details" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMPDU_TYPE_BASIC_E                              = 0,
  WIFIMPDU_TYPE_AMSDU_E                              = 1
} tx_mpdu_details__mpdu_type__e;                     ///< mpdu_type Enum Type

//-----------------------------------------------------------------------------
// "tx_mpdu_queue_head" enums
//-----------------------------------------------------------------------------
typedef tx_mpdu_details__mpdu_type__e                tx_mpdu_queue_head__mpdu_type__e;

//-----------------------------------------------------------------------------
// "tx_msdu_details" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMSDU_TX_BUFFER_E                               = 0,
  WIFIMSDU_TX_EXT_DESCRIPTOR_E                       = 1
} tx_msdu_details__msdu_buffer_type__e;              ///< msdu_buffer_type Enum Type
typedef rx_msdu_desc_info__last_msdu_in_mpdu_flag__e tx_msdu_details__last_msdu_in_mpdu_flag__e;
typedef rx_mpdu_info__decap_type__e                  tx_msdu_details__encap_type__e;

//-----------------------------------------------------------------------------
// "tx_msdu_flow" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIDROP_DISABLED_E                                = 0,
  WIFIDROP_ENABLED_E                                 = 1
} tx_msdu_flow__drop_rule__e;                        ///< drop_rule Enum Type

//-----------------------------------------------------------------------------
// "tx_rate_stats_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITRANSMIT_BW_20_MHZ_E                           = 0,
  WIFITRANSMIT_BW_40_MHZ_E                           = 1,
  WIFITRANSMIT_BW_80_MHZ_E                           = 2,
  WIFITRANSMIT_BW_160_MHZ_E                          = 3
} tx_rate_stats_info__transmit_bw__e;                ///< transmit_bw Enum Type
typedef l_sig_a_info__pkt_type__e                    tx_rate_stats_info__transmit_pkt_type__e;
typedef pdg_response_rate_setting__sgi__e            tx_rate_stats_info__transmit_sgi__e;

//-----------------------------------------------------------------------------
// "txpt_classify_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITXPT_TO_FW_E                                   = 0,
  WIFITXPT_TO_TQM_E                                  = 1,
  WIFITXPT_USE_FT_E                                  = 2,
  WIFITXPT_USE_CCE_E                                 = 3
} txpt_classify_info__tqm_flow_handler__e;           ///< tqm_flow_handler Enum Type
typedef enum {
  WIFITXPT_LOOP_TO_FW_E                              = 0,
  WIFITXPT_LOOP_TO_TQM_E                             = 1
} txpt_classify_info__tqm_flow_loop_handler__e;      ///< tqm_flow_loop_handler Enum Type

//-----------------------------------------------------------------------------
// "uniform_descriptor_header" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIWBM_OWNED_E                                    = 0,
  WIFISW_OR_FW_OWNED_E                               = 1,
  WIFITQM_OWNED_E                                    = 2,
  WIFIRXDMA_OWNED_E                                  = 3,
  WIFIREO_OWNED_E                                    = 4,
  WIFISWITCH_OWNED_E                                 = 5
} uniform_descriptor_header__owner__e;               ///< owner Enum Type
typedef enum {
  WIFITRANSMIT_MSDU_LINK_DESCRIPTOR_E                = 0,
  WIFITRANSMIT_MPDU_LINK_DESCRIPTOR_E                = 1,
  WIFITRANSMIT_MPDU_QUEUE_HEAD_DESCRIPTOR_E          = 2,
  WIFITRANSMIT_MPDU_QUEUE_EXT_DESCRIPTOR_E           = 3,
  WIFITRANSMIT_FLOW_DESCRIPTOR_E                     = 4,
  WIFITRANSMIT_BUFFER_E                              = 5,
  WIFIRECEIVE_MSDU_LINK_DESCRIPTOR_E                 = 6,
  WIFIRECEIVE_MPDU_LINK_DESCRIPTOR_E                 = 7,
  WIFIRECEIVE_REO_QUEUE_DESCRIPTOR_E                 = 8,
  WIFIRECEIVE_REO_QUEUE_EXT_DESCRIPTOR_E             = 9,
  WIFIRECEIVE_BUFFER_E                               = 10,
  WIFIIDLE_LINK_LIST_ENTRY_E                         = 11
} uniform_descriptor_header__buffer_type__e;         ///< buffer_type Enum Type

//-----------------------------------------------------------------------------
// "uniform_reo_cmd_header" enums
//-----------------------------------------------------------------------------
typedef tqm_entrance_ring__tqm_status_required__e    uniform_reo_cmd_header__reo_status_required__e;

//-----------------------------------------------------------------------------
// "uniform_reo_status_header" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREO_SUCCESSFUL_EXECUTION_E                     = 0,
  WIFIREO_BLOCKED_EXECUTION_E                        = 1,
  WIFIREO_FAILED_EXECUTION_E                         = 2,
  WIFIREO_RESOURCE_BLOCKED_E                         = 3
} uniform_reo_status_header__reo_cmd_execution_status__e; ///< reo_cmd_execution_status Enum Type

//-----------------------------------------------------------------------------
// "uniform_tqm_cmd_header" enums
//-----------------------------------------------------------------------------
typedef tqm_entrance_ring__tqm_status_required__e    uniform_tqm_cmd_header__tqm_status_required__e;
typedef tqm_entrance_ring__tqm_status_ring__e        uniform_tqm_cmd_header__tqm_status_ring__e;

//-----------------------------------------------------------------------------
// "uniform_tqm_status_header" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITIME_UNIT_2_US_E                               = 0,
  WIFITIME_UNIT_32_US_E                              = 1
} uniform_tqm_status_header__cmd_execution_time_unit__e; ///< cmd_execution_time_unit Enum Type
typedef enum {
  WIFITQM_SUCCESSFUL_EXECUTION_E                     = 0,
  WIFITQM_FAILED_WITH_INVALID_DESCRIPTOR_E           = 1,
  WIFITQM_RESOURCE_BLOCKED_E                         = 2,
  WIFITQM_SCH_FLUSH_E                                = 3
} uniform_tqm_status_header__tqm_cmd_execution_status__e; ///< tqm_cmd_execution_status Enum Type

//-----------------------------------------------------------------------------
// "uplink_common_info" enums
//-----------------------------------------------------------------------------
typedef prot_rate_setting__sgi__e                    uplink_common_info__bw20_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info__bw20_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info__bw20_packet_extension_a_factor__e;
typedef enum {
  WIFIPACKET_EXT_DURATION_0_E                        = 0,
  WIFIPACKET_EXT_DURATION_4_E                        = 1,
  WIFIPACKET_EXT_DURATION_8_E                        = 2,
  WIFIPACKET_EXT_DURATION_12_E                       = 3,
  WIFIPACKET_EXT_DURATION_16_E                       = 4
} uplink_common_info__bw20_packet_extension_duration__e; ///< bw20_packet_extension_duration Enum Type
typedef prot_rate_setting__sgi__e                    uplink_common_info__bw40_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info__bw40_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info__bw40_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info__bw40_packet_extension_duration__e;
typedef prot_rate_setting__sgi__e                    uplink_common_info__bw80_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info__bw80_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info__bw80_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info__bw80_packet_extension_duration__e;
typedef prot_rate_setting__sgi__e                    uplink_common_info__bw160_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info__bw160_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info__bw160_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info__bw160_packet_extension_duration__e;

//-----------------------------------------------------------------------------
// "uplink_common_info_punc" enums
//-----------------------------------------------------------------------------
typedef prot_rate_setting__sgi__e                    uplink_common_info_punc__pat4_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info_punc__pat4_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info_punc__pat4_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info_punc__pat4_packet_extension_duration__e;
typedef prot_rate_setting__sgi__e                    uplink_common_info_punc__pat5_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info_punc__pat5_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info_punc__pat5_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info_punc__pat5_packet_extension_duration__e;
typedef prot_rate_setting__sgi__e                    uplink_common_info_punc__pat6_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info_punc__pat6_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info_punc__pat6_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info_punc__pat6_packet_extension_duration__e;
typedef prot_rate_setting__sgi__e                    uplink_common_info_punc__pat7_sgi__e;
typedef ppdu_rate_setting__ltf_size__e               uplink_common_info_punc__pat7_ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e uplink_common_info_punc__pat7_packet_extension_a_factor__e;
typedef uplink_common_info__bw20_packet_extension_duration__e uplink_common_info_punc__pat7_packet_extension_duration__e;

//-----------------------------------------------------------------------------
// "uplink_user_setup_info" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             uplink_user_setup_info__nss__e;

//-----------------------------------------------------------------------------
// "vht_sig_a_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFI20_MHZ_11AC_E                                  = 0,
  WIFI40_MHZ_11AC_E                                  = 1,
  WIFI80_MHZ_11AC_E                                  = 2,
  WIFI160_MHZ_11AC_E                                 = 3
} vht_sig_a_info__bandwidth__e;                      ///< bandwidth Enum Type
typedef enum {
  WIFISTBC_DISABLED_E                                = 0,
  WIFISTBC_ENABLED_E                                 = 1
} vht_sig_a_info__stbc__e;                           ///< stbc Enum Type
typedef enum {
  WIFINO_TXOP_PS_ALLOWED_E                           = 1
} vht_sig_a_info__txop_ps_not_allowed__e;            ///< txop_ps_not_allowed Enum Type
typedef enum {
  WIFINORMAL_GI_E                                    = 0,
  WIFISHORT_GI_E                                     = 1,
  WIFISHORT_GI_AMBIGUITY_E                           = 3
} vht_sig_a_info__gi_setting__e;                     ///< gi_setting Enum Type

//-----------------------------------------------------------------------------
// "wbm_release_ring" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRELEASE_SOURCE_TQM_E                           = 0,
  WIFIRELEASE_SOURCE_RXDMA_E                         = 1,
  WIFIRELEASE_SOURCE_REO_E                           = 2,
  WIFIRELEASE_SOURCE_FW_E                            = 3,
  WIFIRELEASE_SOURCE_SW_E                            = 4
} wbm_release_ring__release_source_module__e;        ///< release_source_module Enum Type
typedef enum {
  WIFIPUT_IN_IDLE_LIST_E                             = 0,
  WIFIRELEASE_MSDU_LIST_E                            = 1,
  WIFIPUT_IN_IDLE_LIST_EXPANDED_E                    = 2
} wbm_release_ring__bm_action__e;                    ///< bm_action Enum Type
typedef enum {
  WIFIMSDU_REL_BUFFER_E                              = 0,
  WIFIMSDU_LINK_DESCRIPTOR_E                         = 1,
  WIFIMPDU_LINK_DESCRIPTOR_E                         = 2,
  WIFIMSDU_EXT_DESCRIPTOR_E                          = 3,
  WIFIQUEUE_EXT_DESCRIPTOR_E                         = 4
} wbm_release_ring__buffer_or_desc_type__e;          ///< buffer_or_desc_type Enum Type
typedef enum {
  WIFITQM_RR_FRAME_ACKED_E                           = 0,
  WIFITQM_RR_REM_CMD_REM_E                           = 1,
  WIFITQM_RR_REM_CMD_TX_E                            = 2,
  WIFITQM_RR_REM_CMD_NOTX_E                          = 3,
  WIFITQM_RR_REM_CMD_AGED_E                          = 4,
  WIFITQM_FW_REASON1_E                               = 5,
  WIFITQM_FW_REASON2_E                               = 6,
  WIFITQM_FW_REASON3_E                               = 7,
  WIFITQM_RR_REM_CMD_DISABLE_QUEUE_E                 = 8
} wbm_release_ring__tqm_release_reason__e;           ///< tqm_release_reason Enum Type
typedef reo_entrance_ring__rxdma_push_reason__e      wbm_release_ring__rxdma_push_reason__e;
typedef reo_entrance_ring__rxdma_error_code__e       wbm_release_ring__rxdma_error_code__e;
typedef reo_destination_ring__reo_push_reason__e     wbm_release_ring__reo_push_reason__e;
typedef reo_destination_ring__reo_error_code__e      wbm_release_ring__reo_error_code__e;

//-----------------------------------------------------------------------------
// "macrx_abort_request_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMACRX_ABORT_SW_INITIATED_E                     = 0,
  WIFIMACRX_ABORT_OBSS_RECEPTION_E                   = 1,
  WIFIMACRX_ABORT_OTHER_E                            = 2,
  WIFIMACRX_ABORT_SW_INITIATED_CHANNEL_SWITCH_E      = 3,
  WIFIMACRX_ABORT_SW_INITIATED_POWER_SAVE_E          = 4,
  WIFIMACRX_ABORT_TOO_MUCH_BAD_DATA_E                = 5
} macrx_abort_request_info__macrx_abort_reason__e;   ///< macrx_abort_reason Enum Type

//-----------------------------------------------------------------------------
// "phytx_abort_request_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_PHYTX_ERROR_REPORTED_E                      = 0,
  WIFIERROR_TX_UNDERRUN_E                            = 1,
  WIFIERROR_TX_CHAIN_MASK_ZERO_E                     = 2,
  WIFIERROR_TX_EXTRA_SYM_MISMATCH_E                  = 3,
  WIFIERROR_TX_VHT_LENGTH_NOT_MULTIPLE_OF_3_E        = 4,
  WIFIERROR_TX_BW_IS_GT_DYN_BW_E                     = 5,
  WIFIERROR_TX_11B_RATE_ILLEGAL_E                    = 6,
  WIFIERROR_TX_LEGACY_RATE_ILLEGAL_E                 = 7,
  WIFIERROR_TX_HT_RATE_ILLEGAL_E                     = 8,
  WIFIERROR_TX_VHT_RATE_ILLEGAL_E                    = 9,
  WIFIERROR_TX_TPC_MISS_E                            = 10,
  WIFIERROR_MAC_TX_ABORT_E                           = 11,
  WIFIERROR_MAC_RF_ONLY_ABORT_E                      = 12,
  WIFIERROR_UNSUPPORTED_CBF_E                        = 13,
  WIFIERROR_CV_STATIC_BANDWIDTH_MISMATCH_E           = 14,
  WIFIERROR_CV_DYNAMIC_BANDWIDTH_MISMATCH_E          = 15,
  WIFIERROR_CV_UNSUPPORTED_NSS_TOTAL_E               = 16,
  WIFIERROR_NSS_BF_PARAMS_MISMATCH_E                 = 17,
  WIFIERROR_TXBF_FAIL_E                              = 18,
  WIFIERROR_ILLEGAL_NSS_E                            = 19,
  WIFIERROR_OTP_TXBF_E                               = 20,
  WIFIERROR_ILLEGAL_80P80_CONFIG_E                   = 21,
  WIFIERROR_CV_INDEX_ASSIGN_OVERLOAD_E               = 22,
  WIFIERROR_CV_INDEX_DELETE_E                        = 23,
  WIFIERROR_TX_HE_RATE_ILLEGAL_E                     = 24
} phytx_abort_request_info__phytx_abort_reason__e;   ///< phytx_abort_reason Enum Type

//-----------------------------------------------------------------------------
// "coex_rx_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIPPDU_START_E                                   = 0,
  WIFIFIRST_MPDU_FCS_PASS_E                          = 1,
  WIFIPPDU_END_E                                     = 2,
  WIFIPPDU_END_DUE_TO_PHY_NAP_E                      = 3
} coex_rx_status__rx_mac_frame_status__e;            ///< rx_mac_frame_status Enum Type
typedef pdg_response_rate_setting__alt_bw__e         coex_rx_status__rx_bw__e;
typedef addr_search_entry__ftm_pe_nss__e             coex_rx_status__rx_nss__e;
typedef enum {
  WIFIDATA_E                                         = 0,
  WIFIMANAGEMENT_E                                   = 1,
  WIFIBEACON_E                                       = 2,
  WIFICONTROL_E                                      = 3,
  WIFICONTROL_RESPONSE_E                             = 4,
  WIFIOTHERS_E                                       = 5
} coex_rx_status__rx_type__e;                        ///< rx_type Enum Type

//-----------------------------------------------------------------------------
// "coex_tx_req" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             coex_tx_req__nss__e;
typedef pdg_response_rate_setting__alt_bw__e         coex_tx_req__bw__e;
typedef addr_search_entry__ftm_pe_nss__e             coex_tx_req__alt_nss__e;
typedef pdg_response_rate_setting__alt_bw__e         coex_tx_req__alt_bw__e;
typedef l_sig_a_info__pkt_type__e                    coex_tx_req__wlan_pkt_type__e;
typedef enum {
  WIFICXC_FES_PROTECTION_FRAME_E                     = 0,
  WIFICXC_FES_AFTER_PROTECTION_E                     = 1,
  WIFICXC_FES_ONLY_E                                 = 2,
  WIFICXC_RESPONSE_FRAME_E                           = 3
} coex_tx_req__coex_tx_reason__e;                    ///< coex_tx_reason Enum Type
typedef enum {
  WIFIRESP_NON_11AH_ACK_E                            = 0,
  WIFIRESP_NON_11AH_BA_E                             = 1,
  WIFIRESP_NON_11AH_CTS_E                            = 2,
  WIFIRESP_AH_NDP_CTS_E                              = 3,
  WIFIRESP_AH_NDP_ACK_E                              = 4,
  WIFIRESP_AH_NDP_BA_E                               = 5,
  WIFIRESP_AH_NDP_MOD_ACK_E                          = 6,
  WIFIRESP_AH_NORMAL_ACK_E                           = 7,
  WIFIRESP_AH_NORMAL_BA_E                            = 8,
  WIFIRESP_RTT_ACK_E                                 = 9,
  WIFIRESP_CBF_RESPONSE_E                            = 10,
  WIFIRESP_MBA_E                                     = 11,
  WIFIRESP_TRIGGER_RESPONSE_BASIC_E                  = 12,
  WIFIRESP_TRIGGER_RESPONSE_BUF_SIZE_E               = 13,
  WIFIRESP_TRIGGER_RESPONSE_BRPOLL_E                 = 14,
  WIFIRESP_TRIGGER_RESPONSE_CTS_E                    = 15,
  WIFIRESP_TRIGGER_RESPONSE_OTHER_E                  = 16
} coex_tx_req__response_frame_type__e;               ///< response_frame_type Enum Type

//-----------------------------------------------------------------------------
// "coex_tx_resp" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             coex_tx_resp__coex_based_nss__e;
typedef pdg_response_rate_setting__alt_bw__e         coex_tx_resp__coex_based_tx_bw__e;
typedef enum {
  WIFICOEX_RESP_NOACK_E                              = 0,
  WIFICOEX_RESP_ACK_E                                = 1
} coex_tx_resp__coex_ack_noack__e;                   ///< coex_ack_noack Enum Type
typedef enum {
  WIFINOACK_HIGHER_PRIORITY_BT_ONGOING_E             = 0,
  WIFINOACK_OTHER_E                                  = 1
} coex_tx_resp__coex_noack_reason__e;                ///< coex_noack_reason Enum Type

//-----------------------------------------------------------------------------
// "coex_tx_status" enums
//-----------------------------------------------------------------------------
typedef pdg_response_rate_setting__alt_bw__e         coex_tx_status__tx_bw__e;
typedef enum {
  WIFIFES_TX_START_E                                 = 0,
  WIFIFES_TX_END_E                                   = 1,
  WIFIFES_END_E                                      = 2,
  WIFIRESPONSE_TX_START_E                            = 3,
  WIFIRESPONSE_TX_END_E                              = 4,
  WIFINO_TX_ONGOING_E                                = 5
} coex_tx_status__tx_status_reason__e;               ///< tx_status_reason Enum Type

//-----------------------------------------------------------------------------
// "expected_response" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_RESPONSE_EXPECTED_E                         = 0,
  WIFIACK_EXPECTED_E                                 = 1,
  WIFIBA_64_BITMAP_EXPECTED_E                        = 2,
  WIFIBA_256_EXPECTED_E                              = 3,
  WIFIACTIONNOACK_EXPECTED_E                         = 4,
  WIFIACK_BA_EXPECTED_E                              = 5,
  WIFICTS_EXPECTED_E                                 = 6,
  WIFIACK_DATA_EXPECTED_E                            = 7,
  WIFINDP_ACK_EXPECTED_E                             = 8,
  WIFINDP_MODIFIED_ACK_E                             = 9,
  WIFINDP_BA_EXPECTED_E                              = 10,
  WIFINDP_CTS_EXPECTED_E                             = 11,
  WIFINDP_ACK_OR_NDP_MODIFIED_ACK_EXPECTED_E         = 12,
  WIFIUL_MU_BA_EXPECTED_E                            = 13,
  WIFIUL_MU_BA_AND_DATA_EXPECTED_E                   = 14,
  WIFIUL_MU_CBF_EXPECTED_E                           = 15,
  WIFIUL_MU_FRAMES_EXPECTED_E                        = 16,
  WIFIANY_RESPONSE_TO_THIS_DEVICE_E                  = 17,
  WIFIANY_RESPONSE_ACCEPTED_E                        = 18,
  WIFIFRAMELESS_PHYRX_RESPONSE_ACCEPTED_E            = 19
} expected_response__expected_response_type__e;      ///< expected_response_type Enum Type
typedef enum {
  WIFINONE_E                                         = 0,
  WIFISU_BA_E                                        = 1,
  WIFIMU_BA_E                                        = 2,
  WIFIRESPONSE_TO_RESPONSE_CMD_E                     = 3
} expected_response__response_to_response__e;        ///< response_to_response Enum Type

//-----------------------------------------------------------------------------
// "mactx_coex_phy_ctrl" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIFALSE_E                                        = 0,
  WIFITRUE_E                                         = 1
} mactx_coex_phy_ctrl__cca_threshold_index_valid__e; ///< cca_threshold_index_valid Enum Type
typedef mactx_coex_phy_ctrl__cca_threshold_index_valid__e mactx_coex_phy_ctrl__shared_ant_deweight_valid__e;
typedef enum {
  WIFIWLAN_PRIORITY_E                                = 0,
  WIFIEQUAL_PRIORITY_E                               = 1,
  WIFIBT_PRIORITY_E                                  = 2
} mactx_coex_phy_ctrl__wlan_bt_priority__e;          ///< wlan_bt_priority Enum Type

//-----------------------------------------------------------------------------
// "mactx_phy_desc" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_BF_E                                        = 0,
  WIFILEGACY_BF_E                                    = 1,
  WIFISU_BF_E                                        = 2,
  WIFIMU_BF_E                                        = 3
} mactx_phy_desc__bf_type__e;                        ///< bf_type Enum Type
typedef enum {
  WIFINO_SIFS_TIMING_E                               = 0,
  WIFISIFS_TIMING_DESIRED_E                          = 1,
  WIFISIFS_TIMING_MANDATED_E                         = 2
} mactx_phy_desc__wait_sifs__e;                      ///< wait_sifs Enum Type
typedef enum {
  WIFISHORT_PREAMBLE_E                               = 0,
  WIFILONG_PREAMBLE_E                                = 1
} mactx_phy_desc__dot11b_preamble_type__e;           ///< dot11b_preamble_type Enum Type
typedef l_sig_a_info__pkt_type__e                    mactx_phy_desc__pkt_type__e;
typedef enum {
  WIFISU_TRANSMISSION_E                              = 0,
  WIFIMU_TRANSMISSION_E                              = 1,
  WIFIMU_SU_TRANSMISSION_E                           = 2
} mactx_phy_desc__su_or_mu__e;                       ///< su_or_mu Enum Type
typedef enum {
  WIFIMU_MIMO_TRANSMISSION_E                         = 0,
  WIFIMU_OFDMA_TRANSMISSION_E                        = 1
} mactx_phy_desc__mu_type__e;                        ///< mu_type Enum Type
typedef pdg_response_rate_setting__alt_bw__e         mactx_phy_desc__bandwidth__e;
typedef enum {
  WIFIRU26_E                                         = 0,
  WIFIRU52_E                                         = 1,
  WIFIRU106_E                                        = 2,
  WIFIRUFULLBW_E                                     = 3
} mactx_phy_desc__ru_size__e;                        ///< ru_size Enum Type
typedef enum {
  WIFIMEASURE_DIS_E                                  = 0,
  WIFIMEASURE_EN_E                                   = 1
} mactx_phy_desc__measure_power__e;                  ///< measure_power Enum Type
typedef addr_search_entry__ftm_pe_nss__e             mactx_phy_desc__heavy_clip_nss__e;
typedef enum {
  WIFIDOWNLINK_E                                     = 0,
  WIFIUPLINK_E                                       = 1
} mactx_phy_desc__ul_flag__e;                        ///< ul_flag Enum Type
typedef enum {
  WIFINON_TRIGERRED_E                                = 0,
  WIFIIS_TRIGGERED_E                                 = 1
} mactx_phy_desc__triggered__e;                      ///< triggered Enum Type
typedef pdg_response_rate_setting__alt_bw__e         mactx_phy_desc__ap_pkt_bw__e;
typedef addr_search_entry__ftm_pe_nss__e             mactx_phy_desc__nss__e;
typedef enum {
  WIFICLPC_OFF_E                                     = 0,
  WIFICLPC_ON_E                                      = 1
} mactx_phy_desc__clpc_enable__e;                    ///< clpc_enable Enum Type
typedef enum {
  WIFIRX_CHAIN_MASK_IS_NOT_VALID_E                   = 0,
  WIFIRX_CHAIN_MASK_IS_VALID_E                       = 1
} mactx_phy_desc__rx_chain_mask_valid__e;            ///< rx_chain_mask_valid Enum Type
typedef enum {
  WIFIANT_SEL_IS_NOT_VALID_E                         = 0,
  WIFIANT_SEL_IS_VALID_E                             = 1
} mactx_phy_desc__ant_sel_valid__e;                  ///< ant_sel_valid Enum Type
typedef enum {
  WIFIANTENNA_0_E                                    = 0,
  WIFIANTENNA_1_E                                    = 1
} mactx_phy_desc__ant_sel__e;                        ///< ant_sel Enum Type
typedef prot_rate_setting__sgi__e                    mactx_phy_desc__cp_setting__e;
typedef enum {
  WIFIHE_SUBTYPE_SU_E                                = 0,
  WIFIHE_SUBTYPE_TRIG_E                              = 1,
  WIFIHE_SUBTYPE_MU_E                                = 2,
  WIFIHE_SUBTYPE_EXT_SU_E                            = 3
} mactx_phy_desc__he_ppdu_subtype__e;                ///< he_ppdu_subtype Enum Type
typedef enum {
  WIFIRU_26_E                                        = 0,
  WIFIRU_52_E                                        = 1,
  WIFIRU_106_E                                       = 2,
  WIFIRU_242_E                                       = 3,
  WIFIRU_484_E                                       = 4,
  WIFIRU_996_E                                       = 5,
  WIFIRU_1992_E                                      = 6,
  WIFIRU_FULLBW_E                                    = 7
} mactx_phy_desc__ru_size_updated__e;                ///< ru_size_updated Enum Type
typedef ppdu_rate_setting__ltf_size__e               mactx_phy_desc__ltf_size__e;
typedef mactx_phy_desc__ru_size_updated__e           mactx_phy_desc__ru_size_updated_v2__e;

//-----------------------------------------------------------------------------
// "mactx_pre_phy_desc" enums
//-----------------------------------------------------------------------------
typedef pdg_response_rate_setting__alt_bw__e         mactx_pre_phy_desc__transmit_bw__e;
typedef enum {
  WIFITX_DURATION_RANGE_0_E                          = 0,
  WIFITX_DURATION_RANGE_1_E                          = 1,
  WIFITX_DURATION_RANGE_2_E                          = 2,
  WIFITX_DURATION_RANGE_3_E                          = 3,
  WIFITX_DURATION_RANGE_UNKNOWN_E                    = 4
} mactx_pre_phy_desc__estimated_transmit_duration__e; ///< estimated_transmit_duration Enum Type
typedef pdg_response_rate_setting__dpd_enable__e     mactx_pre_phy_desc__dpd_enable__e;

//-----------------------------------------------------------------------------
// "mactx_user_desc_common" enums
//-----------------------------------------------------------------------------
typedef ppdu_rate_setting__ltf_size__e               mactx_user_desc_common__ltf_size__e;
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e mactx_user_desc_common__packet_extension_a_factor__e;
typedef enum {
  WIFIPACKET_EXT_0_E                                 = 0,
  WIFIPACKET_EXT_4_E                                 = 1,
  WIFIPACKET_EXT_8_E                                 = 2,
  WIFIPACKET_EXT_12_E                                = 3,
  WIFIPACKET_EXT_16_E                                = 4
} mactx_user_desc_common__packet_extension__e;       ///< packet_extension Enum Type
typedef addr_search_entry__ftm_pe_nss__e             mactx_user_desc_common__pe_nss__e;

//-----------------------------------------------------------------------------
// "ofdma_trigger_details" enums
//-----------------------------------------------------------------------------
typedef received_trigger_info_details__ax_trigger_source__e ofdma_trigger_details__ax_trigger_source__e;
typedef enum {
  WIFIDOT11AX_DIRECT_TRIGGER_FRAME_E                 = 0,
  WIFIDOT11AX_WILDCARD_TRIGGER_FRAME_E               = 1,
  WIFIDOT11AX_USASSOC_WILDCARD_TRIGGER_FRAME_E       = 2
} ofdma_trigger_details__rx_trigger_frame_user_source__e; ///< rx_trigger_frame_user_source Enum Type
typedef enum {
  WIFI_E                                             = 0
} ofdma_trigger_details__received_bandwidth__e;      ///< received_bandwidth Enum Type
typedef he_sig_a_mu_dl_info__transmit_bw__e          ofdma_trigger_details__mu_dl_sig_a_bw__e;
typedef enum {
  WIFIAX_TRIGGER_BASIC_E                             = 0,
  WIFIAX_TRIGGER_BRPOLL_E                            = 1,
  WIFIAX_TRIGGER_MU_BAR_E                            = 2,
  WIFIAX_TRIGGER_MU_RTS_E                            = 3,
  WIFIAX_TRIGGER_BUFFER_SIZE_E                       = 4,
  WIFIAX_TRIGGER_GCR_MU_BAR_E                        = 5,
  WIFIAX_TRIGGER_BQRP_E                              = 6,
  WIFIAX_TRIGGER_NDP_FB_REPORT_POLL_E                = 7,
  WIFIAX_TRIGGER_RESERVED_8_E                        = 8,
  WIFIAX_TRIGGER_RESERVED_9_E                        = 9,
  WIFIAX_TRIGGER_RESERVED_10_E                       = 10,
  WIFIAX_TRIGGER_RESERVED_11_E                       = 11,
  WIFIAX_TRIGGER_RESERVED_12_E                       = 12,
  WIFIAX_TRIGGER_RESERVED_13_E                       = 13,
  WIFIAX_TRIGGER_RESERVED_14_E                       = 14,
  WIFIAX_TRIGGER_RESERVED_15_E                       = 15
} ofdma_trigger_details__trigger_type__e;            ///< trigger_type Enum Type
typedef he_sig_a_su_info__transmit_bw__e             ofdma_trigger_details__bandwidth__e;
typedef enum {
  WIFITRIG_ONEX_LTF_1_6CP_E                          = 0,
  WIFITRIG_TWOX_LTF_1_6CP_E                          = 1,
  WIFITRIG_FOURX_LTF_3_2CP_E                         = 2
} ofdma_trigger_details__cp_ltf_size__e;             ///< cp_ltf_size Enum Type
typedef he_sig_a_mu_dl_info__packet_extension_a_factor__e ofdma_trigger_details__packet_extension_a_factor__e;
typedef enum {
  WIFISPACING_MULTIPLIER_IS_1_E                      = 0,
  WIFISPACING_MULTIPLIER_IS_2_E                      = 1,
  WIFISPACING_MULTIPLIER_IS_4_E                      = 2,
  WIFISPACING_MULTIPLIER_IS_8_E                      = 3
} ofdma_trigger_details__mpdu_mu_spacing_factor__e;  ///< mpdu_mu_spacing_factor Enum Type
typedef enum {
  WIFIPREFERED_AC_IS_BK_E                            = 0,
  WIFIPREFERED_AC_IS_BE_E                            = 1,
  WIFIPREFERED_AC_IS_VI_E                            = 2,
  WIFIPREFERED_AC_IS_VO_E                            = 3
} ofdma_trigger_details__prefered_ac__e;             ///< prefered_ac Enum Type

//-----------------------------------------------------------------------------
// "pcu_ppdu_setup_init" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_PROTECTION_E                                = 0,
  WIFIRTS_LEGACY_E                                   = 1,
  WIFIRTS_11AC_STATIC_BW_E                           = 2,
  WIFIRTS_11AC_DYNAMIC_BW_E                          = 3,
  WIFICTS2SELF_E                                     = 4,
  WIFIRTS_11AH_STATIC_BW_E                           = 5,
  WIFIRTS_11AH_DYNAMIC_BW_E                          = 6,
  WIFI11AH_CTS2SELF_E                                = 7
} pcu_ppdu_setup_init__medium_prot_type__e;          ///< medium_prot_type Enum Type
typedef expected_response__expected_response_type__e pcu_ppdu_setup_init__response_type__e;
typedef expected_response__response_to_response__e   pcu_ppdu_setup_init__response_to_response__e;
typedef enum {
  WIFIMU_BA_FIXED_USER_ORDER_E                       = 0,
  WIFIMU_BA_OPTIMIZED_USER_ORDER_E                   = 1,
  WIFIMU_BA_FULLY_OPTIMIZED_E                        = 2,
  WIFIMU_BA_FULLY_OPTIMIZED_MULTI_TID_E              = 3
} pcu_ppdu_setup_init__mba_user_order__e;            ///< mba_user_order Enum Type
typedef enum {
  WIFINON_11AH_ACK_E                                 = 0,
  WIFINON_11AH_BA_E                                  = 1,
  WIFINON_11AH_CTS_E                                 = 2,
  WIFIAH_NDP_CTS_E                                   = 3,
  WIFIAH_NDP_ACK_E                                   = 4,
  WIFIAH_NDP_BA_E                                    = 5,
  WIFIAH_NDP_MOD_ACK_E                               = 6,
  WIFIAH_NORMAL_ACK_E                                = 7,
  WIFIAH_NORMAL_BA_E                                 = 8,
  WIFIRTT_ACK_E                                      = 9,
  WIFICBF_RESPONSE_E                                 = 10,
  WIFIMBA_E                                          = 11
} pcu_ppdu_setup_init__r2r_response_frame_type__e;   ///< r2r_response_frame_type Enum Type
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_init__r2r_bw20__e;
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_init__r2r_bw40__e;
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_init__r2r_bw80__e;
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_init__r2r_bw160__e;

//-----------------------------------------------------------------------------
// "pcu_ppdu_setup_start" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIFES_PROTECTION_FRAME_E                         = 0,
  WIFIFES_AFTER_PROTECTION_E                         = 1,
  WIFIFES_ONLY_E                                     = 2,
  WIFIRESPONSE_FRAME_E                               = 3,
  WIFITRIG_RESPONSE_FRAME_E                          = 4
} pcu_ppdu_setup_start__setup_start_reason__e;       ///< setup_start_reason Enum Type
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_start__tx_bw__e;
typedef pdg_response_rate_setting__alt_bw__e         pcu_ppdu_setup_start__txpcu_requested_tx_bw__e;
typedef l_sig_a_info__pkt_type__e                    pcu_ppdu_setup_start__pkt_type__e;
typedef pdg_response_rate_setting__dpd_enable__e     pcu_ppdu_setup_start__dpd_enable__e;
typedef mactx_pre_phy_desc__estimated_transmit_duration__e pcu_ppdu_setup_start__estimated_transmit_duration__e;

//-----------------------------------------------------------------------------
// "pdg_fes_setup" enums
//-----------------------------------------------------------------------------
typedef mactx_phy_desc__ul_flag__e                   pdg_fes_setup__dl_ul_flag__e;
typedef mactx_phy_desc__clpc_enable__e               pdg_fes_setup__clpc_en__e;
typedef mactx_phy_desc__measure_power__e             pdg_fes_setup__measure_power__e;
typedef enum {
  WIFISET_DUR_ZERO_E                                 = 0,
  WIFIPROTECT_ENTIRE_FES_E                           = 1,
  WIFIPROTECT_ENTIRE_TXOP_E                          = 2
} pdg_fes_setup__duration_field_ctrl__e;             ///< duration_field_ctrl Enum Type
typedef enum {
  WIFILEGACY_MODE_E                                  = 0,
  WIFIGEN_4BIT_STATIC_E                              = 1,
  WIFIGEN_4BIT_DYNAMIC_E                             = 2,
  WIFIGEN_5BIT_E                                     = 3,
  WIFIGEN_7BIT_E                                     = 4
} pdg_fes_setup__hw_mode_mprot_scrambler_seed_mode__e; ///< hw_mode_mprot_scrambler_seed_mode Enum Type
typedef pdg_fes_setup__hw_mode_mprot_scrambler_seed_mode__e pdg_fes_setup__hw_mode_ppdu_scrambler_seed_mode__e;
typedef mactx_phy_desc__rx_chain_mask_valid__e       pdg_fes_setup__rx_chain_mask_valid__e;
typedef mactx_phy_desc__ant_sel_valid__e             pdg_fes_setup__ant_sel_valid__e;
typedef enum {
  WIFINO_MPDU_INFO_MODE_BASIC_E                      = 0,
  WIFINO_MPDU_INFO_MODE_ENHANCED_E                   = 1
} pdg_fes_setup__max_txop_with_no_mpdu_info_mode__e; ///< max_txop_with_no_mpdu_info_mode Enum Type
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__pe_nss__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__prot_pe_nss__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__coex_ofdma_mimo_nss_bw20__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__coex_ofdma_mimo_nss_bw40__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__coex_ofdma_mimo_nss_bw80__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_fes_setup__coex_ofdma_mimo_nss_bw160__e;

//-----------------------------------------------------------------------------
// "pdg_response" enums
//-----------------------------------------------------------------------------
typedef pdg_response_rate_setting__alt_bw__e         pdg_response__bandwidth__e;
typedef pcu_ppdu_setup_init__r2r_response_frame_type__e pdg_response__response_frame_type__e;

//-----------------------------------------------------------------------------
// "pdg_sw_mode_bw_start" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFI20_MHZ_SW_TLVS_E                               = 0,
  WIFI40_MHZ_SW_TLVS_E                               = 1,
  WIFI80_MHZ_SW_TLVS_E                               = 2,
  WIFI160_MHZ_SW_TLVS_E                              = 3
} pdg_sw_mode_bw_start__transmit_bw_sw_tlvs__e;      ///< transmit_bw_sw_tlvs Enum Type

//-----------------------------------------------------------------------------
// "pdg_tx_req" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITX_FES_PROTECTION_FRAME_E                      = 0,
  WIFITX_FES_AFTER_PROTECTION_E                      = 1,
  WIFITX_FES_ONLY_E                                  = 2
} pdg_tx_req__tx_reason__e;                          ///< tx_reason Enum Type
typedef pdg_response_rate_setting__alt_bw__e         pdg_tx_req__req_bw__e;

//-----------------------------------------------------------------------------
// "pdg_wait_for_mac_request" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMAC_REQUEST_PDG_TX_REQ_E                       = 0,
  WIFIMAC_REQUEST_COEX_TX_RESP_E                     = 1
} pdg_wait_for_mac_request__mac_request_type__e;     ///< mac_request_type Enum Type

//-----------------------------------------------------------------------------
// "pdg_wait_for_phy_request" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREQUEST_L_SIG_B_E                              = 0,
  WIFIREQUEST_L_SIG_A_E                              = 1,
  WIFIREQUEST_USER_DESC_E                            = 2,
  WIFIREQUEST_HT_SIG_E                               = 3,
  WIFIREQUEST_VHT_SIG_A_E                            = 4,
  WIFIREQUEST_VHT_SIG_B_E                            = 5,
  WIFIREQUEST_TX_SERVICE_E                           = 6,
  WIFIREQUEST_HE_SIG_A_E                             = 7,
  WIFIREQUEST_HE_SIG_B_E                             = 8
} pdg_wait_for_phy_request__phy_request_type__e;     ///< phy_request_type Enum Type

//-----------------------------------------------------------------------------
// "phyrx_cbf_read_request_ack" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFICRRA_OK_E                                      = 0,
  WIFICRRA_ERR_NOT_AVAILABLE_E                       = 1,
  WIFICRRA_ERR_NOT_COMPLETE_E                        = 2,
  WIFICRRA_ERR_OTHER_E                               = 3
} phyrx_cbf_read_request_ack__cbf_request_ack_status__e; ///< cbf_request_ack_status Enum Type

//-----------------------------------------------------------------------------
// "phyrx_rssi_legacy" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRECEPTION_IS_UPLINK_OFDMA_E                    = 0,
  WIFIRECEPTION_IS_UPLINK_MIMO_E                     = 1,
  WIFIRECEPTION_IS_OTHER_E                           = 2,
  WIFIRECEPTION_IS_FRAMELESS_E                       = 3
} phyrx_rssi_legacy__reception_type__e;              ///< reception_type Enum Type
typedef receive_user_info__receive_bandwidth__e      phyrx_rssi_legacy__receive_bandwidth__e;

//-----------------------------------------------------------------------------
// "received_response_info" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRECEPTION_IS_SU_E                              = 0,
  WIFIRECEPTION_IS_MU_E                              = 1
} received_response_info__su_or_uplink_mu_reception__e; ///< su_or_uplink_mu_reception Enum Type
typedef enum {
  WIFIUPLINK_TYPE_MU_MIMO_E                          = 0,
  WIFIUPLINK_TYPE_MU_OFDMA_E                         = 1
} received_response_info__uplink_mu_type__e;         ///< uplink_mu_type Enum Type
typedef enum {
  WIFI20MHZ_CBF_E                                    = 0,
  WIFI40MHZ_CBF_E                                    = 1,
  WIFI80MHZ_CBF_E                                    = 2,
  WIFI160MHZ_CBF_E                                   = 3
} received_response_info__cbf_bw__e;                 ///< cbf_bw Enum Type
typedef pdg_response_rate_setting__alt_bw__e         received_response_info__agc_cbw__e;
typedef pdg_response_rate_setting__alt_bw__e         received_response_info__service_cbw__e;
typedef enum {
  WIFINC_1_E                                         = 0,
  WIFINC_2_E                                         = 1,
  WIFINC_3_E                                         = 2,
  WIFINC_4_E                                         = 3,
  WIFINC_5_E                                         = 4,
  WIFINC_6_E                                         = 5,
  WIFINC_7_E                                         = 6,
  WIFINC_8_E                                         = 7
} received_response_info__cbf_nc_index__e;           ///< cbf_nc_index Enum Type
typedef enum {
  WIFINR_1_E                                         = 0,
  WIFINR_2_E                                         = 1,
  WIFINR_3_E                                         = 2,
  WIFINR_4_E                                         = 3,
  WIFINR_5_E                                         = 4,
  WIFINR_6_E                                         = 5,
  WIFINR_7_E                                         = 6,
  WIFINR_8_E                                         = 7
} received_response_info__cbf_nr_index__e;           ///< cbf_nr_index Enum Type
typedef enum {
  WIFISU_E                                           = 0,
  WIFIMU_E                                           = 1
} received_response_info__cbf_feedback_type__e;      ///< cbf_feedback_type Enum Type

//-----------------------------------------------------------------------------
// "reo_descriptor_threshold_reached_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREO_DESC_COUNTER0_THRESHOLD_E                  = 0,
  WIFIREO_DESC_COUNTER1_THRESHOLD_E                  = 1,
  WIFIREO_DESC_COUNTER2_THRESHOLD_E                  = 2,
  WIFIREO_DESC_COUNTER_SUM_THRESHOLD_E               = 3
} reo_descriptor_threshold_reached_status__threshold_index__e; ///< threshold_index Enum Type

//-----------------------------------------------------------------------------
// "reo_get_queue_stats" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_CLEAR_E                                     = 0,
  WIFICLEAR_THE_STATS_E                              = 1
} reo_get_queue_stats__clear_stats__e;               ///< clear_stats Enum Type

//-----------------------------------------------------------------------------
// "reo_unblock_cache" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIUNBLOCK_RESOURCE_INDEX_E                       = 0,
  WIFIUNBLOCK_CACHE_E                                = 1
} reo_unblock_cache__unblock_type__e;                ///< unblock_type Enum Type

//-----------------------------------------------------------------------------
// "reo_unblock_cache_status" enums
//-----------------------------------------------------------------------------
typedef reo_unblock_cache__unblock_type__e           reo_unblock_cache_status__unblock_type__e;

//-----------------------------------------------------------------------------
// "reo_update_rx_reo_queue" enums
//-----------------------------------------------------------------------------
typedef rx_reo_queue__pn_size__e                     reo_update_rx_reo_queue__pn_size__e;

//-----------------------------------------------------------------------------
// "response_end_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIRESPONSE_OK_E                                  = 0,
  WIFIRESPONSE_COEX_SOFT_ABORT_E                     = 1,
  WIFIRESPONSE_PHY_ERR_E                             = 2,
  WIFIRESPONSE_FLUSH_RECEIVED_E                      = 3,
  WIFIRESPONSE_OTHER_ERR_E                           = 4
} response_end_status__response_transmit_status__e;  ///< response_transmit_status Enum Type
typedef enum {
  WIFISELFGEN_ACK_E                                  = 0,
  WIFISELFGEN_CTS_E                                  = 1,
  WIFISELFGEN_BA_E                                   = 2,
  WIFISELFGEN_MBA_E                                  = 3,
  WIFISELFGEN_CBF_E                                  = 4,
  WIFISELFGEN_OTHER_TRIG_RESPONSE_E                  = 5
} response_end_status__generated_response__e;        ///< generated_response Enum Type
typedef pdg_response_rate_setting__alt_bw__e         response_end_status__coex_based_tx_bw__e;
typedef enum {
  WIFINO_DATA_UNDERRUN_E                             = 0,
  WIFIDATA_UNDERRUN_BETWEEN_MPDU_E                   = 1,
  WIFIDATA_UNDERRUN_WITHIN_MPDU_E                    = 2
} response_end_status__data_underflow_warning__e;    ///< data_underflow_warning Enum Type
typedef enum {
  WIFINO_TX_TIMING_REQUEST_E                         = 0,
  WIFISUCCESSFUL_TX_TIMING_E                         = 1,
  WIFITX_TIMING_NOT_HONOURED_E                       = 2
} response_end_status__timing_status__e;             ///< timing_status Enum Type

//-----------------------------------------------------------------------------
// "response_start_status" enums
//-----------------------------------------------------------------------------
typedef response_end_status__generated_response__e   response_start_status__generated_response__e;

//-----------------------------------------------------------------------------
// "rx_frame_bitmap_req" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIBITMAP_REQ_USER_NUMBER_BASED_E                 = 0,
  WIFIBITMAP_REQ_SW_PEER_ID_BASED_E                  = 1
} rx_frame_bitmap_req__user_request_type__e;         ///< user_request_type Enum Type

//-----------------------------------------------------------------------------
// "rx_preamble" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    rx_preamble__pkt_type__e;
typedef enum {
  WIFIDIRECTION_UPLINK_RECEPTION_E                   = 0,
  WIFIDIRECTION_DOWNLINK_RECEPTION_E                 = 1
} rx_preamble__direction__e;                         ///< direction Enum Type

//-----------------------------------------------------------------------------
// "rx_response_required_info" enums
//-----------------------------------------------------------------------------
typedef received_response_info__su_or_uplink_mu_reception__e rx_response_required_info__su_or_uplink_mu_reception__e;
typedef mactx_phy_desc__wait_sifs__e                 rx_response_required_info__wait_sifs__e;
typedef l_sig_a_info__pkt_type__e                    rx_response_required_info__pkt_type__e;
typedef pdg_response_rate_setting__sgi__e            rx_response_required_info__sgi__e;
typedef enum {
  WIFINO_RESPONSE_E                                  = 0,
  WIFINDP_RESPONSE_E                                 = 1,
  WIFINORMAL_RESPONSE_E                              = 2,
  WIFILONG_RESPONSE_E                                = 3
} rx_response_required_info__response_indication__e; ///< response_indication Enum Type
typedef pdg_response_rate_setting__alt_bw__e         rx_response_required_info__agc_cbw__e;
typedef pdg_response_rate_setting__alt_bw__e         rx_response_required_info__service_cbw__e;
typedef pdg_response_rate_setting__alt_bw__e         rx_response_required_info__ht_vht_sig_cbw__e;
typedef pdg_response_rate_setting__alt_bw__e         rx_response_required_info__cts_cbw__e;
typedef he_sig_a_mu_ul_info__format_indication__e    rx_response_required_info__dot11ax_received_format_indication__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     rx_response_required_info__dot11ax_received_dl_ul_flag__e;
typedef pdg_response_rate_setting__sgi__e            rx_response_required_info__dot11ax_received_cp_size__e;
typedef ppdu_rate_setting__ltf_size__e               rx_response_required_info__dot11ax_received_ltf_size__e;
typedef addr_search_entry__ftm_pe_nss__e             rx_response_required_info__ftm_pe_nss__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     rx_response_required_info__dot11ax_dl_ul_flag__e;

//-----------------------------------------------------------------------------
// "rx_ring_mask" enums
//-----------------------------------------------------------------------------
typedef rxpt_classify_info__rxdma0_source_ring_selection__e rx_ring_mask__rxdma0_buffer_source_ring_mask__e;
typedef rxpt_classify_info__rxdma0_source_ring_selection__e rx_ring_mask__rxdma1_buffer_source_ring_mask__e;

//-----------------------------------------------------------------------------
// "rx_start_param" enums
//-----------------------------------------------------------------------------
typedef l_sig_a_info__pkt_type__e                    rx_start_param__pkt_type__e;

//-----------------------------------------------------------------------------
// "rx_trig_info" enums
//-----------------------------------------------------------------------------
typedef ofdma_trigger_details__rx_trigger_frame_user_source__e rx_trig_info__rx_trigger_frame_type__e;
typedef enum {
  WIFIOFDMA_ACK_FRAME_E                              = 0,
  WIFIOFDMA_BA_FRAMES_E                              = 1,
  WIFIOFDMA_DATA_FRAMES_E                            = 2,
  WIFIOFDMA_BA_DATA_FRAMES_E                         = 3
} rx_trig_info__trigger_resp_type__e;                ///< trigger_resp_type Enum Type

//-----------------------------------------------------------------------------
// "sch_wait_instr" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIWAIT_TIMER_E                                   = 0,
  WIFIWAIT_PROT_TX_END_E                             = 1,
  WIFIWAIT_PPDU_TX_END_E                             = 2,
  WIFIWAIT_FES_END_E                                 = 3
} sch_wait_instr__wait_type__e;                      ///< wait_type Enum Type

//-----------------------------------------------------------------------------
// "scheduler_command_status" enums
//-----------------------------------------------------------------------------
typedef phytx_abort_request_info__phytx_abort_reason__e scheduler_command_status__phytx_abort_reason__e;
typedef enum {
  WIFIBO_BASED_TRANSMIT_START_E                      = 0,
  WIFITRIGGER_BASED_TRANSMIT_START_E                 = 1,
  WIFISIFS_CONTINUATION_BASED_TRANSMIT_START_E       = 2
} scheduler_command_status__transmit_start_reason__e; ///< transmit_start_reason Enum Type
typedef enum {
  WIFISCH_CMD_RING_NUMBER0_E                         = 0,
  WIFISCH_CMD_RING_NUMBER1_E                         = 1,
  WIFISCH_CMD_RING_NUMBER2_E                         = 2,
  WIFISCH_CMD_RING_NUMBER3_E                         = 3,
  WIFISCH_CMD_RING_NUMBER4_E                         = 4,
  WIFISCH_CMD_RING_NUMBER5_E                         = 5,
  WIFISCH_CMD_RING_NUMBER6_E                         = 6,
  WIFISCH_CMD_RING_NUMBER7_E                         = 7,
  WIFISCH_CMD_RING_NUMBER8_E                         = 8,
  WIFISCH_CMD_RING_NUMBER9_E                         = 9,
  WIFISCH_CMD_RING_NUMBER10_E                        = 10,
  WIFISCH_CMD_RING_NUMBER11_E                        = 11,
  WIFISCH_CMD_RING_NUMBER12_E                        = 12,
  WIFISCH_CMD_RING_NUMBER13_E                        = 13,
  WIFISCH_CMD_RING_NUMBER14_E                        = 14,
  WIFISCH_CMD_RING_NUMBER15_E                        = 15
} scheduler_command_status__schedule_cmd_ring_id__e; ///< schedule_cmd_ring_id Enum Type
typedef scheduler_cmd__fes_control_mode__e           scheduler_command_status__fes_control_mode__e;
typedef enum {
  WIFINOT_STALLED_E                                  = 0,
  WIFISTALLED_ON_UNSUCCESSFUL_TRANSMIT_E             = 1,
  WIFISTALLED_ON_CBF_LOWER_BW_E                      = 2,
  WIFISTALLED_ON_EXCESSIVE_REQUEUE_E                 = 3,
  WIFISTALLED_AFTER_11AX_TRIGGER_HANDLING_E          = 4,
  WIFISTALLED_AFTER_DATA_NULL_RATIO_NOT_MET_E        = 5
} scheduler_command_status__cmd_ring_stall_status__e; ///< cmd_ring_stall_status Enum Type
typedef enum {
  WIFISUCCESS_E                                      = 0,
  WIFISU_RESPONSE_TYPE_MISMATCH_FAIL_E               = 1,
  WIFICBF_MIMO_CTRL_MISMATCH_FAIL_E                  = 2,
  WIFILIFESPAN_EXPIRY_FAIL_E                         = 3,
  WIFIMAX_SCHEDULE_RETRY_COUNT_FAIL_E                = 4,
  WIFIQCU_FILTERED_E                                 = 5,
  WIFILOWER_CBF_BW_FAIL_E                            = 6,
  WIFIFLUSH_RCVD_E                                   = 7,
  WIFIIBSS_NAN_STALL_E                               = 8,
  WIFISW_STALL_E                                     = 9,
  WIFIMISSING_MPDU_INFO_E                            = 10,
  WIFITRIGGER_RESP_TOO_LATE_RECEIVED_E               = 11,
  WIFILEGACY_TRIGGER_RESP_TOO_LATE_RECEIVED_E        = 12,
  WIFIMU_RESPONSE_TYPE_MISMATCH_FAIL_E               = 13,
  WIFIMU_RESPONSE_MPDU_NOT_VALID_FAIL_E              = 14,
  WIFISCH_CMD_RETRY_CONDITION_E                      = 15
} scheduler_command_status__sch_cmd_result__e;       ///< sch_cmd_result Enum Type
typedef enum {
  WIFINO_SIFS_BURST_E                                = 0,
  WIFISIFS_BURST_IN_PROGRESS_E                       = 1,
  WIFISIFS_BURST_END_E                               = 2,
  WIFISIFS_BURST_PAUSE_ON_FILTER_E                   = 3,
  WIFISIFS_BURST_SPLIT_E                             = 4,
  WIFISIFS_BURST_COMPLETE_IN_PIFS_E                  = 5,
  WIFISIFS_BURST_PAUSE_ON_REQUEUE_E                  = 6,
  WIFISIFS_BURST_PAUSE_ON_NO_NEXT_CMD_E              = 7,
  WIFISIFS_BURST_ENDED_ON_LOW_STATUS_E               = 8
} scheduler_command_status__sifs_burst_status__e;    ///< sifs_burst_status Enum Type
typedef enum {
  WIFIRESERVED_CODE_E                                = 0,
  WIFITXPCU_FLREQ_CODE_TXOP_EXCEEDED_E               = 1,
  WIFICRYPT_FLREQ_RX_INT_TX_E                        = 2,
  WIFITXPCU_FLREQ_CODE_RTS_PKT_CCA_ABORT_E           = 3,
  WIFITXPCU_FLREQ_CODE_CTS_CCA_ABORT_E               = 4,
  WIFIPDG_FLREQ_CODE_TXOP_ABORT_E                    = 5,
  WIFISW_EXPLICIT_FLUSH_TERMINATION_E                = 6,
  WIFIFES_STP_NOT_ENOUGH_TXOP_REM_E                  = 7,
  WIFIFES_STP_SCH_TLV_LEN_ERR_E                      = 8,
  WIFIFES_STP_TLV_TIME_EXCEEDED_BKOF_EXP_E           = 9,
  WIFIFES_STP_SW_FES_TIME_GT_HW_E                    = 10,
  WIFITXPCU_FLREQ_PPDU_ALLOW_BW_FIELDS_NOT_SET_E     = 11,
  WIFISPARE_FLUSH_REQ_12_E                           = 12,
  WIFIHWSCH_COEX_ABORT_E                             = 13,
  WIFIHWSCH_SVD_RDY_TIMEOUT_E                        = 14,
  WIFINUM_MPDU_COUNT_ZERO_E                          = 15,
  WIFIUNSUPPORTED_CBF_E                              = 16,
  WIFITXPCU_FLREQ_PCU_PPDU_SETUP_INIT_NOT_VALID_E    = 17,
  WIFITXPCU_FLREQ_PCU_PPDU_SETUP_START_NOT_VALID_E   = 18,
  WIFITXPCU_FLREQ_TX_PHY_DESCRIPTOR_NOT_VALID_E      = 19,
  WIFITXPCU_REQ_TLVS_TIMEOUT_FOR_CBF_E               = 20,
  WIFITXDMA_FLREQ_NO_OF_MPDU_LESS_THAN_LIMIT_STATUS_E = 21,
  WIFITXOLE_FLREQ_FRAG_EN_AMSDU_AMPDU_E              = 22,
  WIFITXOLE_FLREQ_MORE_FRAG_SET_FOR_LAST_SEG_E       = 23,
  WIFITXPCU_FLREQ_START_TX_BW_MISMATCH_E             = 24,
  WIFITXPCU_FLREQ_COEX_BW_NOT_ALLOWED_E              = 25,
  WIFITXOLE_FLREQ_FRAG_EN_SW_ENCRYPTED_E             = 26,
  WIFITXOLE_FLREQ_FRAG_EN_BUFFER_CHAINING_E          = 27,
  WIFITXOLE_FLREQ_PV1_TYPE3_AMSDU_ERROR_E            = 28,
  WIFITXOLE_FLREQ_PV1_WRONG_KEY_TYPE_E               = 29,
  WIFITXOLE_FLREQ_ILLEGAL_FRAME_E                    = 30,
  WIFIPDG_FLREQ_COEX_REASONS_E                       = 31,
  WIFIWIFI_TXOLE_NO_FULL_MSDU_FOR_CHECKSUM_EN_E      = 32,
  WIFIWIFI_TXOLE_LENGTH_MISMATCH_802_3_ETH_FRAME_E   = 33,
  WIFIWIFI_TXOLE_PV0_AMSDU_FRAME_ERR_E               = 34,
  WIFIWIFI_TXOLE_PV0_WRONG_KEY_TYPE_E                = 35,
  WIFIWIFI_FES_STP_CCA_BUSY_IN_PIFS_E                = 36,
  WIFIPROT_FRAME_DATA_UNDERRUN_E                     = 37,
  WIFIPDG_NO_LENGTH_RECEIVED_E                       = 38,
  WIFIPDG_WRONG_PREAMBLE_REQ_ORDER_E                 = 39,
  WIFITXPCU_FLREQ_RETRY_FOR_OPTIMAL_BW_E             = 40,
  WIFIWIFI_TXOLE_INCOMPLETE_LLC_FRAME_E              = 41,
  WIFIPDG_CTS_LOWER_BW_FIT_ERR_E                     = 42,
  WIFIPDG_CTS_SHORTER_DUR_FIT_ERR_E                  = 43,
  WIFIHWSCH_TQM_APB_UNEXPECTED_END_ERR_E             = 44,
  WIFIWIFI_HWSCH_DUPLICATE_ID_SFM_WRITE_ERR_E        = 45,
  WIFIWIFI_HWSCH_DUPLICATE_ID_SFM_READ_ERR_E         = 46,
  WIFIWIFI_HWSCH_SFM_FIFO_OVF_ERR_E                  = 47,
  WIFIWIFI_HWSCH_SFM_FIFO_UNDR_ERR_E                 = 48,
  WIFIHWSCH_SFM_DELAY_EXCEEDED_E                     = 49,
  WIFIHWSCH_UNEXPECTED_SCH_TLV_END_ERR_E             = 50,
  WIFIHWSCH_UNEXPECTED_SCH_TLV_START_ERR_E           = 51,
  WIFITXPCU_PHYTX_ABORT_ERR_E                        = 52,
  WIFITXPCU_COEX_SOFT_ABORT_ERR_E                    = 53,
  WIFIPDG_MIN_USER_COUNT_MISSED_E                    = 54,
  WIFIPDG_MIN_BYTE_COUNT_MISSED_E                    = 55,
  WIFIPDG_MIN_MPDU_COUNT_MISSED_E                    = 56,
  WIFIPDG_MIN_PPDU_TIME_E                            = 57,
  WIFIUCODE_FLUSH_REQUEST_E                          = 58,
  WIFISPARE_FLUSH_REQUEST_1_E                        = 59,
  WIFIHWSCH_SIFS_BURST_SVD_READY_TIMEOUT_E           = 60,
  WIFITXPCU_PHY_DATA_REQUEST_TO_EARLY_E              = 61,
  WIFITXPCU_TRIGGER_RESPONSE_CS_CHECK_FAIL_E         = 62,
  WIFIPDG_OFDMA_MAX_UNUSED_SPACE_VIOLATION_E         = 63,
  WIFICRYPTO_TX_USER_CAPACITY_EXCEEDED_E             = 64,
  WIFICRYPTO_TX_NON_MU_KEY_TYPE_RCVD_E               = 65,
  WIFITXPCU_CBF_RESP_ABORT_E                         = 66,
  WIFITXPCU_PHY_NAP_RECEIVED_DURING_TX_E             = 67,
  WIFIRXPCU_TRIGGER_WITH_FCS_ERROR_E                 = 68,
  WIFIPDG_FLREQ_COEX_BT_HIGHER_PRIORITY_E            = 69,
  WIFISPARE_FLUSH_REQUEST_2_E                        = 70,
  WIFIPDG_MU_CTS_RU_ALLOCATION_CORRUPTION_E          = 71,
  WIFIPDG_TRIG_FOR_BLOCKED_RU_E                      = 72,
  WIFIPDG_TRIG_RESPONSE_MODE_CORRUPTION_E            = 73,
  WIFIPDG_INVALID_TRIGGER_CONFIG_RECEIVED_E          = 74,
  WIFITXOLE_MSDU_TOO_LONG_E                          = 75,
  WIFITXOLE_INCONSISTENT_MESH_E                      = 76,
  WIFITXOLE_MESH_ENABLE_FOR_ETHERNET_E               = 77,
  WIFITXPCU_TRIG_RESPONSE_MODE_CORRUPTION_E          = 78,
  WIFIPDG_11AX_INVALID_RATE_SETUP_E                  = 79,
  WIFITXPCU_TRIG_RESPONSE_INFO_TOO_LATE_E            = 80,
  WIFIUNKNOWN_FLUSH_REQUEST_CODE_E                   = 81
} scheduler_command_status__flush_req_reason__e;     ///< flush_req_reason Enum Type

//-----------------------------------------------------------------------------
// "scheduler_selfgen_response_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIHW_RESPONSE_SUCCESS_E                          = 0,
  WIFIHW_FLUSH_RCVD_E                                = 1
} scheduler_selfgen_response_status__hw_response_result__e; ///< hw_response_result Enum Type
typedef scheduler_command_status__flush_req_reason__e scheduler_selfgen_response_status__flush_req_reason__e;
typedef phytx_abort_request_info__phytx_abort_reason__e scheduler_selfgen_response_status__phytx_abort_reason__e;
typedef response_end_status__data_underflow_warning__e scheduler_selfgen_response_status__data_underflow_warning__e;

//-----------------------------------------------------------------------------
// "tcl_data_cmd" enums
//-----------------------------------------------------------------------------
typedef tcl_exit_base__buf_or_ext_desc_type__e       tcl_data_cmd__buf_or_ext_desc_type__e;
typedef rx_mpdu_info__decap_type__e                  tcl_data_cmd__encap_type__e;
typedef addr_search_entry__key_type__e               tcl_data_cmd__encrypt_type__e;
typedef enum {
  WIFINORMAL_SEARCH_E                                = 0,
  WIFIINDEX_BASED_ADDRESS_SEARCH_E                   = 1,
  WIFIINDEX_BASED_FLOW_SEARCH_E                      = 2
} tcl_data_cmd__search_type__e;                      ///< search_type Enum Type

//-----------------------------------------------------------------------------
// "tcl_gse_cmd" enums
//-----------------------------------------------------------------------------
typedef tcl_status_ring__gse_ctrl__e                 tcl_gse_cmd__gse_ctrl__e;
typedef enum {
  WIFITCL_STATUS_0_RING_E                            = 0,
  WIFITCL_STATUS_1_RING_E                            = 1
} tcl_gse_cmd__status_destination_ring_id__e;        ///< status_destination_ring_id Enum Type
typedef enum {
  WIFIBYTE_SWAP_DISABLE_E                            = 0,
  WIFIBYTE_SWAP_ENABLE_E                             = 1
} tcl_gse_cmd__swap__e;                              ///< swap Enum Type
typedef tcl_status_ring__index_search_en__e          tcl_gse_cmd__index_search_en__e;

//-----------------------------------------------------------------------------
// "tqm_descriptor_threshold_reached_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITQM_DESC_COUNTER0_THRESHOLD_E                  = 0,
  WIFITQM_DESC_COUNTER1_THRESHOLD_E                  = 1,
  WIFITQM_DESC_COUNTER2_THRESHOLD_E                  = 2,
  WIFITQM_DESC_COUNTER_SUM_THRESHOLD_E               = 3
} tqm_descriptor_threshold_reached_status__threshold_index__e; ///< threshold_index Enum Type

//-----------------------------------------------------------------------------
// "tqm_flow_empty_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIFLOW_ENTRY_NOT_VALID_E                         = 0,
  WIFIFLOW_IS_EMPTY_E                                = 1,
  WIFIFLOW_IS_NOT_EMPTY_E                            = 2
} tqm_flow_empty_status__flow0_status__e;            ///< flow0_status Enum Type
typedef tqm_flow_empty_status__flow0_status__e       tqm_flow_empty_status__flow1_status__e;
typedef tqm_flow_empty_status__flow0_status__e       tqm_flow_empty_status__flow2_status__e;
typedef tqm_flow_empty_status__flow0_status__e       tqm_flow_empty_status__flow3_status__e;

//-----------------------------------------------------------------------------
// "tqm_gen_mpdu_length_list" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMEMORY_ADDRESS_E                               = 0,
  WIFITLV_CONNECTION_E                               = 1
} tqm_gen_mpdu_length_list__command_output_selection__e; ///< command_output_selection Enum Type
typedef enum {
  WIFITQM_SCH0_TLV_E                                 = 0,
  WIFITQM_SCH1_TLV_E                                 = 1,
  WIFITQM_SCH2_TLV_E                                 = 2
} tqm_gen_mpdu_length_list__pdg_to_sch_tlv_index__e; ///< pdg_to_sch_tlv_index Enum Type
typedef enum {
  WIFIFIXED_LENGTH_TLV_E                             = 0,
  WIFIOPTIMIZED_LENGTH_TLV_E                         = 1
} tqm_gen_mpdu_length_list__size_optimized_tlv__e;   ///< size_optimized_tlv Enum Type

//-----------------------------------------------------------------------------
// "tqm_gen_mpdu_length_list_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFILIST_MPDU_SUCCESSFUL_COMPLETION_E              = 0,
  WIFILIST_MPDU_GEN_FLAGS_NOT_VALID_E                = 1,
  WIFILIST_MPDU_MAX_MPDU_COUNT_IS_0_E                = 2,
  WIFILIST_MPDU_QUEUE_HEAD_INVALID_E                 = 3,
  WIFILIST_MPDU_PAUSE_ASSERTED_E                     = 4,
  WIFILIST_MPDU_QUEUE_COUNT_IS_0_E                   = 5,
  WIFILIST_MPDU_NOTIFY_FRAME_DETECTED_E              = 6,
  WIFILIST_MPDU_WINDOW_SIZE_REACHED_E                = 7,
  WIFILIST_MPDU_FLUSH_ASSERTED_E                     = 8,
  WIFILIST_INCORRECT_TLV_INDEX_E                     = 9,
  WIFILIST_MPDU_RESOURCE_BLOCKED_E                   = 10,
  WIFILIST_MPDU_END_FOR_MULTIPLE_REASONS_E           = 11,
  WIFILIST_BYTE_COUNT_SIZE_REACHED_E                 = 12
} tqm_gen_mpdu_length_list_status__list_mpdu_end_reason__e; ///< list_mpdu_end_reason Enum Type

//-----------------------------------------------------------------------------
// "tqm_gen_mpdus_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIGEN_MPDU_SUCCESSFUL_COMPLETION_E               = 0,
  WIFIMSDU_MIX_RATIO_ALL_FLOWS_IS_0_E                = 1,
  WIFIMAX_MPDU_SIZE_IS_0_E                           = 2,
  WIFIMAX_MSDU_COUNT_IN_MPDU_IS_0_E                  = 3,
  WIFITARGET_MPDU_LIMIT_REACHED_E                    = 4,
  WIFIGEN_MPDU_WINDOW_SIZE_REACHED_E                 = 5,
  WIFITARGET_EXT_DESC_USAGE_COUNT_REACHED_E          = 6,
  WIFIGEN_MPDU_PAUSE_ASSERTED_E                      = 7,
  WIFIFIRST_MSDU_DID_NOT_FIT_ERROR_E                 = 8,
  WIFIRAN_OUT_OF_MSDUS_IN_ALL_VALID_FLOWS_E          = 9,
  WIFIGEN_MPDU_ALL_FLOW_QUEUES_INVALID_E             = 10,
  WIFIGEN_MPDU_QUEUE_HEAD_INVALID_E                  = 11,
  WIFIGEN_MPDU_FLUSH_ASSERTED_E                      = 12,
  WIFIGEN_MPDU_MAX_MPDU_COUNT_REACHED_E              = 13,
  WIFIGEN_MPDU_MAX_MPDU_GEN_TIME_REACHED_E           = 14,
  WIFIGEN_MPDU_END_FOR_MULTIPLE_REASONS_E            = 15
} tqm_gen_mpdus_status__gen_mpdu_end_reason__e;      ///< gen_mpdu_end_reason Enum Type

//-----------------------------------------------------------------------------
// "tqm_get_mpdu_queue_stats" enums
//-----------------------------------------------------------------------------
typedef reo_get_queue_stats__clear_stats__e          tqm_get_mpdu_queue_stats__clear_stats__e;

//-----------------------------------------------------------------------------
// "tqm_get_msdu_flow_stats" enums
//-----------------------------------------------------------------------------
typedef reo_get_queue_stats__clear_stats__e          tqm_get_msdu_flow_stats__clear_stats__e;

//-----------------------------------------------------------------------------
// "tqm_remove_mpdu" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREMOVE_MPDUS_E                                 = 0,
  WIFIREMOVE_TRANSMITTED_MPDUS_E                     = 1,
  WIFIREMOVE_UNTRANSMITTED_MPDUS_E                   = 2,
  WIFIREMOVE_AGED_MPDUS_E                            = 3,
  WIFIREMOVE_MPDUS_AND_DISABLE_QUEUE_E               = 4
} tqm_remove_mpdu__remove_mpdu_cmd_type__e;          ///< remove_mpdu_cmd_type Enum Type
typedef enum {
  WIFINO_FW_RELEASE_REASON_OVERWRITE_E               = 0,
  WIFISET_RELEASE_FW_REASON1_E                       = 1,
  WIFISET_RELEASE_FW_REASON2_E                       = 2,
  WIFISET_RELEASE_FW_REASON3_E                       = 3
} tqm_remove_mpdu__tqm_release_reason_overwrite__e;  ///< tqm_release_reason_overwrite Enum Type

//-----------------------------------------------------------------------------
// "tqm_remove_mpdu_status" enums
//-----------------------------------------------------------------------------
typedef tqm_remove_mpdu__remove_mpdu_cmd_type__e     tqm_remove_mpdu_status__remove_mpdu_cmd_type__e;

//-----------------------------------------------------------------------------
// "tqm_remove_msdu" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREMOVE_HEAD_MSDUS_E                            = 0,
  WIFIREMOVE_AGED_MSDUS_E                            = 1,
  WIFIREMOVE_MSDUS_AND_DISABLE_FLOW_E                = 2
} tqm_remove_msdu__remove_msdu_cmd_type__e;          ///< remove_msdu_cmd_type Enum Type
typedef tqm_remove_mpdu__tqm_release_reason_overwrite__e tqm_remove_msdu__tqm_release_reason_overwrite__e;

//-----------------------------------------------------------------------------
// "tqm_remove_msdu_status" enums
//-----------------------------------------------------------------------------
typedef tqm_remove_msdu__remove_msdu_cmd_type__e     tqm_remove_msdu_status__remove_msdu_cmd_type__e;

//-----------------------------------------------------------------------------
// "tqm_threshold_drop_notification_status" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFISLOW_DROP_THRESHOLD_REACHED_E                  = 0,
  WIFIMEDIUM_DROP_THRESHOLD_REACHED_E                = 1,
  WIFIHARD_DROP_THRESHOLD_REACHED_E                  = 2
} tqm_threshold_drop_notification_status__drop_threshold_type__e; ///< drop_threshold_type Enum Type

//-----------------------------------------------------------------------------
// "tqm_unblock_cache" enums
//-----------------------------------------------------------------------------
typedef reo_unblock_cache__unblock_type__e           tqm_unblock_cache__unblock_type__e;

//-----------------------------------------------------------------------------
// "tqm_unblock_cache_status" enums
//-----------------------------------------------------------------------------
typedef reo_unblock_cache__unblock_type__e           tqm_unblock_cache_status__unblock_type__e;

//-----------------------------------------------------------------------------
// "tqm_update_tx_msdu_flow" enums
//-----------------------------------------------------------------------------
typedef tx_msdu_flow__drop_rule__e                   tqm_update_tx_msdu_flow__drop_rule__e;

//-----------------------------------------------------------------------------
// "tqm_write_cmd" enums
//-----------------------------------------------------------------------------
typedef tqm_gen_mpdu_length_list__command_output_selection__e tqm_write_cmd__command_output_selection__e;
typedef tqm_gen_mpdu_length_list__pdg_to_sch_tlv_index__e tqm_write_cmd__pdg_to_sch_tlv_index__e;

//-----------------------------------------------------------------------------
// "tx_cbf_info" enums
//-----------------------------------------------------------------------------
typedef pdg_response_rate_setting__alt_bw__e         tx_cbf_info__bandwidth__e;
typedef l_sig_a_info__pkt_type__e                    tx_cbf_info__pkt_type__e;
typedef he_sig_a_mu_ul_info__format_indication__e    tx_cbf_info__dot11ax_received_format_indication__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     tx_cbf_info__dot11ax_received_dl_ul_flag__e;
typedef pdg_response_rate_setting__sgi__e            tx_cbf_info__dot11ax_received_cp_size__e;
typedef ppdu_rate_setting__ltf_size__e               tx_cbf_info__dot11ax_received_ltf_size__e;
typedef addr_search_entry__dot11ax_dl_ul_flag__e     tx_cbf_info__dot11ax_dl_ul_flag__e;
typedef mactx_phy_desc__wait_sifs__e                 tx_cbf_info__wait_sifs__e;

//-----------------------------------------------------------------------------
// "tx_fes_setup" enums
//-----------------------------------------------------------------------------
typedef ofdma_trigger_details__prefered_ac__e        tx_fes_setup__bo_based_prefered_ac__e;
typedef scheduler_command_status__transmit_start_reason__e tx_fes_setup__transmit_start_reason__e;
typedef scheduler_command_status__schedule_cmd_ring_id__e tx_fes_setup__schedule_cmd_ring_id__e;
typedef scheduler_cmd__fes_control_mode__e           tx_fes_setup__fes_control_mode__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_setup__static_bandwidth__e;
typedef enum {
  WIFITXPCU_TRIG_RESPONSE_BOUNDARY_75_E              = 0,
  WIFITXPCU_TRIG_RESPONSE_BOUNDARY_50_E              = 1,
  WIFITXPCU_TRIG_RESPONSE_BOUNDARY_25_E              = 2
} tx_fes_setup__trigger_resp_txpdu_ppdu_boundary__e; ///< trigger_resp_txpdu_ppdu_boundary Enum Type
typedef mactx_phy_desc__bf_type__e                   tx_fes_setup__bf_type__e;
typedef received_response_info__cbf_nc_index__e      tx_fes_setup__cbf_nc_index__e;
typedef received_response_info__cbf_nr_index__e      tx_fes_setup__cbf_nr_index__e;
typedef mactx_phy_desc__wait_sifs__e                 tx_fes_setup__wait_sifs__e;
typedef received_response_info__cbf_feedback_type__e tx_fes_setup__cbf_feedback_type__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_setup__cbf_bw__e;
typedef enum {
  WIFINO_DATA_NULL_RATIO_REQUIREMENT_E               = 0,
  WIFIDATA_NULL_RATIO_16_1_E                         = 1,
  WIFIDATA_NULL_RATIO_18_1_E                         = 2,
  WIFIDATA_NULL_RATIO_4_1_E                          = 3,
  WIFIDATA_NULL_RATIO_2_1_E                          = 4,
  WIFIDATA_NULL_RATIO_1_1_E                          = 5,
  WIFIDATA_NULL_RATIO_1_2_E                          = 6,
  WIFIDATA_NULL_RATIO_1_4_E                          = 7,
  WIFIDATA_NULL_RATIO_1_8_E                          = 8,
  WIFIDATA_NULL_RATIO_1_16_E                         = 9
} tx_fes_setup__fes_continuation_ratio_threshold__e; ///< fes_continuation_ratio_threshold Enum Type

//-----------------------------------------------------------------------------
// "tx_fes_status_end" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFITX_OK_E                                        = 0,
  WIFIPROT_RESP_RX_TIMEOUT_E                         = 1,
  WIFIPPDU_RESP_RX_TIMEOUT_E                         = 2,
  WIFIFES_ERROR_E                                    = 3,
  WIFITX_PHY_ERROR_E                                 = 4,
  WIFIRESP_FRAME_CRC_ERR_E                           = 5,
  WIFISU_RESPONSE_TYPE_MISMATCH_E                    = 6,
  WIFICBF_MIMO_CTRL_MISMATCH_E                       = 7,
  WIFICOEX_SOFT_ABORT_RCVD_E                         = 8,
  WIFIEARLY_TERMINATION_E                            = 9,
  WIFIMU_RESPONSE_TYPE_MISMATCH_E                    = 10,
  WIFIMU_RESPONSE_MPDU_NOT_VALID_E                   = 11,
  WIFIMU_UL_NOT_ENOUGH_USER_RESPONSE_E               = 12,
  WIFITRANSMIT_DATA_NULL_RATIO_NOT_MET_E             = 13
} tx_fes_status_end__global_fes_transmit_result__e;  ///< global_fes_transmit_result Enum Type
typedef received_response_info__cbf_bw__e            tx_fes_status_end__cbf_bw_received__e;
typedef enum {
  WIFIACK_NOT_APPLICABLE_E                           = 0,
  WIFIACK_BASIC_RECEIVED_E                           = 1,
  WIFIACK_BA_0_E                                     = 2,
  WIFIACK_BA_32_RECEIVED_E                           = 3,
  WIFIACK_BA_64_RECEIVED_E                           = 4,
  WIFIACK_BA_128_RECEIVED_E                          = 5,
  WIFIACK_BA_256_RECEIVED_E                          = 6,
  WIFIACK_BA_MULTIPLE_RECEIVED_E                     = 7
} tx_fes_status_end__actual_received_ack_type__e;    ///< actual_received_ack_type Enum Type
typedef response_end_status__timing_status__e        tx_fes_status_end__timing_status__e;
typedef expected_response__expected_response_type__e tx_fes_status_end__response_type__e;

//-----------------------------------------------------------------------------
// "tx_fes_status_prot" enums
//-----------------------------------------------------------------------------
typedef received_response_info__cbf_bw__e            tx_fes_status_prot__cbf_bw__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_prot__agc_cbw__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_prot__service_cbw__e;
typedef response_end_status__timing_status__e        tx_fes_status_prot__timing_status__e;

//-----------------------------------------------------------------------------
// "tx_fes_status_start" enums
//-----------------------------------------------------------------------------
typedef scheduler_command_status__schedule_cmd_ring_id__e tx_fes_status_start__schedule_cmd_ring_id__e;
typedef scheduler_cmd__fes_control_mode__e           tx_fes_status_start__fes_control_mode__e;
typedef pcu_ppdu_setup_init__medium_prot_type__e     tx_fes_status_start__medium_prot_type__e;
typedef scheduler_command_status__transmit_start_reason__e tx_fes_status_start__transmit_start_reason__e;

//-----------------------------------------------------------------------------
// "tx_fes_status_start_ppdu" enums
//-----------------------------------------------------------------------------
typedef expected_response__expected_response_type__e tx_fes_status_start_ppdu__response_type__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_start_ppdu__coex_based_tx_bw__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_start_ppdu__request_packet_bw__e;

//-----------------------------------------------------------------------------
// "tx_fes_status_start_prot" enums
//-----------------------------------------------------------------------------
typedef expected_response__expected_response_type__e tx_fes_status_start_prot__response_type__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_start_prot__prot_coex_based_tx_bw__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_fes_status_start_prot__prot_request_packet_bw__e;

//-----------------------------------------------------------------------------
// "tx_flush_req" enums
//-----------------------------------------------------------------------------
typedef scheduler_command_status__flush_req_reason__e tx_flush_req__flush_req_reason__e;
typedef phytx_abort_request_info__phytx_abort_reason__e tx_flush_req__phytx_abort_reason__e;

//-----------------------------------------------------------------------------
// "tx_puncture_setup" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             tx_puncture_setup__coex_ofdma_mimo_nss_pattern_4__e;
typedef addr_search_entry__ftm_pe_nss__e             tx_puncture_setup__coex_ofdma_mimo_nss_pattern_5__e;
typedef addr_search_entry__ftm_pe_nss__e             tx_puncture_setup__coex_ofdma_mimo_nss_pattern_6__e;
typedef addr_search_entry__ftm_pe_nss__e             tx_puncture_setup__coex_ofdma_mimo_nss_pattern_7__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_puncture_setup__r2r_pattern_4__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_puncture_setup__r2r_pattern_5__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_puncture_setup__r2r_pattern_6__e;
typedef pdg_response_rate_setting__alt_bw__e         tx_puncture_setup__r2r_pattern_7__e;

//-----------------------------------------------------------------------------
// "mactx_expect_cbf_per_user" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_CBF_STORAGE_IN_DDR_E                        = 0,
  WIFISTORE_CBF_IN_MAC_DDR_ADDR_E                    = 1,
  WIFISTORE_CBF_IN_PHY_DDR_ADDR_E                    = 2
} mactx_expect_cbf_per_user__cv_in_ddr_mode__e;      ///< cv_in_ddr_mode Enum Type
typedef enum {
  WIFINO_CBF_STORAGE_IN_PREFETCH_MEM_E               = 0,
  WIFISTORE_CBF_IN_PREFETCH_MEM_E                    = 1
} mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw20__e; ///< cv_in_prefetch_mem_mode_bw20 Enum Type
typedef mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw20__e mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw40__e;
typedef mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw20__e mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw80__e;
typedef mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw20__e mactx_expect_cbf_per_user__cv_in_prefetch_mem_mode_bw160__e;

//-----------------------------------------------------------------------------
// "mactx_user_desc_per_user" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             mactx_user_desc_per_user__nss__e;
typedef enum {
  WIFIUSER_NO_BF_E                                   = 0,
  WIFIUSER_WALSH_ONLY_E                              = 1,
  WIFIUSER_BF_ONLY_E                                 = 2,
  WIFIUSER_WALSH_AND_BF_E                            = 3
} mactx_user_desc_per_user__user_bf_type__e;         ///< user_bf_type Enum Type

//-----------------------------------------------------------------------------
// "pdg_user_setup" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             pdg_user_setup__nss_bw20__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_user_setup__nss_bw40__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_user_setup__nss_bw80__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_user_setup__nss_bw160__e;
typedef addr_search_entry__ftm_pe_nss__e             pdg_user_setup__alt_nss__e;
typedef enum {
  WIFIMPDU_IS_OF_AC_VO_E                             = 0,
  WIFIMPDU_IS_OF_AC_VI_E                             = 1,
  WIFIMPDU_IS_OF_AC_BE_E                             = 2,
  WIFIMPDU_IS_OF_AC_BK_E                             = 3
} pdg_user_setup__triggered_mpdu_ac_category__e;     ///< triggered_mpdu_ac_category Enum Type
typedef enum {
  WIFINO_IMMEDIATE_RESPONSE_E                        = 0,
  WIFIIMMEDIATE_RESPONSE_REQUIRED_E                  = 1
} pdg_user_setup__triggered_mpdu_response_type__e;   ///< triggered_mpdu_response_type Enum Type
typedef mactx_user_desc_per_user__user_bf_type__e    pdg_user_setup__bw20_user_bf_type__e;
typedef mactx_user_desc_per_user__user_bf_type__e    pdg_user_setup__bw40_user_bf_type__e;
typedef mactx_user_desc_per_user__user_bf_type__e    pdg_user_setup__bw80_user_bf_type__e;
typedef mactx_user_desc_per_user__user_bf_type__e    pdg_user_setup__bw160_user_bf_type__e;

//-----------------------------------------------------------------------------
// "rx_attention" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__rxpcu_mpdu_filter_in_category__e rx_attention__rxpcu_mpdu_filter_in_category__e;
typedef rx_mpdu_info__sw_frame_group_id__e           rx_attention__sw_frame_group_id__e;
typedef enum {
  WIFIDECRYPT_OK_E                                   = 0,
  WIFIDECRYPT_UNPROTECTED_FRAME_E                    = 1,
  WIFIDECRYPT_DATA_ERR_E                             = 2,
  WIFIDECRYPT_KEY_INVALID_E                          = 3,
  WIFIDECRYPT_PEER_ENTRY_INVALID_E                   = 4,
  WIFIDECRYPT_OTHER_E                                = 5
} rx_attention__decrypt_status_code__e;              ///< decrypt_status_code Enum Type

//-----------------------------------------------------------------------------
// "rx_frame_bitmap_ack" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIACK_FOR_SINGLE_DATA_FRAME_E                    = 0,
  WIFIACK_FOR_MANAGEMENT_E                           = 1,
  WIFIACK_FOR_PSPOLL_E                               = 2,
  WIFIACK_FOR_ASSOC_REQUEST_E                        = 3,
  WIFIACK_FOR_ALL_FRAMES_E                           = 4
} rx_frame_bitmap_ack__explict_ack_type__e;          ///< explict_ack_type Enum Type
typedef enum {
  WIFIBA_BITMAP_32_E                                 = 0,
  WIFIBA_BITMAP_64_E                                 = 1,
  WIFIBA_BITMAP_128_E                                = 2,
  WIFIBA_BITMAP_256_E                                = 3
} rx_frame_bitmap_ack__ba_bitmap_size__e;            ///< ba_bitmap_size Enum Type

//-----------------------------------------------------------------------------
// "rx_mpdu_end" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__rxpcu_mpdu_filter_in_category__e rx_mpdu_end__rxpcu_mpdu_filter_in_category__e;
typedef rx_mpdu_info__sw_frame_group_id__e           rx_mpdu_end__sw_frame_group_id__e;
typedef rxpt_classify_info__rxdma0_destination_ring_selection__e rx_mpdu_end__rxdma0_destination_ring__e;
typedef rxpt_classify_info__rxdma0_destination_ring_selection__e rx_mpdu_end__rxdma1_destination_ring__e;
typedef rx_attention__decrypt_status_code__e         rx_mpdu_end__decrypt_status_code__e;

//-----------------------------------------------------------------------------
// "rx_msdu_end" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__rxpcu_mpdu_filter_in_category__e rx_msdu_end__rxpcu_mpdu_filter_in_category__e;
typedef rx_mpdu_info__sw_frame_group_id__e           rx_msdu_end__sw_frame_group_id__e;
typedef reo_entrance_ring__reo_destination_indication__e rx_msdu_end__reo_destination_indication__e;

//-----------------------------------------------------------------------------
// "rx_msdu_start" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__rxpcu_mpdu_filter_in_category__e rx_msdu_start__rxpcu_mpdu_filter_in_category__e;
typedef rx_mpdu_info__sw_frame_group_id__e           rx_msdu_start__sw_frame_group_id__e;
typedef rx_mpdu_info__decap_type__e                  rx_msdu_start__decap_format__e;
typedef l_sig_a_info__pkt_type__e                    rx_msdu_start__pkt_type__e;
typedef pdg_response_rate_setting__sgi__e            rx_msdu_start__sgi__e;
typedef receive_user_info__receive_bandwidth__e      rx_msdu_start__receive_bandwidth__e;
typedef receive_user_info__reception_type__e         rx_msdu_start__reception_type__e;

//-----------------------------------------------------------------------------
// "rx_ppdu_end_user_stats" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__ftm_pe_nss__e             rx_ppdu_end_user_stats__nss__e;
typedef l_sig_a_info__pkt_type__e                    rx_ppdu_end_user_stats__ht_control_field_pkt_type__e;

//-----------------------------------------------------------------------------
// "rxpcu_user_setup" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIACK_FRAME_TO_REMOVE_HEAD_E                     = 0,
  WIFIACK_FRAME_TO_REMOVE_TRANSMITTED_E              = 1
} rxpcu_user_setup__ack_frame_handling_type__e;      ///< ack_frame_handling_type Enum Type
typedef tqm_entrance_ring__tqm_status_required__e    rxpcu_user_setup__tqm_status_required__e;
typedef tqm_entrance_ring__tqm_status_ring__e        rxpcu_user_setup__tqm_status_ring__e;
typedef enum {
  WIFINO_LIST_E                                      = 0,
  WIFIGEN_64_MPDU_LIST_E                             = 1,
  WIFIGEN_128_MPDU_LIST_E                            = 2,
  WIFIGEN_256_MPDU_LIST_E                            = 3
} rxpcu_user_setup__gen_mpdu_list_control__e;        ///< gen_mpdu_list_control Enum Type

//-----------------------------------------------------------------------------
// "rxpcu_user_setup_ext" enums
//-----------------------------------------------------------------------------
typedef tqm_entrance_ring__tqm_status_required__e    rxpcu_user_setup_ext__tqm_status_required__e;

//-----------------------------------------------------------------------------
// "tqm_acked_mpdu" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIREMOVE_ACKED_MPDU_E                            = 0,
  WIFIREMOVE_BLOCK_ACKED_MPDUS_E                     = 1,
  WIFIREMOVE_ALL_MPDUS_TRANSMITTED_E                 = 2
} tqm_acked_mpdu__remove_acked_cmd_type__e;          ///< remove_acked_cmd_type Enum Type
typedef rxpcu_user_setup__gen_mpdu_list_control__e   tqm_acked_mpdu__gen_mpdu_list_control__e;

//-----------------------------------------------------------------------------
// "tx_11ah_setup" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIA1_A2_PRESENT_E                                = 0,
  WIFICOMPRESSED_A1_E                                = 1,
  WIFICOMPRESSED_A2_E                                = 2
} tx_11ah_setup__a1_a2_compression__e;               ///< a1_a2_compression Enum Type
typedef enum {
  WIFIA3_A4_ABSENT_E                                 = 0,
  WIFIA4_PRESENT_E                                   = 1,
  WIFIA3_PRESENT_E                                   = 2,
  WIFIA3_A4_PRESENT_E                                = 3
} tx_11ah_setup__a3_a4_compression__e;               ///< a3_a4_compression Enum Type

//-----------------------------------------------------------------------------
// "tx_fes_status_ack_or_ba" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIACK_TYPE_E                                     = 0,
  WIFIBA_TYPE_E                                      = 1
} tx_fes_status_ack_or_ba__ack_ba_status_type__e;    ///< ack_ba_status_type Enum Type
typedef enum {
  WIFIBA_TYPE_ACK_E                                  = 0,
  WIFIBA_TYPE_BITMAP_E                               = 1
} tx_fes_status_ack_or_ba__ba_type__e;               ///< ba_type Enum Type

//-----------------------------------------------------------------------------
// "tx_fes_status_user_ppdu" enums
//-----------------------------------------------------------------------------
typedef response_end_status__data_underflow_warning__e tx_fes_status_user_ppdu__data_underflow_warning__e;

//-----------------------------------------------------------------------------
// "tx_fes_status_user_response" enums
//-----------------------------------------------------------------------------
typedef tx_fes_status_end__global_fes_transmit_result__e tx_fes_status_user_response__fes_transmit_result__e;

//-----------------------------------------------------------------------------
// "tx_mpdu_start" enums
//-----------------------------------------------------------------------------
typedef tx_mpdu_details__mpdu_type__e                tx_mpdu_start__mpdu_type__e;

//-----------------------------------------------------------------------------
// "tx_msdu_start" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__decap_type__e                  tx_msdu_start__encap_type__e;
typedef enum {
  WIFIDA_SA_IS_ABSENT_E                              = 0,
  WIFIDA_IS_PRESENT_E                                = 1,
  WIFISA_IS_PRESENT_E                                = 2,
  WIFIDA_SA_IS_PRESENT_E                             = 3
} tx_msdu_start__da_sa_present__e;                   ///< da_sa_present Enum Type

//-----------------------------------------------------------------------------
// "tx_peer_entry" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__key_type__e               tx_peer_entry__key_type__e;
typedef enum {
  WIFIAD3_A__AD4_A_E                                 = 0,
  WIFIAD3_A__AD4_B_E                                 = 1,
  WIFIAD3_B__AD4_A_E                                 = 2,
  WIFIAD3_B__AD4_B_E                                 = 3,
  WIFIAD3_DA__AD4_SA_E                               = 4
} tx_peer_entry__a_msdu_wds_ad3_ad4__e;              ///< a_msdu_wds_ad3_ad4 Enum Type

//-----------------------------------------------------------------------------
// "tx_queue_extension" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIBUF_STATE_TID_BASED_E                          = 0,
  WIFIBUF_STATE_SUM_BASED_E                          = 1
} tx_queue_extension__buf_state_source__e;           ///< buf_state_source Enum Type

//-----------------------------------------------------------------------------
// "tx_raw_or_native_frame_setup" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIMASK_DISABLE_E                                 = 0,
  WIFIMASK_ENABLE_E                                  = 1
} tx_raw_or_native_frame_setup__fc_to_ds_mask__e;    ///< fc_to_ds_mask Enum Type
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_from_ds_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_more_frag_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_retry_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_pwr_mgt_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_more_data_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_prot_frame_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_order_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__duration_field_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__sequence_control_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__qc_tid_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__qc_eosp_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__qc_ack_policy_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__qc_amsdu_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__fc_relay_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__qc_15to8_mask__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_mask__e tx_raw_or_native_frame_setup__iv_mask__e;
typedef enum {
  WIFICLEAR_E                                        = 0,
  WIFISET_E                                          = 1
} tx_raw_or_native_frame_setup__fc_to_ds_setting__e; ///< fc_to_ds_setting Enum Type
typedef tx_raw_or_native_frame_setup__fc_to_ds_setting__e tx_raw_or_native_frame_setup__fc_from_ds_setting__e;
typedef tx_raw_or_native_frame_setup__fc_to_ds_setting__e tx_raw_or_native_frame_setup__fc_more_frag_setting__e;
typedef enum {
  WIFIFC_RETRY_CLEAR_E                               = 0,
  WIFIFC_RETRY_SET_E                                 = 1,
  WIFIFC_RETRY_BIMAP_BASED_E                         = 2
} tx_raw_or_native_frame_setup__fc_retry_setting__e; ///< fc_retry_setting Enum Type
typedef tx_raw_or_native_frame_setup__fc_to_ds_setting__e tx_raw_or_native_frame_setup__fc_pwr_mgt_setting__e;
typedef enum {
  WIFIFC_MORE_DATA_CLEAR_E                           = 0,
  WIFIFC_MORE_DATA_SET_E                             = 1,
  WIFIFC_MORE_DATA_PDG_BASED_E                       = 2
} tx_raw_or_native_frame_setup__fc_more_data_setting__e; ///< fc_more_data_setting Enum Type
typedef enum {
  WIFIFC_PROT_FRAME_CLEAR_E                          = 0,
  WIFIFC_PROT_FRAME_SET_E                            = 1,
  WIFIFC_PROT_FRAME_ENCAP_TYPE_BASED_E               = 2
} tx_raw_or_native_frame_setup__fc_prot_frame_setting__e; ///< fc_prot_frame_setting Enum Type
typedef tx_raw_or_native_frame_setup__fc_to_ds_setting__e tx_raw_or_native_frame_setup__fc_order_setting__e;
typedef enum {
  WIFIQC_EOSP_CLEAR_E                                = 0,
  WIFIQC_EOSP_SET_E                                  = 1,
  WIFIQC_EOSP_PDG_BASED_E                            = 2
} tx_raw_or_native_frame_setup__qc_eosp_setting__e;  ///< qc_eosp_setting Enum Type
typedef tx_raw_or_native_frame_setup__fc_to_ds_setting__e tx_raw_or_native_frame_setup__qc_amsdu_setting__e;
typedef enum {
  WIFIFC_RELAY_CLEAR_E                               = 0,
  WIFIFC_RELAY_SET_E                                 = 1
} tx_raw_or_native_frame_setup__fc_relay_setting__e; ///< fc_relay_setting Enum Type
typedef enum {
  WIFISEQ_CTRL_SOURCE_MPDU_START_E                   = 0,
  WIFISEQ_CTRL_SOURCE_THIS_TLV_E                     = 1
} tx_raw_or_native_frame_setup__sequence_control_source__e; ///< sequence_control_source Enum Type

//-----------------------------------------------------------------------------
// "txpcu_user_setup" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINO_ACK_BA_CHECK_E                              = 0,
  WIFISINGLE_ACK_BA_CHECK_E                          = 1,
  WIFIMULTIPLE_ACK_BA_CHECK_E                        = 2
} txpcu_user_setup__check_with_rxpcu_for_ack_ba__e;  ///< check_with_rxpcu_for_ack_ba Enum Type
typedef rx_frame_bitmap_req__user_request_type__e    txpcu_user_setup__user_request_type__e;

//-----------------------------------------------------------------------------
// "who_anchor_value" enums
//-----------------------------------------------------------------------------
typedef cce_rule__anchor_type__e                     who_anchor_value__anchor_type__e;

//-----------------------------------------------------------------------------
// "who_packet_hdr" enums
//-----------------------------------------------------------------------------
typedef rx_mpdu_info__decap_type__e                  who_packet_hdr__encap_decap_type__e;
typedef addr_search_entry__key_type__e               who_packet_hdr__encrypt_type__e;

//-----------------------------------------------------------------------------
// "mactx_cbf_done" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFICBF_SUCCESSFULLY_RECEIVED_E                    = 0,
  WIFICBF_FCS_ERROR_E                                = 1,
  WIFICBF_OTHER_ERROR_E                              = 2
} mactx_cbf_done__cbf_receive_status__e;             ///< cbf_receive_status Enum Type

//-----------------------------------------------------------------------------
// "phyrx_user_abort_notification" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIPHYRX_NO_MORE_DATA_E                           = 0,
  WIFIPHYRX_USER_POWER_DROP_E                        = 1,
  WIFIPHYRX_HW_RESOURCE_LIMIT_E                      = 2,
  WIFIPHYRX_FW_RESOURCE_LIMIT_E                      = 3,
  WIFIPHYRX_UNSUPPORTED_PARAM_E                      = 4,
  WIFIPHYRX_OTHER_E                                  = 5
} phyrx_user_abort_notification__phyrx_user_abort_reason__e; ///< phyrx_user_abort_reason Enum Type

//-----------------------------------------------------------------------------
// "macrx_chain_mask" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFICHAIN_MASK_NOT_VALID_E                         = 0,
  WIFICHAIN_MASK_VALID_E                             = 1
} macrx_chain_mask__rx_chain_mask_valid__e;          ///< rx_chain_mask_valid Enum Type
typedef mactx_phy_desc__ant_sel__e                   macrx_chain_mask__ant_sel__e;
typedef mactx_phy_desc__ant_sel_valid__e             macrx_chain_mask__ant_sel_valid__e;

//-----------------------------------------------------------------------------
// "macrx_expect_ndp_reception" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFINDP_FEEDBACK_11AC_SU_E                         = 0,
  WIFINDP_FEEDBACK_11AC_MU_E                         = 1,
  WIFINDP_FEEDBACK_11AX_SU_E                         = 2,
  WIFINDP_FEEDBACK_11AX_MU_E                         = 3,
  WIFINDP_FEEDBACK_11AX_CQI_E                        = 4,
  WIFINDP_FEEDBACK_11AX_RESERVED_E                   = 5
} macrx_expect_ndp_reception__ndp_feedback_type__e;  ///< ndp_feedback_type Enum Type
typedef pdg_response_rate_setting__alt_bw__e         macrx_expect_ndp_reception__ndpa_bw__e;
typedef l_sig_a_info__pkt_type__e                    macrx_expect_ndp_reception__ndpa_pkt_type__e;

//-----------------------------------------------------------------------------
// "macrx_freeze_capture_channel" enums
//-----------------------------------------------------------------------------
typedef enum {
  WIFIFREEZE_REASON_TM_E                             = 0,
  WIFIFREEZE_REASON_FTM_E                            = 1,
  WIFIFREEZE_REASON_ACK_RESP_TO_TM_FTM_E             = 2
} macrx_freeze_capture_channel__capture_reason__e;   ///< capture_reason Enum Type

//-----------------------------------------------------------------------------
// "macrx_req_implicit_fb" enums
//-----------------------------------------------------------------------------
typedef addr_search_entry__capture_channel_type__e   macrx_req_implicit_fb__capture_channel_type__e;

//-----------------------------------------------------------------------------
// "phytx_pkt_end" enums
//-----------------------------------------------------------------------------
typedef response_end_status__timing_status__e        phytx_pkt_end__timing_status__e;

//-----------------------------------------------------------------------------
// "phytx_ppdu_header_info_request" enums
//-----------------------------------------------------------------------------
typedef pdg_wait_for_phy_request__phy_request_type__e phytx_ppdu_header_info_request__request_type__e;


#endif // _TLV_ENUM_H_
