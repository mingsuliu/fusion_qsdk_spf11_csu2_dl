// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MACRX_CBF_READ_REQUEST_H_
#define _MACRX_CBF_READ_REQUEST_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	brpoll_info_valid[0], trigger_brpoll_info_valid[1], npda_info_11ac_valid[2], npda_info_11ax_valid[3], first_cbf_read_request[4], reserved_0a[7:5], brpoll_info[15:8]
//	1	vht_ndpa_sta_info[15:0]
//	2	he_sta_info_15_0[15:0]
//	3	he_sta_info_31_16[15:0]
//	4	trigger_brpoll_common_info_15_0[15:0]
//	5	trigger_brpoll_common_info_31_16[15:0]
//	6	trigger_brpoll_user_info_15_0[15:0]
//	7	trigger_brpoll_user_info_31_16[15:0]
//	8	cbf_id[1:0], cbf_fragment_number[5:2], requested_cbf_frag_count[9:6], reserved_8a[15:10]
//	9	cbf_fragment_size[13:0], reserved_9a[15:14]
//	10	sw_peer_id[15:0]
//	11	cbf_data_read_size[15:0]
//
// ################ END SUMMARY #################

#define NUM_OF_WORDS_MACRX_CBF_READ_REQUEST 12

struct macrx_cbf_read_request {
             uint16_t brpoll_info_valid               :  1, //[0]
                      trigger_brpoll_info_valid       :  1, //[1]
                      npda_info_11ac_valid            :  1, //[2]
                      npda_info_11ax_valid            :  1, //[3]
                      first_cbf_read_request          :  1, //[4]
                      reserved_0a                     :  3, //[7:5]
                      brpoll_info                     :  8; //[15:8]
             uint16_t vht_ndpa_sta_info               : 16; //[15:0]
             uint16_t he_sta_info_15_0                : 16; //[15:0]
             uint16_t he_sta_info_31_16               : 16; //[15:0]
             uint16_t trigger_brpoll_common_info_15_0 : 16; //[15:0]
             uint16_t trigger_brpoll_common_info_31_16: 16; //[15:0]
             uint16_t trigger_brpoll_user_info_15_0   : 16; //[15:0]
             uint16_t trigger_brpoll_user_info_31_16  : 16; //[15:0]
             uint16_t cbf_id                          :  2, //[1:0]
                      cbf_fragment_number             :  4, //[5:2]
                      requested_cbf_frag_count        :  4, //[9:6]
                      reserved_8a                     :  6; //[15:10]
             uint16_t cbf_fragment_size               : 14, //[13:0]
                      reserved_9a                     :  2; //[15:14]
             uint16_t sw_peer_id                      : 16; //[15:0]
             uint16_t cbf_data_read_size              : 16; //[15:0]
};

/*

brpoll_info_valid
			
			When set, legacy brpoll info is valid
			
			<legal all>

trigger_brpoll_info_valid
			
			When set, trigger based brpoll info is valid
			
			<legal all>

npda_info_11ac_valid
			
			When set, 11ac_NDPA info is valid.
			
			TXPCU will have to trigger the PDG for response
			transmission 
			
			<legal all>

npda_info_11ax_valid
			
			When set, 11ax_NDPA info is valid.
			
			TXPCU will have to trigger the PDG for response
			transmission 
			
			<legal all>

first_cbf_read_request
			
			When set, this is the first CBF_READ_REQUEST TLV that
			MAC generates after having received a request (BRPOLL,
			trigger BRBOLL or NDPA based ...) from the beamformer to
			send CBF info back. 
			
			The PHY can potentially expect more CBF_READ_REQUEST
			TLVs to follow for this CBF  request.
			
			
			
			Generally only for this setting, PHY will have to
			(double) check that what MAC is asking for is in line with
			what the beamformer has been asking for. For this PHY needs
			to look into the valid BRPOLL or NDPA related fields.
			
			
			
			PHY also uses this to determine what the contents of the
			response MIMO control fields need to be. This is especially
			important when the beamformer only requested a subset of the
			fragments to be sent back.
			
			
			
			When clear, this TLV is generated by MAC to ask PHY for
			and additional fragments to be returned to the MAC
			
			<legal all>

reserved_0a
			
			<legal 0>

brpoll_info
			
			Field only valid when Brpoll_info_valid  is set.
			
			
			
			Feedback Segment retransmission feedback field from the
			BRPOLL frame.
			
			<legal all>

vht_ndpa_sta_info
			
			Field only valid when Npda_info_11ac_valid is set
			
			
			
			The complete STA INFO field that MAC extracted from the
			VHT NDPA frame.
			
			<legal all>

he_sta_info_15_0
			
			Field only valid when Npda_info_11ax_valid is set
			
			
			
			The first 16 bits of the STA INFO field in the NDPA
			frame
			
			
			
			Put here for backup reasons in case last moment fields
			got added that PHY needs to be able to interpret
			
			<legal all>

he_sta_info_31_16
			
			Field only valid when Npda_info_11ax_valid is set
			
			
			
			The second 16 bits of the STA INFO field in the NDPA
			frame
			
			
			
			Put here for backup reasons in case last moment fields
			got added that PHY needs to be able to interpret
			
			<legal all>

trigger_brpoll_common_info_15_0
			
			
			
			
			Trigger based BRPOLL request info... bits [15:0] 
			
			
			
			This is the variable common info field from the trigger
			related to the BTPOLL. For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>

trigger_brpoll_common_info_31_16
			
			
			
			
			Trigger based BRPOLL request info... bits [31:15] 
			
			
			
			This is the variable common info field from the trigger
			related to the BTPOLL. For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>

trigger_brpoll_user_info_15_0
			
			
			
			
			BRPOLL trigger Type dependent User information bits
			[15:0] 
			
			
			
			This is the variable user info field from the trigger
			related to the BTPOLL. 
			
			
			
			For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>

trigger_brpoll_user_info_31_16
			
			
			
			
			BRPOLL trigger Type dependent User information bits
			[31:16] 
			
			
			
			This is the variable user info field from the trigger
			related to the BTPOLL. 
			
			
			
			For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>

cbf_id
			
			Indicates to the PHY, which CBF storage resource ID is
			associated with this CBF according to the MAC.
			
			 
			
			Used by the PHY  (together with the field sw_peer_id) to
			double check if this is in sync with what the PHY has.
			
			<legal 0-1>

cbf_fragment_number
			
			The CBF fragment number requested for this CBF. Used the
			PHY to double check the MAC's request as well as to
			determine the start address of the CBF data to be given to
			the MAC.
			
			
			
			Note that PHY has provided MAC all the fragment size
			info already in the PHYRX_GENERATED_CBF_DETAILS. PHY
			internally stores the address info for each of the CBF
			fragments.
			
			<legal all>

requested_cbf_frag_count
			
			Field only valid when First_cbf_read_request  is set.
			
			
			
			The number of fragments that need to be sent back to
			this brpoll or NDPA request.
			
			
			
			Used the PHY to double check that MAC's interpretation
			of the BRPOLL or NPDA is correct
			
			<legal 1 - 8>

reserved_8a
			
			<legal 0>

cbf_fragment_size
			
			The total number of bytes requested from the PHY for
			this CBF Fragment.
			
			
			
			Can be used by the PHY to double check is this is line
			with what is expected for this Fragment.
			
			<legal all>

reserved_9a
			
			<legal 0>

sw_peer_id
			
			For DEBUG
			
			
			
			An identifier indicating from which AP this CBF is being
			requested. Helps in crosschecking that the MAC and PHY are
			still in sync on what is stored in the cbf_mem_index
			location.
			
			<legal all>

cbf_data_read_size
			
			The number of bytes MAC will start requesting in each
			MACRX_CBF_DATA_READ_REQUEST. This field shall always have
			the same value as the field 'Byte_count' in that TLV, accept
			for maybe the last generated MACRX_CBF_DATA_READ_REQUEST
			where this value could be less..
			
			
			
			Used by the PHY to prime the DMA engines...
			
			<legal all>
*/


/* Description		MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_VALID
			
			When set, legacy brpoll info is valid
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_VALID_OFFSET            0x00000000
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_VALID_LSB               0
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_VALID_MASK              0x00000001

/* Description		MACRX_CBF_READ_REQUEST_0_TRIGGER_BRPOLL_INFO_VALID
			
			When set, trigger based brpoll info is valid
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_TRIGGER_BRPOLL_INFO_VALID_OFFSET    0x00000000
#define MACRX_CBF_READ_REQUEST_0_TRIGGER_BRPOLL_INFO_VALID_LSB       1
#define MACRX_CBF_READ_REQUEST_0_TRIGGER_BRPOLL_INFO_VALID_MASK      0x00000002

/* Description		MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AC_VALID
			
			When set, 11ac_NDPA info is valid.
			
			TXPCU will have to trigger the PDG for response
			transmission 
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AC_VALID_OFFSET         0x00000000
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AC_VALID_LSB            2
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AC_VALID_MASK           0x00000004

/* Description		MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AX_VALID
			
			When set, 11ax_NDPA info is valid.
			
			TXPCU will have to trigger the PDG for response
			transmission 
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AX_VALID_OFFSET         0x00000000
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AX_VALID_LSB            3
#define MACRX_CBF_READ_REQUEST_0_NPDA_INFO_11AX_VALID_MASK           0x00000008

/* Description		MACRX_CBF_READ_REQUEST_0_FIRST_CBF_READ_REQUEST
			
			When set, this is the first CBF_READ_REQUEST TLV that
			MAC generates after having received a request (BRPOLL,
			trigger BRBOLL or NDPA based ...) from the beamformer to
			send CBF info back. 
			
			The PHY can potentially expect more CBF_READ_REQUEST
			TLVs to follow for this CBF  request.
			
			
			
			Generally only for this setting, PHY will have to
			(double) check that what MAC is asking for is in line with
			what the beamformer has been asking for. For this PHY needs
			to look into the valid BRPOLL or NDPA related fields.
			
			
			
			PHY also uses this to determine what the contents of the
			response MIMO control fields need to be. This is especially
			important when the beamformer only requested a subset of the
			fragments to be sent back.
			
			
			
			When clear, this TLV is generated by MAC to ask PHY for
			and additional fragments to be returned to the MAC
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_FIRST_CBF_READ_REQUEST_OFFSET       0x00000000
#define MACRX_CBF_READ_REQUEST_0_FIRST_CBF_READ_REQUEST_LSB          4
#define MACRX_CBF_READ_REQUEST_0_FIRST_CBF_READ_REQUEST_MASK         0x00000010

/* Description		MACRX_CBF_READ_REQUEST_0_RESERVED_0A
			
			<legal 0>
*/
#define MACRX_CBF_READ_REQUEST_0_RESERVED_0A_OFFSET                  0x00000000
#define MACRX_CBF_READ_REQUEST_0_RESERVED_0A_LSB                     5
#define MACRX_CBF_READ_REQUEST_0_RESERVED_0A_MASK                    0x000000e0

/* Description		MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO
			
			Field only valid when Brpoll_info_valid  is set.
			
			
			
			Feedback Segment retransmission feedback field from the
			BRPOLL frame.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_OFFSET                  0x00000000
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_LSB                     8
#define MACRX_CBF_READ_REQUEST_0_BRPOLL_INFO_MASK                    0x0000ff00

/* Description		MACRX_CBF_READ_REQUEST_1_VHT_NDPA_STA_INFO
			
			Field only valid when Npda_info_11ac_valid is set
			
			
			
			The complete STA INFO field that MAC extracted from the
			VHT NDPA frame.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_1_VHT_NDPA_STA_INFO_OFFSET            0x00000004
#define MACRX_CBF_READ_REQUEST_1_VHT_NDPA_STA_INFO_LSB               0
#define MACRX_CBF_READ_REQUEST_1_VHT_NDPA_STA_INFO_MASK              0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_2_HE_STA_INFO_15_0
			
			Field only valid when Npda_info_11ax_valid is set
			
			
			
			The first 16 bits of the STA INFO field in the NDPA
			frame
			
			
			
			Put here for backup reasons in case last moment fields
			got added that PHY needs to be able to interpret
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_2_HE_STA_INFO_15_0_OFFSET             0x00000008
#define MACRX_CBF_READ_REQUEST_2_HE_STA_INFO_15_0_LSB                0
#define MACRX_CBF_READ_REQUEST_2_HE_STA_INFO_15_0_MASK               0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_3_HE_STA_INFO_31_16
			
			Field only valid when Npda_info_11ax_valid is set
			
			
			
			The second 16 bits of the STA INFO field in the NDPA
			frame
			
			
			
			Put here for backup reasons in case last moment fields
			got added that PHY needs to be able to interpret
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_3_HE_STA_INFO_31_16_OFFSET            0x0000000c
#define MACRX_CBF_READ_REQUEST_3_HE_STA_INFO_31_16_LSB               0
#define MACRX_CBF_READ_REQUEST_3_HE_STA_INFO_31_16_MASK              0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_4_TRIGGER_BRPOLL_COMMON_INFO_15_0
			
			
			
			
			Trigger based BRPOLL request info... bits [15:0] 
			
			
			
			This is the variable common info field from the trigger
			related to the BTPOLL. For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_4_TRIGGER_BRPOLL_COMMON_INFO_15_0_OFFSET 0x00000010
#define MACRX_CBF_READ_REQUEST_4_TRIGGER_BRPOLL_COMMON_INFO_15_0_LSB 0
#define MACRX_CBF_READ_REQUEST_4_TRIGGER_BRPOLL_COMMON_INFO_15_0_MASK 0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_5_TRIGGER_BRPOLL_COMMON_INFO_31_16
			
			
			
			
			Trigger based BRPOLL request info... bits [31:15] 
			
			
			
			This is the variable common info field from the trigger
			related to the BTPOLL. For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_5_TRIGGER_BRPOLL_COMMON_INFO_31_16_OFFSET 0x00000014
#define MACRX_CBF_READ_REQUEST_5_TRIGGER_BRPOLL_COMMON_INFO_31_16_LSB 0
#define MACRX_CBF_READ_REQUEST_5_TRIGGER_BRPOLL_COMMON_INFO_31_16_MASK 0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_6_TRIGGER_BRPOLL_USER_INFO_15_0
			
			
			
			
			BRPOLL trigger Type dependent User information bits
			[15:0] 
			
			
			
			This is the variable user info field from the trigger
			related to the BTPOLL. 
			
			
			
			For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_6_TRIGGER_BRPOLL_USER_INFO_15_0_OFFSET 0x00000018
#define MACRX_CBF_READ_REQUEST_6_TRIGGER_BRPOLL_USER_INFO_15_0_LSB   0
#define MACRX_CBF_READ_REQUEST_6_TRIGGER_BRPOLL_USER_INFO_15_0_MASK  0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_7_TRIGGER_BRPOLL_USER_INFO_31_16
			
			
			
			
			BRPOLL trigger Type dependent User information bits
			[31:16] 
			
			
			
			This is the variable user info field from the trigger
			related to the BTPOLL. 
			
			
			
			For field definition see IEEE spec
			
			
			
			Note: final IEEE field might not need all these bits. If
			so, the extra bits become reserved fields.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_7_TRIGGER_BRPOLL_USER_INFO_31_16_OFFSET 0x0000001c
#define MACRX_CBF_READ_REQUEST_7_TRIGGER_BRPOLL_USER_INFO_31_16_LSB  0
#define MACRX_CBF_READ_REQUEST_7_TRIGGER_BRPOLL_USER_INFO_31_16_MASK 0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_8_CBF_ID
			
			Indicates to the PHY, which CBF storage resource ID is
			associated with this CBF according to the MAC.
			
			 
			
			Used by the PHY  (together with the field sw_peer_id) to
			double check if this is in sync with what the PHY has.
			
			<legal 0-1>
*/
#define MACRX_CBF_READ_REQUEST_8_CBF_ID_OFFSET                       0x00000020
#define MACRX_CBF_READ_REQUEST_8_CBF_ID_LSB                          0
#define MACRX_CBF_READ_REQUEST_8_CBF_ID_MASK                         0x00000003

/* Description		MACRX_CBF_READ_REQUEST_8_CBF_FRAGMENT_NUMBER
			
			The CBF fragment number requested for this CBF. Used the
			PHY to double check the MAC's request as well as to
			determine the start address of the CBF data to be given to
			the MAC.
			
			
			
			Note that PHY has provided MAC all the fragment size
			info already in the PHYRX_GENERATED_CBF_DETAILS. PHY
			internally stores the address info for each of the CBF
			fragments.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_8_CBF_FRAGMENT_NUMBER_OFFSET          0x00000020
#define MACRX_CBF_READ_REQUEST_8_CBF_FRAGMENT_NUMBER_LSB             2
#define MACRX_CBF_READ_REQUEST_8_CBF_FRAGMENT_NUMBER_MASK            0x0000003c

/* Description		MACRX_CBF_READ_REQUEST_8_REQUESTED_CBF_FRAG_COUNT
			
			Field only valid when First_cbf_read_request  is set.
			
			
			
			The number of fragments that need to be sent back to
			this brpoll or NDPA request.
			
			
			
			Used the PHY to double check that MAC's interpretation
			of the BRPOLL or NPDA is correct
			
			<legal 1 - 8>
*/
#define MACRX_CBF_READ_REQUEST_8_REQUESTED_CBF_FRAG_COUNT_OFFSET     0x00000020
#define MACRX_CBF_READ_REQUEST_8_REQUESTED_CBF_FRAG_COUNT_LSB        6
#define MACRX_CBF_READ_REQUEST_8_REQUESTED_CBF_FRAG_COUNT_MASK       0x000003c0

/* Description		MACRX_CBF_READ_REQUEST_8_RESERVED_8A
			
			<legal 0>
*/
#define MACRX_CBF_READ_REQUEST_8_RESERVED_8A_OFFSET                  0x00000020
#define MACRX_CBF_READ_REQUEST_8_RESERVED_8A_LSB                     10
#define MACRX_CBF_READ_REQUEST_8_RESERVED_8A_MASK                    0x0000fc00

/* Description		MACRX_CBF_READ_REQUEST_9_CBF_FRAGMENT_SIZE
			
			The total number of bytes requested from the PHY for
			this CBF Fragment.
			
			
			
			Can be used by the PHY to double check is this is line
			with what is expected for this Fragment.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_9_CBF_FRAGMENT_SIZE_OFFSET            0x00000024
#define MACRX_CBF_READ_REQUEST_9_CBF_FRAGMENT_SIZE_LSB               0
#define MACRX_CBF_READ_REQUEST_9_CBF_FRAGMENT_SIZE_MASK              0x00003fff

/* Description		MACRX_CBF_READ_REQUEST_9_RESERVED_9A
			
			<legal 0>
*/
#define MACRX_CBF_READ_REQUEST_9_RESERVED_9A_OFFSET                  0x00000024
#define MACRX_CBF_READ_REQUEST_9_RESERVED_9A_LSB                     14
#define MACRX_CBF_READ_REQUEST_9_RESERVED_9A_MASK                    0x0000c000

/* Description		MACRX_CBF_READ_REQUEST_10_SW_PEER_ID
			
			For DEBUG
			
			
			
			An identifier indicating from which AP this CBF is being
			requested. Helps in crosschecking that the MAC and PHY are
			still in sync on what is stored in the cbf_mem_index
			location.
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_10_SW_PEER_ID_OFFSET                  0x00000028
#define MACRX_CBF_READ_REQUEST_10_SW_PEER_ID_LSB                     0
#define MACRX_CBF_READ_REQUEST_10_SW_PEER_ID_MASK                    0x0000ffff

/* Description		MACRX_CBF_READ_REQUEST_11_CBF_DATA_READ_SIZE
			
			The number of bytes MAC will start requesting in each
			MACRX_CBF_DATA_READ_REQUEST. This field shall always have
			the same value as the field 'Byte_count' in that TLV, accept
			for maybe the last generated MACRX_CBF_DATA_READ_REQUEST
			where this value could be less..
			
			
			
			Used by the PHY to prime the DMA engines...
			
			<legal all>
*/
#define MACRX_CBF_READ_REQUEST_11_CBF_DATA_READ_SIZE_OFFSET          0x0000002c
#define MACRX_CBF_READ_REQUEST_11_CBF_DATA_READ_SIZE_LSB             0
#define MACRX_CBF_READ_REQUEST_11_CBF_DATA_READ_SIZE_MASK            0x0000ffff


#endif // _MACRX_CBF_READ_REQUEST_H_
