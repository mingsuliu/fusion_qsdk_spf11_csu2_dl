// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _UPLINK_USER_SETUP_INFO_H_
#define _UPLINK_USER_SETUP_INFO_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	bw_info_valid[0], uplink_receive_type[2:1], reserved_0a[3], uplink_11ax_mcs[7:4], ru_width[14:8], reserved_0b[15], nss[18:16], stream_offset[21:19], sta_dcm[22], sta_coding[23], ru_start_index[30:24], reserved_0c[31]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_UPLINK_USER_SETUP_INFO 1

struct uplink_user_setup_info {
             uint32_t bw_info_valid                   :  1, //[0]
                      uplink_receive_type             :  2, //[2:1]
                      reserved_0a                     :  1, //[3]
                      uplink_11ax_mcs                 :  4, //[7:4]
                      ru_width                        :  7, //[14:8]
                      reserved_0b                     :  1, //[15]
                      nss                             :  3, //[18:16]
                      stream_offset                   :  3, //[21:19]
                      sta_dcm                         :  1, //[22]
                      sta_coding                      :  1, //[23]
                      ru_start_index                  :  7, //[30:24]
                      reserved_0c                     :  1; //[31]
};

/*

bw_info_valid
			
			When clear, non of the info in this STRUCT contains
			valid info
			
			
			
			When set, all the info in this STRUCT contains valid
			information.
			
			
			
			Note that the field where this struct is included is
			indicative for the BW

uplink_receive_type
			
			The type of uplink reception for this user.
			
			
			
			0: ofdma_only
			
			1: mimo_only
			
			2:ofdma_mimo
			
			
			
			<legal 0-2>

reserved_0a
			
			<legal 0>

uplink_11ax_mcs
			
			11ax Modulation and Coding Scheme.
			
			
			
			<legal 0- 11> 

ru_width
			
			The size of the RU for this user.
			
			In units of 1 (26 tone) RU
			
			<legal 1-74>

reserved_0b
			
			<legal 0>

nss
			
			Field only valid in case of Uplink_receive_type ==
			mimo_only OR ofdma_mimo
			
			
			
			Number of Spatial Streams occupied by the User
			
			
			
			<enum 0 1_spatial_stream>Single spatial stream
			
			<enum 1 2_spatial_streams>2 spatial streams
			
			<enum 2 3_spatial_streams>3 spatial streams
			
			<enum 3 4_spatial_streams>4 spatial streams
			
			<enum 4 5_spatial_streams>5 spatial streams
			
			<enum 5 6_spatial_streams>6 spatial streams
			
			<enum 6 7_spatial_streams>7 spatial streams
			
			<enum 7 8_spatial_streams>8 spatial streams

stream_offset
			
			Field only valid in case of Uplink_receive_type ==
			mimo_only OR ofdma_mimo
			
			
			
			Stream Offset from which the User occupies the Streams
			
			
			
			Note MAC:
			
			directly from pdg_fes_setup, based on BW

sta_dcm
			
			
			0: No DCM
			
			1:DCM
			
			<legal all>

sta_coding
			
			Distinguishes between BCC/LDPC
			
			
			
			0: BCC
			
			1: LDPC
			
			<legal all>

ru_start_index
			
			RU index number to which User is assigned
			
			RU numbering is over the entire BW, starting from 0 and
			in increasing frequency order and not primary-secondary
			order
			
			<legal 0-73>

reserved_0c
			
			<legal 0>
*/


/* Description		UPLINK_USER_SETUP_INFO_0_BW_INFO_VALID
			
			When clear, non of the info in this STRUCT contains
			valid info
			
			
			
			When set, all the info in this STRUCT contains valid
			information.
			
			
			
			Note that the field where this struct is included is
			indicative for the BW
*/
#define UPLINK_USER_SETUP_INFO_0_BW_INFO_VALID_OFFSET                0x00000000
#define UPLINK_USER_SETUP_INFO_0_BW_INFO_VALID_LSB                   0
#define UPLINK_USER_SETUP_INFO_0_BW_INFO_VALID_MASK                  0x00000001

/* Description		UPLINK_USER_SETUP_INFO_0_UPLINK_RECEIVE_TYPE
			
			The type of uplink reception for this user.
			
			
			
			0: ofdma_only
			
			1: mimo_only
			
			2:ofdma_mimo
			
			
			
			<legal 0-2>
*/
#define UPLINK_USER_SETUP_INFO_0_UPLINK_RECEIVE_TYPE_OFFSET          0x00000000
#define UPLINK_USER_SETUP_INFO_0_UPLINK_RECEIVE_TYPE_LSB             1
#define UPLINK_USER_SETUP_INFO_0_UPLINK_RECEIVE_TYPE_MASK            0x00000006

/* Description		UPLINK_USER_SETUP_INFO_0_RESERVED_0A
			
			<legal 0>
*/
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0A_OFFSET                  0x00000000
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0A_LSB                     3
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0A_MASK                    0x00000008

/* Description		UPLINK_USER_SETUP_INFO_0_UPLINK_11AX_MCS
			
			11ax Modulation and Coding Scheme.
			
			
			
			<legal 0- 11> 
*/
#define UPLINK_USER_SETUP_INFO_0_UPLINK_11AX_MCS_OFFSET              0x00000000
#define UPLINK_USER_SETUP_INFO_0_UPLINK_11AX_MCS_LSB                 4
#define UPLINK_USER_SETUP_INFO_0_UPLINK_11AX_MCS_MASK                0x000000f0

/* Description		UPLINK_USER_SETUP_INFO_0_RU_WIDTH
			
			The size of the RU for this user.
			
			In units of 1 (26 tone) RU
			
			<legal 1-74>
*/
#define UPLINK_USER_SETUP_INFO_0_RU_WIDTH_OFFSET                     0x00000000
#define UPLINK_USER_SETUP_INFO_0_RU_WIDTH_LSB                        8
#define UPLINK_USER_SETUP_INFO_0_RU_WIDTH_MASK                       0x00007f00

/* Description		UPLINK_USER_SETUP_INFO_0_RESERVED_0B
			
			<legal 0>
*/
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0B_OFFSET                  0x00000000
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0B_LSB                     15
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0B_MASK                    0x00008000

/* Description		UPLINK_USER_SETUP_INFO_0_NSS
			
			Field only valid in case of Uplink_receive_type ==
			mimo_only OR ofdma_mimo
			
			
			
			Number of Spatial Streams occupied by the User
			
			
			
			<enum 0 1_spatial_stream>Single spatial stream
			
			<enum 1 2_spatial_streams>2 spatial streams
			
			<enum 2 3_spatial_streams>3 spatial streams
			
			<enum 3 4_spatial_streams>4 spatial streams
			
			<enum 4 5_spatial_streams>5 spatial streams
			
			<enum 5 6_spatial_streams>6 spatial streams
			
			<enum 6 7_spatial_streams>7 spatial streams
			
			<enum 7 8_spatial_streams>8 spatial streams
*/
#define UPLINK_USER_SETUP_INFO_0_NSS_OFFSET                          0x00000000
#define UPLINK_USER_SETUP_INFO_0_NSS_LSB                             16
#define UPLINK_USER_SETUP_INFO_0_NSS_MASK                            0x00070000

/* Description		UPLINK_USER_SETUP_INFO_0_STREAM_OFFSET
			
			Field only valid in case of Uplink_receive_type ==
			mimo_only OR ofdma_mimo
			
			
			
			Stream Offset from which the User occupies the Streams
			
			
			
			Note MAC:
			
			directly from pdg_fes_setup, based on BW
*/
#define UPLINK_USER_SETUP_INFO_0_STREAM_OFFSET_OFFSET                0x00000000
#define UPLINK_USER_SETUP_INFO_0_STREAM_OFFSET_LSB                   19
#define UPLINK_USER_SETUP_INFO_0_STREAM_OFFSET_MASK                  0x00380000

/* Description		UPLINK_USER_SETUP_INFO_0_STA_DCM
			
			
			0: No DCM
			
			1:DCM
			
			<legal all>
*/
#define UPLINK_USER_SETUP_INFO_0_STA_DCM_OFFSET                      0x00000000
#define UPLINK_USER_SETUP_INFO_0_STA_DCM_LSB                         22
#define UPLINK_USER_SETUP_INFO_0_STA_DCM_MASK                        0x00400000

/* Description		UPLINK_USER_SETUP_INFO_0_STA_CODING
			
			Distinguishes between BCC/LDPC
			
			
			
			0: BCC
			
			1: LDPC
			
			<legal all>
*/
#define UPLINK_USER_SETUP_INFO_0_STA_CODING_OFFSET                   0x00000000
#define UPLINK_USER_SETUP_INFO_0_STA_CODING_LSB                      23
#define UPLINK_USER_SETUP_INFO_0_STA_CODING_MASK                     0x00800000

/* Description		UPLINK_USER_SETUP_INFO_0_RU_START_INDEX
			
			RU index number to which User is assigned
			
			RU numbering is over the entire BW, starting from 0 and
			in increasing frequency order and not primary-secondary
			order
			
			<legal 0-73>
*/
#define UPLINK_USER_SETUP_INFO_0_RU_START_INDEX_OFFSET               0x00000000
#define UPLINK_USER_SETUP_INFO_0_RU_START_INDEX_LSB                  24
#define UPLINK_USER_SETUP_INFO_0_RU_START_INDEX_MASK                 0x7f000000

/* Description		UPLINK_USER_SETUP_INFO_0_RESERVED_0C
			
			<legal 0>
*/
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0C_OFFSET                  0x00000000
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0C_LSB                     31
#define UPLINK_USER_SETUP_INFO_0_RESERVED_0C_MASK                    0x80000000


#endif // _UPLINK_USER_SETUP_INFO_H_
