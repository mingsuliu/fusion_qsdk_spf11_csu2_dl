// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RX_REO_QUEUE_REFERENCE_H_
#define _RX_REO_QUEUE_REFERENCE_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	rx_reo_queue_desc_addr_31_0[31:0]
//	1	rx_reo_queue_desc_addr_39_32[7:0], reserved_1[15:8], receive_queue_number[31:16]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RX_REO_QUEUE_REFERENCE 2

struct rx_reo_queue_reference {
             uint32_t rx_reo_queue_desc_addr_31_0     : 32; //[31:0]
             uint32_t rx_reo_queue_desc_addr_39_32    :  8, //[7:0]
                      reserved_1                      :  8, //[15:8]
                      receive_queue_number            : 16; //[31:16]
};

/*

rx_reo_queue_desc_addr_31_0
			
			Consumer: RXDMA
			
			Producer: RXOLE
			
			
			
			Address (lower 32 bits) of the REO queue descriptor. 
			
			<legal all>

rx_reo_queue_desc_addr_39_32
			
			Consumer: RXDMA
			
			Producer: RXOLE
			
			
			
			Address (upper 8 bits) of the REO queue descriptor. 
			
			<legal all>

reserved_1
			
			<legal 0>

receive_queue_number
			
			Indicates the MPDU queue ID to which this MPDU link
			descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>
*/


/* Description		RX_REO_QUEUE_REFERENCE_0_RX_REO_QUEUE_DESC_ADDR_31_0
			
			Consumer: RXDMA
			
			Producer: RXOLE
			
			
			
			Address (lower 32 bits) of the REO queue descriptor. 
			
			<legal all>
*/
#define RX_REO_QUEUE_REFERENCE_0_RX_REO_QUEUE_DESC_ADDR_31_0_OFFSET  0x00000000
#define RX_REO_QUEUE_REFERENCE_0_RX_REO_QUEUE_DESC_ADDR_31_0_LSB     0
#define RX_REO_QUEUE_REFERENCE_0_RX_REO_QUEUE_DESC_ADDR_31_0_MASK    0xffffffff

/* Description		RX_REO_QUEUE_REFERENCE_1_RX_REO_QUEUE_DESC_ADDR_39_32
			
			Consumer: RXDMA
			
			Producer: RXOLE
			
			
			
			Address (upper 8 bits) of the REO queue descriptor. 
			
			<legal all>
*/
#define RX_REO_QUEUE_REFERENCE_1_RX_REO_QUEUE_DESC_ADDR_39_32_OFFSET 0x00000004
#define RX_REO_QUEUE_REFERENCE_1_RX_REO_QUEUE_DESC_ADDR_39_32_LSB    0
#define RX_REO_QUEUE_REFERENCE_1_RX_REO_QUEUE_DESC_ADDR_39_32_MASK   0x000000ff

/* Description		RX_REO_QUEUE_REFERENCE_1_RESERVED_1
			
			<legal 0>
*/
#define RX_REO_QUEUE_REFERENCE_1_RESERVED_1_OFFSET                   0x00000004
#define RX_REO_QUEUE_REFERENCE_1_RESERVED_1_LSB                      8
#define RX_REO_QUEUE_REFERENCE_1_RESERVED_1_MASK                     0x0000ff00

/* Description		RX_REO_QUEUE_REFERENCE_1_RECEIVE_QUEUE_NUMBER
			
			Indicates the MPDU queue ID to which this MPDU link
			descriptor belongs
			
			Used for tracking and debugging
			
			<legal all>
*/
#define RX_REO_QUEUE_REFERENCE_1_RECEIVE_QUEUE_NUMBER_OFFSET         0x00000004
#define RX_REO_QUEUE_REFERENCE_1_RECEIVE_QUEUE_NUMBER_LSB            16
#define RX_REO_QUEUE_REFERENCE_1_RECEIVE_QUEUE_NUMBER_MASK           0xffff0000


#endif // _RX_REO_QUEUE_REFERENCE_H_
