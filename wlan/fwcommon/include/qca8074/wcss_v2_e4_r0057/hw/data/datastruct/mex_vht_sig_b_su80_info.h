// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "vht_sig_b_su80_info.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<vht_sig_b_su80_info, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"vht_sig_b_su80_info"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(length);
    MEX_PARSE_TLV_FIELD(vhtb_reserved);
    MEX_PARSE_TLV_FIELD(tail);
    MEX_PARSE_TLV_FIELD(reserved_0);
    MEX_PARSE_TLV_FIELD(rx_ndp);
    MEX_PARSE_TLV_FIELD(length_copy_a);
    MEX_PARSE_TLV_FIELD(vhtb_reserved_copy_a);
    MEX_PARSE_TLV_FIELD(tail_copy_a);
    MEX_PARSE_TLV_FIELD(reserved_1);
    MEX_PARSE_TLV_FIELD(rx_ndp_copy_a);
    MEX_PARSE_TLV_FIELD(length_copy_b);
    MEX_PARSE_TLV_FIELD(vhtb_reserved_copy_b);
    MEX_PARSE_TLV_FIELD(tail_copy_b);
    MEX_PARSE_TLV_FIELD(reserved_2);
    MEX_PARSE_TLV_FIELD(rx_ndp_copy_b);
    MEX_PARSE_TLV_FIELD(length_copy_c);
    MEX_PARSE_TLV_FIELD(vhtb_reserved_copy_c);
    MEX_PARSE_TLV_FIELD(tail_copy_c);
    MEX_PARSE_TLV_FIELD(reserved_3);
    MEX_PARSE_TLV_FIELD(rx_ndp_copy_c);
  }
};


