// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "addr_search_entry.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<addr_search_entry, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"addr_search_entry"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(mac_addr_31_0);
    MEX_PARSE_TLV_FIELD(mac_addr_47_32);
    MEX_PARSE_TLV_FIELD(mcast);
    MEX_PARSE_TLV_FIELD(reserved_1a);
    MEX_PARSE_TLV_FIELD(ad1_match);
    MEX_PARSE_TLV_FIELD(lmac_id);
    MEX_PARSE_TLV_FIELD(valid);
    MEX_PARSE_TLV_FIELD(next_hop);
    MEX_PARSE_TLV_FIELD(no_ack);
    MEX_PARSE_TLV_FIELD(dot11ax_dl_ul_flag);
    MEX_PARSE_TLV_FIELD(key_type);
    MEX_PARSE_TLV_FIELD(nc_index);
    MEX_PARSE_TLV_FIELD(ba_size_single_ack_allowed);
    MEX_PARSE_TLV_FIELD(ba_size_32_allowed);
    MEX_PARSE_TLV_FIELD(ba_size_64_allowed);
    MEX_PARSE_TLV_FIELD(ba_size_128_allowed);
    MEX_PARSE_TLV_FIELD(ba_size_256_allowed);
    MEX_PARSE_TLV_FIELD(use_compressed_bar_fragment_nr);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(epd_output);
    MEX_PARSE_TLV_FIELD(vlan_llc_mode);
    MEX_PARSE_TLV_FIELD(use_addrx_search);
    MEX_PARSE_TLV_FIELD(tx_override_non_qos);
    MEX_PARSE_TLV_FIELD(tx_msdu_count);
    MEX_PARSE_TLV_FIELD(reserved_3a);
    MEX_PARSE_TLV_FIELD(tx_timestamp);
    MEX_PARSE_TLV_FIELD(peer_entry_addr_31_0);
    MEX_PARSE_TLV_FIELD(peer_entry_addr_39_32);
    MEX_PARSE_TLV_FIELD(reserved_6a);
    MEX_PARSE_TLV_FIELD(rx_msdu_count);
    MEX_PARSE_TLV_FIELD(reserved_7a);
    MEX_PARSE_TLV_FIELD(rx_timestamp);
    MEX_PARSE_TLV_FIELD(sw_peer_id);
    MEX_PARSE_TLV_FIELD(cbf_response_table_base_index);
    MEX_PARSE_TLV_FIELD(cbf_resp_peer_index);
    MEX_PARSE_TLV_FIELD(cbf_resp_pwr_mgmt);
    MEX_PARSE_TLV_FIELD(sch_response_table_id);
    MEX_PARSE_TLV_FIELD(sta_full_aid);
    MEX_PARSE_TLV_FIELD(sta_partial_aid);
    MEX_PARSE_TLV_FIELD(mesh_sta);
    MEX_PARSE_TLV_FIELD(monitor_sta);
    MEX_PARSE_TLV_FIELD(group_id);
    MEX_PARSE_TLV_FIELD(tm_response_table_base_index);
    MEX_PARSE_TLV_FIELD(ftm_response_table_base_index);
    MEX_PARSE_TLV_FIELD(ftm_fields_valid);
    MEX_PARSE_TLV_FIELD(ftm_pe_nss);
    MEX_PARSE_TLV_FIELD(ftm_pe_ltf_size);
    MEX_PARSE_TLV_FIELD(ftm_pe_content);
    MEX_PARSE_TLV_FIELD(ftm_chain_csd_en);
    MEX_PARSE_TLV_FIELD(ftm_pe_chain_csd_en);
    MEX_PARSE_TLV_FIELD(mpdu_drop);
    MEX_PARSE_TLV_FIELD(ctrl_resp_pwr_mgmt);
    MEX_PARSE_TLV_FIELD(dot11ax_response_rate_source);
    MEX_PARSE_TLV_FIELD(mac_addr_c_31_0);
    MEX_PARSE_TLV_FIELD(mac_addr_c_47_32);
    MEX_PARSE_TLV_FIELD(dot11ax_ext_response_rate_source);
    MEX_PARSE_TLV_FIELD(ps_poll_sifs_resp);
    MEX_PARSE_TLV_FIELD(uapd_sifs_resp);
    MEX_PARSE_TLV_FIELD(qboost_qosdata_sifs_resp);
    MEX_PARSE_TLV_FIELD(qboost_bar_sifs_resp);
    MEX_PARSE_TLV_FIELD(qboost_explicit_sifs_resp);
    MEX_PARSE_TLV_FIELD(reserved_13a);
    MEX_PARSE_TLV_FIELD(capture_channel_type);
    MEX_PARSE_TLV_FIELD(uapsd_trigger_bitmap);
    MEX_PARSE_TLV_FIELD(qboost_trigger_bitmap);
    MEX_PARSE_TLV_FIELD(capture_sounding_1str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_1str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_1str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_1str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_2str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_2str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_2str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_2str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_3str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_3str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_3str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_3str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_4str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_4str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_4str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_4str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_5str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_5str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_5str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_5str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_6str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_6str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_6str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_6str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_7str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_7str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_7str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_7str_160mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_8str_20mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_8str_40mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_8str_80mhz);
    MEX_PARSE_TLV_FIELD(capture_sounding_8str_160mhz);
  }
};


