// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "rx_mpdu_link.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<rx_mpdu_link, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"rx_mpdu_link"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(descriptor_header);
    MEX_PARSE_TLV_FIELD(start_mpdu_sequence_number);
    MEX_PARSE_TLV_FIELD(reserved_1a);
    MEX_PARSE_TLV_FIELD(receive_mpdu_queue_number);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(reserved_3a);
    MEX_PARSE_TLV_FIELD(mpdu_0);
    MEX_PARSE_TLV_FIELD(mpdu_1);
    MEX_PARSE_TLV_FIELD(mpdu_2);
    MEX_PARSE_TLV_FIELD(mpdu_3);
    MEX_PARSE_TLV_FIELD(mpdu_4);
    MEX_PARSE_TLV_FIELD(mpdu_5);
    MEX_PARSE_TLV_FIELD(mpdu_6);
  }
};


