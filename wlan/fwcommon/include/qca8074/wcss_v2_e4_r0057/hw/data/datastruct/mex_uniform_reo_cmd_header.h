// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "uniform_reo_cmd_header.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<uniform_reo_cmd_header, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"uniform_reo_cmd_header"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(reo_cmd_number);
    MEX_PARSE_TLV_FIELD(reo_status_required);
    MEX_PARSE_TLV_FIELD(reserved_0a);
  }
};


