// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "rx_mpdu_desc_info.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<rx_mpdu_desc_info, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"rx_mpdu_desc_info"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(msdu_count);
    MEX_PARSE_TLV_FIELD(mpdu_sequence_number);
    MEX_PARSE_TLV_FIELD(fragment_flag);
    MEX_PARSE_TLV_FIELD(mpdu_retry_bit);
    MEX_PARSE_TLV_FIELD(ampdu_flag);
    MEX_PARSE_TLV_FIELD(bar_frame);
    MEX_PARSE_TLV_FIELD(pn_fields_contain_valid_info);
    MEX_PARSE_TLV_FIELD(sa_is_valid);
    MEX_PARSE_TLV_FIELD(sa_idx_timeout);
    MEX_PARSE_TLV_FIELD(da_is_valid);
    MEX_PARSE_TLV_FIELD(da_is_mcbc);
    MEX_PARSE_TLV_FIELD(da_idx_timeout);
    MEX_PARSE_TLV_FIELD(raw_mpdu);
    MEX_PARSE_TLV_FIELD(more_fragment_flag);
    MEX_PARSE_TLV_FIELD(peer_meta_data);
  }
};


