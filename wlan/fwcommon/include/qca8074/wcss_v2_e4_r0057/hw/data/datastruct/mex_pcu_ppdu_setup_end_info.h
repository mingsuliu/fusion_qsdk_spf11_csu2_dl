// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "pcu_ppdu_setup_end_info.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<pcu_ppdu_setup_end_info, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"pcu_ppdu_setup_end_info"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(current_tx_duration);
    MEX_PARSE_TLV_FIELD(coex_limited_fes);
    MEX_PARSE_TLV_FIELD(final_bw_based_number_of_users);
    MEX_PARSE_TLV_FIELD(md_user_reduction_type);
    MEX_PARSE_TLV_FIELD(md_user_reduction_first_user);
    MEX_PARSE_TLV_FIELD(md_user_reduction_last_user);
    MEX_PARSE_TLV_FIELD(reserved_0a);
    MEX_PARSE_TLV_FIELD(next_rx_duration);
    MEX_PARSE_TLV_FIELD(remaining_fes_time);
    MEX_PARSE_TLV_FIELD(duration);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(pdg_sifs_response_valid);
    MEX_PARSE_TLV_FIELD(pdg_sifs_response_bw);
    MEX_PARSE_TLV_FIELD(reserved_2b);
  }
};


