// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RECEIVED_RESPONSE_USER_INFO_H_
#define _RECEIVED_RESPONSE_USER_INFO_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	mpdu_fcs_pass_count[7:0], mpdu_fcs_fail_count[15:8], data_frame_count[23:16], qosnull_frame_count[27:24], reserved_0a[30:28], user_info_valid[31]
//	1	null_delimiter_count[15:0], reserved_1a[31:16]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RECEIVED_RESPONSE_USER_INFO 2

struct received_response_user_info {
             uint32_t mpdu_fcs_pass_count             :  8, //[7:0]
                      mpdu_fcs_fail_count             :  8, //[15:8]
                      data_frame_count                :  8, //[23:16]
                      qosnull_frame_count             :  4, //[27:24]
                      reserved_0a                     :  3, //[30:28]
                      user_info_valid                 :  1; //[31]
             uint32_t null_delimiter_count            : 16, //[15:0]
                      reserved_1a                     : 16; //[31:16]
};

/*

mpdu_fcs_pass_count
			
			The number of MPDUs received with correct FCS.
			
			<legal all>

mpdu_fcs_fail_count
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>

data_frame_count
			
			The number of data frames received with correct FCS.
			
			<legal all>

qosnull_frame_count
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>

reserved_0a
			
			<legal 0>

user_info_valid
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>

null_delimiter_count
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>

reserved_1a
			
			<legal 0>
*/


/* Description		RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_PASS_COUNT_OFFSET     0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_PASS_COUNT_LSB        0
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_PASS_COUNT_MASK       0x000000ff

/* Description		RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_FAIL_COUNT_OFFSET     0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_FAIL_COUNT_LSB        8
#define RECEIVED_RESPONSE_USER_INFO_0_MPDU_FCS_FAIL_COUNT_MASK       0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_INFO_0_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_DATA_FRAME_COUNT_OFFSET        0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_DATA_FRAME_COUNT_LSB           16
#define RECEIVED_RESPONSE_USER_INFO_0_DATA_FRAME_COUNT_MASK          0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_INFO_0_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_QOSNULL_FRAME_COUNT_OFFSET     0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_QOSNULL_FRAME_COUNT_LSB        24
#define RECEIVED_RESPONSE_USER_INFO_0_QOSNULL_FRAME_COUNT_MASK       0x0f000000

/* Description		RECEIVED_RESPONSE_USER_INFO_0_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_RESERVED_0A_OFFSET             0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_RESERVED_0A_LSB                28
#define RECEIVED_RESPONSE_USER_INFO_0_RESERVED_0A_MASK               0x70000000

/* Description		RECEIVED_RESPONSE_USER_INFO_0_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_0_USER_INFO_VALID_OFFSET         0x00000000
#define RECEIVED_RESPONSE_USER_INFO_0_USER_INFO_VALID_LSB            31
#define RECEIVED_RESPONSE_USER_INFO_0_USER_INFO_VALID_MASK           0x80000000

/* Description		RECEIVED_RESPONSE_USER_INFO_1_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_INFO_1_NULL_DELIMITER_COUNT_OFFSET    0x00000004
#define RECEIVED_RESPONSE_USER_INFO_1_NULL_DELIMITER_COUNT_LSB       0
#define RECEIVED_RESPONSE_USER_INFO_1_NULL_DELIMITER_COUNT_MASK      0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_INFO_1_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_INFO_1_RESERVED_1A_OFFSET             0x00000004
#define RECEIVED_RESPONSE_USER_INFO_1_RESERVED_1A_LSB                16
#define RECEIVED_RESPONSE_USER_INFO_1_RESERVED_1A_MASK               0xffff0000


#endif // _RECEIVED_RESPONSE_USER_INFO_H_
