// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "l_sig_b_info.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<l_sig_b_info, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"l_sig_b_info"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(rate);
    MEX_PARSE_TLV_FIELD(length);
    MEX_PARSE_TLV_FIELD(reserved);
  }
};


