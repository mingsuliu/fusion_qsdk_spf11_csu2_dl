// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "rx_frame_bitmap_req.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<rx_frame_bitmap_req, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"rx_frame_bitmap_req"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(explicit_user_request);
    MEX_PARSE_TLV_FIELD(user_request_type);
    MEX_PARSE_TLV_FIELD(user_number);
    MEX_PARSE_TLV_FIELD(sw_peer_id);
    MEX_PARSE_TLV_FIELD(tid_specific_request);
    MEX_PARSE_TLV_FIELD(requested_tid);
    MEX_PARSE_TLV_FIELD(reserved_0);
  }
};


