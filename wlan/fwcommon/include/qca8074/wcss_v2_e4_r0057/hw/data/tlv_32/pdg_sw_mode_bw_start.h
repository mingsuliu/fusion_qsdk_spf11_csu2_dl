// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _PDG_SW_MODE_BW_START_H_
#define _PDG_SW_MODE_BW_START_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	transmit_bw_sw_tlvs[1:0], reserved_0a[31:2]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_PDG_SW_MODE_BW_START 1

struct pdg_sw_mode_bw_start {
             uint32_t transmit_bw_sw_tlvs             :  2, //[1:0]
                      reserved_0a                     : 30; //[31:2]
};

/*

transmit_bw_sw_tlvs
			
			The bandwidth for which all the TLVs that follow are
			applicable. That Bandwidth selection ends with the
			PDG_SW_MODE_BW_END TLV.
			
			
			
			<enum 0     20_mhz_sw_tlvs>
			
			<enum 1     40_mhz_sw_tlvs >
			
			<enum 2     80_mhz_sw_tlvs >
			
			<enum 3     160_mhz_sw_tlvs > 
			
			<legal 0-3>

reserved_0a
			
			<legal 0>
*/


/* Description		PDG_SW_MODE_BW_START_0_TRANSMIT_BW_SW_TLVS
			
			The bandwidth for which all the TLVs that follow are
			applicable. That Bandwidth selection ends with the
			PDG_SW_MODE_BW_END TLV.
			
			
			
			<enum 0     20_mhz_sw_tlvs>
			
			<enum 1     40_mhz_sw_tlvs >
			
			<enum 2     80_mhz_sw_tlvs >
			
			<enum 3     160_mhz_sw_tlvs > 
			
			<legal 0-3>
*/
#define PDG_SW_MODE_BW_START_0_TRANSMIT_BW_SW_TLVS_OFFSET            0x00000000
#define PDG_SW_MODE_BW_START_0_TRANSMIT_BW_SW_TLVS_LSB               0
#define PDG_SW_MODE_BW_START_0_TRANSMIT_BW_SW_TLVS_MASK              0x00000003

/* Description		PDG_SW_MODE_BW_START_0_RESERVED_0A
			
			<legal 0>
*/
#define PDG_SW_MODE_BW_START_0_RESERVED_0A_OFFSET                    0x00000000
#define PDG_SW_MODE_BW_START_0_RESERVED_0A_LSB                       2
#define PDG_SW_MODE_BW_START_0_RESERVED_0A_MASK                      0xfffffffc


#endif // _PDG_SW_MODE_BW_START_H_
