// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MACTX_EXPECT_CBF_COMMON_H_
#define _MACTX_EXPECT_CBF_COMMON_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	cbf_user_count[5:0], reserved_0a[7:6], cbf_format[8], reserved_0b[15:9], number_of_user_tlvs[23:16], reserved_0c[31:24]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_MACTX_EXPECT_CBF_COMMON 1

struct mactx_expect_cbf_common {
             uint32_t cbf_user_count                  :  6, //[5:0]
                      reserved_0a                     :  2, //[7:6]
                      cbf_format                      :  1, //[8]
                      reserved_0b                     :  7, //[15:9]
                      number_of_user_tlvs             :  8, //[23:16]
                      reserved_0c                     :  8; //[31:24]
};

/*

cbf_user_count
			
			The number of users in the upcoming CBF Frame. This will
			be set to 1 for non-triggered CBF_RX.
			
			<legal 1-37>

reserved_0a
			
			<legal 0>

cbf_format
			
			The CBF type. This is used to decode the MIMO CTRL
			field.
			
			
			
			0: 11ac
			
			1: 11ax
			
			
			
			<legal all>

reserved_0b
			
			<legal 0>

number_of_user_tlvs
			
			
			
			
			<legal 1-255>

reserved_0c
			
			<legal 0>
*/


/* Description		MACTX_EXPECT_CBF_COMMON_0_CBF_USER_COUNT
			
			The number of users in the upcoming CBF Frame. This will
			be set to 1 for non-triggered CBF_RX.
			
			<legal 1-37>
*/
#define MACTX_EXPECT_CBF_COMMON_0_CBF_USER_COUNT_OFFSET              0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_CBF_USER_COUNT_LSB                 0
#define MACTX_EXPECT_CBF_COMMON_0_CBF_USER_COUNT_MASK                0x0000003f

/* Description		MACTX_EXPECT_CBF_COMMON_0_RESERVED_0A
			
			<legal 0>
*/
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0A_OFFSET                 0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0A_LSB                    6
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0A_MASK                   0x000000c0

/* Description		MACTX_EXPECT_CBF_COMMON_0_CBF_FORMAT
			
			The CBF type. This is used to decode the MIMO CTRL
			field.
			
			
			
			0: 11ac
			
			1: 11ax
			
			
			
			<legal all>
*/
#define MACTX_EXPECT_CBF_COMMON_0_CBF_FORMAT_OFFSET                  0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_CBF_FORMAT_LSB                     8
#define MACTX_EXPECT_CBF_COMMON_0_CBF_FORMAT_MASK                    0x00000100

/* Description		MACTX_EXPECT_CBF_COMMON_0_RESERVED_0B
			
			<legal 0>
*/
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0B_OFFSET                 0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0B_LSB                    9
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0B_MASK                   0x0000fe00

/* Description		MACTX_EXPECT_CBF_COMMON_0_NUMBER_OF_USER_TLVS
			
			
			
			
			<legal 1-255>
*/
#define MACTX_EXPECT_CBF_COMMON_0_NUMBER_OF_USER_TLVS_OFFSET         0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_NUMBER_OF_USER_TLVS_LSB            16
#define MACTX_EXPECT_CBF_COMMON_0_NUMBER_OF_USER_TLVS_MASK           0x00ff0000

/* Description		MACTX_EXPECT_CBF_COMMON_0_RESERVED_0C
			
			<legal 0>
*/
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0C_OFFSET                 0x00000000
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0C_LSB                    24
#define MACTX_EXPECT_CBF_COMMON_0_RESERVED_0C_MASK                   0xff000000


#endif // _MACTX_EXPECT_CBF_COMMON_H_
