// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "reo_descriptor_threshold_reached_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<reo_descriptor_threshold_reached_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"reo_descriptor_threshold_reached_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(status_header);
    MEX_PARSE_TLV_FIELD(threshold_index);
    MEX_PARSE_TLV_FIELD(reserved_2);
    MEX_PARSE_TLV_FIELD(link_descriptor_counter0);
    MEX_PARSE_TLV_FIELD(reserved_3);
    MEX_PARSE_TLV_FIELD(link_descriptor_counter1);
    MEX_PARSE_TLV_FIELD(reserved_4);
    MEX_PARSE_TLV_FIELD(link_descriptor_counter2);
    MEX_PARSE_TLV_FIELD(reserved_5);
    MEX_PARSE_TLV_FIELD(link_descriptor_counter_sum);
    MEX_PARSE_TLV_FIELD(reserved_6);
    MEX_PARSE_TLV_FIELD(reserved_7);
    MEX_PARSE_TLV_FIELD(reserved_8);
    MEX_PARSE_TLV_FIELD(reserved_9a);
    MEX_PARSE_TLV_FIELD(reserved_10a);
    MEX_PARSE_TLV_FIELD(reserved_11a);
    MEX_PARSE_TLV_FIELD(reserved_12a);
    MEX_PARSE_TLV_FIELD(reserved_13a);
    MEX_PARSE_TLV_FIELD(reserved_14a);
    MEX_PARSE_TLV_FIELD(reserved_15a);
    MEX_PARSE_TLV_FIELD(reserved_16a);
    MEX_PARSE_TLV_FIELD(reserved_17a);
    MEX_PARSE_TLV_FIELD(reserved_18a);
    MEX_PARSE_TLV_FIELD(reserved_19a);
    MEX_PARSE_TLV_FIELD(reserved_20a);
    MEX_PARSE_TLV_FIELD(reserved_21a);
    MEX_PARSE_TLV_FIELD(reserved_22a);
    MEX_PARSE_TLV_FIELD(reserved_23a);
    MEX_PARSE_TLV_FIELD(reserved_24a);
    MEX_PARSE_TLV_FIELD(looping_count);
  }
};


