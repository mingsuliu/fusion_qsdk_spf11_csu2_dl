// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "txpcu_buffer_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<txpcu_buffer_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"txpcu_buffer_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(txpcu_basix_buffer_info);
    MEX_PARSE_TLV_FIELD(reserved);
    MEX_PARSE_TLV_FIELD(tx_data_sync_value);
  }
};


