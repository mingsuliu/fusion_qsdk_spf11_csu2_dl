// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "tqm_add_msdu_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<tqm_add_msdu_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"tqm_add_msdu_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(status_header);
    MEX_PARSE_TLV_FIELD(msdu_count);
    MEX_PARSE_TLV_FIELD(ran_out_of_link_descriptors);
    MEX_PARSE_TLV_FIELD(msdu_length_is_zero_err);
    MEX_PARSE_TLV_FIELD(flow_byte_count_overflow);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(flow_queue_addr_31_0);
    MEX_PARSE_TLV_FIELD(flow_queue_addr_39_32);
    MEX_PARSE_TLV_FIELD(reserved_4a);
    MEX_PARSE_TLV_FIELD(msdu_0);
    MEX_PARSE_TLV_FIELD(tx_flow_number);
    MEX_PARSE_TLV_FIELD(reserved_9a);
    MEX_PARSE_TLV_FIELD(reserved_10a);
    MEX_PARSE_TLV_FIELD(looping_count);
  }
};


