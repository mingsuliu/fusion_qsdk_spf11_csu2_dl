// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "phyrx_rssi_legacy.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<phyrx_rssi_legacy, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"phyrx_rssi_legacy"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(reception_type);
    MEX_PARSE_TLV_FIELD(rx_chain_mask_type);
    MEX_PARSE_TLV_FIELD(reserved_0);
    MEX_PARSE_TLV_FIELD(receive_bandwidth);
    MEX_PARSE_TLV_FIELD(rx_chain_mask);
    MEX_PARSE_TLV_FIELD(phy_ppdu_id);
    MEX_PARSE_TLV_FIELD(sw_phy_meta_data);
    MEX_PARSE_TLV_FIELD(ppdu_start_timestamp);
    MEX_PARSE_TLV_FIELD(pre_rssi_info_details);
    MEX_PARSE_TLV_FIELD(preamble_rssi_info_details);
    MEX_PARSE_TLV_FIELD(pre_rssi_comb);
    MEX_PARSE_TLV_FIELD(rssi_comb);
    MEX_PARSE_TLV_FIELD(normalized_pre_rssi_comb);
    MEX_PARSE_TLV_FIELD(normalized_rssi_comb);
  }
};


