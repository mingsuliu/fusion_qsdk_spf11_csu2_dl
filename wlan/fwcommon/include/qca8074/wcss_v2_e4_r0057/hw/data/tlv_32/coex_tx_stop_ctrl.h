// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _COEX_TX_STOP_CTRL_H_
#define _COEX_TX_STOP_CTRL_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	tx_hard_abort[0], tx_soft_abort[1], sch_backoff_expiration_block[2], txpcu_response_block[3], cxc_abort_reason_details[17:4], wlan_status_at_abort_details[31:18]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_COEX_TX_STOP_CTRL 1

struct coex_tx_stop_ctrl {
             uint32_t tx_hard_abort                   :  1, //[0]
                      tx_soft_abort                   :  1, //[1]
                      sch_backoff_expiration_block    :  1, //[2]
                      txpcu_response_block            :  1, //[3]
                      cxc_abort_reason_details        : 14, //[17:4]
                      wlan_status_at_abort_details    : 14; //[31:18]
};

/*

tx_hard_abort
			
			When set, any ongoing transmission (and it's related
			FES) should be aborted.
			
			This includes self generated responses.
			
			The transmission on the medium should be aborted and all
			MAC module TX related statemachines should return to idle.
			
			
			
			When the SCH module sees this bit is set, it will
			generate a flush command

tx_soft_abort
			
			When set, any ongoing transmission on the medium should
			be aborted, but the PHY will act to the MAC as all is still
			fine.
			
			 
			
			From the MAC perspective, the transmit seems to be going
			on and therefor all MAC statemachines will continue behaving
			as normal. If a response frame is expected, the TXPCU will
			wait for it and and generate the TX_FES_STATUS tlv as in any
			normal scenario.

sch_backoff_expiration_block
			
			SCH is not allowed to initiate any new transmissions.
			TXPCU however is still allowed to initiate self gen response
			frames.
			
			
			
			If any backoff based transmission (or FES) is going on
			at time of reception of this TLV, that transmission (or FES)
			is allowed to finish.
			
			
			
			When set, SCH will block all backoff engines from
			expiration till a new COEX_TX_STOP_CTRL TLV is received
			where this bit is NOT set.
			
			
			
			Note that when this bit is set, backoff engines are
			allowed to count down, just not expire...

txpcu_response_block
			
			TCPCU is not allowed to initiate any new response
			transmissions. 
			
			
			
			If any response based transmission is going on at time
			of reception of this TLV, that transmission is allowed to
			finish.
			
			
			
			When set, TXPCU will block all reponse transmissions
			till a new COEX_TX_STOP_CTRL TLV is received where this bit
			is NOT set.

cxc_abort_reason_details
			
			Information related to what BT/WAN/wlan activity
			
			
			
			For detailed info see doc: TBD
			
			<legal all>

wlan_status_at_abort_details
			
			Information related to what WLAN activity (according to
			CXC) was ongoing at the time that  resulted in CXC module
			deciding to send an abort to the WLAN
			
			
			
			For detailed info see doc: TBD
			
			<legal all>
*/


/* Description		COEX_TX_STOP_CTRL_0_TX_HARD_ABORT
			
			When set, any ongoing transmission (and it's related
			FES) should be aborted.
			
			This includes self generated responses.
			
			The transmission on the medium should be aborted and all
			MAC module TX related statemachines should return to idle.
			
			
			
			When the SCH module sees this bit is set, it will
			generate a flush command
*/
#define COEX_TX_STOP_CTRL_0_TX_HARD_ABORT_OFFSET                     0x00000000
#define COEX_TX_STOP_CTRL_0_TX_HARD_ABORT_LSB                        0
#define COEX_TX_STOP_CTRL_0_TX_HARD_ABORT_MASK                       0x00000001

/* Description		COEX_TX_STOP_CTRL_0_TX_SOFT_ABORT
			
			When set, any ongoing transmission on the medium should
			be aborted, but the PHY will act to the MAC as all is still
			fine.
			
			 
			
			From the MAC perspective, the transmit seems to be going
			on and therefor all MAC statemachines will continue behaving
			as normal. If a response frame is expected, the TXPCU will
			wait for it and and generate the TX_FES_STATUS tlv as in any
			normal scenario.
*/
#define COEX_TX_STOP_CTRL_0_TX_SOFT_ABORT_OFFSET                     0x00000000
#define COEX_TX_STOP_CTRL_0_TX_SOFT_ABORT_LSB                        1
#define COEX_TX_STOP_CTRL_0_TX_SOFT_ABORT_MASK                       0x00000002

/* Description		COEX_TX_STOP_CTRL_0_SCH_BACKOFF_EXPIRATION_BLOCK
			
			SCH is not allowed to initiate any new transmissions.
			TXPCU however is still allowed to initiate self gen response
			frames.
			
			
			
			If any backoff based transmission (or FES) is going on
			at time of reception of this TLV, that transmission (or FES)
			is allowed to finish.
			
			
			
			When set, SCH will block all backoff engines from
			expiration till a new COEX_TX_STOP_CTRL TLV is received
			where this bit is NOT set.
			
			
			
			Note that when this bit is set, backoff engines are
			allowed to count down, just not expire...
*/
#define COEX_TX_STOP_CTRL_0_SCH_BACKOFF_EXPIRATION_BLOCK_OFFSET      0x00000000
#define COEX_TX_STOP_CTRL_0_SCH_BACKOFF_EXPIRATION_BLOCK_LSB         2
#define COEX_TX_STOP_CTRL_0_SCH_BACKOFF_EXPIRATION_BLOCK_MASK        0x00000004

/* Description		COEX_TX_STOP_CTRL_0_TXPCU_RESPONSE_BLOCK
			
			TCPCU is not allowed to initiate any new response
			transmissions. 
			
			
			
			If any response based transmission is going on at time
			of reception of this TLV, that transmission is allowed to
			finish.
			
			
			
			When set, TXPCU will block all reponse transmissions
			till a new COEX_TX_STOP_CTRL TLV is received where this bit
			is NOT set.
*/
#define COEX_TX_STOP_CTRL_0_TXPCU_RESPONSE_BLOCK_OFFSET              0x00000000
#define COEX_TX_STOP_CTRL_0_TXPCU_RESPONSE_BLOCK_LSB                 3
#define COEX_TX_STOP_CTRL_0_TXPCU_RESPONSE_BLOCK_MASK                0x00000008

/* Description		COEX_TX_STOP_CTRL_0_CXC_ABORT_REASON_DETAILS
			
			Information related to what BT/WAN/wlan activity
			
			
			
			For detailed info see doc: TBD
			
			<legal all>
*/
#define COEX_TX_STOP_CTRL_0_CXC_ABORT_REASON_DETAILS_OFFSET          0x00000000
#define COEX_TX_STOP_CTRL_0_CXC_ABORT_REASON_DETAILS_LSB             4
#define COEX_TX_STOP_CTRL_0_CXC_ABORT_REASON_DETAILS_MASK            0x0003fff0

/* Description		COEX_TX_STOP_CTRL_0_WLAN_STATUS_AT_ABORT_DETAILS
			
			Information related to what WLAN activity (according to
			CXC) was ongoing at the time that  resulted in CXC module
			deciding to send an abort to the WLAN
			
			
			
			For detailed info see doc: TBD
			
			<legal all>
*/
#define COEX_TX_STOP_CTRL_0_WLAN_STATUS_AT_ABORT_DETAILS_OFFSET      0x00000000
#define COEX_TX_STOP_CTRL_0_WLAN_STATUS_AT_ABORT_DETAILS_LSB         18
#define COEX_TX_STOP_CTRL_0_WLAN_STATUS_AT_ABORT_DETAILS_MASK        0xfffc0000


#endif // _COEX_TX_STOP_CTRL_H_
