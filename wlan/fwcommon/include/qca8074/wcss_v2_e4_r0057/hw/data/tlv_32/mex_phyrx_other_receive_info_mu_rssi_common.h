// Copyright (c) 2018 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "phyrx_other_receive_info_mu_rssi_common.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<phyrx_other_receive_info_mu_rssi_common, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"phyrx_other_receive_info_mu_rssi_common"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(min_total_gain);
    MEX_PARSE_TLV_FIELD(reserved);
  }
};


