// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_2_SCH_MPDU_AVAILABLE_H_
#define _TQM_2_SCH_MPDU_AVAILABLE_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	tqm_status_number[23:0], reserved_0a[31:24]
//	1	tx_mpdu_queue_number[15:0], reserved_1a[31:16]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_2_SCH_MPDU_AVAILABLE 2

struct tqm_2_sch_mpdu_available {
             uint32_t tqm_status_number               : 24, //[23:0]
                      reserved_0a                     :  8; //[31:24]
             uint32_t tx_mpdu_queue_number            : 16, //[15:0]
                      reserved_1a                     : 16; //[31:16]
};

/*

tqm_status_number
			
			Consumer: Debug
			
			Producer: TQM 
			
			
			
			The value in this field is equal to value of the
			'TQM_CMD_Number' field of the TQM command that resulted in
			this TLV being send to the SCH
			
			
			
			<legal all> 

reserved_0a
			
			<legal 0>

tx_mpdu_queue_number
			
			Indicates the MPDU queue that is not empty
			
			<legal all>

reserved_1a
			
			<legal 0>
*/


/* Description		TQM_2_SCH_MPDU_AVAILABLE_0_TQM_STATUS_NUMBER
			
			Consumer: Debug
			
			Producer: TQM 
			
			
			
			The value in this field is equal to value of the
			'TQM_CMD_Number' field of the TQM command that resulted in
			this TLV being send to the SCH
			
			
			
			<legal all> 
*/
#define TQM_2_SCH_MPDU_AVAILABLE_0_TQM_STATUS_NUMBER_OFFSET          0x00000000
#define TQM_2_SCH_MPDU_AVAILABLE_0_TQM_STATUS_NUMBER_LSB             0
#define TQM_2_SCH_MPDU_AVAILABLE_0_TQM_STATUS_NUMBER_MASK            0x00ffffff

/* Description		TQM_2_SCH_MPDU_AVAILABLE_0_RESERVED_0A
			
			<legal 0>
*/
#define TQM_2_SCH_MPDU_AVAILABLE_0_RESERVED_0A_OFFSET                0x00000000
#define TQM_2_SCH_MPDU_AVAILABLE_0_RESERVED_0A_LSB                   24
#define TQM_2_SCH_MPDU_AVAILABLE_0_RESERVED_0A_MASK                  0xff000000

/* Description		TQM_2_SCH_MPDU_AVAILABLE_1_TX_MPDU_QUEUE_NUMBER
			
			Indicates the MPDU queue that is not empty
			
			<legal all>
*/
#define TQM_2_SCH_MPDU_AVAILABLE_1_TX_MPDU_QUEUE_NUMBER_OFFSET       0x00000004
#define TQM_2_SCH_MPDU_AVAILABLE_1_TX_MPDU_QUEUE_NUMBER_LSB          0
#define TQM_2_SCH_MPDU_AVAILABLE_1_TX_MPDU_QUEUE_NUMBER_MASK         0x0000ffff

/* Description		TQM_2_SCH_MPDU_AVAILABLE_1_RESERVED_1A
			
			<legal 0>
*/
#define TQM_2_SCH_MPDU_AVAILABLE_1_RESERVED_1A_OFFSET                0x00000004
#define TQM_2_SCH_MPDU_AVAILABLE_1_RESERVED_1A_LSB                   16
#define TQM_2_SCH_MPDU_AVAILABLE_1_RESERVED_1A_MASK                  0xffff0000


#endif // _TQM_2_SCH_MPDU_AVAILABLE_H_
