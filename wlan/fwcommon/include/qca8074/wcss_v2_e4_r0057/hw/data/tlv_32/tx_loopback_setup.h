// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TX_LOOPBACK_SETUP_H_
#define _TX_LOOPBACK_SETUP_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	nr_back_to_back_req[9:0], wait_interval[19:10], reserved[31:20]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TX_LOOPBACK_SETUP 1

struct tx_loopback_setup {
             uint32_t nr_back_to_back_req             : 10, //[9:0]
                      wait_interval                   : 10, //[19:10]
                      reserved                        : 12; //[31:20]
};

/*

nr_back_to_back_req
			
			The number of back to back word requests that the
			loopback module shall generate
			
			
			
			<legal all>

wait_interval
			
			After finishing the back to back data requests, this
			interval specifies the number of clk cycles that loopback
			module will NOT generate any data requests
			
			
			
			<legal all>

reserved
			
			<legal 0>
*/


/* Description		TX_LOOPBACK_SETUP_0_NR_BACK_TO_BACK_REQ
			
			The number of back to back word requests that the
			loopback module shall generate
			
			
			
			<legal all>
*/
#define TX_LOOPBACK_SETUP_0_NR_BACK_TO_BACK_REQ_OFFSET               0x00000000
#define TX_LOOPBACK_SETUP_0_NR_BACK_TO_BACK_REQ_LSB                  0
#define TX_LOOPBACK_SETUP_0_NR_BACK_TO_BACK_REQ_MASK                 0x000003ff

/* Description		TX_LOOPBACK_SETUP_0_WAIT_INTERVAL
			
			After finishing the back to back data requests, this
			interval specifies the number of clk cycles that loopback
			module will NOT generate any data requests
			
			
			
			<legal all>
*/
#define TX_LOOPBACK_SETUP_0_WAIT_INTERVAL_OFFSET                     0x00000000
#define TX_LOOPBACK_SETUP_0_WAIT_INTERVAL_LSB                        10
#define TX_LOOPBACK_SETUP_0_WAIT_INTERVAL_MASK                       0x000ffc00

/* Description		TX_LOOPBACK_SETUP_0_RESERVED
			
			<legal 0>
*/
#define TX_LOOPBACK_SETUP_0_RESERVED_OFFSET                          0x00000000
#define TX_LOOPBACK_SETUP_0_RESERVED_LSB                             20
#define TX_LOOPBACK_SETUP_0_RESERVED_MASK                            0xfff00000


#endif // _TX_LOOPBACK_SETUP_H_
