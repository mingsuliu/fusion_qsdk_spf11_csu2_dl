// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "ole_buf_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<ole_buf_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"ole_buf_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(avail_ole_buf_bytes);
    MEX_PARSE_TLV_FIELD(avail_ole_cmd_entries);
    MEX_PARSE_TLV_FIELD(reserved);
  }
};


