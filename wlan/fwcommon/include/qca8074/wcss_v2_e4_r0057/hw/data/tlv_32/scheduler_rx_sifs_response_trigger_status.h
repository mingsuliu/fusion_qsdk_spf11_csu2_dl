// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_H_
#define _SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "received_trigger_info_details.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-4	struct received_trigger_info_details received_trigger_details;
//	5	sch_timestamp_l32[31:0]
//	6	sch_timestamp_u32[31:0]
//	7	reserved_7[31:0]
//	8	reserved_8[31:0]
//	9	reserved_9[31:0]
//	10	reserved_10[31:0]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS 11

struct scheduler_rx_sifs_response_trigger_status {
    struct            received_trigger_info_details                       received_trigger_details;
             uint32_t sch_timestamp_l32               : 32; //[31:0]
             uint32_t sch_timestamp_u32               : 32; //[31:0]
             uint32_t reserved_7                      : 32; //[31:0]
             uint32_t reserved_8                      : 32; //[31:0]
             uint32_t reserved_9                      : 32; //[31:0]
             uint32_t reserved_10                     : 32; //[31:0]
};

/*

struct received_trigger_info_details received_trigger_details
			
			Info related to the type of trigger (that potentially
			requires SIFS response) that was received

sch_timestamp_l32
			
			The timestamp (based on always running system timer) at
			which the scheduler received the RECEIVED_TRIGGER_INFO TLV
			from RXPCU

sch_timestamp_u32
			
			Upper portion of the timestamp 

reserved_7
			
			<legal 0>

reserved_8
			
			<legal 0>

reserved_9
			
			<legal 0>

reserved_10
			
			<legal 0>
*/


 /* EXTERNAL REFERENCE : struct received_trigger_info_details received_trigger_details */ 


/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_TYPE
			
			This field indicates for what type of trigger has been
			received
			
			
			
			<enum 0 SCH_Qboost_trigger> 
			
			<enum 1 SCH_PSPOLL_trigger>
			
			<enum 2 SCH_UAPSD_trigger>
			
			<enum 3 SCH_11ax_trigger> 
			
			Field AX_trigger_type indicates the ID of the received
			trigger
			
			<enum 4 SCH_11ax_wildcard_trigger> 
			
			Field AX_trigger_type indicates the ID of the received
			trigger
			
			<enum 5 SCH_11ax_unassoc_wildcard_trigger > 
			
			Field AX_trigger_type indicates the ID of the received
			trigger
			
			
			
			<legal 0-5>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_TYPE_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_TYPE_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_TYPE_MASK 0x0000000f

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_SOURCE
			
			Field Only valid when Trigger_type  is an 11ax related
			trigger 
			
			
			
			<enum 0 11ax_trigger_frame>
			
			<enum 1 he_control_based_trigger>
			
			
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_SOURCE_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_SOURCE_LSB 4
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_SOURCE_MASK 0x00000010

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_TYPE
			
			Field Only valid when Trigger_type  is an 11ax related
			trigger 
			
			
			
			The 11AX trigger type/ trigger number
			
			It identifies which trigger was received.
			
			
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_TYPE_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_TYPE_LSB 5
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_AX_TRIGGER_TYPE_MASK 0x000001e0

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_SOURCE_STA_FULL_AID
			
			The sta_full_aid of the sta/ap that generated the
			trigger.
			
			Comes from the address_search_entry
			
			
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_SOURCE_STA_FULL_AID_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_SOURCE_STA_FULL_AID_LSB 9
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_TRIGGER_SOURCE_STA_FULL_AID_MASK 0x003ffe00

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_VALID
			
			When set, the 'frame_control' field contains valid info
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_VALID_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_VALID_LSB 22
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_VALID_MASK 0x00400000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_VALID
			
			When set, the 'QoS_control' field contains valid info
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_VALID_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_VALID_LSB 23
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_VALID_MASK 0x00800000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_INFO_VALID
			
			When set, the 'HE control' field contains valid info
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_INFO_VALID_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_INFO_VALID_LSB 24
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_INFO_VALID_MASK 0x01000000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_RESERVED_0B
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_RESERVED_0B_OFFSET 0x00000000
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_RESERVED_0B_LSB 25
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_0_RECEIVED_TRIGGER_DETAILS_RESERVED_0B_MASK 0xfe000000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_PHY_PPDU_ID
			
			A ppdu counter value that PHY increments for every PPDU
			received. The counter value wraps around  
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_PHY_PPDU_ID_OFFSET 0x00000004
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_PHY_PPDU_ID_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_PHY_PPDU_ID_MASK 0x0000ffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_LSIG_RESPONSE_LENGTH
			
			Field only valid in case of OFDMA trigger
			
			
			
			Indicates the value of the L-SIG Length field of the HE
			trigger-based PPDU that is the response to the Trigger frame
			
			
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_LSIG_RESPONSE_LENGTH_OFFSET 0x00000004
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_LSIG_RESPONSE_LENGTH_LSB 16
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_LSIG_RESPONSE_LENGTH_MASK 0x0fff0000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_RESERVED_1A
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_RESERVED_1A_OFFSET 0x00000004
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_RESERVED_1A_LSB 28
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_1_RECEIVED_TRIGGER_DETAILS_RESERVED_1A_MASK 0xf0000000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL
			
			frame control field of the received frame
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_OFFSET 0x00000008
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_FRAME_CONTROL_MASK 0x0000ffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL
			
			frame control field of the received frame (if present)
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_OFFSET 0x00000008
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_LSB 16
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_2_RECEIVED_TRIGGER_DETAILS_QOS_CONTROL_MASK 0xffff0000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_SW_PEER_ID
			
			A unique identifier for this STA. Extracted from the
			Address_Search_Entry
			
			
			
			Used by the SCH to find linkage between this trigger and
			potentially pre-programmed responses.
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_SW_PEER_ID_OFFSET 0x0000000c
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_SW_PEER_ID_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_SW_PEER_ID_MASK 0x0000ffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_RESERVED_3A
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_RESERVED_3A_OFFSET 0x0000000c
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_RESERVED_3A_LSB 16
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_3_RECEIVED_TRIGGER_DETAILS_RESERVED_3A_MASK 0xffff0000

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_4_RECEIVED_TRIGGER_DETAILS_HE_CONTROL
			
			Field only valid when HE_control_info_valid is set
			
			
			
			This is the 'RAW HE_CONTROL field' that was present in
			the frame.
			
			<legal all>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_4_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_OFFSET 0x00000010
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_4_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_4_RECEIVED_TRIGGER_DETAILS_HE_CONTROL_MASK 0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_5_SCH_TIMESTAMP_L32
			
			The timestamp (based on always running system timer) at
			which the scheduler received the RECEIVED_TRIGGER_INFO TLV
			from RXPCU
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_5_SCH_TIMESTAMP_L32_OFFSET 0x00000014
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_5_SCH_TIMESTAMP_L32_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_5_SCH_TIMESTAMP_L32_MASK 0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_6_SCH_TIMESTAMP_U32
			
			Upper portion of the timestamp 
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_6_SCH_TIMESTAMP_U32_OFFSET 0x00000018
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_6_SCH_TIMESTAMP_U32_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_6_SCH_TIMESTAMP_U32_MASK 0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_7_RESERVED_7
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_7_RESERVED_7_OFFSET 0x0000001c
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_7_RESERVED_7_LSB   0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_7_RESERVED_7_MASK  0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_8_RESERVED_8
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_8_RESERVED_8_OFFSET 0x00000020
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_8_RESERVED_8_LSB   0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_8_RESERVED_8_MASK  0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_9_RESERVED_9
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_9_RESERVED_9_OFFSET 0x00000024
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_9_RESERVED_9_LSB   0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_9_RESERVED_9_MASK  0xffffffff

/* Description		SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_10_RESERVED_10
			
			<legal 0>
*/
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_10_RESERVED_10_OFFSET 0x00000028
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_10_RESERVED_10_LSB 0
#define SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_10_RESERVED_10_MASK 0xffffffff


#endif // _SCHEDULER_RX_SIFS_RESPONSE_TRIGGER_STATUS_H_
