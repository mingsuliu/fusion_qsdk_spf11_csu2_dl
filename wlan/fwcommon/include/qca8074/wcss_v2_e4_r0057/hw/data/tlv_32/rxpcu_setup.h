// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RXPCU_SETUP_H_
#define _RXPCU_SETUP_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	user_count[5:0], clear_previous_settings[6], clear_all_bitmaps[7], set_freeze_reason_ack_resp_to_tm_ftm[8], good_ampdu_delimiter_threshold[15:9], bad_ampdu_delimiter_threshold[31:16]
//	1	sr_clear_sent_setting[0], sr_set_obss_pd_prohibited[1], force_rx_ppdu_stats_generation[2], gen_received_response_user_tlvs[3], reserved_1a[31:4]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RXPCU_SETUP 2

struct rxpcu_setup {
             uint32_t user_count                      :  6, //[5:0]
                      clear_previous_settings         :  1, //[6]
                      clear_all_bitmaps               :  1, //[7]
                      set_freeze_reason_ack_resp_to_tm_ftm:  1, //[8]
                      good_ampdu_delimiter_threshold  :  7, //[15:9]
                      bad_ampdu_delimiter_threshold   : 16; //[31:16]
             uint32_t sr_clear_sent_setting           :  1, //[0]
                      sr_set_obss_pd_prohibited       :  1, //[1]
                      force_rx_ppdu_stats_generation  :  1, //[2]
                      gen_received_response_user_tlvs :  1, //[3]
                      reserved_1a                     : 28; //[31:4]
};

/*

user_count
			
			Consumer: RXPCU
			
			Producer: SW
			
			
			
			(for debug only)
			
			The number of users to be expected in upcoming
			receptions
			
			<legal all>

clear_previous_settings
			
			When set, clear all setting from previous received
			RXPCU_(USER)_SETUP related TLVs.
			
			If this bit is not set, do not clear those but just
			overwrite those fields/users for which new information gets
			received.
			
			<legal all>

clear_all_bitmaps
			
			When set, RXPCU is requested to clear all bitmaps. This
			is used typically when a trigger frame is going to be
			transmitted, and after all data is received, the smallest BA
			responses can be generated
			
			<legal all>

set_freeze_reason_ack_resp_to_tm_ftm
			
			A TM or FTM frame is being transmitted and (ACK)
			response is expected. When a (=> any) response is received
			with passing FCS, RXPCU shall generate the
			MACRX_FREEZE_CAPTURE_CHANNEL TLV with Capture_reason set to
			freeze_reason_ACK_resp_to_TM_FTM after PHYRX_PKT_END TLV has
			been received.

good_ampdu_delimiter_threshold
			
			When equal to 0, not AMPDU delimiter checks are needed.
			
			
			
			When > 0 
			
			field Bad_AMPDU_delimiter_threshold must also be > 0. 
			
			RXPCU shall check that the number of good AMPDU
			delimiters received reaches the
			Good_AMPDU_delimiter_threshold value before the
			Bad_AMPDU_delimiter_threshold is reached. If so, RXPCU shall
			allow the reception to finish. 
			
			When RXPCU determines that the
			Bad_AMPDU_delimiter_threshold is reached before the
			Good_AMPDU_delimiter_threshold, RXPCU shall terminate the
			reception by sending MACRX_ABORT_REQUEST to the PHYRX, with
			reason: macrx_abort_too_much_bad_data
			
			
			
			In case of an SU reception, the good and bad delimiters
			thresholds are applied to the single device.
			
			In case of an MU UL reception, the good and bad
			delimiter thresholds are applied to the aggregate of the MU
			UL devices. In other words, if from two different users, a
			good MPDU delimiter is seen, the aggregate good MPDU count
			is now two. Same rules apply  to the bad MPDU count
			
			
			
			<legal all>

bad_ampdu_delimiter_threshold
			
			When equal to 0, not AMPDU delimiter checks are needed.
			
			
			
			When > 0 
			
			field Good_AMPDU_delimiter_threshold must also be > 0. 
			
			For details, see description of field:
			Good_AMPDU_delimiter_threshold
			
			<legal all>

sr_clear_sent_setting
			
			When set, RXPCU shall clear it's internal flag
			indicating the types of Spatial Reuse parameters
			transmitted.
			
			SW shall typically set this bit for all beacon
			transmissions
			
			<legal all>

sr_set_obss_pd_prohibited
			
			When set, a transmission is initialized where TXVECTOR
			parameter SPATIAL_REUSE is set to the value
			SRP_and_NON_SRG_OBSS_PD_PROHIBITED.
			
			<legal all>

force_rx_ppdu_stats_generation
			
			SW can set this in combination with expecting (MU) UL
			reception
			
			
			
			1'b0: no action needed
			
			1'b1: RXPCU, after the MU- UL reception, irrespective of
			having received any MPDUs with address filter pass for any
			of the users or any MPDU FCS pass for any of the users,
			RXPCU shall generated the RX_PPDU_STATUS related TLVs. This
			is needed for SW rate adaptation purposes..
			
			
			
			<legal all>

gen_received_response_user_tlvs
			
			When set, RXPCU shall generate all the
			RECEIVED_RESPONSE_USER TLVs for which a response was
			expected.
			
			
			
			<legal all>

reserved_1a
			
			<legal 0>
*/


/* Description		RXPCU_SETUP_0_USER_COUNT
			
			Consumer: RXPCU
			
			Producer: SW
			
			
			
			(for debug only)
			
			The number of users to be expected in upcoming
			receptions
			
			<legal all>
*/
#define RXPCU_SETUP_0_USER_COUNT_OFFSET                              0x00000000
#define RXPCU_SETUP_0_USER_COUNT_LSB                                 0
#define RXPCU_SETUP_0_USER_COUNT_MASK                                0x0000003f

/* Description		RXPCU_SETUP_0_CLEAR_PREVIOUS_SETTINGS
			
			When set, clear all setting from previous received
			RXPCU_(USER)_SETUP related TLVs.
			
			If this bit is not set, do not clear those but just
			overwrite those fields/users for which new information gets
			received.
			
			<legal all>
*/
#define RXPCU_SETUP_0_CLEAR_PREVIOUS_SETTINGS_OFFSET                 0x00000000
#define RXPCU_SETUP_0_CLEAR_PREVIOUS_SETTINGS_LSB                    6
#define RXPCU_SETUP_0_CLEAR_PREVIOUS_SETTINGS_MASK                   0x00000040

/* Description		RXPCU_SETUP_0_CLEAR_ALL_BITMAPS
			
			When set, RXPCU is requested to clear all bitmaps. This
			is used typically when a trigger frame is going to be
			transmitted, and after all data is received, the smallest BA
			responses can be generated
			
			<legal all>
*/
#define RXPCU_SETUP_0_CLEAR_ALL_BITMAPS_OFFSET                       0x00000000
#define RXPCU_SETUP_0_CLEAR_ALL_BITMAPS_LSB                          7
#define RXPCU_SETUP_0_CLEAR_ALL_BITMAPS_MASK                         0x00000080

/* Description		RXPCU_SETUP_0_SET_FREEZE_REASON_ACK_RESP_TO_TM_FTM
			
			A TM or FTM frame is being transmitted and (ACK)
			response is expected. When a (=> any) response is received
			with passing FCS, RXPCU shall generate the
			MACRX_FREEZE_CAPTURE_CHANNEL TLV with Capture_reason set to
			freeze_reason_ACK_resp_to_TM_FTM after PHYRX_PKT_END TLV has
			been received.
*/
#define RXPCU_SETUP_0_SET_FREEZE_REASON_ACK_RESP_TO_TM_FTM_OFFSET    0x00000000
#define RXPCU_SETUP_0_SET_FREEZE_REASON_ACK_RESP_TO_TM_FTM_LSB       8
#define RXPCU_SETUP_0_SET_FREEZE_REASON_ACK_RESP_TO_TM_FTM_MASK      0x00000100

/* Description		RXPCU_SETUP_0_GOOD_AMPDU_DELIMITER_THRESHOLD
			
			When equal to 0, not AMPDU delimiter checks are needed.
			
			
			
			When > 0 
			
			field Bad_AMPDU_delimiter_threshold must also be > 0. 
			
			RXPCU shall check that the number of good AMPDU
			delimiters received reaches the
			Good_AMPDU_delimiter_threshold value before the
			Bad_AMPDU_delimiter_threshold is reached. If so, RXPCU shall
			allow the reception to finish. 
			
			When RXPCU determines that the
			Bad_AMPDU_delimiter_threshold is reached before the
			Good_AMPDU_delimiter_threshold, RXPCU shall terminate the
			reception by sending MACRX_ABORT_REQUEST to the PHYRX, with
			reason: macrx_abort_too_much_bad_data
			
			
			
			In case of an SU reception, the good and bad delimiters
			thresholds are applied to the single device.
			
			In case of an MU UL reception, the good and bad
			delimiter thresholds are applied to the aggregate of the MU
			UL devices. In other words, if from two different users, a
			good MPDU delimiter is seen, the aggregate good MPDU count
			is now two. Same rules apply  to the bad MPDU count
			
			
			
			<legal all>
*/
#define RXPCU_SETUP_0_GOOD_AMPDU_DELIMITER_THRESHOLD_OFFSET          0x00000000
#define RXPCU_SETUP_0_GOOD_AMPDU_DELIMITER_THRESHOLD_LSB             9
#define RXPCU_SETUP_0_GOOD_AMPDU_DELIMITER_THRESHOLD_MASK            0x0000fe00

/* Description		RXPCU_SETUP_0_BAD_AMPDU_DELIMITER_THRESHOLD
			
			When equal to 0, not AMPDU delimiter checks are needed.
			
			
			
			When > 0 
			
			field Good_AMPDU_delimiter_threshold must also be > 0. 
			
			For details, see description of field:
			Good_AMPDU_delimiter_threshold
			
			<legal all>
*/
#define RXPCU_SETUP_0_BAD_AMPDU_DELIMITER_THRESHOLD_OFFSET           0x00000000
#define RXPCU_SETUP_0_BAD_AMPDU_DELIMITER_THRESHOLD_LSB              16
#define RXPCU_SETUP_0_BAD_AMPDU_DELIMITER_THRESHOLD_MASK             0xffff0000

/* Description		RXPCU_SETUP_1_SR_CLEAR_SENT_SETTING
			
			When set, RXPCU shall clear it's internal flag
			indicating the types of Spatial Reuse parameters
			transmitted.
			
			SW shall typically set this bit for all beacon
			transmissions
			
			<legal all>
*/
#define RXPCU_SETUP_1_SR_CLEAR_SENT_SETTING_OFFSET                   0x00000004
#define RXPCU_SETUP_1_SR_CLEAR_SENT_SETTING_LSB                      0
#define RXPCU_SETUP_1_SR_CLEAR_SENT_SETTING_MASK                     0x00000001

/* Description		RXPCU_SETUP_1_SR_SET_OBSS_PD_PROHIBITED
			
			When set, a transmission is initialized where TXVECTOR
			parameter SPATIAL_REUSE is set to the value
			SRP_and_NON_SRG_OBSS_PD_PROHIBITED.
			
			<legal all>
*/
#define RXPCU_SETUP_1_SR_SET_OBSS_PD_PROHIBITED_OFFSET               0x00000004
#define RXPCU_SETUP_1_SR_SET_OBSS_PD_PROHIBITED_LSB                  1
#define RXPCU_SETUP_1_SR_SET_OBSS_PD_PROHIBITED_MASK                 0x00000002

/* Description		RXPCU_SETUP_1_FORCE_RX_PPDU_STATS_GENERATION
			
			SW can set this in combination with expecting (MU) UL
			reception
			
			
			
			1'b0: no action needed
			
			1'b1: RXPCU, after the MU- UL reception, irrespective of
			having received any MPDUs with address filter pass for any
			of the users or any MPDU FCS pass for any of the users,
			RXPCU shall generated the RX_PPDU_STATUS related TLVs. This
			is needed for SW rate adaptation purposes..
			
			
			
			<legal all>
*/
#define RXPCU_SETUP_1_FORCE_RX_PPDU_STATS_GENERATION_OFFSET          0x00000004
#define RXPCU_SETUP_1_FORCE_RX_PPDU_STATS_GENERATION_LSB             2
#define RXPCU_SETUP_1_FORCE_RX_PPDU_STATS_GENERATION_MASK            0x00000004

/* Description		RXPCU_SETUP_1_GEN_RECEIVED_RESPONSE_USER_TLVS
			
			When set, RXPCU shall generate all the
			RECEIVED_RESPONSE_USER TLVs for which a response was
			expected.
			
			
			
			<legal all>
*/
#define RXPCU_SETUP_1_GEN_RECEIVED_RESPONSE_USER_TLVS_OFFSET         0x00000004
#define RXPCU_SETUP_1_GEN_RECEIVED_RESPONSE_USER_TLVS_LSB            3
#define RXPCU_SETUP_1_GEN_RECEIVED_RESPONSE_USER_TLVS_MASK           0x00000008

/* Description		RXPCU_SETUP_1_RESERVED_1A
			
			<legal 0>
*/
#define RXPCU_SETUP_1_RESERVED_1A_OFFSET                             0x00000004
#define RXPCU_SETUP_1_RESERVED_1A_LSB                                4
#define RXPCU_SETUP_1_RESERVED_1A_MASK                               0xfffffff0


#endif // _RXPCU_SETUP_H_
