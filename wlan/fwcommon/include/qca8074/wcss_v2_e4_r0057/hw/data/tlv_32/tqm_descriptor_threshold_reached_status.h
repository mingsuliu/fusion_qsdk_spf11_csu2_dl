// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_H_
#define _TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_status_header.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct uniform_tqm_status_header status_header;
//	2	threshold_index[1:0], reserved_2a[31:2]
//	3	link_descriptor_counter0[23:0], reserved_3a[31:24]
//	4	link_descriptor_counter1[23:0], reserved_4a[31:24]
//	5	link_descriptor_counter2[23:0], reserved_5a[31:24]
//	6	link_descriptor_counter_sum[25:0], reserved_6a[31:26]
//	7	reserved_7a[31:0]
//	8	reserved_8a[31:0]
//	9	reserved_9a[31:0]
//	10	reserved_10a[27:0], looping_count[31:28]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS 11

struct tqm_descriptor_threshold_reached_status {
    struct            uniform_tqm_status_header                       status_header;
             uint32_t threshold_index                 :  2, //[1:0]
                      reserved_2a                     : 30; //[31:2]
             uint32_t link_descriptor_counter0        : 24, //[23:0]
                      reserved_3a                     :  8; //[31:24]
             uint32_t link_descriptor_counter1        : 24, //[23:0]
                      reserved_4a                     :  8; //[31:24]
             uint32_t link_descriptor_counter2        : 24, //[23:0]
                      reserved_5a                     :  8; //[31:24]
             uint32_t link_descriptor_counter_sum     : 26, //[25:0]
                      reserved_6a                     :  6; //[31:26]
             uint32_t reserved_7a                     : 32; //[31:0]
             uint32_t reserved_8a                     : 32; //[31:0]
             uint32_t reserved_9a                     : 32; //[31:0]
             uint32_t reserved_10a                    : 28, //[27:0]
                      looping_count                   :  4; //[31:28]
};

/*

struct uniform_tqm_status_header status_header
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			Details that can link this status with the original
			command. It also contains info on how long TQM took to
			execute this command.
			
			
			
			Cmd_Execution_time is set to 0
			
			TQM_cmd_execution_status is set to
			tqm_successful_execution
			
			Cmd_execution_was_paused is set to 0

threshold_index
			
			The index of the threshold register whose value got
			reached
			
			
			
			<enum 0     tqm_desc_counter0_threshold>
			
			<enum 1     tqm_desc_counter1_threshold>
			
			<enum 2     tqm_desc_counter2_threshold>
			
			<enum 3     tqm_desc_counter_sum_threshold>
			
			
			
			<legal all>

reserved_2a
			
			<legal 0>

link_descriptor_counter0
			
			Value of this counter at generation of this message
			
			<legal all>

reserved_3a
			
			<legal 0>

link_descriptor_counter1
			
			Value of this counter at generation of this message
			
			<legal all>

reserved_4a
			
			<legal 0>

link_descriptor_counter2
			
			Value of this counter at generation of this message
			
			<legal all>

reserved_5a
			
			<legal 0>

link_descriptor_counter_sum
			
			Value of this counter at generation of this message
			
			<legal all>

reserved_6a
			
			<legal 0>

reserved_7a
			
			<legal 0>

reserved_8a
			
			<legal 0>

reserved_9a
			
			<legal 0>

reserved_10a
			
			<legal 0>

looping_count
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/


 /* EXTERNAL REFERENCE : struct uniform_tqm_status_header status_header */ 


/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER
			
			Consumer: SW , DEBUG
			
			Producer: TQM 
			
			
			
			The value in this field is equal to value of the
			'TQM_CMD_Number' field the TQM command or the
			'TQM_add_cmd_Number' field from the TQM entrance ring
			descriptor
			
			
			
			This field helps to correlate the statuses with the TQM
			commands.
			
			
			
			<legal all> 
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_OFFSET 0x00000000
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_MASK 0x00ffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME
			
			Consumer: DEBUG
			
			Producer: TQM 
			
			
			
			The amount of time TQM took to excecute the command.
			Note that this time does not include the duration of the
			command waiting in the command ring, before the execution
			started.
			
			
			
			The counter saturates at 0xF
			
			
			
			Field 'Cmd_Execution_time_unit' indicates what the time
			unit is of this field.
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_OFFSET 0x00000000
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_LSB 24
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_MASK 0x0f000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT
			
			The time unit for field Cmd_Execution_time
			
			<enum 0 time_unit_2_us> The time unit is 2 us
			
			<enum 1 time_unit_32_us> The time unit is 32 us
			
			
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_OFFSET 0x00000000
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_LSB 28
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_MASK 0x10000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS
			
			Consumer: DEBUG
			
			Producer: TQM 
			
			
			
			Execution status of the command.
			
			
			
			<enum 0 tqm_successful_execution> Command has
			successfully been executed
			
			<enum 1 tqm_failed_with_invalid_descriptor> queue or
			flow descriptor valid bit is not set. None of the status
			fields in the entire STATUS TLV are valid, accept when
			explicitly mentioned in the field description
			
			<enum 2 tqm_resource_blocked> Command is NOT  executed
			(in the normal way or maybe only partially) because one or
			more descriptors were blocked OR the entire cache was
			blocked. Part that is still executed is command specific...
			See command details. None of the status fields in the entire
			STATUS TLV are valid.
			
			<enum 3 tqm_sch_flush> Command is partially or not
			executed at all due to SCH generating a flush command.
			
			
			
			<legal  0-3>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_OFFSET 0x00000000
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_LSB 29
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_MASK 0x60000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED
			
			Consumer: DEBUG/SW
			
			Producer: TQM 
			
			
			
			This field is only applicable for commands that were
			given in the SW command ring to TQM. For the HW ring, which
			is filled by the scheduler this field is always set to 0
			
			
			
			When set, the command execution was temporarily paused
			because a command from the HW ring (filled by SCH)
			interrupted the execution of this command.
			
			Note that when this status is generated, the 'pause' has
			ended and TQM has resumed the command execution and has now
			finished it.
			
			
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_OFFSET 0x00000000
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_LSB 31
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_MASK 0x80000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_1_STATUS_HEADER_TIMESTAMP
			
			Timestamp at the moment that this status report is
			written.
			
			
			
			In us, based of the global timer
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_1_STATUS_HEADER_TIMESTAMP_OFFSET 0x00000004
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_1_STATUS_HEADER_TIMESTAMP_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_1_STATUS_HEADER_TIMESTAMP_MASK 0xffffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_THRESHOLD_INDEX
			
			The index of the threshold register whose value got
			reached
			
			
			
			<enum 0     tqm_desc_counter0_threshold>
			
			<enum 1     tqm_desc_counter1_threshold>
			
			<enum 2     tqm_desc_counter2_threshold>
			
			<enum 3     tqm_desc_counter_sum_threshold>
			
			
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_THRESHOLD_INDEX_OFFSET 0x00000008
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_THRESHOLD_INDEX_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_THRESHOLD_INDEX_MASK 0x00000003

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_RESERVED_2A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_RESERVED_2A_OFFSET 0x00000008
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_RESERVED_2A_LSB    2
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_2_RESERVED_2A_MASK   0xfffffffc

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_LINK_DESCRIPTOR_COUNTER0
			
			Value of this counter at generation of this message
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_LINK_DESCRIPTOR_COUNTER0_OFFSET 0x0000000c
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_LINK_DESCRIPTOR_COUNTER0_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_LINK_DESCRIPTOR_COUNTER0_MASK 0x00ffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_RESERVED_3A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_RESERVED_3A_OFFSET 0x0000000c
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_RESERVED_3A_LSB    24
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_3_RESERVED_3A_MASK   0xff000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_LINK_DESCRIPTOR_COUNTER1
			
			Value of this counter at generation of this message
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_LINK_DESCRIPTOR_COUNTER1_OFFSET 0x00000010
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_LINK_DESCRIPTOR_COUNTER1_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_LINK_DESCRIPTOR_COUNTER1_MASK 0x00ffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_RESERVED_4A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_RESERVED_4A_OFFSET 0x00000010
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_RESERVED_4A_LSB    24
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_4_RESERVED_4A_MASK   0xff000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_LINK_DESCRIPTOR_COUNTER2
			
			Value of this counter at generation of this message
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_LINK_DESCRIPTOR_COUNTER2_OFFSET 0x00000014
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_LINK_DESCRIPTOR_COUNTER2_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_LINK_DESCRIPTOR_COUNTER2_MASK 0x00ffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_RESERVED_5A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_RESERVED_5A_OFFSET 0x00000014
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_RESERVED_5A_LSB    24
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_5_RESERVED_5A_MASK   0xff000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_LINK_DESCRIPTOR_COUNTER_SUM
			
			Value of this counter at generation of this message
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_LINK_DESCRIPTOR_COUNTER_SUM_OFFSET 0x00000018
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_LINK_DESCRIPTOR_COUNTER_SUM_LSB 0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_LINK_DESCRIPTOR_COUNTER_SUM_MASK 0x03ffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_RESERVED_6A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_RESERVED_6A_OFFSET 0x00000018
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_RESERVED_6A_LSB    26
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_6_RESERVED_6A_MASK   0xfc000000

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_7_RESERVED_7A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_7_RESERVED_7A_OFFSET 0x0000001c
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_7_RESERVED_7A_LSB    0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_7_RESERVED_7A_MASK   0xffffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_8_RESERVED_8A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_8_RESERVED_8A_OFFSET 0x00000020
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_8_RESERVED_8A_LSB    0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_8_RESERVED_8A_MASK   0xffffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_9_RESERVED_9A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_9_RESERVED_9A_OFFSET 0x00000024
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_9_RESERVED_9A_LSB    0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_9_RESERVED_9A_MASK   0xffffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_RESERVED_10A
			
			<legal 0>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_RESERVED_10A_OFFSET 0x00000028
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_RESERVED_10A_LSB  0
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_RESERVED_10A_MASK 0x0fffffff

/* Description		TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_LOOPING_COUNT
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_LOOPING_COUNT_OFFSET 0x00000028
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_LOOPING_COUNT_LSB 28
#define TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_10_LOOPING_COUNT_MASK 0xf0000000


#endif // _TQM_DESCRIPTOR_THRESHOLD_REACHED_STATUS_H_
