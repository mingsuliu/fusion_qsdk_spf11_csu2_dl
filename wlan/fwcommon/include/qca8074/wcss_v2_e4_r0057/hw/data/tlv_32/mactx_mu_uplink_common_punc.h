// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MACTX_MU_UPLINK_COMMON_PUNC_H_
#define _MACTX_MU_UPLINK_COMMON_PUNC_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uplink_common_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-4	struct uplink_common_info uplink_common_info_details_pat4567;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_MACTX_MU_UPLINK_COMMON_PUNC 5

struct mactx_mu_uplink_common_punc {
    struct            uplink_common_info                       uplink_common_info_details_pat4567;
};

/*

struct uplink_common_info uplink_common_info_details_pat4567
			
			Contains information related to expected UL OFDMA/MIMO 
*/


 /* EXTERNAL REFERENCE : struct uplink_common_info uplink_common_info_details_pat4567 */ 


/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_INFO_VALID
			
			When set, all bw20_... fields contain valid info
			
			
			
			Note that  bw20 also corresponds to Puncture Pattern 0
			in case of punctured transmissions.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_INFO_VALID_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_INFO_VALID_LSB 0
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_INFO_VALID_MASK 0x00000001

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_STBC
			
			Field valid when Bw20_info_valid is set
			
			
			
			When set, use STBC transmission rates
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_STBC_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_STBC_LSB 1
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_STBC_MASK 0x00000002

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_SGI
			
			Field valid when Bw20_info_valid is set
			
			
			
			<enum 0     gi_0_8_us > HE related GI
			
			<enum 1     gi_0_4_us > HE related GI <enum 2    
			gi_1_6_us > HE related GI
			
			<enum 3     gi_3_2_us > HE related GI
			
			<legal 0 - 3>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_SGI_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_SGI_LSB 2
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_SGI_MASK 0x0000000c

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_FRAMELESS_MU_RX
			
			Field valid when Bw20_info_valid is set
			
			
			
			This is set, when PHY RX will have to do a MU reception,
			but it is already known that PHY will not receive any real
			data that the MAC can decode. For this scenario, PHY should
			still send all non Data TLVs for the MU reception, but block
			all data TLVs.
			
			Used in for example MU_RTS/CTS 
			
			exchanges.
			
			This feature might not get used in the end, and is more
			of a placeholder in case something like this is needed
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_FRAMELESS_MU_RX_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_FRAMELESS_MU_RX_LSB 4
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_FRAMELESS_MU_RX_MASK 0x00000010

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0A
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0A_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0A_LSB 5
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0A_MASK 0x000000e0

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_USERS
			
			Field valid when Bw20_info_valid is set
			
			
			
			Number of users for 20 Mhz uplink reception.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_USERS_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_USERS_LSB 8
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_USERS_MASK 0x00003f00

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0B
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0B_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0B_LSB 14
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0B_MASK 0x0000c000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LDPC_EXTRA_SYMBOL
			
			If LDPC, 
			
			  0: LDPC extra symbol not present
			
			  1: LDPC extra symbol present
			
			Else 
			
			  Set to 1
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LDPC_EXTRA_SYMBOL_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LDPC_EXTRA_SYMBOL_LSB 16
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LDPC_EXTRA_SYMBOL_MASK 0x00010000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LTF_SIZE
			
			Ltf size
			
			
			
			<enum 0     ltf_1x > 
			
			<enum 1     ltf_2x > 
			
			<enum 2     ltf_4x > 
			
			<legal 0 - 2>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LTF_SIZE_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LTF_SIZE_LSB 17
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_LTF_SIZE_MASK 0x00060000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_LTF_SYMBOLS
			
			Indicates the number of HE-LTF symbols
			
			
			
			0: 1 symbol
			
			1: 2 symbols
			
			2: 3 symbols
			
			3: 4 symbols
			
			4: 5 symbols
			
			5: 6 symbols
			
			6: 7 symbols
			
			7: 8 symbols
			
			
			
			NOTE that this encoding is different from what is in
			Num_LTF_symbols in the HE_SIG_A_MU_DL
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_LTF_SYMBOLS_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_LTF_SYMBOLS_LSB 19
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_NUM_LTF_SYMBOLS_MASK 0x00380000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_A_FACTOR
			
			The packet extension duration of the trigger-based PPDU
			response with these two bits indicating the a-factor 
			
			
			
			<enum 0 a_factor_4>
			
			<enum 1 a_factor_1>
			
			<enum 2 a_factor_2>
			
			<enum 3 a_factor_3>
			
			
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_A_FACTOR_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_A_FACTOR_LSB 22
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_A_FACTOR_MASK 0x00c00000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_PE_DISAMBIGUITY
			
			The packet extension duration of the trigger-based PPDU
			response with this bit indicating the PE-Disambiguity 
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_PE_DISAMBIGUITY_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_PE_DISAMBIGUITY_LSB 24
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_PE_DISAMBIGUITY_MASK 0x01000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_DURATION
			
			<enum 0     packet_ext_duration_0> 0 us duration
			
			<enum 1     packet_ext_duration_4> 4 us duration
			
			<enum 2     packet_ext_duration_8> 8 us duration
			
			<enum 3     packet_ext_duration_12> 12 us duration
			
			<enum 4     packet_ext_duration_16> 16 us duration
			
			<legal 0 - 4>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_DURATION_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_DURATION_LSB 25
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW20_PACKET_EXTENSION_DURATION_MASK 0x0e000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0C
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0C_OFFSET 0x00000000
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0C_LSB 28
#define MACTX_MU_UPLINK_COMMON_PUNC_0_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_0C_MASK 0xf0000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_INFO_VALID
			
			When set, all bw40_... fields contain valid info
			
			
			
			Note that  bw40 also corresponds to Puncture Pattern 1
			in case of punctured transmissions.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_INFO_VALID_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_INFO_VALID_LSB 0
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_INFO_VALID_MASK 0x00000001

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_STBC
			
			Field valid when Bw40_info_valid is set
			
			
			
			When set, use STBC transmission rates
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_STBC_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_STBC_LSB 1
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_STBC_MASK 0x00000002

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_SGI
			
			Field valid when Bw40_info_valid is set
			
			
			
			<enum 0     gi_0_8_us > HE related GI
			
			<enum 1     gi_0_4_us > HE related GI <enum 2    
			gi_1_6_us > HE related GI
			
			<enum 3     gi_3_2_us > HE related GI
			
			<legal 0 - 3>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_SGI_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_SGI_LSB 2
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_SGI_MASK 0x0000000c

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_FRAMELESS_MU_RX
			
			Field valid when Bw40_info_valid is set
			
			
			
			This is set, when PHY RX will have to do a MU reception,
			but it is already known that PHY will not receive any real
			data that the MAC can decode. For this scenario, PHY should
			still send all non Data TLVs for the MU reception, but block
			all data TLVs.
			
			Used in for example MU_RTS/CTS exchanges.
			
			This feature might not get used in the end, and is more
			of a placeholder in case something like this is needed
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_FRAMELESS_MU_RX_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_FRAMELESS_MU_RX_LSB 4
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_FRAMELESS_MU_RX_MASK 0x00000010

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1A
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1A_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1A_LSB 5
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1A_MASK 0x000000e0

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_USERS
			
			Field valid when Bw40_info_valid is set
			
			
			
			Number of users for 40 Mhz uplink reception.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_USERS_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_USERS_LSB 8
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_USERS_MASK 0x00003f00

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1B
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1B_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1B_LSB 14
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1B_MASK 0x0000c000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LDPC_EXTRA_SYMBOL
			
			If LDPC, 
			
			  0: LDPC extra symbol not present
			
			  1: LDPC extra symbol present
			
			Else 
			
			  Set to 1
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LDPC_EXTRA_SYMBOL_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LDPC_EXTRA_SYMBOL_LSB 16
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LDPC_EXTRA_SYMBOL_MASK 0x00010000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LTF_SIZE
			
			Ltf size
			
			
			
			<enum 0     ltf_1x > 
			
			<enum 1     ltf_2x > 
			
			<enum 2     ltf_4x > 
			
			<legal 0 - 2>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LTF_SIZE_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LTF_SIZE_LSB 17
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_LTF_SIZE_MASK 0x00060000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_LTF_SYMBOLS
			
			Indicates the number of HE-LTF symbols
			
			
			
			0: 1 symbol
			
			1: 2 symbols
			
			2: 3 symbols
			
			3: 4 symbols
			
			4: 5 symbols
			
			5: 6 symbols
			
			6: 7 symbols
			
			7: 8 symbols
			
			
			
			NOTE that this encoding is different from what is in
			Num_LTF_symbols in the HE_SIG_A_MU_DL
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_LTF_SYMBOLS_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_LTF_SYMBOLS_LSB 19
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_NUM_LTF_SYMBOLS_MASK 0x00380000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_A_FACTOR
			
			The packet extension duration of the trigger-based PPDU
			response with these two bits indicating the a-factor 
			
			
			
			<enum 0 a_factor_4>
			
			<enum 1 a_factor_1>
			
			<enum 2 a_factor_2>
			
			<enum 3 a_factor_3>
			
			
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_A_FACTOR_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_A_FACTOR_LSB 22
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_A_FACTOR_MASK 0x00c00000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_PE_DISAMBIGUITY
			
			The packet extension duration of the trigger-based PPDU
			response with this bit indicating the PE-Disambiguity 
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_PE_DISAMBIGUITY_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_PE_DISAMBIGUITY_LSB 24
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_PE_DISAMBIGUITY_MASK 0x01000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_DURATION
			
			<enum 0     packet_ext_duration_0> 0 us duration
			
			<enum 1     packet_ext_duration_4> 4 us duration
			
			<enum 2     packet_ext_duration_8> 8 us duration
			
			<enum 3     packet_ext_duration_12> 12 us duration
			
			<enum 4     packet_ext_duration_16> 16 us duration
			
			<legal 0 - 4>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_DURATION_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_DURATION_LSB 25
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW40_PACKET_EXTENSION_DURATION_MASK 0x0e000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1C
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1C_OFFSET 0x00000004
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1C_LSB 28
#define MACTX_MU_UPLINK_COMMON_PUNC_1_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_1C_MASK 0xf0000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_INFO_VALID
			
			When set, all bw80_... fields contain valid info
			
			
			
			Note that  bw80 also corresponds to Puncture Pattern 2
			in case of punctured transmissions.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_INFO_VALID_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_INFO_VALID_LSB 0
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_INFO_VALID_MASK 0x00000001

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_STBC
			
			Field valid when Bw80_info_valid is set
			
			
			
			When set, use STBC transmission rates
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_STBC_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_STBC_LSB 1
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_STBC_MASK 0x00000002

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_SGI
			
			Field valid when Bw80_info_valid is set
			
			
			
			<enum 0     gi_0_8_us > HE related GI
			
			<enum 1     gi_0_4_us > HE related GI <enum 2    
			gi_1_6_us > HE related GI
			
			<enum 3     gi_3_2_us > HE related GI
			
			<legal 0 - 3>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_SGI_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_SGI_LSB 2
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_SGI_MASK 0x0000000c

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_FRAMELESS_MU_RX
			
			Field valid when Bw80_info_valid is set
			
			
			
			This is set, when PHY RX will have to do a MU reception,
			but it is already known that PHY will not receive any real
			data that the MAC can decode. For this scenario, PHY should
			still send all non Data TLVs for the MU reception, but block
			all data TLVs.
			
			Used in for example MU_RTS/CTS exchanges.
			
			This feature might not get used in the end, and is more
			of a placeholder in case something like this is needed
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_FRAMELESS_MU_RX_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_FRAMELESS_MU_RX_LSB 4
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_FRAMELESS_MU_RX_MASK 0x00000010

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2A
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2A_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2A_LSB 5
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2A_MASK 0x000000e0

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_USERS
			
			Field valid when Bw80_info_valid is set
			
			
			
			Number of users for 80 Mhz uplink reception.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_USERS_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_USERS_LSB 8
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_USERS_MASK 0x00003f00

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2B
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2B_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2B_LSB 14
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2B_MASK 0x0000c000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LDPC_EXTRA_SYMBOL
			
			If LDPC, 
			
			  0: LDPC extra symbol not present
			
			  1: LDPC extra symbol present
			
			Else 
			
			  Set to 1
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LDPC_EXTRA_SYMBOL_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LDPC_EXTRA_SYMBOL_LSB 16
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LDPC_EXTRA_SYMBOL_MASK 0x00010000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LTF_SIZE
			
			Ltf size
			
			
			
			<enum 0     ltf_1x > 
			
			<enum 1     ltf_2x > 
			
			<enum 2     ltf_4x > 
			
			<legal 0 - 2>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LTF_SIZE_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LTF_SIZE_LSB 17
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_LTF_SIZE_MASK 0x00060000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_LTF_SYMBOLS
			
			Indicates the number of HE-LTF symbols
			
			
			
			0: 1 symbol
			
			1: 2 symbols
			
			2: 3 symbols
			
			3: 4 symbols
			
			4: 5 symbols
			
			5: 6 symbols
			
			6: 7 symbols
			
			7: 8 symbols
			
			
			
			NOTE that this encoding is different from what is in
			Num_LTF_symbols in the HE_SIG_A_MU_DL
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_LTF_SYMBOLS_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_LTF_SYMBOLS_LSB 19
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_NUM_LTF_SYMBOLS_MASK 0x00380000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_A_FACTOR
			
			The packet extension duration of the trigger-based PPDU
			response with these two bits indicating the a-factor 
			
			
			
			<enum 0 a_factor_4>
			
			<enum 1 a_factor_1>
			
			<enum 2 a_factor_2>
			
			<enum 3 a_factor_3>
			
			
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_A_FACTOR_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_A_FACTOR_LSB 22
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_A_FACTOR_MASK 0x00c00000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_PE_DISAMBIGUITY
			
			The packet extension duration of the trigger-based PPDU
			response with this bit indicating the PE-Disambiguity 
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_PE_DISAMBIGUITY_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_PE_DISAMBIGUITY_LSB 24
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_PE_DISAMBIGUITY_MASK 0x01000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_DURATION
			
			<enum 0     packet_ext_duration_0> 0 us duration
			
			<enum 1     packet_ext_duration_4> 4 us duration
			
			<enum 2     packet_ext_duration_8> 8 us duration
			
			<enum 3     packet_ext_duration_12> 12 us duration
			
			<enum 4     packet_ext_duration_16> 16 us duration
			
			<legal 0 - 4>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_DURATION_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_DURATION_LSB 25
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW80_PACKET_EXTENSION_DURATION_MASK 0x0e000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2C
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2C_OFFSET 0x00000008
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2C_LSB 28
#define MACTX_MU_UPLINK_COMMON_PUNC_2_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_2C_MASK 0xf0000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_INFO_VALID
			
			When set, all bw160_... fields contain valid info
			
			
			
			Note that  bw160 also corresponds to Puncture Pattern 3
			in case of punctured transmissions.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_INFO_VALID_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_INFO_VALID_LSB 0
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_INFO_VALID_MASK 0x00000001

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_STBC
			
			Field valid when Bw160_info_valid is set
			
			
			
			When set, use STBC transmission rates
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_STBC_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_STBC_LSB 1
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_STBC_MASK 0x00000002

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_SGI
			
			Field valid when Bw160_info_valid is set
			
			
			
			<enum 0     gi_0_8_us > HE related GI
			
			<enum 1     gi_0_4_us > HE related GI <enum 2    
			gi_1_6_us > HE related GI
			
			<enum 3     gi_3_2_us > HE related GI
			
			<legal 0 - 3>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_SGI_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_SGI_LSB 2
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_SGI_MASK 0x0000000c

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_FRAMELESS_MU_RX
			
			Field valid when Bw160_info_valid is set
			
			
			
			This is set, when PHY RX will have to do a MU reception,
			but it is already known that PHY will not receive any real
			data that the MAC can decode. For this scenario, PHY should
			still send all non Data TLVs for the MU reception, but block
			all data TLVs.
			
			Used in for example MU_RTS/CTS exchanges.
			
			This feature might not get used in the end, and is more
			of a placeholder in case something like this is needed
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_FRAMELESS_MU_RX_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_FRAMELESS_MU_RX_LSB 4
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_FRAMELESS_MU_RX_MASK 0x00000010

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3A
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3A_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3A_LSB 5
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3A_MASK 0x000000e0

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_USERS
			
			Field valid when Bw160_info_valid is set
			
			
			
			Number of users for 80 Mhz uplink reception.
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_USERS_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_USERS_LSB 8
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_USERS_MASK 0x00003f00

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3B
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3B_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3B_LSB 14
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3B_MASK 0x0000c000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LDPC_EXTRA_SYMBOL
			
			If LDPC, 
			
			  0: LDPC extra symbol not present
			
			  1: LDPC extra symbol present
			
			Else 
			
			  Set to 1
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LDPC_EXTRA_SYMBOL_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LDPC_EXTRA_SYMBOL_LSB 16
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LDPC_EXTRA_SYMBOL_MASK 0x00010000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LTF_SIZE
			
			Ltf size
			
			
			
			<enum 0     ltf_1x > 
			
			<enum 1     ltf_2x > 
			
			<enum 2     ltf_4x > 
			
			<legal 0 - 2>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LTF_SIZE_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LTF_SIZE_LSB 17
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_LTF_SIZE_MASK 0x00060000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_LTF_SYMBOLS
			
			Indicates the number of HE-LTF symbols
			
			
			
			0: 1 symbol
			
			1: 2 symbols
			
			2: 3 symbols
			
			3: 4 symbols
			
			4: 5 symbols
			
			5: 6 symbols
			
			6: 7 symbols
			
			7: 8 symbols
			
			
			
			NOTE that this encoding is different from what is in
			Num_LTF_symbols in the HE_SIG_A_MU_DL
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_LTF_SYMBOLS_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_LTF_SYMBOLS_LSB 19
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_NUM_LTF_SYMBOLS_MASK 0x00380000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_A_FACTOR
			
			The packet extension duration of the trigger-based PPDU
			response with these two bits indicating the a-factor 
			
			
			
			<enum 0 a_factor_4>
			
			<enum 1 a_factor_1>
			
			<enum 2 a_factor_2>
			
			<enum 3 a_factor_3>
			
			
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_A_FACTOR_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_A_FACTOR_LSB 22
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_A_FACTOR_MASK 0x00c00000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_PE_DISAMBIGUITY
			
			The packet extension duration of the trigger-based PPDU
			response with this bit indicating the PE-Disambiguity 
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_PE_DISAMBIGUITY_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_PE_DISAMBIGUITY_LSB 24
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_PE_DISAMBIGUITY_MASK 0x01000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_DURATION
			
			<enum 0     packet_ext_duration_0> 0 us duration
			
			<enum 1     packet_ext_duration_4> 4 us duration
			
			<enum 2     packet_ext_duration_8> 8 us duration
			
			<enum 3     packet_ext_duration_12> 12 us duration
			
			<enum 4     packet_ext_duration_16> 16 us duration
			
			<legal 0 - 4>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_DURATION_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_DURATION_LSB 25
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_BW160_PACKET_EXTENSION_DURATION_MASK 0x0e000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3C
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3C_OFFSET 0x0000000c
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3C_LSB 28
#define MACTX_MU_UPLINK_COMMON_PUNC_3_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_3C_MASK 0xf0000000

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_LTF_MODE
			
			Common trigger info
			
			
			
			MU MIMO LTF mode field indicates the mode in which
			pilots are allocated
			
			
			
			0: Single-stream pilot
			
			1: Mask LTF sequence of each spatial stream by a
			distinct orthogonal code
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_LTF_MODE_OFFSET 0x00000010
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_LTF_MODE_LSB 0
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_LTF_MODE_MASK 0x00000001

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_NUM_DATA_SYMBOLS
			
			The Number of Data symbols in the UL response.
			
			
			
			<legal all>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_NUM_DATA_SYMBOLS_OFFSET 0x00000010
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_NUM_DATA_SYMBOLS_LSB 1
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_NUM_DATA_SYMBOLS_MASK 0x0000fffe

/* Description		MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_4A
			
			<legal 0>
*/
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_4A_OFFSET 0x00000010
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_4A_LSB 16
#define MACTX_MU_UPLINK_COMMON_PUNC_4_UPLINK_COMMON_INFO_DETAILS_PAT4567_RESERVED_4A_MASK 0xffff0000


#endif // _MACTX_MU_UPLINK_COMMON_PUNC_H_
