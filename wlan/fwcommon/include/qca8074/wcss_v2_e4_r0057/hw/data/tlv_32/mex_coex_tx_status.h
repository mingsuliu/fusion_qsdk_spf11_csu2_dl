// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "coex_tx_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<coex_tx_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"coex_tx_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(reserved_0a);
    MEX_PARSE_TLV_FIELD(tx_bw);
    MEX_PARSE_TLV_FIELD(tx_status_reason);
    MEX_PARSE_TLV_FIELD(tx_wait_ack);
    MEX_PARSE_TLV_FIELD(fes_tx_is_gen_frame);
    MEX_PARSE_TLV_FIELD(sch_tx_burst_ongoing);
    MEX_PARSE_TLV_FIELD(current_tx_duration);
    MEX_PARSE_TLV_FIELD(next_rx_active_time);
    MEX_PARSE_TLV_FIELD(remaining_fes_time);
    MEX_PARSE_TLV_FIELD(tx_antenna_mask);
    MEX_PARSE_TLV_FIELD(shared_ant_tx_pwr);
    MEX_PARSE_TLV_FIELD(other_ant_tx_pwr);
    MEX_PARSE_TLV_FIELD(reserved_2);
  }
};


