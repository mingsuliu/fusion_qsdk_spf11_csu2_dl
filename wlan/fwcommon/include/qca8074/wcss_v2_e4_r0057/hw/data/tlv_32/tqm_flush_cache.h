// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_FLUSH_CACHE_H_
#define _TQM_FLUSH_CACHE_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_cmd_header.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	struct uniform_tqm_cmd_header cmd_header;
//	1	flush_addr_31_0[31:0]
//	2	flush_addr_39_32[7:0], reserved_2a[8], release_cache_block_index[9], cache_block_resource_index[11:10], flush_without_invalidate[12], block_cache_usage_after_flush[13], flush_entire_cache[14], reserved_2b[31:15]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_FLUSH_CACHE 3

struct tqm_flush_cache {
    struct            uniform_tqm_cmd_header                       cmd_header;
             uint32_t flush_addr_31_0                 : 32; //[31:0]
             uint32_t flush_addr_39_32                :  8, //[7:0]
                      reserved_2a                     :  1, //[8]
                      release_cache_block_index       :  1, //[9]
                      cache_block_resource_index      :  2, //[11:10]
                      flush_without_invalidate        :  1, //[12]
                      block_cache_usage_after_flush   :  1, //[13]
                      flush_entire_cache              :  1, //[14]
                      reserved_2b                     : 17; //[31:15]
};

/*

struct uniform_tqm_cmd_header cmd_header
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Details for command execution tracking purposes.

flush_addr_31_0
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Address (lower 32 bits) of the descriptor to flush
			
			<legal all>

flush_addr_39_32
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Address (upper 8 bits) of the descriptor to flush
			
			<legal all>

reserved_2a
			
			<legal 0>

release_cache_block_index
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			If SW has previously used a blocking resource that it
			now wants to re-use for this command, this bit shall be set.
			It prevents SW from having to send a separate
			TQM_UNBLOCK_CACHE command.
			
			
			
			When set, HW will first release the blocking resource
			(indicated in field 'Cache_block_resouce_index') before this
			command gets executed.
			
			If that resource was already unblocked, this will be
			considered an error. This command will not be executed, and
			an error shall be returned.
			
			<legal all>

cache_block_resource_index
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Indicates which of the four blocking resources in TQM
			will be assigned for managing the blocking of this
			(descriptor) address 
			
			<legal all>

flush_without_invalidate
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			When set, REO shall flush the cache line contents from
			the cache, but there is NO need to invalidate the cache line
			entry... The contents in the cache can be maintained. This
			feature can be used by SW (and DV) to get a current snapshot
			of the contents in the cache
			
			
			
			<legal all>

block_cache_usage_after_flush
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			When set, TQM shall block any cache accesses to this
			address  till explicitly unblocked. The 'blocking' index to
			be used for this is indicated in field
			'cache_block_resource_index'. If SW had previously used this
			blocking resource and was not freed up yet, SW shall first
			unblock that index (by setting bit
			Release_cache_block_index) or use an unblock command.
			
			
			
			If the resource indicated here was already blocked (and
			did not get unblocked in this command), it is considered an
			error scenario...
			
			No flush shall happen. The status for this command shall
			indicate error.
			
			
			
			<legal all>

flush_entire_cache
			
			When set, the entire cache shall be flushed. The entire
			cache will also remain blocked, till the
			'TQM_UNBLOCK_COMMAND' is received with bit unblock type set
			to unblock_cache. All other fields in this command are to be
			ignored.
			
			
			
			Note that flushing the entire cache has no changes to
			the current settings of the blocking resource settings
			
			<legal all>

reserved_2b
			
			<legal 0>
*/


 /* EXTERNAL REFERENCE : struct uniform_tqm_cmd_header cmd_header */ 


/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_CMD_NUMBER
			
			Consumer: TQM/SW/DEBUG
			
			Producer: SW 
			
			
			
			This number can be used by SW to track, identify and
			link the created commands with the command statusses
			
			
			
			<legal all> 
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_CMD_NUMBER_OFFSET           0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_CMD_NUMBER_LSB              0
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_CMD_NUMBER_MASK             0x00ffffff

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_REQUIRED
			
			Consumer: TQM
			
			Producer: SW 
			
			
			
			<enum 0 NoStatus> TQM does not need to generate a status
			TLV for the execution of this command
			
			<enum 1 StatusRequired> TQM shall generate a status TLV
			for the execution of this command
			
			
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_REQUIRED_OFFSET      0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_REQUIRED_LSB         24
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_REQUIRED_MASK        0x01000000

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_RING
			
			Consumer: TQM
			
			Producer: SW 
			
			
			
			Field only valid when 'TQM_status_required' is set.
			
			
			
			<enum 0 TQM_STATUS_RING_0> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 0
			
			<enum 1 TQM_STATUS_RING_1> If a status  after execution
			of this command is requested, it should go to the TQM status
			ring 1
			
			
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_RING_OFFSET          0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_RING_LSB             25
#define TQM_FLUSH_CACHE_0_CMD_HEADER_TQM_STATUS_RING_MASK            0x02000000

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_PASS_ON_TO_TQM
			
			Consumer: SCH
			
			Producer: SW 
			
			
			
			This field is only used by SCH for the TQM commands that
			it receives on the PDG interface (but could have been
			generated by TXPCU or RXPCU.
			
			
			
			When set, and SCH should pass this returning command on
			to the TQM
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_PASS_ON_TO_TQM_OFFSET           0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_PASS_ON_TO_TQM_LSB              26
#define TQM_FLUSH_CACHE_0_CMD_HEADER_PASS_ON_TO_TQM_MASK             0x04000000

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_SIFS_BURST_CMD_DROP
			
			Consumer: SCH
			
			Producer: SW 
			
			
			
			This field is only used by SCH.
			
			
			
			When set:
			
			If this TQM command is embedded in a scheduler TLV that
			executes as the result of SIFS burst continuation, the
			SCHeduler shall drop this command
			
			
			
			When clear:
			
			Scheduler shall process the command as usual
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_SIFS_BURST_CMD_DROP_OFFSET  0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_SIFS_BURST_CMD_DROP_LSB     27
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_SIFS_BURST_CMD_DROP_MASK    0x08000000

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_BACKOFF_CMD_DROP
			
			Consumer: SCH
			
			Producer: SW 
			
			
			
			This field is only used by SCH.
			
			
			
			When set:
			
			If this TQM command is embedded in a scheduler TLV that
			executes as the result of a Backoff, the SCHeduler shall
			drop this command
			
			
			
			When clear:
			
			Scheduler shall process the command as usual
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_BACKOFF_CMD_DROP_OFFSET     0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_BACKOFF_CMD_DROP_LSB        28
#define TQM_FLUSH_CACHE_0_CMD_HEADER_SCH_BACKOFF_CMD_DROP_MASK       0x10000000

/* Description		TQM_FLUSH_CACHE_0_CMD_HEADER_RESERVED_0A
			
			<legal 0>
*/
#define TQM_FLUSH_CACHE_0_CMD_HEADER_RESERVED_0A_OFFSET              0x00000000
#define TQM_FLUSH_CACHE_0_CMD_HEADER_RESERVED_0A_LSB                 29
#define TQM_FLUSH_CACHE_0_CMD_HEADER_RESERVED_0A_MASK                0xe0000000

/* Description		TQM_FLUSH_CACHE_1_FLUSH_ADDR_31_0
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Address (lower 32 bits) of the descriptor to flush
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_1_FLUSH_ADDR_31_0_OFFSET                     0x00000004
#define TQM_FLUSH_CACHE_1_FLUSH_ADDR_31_0_LSB                        0
#define TQM_FLUSH_CACHE_1_FLUSH_ADDR_31_0_MASK                       0xffffffff

/* Description		TQM_FLUSH_CACHE_2_FLUSH_ADDR_39_32
			
			Consumer: TQM
			
			Producer: SW
			
			
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Address (upper 8 bits) of the descriptor to flush
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_FLUSH_ADDR_39_32_OFFSET                    0x00000008
#define TQM_FLUSH_CACHE_2_FLUSH_ADDR_39_32_LSB                       0
#define TQM_FLUSH_CACHE_2_FLUSH_ADDR_39_32_MASK                      0x000000ff

/* Description		TQM_FLUSH_CACHE_2_RESERVED_2A
			
			<legal 0>
*/
#define TQM_FLUSH_CACHE_2_RESERVED_2A_OFFSET                         0x00000008
#define TQM_FLUSH_CACHE_2_RESERVED_2A_LSB                            8
#define TQM_FLUSH_CACHE_2_RESERVED_2A_MASK                           0x00000100

/* Description		TQM_FLUSH_CACHE_2_RELEASE_CACHE_BLOCK_INDEX
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			If SW has previously used a blocking resource that it
			now wants to re-use for this command, this bit shall be set.
			It prevents SW from having to send a separate
			TQM_UNBLOCK_CACHE command.
			
			
			
			When set, HW will first release the blocking resource
			(indicated in field 'Cache_block_resouce_index') before this
			command gets executed.
			
			If that resource was already unblocked, this will be
			considered an error. This command will not be executed, and
			an error shall be returned.
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_RELEASE_CACHE_BLOCK_INDEX_OFFSET           0x00000008
#define TQM_FLUSH_CACHE_2_RELEASE_CACHE_BLOCK_INDEX_LSB              9
#define TQM_FLUSH_CACHE_2_RELEASE_CACHE_BLOCK_INDEX_MASK             0x00000200

/* Description		TQM_FLUSH_CACHE_2_CACHE_BLOCK_RESOURCE_INDEX
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			Indicates which of the four blocking resources in TQM
			will be assigned for managing the blocking of this
			(descriptor) address 
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_CACHE_BLOCK_RESOURCE_INDEX_OFFSET          0x00000008
#define TQM_FLUSH_CACHE_2_CACHE_BLOCK_RESOURCE_INDEX_LSB             10
#define TQM_FLUSH_CACHE_2_CACHE_BLOCK_RESOURCE_INDEX_MASK            0x00000c00

/* Description		TQM_FLUSH_CACHE_2_FLUSH_WITHOUT_INVALIDATE
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			When set, REO shall flush the cache line contents from
			the cache, but there is NO need to invalidate the cache line
			entry... The contents in the cache can be maintained. This
			feature can be used by SW (and DV) to get a current snapshot
			of the contents in the cache
			
			
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_FLUSH_WITHOUT_INVALIDATE_OFFSET            0x00000008
#define TQM_FLUSH_CACHE_2_FLUSH_WITHOUT_INVALIDATE_LSB               12
#define TQM_FLUSH_CACHE_2_FLUSH_WITHOUT_INVALIDATE_MASK              0x00001000

/* Description		TQM_FLUSH_CACHE_2_BLOCK_CACHE_USAGE_AFTER_FLUSH
			
			Field not valid when Flush_entire_cache is set.
			
			
			
			When set, TQM shall block any cache accesses to this
			address  till explicitly unblocked. The 'blocking' index to
			be used for this is indicated in field
			'cache_block_resource_index'. If SW had previously used this
			blocking resource and was not freed up yet, SW shall first
			unblock that index (by setting bit
			Release_cache_block_index) or use an unblock command.
			
			
			
			If the resource indicated here was already blocked (and
			did not get unblocked in this command), it is considered an
			error scenario...
			
			No flush shall happen. The status for this command shall
			indicate error.
			
			
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_BLOCK_CACHE_USAGE_AFTER_FLUSH_OFFSET       0x00000008
#define TQM_FLUSH_CACHE_2_BLOCK_CACHE_USAGE_AFTER_FLUSH_LSB          13
#define TQM_FLUSH_CACHE_2_BLOCK_CACHE_USAGE_AFTER_FLUSH_MASK         0x00002000

/* Description		TQM_FLUSH_CACHE_2_FLUSH_ENTIRE_CACHE
			
			When set, the entire cache shall be flushed. The entire
			cache will also remain blocked, till the
			'TQM_UNBLOCK_COMMAND' is received with bit unblock type set
			to unblock_cache. All other fields in this command are to be
			ignored.
			
			
			
			Note that flushing the entire cache has no changes to
			the current settings of the blocking resource settings
			
			<legal all>
*/
#define TQM_FLUSH_CACHE_2_FLUSH_ENTIRE_CACHE_OFFSET                  0x00000008
#define TQM_FLUSH_CACHE_2_FLUSH_ENTIRE_CACHE_LSB                     14
#define TQM_FLUSH_CACHE_2_FLUSH_ENTIRE_CACHE_MASK                    0x00004000

/* Description		TQM_FLUSH_CACHE_2_RESERVED_2B
			
			<legal 0>
*/
#define TQM_FLUSH_CACHE_2_RESERVED_2B_OFFSET                         0x00000008
#define TQM_FLUSH_CACHE_2_RESERVED_2B_LSB                            15
#define TQM_FLUSH_CACHE_2_RESERVED_2B_MASK                           0xffff8000


#endif // _TQM_FLUSH_CACHE_H_
