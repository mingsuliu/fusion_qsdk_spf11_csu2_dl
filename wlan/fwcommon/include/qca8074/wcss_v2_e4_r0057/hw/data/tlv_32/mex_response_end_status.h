// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "response_end_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<response_end_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"response_end_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(coex_bt_tx_while_wlan_tx);
    MEX_PARSE_TLV_FIELD(coex_wan_tx_while_wlan_tx);
    MEX_PARSE_TLV_FIELD(coex_wlan_tx_while_wlan_tx);
    MEX_PARSE_TLV_FIELD(global_data_underflow_warning);
    MEX_PARSE_TLV_FIELD(response_transmit_status);
    MEX_PARSE_TLV_FIELD(phytx_pkt_end_info_valid);
    MEX_PARSE_TLV_FIELD(phytx_abort_request_info_valid);
    MEX_PARSE_TLV_FIELD(generated_response);
    MEX_PARSE_TLV_FIELD(mba_user_count);
    MEX_PARSE_TLV_FIELD(mba_fake_bitmap_count);
    MEX_PARSE_TLV_FIELD(coex_based_tx_bw);
    MEX_PARSE_TLV_FIELD(trig_response_related);
    MEX_PARSE_TLV_FIELD(dpdtrain_done);
    MEX_PARSE_TLV_FIELD(phytx_abort_request_info_details);
    MEX_PARSE_TLV_FIELD(cbf_segment_request_mask);
    MEX_PARSE_TLV_FIELD(cbf_segment_sent_mask);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(data_underflow_warning);
    MEX_PARSE_TLV_FIELD(phy_tx_gain_setting);
    MEX_PARSE_TLV_FIELD(timing_status);
    MEX_PARSE_TLV_FIELD(only_null_delim_sent);
    MEX_PARSE_TLV_FIELD(brp_info_valid);
    MEX_PARSE_TLV_FIELD(reserved_2b);
    MEX_PARSE_TLV_FIELD(mu_response_bitmap_31_0);
    MEX_PARSE_TLV_FIELD(mu_response_bitmap_36_32);
    MEX_PARSE_TLV_FIELD(reserved_4a);
    MEX_PARSE_TLV_FIELD(transmit_delay);
    MEX_PARSE_TLV_FIELD(start_of_frame_timestamp_15_0);
    MEX_PARSE_TLV_FIELD(start_of_frame_timestamp_31_16);
    MEX_PARSE_TLV_FIELD(start_of_frame_timestamp_47_32);
    MEX_PARSE_TLV_FIELD(start_of_frame_timestamp_63_48);
    MEX_PARSE_TLV_FIELD(end_of_frame_timestamp_15_0);
    MEX_PARSE_TLV_FIELD(end_of_frame_timestamp_31_16);
    MEX_PARSE_TLV_FIELD(end_of_frame_timestamp_47_32);
    MEX_PARSE_TLV_FIELD(end_of_frame_timestamp_63_48);
    MEX_PARSE_TLV_FIELD(phytx_tx_end_sw_info_16_0);
    MEX_PARSE_TLV_FIELD(phytx_tx_end_sw_info_31_17);
    MEX_PARSE_TLV_FIELD(phytx_tx_end_sw_info_47_32);
    MEX_PARSE_TLV_FIELD(phytx_tx_end_sw_info_63_48);
  }
};


