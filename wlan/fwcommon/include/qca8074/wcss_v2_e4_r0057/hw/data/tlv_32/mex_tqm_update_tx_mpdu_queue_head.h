// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "tqm_update_tx_mpdu_queue_head.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<tqm_update_tx_mpdu_queue_head, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"tqm_update_tx_mpdu_queue_head"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(cmd_header);
    MEX_PARSE_TLV_FIELD(mpdu_queue_desc_addr_31_0);
    MEX_PARSE_TLV_FIELD(mpdu_queue_desc_addr_39_32);
    MEX_PARSE_TLV_FIELD(update_tx_mpdu_queue_number);
    MEX_PARSE_TLV_FIELD(update_mpdu_sequence_numbers);
    MEX_PARSE_TLV_FIELD(update_queue_valid);
    MEX_PARSE_TLV_FIELD(update_pn_addr_31_0);
    MEX_PARSE_TLV_FIELD(update_pn_addr_39_32);
    MEX_PARSE_TLV_FIELD(update_pn_increment_value);
    MEX_PARSE_TLV_FIELD(update_mpdu_header_length);
    MEX_PARSE_TLV_FIELD(update_sw_peer_id);
    MEX_PARSE_TLV_FIELD(update_tid);
    MEX_PARSE_TLV_FIELD(update_associated_link_descriptor_counter);
    MEX_PARSE_TLV_FIELD(update_mpdu_type);
    MEX_PARSE_TLV_FIELD(clear_stat_counters);
    MEX_PARSE_TLV_FIELD(flush_from_cache);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(tx_mpdu_queue_number);
    MEX_PARSE_TLV_FIELD(mpdu_start_sequence_number);
    MEX_PARSE_TLV_FIELD(queue_valid);
    MEX_PARSE_TLV_FIELD(associated_link_descriptor_counter);
    MEX_PARSE_TLV_FIELD(mpdu_type);
    MEX_PARSE_TLV_FIELD(pn_addr_31_0);
    MEX_PARSE_TLV_FIELD(pn_addr_39_32);
    MEX_PARSE_TLV_FIELD(pn_increment_value);
    MEX_PARSE_TLV_FIELD(mpdu_header_length);
    MEX_PARSE_TLV_FIELD(tid);
    MEX_PARSE_TLV_FIELD(reserved_5);
    MEX_PARSE_TLV_FIELD(mpdu_last_sequence_number);
    MEX_PARSE_TLV_FIELD(reserved_6);
    MEX_PARSE_TLV_FIELD(sw_peer_id);
  }
};


