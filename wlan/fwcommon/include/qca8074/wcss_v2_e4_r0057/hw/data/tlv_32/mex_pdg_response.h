// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "pdg_response.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<pdg_response, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"pdg_response"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(hw_response_rate_info);
    MEX_PARSE_TLV_FIELD(hw_response_tx_duration);
    MEX_PARSE_TLV_FIELD(rx_duration_field);
    MEX_PARSE_TLV_FIELD(punctured_response_transmission);
    MEX_PARSE_TLV_FIELD(cca_subband_channel_bonding_mask);
    MEX_PARSE_TLV_FIELD(reserved_7a);
    MEX_PARSE_TLV_FIELD(scrambler_seed_override);
    MEX_PARSE_TLV_FIELD(response_density_valid);
    MEX_PARSE_TLV_FIELD(response_density);
    MEX_PARSE_TLV_FIELD(more_data);
    MEX_PARSE_TLV_FIELD(duration_indication);
    MEX_PARSE_TLV_FIELD(relayed_frame);
    MEX_PARSE_TLV_FIELD(address_indicator);
    MEX_PARSE_TLV_FIELD(early_sector_indicator);
    MEX_PARSE_TLV_FIELD(bandwidth);
    MEX_PARSE_TLV_FIELD(ack_id);
    MEX_PARSE_TLV_FIELD(block_ack_bitmap);
    MEX_PARSE_TLV_FIELD(response_frame_type);
    MEX_PARSE_TLV_FIELD(ack_id_ext);
    MEX_PARSE_TLV_FIELD(ftm_en);
    MEX_PARSE_TLV_FIELD(group_id);
    MEX_PARSE_TLV_FIELD(sta_partial_aid);
    MEX_PARSE_TLV_FIELD(ndp_ba_start_seq_ctrl);
    MEX_PARSE_TLV_FIELD(active_channel);
    MEX_PARSE_TLV_FIELD(reserved_10);
    MEX_PARSE_TLV_FIELD(frame_length);
  }
};


