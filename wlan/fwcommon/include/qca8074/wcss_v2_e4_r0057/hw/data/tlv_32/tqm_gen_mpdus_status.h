// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _TQM_GEN_MPDUS_STATUS_H_
#define _TQM_GEN_MPDUS_STATUS_H_
#if !defined(__ASSEMBLER__)
#endif

#include "uniform_tqm_status_header.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct uniform_tqm_status_header status_header;
//	2	total_queue_mpdu_count[15:0], generated_mpdu_count[31:16]
//	3	used_msdu_count[15:0], mpdu_tx_notify_frame_generated[16], mpdu_head_is_fw_tx_notify_frame[17], flow0_remaining_msdu_count_valid[18], flow1_remaining_msdu_count_valid[19], flow2_remaining_msdu_count_valid[20], flow3_remaining_msdu_count_valid[21], ran_out_of_link_descriptors[22], first_msdu_did_not_fit[23], gen_mpdu_end_reason[27:24], reserved_3a[31:28]
//	4	flow0_remaining_msdu_count[15:0], flow1_remaining_msdu_count[31:16]
//	5	flow2_remaining_msdu_count[15:0], flow3_remaining_msdu_count[31:16]
//	6	mpdu_start_sequence_number[11:0], reserved_6a[15:12], mpdu_last_sequence_number[27:16], reserved_6b[31:28]
//	7	flow_queue0_valid_error[0], flow_queue1_valid_error[1], flow_queue2_valid_error[2], flow_queue3_valid_error[3], reserved_7a[15:4], tx_mpdu_queue_number[31:16]
//	8	tx_flow_number_0[15:0], tx_flow_number_1[31:16]
//	9	tx_flow_number_2[15:0], tx_flow_number_3[31:16]
//	10	reserved_10a[27:0], looping_count[31:28]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_TQM_GEN_MPDUS_STATUS 11

struct tqm_gen_mpdus_status {
    struct            uniform_tqm_status_header                       status_header;
             uint32_t total_queue_mpdu_count          : 16, //[15:0]
                      generated_mpdu_count            : 16; //[31:16]
             uint32_t used_msdu_count                 : 16, //[15:0]
                      mpdu_tx_notify_frame_generated  :  1, //[16]
                      mpdu_head_is_fw_tx_notify_frame :  1, //[17]
                      flow0_remaining_msdu_count_valid:  1, //[18]
                      flow1_remaining_msdu_count_valid:  1, //[19]
                      flow2_remaining_msdu_count_valid:  1, //[20]
                      flow3_remaining_msdu_count_valid:  1, //[21]
                      ran_out_of_link_descriptors     :  1, //[22]
                      first_msdu_did_not_fit          :  1, //[23]
                      gen_mpdu_end_reason             :  4, //[27:24]
                      reserved_3a                     :  4; //[31:28]
             uint32_t flow0_remaining_msdu_count      : 16, //[15:0]
                      flow1_remaining_msdu_count      : 16; //[31:16]
             uint32_t flow2_remaining_msdu_count      : 16, //[15:0]
                      flow3_remaining_msdu_count      : 16; //[31:16]
             uint32_t mpdu_start_sequence_number      : 12, //[11:0]
                      reserved_6a                     :  4, //[15:12]
                      mpdu_last_sequence_number       : 12, //[27:16]
                      reserved_6b                     :  4; //[31:28]
             uint32_t flow_queue0_valid_error         :  1, //[0]
                      flow_queue1_valid_error         :  1, //[1]
                      flow_queue2_valid_error         :  1, //[2]
                      flow_queue3_valid_error         :  1, //[3]
                      reserved_7a                     : 12, //[15:4]
                      tx_mpdu_queue_number            : 16; //[31:16]
             uint32_t tx_flow_number_0                : 16, //[15:0]
                      tx_flow_number_1                : 16; //[31:16]
             uint32_t tx_flow_number_2                : 16, //[15:0]
                      tx_flow_number_3                : 16; //[31:16]
             uint32_t reserved_10a                    : 28, //[27:0]
                      looping_count                   :  4; //[31:28]
};

/*

struct uniform_tqm_status_header status_header
			
			Consumer: SW
			
			Producer: TQM
			
			
			
			Details that can link this status with the original
			command. It also contains info on how long TQM took to
			execute this command.

total_queue_mpdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MPDUs in the queue after command execution
			finished
			
			<legal all>

generated_mpdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MPDUs created by this command
			
			<legal all>

used_msdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MSDUs used by this command to create
			MPDUs.
			
			<legal all>

mpdu_tx_notify_frame_generated
			
			At least one of the MPDUs generated is marked as a
			'fw_tx_notify_frame'
			
			<legal all>

mpdu_head_is_fw_tx_notify_frame
			
			When set, the MPDU frame at the head of the MPDU queue
			is a FW_tx_notify_frame 
			
			<legal all>

flow0_remaining_msdu_count_valid
			
			When set, flow0 remaining msdu count info is valid. 
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>

flow1_remaining_msdu_count_valid
			
			When set, flow1 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>

flow2_remaining_msdu_count_valid
			
			When set, flow2 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>

flow3_remaining_msdu_count_valid
			
			When set, flow3 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>

ran_out_of_link_descriptors
			
			When set, the MPDU generation for this command got
			temporarily halted because the idle link descriptor ring got
			empty. The MPDU generation module kept waiting till these
			link descriptors were available again.
			
			<legal all>

first_msdu_did_not_fit
			
			When set, the MPDU generation for this command got
			terminated because the very first msdu was already too long
			to fit into an MPDU.
			
			<legal all>

gen_mpdu_end_reason
			
			The end reason for TQM to finish mpdu generation
			
			
			
			This is DV only. Do NOT use. Not all functionality is
			validated.
			
			
			
			<enum 0 GEN_MPDU_SUCCESSFUL_COMPLETION>
			
			<enum 1 MSDU_MIX_RATIO_ALL_FLOWS_IS_0>
			
			<enum 2 MAX_MPDU_SIZE_IS_0>
			
			<enum 3 MAX_MSDU_COUNT_IN_MPDU_IS_0>
			
			<enum 4 TARGET_MPDU_LIMIT_REACHED>
			
			<enum 5 GEN_MPDU_WINDOW_SIZE_REACHED>
			
			<enum 6 TARGET_EXT_DESC_USAGE_COUNT_REACHED>
			
			<enum 7 GEN_MPDU_PAUSE_ASSERTED >
			
			<enum 8 FIRST_MSDU_DID_NOT_FIT_ERROR >
			
			<enum 9 RAN_OUT_OF_MSDUS_IN_ALL_VALID_FLOWS>
			
			<enum 10 GEN_MPDU_ALL_FLOW_QUEUES_INVALID>
			
			<enum 11 GEN_MPDU_QUEUE_HEAD_INVALID>
			
			<enum 12 GEN_MPDU_FLUSH_ASSERTED>
			
			<enum 13 GEN_MPDU_MAX_MPDU_COUNT_REACHED>
			
			<enum 14 GEN_MPDU_MAX_MPDU_GEN_TIME_REACHED>
			
			<enum 15 GEN_MPDU_END_FOR_MULTIPLE_REASONS>
			
			<legal all>

reserved_3a
			
			<legal 0>

flow0_remaining_msdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow0_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 0 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_0 is set to 0 
			
			<legal all>

flow1_remaining_msdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow1_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 1 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_1 is set to 0 <legal all>

flow2_remaining_msdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow2_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 2 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_2 is set to 0 
			
			<legal all>

flow3_remaining_msdu_count
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow3_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 3 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_3 is set to 0 
			
			<legal all>

mpdu_start_sequence_number
			
			Sequence number of  the first MPDU in the queue. If the
			queue is empty, this number will be set equal to the field
			MPDU_last_sequence_number field.
			
			<legal all>

reserved_6a
			
			<legal 0>

mpdu_last_sequence_number
			
			Sequence number assigned to the last MPDU in the queue.
			
			
			
			If the queue is empty, this number shall be equal to
			MPDU_start_sequence_number, and represents the value that
			was assigned to the last generated MPDU.
			
			<legal all>

reserved_6b
			
			<legal 0>

flow_queue0_valid_error
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>

flow_queue1_valid_error
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>

flow_queue2_valid_error
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>

flow_queue3_valid_error
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>

reserved_7a
			
			<legal 0>

tx_mpdu_queue_number
			
			Indicates the MPDU queue ID for which MPDUs were
			generated
			
			<legal all>

tx_flow_number_0
			
			Indicates the flow ID flow queue 0 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>

tx_flow_number_1
			
			Indicates the flow ID flow queue 1 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>

tx_flow_number_2
			
			Indicates the flow ID flow queue 2 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>

tx_flow_number_3
			
			Indicates the flow ID flow queue 3 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>

reserved_10a
			
			<legal 0>

looping_count
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/


 /* EXTERNAL REFERENCE : struct uniform_tqm_status_header status_header */ 


/* Description		TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER
			
			Consumer: SW , DEBUG
			
			Producer: TQM 
			
			
			
			The value in this field is equal to value of the
			'TQM_CMD_Number' field the TQM command or the
			'TQM_add_cmd_Number' field from the TQM entrance ring
			descriptor
			
			
			
			This field helps to correlate the statuses with the TQM
			commands.
			
			
			
			<legal all> 
*/
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_OFFSET 0x00000000
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_LSB   0
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_STATUS_NUMBER_MASK  0x00ffffff

/* Description		TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME
			
			Consumer: DEBUG
			
			Producer: TQM 
			
			
			
			The amount of time TQM took to excecute the command.
			Note that this time does not include the duration of the
			command waiting in the command ring, before the execution
			started.
			
			
			
			The counter saturates at 0xF
			
			
			
			Field 'Cmd_Execution_time_unit' indicates what the time
			unit is of this field.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_OFFSET 0x00000000
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_LSB  24
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_MASK 0x0f000000

/* Description		TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT
			
			The time unit for field Cmd_Execution_time
			
			<enum 0 time_unit_2_us> The time unit is 2 us
			
			<enum 1 time_unit_32_us> The time unit is 32 us
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_OFFSET 0x00000000
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_LSB 28
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_TIME_UNIT_MASK 0x10000000

/* Description		TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS
			
			Consumer: DEBUG
			
			Producer: TQM 
			
			
			
			Execution status of the command.
			
			
			
			<enum 0 tqm_successful_execution> Command has
			successfully been executed
			
			<enum 1 tqm_failed_with_invalid_descriptor> queue or
			flow descriptor valid bit is not set. None of the status
			fields in the entire STATUS TLV are valid, accept when
			explicitly mentioned in the field description
			
			<enum 2 tqm_resource_blocked> Command is NOT  executed
			(in the normal way or maybe only partially) because one or
			more descriptors were blocked OR the entire cache was
			blocked. Part that is still executed is command specific...
			See command details. None of the status fields in the entire
			STATUS TLV are valid.
			
			<enum 3 tqm_sch_flush> Command is partially or not
			executed at all due to SCH generating a flush command.
			
			
			
			<legal  0-3>
*/
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_OFFSET 0x00000000
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_LSB 29
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_TQM_CMD_EXECUTION_STATUS_MASK 0x60000000

/* Description		TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED
			
			Consumer: DEBUG/SW
			
			Producer: TQM 
			
			
			
			This field is only applicable for commands that were
			given in the SW command ring to TQM. For the HW ring, which
			is filled by the scheduler this field is always set to 0
			
			
			
			When set, the command execution was temporarily paused
			because a command from the HW ring (filled by SCH)
			interrupted the execution of this command.
			
			Note that when this status is generated, the 'pause' has
			ended and TQM has resumed the command execution and has now
			finished it.
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_OFFSET 0x00000000
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_LSB 31
#define TQM_GEN_MPDUS_STATUS_0_STATUS_HEADER_CMD_EXECUTION_WAS_PAUSED_MASK 0x80000000

/* Description		TQM_GEN_MPDUS_STATUS_1_STATUS_HEADER_TIMESTAMP
			
			Timestamp at the moment that this status report is
			written.
			
			
			
			In us, based of the global timer
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_1_STATUS_HEADER_TIMESTAMP_OFFSET        0x00000004
#define TQM_GEN_MPDUS_STATUS_1_STATUS_HEADER_TIMESTAMP_LSB           0
#define TQM_GEN_MPDUS_STATUS_1_STATUS_HEADER_TIMESTAMP_MASK          0xffffffff

/* Description		TQM_GEN_MPDUS_STATUS_2_TOTAL_QUEUE_MPDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MPDUs in the queue after command execution
			finished
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_2_TOTAL_QUEUE_MPDU_COUNT_OFFSET         0x00000008
#define TQM_GEN_MPDUS_STATUS_2_TOTAL_QUEUE_MPDU_COUNT_LSB            0
#define TQM_GEN_MPDUS_STATUS_2_TOTAL_QUEUE_MPDU_COUNT_MASK           0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_2_GENERATED_MPDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MPDUs created by this command
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_2_GENERATED_MPDU_COUNT_OFFSET           0x00000008
#define TQM_GEN_MPDUS_STATUS_2_GENERATED_MPDU_COUNT_LSB              16
#define TQM_GEN_MPDUS_STATUS_2_GENERATED_MPDU_COUNT_MASK             0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_3_USED_MSDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			The number of MSDUs used by this command to create
			MPDUs.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_USED_MSDU_COUNT_OFFSET                0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_USED_MSDU_COUNT_LSB                   0
#define TQM_GEN_MPDUS_STATUS_3_USED_MSDU_COUNT_MASK                  0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_3_MPDU_TX_NOTIFY_FRAME_GENERATED
			
			At least one of the MPDUs generated is marked as a
			'fw_tx_notify_frame'
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_MPDU_TX_NOTIFY_FRAME_GENERATED_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_MPDU_TX_NOTIFY_FRAME_GENERATED_LSB    16
#define TQM_GEN_MPDUS_STATUS_3_MPDU_TX_NOTIFY_FRAME_GENERATED_MASK   0x00010000

/* Description		TQM_GEN_MPDUS_STATUS_3_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME
			
			When set, the MPDU frame at the head of the MPDU queue
			is a FW_tx_notify_frame 
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_LSB   17
#define TQM_GEN_MPDUS_STATUS_3_MPDU_HEAD_IS_FW_TX_NOTIFY_FRAME_MASK  0x00020000

/* Description		TQM_GEN_MPDUS_STATUS_3_FLOW0_REMAINING_MSDU_COUNT_VALID
			
			When set, flow0 remaining msdu count info is valid. 
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_FLOW0_REMAINING_MSDU_COUNT_VALID_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_FLOW0_REMAINING_MSDU_COUNT_VALID_LSB  18
#define TQM_GEN_MPDUS_STATUS_3_FLOW0_REMAINING_MSDU_COUNT_VALID_MASK 0x00040000

/* Description		TQM_GEN_MPDUS_STATUS_3_FLOW1_REMAINING_MSDU_COUNT_VALID
			
			When set, flow1 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_FLOW1_REMAINING_MSDU_COUNT_VALID_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_FLOW1_REMAINING_MSDU_COUNT_VALID_LSB  19
#define TQM_GEN_MPDUS_STATUS_3_FLOW1_REMAINING_MSDU_COUNT_VALID_MASK 0x00080000

/* Description		TQM_GEN_MPDUS_STATUS_3_FLOW2_REMAINING_MSDU_COUNT_VALID
			
			When set, flow2 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_FLOW2_REMAINING_MSDU_COUNT_VALID_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_FLOW2_REMAINING_MSDU_COUNT_VALID_LSB  20
#define TQM_GEN_MPDUS_STATUS_3_FLOW2_REMAINING_MSDU_COUNT_VALID_MASK 0x00100000

/* Description		TQM_GEN_MPDUS_STATUS_3_FLOW3_REMAINING_MSDU_COUNT_VALID
			
			When set, flow3 remaining msdu count info is valid
			
			
			
			When clear, this flow info is not valid. 
			
			
			
			TQM will only set the valid bit when a Flow Queue
			descriptor has been fetched (because the flow_mix_ratio is
			non-zero) and the MSDU_Count at that time is non-zero.
			
			
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_FLOW3_REMAINING_MSDU_COUNT_VALID_OFFSET 0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_FLOW3_REMAINING_MSDU_COUNT_VALID_LSB  21
#define TQM_GEN_MPDUS_STATUS_3_FLOW3_REMAINING_MSDU_COUNT_VALID_MASK 0x00200000

/* Description		TQM_GEN_MPDUS_STATUS_3_RAN_OUT_OF_LINK_DESCRIPTORS
			
			When set, the MPDU generation for this command got
			temporarily halted because the idle link descriptor ring got
			empty. The MPDU generation module kept waiting till these
			link descriptors were available again.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_RAN_OUT_OF_LINK_DESCRIPTORS_OFFSET    0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_RAN_OUT_OF_LINK_DESCRIPTORS_LSB       22
#define TQM_GEN_MPDUS_STATUS_3_RAN_OUT_OF_LINK_DESCRIPTORS_MASK      0x00400000

/* Description		TQM_GEN_MPDUS_STATUS_3_FIRST_MSDU_DID_NOT_FIT
			
			When set, the MPDU generation for this command got
			terminated because the very first msdu was already too long
			to fit into an MPDU.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_FIRST_MSDU_DID_NOT_FIT_OFFSET         0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_FIRST_MSDU_DID_NOT_FIT_LSB            23
#define TQM_GEN_MPDUS_STATUS_3_FIRST_MSDU_DID_NOT_FIT_MASK           0x00800000

/* Description		TQM_GEN_MPDUS_STATUS_3_GEN_MPDU_END_REASON
			
			The end reason for TQM to finish mpdu generation
			
			
			
			This is DV only. Do NOT use. Not all functionality is
			validated.
			
			
			
			<enum 0 GEN_MPDU_SUCCESSFUL_COMPLETION>
			
			<enum 1 MSDU_MIX_RATIO_ALL_FLOWS_IS_0>
			
			<enum 2 MAX_MPDU_SIZE_IS_0>
			
			<enum 3 MAX_MSDU_COUNT_IN_MPDU_IS_0>
			
			<enum 4 TARGET_MPDU_LIMIT_REACHED>
			
			<enum 5 GEN_MPDU_WINDOW_SIZE_REACHED>
			
			<enum 6 TARGET_EXT_DESC_USAGE_COUNT_REACHED>
			
			<enum 7 GEN_MPDU_PAUSE_ASSERTED >
			
			<enum 8 FIRST_MSDU_DID_NOT_FIT_ERROR >
			
			<enum 9 RAN_OUT_OF_MSDUS_IN_ALL_VALID_FLOWS>
			
			<enum 10 GEN_MPDU_ALL_FLOW_QUEUES_INVALID>
			
			<enum 11 GEN_MPDU_QUEUE_HEAD_INVALID>
			
			<enum 12 GEN_MPDU_FLUSH_ASSERTED>
			
			<enum 13 GEN_MPDU_MAX_MPDU_COUNT_REACHED>
			
			<enum 14 GEN_MPDU_MAX_MPDU_GEN_TIME_REACHED>
			
			<enum 15 GEN_MPDU_END_FOR_MULTIPLE_REASONS>
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_3_GEN_MPDU_END_REASON_OFFSET            0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_GEN_MPDU_END_REASON_LSB               24
#define TQM_GEN_MPDUS_STATUS_3_GEN_MPDU_END_REASON_MASK              0x0f000000

/* Description		TQM_GEN_MPDUS_STATUS_3_RESERVED_3A
			
			<legal 0>
*/
#define TQM_GEN_MPDUS_STATUS_3_RESERVED_3A_OFFSET                    0x0000000c
#define TQM_GEN_MPDUS_STATUS_3_RESERVED_3A_LSB                       28
#define TQM_GEN_MPDUS_STATUS_3_RESERVED_3A_MASK                      0xf0000000

/* Description		TQM_GEN_MPDUS_STATUS_4_FLOW0_REMAINING_MSDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow0_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 0 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_0 is set to 0 
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_4_FLOW0_REMAINING_MSDU_COUNT_OFFSET     0x00000010
#define TQM_GEN_MPDUS_STATUS_4_FLOW0_REMAINING_MSDU_COUNT_LSB        0
#define TQM_GEN_MPDUS_STATUS_4_FLOW0_REMAINING_MSDU_COUNT_MASK       0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_4_FLOW1_REMAINING_MSDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow1_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 1 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_1 is set to 0 <legal all>
*/
#define TQM_GEN_MPDUS_STATUS_4_FLOW1_REMAINING_MSDU_COUNT_OFFSET     0x00000010
#define TQM_GEN_MPDUS_STATUS_4_FLOW1_REMAINING_MSDU_COUNT_LSB        16
#define TQM_GEN_MPDUS_STATUS_4_FLOW1_REMAINING_MSDU_COUNT_MASK       0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_5_FLOW2_REMAINING_MSDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow2_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 2 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_2 is set to 0 
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_5_FLOW2_REMAINING_MSDU_COUNT_OFFSET     0x00000014
#define TQM_GEN_MPDUS_STATUS_5_FLOW2_REMAINING_MSDU_COUNT_LSB        0
#define TQM_GEN_MPDUS_STATUS_5_FLOW2_REMAINING_MSDU_COUNT_MASK       0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_5_FLOW3_REMAINING_MSDU_COUNT
			
			Consumer: SW/DEBUG
			
			Producer: TQM
			
			
			
			Field only valid when Flow3_Remaining_msdu_count_valid
			is set
			
			
			
			The total number of MSDUs remaining in the flow 3 after
			command execution finished.
			
			Note that by the time that SW reads this STATUS TLV, the
			real number might be larger, as new MSDUs can be added to
			the MSDU flows at any time.
			
			
			
			This field is set to 0, when in the TQM_GEN_MPDUS
			command the MSDU_mix_ratio_flow_3 is set to 0 
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_5_FLOW3_REMAINING_MSDU_COUNT_OFFSET     0x00000014
#define TQM_GEN_MPDUS_STATUS_5_FLOW3_REMAINING_MSDU_COUNT_LSB        16
#define TQM_GEN_MPDUS_STATUS_5_FLOW3_REMAINING_MSDU_COUNT_MASK       0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_6_MPDU_START_SEQUENCE_NUMBER
			
			Sequence number of  the first MPDU in the queue. If the
			queue is empty, this number will be set equal to the field
			MPDU_last_sequence_number field.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_6_MPDU_START_SEQUENCE_NUMBER_OFFSET     0x00000018
#define TQM_GEN_MPDUS_STATUS_6_MPDU_START_SEQUENCE_NUMBER_LSB        0
#define TQM_GEN_MPDUS_STATUS_6_MPDU_START_SEQUENCE_NUMBER_MASK       0x00000fff

/* Description		TQM_GEN_MPDUS_STATUS_6_RESERVED_6A
			
			<legal 0>
*/
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6A_OFFSET                    0x00000018
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6A_LSB                       12
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6A_MASK                      0x0000f000

/* Description		TQM_GEN_MPDUS_STATUS_6_MPDU_LAST_SEQUENCE_NUMBER
			
			Sequence number assigned to the last MPDU in the queue.
			
			
			
			If the queue is empty, this number shall be equal to
			MPDU_start_sequence_number, and represents the value that
			was assigned to the last generated MPDU.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_6_MPDU_LAST_SEQUENCE_NUMBER_OFFSET      0x00000018
#define TQM_GEN_MPDUS_STATUS_6_MPDU_LAST_SEQUENCE_NUMBER_LSB         16
#define TQM_GEN_MPDUS_STATUS_6_MPDU_LAST_SEQUENCE_NUMBER_MASK        0x0fff0000

/* Description		TQM_GEN_MPDUS_STATUS_6_RESERVED_6B
			
			<legal 0>
*/
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6B_OFFSET                    0x00000018
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6B_LSB                       28
#define TQM_GEN_MPDUS_STATUS_6_RESERVED_6B_MASK                      0xf0000000

/* Description		TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE0_VALID_ERROR
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE0_VALID_ERROR_OFFSET        0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE0_VALID_ERROR_LSB           0
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE0_VALID_ERROR_MASK          0x00000001

/* Description		TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE1_VALID_ERROR
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE1_VALID_ERROR_OFFSET        0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE1_VALID_ERROR_LSB           1
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE1_VALID_ERROR_MASK          0x00000002

/* Description		TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE2_VALID_ERROR
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE2_VALID_ERROR_OFFSET        0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE2_VALID_ERROR_LSB           2
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE2_VALID_ERROR_MASK          0x00000004

/* Description		TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE3_VALID_ERROR
			
			When set, TQM found that the valid bit in the flow queue
			descriptor for this flow was not set.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE3_VALID_ERROR_OFFSET        0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE3_VALID_ERROR_LSB           3
#define TQM_GEN_MPDUS_STATUS_7_FLOW_QUEUE3_VALID_ERROR_MASK          0x00000008

/* Description		TQM_GEN_MPDUS_STATUS_7_RESERVED_7A
			
			<legal 0>
*/
#define TQM_GEN_MPDUS_STATUS_7_RESERVED_7A_OFFSET                    0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_RESERVED_7A_LSB                       4
#define TQM_GEN_MPDUS_STATUS_7_RESERVED_7A_MASK                      0x0000fff0

/* Description		TQM_GEN_MPDUS_STATUS_7_TX_MPDU_QUEUE_NUMBER
			
			Indicates the MPDU queue ID for which MPDUs were
			generated
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_7_TX_MPDU_QUEUE_NUMBER_OFFSET           0x0000001c
#define TQM_GEN_MPDUS_STATUS_7_TX_MPDU_QUEUE_NUMBER_LSB              16
#define TQM_GEN_MPDUS_STATUS_7_TX_MPDU_QUEUE_NUMBER_MASK             0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_0
			
			Indicates the flow ID flow queue 0 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_0_OFFSET               0x00000020
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_0_LSB                  0
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_0_MASK                 0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_1
			
			Indicates the flow ID flow queue 1 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_1_OFFSET               0x00000020
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_1_LSB                  16
#define TQM_GEN_MPDUS_STATUS_8_TX_FLOW_NUMBER_1_MASK                 0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_2
			
			Indicates the flow ID flow queue 2 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_2_OFFSET               0x00000024
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_2_LSB                  0
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_2_MASK                 0x0000ffff

/* Description		TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_3
			
			Indicates the flow ID flow queue 3 
			
			Set to 0 if this flow was not accessed or programmed
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_3_OFFSET               0x00000024
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_3_LSB                  16
#define TQM_GEN_MPDUS_STATUS_9_TX_FLOW_NUMBER_3_MASK                 0xffff0000

/* Description		TQM_GEN_MPDUS_STATUS_10_RESERVED_10A
			
			<legal 0>
*/
#define TQM_GEN_MPDUS_STATUS_10_RESERVED_10A_OFFSET                  0x00000028
#define TQM_GEN_MPDUS_STATUS_10_RESERVED_10A_LSB                     0
#define TQM_GEN_MPDUS_STATUS_10_RESERVED_10A_MASK                    0x0fffffff

/* Description		TQM_GEN_MPDUS_STATUS_10_LOOPING_COUNT
			
			A count value that indicates the number of times the
			producer of entries into this Ring has looped around the
			ring.
			
			At initialization time, this value is set to 0. On the
			first loop, this value is set to 1. After the max value is
			reached allowed by the number of bits for this field, the
			count value continues with 0 again.
			
			
			
			In case SW is the consumer of the ring entries, it can
			use this field to figure out up to where the producer of
			entries has created new entries. This eliminates the need to
			check where the head pointer' of the ring is located once
			the SW starts processing an interrupt indicating that new
			entries have been put into this ring...
			
			
			
			Also note that SW if it wants only needs to look at the
			LSB bit of this count value.
			
			<legal all>
*/
#define TQM_GEN_MPDUS_STATUS_10_LOOPING_COUNT_OFFSET                 0x00000028
#define TQM_GEN_MPDUS_STATUS_10_LOOPING_COUNT_LSB                    28
#define TQM_GEN_MPDUS_STATUS_10_LOOPING_COUNT_MASK                   0xf0000000


#endif // _TQM_GEN_MPDUS_STATUS_H_
