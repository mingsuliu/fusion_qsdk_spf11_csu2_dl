// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "tqm_get_mpdu_head_info_status.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<tqm_get_mpdu_head_info_status, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"tqm_get_mpdu_head_info_status"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(status_header);
    MEX_PARSE_TLV_FIELD(mpdu_length);
    MEX_PARSE_TLV_FIELD(mpdu_sequence_number);
    MEX_PARSE_TLV_FIELD(fw_tx_notify_frame);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(msdu_count);
    MEX_PARSE_TLV_FIELD(transmit_count);
    MEX_PARSE_TLV_FIELD(reserved_3a);
    MEX_PARSE_TLV_FIELD(total_queue_mpdu_count);
    MEX_PARSE_TLV_FIELD(msdu_0);
    MEX_PARSE_TLV_FIELD(tx_mpdu_queue_number);
    MEX_PARSE_TLV_FIELD(reserved_8a);
    MEX_PARSE_TLV_FIELD(reserved_9a);
    MEX_PARSE_TLV_FIELD(reserved_10a);
    MEX_PARSE_TLV_FIELD(looping_count);
  }
};


