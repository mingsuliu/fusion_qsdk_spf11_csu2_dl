// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "rx_ring_mask.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<rx_ring_mask, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"rx_ring_mask"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(rxdma0_status_ring_mask);
    MEX_PARSE_TLV_FIELD(rxdma0_buffer_source_ring_mask);
    MEX_PARSE_TLV_FIELD(rxdma1_status_ring_mask);
    MEX_PARSE_TLV_FIELD(rxdma1_buffer_source_ring_mask);
    MEX_PARSE_TLV_FIELD(reserved_0);
  }
};


