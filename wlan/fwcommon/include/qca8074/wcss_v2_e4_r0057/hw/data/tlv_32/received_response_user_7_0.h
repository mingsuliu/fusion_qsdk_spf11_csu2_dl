// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RECEIVED_RESPONSE_USER_7_0_H_
#define _RECEIVED_RESPONSE_USER_7_0_H_
#if !defined(__ASSEMBLER__)
#endif

#include "received_response_user_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-1	struct received_response_user_info received_response_details_user0;
//	2-3	struct received_response_user_info received_response_details_user1;
//	4-5	struct received_response_user_info received_response_details_user2;
//	6-7	struct received_response_user_info received_response_details_user3;
//	8-9	struct received_response_user_info received_response_details_user4;
//	10-11	struct received_response_user_info received_response_details_user5;
//	12-13	struct received_response_user_info received_response_details_user6;
//	14-15	struct received_response_user_info received_response_details_user7;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RECEIVED_RESPONSE_USER_7_0 16

struct received_response_user_7_0 {
    struct            received_response_user_info                       received_response_details_user0;
    struct            received_response_user_info                       received_response_details_user1;
    struct            received_response_user_info                       received_response_details_user2;
    struct            received_response_user_info                       received_response_details_user3;
    struct            received_response_user_info                       received_response_details_user4;
    struct            received_response_user_info                       received_response_details_user5;
    struct            received_response_user_info                       received_response_details_user6;
    struct            received_response_user_info                       received_response_details_user7;
};

/*

struct received_response_user_info received_response_details_user0
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user1
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user2
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user3
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user4
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user5
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user6
			
			Field contains details about the response received for
			this user

struct received_response_user_info received_response_details_user7
			
			Field contains details about the response received for
			this user
*/


 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user0 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_PASS_COUNT_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_DATA_FRAME_COUNT_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_QOSNULL_FRAME_COUNT_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_0A_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_USER_INFO_VALID_OFFSET 0x00000000
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_0_RECEIVED_RESPONSE_DETAILS_USER0_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_NULL_DELIMITER_COUNT_OFFSET 0x00000004
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_1A_OFFSET 0x00000004
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_1_RECEIVED_RESPONSE_DETAILS_USER0_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user1 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_PASS_COUNT_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_DATA_FRAME_COUNT_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_QOSNULL_FRAME_COUNT_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_0A_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_USER_INFO_VALID_OFFSET 0x00000008
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_2_RECEIVED_RESPONSE_DETAILS_USER1_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_NULL_DELIMITER_COUNT_OFFSET 0x0000000c
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_1A_OFFSET 0x0000000c
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_3_RECEIVED_RESPONSE_DETAILS_USER1_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user2 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_PASS_COUNT_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_DATA_FRAME_COUNT_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_QOSNULL_FRAME_COUNT_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_0A_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_USER_INFO_VALID_OFFSET 0x00000010
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_4_RECEIVED_RESPONSE_DETAILS_USER2_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_NULL_DELIMITER_COUNT_OFFSET 0x00000014
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_1A_OFFSET 0x00000014
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_5_RECEIVED_RESPONSE_DETAILS_USER2_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user3 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_PASS_COUNT_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_DATA_FRAME_COUNT_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_QOSNULL_FRAME_COUNT_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_0A_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_USER_INFO_VALID_OFFSET 0x00000018
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_6_RECEIVED_RESPONSE_DETAILS_USER3_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_NULL_DELIMITER_COUNT_OFFSET 0x0000001c
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_1A_OFFSET 0x0000001c
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_7_RECEIVED_RESPONSE_DETAILS_USER3_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user4 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_PASS_COUNT_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_DATA_FRAME_COUNT_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_QOSNULL_FRAME_COUNT_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_0A_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_USER_INFO_VALID_OFFSET 0x00000020
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_8_RECEIVED_RESPONSE_DETAILS_USER4_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_NULL_DELIMITER_COUNT_OFFSET 0x00000024
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_1A_OFFSET 0x00000024
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_9_RECEIVED_RESPONSE_DETAILS_USER4_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user5 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_PASS_COUNT_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_DATA_FRAME_COUNT_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_QOSNULL_FRAME_COUNT_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_0A_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_USER_INFO_VALID_OFFSET 0x00000028
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_10_RECEIVED_RESPONSE_DETAILS_USER5_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_NULL_DELIMITER_COUNT_OFFSET 0x0000002c
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_1A_OFFSET 0x0000002c
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_11_RECEIVED_RESPONSE_DETAILS_USER5_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user6 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_PASS_COUNT_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_DATA_FRAME_COUNT_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_QOSNULL_FRAME_COUNT_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_0A_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_USER_INFO_VALID_OFFSET 0x00000030
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_12_RECEIVED_RESPONSE_DETAILS_USER6_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_NULL_DELIMITER_COUNT_OFFSET 0x00000034
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_1A_OFFSET 0x00000034
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_13_RECEIVED_RESPONSE_DETAILS_USER6_RESERVED_1A_MASK 0xffff0000

 /* EXTERNAL REFERENCE : struct received_response_user_info received_response_details_user7 */ 


/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_PASS_COUNT
			
			The number of MPDUs received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_PASS_COUNT_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_PASS_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_PASS_COUNT_MASK 0x000000ff

/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_FAIL_COUNT
			
			The number of MPDUs received with wrong FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_FAIL_COUNT_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_FAIL_COUNT_LSB 8
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_MPDU_FCS_FAIL_COUNT_MASK 0x0000ff00

/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_DATA_FRAME_COUNT
			
			The number of data frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_DATA_FRAME_COUNT_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_DATA_FRAME_COUNT_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_DATA_FRAME_COUNT_MASK 0x00ff0000

/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_QOSNULL_FRAME_COUNT
			
			The number of QoSNULL frames received with correct FCS.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_QOSNULL_FRAME_COUNT_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_QOSNULL_FRAME_COUNT_LSB 24
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_QOSNULL_FRAME_COUNT_MASK 0x0f000000

/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_0A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_0A_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_0A_LSB 28
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_0A_MASK 0x70000000

/* Description		RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_USER_INFO_VALID
			
			When set, this RECEIVED_RESPONSE_USER_INFO STRUCT
			contains valid information.
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_USER_INFO_VALID_OFFSET 0x00000038
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_USER_INFO_VALID_LSB 31
#define RECEIVED_RESPONSE_USER_7_0_14_RECEIVED_RESPONSE_DETAILS_USER7_USER_INFO_VALID_MASK 0x80000000

/* Description		RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_NULL_DELIMITER_COUNT
			
			The number of valid, properly formed NULL delimiters
			received
			
			<legal all>
*/
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_NULL_DELIMITER_COUNT_OFFSET 0x0000003c
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_NULL_DELIMITER_COUNT_LSB 0
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_NULL_DELIMITER_COUNT_MASK 0x0000ffff

/* Description		RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_1A
			
			<legal 0>
*/
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_1A_OFFSET 0x0000003c
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_1A_LSB 16
#define RECEIVED_RESPONSE_USER_7_0_15_RECEIVED_RESPONSE_DETAILS_USER7_RESERVED_1A_MASK 0xffff0000


#endif // _RECEIVED_RESPONSE_USER_7_0_H_
