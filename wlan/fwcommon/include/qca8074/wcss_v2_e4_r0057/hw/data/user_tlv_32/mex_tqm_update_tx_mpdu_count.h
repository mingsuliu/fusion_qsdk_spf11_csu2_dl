// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "tqm_update_tx_mpdu_count.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<tqm_update_tx_mpdu_count, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"tqm_update_tx_mpdu_count"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(cmd_header);
    MEX_PARSE_TLV_FIELD(mpdu_queue_desc_addr_31_0);
    MEX_PARSE_TLV_FIELD(mpdu_queue_desc_addr_39_32);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(mpdu_tx_count);
    MEX_PARSE_TLV_FIELD(reserved_2b);
    MEX_PARSE_TLV_FIELD(frame_not_from_tqm_count);
    MEX_PARSE_TLV_FIELD(reserved_2c);
    MEX_PARSE_TLV_FIELD(tx_rate_stats);
  }
};


