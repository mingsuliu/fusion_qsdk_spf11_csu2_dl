// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _RX_FRAME_BITMAP_ACK_H_
#define _RX_FRAME_BITMAP_ACK_H_
#if !defined(__ASSEMBLER__)
#endif


// ################ START SUMMARY #################
//
//	Dword	Fields
//	0	no_bitmap_available[0], explicit_ack[1], explict_ack_type[4:2], ba_bitmap_size[6:5], reserved_0a[9:7], ba_tid[13:10], sta_full_aid[26:14], reserved_0b[31:27]
//	1	addr1_31_0[31:0]
//	2	addr1_47_32[15:0], addr2_15_0[31:16]
//	3	addr2_47_16[31:0]
//	4	ba_ts_ctrl[15:0], ba_ts_seq[31:16]
//	5	ba_ts_bitmap_31_0[31:0]
//	6	ba_ts_bitmap_63_32[31:0]
//	7	ba_ts_bitmap_95_64[31:0]
//	8	ba_ts_bitmap_127_96[31:0]
//	9	ba_ts_bitmap_159_128[31:0]
//	10	ba_ts_bitmap_191_160[31:0]
//	11	ba_ts_bitmap_223_192[31:0]
//	12	ba_ts_bitmap_255_224[31:0]
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_RX_FRAME_BITMAP_ACK 13

struct rx_frame_bitmap_ack {
             uint32_t no_bitmap_available             :  1, //[0]
                      explicit_ack                    :  1, //[1]
                      explict_ack_type                :  3, //[4:2]
                      ba_bitmap_size                  :  2, //[6:5]
                      reserved_0a                     :  3, //[9:7]
                      ba_tid                          :  4, //[13:10]
                      sta_full_aid                    : 13, //[26:14]
                      reserved_0b                     :  5; //[31:27]
             uint32_t addr1_31_0                      : 32; //[31:0]
             uint32_t addr1_47_32                     : 16, //[15:0]
                      addr2_15_0                      : 16; //[31:16]
             uint32_t addr2_47_16                     : 32; //[31:0]
             uint32_t ba_ts_ctrl                      : 16, //[15:0]
                      ba_ts_seq                       : 16; //[31:16]
             uint32_t ba_ts_bitmap_31_0               : 32; //[31:0]
             uint32_t ba_ts_bitmap_63_32              : 32; //[31:0]
             uint32_t ba_ts_bitmap_95_64              : 32; //[31:0]
             uint32_t ba_ts_bitmap_127_96             : 32; //[31:0]
             uint32_t ba_ts_bitmap_159_128            : 32; //[31:0]
             uint32_t ba_ts_bitmap_191_160            : 32; //[31:0]
             uint32_t ba_ts_bitmap_223_192            : 32; //[31:0]
             uint32_t ba_ts_bitmap_255_224            : 32; //[31:0]
};

/*

no_bitmap_available
			
			When set, RXPCU does not have any info available for the
			requested user. 
			
			
			
			RXPCU will set the TA/RA, addresses with the devices OWN
			address.
			
			All other fields are set to 0
			
			
			
			TXPCU will just blindly follow RXPCUs info.
			
			(only for status reporting is TXPCU using this).
			
			
			
			Note that this field and field Explicit_ack can not be
			simultaneously set.
			
			<legal all>

explicit_ack
			
			When set, no BA is needed for this STA. Instead just a
			single ACK indication 
			
			
			
			Note that this field and field No_bitmap_available can
			not be simultaneously set.
			
			
			
			Also note that RXPCU might not know if the response that
			TXPCU is generating is a single ACK or M(sta) BA.
			
			For that reason, RXPCU shall also properly fill in all
			the BA related fields. TXPCU will based on the explicit ack
			type and in case of BA type response, blindely copy the
			required BA related fields and not change their contents:
			
			The related fields are:
			
			Ba_tid 
			
			ba_ts_ctrl 
			
			ba_ts_seq
			
			ba_ts_bitmap_...
			
			
			
			<legal all>

explict_ack_type
			
			Field only valid when Explicit_ack is set
			
			
			
			Note that TXPCU only needs to evaluate this field in
			case of generating a multi (STA) BA
			
			
			
			<enum 0 ack_for_single_data_frame> set when only a
			single data frame was received that indicated explicitly a
			'normal' ack (no BA) to be sent.
			
			<enum 1 ack_for_management> set when a management frame
			was received
			
			<enum 2 ack_for_PSPOLL> set when a PS_POLL frame was
			received
			
			<enum 3 ack_for_assoc_request> set when an association
			request was received from an unassociated STA.
			
			<enum 4 ack_for_all_frames> set when RXPCU determined
			that all frames have been properly received.
			
			<legal 0-4>

ba_bitmap_size
			
			Field not valid when No_bitmap_available or Explicit_ack
			is set.
			
			
			
			
			
			<enum 0 BA_bitmap_32 > Bitmap size set to window of 32
			
			<enum 1 BA_bitmap_64 > Bitmap size set to window of 64
			
			
			
			
			
			<legal 0-3>

reserved_0a
			
			<legal 0>

ba_tid
			
			The tid for the BA

sta_full_aid
			
			The full AID of this station. 

reserved_0b
			
			<legal 0>

addr1_31_0
			
			lower 32 bits of addr1 of the received frame

addr1_47_32
			
			upper 16 bits of addr1 of the received frame

addr2_15_0
			
			lower 16 bits of addr2 of the received frame

addr2_47_16
			
			upper 32 bits of addr2 of the received frame

ba_ts_ctrl
			
			Transmit BA control
			
			RXPCU assumes the C-BA format, NOT M-BA format.
			
			In case TXPCU is responding with M-BA, TXPCU will ignore
			this field. TXPCU will generate it 

ba_ts_seq
			
			Transmit BA sequence number. 

ba_ts_bitmap_31_0
			
			Transmit BA bitmap[31:0]

ba_ts_bitmap_63_32
			
			Transmit BA bitmap[63:32]

ba_ts_bitmap_95_64
			
			Transmit BA bitmap[95:64]

ba_ts_bitmap_127_96
			
			Transmit BA bitmap[127:96]

ba_ts_bitmap_159_128
			
			Transmit BA bitmap[159:128]

ba_ts_bitmap_191_160
			
			Transmit BA bitmap[191:160]

ba_ts_bitmap_223_192
			
			Transmit BA bitmap[223:192]

ba_ts_bitmap_255_224
			
			Transmit BA bitmap[255:224]
*/


/* Description		RX_FRAME_BITMAP_ACK_0_NO_BITMAP_AVAILABLE
			
			When set, RXPCU does not have any info available for the
			requested user. 
			
			
			
			RXPCU will set the TA/RA, addresses with the devices OWN
			address.
			
			All other fields are set to 0
			
			
			
			TXPCU will just blindly follow RXPCUs info.
			
			(only for status reporting is TXPCU using this).
			
			
			
			Note that this field and field Explicit_ack can not be
			simultaneously set.
			
			<legal all>
*/
#define RX_FRAME_BITMAP_ACK_0_NO_BITMAP_AVAILABLE_OFFSET             0x00000000
#define RX_FRAME_BITMAP_ACK_0_NO_BITMAP_AVAILABLE_LSB                0
#define RX_FRAME_BITMAP_ACK_0_NO_BITMAP_AVAILABLE_MASK               0x00000001

/* Description		RX_FRAME_BITMAP_ACK_0_EXPLICIT_ACK
			
			When set, no BA is needed for this STA. Instead just a
			single ACK indication 
			
			
			
			Note that this field and field No_bitmap_available can
			not be simultaneously set.
			
			
			
			Also note that RXPCU might not know if the response that
			TXPCU is generating is a single ACK or M(sta) BA.
			
			For that reason, RXPCU shall also properly fill in all
			the BA related fields. TXPCU will based on the explicit ack
			type and in case of BA type response, blindely copy the
			required BA related fields and not change their contents:
			
			The related fields are:
			
			Ba_tid 
			
			ba_ts_ctrl 
			
			ba_ts_seq
			
			ba_ts_bitmap_...
			
			
			
			<legal all>
*/
#define RX_FRAME_BITMAP_ACK_0_EXPLICIT_ACK_OFFSET                    0x00000000
#define RX_FRAME_BITMAP_ACK_0_EXPLICIT_ACK_LSB                       1
#define RX_FRAME_BITMAP_ACK_0_EXPLICIT_ACK_MASK                      0x00000002

/* Description		RX_FRAME_BITMAP_ACK_0_EXPLICT_ACK_TYPE
			
			Field only valid when Explicit_ack is set
			
			
			
			Note that TXPCU only needs to evaluate this field in
			case of generating a multi (STA) BA
			
			
			
			<enum 0 ack_for_single_data_frame> set when only a
			single data frame was received that indicated explicitly a
			'normal' ack (no BA) to be sent.
			
			<enum 1 ack_for_management> set when a management frame
			was received
			
			<enum 2 ack_for_PSPOLL> set when a PS_POLL frame was
			received
			
			<enum 3 ack_for_assoc_request> set when an association
			request was received from an unassociated STA.
			
			<enum 4 ack_for_all_frames> set when RXPCU determined
			that all frames have been properly received.
			
			<legal 0-4>
*/
#define RX_FRAME_BITMAP_ACK_0_EXPLICT_ACK_TYPE_OFFSET                0x00000000
#define RX_FRAME_BITMAP_ACK_0_EXPLICT_ACK_TYPE_LSB                   2
#define RX_FRAME_BITMAP_ACK_0_EXPLICT_ACK_TYPE_MASK                  0x0000001c

/* Description		RX_FRAME_BITMAP_ACK_0_BA_BITMAP_SIZE
			
			Field not valid when No_bitmap_available or Explicit_ack
			is set.
			
			
			
			
			
			<enum 0 BA_bitmap_32 > Bitmap size set to window of 32
			
			<enum 1 BA_bitmap_64 > Bitmap size set to window of 64
			
			
			
			
			
			<legal 0-3>
*/
#define RX_FRAME_BITMAP_ACK_0_BA_BITMAP_SIZE_OFFSET                  0x00000000
#define RX_FRAME_BITMAP_ACK_0_BA_BITMAP_SIZE_LSB                     5
#define RX_FRAME_BITMAP_ACK_0_BA_BITMAP_SIZE_MASK                    0x00000060

/* Description		RX_FRAME_BITMAP_ACK_0_RESERVED_0A
			
			<legal 0>
*/
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0A_OFFSET                     0x00000000
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0A_LSB                        7
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0A_MASK                       0x00000380

/* Description		RX_FRAME_BITMAP_ACK_0_BA_TID
			
			The tid for the BA
*/
#define RX_FRAME_BITMAP_ACK_0_BA_TID_OFFSET                          0x00000000
#define RX_FRAME_BITMAP_ACK_0_BA_TID_LSB                             10
#define RX_FRAME_BITMAP_ACK_0_BA_TID_MASK                            0x00003c00

/* Description		RX_FRAME_BITMAP_ACK_0_STA_FULL_AID
			
			The full AID of this station. 
*/
#define RX_FRAME_BITMAP_ACK_0_STA_FULL_AID_OFFSET                    0x00000000
#define RX_FRAME_BITMAP_ACK_0_STA_FULL_AID_LSB                       14
#define RX_FRAME_BITMAP_ACK_0_STA_FULL_AID_MASK                      0x07ffc000

/* Description		RX_FRAME_BITMAP_ACK_0_RESERVED_0B
			
			<legal 0>
*/
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0B_OFFSET                     0x00000000
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0B_LSB                        27
#define RX_FRAME_BITMAP_ACK_0_RESERVED_0B_MASK                       0xf8000000

/* Description		RX_FRAME_BITMAP_ACK_1_ADDR1_31_0
			
			lower 32 bits of addr1 of the received frame
*/
#define RX_FRAME_BITMAP_ACK_1_ADDR1_31_0_OFFSET                      0x00000004
#define RX_FRAME_BITMAP_ACK_1_ADDR1_31_0_LSB                         0
#define RX_FRAME_BITMAP_ACK_1_ADDR1_31_0_MASK                        0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_2_ADDR1_47_32
			
			upper 16 bits of addr1 of the received frame
*/
#define RX_FRAME_BITMAP_ACK_2_ADDR1_47_32_OFFSET                     0x00000008
#define RX_FRAME_BITMAP_ACK_2_ADDR1_47_32_LSB                        0
#define RX_FRAME_BITMAP_ACK_2_ADDR1_47_32_MASK                       0x0000ffff

/* Description		RX_FRAME_BITMAP_ACK_2_ADDR2_15_0
			
			lower 16 bits of addr2 of the received frame
*/
#define RX_FRAME_BITMAP_ACK_2_ADDR2_15_0_OFFSET                      0x00000008
#define RX_FRAME_BITMAP_ACK_2_ADDR2_15_0_LSB                         16
#define RX_FRAME_BITMAP_ACK_2_ADDR2_15_0_MASK                        0xffff0000

/* Description		RX_FRAME_BITMAP_ACK_3_ADDR2_47_16
			
			upper 32 bits of addr2 of the received frame
*/
#define RX_FRAME_BITMAP_ACK_3_ADDR2_47_16_OFFSET                     0x0000000c
#define RX_FRAME_BITMAP_ACK_3_ADDR2_47_16_LSB                        0
#define RX_FRAME_BITMAP_ACK_3_ADDR2_47_16_MASK                       0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_4_BA_TS_CTRL
			
			Transmit BA control
			
			RXPCU assumes the C-BA format, NOT M-BA format.
			
			In case TXPCU is responding with M-BA, TXPCU will ignore
			this field. TXPCU will generate it 
*/
#define RX_FRAME_BITMAP_ACK_4_BA_TS_CTRL_OFFSET                      0x00000010
#define RX_FRAME_BITMAP_ACK_4_BA_TS_CTRL_LSB                         0
#define RX_FRAME_BITMAP_ACK_4_BA_TS_CTRL_MASK                        0x0000ffff

/* Description		RX_FRAME_BITMAP_ACK_4_BA_TS_SEQ
			
			Transmit BA sequence number. 
*/
#define RX_FRAME_BITMAP_ACK_4_BA_TS_SEQ_OFFSET                       0x00000010
#define RX_FRAME_BITMAP_ACK_4_BA_TS_SEQ_LSB                          16
#define RX_FRAME_BITMAP_ACK_4_BA_TS_SEQ_MASK                         0xffff0000

/* Description		RX_FRAME_BITMAP_ACK_5_BA_TS_BITMAP_31_0
			
			Transmit BA bitmap[31:0]
*/
#define RX_FRAME_BITMAP_ACK_5_BA_TS_BITMAP_31_0_OFFSET               0x00000014
#define RX_FRAME_BITMAP_ACK_5_BA_TS_BITMAP_31_0_LSB                  0
#define RX_FRAME_BITMAP_ACK_5_BA_TS_BITMAP_31_0_MASK                 0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_6_BA_TS_BITMAP_63_32
			
			Transmit BA bitmap[63:32]
*/
#define RX_FRAME_BITMAP_ACK_6_BA_TS_BITMAP_63_32_OFFSET              0x00000018
#define RX_FRAME_BITMAP_ACK_6_BA_TS_BITMAP_63_32_LSB                 0
#define RX_FRAME_BITMAP_ACK_6_BA_TS_BITMAP_63_32_MASK                0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_7_BA_TS_BITMAP_95_64
			
			Transmit BA bitmap[95:64]
*/
#define RX_FRAME_BITMAP_ACK_7_BA_TS_BITMAP_95_64_OFFSET              0x0000001c
#define RX_FRAME_BITMAP_ACK_7_BA_TS_BITMAP_95_64_LSB                 0
#define RX_FRAME_BITMAP_ACK_7_BA_TS_BITMAP_95_64_MASK                0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_8_BA_TS_BITMAP_127_96
			
			Transmit BA bitmap[127:96]
*/
#define RX_FRAME_BITMAP_ACK_8_BA_TS_BITMAP_127_96_OFFSET             0x00000020
#define RX_FRAME_BITMAP_ACK_8_BA_TS_BITMAP_127_96_LSB                0
#define RX_FRAME_BITMAP_ACK_8_BA_TS_BITMAP_127_96_MASK               0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_9_BA_TS_BITMAP_159_128
			
			Transmit BA bitmap[159:128]
*/
#define RX_FRAME_BITMAP_ACK_9_BA_TS_BITMAP_159_128_OFFSET            0x00000024
#define RX_FRAME_BITMAP_ACK_9_BA_TS_BITMAP_159_128_LSB               0
#define RX_FRAME_BITMAP_ACK_9_BA_TS_BITMAP_159_128_MASK              0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_10_BA_TS_BITMAP_191_160
			
			Transmit BA bitmap[191:160]
*/
#define RX_FRAME_BITMAP_ACK_10_BA_TS_BITMAP_191_160_OFFSET           0x00000028
#define RX_FRAME_BITMAP_ACK_10_BA_TS_BITMAP_191_160_LSB              0
#define RX_FRAME_BITMAP_ACK_10_BA_TS_BITMAP_191_160_MASK             0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_11_BA_TS_BITMAP_223_192
			
			Transmit BA bitmap[223:192]
*/
#define RX_FRAME_BITMAP_ACK_11_BA_TS_BITMAP_223_192_OFFSET           0x0000002c
#define RX_FRAME_BITMAP_ACK_11_BA_TS_BITMAP_223_192_LSB              0
#define RX_FRAME_BITMAP_ACK_11_BA_TS_BITMAP_223_192_MASK             0xffffffff

/* Description		RX_FRAME_BITMAP_ACK_12_BA_TS_BITMAP_255_224
			
			Transmit BA bitmap[255:224]
*/
#define RX_FRAME_BITMAP_ACK_12_BA_TS_BITMAP_255_224_OFFSET           0x00000030
#define RX_FRAME_BITMAP_ACK_12_BA_TS_BITMAP_255_224_LSB              0
#define RX_FRAME_BITMAP_ACK_12_BA_TS_BITMAP_255_224_MASK             0xffffffff


#endif // _RX_FRAME_BITMAP_ACK_H_
