// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _PHYRX_VHT_SIG_B_MU160_H_
#define _PHYRX_VHT_SIG_B_MU160_H_
#if !defined(__ASSEMBLER__)
#endif

#include "vht_sig_b_mu160_info.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-7	struct vht_sig_b_mu160_info phyrx_vht_sig_b_mu160_info_details;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_PHYRX_VHT_SIG_B_MU160 8

struct phyrx_vht_sig_b_mu160 {
    struct            vht_sig_b_mu160_info                       phyrx_vht_sig_b_mu160_info_details;
};

/*

struct vht_sig_b_mu160_info phyrx_vht_sig_b_mu160_info_details
			
			See detailed description of the STRUCT
*/


 /* EXTERNAL REFERENCE : struct vht_sig_b_mu160_info phyrx_vht_sig_b_mu160_info_details */ 


/* Description		PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH
			
			VHT-SIG-B Length (in units of 4 octets) = ceiling
			(LENGTH/4)  <legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_OFFSET 0x00000000
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_LSB 0
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS
			
			Modulation as described in vht_sig_a mcs field  <legal
			0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_OFFSET 0x00000000
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_LSB 19
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL
			
			Used to terminate the trellis of the convolutional
			decoder.
			
			Set to 0.  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_OFFSET 0x00000000
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_LSB 23
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_0
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_0_OFFSET 0x00000000
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_0_LSB 29
#define PHYRX_VHT_SIG_B_MU160_0_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_0_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_A
			
			Same as length. This field is not valid for RX packets
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_A_OFFSET 0x00000004
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_A_LSB 0
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_A_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_A
			
			Same as mcs. This field is not valid for RX packets 
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_A_OFFSET 0x00000004
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_A_LSB 19
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_A_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_A
			
			Same as tail. This field is not valid for RX packets 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_A_OFFSET 0x00000004
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_A_LSB 23
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_A_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_1
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_1_OFFSET 0x00000004
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_1_LSB 29
#define PHYRX_VHT_SIG_B_MU160_1_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_1_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_B
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_B_OFFSET 0x00000008
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_B_LSB 0
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_B_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_B
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_B_OFFSET 0x00000008
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_B_LSB 19
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_B_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_B
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_B_OFFSET 0x00000008
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_B_LSB 23
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_B_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_2
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_2_OFFSET 0x00000008
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_2_LSB 29
#define PHYRX_VHT_SIG_B_MU160_2_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_2_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_C
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_C_OFFSET 0x0000000c
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_C_LSB 0
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_C_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_C
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_C_OFFSET 0x0000000c
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_C_LSB 19
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_C_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_C
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_C_OFFSET 0x0000000c
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_C_LSB 23
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_C_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_3
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_3_OFFSET 0x0000000c
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_3_LSB 29
#define PHYRX_VHT_SIG_B_MU160_3_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_3_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_D
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_D_OFFSET 0x00000010
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_D_LSB 0
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_D_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_D
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_D_OFFSET 0x00000010
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_D_LSB 19
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_D_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_D
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_D_OFFSET 0x00000010
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_D_LSB 23
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_D_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_4
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_4_OFFSET 0x00000010
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_4_LSB 29
#define PHYRX_VHT_SIG_B_MU160_4_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_4_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_E
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_E_OFFSET 0x00000014
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_E_LSB 0
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_E_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_E
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_E_OFFSET 0x00000014
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_E_LSB 19
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_E_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_E
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_E_OFFSET 0x00000014
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_E_LSB 23
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_E_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_5
			
			Not part of VHT-SIG-B.
			
			Reserved: Set to 0 and ignored on receive  <legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_5_OFFSET 0x00000014
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_5_LSB 29
#define PHYRX_VHT_SIG_B_MU160_5_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_5_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_F
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_F_OFFSET 0x00000018
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_F_LSB 0
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_F_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_F
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_F_OFFSET 0x00000018
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_F_LSB 19
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_F_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_F
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_F_OFFSET 0x00000018
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_F_LSB 23
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_F_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MU_USER_NUMBER
			
			Not part of VHT-SIG-B.
			
			Mapping from user number (BFer hardware specific) to
			mu_user_number. The reader is directed to the previous
			chapter (User Number) for a definition of the terms user and
			mu_user.   <legal 0-3>
*/
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MU_USER_NUMBER_OFFSET 0x00000018
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MU_USER_NUMBER_LSB 29
#define PHYRX_VHT_SIG_B_MU160_6_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MU_USER_NUMBER_MASK 0xe0000000

/* Description		PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_G
			
			Same as length. This field is not valid for RX packets.
			<legal all>
*/
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_G_OFFSET 0x0000001c
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_G_LSB 0
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_LENGTH_COPY_G_MASK 0x0007ffff

/* Description		PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_G
			
			Same as mcs. This field is not valid for RX packets.  
			
			<legal 0-11>
*/
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_G_OFFSET 0x0000001c
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_G_LSB 19
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_MCS_COPY_G_MASK 0x00780000

/* Description		PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_G
			
			Same as tail. This field is not valid for RX packets. 
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_G_OFFSET 0x0000001c
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_G_LSB 23
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_TAIL_COPY_G_MASK 0x1f800000

/* Description		PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_7
			
			<legal 0>
*/
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_7_OFFSET 0x0000001c
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_7_LSB 29
#define PHYRX_VHT_SIG_B_MU160_7_PHYRX_VHT_SIG_B_MU160_INFO_DETAILS_RESERVED_7_MASK 0xe0000000


#endif // _PHYRX_VHT_SIG_B_MU160_H_
