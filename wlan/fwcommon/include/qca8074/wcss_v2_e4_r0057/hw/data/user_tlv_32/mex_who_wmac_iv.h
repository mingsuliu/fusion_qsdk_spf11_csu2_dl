// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "who_wmac_iv.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<who_wmac_iv, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"who_wmac_iv"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(iv_len);
    MEX_PARSE_TLV_FIELD(icv_len);
    MEX_PARSE_TLV_FIELD(incomplete_iv);
    MEX_PARSE_TLV_FIELD(reserved_0);
    MEX_PARSE_TLV_FIELD(iv_31_0);
    MEX_PARSE_TLV_FIELD(iv_63_32);
    MEX_PARSE_TLV_FIELD(iv_95_64);
    MEX_PARSE_TLV_FIELD(iv_127_96);
    MEX_PARSE_TLV_FIELD(iv_143_128);
    MEX_PARSE_TLV_FIELD(reserved_5);
  }
};


