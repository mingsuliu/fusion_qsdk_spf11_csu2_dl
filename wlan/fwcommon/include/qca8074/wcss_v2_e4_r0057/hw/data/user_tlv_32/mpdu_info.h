// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#ifndef _MPDU_INFO_H_
#define _MPDU_INFO_H_
#if !defined(__ASSEMBLER__)
#endif

#include "tx_mpdu_details.h"

// ################ START SUMMARY #################
//
//	Dword	Fields
//	0-4	struct tx_mpdu_details mpdu_0;
//
// ################ END SUMMARY #################

#define NUM_OF_DWORDS_MPDU_INFO 5

struct mpdu_info {
    struct            tx_mpdu_details                       mpdu_0;
};

/*

struct tx_mpdu_details mpdu_0
			
			Consumer: TXDMA/PDG
			
			Producer: TQM/SW
			
			
			
			Details of the MPDU in the list
*/


 /* EXTERNAL REFERENCE : struct tx_mpdu_details mpdu_0 */ 


 /* EXTERNAL REFERENCE : struct buffer_addr_info msdu_link_desc_addr_info */ 


/* Description		MPDU_INFO_0_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_31_0
			
			Address (lower 32 bits) of the MSDU buffer OR
			MSDU_EXTENSION descriptor OR Link Descriptor
			
			
			
			In case of 'NULL' pointer, this field is set to 0
			
			<legal all>
*/
#define MPDU_INFO_0_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_31_0_OFFSET 0x00000000
#define MPDU_INFO_0_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_31_0_LSB 0
#define MPDU_INFO_0_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_31_0_MASK 0xffffffff

/* Description		MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_39_32
			
			Address (upper 8 bits) of the MSDU buffer OR
			MSDU_EXTENSION descriptor OR Link Descriptor
			
			
			
			In case of 'NULL' pointer, this field is set to 0
			
			<legal all>
*/
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_39_32_OFFSET 0x00000004
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_39_32_LSB 0
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_BUFFER_ADDR_39_32_MASK 0x000000ff

/* Description		MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_RETURN_BUFFER_MANAGER
			
			Consumer: WBM
			
			Producer: SW/FW
			
			
			
			In case of 'NULL' pointer, this field is set to 0
			
			
			
			Indicates to which buffer manager the buffer OR
			MSDU_EXTENSION descriptor OR link descriptor that is being
			pointed to shall be returned after the frame has been
			processed. It is used by WBM for routing purposes.
			
			
			
			<enum 0 WBM_IDLE_BUF_LIST> This buffer shall be returned
			to the WMB buffer idle list
			
			<enum 1 WBM_IDLE_DESC_LIST> This buffer shall be
			returned to the WMB idle link descriptor idle list
			
			<enum 2 FW_BM> This buffer shall be returned to the FW
			
			<enum 3 SW0_BM> This buffer shall be returned to the SW,
			ring 0
			
			<enum 4 SW1_BM> This buffer shall be returned to the SW,
			ring 1
			
			<enum 5 SW2_BM> This buffer shall be returned to the SW,
			ring 2
			
			<enum 6 SW3_BM> This buffer shall be returned to the SW,
			ring 3
			
			<enum 7 SW4_BM> This buffer shall be returned to the SW,
			ring 3
			
			
			
			<legal all>
*/
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_RETURN_BUFFER_MANAGER_OFFSET 0x00000004
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_RETURN_BUFFER_MANAGER_LSB 8
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_RETURN_BUFFER_MANAGER_MASK 0x00000700

/* Description		MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_SW_BUFFER_COOKIE
			
			Cookie field exclusively used by SW. 
			
			
			
			In case of 'NULL' pointer, this field is set to 0
			
			
			
			HW ignores the contents, accept that it passes the
			programmed value on to other descriptors together with the
			physical address 
			
			
			
			Field can be used by SW to for example associate the
			buffers physical address with the virtual address
			
			The bit definitions as used by SW are within SW HLD
			specification
			
			
			
			NOTE:
			
			The three most significant bits can have a special
			meaning in case this struct is embedded in a TX_MPDU_DETAILS
			STRUCT, and field transmit_bw_restriction is set
			
			
			
			In case of NON punctured transmission:
			
			Sw_buffer_cookie[20:19] = 2'b00: 20 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b01: 40 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b10: 80 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b11: 160 MHz TX only
			
			
			
			In case of punctured transmission:
			
			Sw_buffer_cookie[20:18] = 3'b000: pattern 0 only
			
			Sw_buffer_cookie[20:18] = 3'b001: pattern 1 only
			
			Sw_buffer_cookie[20:18] = 3'b010: pattern 2 only
			
			Sw_buffer_cookie[20:18] = 3'b011: pattern 3 only
			
			Sw_buffer_cookie[20:18] = 3'b100: pattern 4 only
			
			Sw_buffer_cookie[20:18] = 3'b101: pattern 5 only
			
			Sw_buffer_cookie[20:18] = 3'b110: pattern 6 only
			
			Sw_buffer_cookie[20:18] = 3'b111: pattern 7 only
			
			
			
			Note: a punctured transmission is indicated by the
			presence of TLV TX_PUNCTURE_SETUP embedded in the scheduler
			TLV
			
			
			
			<legal all>
*/
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_SW_BUFFER_COOKIE_OFFSET 0x00000004
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_SW_BUFFER_COOKIE_LSB 11
#define MPDU_INFO_1_MPDU_0_MSDU_LINK_DESC_ADDR_INFO_SW_BUFFER_COOKIE_MASK 0xfffff800

/* Description		MPDU_INFO_2_MPDU_0_MSDU_LINK_DESC_INDEX
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			MSDU Index within MSDU link descriptor. Indicates which
			MSDU in the MSDU link descriptor is the first MSDU of this
			MPDU
			
			<legal all>
*/
#define MPDU_INFO_2_MPDU_0_MSDU_LINK_DESC_INDEX_OFFSET               0x00000008
#define MPDU_INFO_2_MPDU_0_MSDU_LINK_DESC_INDEX_LSB                  0
#define MPDU_INFO_2_MPDU_0_MSDU_LINK_DESC_INDEX_MASK                 0x00000007

/* Description		MPDU_INFO_2_MPDU_0_MPDU_TYPE
			
			Indicates the type of MPDU that OLE will generate:
			
			
			
			<enum 0    mpdu_type_basic> This MPDU is not in the
			A-MSDU format (meaning there is no A-MSDU delimeter present)
			if there is only 1 MSDU in the MPDU. When there are multiple
			MSDUs in the MPDU, there is no choice, and the MSDUs within
			the MPDU shall all have A-MSDU delimiters in front of them.
			
			<enum 1    mpdu_type_amsdu> The MSDUs within the MPDU
			will all have to be in the A-MSDU format, even if there is
			just a single MSDU embedded in the MPDU. In other words,
			there is always an A-MSDU delimiter in front of the MSDU(s)
			in the MPDU.
			
			<legal all>
*/
#define MPDU_INFO_2_MPDU_0_MPDU_TYPE_OFFSET                          0x00000008
#define MPDU_INFO_2_MPDU_0_MPDU_TYPE_LSB                             3
#define MPDU_INFO_2_MPDU_0_MPDU_TYPE_MASK                            0x00000008

/* Description		MPDU_INFO_2_MPDU_0_MPDU_LENGTH
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			Expected Length of the entire MPDU, which includes all
			MSDUs within the MPDU and all OLE and Crypto processing.
			This length includes the FCS field.
*/
#define MPDU_INFO_2_MPDU_0_MPDU_LENGTH_OFFSET                        0x00000008
#define MPDU_INFO_2_MPDU_0_MPDU_LENGTH_LSB                           4
#define MPDU_INFO_2_MPDU_0_MPDU_LENGTH_MASK                          0x0003fff0

/* Description		MPDU_INFO_2_MPDU_0_MPDU_SEQUENCE_NUMBER
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			Sequence number assigned to this MPDU
			
			<legal all>
*/
#define MPDU_INFO_2_MPDU_0_MPDU_SEQUENCE_NUMBER_OFFSET               0x00000008
#define MPDU_INFO_2_MPDU_0_MPDU_SEQUENCE_NUMBER_LSB                  18
#define MPDU_INFO_2_MPDU_0_MPDU_SEQUENCE_NUMBER_MASK                 0x3ffc0000

/* Description		MPDU_INFO_2_MPDU_0_TRANSMIT_BW_RESTRICTION
			
			This bit is only evaluated by TXPCU
			
			
			
			1'b0: This is a normal frame and there are no
			restrictions on the BW that this frame can be transmitted
			on.
			
			
			
			1'b1: This MPDU is only allowed to be transmitted at
			certain BWs
			
			The allowed BWs are indicated in field:
			
			msdu_link_desc_addr_info.Sw_buffer_cookie[20:19], which
			corresponds to the three most significant bits in the
			Sw_buffer_cookie field.
			
			
			
			In case of NON punctured transmission:
			
			Sw_buffer_cookie[20:19] = 2'b00: 20 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b01: 40 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b10: 80 MHz TX only
			
			Sw_buffer_cookie[20:19] = 2'b11: 160 MHz TX only
			
			
			
			In case of punctured transmission:
			
			Sw_buffer_cookie[20:18] = 3'b000: pattern 0 only
			
			Sw_buffer_cookie[20:18] = 3'b001: pattern 1 only
			
			Sw_buffer_cookie[20:18] = 3'b010: pattern 2 only
			
			Sw_buffer_cookie[20:18] = 3'b011: pattern 3 only
			
			Sw_buffer_cookie[20:18] = 3'b100: pattern 4 only
			
			Sw_buffer_cookie[20:18] = 3'b101: pattern 5 only
			
			Sw_buffer_cookie[20:18] = 3'b110: pattern 6 only
			
			Sw_buffer_cookie[20:18] = 3'b111: pattern 7 only
			
			
			
			Note: a punctured transmission is indicated by the
			presence of TLV TX_PUNCTURE_SETUP embedded in the scheduler
			TLV
			
			
			
			<legal all>
*/
#define MPDU_INFO_2_MPDU_0_TRANSMIT_BW_RESTRICTION_OFFSET            0x00000008
#define MPDU_INFO_2_MPDU_0_TRANSMIT_BW_RESTRICTION_LSB               30
#define MPDU_INFO_2_MPDU_0_TRANSMIT_BW_RESTRICTION_MASK              0x40000000

/* Description		MPDU_INFO_2_MPDU_0_RAW_ALREADY_ENCRYPTED
			
			TQM sets this field equal to the 'raw_already_encrypted'
			field of the first TX_MSDU_DETAILS STRUCT when the encap
			type is 'RAW'.
			
			When the TX_MSDU_DETAILS.encap_type does NOT indicate a
			RAW frame, this field is always set to 0
			
			
			
			If set it indicates that the RAW MPDU has already been
			encrypted and does not require HW encryption.  
			
			
			
			If clear and if the frame control indicates that this
			frame is a protected MPDU and the peer key type indicates a
			cipher type then the HW is expected to encrypt this packet.
			
			<legal all> 
*/
#define MPDU_INFO_2_MPDU_0_RAW_ALREADY_ENCRYPTED_OFFSET              0x00000008
#define MPDU_INFO_2_MPDU_0_RAW_ALREADY_ENCRYPTED_LSB                 31
#define MPDU_INFO_2_MPDU_0_RAW_ALREADY_ENCRYPTED_MASK                0x80000000

/* Description		MPDU_INFO_3_MPDU_0_PN_31_0
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			Bits 31 - 0 for the Packet Number used by encryption
			
			<legal all>
*/
#define MPDU_INFO_3_MPDU_0_PN_31_0_OFFSET                            0x0000000c
#define MPDU_INFO_3_MPDU_0_PN_31_0_LSB                               0
#define MPDU_INFO_3_MPDU_0_PN_31_0_MASK                              0xffffffff

/* Description		MPDU_INFO_4_MPDU_0_PN_47_32
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			Bits 47 - 32 for the Packet Number used by encryption
			
			<legal all>
*/
#define MPDU_INFO_4_MPDU_0_PN_47_32_OFFSET                           0x00000010
#define MPDU_INFO_4_MPDU_0_PN_47_32_LSB                              0
#define MPDU_INFO_4_MPDU_0_PN_47_32_MASK                             0x0000ffff

/* Description		MPDU_INFO_4_MPDU_0_MSDU_COUNT
			
			Consumer: TXDMA
			
			Producer: TQM
			
			
			
			Number of msdus in the MPDU
			
			This number should be in sync with the number of MSDU
			linked to each other
			
			For tracking and debugging
			
			<legal all>
*/
#define MPDU_INFO_4_MPDU_0_MSDU_COUNT_OFFSET                         0x00000010
#define MPDU_INFO_4_MPDU_0_MSDU_COUNT_LSB                            16
#define MPDU_INFO_4_MPDU_0_MSDU_COUNT_MASK                           0x00ff0000

/* Description		MPDU_INFO_4_MPDU_0_TRANSMIT_COUNT
			
			Consumer: TQM
			
			Producer: TQM
			
			
			
			The number of times the frame is transmitted
			
			<legal 0>
*/
#define MPDU_INFO_4_MPDU_0_TRANSMIT_COUNT_OFFSET                     0x00000010
#define MPDU_INFO_4_MPDU_0_TRANSMIT_COUNT_LSB                        24
#define MPDU_INFO_4_MPDU_0_TRANSMIT_COUNT_MASK                       0x7f000000

/* Description		MPDU_INFO_4_MPDU_0_FW_TX_NOTIFY_FRAME
			
			When clear, this frame does not require any special
			handling.
			
			
			
			When set, this MPDU contains an MSDU with the
			'FW_tx_notify_frame' field set.
			
			This means this MPDU is a special frame that requires
			
			* When TQM is executing the GEN_MPDU_LENGTH_LIST command
			and this frame is encountered, this frame and all following
			ones shall NOT be included in the MPDU list. This
			functionality is ignored if the 'ignore_tx_notify_setting'
			bit in the list command is set
			
			*When removing MPDUs from the queue, and this frame ends
			up being at the head of the queue, it should be explicitly
			indicated in the remove (acked) command status
			
			<legal all>
*/
#define MPDU_INFO_4_MPDU_0_FW_TX_NOTIFY_FRAME_OFFSET                 0x00000010
#define MPDU_INFO_4_MPDU_0_FW_TX_NOTIFY_FRAME_LSB                    31
#define MPDU_INFO_4_MPDU_0_FW_TX_NOTIFY_FRAME_MASK                   0x80000000


#endif // _MPDU_INFO_H_
