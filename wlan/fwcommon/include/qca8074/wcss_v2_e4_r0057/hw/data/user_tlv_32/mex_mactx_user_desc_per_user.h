// Copyright (c) 2017 Qualcomm Atheros, Inc.  All rights reserved.
// $ATH_LICENSE_HW_HDR_C$
//
// DO NOT EDIT!  This file is automatically generated
//               These definitions are tied to a particular hardware layout


#pragma once

#include "../mex_tlv_intf.h"
#include "mactx_user_desc_per_user.h"

template <typename TlvType>
struct MexParseTlv<TlvType, typename std::enable_if<std::is_base_of<mactx_user_desc_per_user, TlvType>::value>::type>
    : MexParseTlvBase<TlvType> {

  std::string tlv_name{"mactx_user_desc_per_user"};

  void operator()() {
    MEX_PARSE_TLV_FIELD(psdu_length);
    MEX_PARSE_TLV_FIELD(reserved_0a);
    MEX_PARSE_TLV_FIELD(ru_start_index);
    MEX_PARSE_TLV_FIELD(reserved_1a);
    MEX_PARSE_TLV_FIELD(ru_width);
    MEX_PARSE_TLV_FIELD(reserved_1b);
    MEX_PARSE_TLV_FIELD(ofdma_mu_mimo_enabled);
    MEX_PARSE_TLV_FIELD(nss);
    MEX_PARSE_TLV_FIELD(stream_offset);
    MEX_PARSE_TLV_FIELD(reserved_1c);
    MEX_PARSE_TLV_FIELD(mcs);
    MEX_PARSE_TLV_FIELD(dcm);
    MEX_PARSE_TLV_FIELD(reserved_1d);
    MEX_PARSE_TLV_FIELD(fec_type);
    MEX_PARSE_TLV_FIELD(reserved_2a);
    MEX_PARSE_TLV_FIELD(user_bf_type);
    MEX_PARSE_TLV_FIELD(reserved_2b);
    MEX_PARSE_TLV_FIELD(ldpc_extra_symbol);
    MEX_PARSE_TLV_FIELD(reserved_2d);
    MEX_PARSE_TLV_FIELD(sw_peer_id);
    MEX_PARSE_TLV_FIELD(reserved_3a);
  }
};


