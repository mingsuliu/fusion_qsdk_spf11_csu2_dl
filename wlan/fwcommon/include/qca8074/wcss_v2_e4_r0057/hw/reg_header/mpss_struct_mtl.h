//////////////////////////////////////////////////////////////////////////////
// mpss_struct_mtl.h generated by: GenCStruct.pm 
//////////////////////////////////////////////////////////////////////////////
// **** W A R N I N G ****  THIS FILE IS AUTO GENERATED!! PLEASE DO NOT EDIT!!
//////////////////////////////////////////////////////////////////////////////
// QUALCOMM Proprietary
// Copyright Qualcomm Technologies Incorporated.  All rights reserved.
//
// All data and information contained in or disclosed by this document are
// confidential and proprietary information of Qualcomm Technologies Incorporated, and
// all rights therein are expressly reserved. By accepting this material,
// the recipient agrees that this material and the information contained
// therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in
// any manner to others without the express written permission of QUALCOMM
// Incorporated.
//
// This technology was exported from the United States in accordance with
// the Export Administration Regulations. Diversion contrary to U.S. law
// prohibited.
//////////////////////////////////////////////////////////////////////////////
// RCS File        : -USE CVS LOG-
// Revision        : -USE CVS LOG-
// Last Check In   : -USE CVS LOG-
//////////////////////////////////////////////////////////////////////////////
// Description     : Max to Lowest C Struct file
//
//////////////////////////////////////////////////////////////////////////////


#ifndef MPSS_STRUCT_MTL_H
#define MPSS_STRUCT_MTL_H

#ifndef UINT32_TYPE
#define UINT32_TYPE unsigned int
#endif


#ifndef LONG_TYPE
#define LONG_TYPE long int
#endif

typedef struct _mpss_top_seg0pdmem_pdmem_l_n_t {
     UINT32_TYPE pdmem_low : 32;
} mpss_top_seg0pdmem_pdmem_l_n_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0pdmem_pdmem_l_n_t f;
} mpss_top_seg0pdmem_pdmem_l_n_u;

typedef struct _mpss_top_seg0pdmem_pdmem_u_n_t {
     UINT32_TYPE pdmem_high : 32;
} mpss_top_seg0pdmem_pdmem_u_n_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0pdmem_pdmem_u_n_t f;
} mpss_top_seg0pdmem_pdmem_u_n_u;

typedef struct _mpss_top_seg0_mpss_global_config_l_t {
     UINT32_TYPE _unused0                 : 18;
     UINT32_TYPE cf_reset_noc             : 1;
     UINT32_TYPE disable_dyn_clock_gating : 1;
     UINT32_TYPE en_turbo_mode            : 1;
     UINT32_TYPE cf_active                : 1;
     UINT32_TYPE adc_bw                   : 2;
     UINT32_TYPE dac_bw                   : 2;
     UINT32_TYPE en_165                   : 1;
     UINT32_TYPE en_noncontig             : 1;
     UINT32_TYPE dyn_bw                   : 2;
     UINT32_TYPE quarter_rate             : 1;
     UINT32_TYPE half_rate                : 1;
} mpss_top_seg0_mpss_global_config_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_global_config_l_t f;
} mpss_top_seg0_mpss_global_config_l_u;

typedef struct _mpss_top_seg0_mpss_m3_status_l_t {
     UINT32_TYPE _unused0  : 29;
     UINT32_TYPE m3_sts    : 3;
} mpss_top_seg0_mpss_m3_status_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_status_l_t f;
} mpss_top_seg0_mpss_m3_status_l_u;

typedef struct _mpss_top_seg0_mpss_m3_ctrl_l_t {
     UINT32_TYPE _unused0           : 23;
     UINT32_TYPE wd_timer_clk_en    : 1;
     UINT32_TYPE m3_640_en          : 1;
     UINT32_TYPE dbg_clk_en         : 1;
     UINT32_TYPE dap_disable        : 1;
     UINT32_TYPE cross_trig_disable : 1;
     UINT32_TYPE itm_disable        : 1;
     UINT32_TYPE etm_disable        : 1;
     UINT32_TYPE m3_enable          : 1;
     UINT32_TYPE pcss_enable        : 1;
} mpss_top_seg0_mpss_m3_ctrl_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_ctrl_l_t f;
} mpss_top_seg0_mpss_m3_ctrl_l_u;

typedef struct _mpss_top_seg0_mpss_m3_bp_l_t {
     UINT32_TYPE bp_dbg_cmd : 32;
} mpss_top_seg0_mpss_m3_bp_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_bp_l_t f;
} mpss_top_seg0_mpss_m3_bp_l_u;

typedef struct _mpss_top_seg0_mpss_m3_bp_u_t {
     UINT32_TYPE _unused0   : 31;
     UINT32_TYPE breakpoint : 1;
} mpss_top_seg0_mpss_m3_bp_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_bp_u_t f;
} mpss_top_seg0_mpss_m3_bp_u_u;

typedef struct _mpss_top_seg0_mpss_m3_ipcreg_l_t {
     UINT32_TYPE m3_to_q6_addr_ptr : 28;
     UINT32_TYPE m3_to_q6_ipc      : 4;
} mpss_top_seg0_mpss_m3_ipcreg_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_ipcreg_l_t f;
} mpss_top_seg0_mpss_m3_ipcreg_l_u;

typedef struct _mpss_top_seg0_mpss_m3_ipcreg_u_t {
     UINT32_TYPE q6_to_m3_addr_ptr : 28;
     UINT32_TYPE q6_to_m3_ipc      : 4;
} mpss_top_seg0_mpss_m3_ipcreg_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_ipcreg_u_t f;
} mpss_top_seg0_mpss_m3_ipcreg_u_u;

typedef struct _mpss_top_seg0_mpss_m3_m3_ipcreg_l_t {
     UINT32_TYPE m3_to_m3_addr_ptr : 28;
     UINT32_TYPE m3_to_m3_ipc      : 4;
} mpss_top_seg0_mpss_m3_m3_ipcreg_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_m3_ipcreg_l_t f;
} mpss_top_seg0_mpss_m3_m3_ipcreg_l_u;

typedef struct _mpss_top_seg0_mpss_m3_sw_log_l_t {
     UINT32_TYPE sw_log_0  : 32;
} mpss_top_seg0_mpss_m3_sw_log_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_sw_log_l_t f;
} mpss_top_seg0_mpss_m3_sw_log_l_u;

typedef struct _mpss_top_seg0_mpss_m3_sw_log_u_t {
     UINT32_TYPE sw_log_1  : 32;
} mpss_top_seg0_mpss_m3_sw_log_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_m3_sw_log_u_t f;
} mpss_top_seg0_mpss_m3_sw_log_u_u;

typedef struct _mpss_top_seg0_mpss_crc_ctrl_l_t {
     UINT32_TYPE _unused0        : 26;
     UINT32_TYPE crc_init_allone : 1;
     UINT32_TYPE crc_bit_reverse : 1;
     UINT32_TYPE crc_start       : 1;
     UINT32_TYPE crc_8bit        : 1;
     UINT32_TYPE crc_data_size   : 2;
} mpss_top_seg0_mpss_crc_ctrl_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_ctrl_l_t f;
} mpss_top_seg0_mpss_crc_ctrl_l_u;

typedef struct _mpss_top_seg0_mpss_crc_data0_l_t {
     UINT32_TYPE crc_data_0_0 : 32;
} mpss_top_seg0_mpss_crc_data0_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_data0_l_t f;
} mpss_top_seg0_mpss_crc_data0_l_u;

typedef struct _mpss_top_seg0_mpss_crc_data0_u_t {
     UINT32_TYPE crc_data_1_0 : 32;
} mpss_top_seg0_mpss_crc_data0_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_data0_u_t f;
} mpss_top_seg0_mpss_crc_data0_u_u;

typedef struct _mpss_top_seg0_mpss_crc_data1_l_t {
     UINT32_TYPE crc_data_0_1 : 32;
} mpss_top_seg0_mpss_crc_data1_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_data1_l_t f;
} mpss_top_seg0_mpss_crc_data1_l_u;

typedef struct _mpss_top_seg0_mpss_crc_data1_u_t {
     UINT32_TYPE crc_data_1_1 : 32;
} mpss_top_seg0_mpss_crc_data1_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_data1_u_t f;
} mpss_top_seg0_mpss_crc_data1_u_u;

typedef struct _mpss_top_seg0_mpss_crc_result_l_t {
     UINT32_TYPE _unused0   : 24;
     UINT32_TYPE crc_result : 8;
} mpss_top_seg0_mpss_crc_result_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_crc_result_l_t f;
} mpss_top_seg0_mpss_crc_result_l_u;

typedef struct _mpss_top_seg0_mpss_rri_ctrl_0_l_t {
     UINT32_TYPE _unused0           : 31;
     UINT32_TYPE rri_init_done_intr : 1;
} mpss_top_seg0_mpss_rri_ctrl_0_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_rri_ctrl_0_l_t f;
} mpss_top_seg0_mpss_rri_ctrl_0_l_u;

typedef struct _mpss_top_seg0_mpss_rri_ctrl_0_u_t {
     UINT32_TYPE _unused0          : 8;
     UINT32_TYPE rri_reg_save_addr : 24;
} mpss_top_seg0_mpss_rri_ctrl_0_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_rri_ctrl_0_u_t f;
} mpss_top_seg0_mpss_rri_ctrl_0_u_u;

typedef struct _mpss_top_seg0_mpss_rri_ctrl_1_l_t {
     UINT32_TYPE _unused0             : 8;
     UINT32_TYPE rri_reg_restore_addr : 24;
} mpss_top_seg0_mpss_rri_ctrl_1_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_rri_ctrl_1_l_t f;
} mpss_top_seg0_mpss_rri_ctrl_1_l_u;

typedef struct _mpss_top_seg0_mpss_core_reset_l_t {
     UINT32_TYPE _unused0       : 25;
     UINT32_TYPE fft_sync_rst   : 1;
     UINT32_TYPE robe_sync_rst  : 1;
     UINT32_TYPE txfd_sync_rst  : 1;
     UINT32_TYPE demf_sync_rst  : 1;
     UINT32_TYPE txtd_sync_rst  : 1;
     UINT32_TYPE rxtd_sync_rst  : 1;
     UINT32_TYPE phyrf_sync_rst : 1;
} mpss_top_seg0_mpss_core_reset_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_core_reset_l_t f;
} mpss_top_seg0_mpss_core_reset_l_u;

typedef struct _mpss_top_seg0_mpss_phy_ver_reg_l_t {
     UINT32_TYPE _unused0  : 8;
     UINT32_TYPE major     : 8;
     UINT32_TYPE minor     : 8;
     UINT32_TYPE step      : 8;
} mpss_top_seg0_mpss_phy_ver_reg_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_phy_ver_reg_l_t f;
} mpss_top_seg0_mpss_phy_ver_reg_l_u;

typedef struct _mpss_top_seg0_mpss_phy_otp_l_t {
     UINT32_TYPE phy_otp   : 32;
} mpss_top_seg0_mpss_phy_otp_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_phy_otp_l_t f;
} mpss_top_seg0_mpss_phy_otp_l_u;

typedef struct _mpss_top_seg0_mpss_nts_ctrl_l_t {
     UINT32_TYPE _unused0  : 30;
     UINT32_TYPE nts_ack   : 1;
     UINT32_TYPE nts_en    : 1;
} mpss_top_seg0_mpss_nts_ctrl_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_nts_ctrl_l_t f;
} mpss_top_seg0_mpss_nts_ctrl_l_u;

typedef struct _mpss_top_seg0_mpss_timer_wdog_clk_en_l_t {
     UINT32_TYPE _unused0      : 25;
     UINT32_TYPE eco_rev_no    : 4;
     UINT32_TYPE wdog_clk_en   : 1;
     UINT32_TYPE timer2_clk_en : 1;
     UINT32_TYPE timer1_clk_en : 1;
} mpss_top_seg0_mpss_timer_wdog_clk_en_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_timer_wdog_clk_en_l_t f;
} mpss_top_seg0_mpss_timer_wdog_clk_en_l_u;

typedef struct _mpss_top_seg0_mpss_napier_pmm_reg_l_t {
     UINT32_TYPE _unused0        : 30;
     UINT32_TYPE napier_pmm_idle : 1;
     UINT32_TYPE napier_pmm_ack  : 1;
} mpss_top_seg0_mpss_napier_pmm_reg_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_napier_pmm_reg_l_t f;
} mpss_top_seg0_mpss_napier_pmm_reg_l_u;

typedef struct _mpss_top_seg0_mpss_noc_smartmux_mask_l_t {
     UINT32_TYPE _unused0          : 24;
     UINT32_TYPE noc_smartmux_mask : 8;
} mpss_top_seg0_mpss_noc_smartmux_mask_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_noc_smartmux_mask_l_t f;
} mpss_top_seg0_mpss_noc_smartmux_mask_l_u;

typedef struct _mpss_top_seg0_mpss_watchdog_config_reg_l_t {
     UINT32_TYPE watchdog_config_reg_0 : 32;
} mpss_top_seg0_mpss_watchdog_config_reg_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_config_reg_l_t f;
} mpss_top_seg0_mpss_watchdog_config_reg_l_u;

typedef struct _mpss_top_seg0_mpss_watchdog_config_reg_u_t {
     UINT32_TYPE watchdog_config_reg_1 : 32;
} mpss_top_seg0_mpss_watchdog_config_reg_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_config_reg_u_t f;
} mpss_top_seg0_mpss_watchdog_config_reg_u_u;

typedef struct _mpss_top_seg0_mpss_watchdog_status_reg0_l_t {
     UINT32_TYPE watchdog_status_reg_0_0 : 32;
} mpss_top_seg0_mpss_watchdog_status_reg0_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_status_reg0_l_t f;
} mpss_top_seg0_mpss_watchdog_status_reg0_l_u;

typedef struct _mpss_top_seg0_mpss_watchdog_status_reg0_u_t {
     UINT32_TYPE watchdog_status_reg_1_0 : 32;
} mpss_top_seg0_mpss_watchdog_status_reg0_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_status_reg0_u_t f;
} mpss_top_seg0_mpss_watchdog_status_reg0_u_u;

typedef struct _mpss_top_seg0_mpss_watchdog_status_reg1_l_t {
     UINT32_TYPE watchdog_status_reg_0_1 : 32;
} mpss_top_seg0_mpss_watchdog_status_reg1_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_status_reg1_l_t f;
} mpss_top_seg0_mpss_watchdog_status_reg1_l_u;

typedef struct _mpss_top_seg0_mpss_watchdog_status_reg1_u_t {
     UINT32_TYPE watchdog_status_reg_1_1 : 32;
} mpss_top_seg0_mpss_watchdog_status_reg1_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_watchdog_status_reg1_u_t f;
} mpss_top_seg0_mpss_watchdog_status_reg1_u_u;

typedef struct _mpss_top_seg0_mpss_pcss_spare_l_t {
     UINT32_TYPE pcss_spare_0 : 32;
} mpss_top_seg0_mpss_pcss_spare_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_pcss_spare_l_t f;
} mpss_top_seg0_mpss_pcss_spare_l_u;

typedef struct _mpss_top_seg0_mpss_pcss_spare_u_t {
     UINT32_TYPE pcss_spare_1 : 32;
} mpss_top_seg0_mpss_pcss_spare_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_pcss_spare_u_t f;
} mpss_top_seg0_mpss_pcss_spare_u_u;

typedef struct _mpss_top_seg0_mpss_pcss_eco_l_t {
     UINT32_TYPE pcss_eco_0 : 32;
} mpss_top_seg0_mpss_pcss_eco_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_pcss_eco_l_t f;
} mpss_top_seg0_mpss_pcss_eco_l_u;

typedef struct _mpss_top_seg0_mpss_pcss_eco_u_t {
     UINT32_TYPE pcss_eco_1 : 32;
} mpss_top_seg0_mpss_pcss_eco_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_pcss_eco_u_t f;
} mpss_top_seg0_mpss_pcss_eco_u_u;

typedef struct _mpss_top_seg0_mpss_dummy_l_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE dummy     : 1;
} mpss_top_seg0_mpss_dummy_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_mpss_dummy_l_t f;
} mpss_top_seg0_mpss_dummy_l_u;

typedef struct _mpss_top_seg0_timer0_load_value_l_t {
     UINT32_TYPE timer0_timerload : 32;
} mpss_top_seg0_timer0_load_value_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_load_value_l_t f;
} mpss_top_seg0_timer0_load_value_l_u;

typedef struct _mpss_top_seg0_timer0_load_value_u_t {
     UINT32_TYPE timer0_timervalue : 32;
} mpss_top_seg0_timer0_load_value_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_load_value_u_t f;
} mpss_top_seg0_timer0_load_value_u_u;

typedef struct _mpss_top_seg0_timer0_control_clr_l_t {
     UINT32_TYPE _unused0              : 24;
     UINT32_TYPE timer0_timer_en       : 1;
     UINT32_TYPE timer0_timer_mode     : 1;
     UINT32_TYPE timer0_interrupt_en   : 1;
     UINT32_TYPE _unused1              : 1;
     UINT32_TYPE timer0_timer_pre      : 2;
     UINT32_TYPE timer0_timer_size     : 1;
     UINT32_TYPE timer0_one_shot_count : 1;
} mpss_top_seg0_timer0_control_clr_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_control_clr_l_t f;
} mpss_top_seg0_timer0_control_clr_l_u;

typedef struct _mpss_top_seg0_timer0_control_clr_u_t {
     UINT32_TYPE timer0_timerintclr : 32;
} mpss_top_seg0_timer0_control_clr_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_control_clr_u_t f;
} mpss_top_seg0_timer0_control_clr_u_u;

typedef struct _mpss_top_seg0_timer0_intr_status_l_t {
     UINT32_TYPE _unused0        : 31;
     UINT32_TYPE timer0_timerris : 1;
} mpss_top_seg0_timer0_intr_status_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_intr_status_l_t f;
} mpss_top_seg0_timer0_intr_status_l_u;

typedef struct _mpss_top_seg0_timer0_intr_status_u_t {
     UINT32_TYPE _unused0        : 31;
     UINT32_TYPE timer0_timermis : 1;
} mpss_top_seg0_timer0_intr_status_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_intr_status_u_t f;
} mpss_top_seg0_timer0_intr_status_u_u;

typedef struct _mpss_top_seg0_timer0_bg_load_l_t {
     UINT32_TYPE timer0_timerbgload : 32;
} mpss_top_seg0_timer0_bg_load_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer0_bg_load_l_t f;
} mpss_top_seg0_timer0_bg_load_l_u;

typedef struct _mpss_top_seg0_timer1_load_value_l_t {
     UINT32_TYPE timer1_timerload : 32;
} mpss_top_seg0_timer1_load_value_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_load_value_l_t f;
} mpss_top_seg0_timer1_load_value_l_u;

typedef struct _mpss_top_seg0_timer1_load_value_u_t {
     UINT32_TYPE timer1_timervalue : 32;
} mpss_top_seg0_timer1_load_value_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_load_value_u_t f;
} mpss_top_seg0_timer1_load_value_u_u;

typedef struct _mpss_top_seg0_timer1_control_clr_l_t {
     UINT32_TYPE _unused0              : 24;
     UINT32_TYPE timer1_timer_en       : 1;
     UINT32_TYPE timer1_timer_mode     : 1;
     UINT32_TYPE timer1_interrupt_en   : 1;
     UINT32_TYPE _unused1              : 1;
     UINT32_TYPE timer1_timer_pre      : 2;
     UINT32_TYPE timer1_timer_size     : 1;
     UINT32_TYPE timer1_one_shot_count : 1;
} mpss_top_seg0_timer1_control_clr_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_control_clr_l_t f;
} mpss_top_seg0_timer1_control_clr_l_u;

typedef struct _mpss_top_seg0_timer1_control_clr_u_t {
     UINT32_TYPE timer1_timerintclr : 32;
} mpss_top_seg0_timer1_control_clr_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_control_clr_u_t f;
} mpss_top_seg0_timer1_control_clr_u_u;

typedef struct _mpss_top_seg0_timer1_intr_status_l_t {
     UINT32_TYPE _unused0        : 31;
     UINT32_TYPE timer1_timerris : 1;
} mpss_top_seg0_timer1_intr_status_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_intr_status_l_t f;
} mpss_top_seg0_timer1_intr_status_l_u;

typedef struct _mpss_top_seg0_timer1_intr_status_u_t {
     UINT32_TYPE _unused0        : 31;
     UINT32_TYPE timer1_timermis : 1;
} mpss_top_seg0_timer1_intr_status_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_intr_status_u_t f;
} mpss_top_seg0_timer1_intr_status_u_u;

typedef struct _mpss_top_seg0_timer1_bg_load_l_t {
     UINT32_TYPE timer1_timerbgload : 32;
} mpss_top_seg0_timer1_bg_load_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_timer1_bg_load_l_t f;
} mpss_top_seg0_timer1_bg_load_l_u;

typedef struct _mpss_top_seg0_wdogload_l_t {
     UINT32_TYPE wdogload  : 32;
} mpss_top_seg0_wdogload_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_wdogload_l_t f;
} mpss_top_seg0_wdogload_l_u;

typedef struct _mpss_top_seg0_wdogload_u_t {
     UINT32_TYPE wdogvalue : 32;
} mpss_top_seg0_wdogload_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_wdogload_u_t f;
} mpss_top_seg0_wdogload_u_u;

typedef struct _mpss_top_seg0_control_clr_0_l_t {
     UINT32_TYPE _unused0  : 30;
     UINT32_TYPE inten     : 1;
     UINT32_TYPE resen     : 1;
} mpss_top_seg0_control_clr_0_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_control_clr_0_l_t f;
} mpss_top_seg0_control_clr_0_l_u;

typedef struct _mpss_top_seg0_control_clr_0_u_t {
     UINT32_TYPE _unused0   : 31;
     UINT32_TYPE wdogintclr : 1;
} mpss_top_seg0_control_clr_0_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_control_clr_0_u_t f;
} mpss_top_seg0_control_clr_0_u_u;

typedef struct _mpss_top_seg0_control_clr_1_l_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE wdogris   : 1;
} mpss_top_seg0_control_clr_1_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_control_clr_1_l_t f;
} mpss_top_seg0_control_clr_1_l_u;

typedef struct _mpss_top_seg0_control_clr_1_u_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE wdogmis   : 1;
} mpss_top_seg0_control_clr_1_u_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_control_clr_1_u_t f;
} mpss_top_seg0_control_clr_1_u_u;

typedef struct _mpss_top_seg0_control_clr_2_l_t {
     UINT32_TYPE wdoglock  : 32;
} mpss_top_seg0_control_clr_2_l_t;

typedef union {
     UINT32_TYPE val : 32;
     mpss_top_seg0_control_clr_2_l_t f;
} mpss_top_seg0_control_clr_2_l_u;


#endif

