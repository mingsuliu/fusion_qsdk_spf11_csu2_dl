///////////////////////////////////////////////////////////////////////////////////////////////
//
// QUALCOMM Proprietary Design Data
// Copyright (c) 2011, Qualcomm Technologies Incorporated. All rights reserved.
//
// All data and information contained in or disclosed by this document are confidential and
// proprietary information of Qualcomm Technologies Incorporated, and all rights therein are expressly
// reserved. By accepting this material, the recipient agrees that this material and the
// information contained therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in any manner to others
// without the express written permission of Qualcomm Technologies Incorporated.
//
// This technology was exported from the United States in accordance with the Export
// Administration Regulations. Diversion contrary to U. S. law prohibited.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// saw2_seq_hwioreg.h : automatically generated by Autoseq  3.1 5/20/2018 
// User Name:vakkati
//
// !! WARNING !!  DO NOT MANUALLY EDIT THIS FILE.
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __SAW2_SEQ_REG_H__
#define __SAW2_SEQ_REG_H__

#include "seq_hwio.h"
#include "saw2_seq_hwiobase.h"
#ifdef SCALE_INCLUDES
	#include "HALhwio.h"
#else
	#include "msmhwio.h"
#endif


///////////////////////////////////////////////////////////////////////////////////////////////
// Register Data for Block SAW2
///////////////////////////////////////////////////////////////////////////////////////////////

//// Register SAW2_SECURE ////

#define HWIO_SAW2_SECURE_ADDR(x)                                     (x+0x00000000)
#define HWIO_SAW2_SECURE_PHYS(x)                                     (x+0x00000000)
#define HWIO_SAW2_SECURE_RMSK                                        0x00000007
#define HWIO_SAW2_SECURE_SHFT                                                 0
#define HWIO_SAW2_SECURE_IN(x)                                       \
	in_dword_masked ( HWIO_SAW2_SECURE_ADDR(x), HWIO_SAW2_SECURE_RMSK)
#define HWIO_SAW2_SECURE_INM(x, mask)                                \
	in_dword_masked ( HWIO_SAW2_SECURE_ADDR(x), mask) 
#define HWIO_SAW2_SECURE_OUT(x, val)                                 \
	out_dword( HWIO_SAW2_SECURE_ADDR(x), val)
#define HWIO_SAW2_SECURE_OUTM(x, mask, val)                          \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SECURE_ADDR(x), mask, val, HWIO_SAW2_SECURE_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SECURE_SAW_CTL_BMSK                                0x00000004
#define HWIO_SAW2_SECURE_SAW_CTL_SHFT                                       0x2
#define HWIO_SAW2_SECURE_SAW_CTL_SEC_FVAL                                  0x0u
#define HWIO_SAW2_SECURE_SAW_CTL_NSEC_FVAL                                 0x1u

#define HWIO_SAW2_SECURE_PWR_CTL_BMSK                                0x00000002
#define HWIO_SAW2_SECURE_PWR_CTL_SHFT                                       0x1
#define HWIO_SAW2_SECURE_PWR_CTL_SEC_FVAL                                  0x0u
#define HWIO_SAW2_SECURE_PWR_CTL_NSEC_FVAL                                 0x1u

#define HWIO_SAW2_SECURE_VLT_CTL_BMSK                                0x00000001
#define HWIO_SAW2_SECURE_VLT_CTL_SHFT                                       0x0
#define HWIO_SAW2_SECURE_VLT_CTL_SEC_FVAL                                  0x0u
#define HWIO_SAW2_SECURE_VLT_CTL_NSEC_FVAL                                 0x1u

//// Register SAW2_ID ////

#define HWIO_SAW2_ID_ADDR(x)                                         (x+0x00000004)
#define HWIO_SAW2_ID_PHYS(x)                                         (x+0x00000004)
#define HWIO_SAW2_ID_RMSK                                            0xff3f1f7f
#define HWIO_SAW2_ID_SHFT                                                     0
#define HWIO_SAW2_ID_IN(x)                                           \
	in_dword_masked ( HWIO_SAW2_ID_ADDR(x), HWIO_SAW2_ID_RMSK)
#define HWIO_SAW2_ID_INM(x, mask)                                    \
	in_dword_masked ( HWIO_SAW2_ID_ADDR(x), mask) 
#define HWIO_SAW2_ID_OUT(x, val)                                     \
	out_dword( HWIO_SAW2_ID_ADDR(x), val)
#define HWIO_SAW2_ID_OUTM(x, mask, val)                              \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_ID_ADDR(x), mask, val, HWIO_SAW2_ID_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_ID_NUM_SPM_ENTRY_BMSK                              0xff000000
#define HWIO_SAW2_ID_NUM_SPM_ENTRY_SHFT                                    0x18

#define HWIO_SAW2_ID_NUM_PWR_CTL_BMSK                                0x003f0000
#define HWIO_SAW2_ID_NUM_PWR_CTL_SHFT                                      0x10

#define HWIO_SAW2_ID_PMIC_DATA_WIDTH_BMSK                            0x00001f00
#define HWIO_SAW2_ID_PMIC_DATA_WIDTH_SHFT                                   0x8

#define HWIO_SAW2_ID_NUM_PMIC_DATA_BMSK                              0x00000070
#define HWIO_SAW2_ID_NUM_PMIC_DATA_SHFT                                     0x4

#define HWIO_SAW2_ID_CFG_NS_ACCESS_BMSK                              0x00000008
#define HWIO_SAW2_ID_CFG_NS_ACCESS_SHFT                                     0x3

#define HWIO_SAW2_ID_PMIC_ARB_INTF_BMSK                              0x00000004
#define HWIO_SAW2_ID_PMIC_ARB_INTF_SHFT                                     0x2

#define HWIO_SAW2_ID_AVS_PRESENT_BMSK                                0x00000002
#define HWIO_SAW2_ID_AVS_PRESENT_SHFT                                       0x1

#define HWIO_SAW2_ID_SPM_PRESENT_BMSK                                0x00000001
#define HWIO_SAW2_ID_SPM_PRESENT_SHFT                                       0x0

//// Register SAW2_CFG ////

#define HWIO_SAW2_CFG_ADDR(x)                                        (x+0x00000008)
#define HWIO_SAW2_CFG_PHYS(x)                                        (x+0x00000008)
#define HWIO_SAW2_CFG_RMSK                                           0x0000031f
#define HWIO_SAW2_CFG_SHFT                                                    0
#define HWIO_SAW2_CFG_IN(x)                                          \
	in_dword_masked ( HWIO_SAW2_CFG_ADDR(x), HWIO_SAW2_CFG_RMSK)
#define HWIO_SAW2_CFG_INM(x, mask)                                   \
	in_dword_masked ( HWIO_SAW2_CFG_ADDR(x), mask) 
#define HWIO_SAW2_CFG_OUT(x, val)                                    \
	out_dword( HWIO_SAW2_CFG_ADDR(x), val)
#define HWIO_SAW2_CFG_OUTM(x, mask, val)                             \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_CFG_ADDR(x), mask, val, HWIO_SAW2_CFG_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_CFG_TEST_BUS_EN_BMSK                               0x00000200
#define HWIO_SAW2_CFG_TEST_BUS_EN_SHFT                                      0x9

#define HWIO_SAW2_CFG_FRC_REF_CLK_ON_BMSK                            0x00000100
#define HWIO_SAW2_CFG_FRC_REF_CLK_ON_SHFT                                   0x8

#define HWIO_SAW2_CFG_CLK_DIV_BMSK                                   0x0000001f
#define HWIO_SAW2_CFG_CLK_DIV_SHFT                                          0x0

//// Register SAW2_SPM_STS ////

#define HWIO_SAW2_SPM_STS_ADDR(x)                                    (x+0x0000000c)
#define HWIO_SAW2_SPM_STS_PHYS(x)                                    (x+0x0000000c)
#define HWIO_SAW2_SPM_STS_RMSK                                       0xfffffdff
#define HWIO_SAW2_SPM_STS_SHFT                                                0
#define HWIO_SAW2_SPM_STS_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_SPM_STS_ADDR(x), HWIO_SAW2_SPM_STS_RMSK)
#define HWIO_SAW2_SPM_STS_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_SPM_STS_ADDR(x), mask) 
#define HWIO_SAW2_SPM_STS_OUT(x, val)                                \
	out_dword( HWIO_SAW2_SPM_STS_ADDR(x), val)
#define HWIO_SAW2_SPM_STS_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_STS_ADDR(x), mask, val, HWIO_SAW2_SPM_STS_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_STS_CURR_PWR_CTL_BMSK                          0xffff0000
#define HWIO_SAW2_SPM_STS_CURR_PWR_CTL_SHFT                                0x10

#define HWIO_SAW2_SPM_STS_SHTDWN_REQ_BMSK                            0x00008000
#define HWIO_SAW2_SPM_STS_SHTDWN_REQ_SHFT                                   0xf

#define HWIO_SAW2_SPM_STS_SHTDWN_ACK_BMSK                            0x00004000
#define HWIO_SAW2_SPM_STS_SHTDWN_ACK_SHFT                                   0xe

#define HWIO_SAW2_SPM_STS_BRNGUP_REQ_BMSK                            0x00002000
#define HWIO_SAW2_SPM_STS_BRNGUP_REQ_SHFT                                   0xd

#define HWIO_SAW2_SPM_STS_BRNGUP_ACK_BMSK                            0x00001000
#define HWIO_SAW2_SPM_STS_BRNGUP_ACK_SHFT                                   0xc

#define HWIO_SAW2_SPM_STS_RPM_STATE_BMSK                             0x00000c00
#define HWIO_SAW2_SPM_STS_RPM_STATE_SHFT                                    0xa
#define HWIO_SAW2_SPM_STS_RPM_STATE_RUN_FVAL                               0x0u
#define HWIO_SAW2_SPM_STS_RPM_STATE_STDNACK_FVAL                           0x1u
#define HWIO_SAW2_SPM_STS_RPM_STATE_BGUPACK_FVAL                           0x2u
#define HWIO_SAW2_SPM_STS_RPM_STATE_WAKEUP_FVAL                            0x3u

#define HWIO_SAW2_SPM_STS_SPM_CMD_ADDR_BMSK                          0x000001ff
#define HWIO_SAW2_SPM_STS_SPM_CMD_ADDR_SHFT                                 0x0

//// Register SAW2_AVS_STS ////

#define HWIO_SAW2_AVS_STS_ADDR(x)                                    (x+0x00000010)
#define HWIO_SAW2_AVS_STS_PHYS(x)                                    (x+0x00000010)
#define HWIO_SAW2_AVS_STS_RMSK                                       0x00ff007f
#define HWIO_SAW2_AVS_STS_SHFT                                                0
#define HWIO_SAW2_AVS_STS_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_AVS_STS_ADDR(x), HWIO_SAW2_AVS_STS_RMSK)
#define HWIO_SAW2_AVS_STS_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_AVS_STS_ADDR(x), mask) 
#define HWIO_SAW2_AVS_STS_OUT(x, val)                                \
	out_dword( HWIO_SAW2_AVS_STS_ADDR(x), val)
#define HWIO_SAW2_AVS_STS_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_AVS_STS_ADDR(x), mask, val, HWIO_SAW2_AVS_STS_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_AVS_STS_SPM_EVENT_BMSK                             0x00ff0000
#define HWIO_SAW2_AVS_STS_SPM_EVENT_SHFT                                   0x10

#define HWIO_SAW2_AVS_STS_AVS_FSM_STATE_BMSK                         0x00000040
#define HWIO_SAW2_AVS_STS_AVS_FSM_STATE_SHFT                                0x6

#define HWIO_SAW2_AVS_STS_MAX_INT_BMSK                               0x00000020
#define HWIO_SAW2_AVS_STS_MAX_INT_SHFT                                      0x5

#define HWIO_SAW2_AVS_STS_MIN_INT_BMSK                               0x00000010
#define HWIO_SAW2_AVS_STS_MIN_INT_SHFT                                      0x4

#define HWIO_SAW2_AVS_STS_CPU_UP_BMSK                                0x00000008
#define HWIO_SAW2_AVS_STS_CPU_UP_SHFT                                       0x3

#define HWIO_SAW2_AVS_STS_CPU_DN_BMSK                                0x00000004
#define HWIO_SAW2_AVS_STS_CPU_DN_SHFT                                       0x2

#define HWIO_SAW2_AVS_STS_SW_WR_PEND_BMSK                            0x00000002
#define HWIO_SAW2_AVS_STS_SW_WR_PEND_SHFT                                   0x1

#define HWIO_SAW2_AVS_STS_AVS_STATE_BMSK                             0x00000001
#define HWIO_SAW2_AVS_STS_AVS_STATE_SHFT                                    0x0
#define HWIO_SAW2_AVS_STS_AVS_STATE_IDLE_FVAL                              0x0u
#define HWIO_SAW2_AVS_STS_AVS_STATE_REQ_FVAL                               0x1u

//// Register SAW2_PMIC_STS ////

#define HWIO_SAW2_PMIC_STS_ADDR(x)                                   (x+0x00000014)
#define HWIO_SAW2_PMIC_STS_PHYS(x)                                   (x+0x00000014)
#define HWIO_SAW2_PMIC_STS_RMSK                                      0x3ff700ff
#define HWIO_SAW2_PMIC_STS_SHFT                                               0
#define HWIO_SAW2_PMIC_STS_IN(x)                                     \
	in_dword_masked ( HWIO_SAW2_PMIC_STS_ADDR(x), HWIO_SAW2_PMIC_STS_RMSK)
#define HWIO_SAW2_PMIC_STS_INM(x, mask)                              \
	in_dword_masked ( HWIO_SAW2_PMIC_STS_ADDR(x), mask) 
#define HWIO_SAW2_PMIC_STS_OUT(x, val)                               \
	out_dword( HWIO_SAW2_PMIC_STS_ADDR(x), val)
#define HWIO_SAW2_PMIC_STS_OUTM(x, mask, val)                        \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_PMIC_STS_ADDR(x), mask, val, HWIO_SAW2_PMIC_STS_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_PMIC_STS_CURR_DLY_BMSK                             0x3ff00000
#define HWIO_SAW2_PMIC_STS_CURR_DLY_SHFT                                   0x14

#define HWIO_SAW2_PMIC_STS_CURR_PMIC_SIZE_BMSK                       0x00040000
#define HWIO_SAW2_PMIC_STS_CURR_PMIC_SIZE_SHFT                             0x12

#define HWIO_SAW2_PMIC_STS_PMIC_STATE_BMSK                           0x00030000
#define HWIO_SAW2_PMIC_STS_PMIC_STATE_SHFT                                 0x10
#define HWIO_SAW2_PMIC_STS_PMIC_STATE_IDLE_FVAL                            0x0u
#define HWIO_SAW2_PMIC_STS_PMIC_STATE_DONE_FVAL                            0x1u
#define HWIO_SAW2_PMIC_STS_PMIC_STATE_DELAY_FVAL                           0x2u
#define HWIO_SAW2_PMIC_STS_PMIC_STATE_DONEBAR_FVAL                         0x3u

#define HWIO_SAW2_PMIC_STS_CURR_PMIC_DATA_BMSK                       0x000000ff
#define HWIO_SAW2_PMIC_STS_CURR_PMIC_DATA_SHFT                              0x0

//// Register SAW2_RST ////

#define HWIO_SAW2_RST_ADDR(x)                                        (x+0x00000018)
#define HWIO_SAW2_RST_PHYS(x)                                        (x+0x00000018)
#define HWIO_SAW2_RST_RMSK                                           0x00000001
#define HWIO_SAW2_RST_SHFT                                                    0
#define HWIO_SAW2_RST_IN(x)                                          \
	in_dword_masked ( HWIO_SAW2_RST_ADDR(x), HWIO_SAW2_RST_RMSK)
#define HWIO_SAW2_RST_INM(x, mask)                                   \
	in_dword_masked ( HWIO_SAW2_RST_ADDR(x), mask) 
#define HWIO_SAW2_RST_OUT(x, val)                                    \
	out_dword( HWIO_SAW2_RST_ADDR(x), val)
#define HWIO_SAW2_RST_OUTM(x, mask, val)                             \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_RST_ADDR(x), mask, val, HWIO_SAW2_RST_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_RST_RST_BMSK                                       0x00000001
#define HWIO_SAW2_RST_RST_SHFT                                              0x0

//// Register SAW2_VCTL ////

#define HWIO_SAW2_VCTL_ADDR(x)                                       (x+0x0000001c)
#define HWIO_SAW2_VCTL_PHYS(x)                                       (x+0x0000001c)
#define HWIO_SAW2_VCTL_RMSK                                          0x001700ff
#define HWIO_SAW2_VCTL_SHFT                                                   0
#define HWIO_SAW2_VCTL_IN(x)                                         \
	in_dword_masked ( HWIO_SAW2_VCTL_ADDR(x), HWIO_SAW2_VCTL_RMSK)
#define HWIO_SAW2_VCTL_INM(x, mask)                                  \
	in_dword_masked ( HWIO_SAW2_VCTL_ADDR(x), mask) 
#define HWIO_SAW2_VCTL_OUT(x, val)                                   \
	out_dword( HWIO_SAW2_VCTL_ADDR(x), val)
#define HWIO_SAW2_VCTL_OUTM(x, mask, val)                            \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_VCTL_ADDR(x), mask, val, HWIO_SAW2_VCTL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_VCTL_SIZE_BMSK                                     0x00100000
#define HWIO_SAW2_VCTL_SIZE_SHFT                                           0x14

#define HWIO_SAW2_VCTL_ADR_IDX_BMSK                                  0x00070000
#define HWIO_SAW2_VCTL_ADR_IDX_SHFT                                        0x10

#define HWIO_SAW2_VCTL_PMIC_DATA_BMSK                                0x000000ff
#define HWIO_SAW2_VCTL_PMIC_DATA_SHFT                                       0x0

//// Register SAW2_AVS_CTL ////

#define HWIO_SAW2_AVS_CTL_ADDR(x)                                    (x+0x00000020)
#define HWIO_SAW2_AVS_CTL_PHYS(x)                                    (x+0x00000020)
#define HWIO_SAW2_AVS_CTL_RMSK                                       0x0000130f
#define HWIO_SAW2_AVS_CTL_SHFT                                                0
#define HWIO_SAW2_AVS_CTL_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_AVS_CTL_ADDR(x), HWIO_SAW2_AVS_CTL_RMSK)
#define HWIO_SAW2_AVS_CTL_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_AVS_CTL_ADDR(x), mask) 
#define HWIO_SAW2_AVS_CTL_OUT(x, val)                                \
	out_dword( HWIO_SAW2_AVS_CTL_ADDR(x), val)
#define HWIO_SAW2_AVS_CTL_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_AVS_CTL_ADDR(x), mask, val, HWIO_SAW2_AVS_CTL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_AVS_CTL_VLVL_WIDTH_BMSK                            0x00001000
#define HWIO_SAW2_AVS_CTL_VLVL_WIDTH_SHFT                                   0xc
#define HWIO_SAW2_AVS_CTL_VLVL_WIDTH_ENUM_SIX_BITS_VLVL_FVAL               0x0u
#define HWIO_SAW2_AVS_CTL_VLVL_WIDTH_ENUM_EIGHT_BITS_VLVL_FVAL             0x1u

#define HWIO_SAW2_AVS_CTL_VLVL_STEP_BMSK                             0x00000300
#define HWIO_SAW2_AVS_CTL_VLVL_STEP_SHFT                                    0x8

#define HWIO_SAW2_AVS_CTL_SW_DONE_INT_EN_BMSK                        0x00000008
#define HWIO_SAW2_AVS_CTL_SW_DONE_INT_EN_SHFT                               0x3

#define HWIO_SAW2_AVS_CTL_MAX_INT_EN_BMSK                            0x00000004
#define HWIO_SAW2_AVS_CTL_MAX_INT_EN_SHFT                                   0x2

#define HWIO_SAW2_AVS_CTL_MIN_INT_EN_BMSK                            0x00000002
#define HWIO_SAW2_AVS_CTL_MIN_INT_EN_SHFT                                   0x1

#define HWIO_SAW2_AVS_CTL_EN_BMSK                                    0x00000001
#define HWIO_SAW2_AVS_CTL_EN_SHFT                                           0x0
#define HWIO_SAW2_AVS_CTL_EN_DISABLE_AVS_FVAL                              0x0u
#define HWIO_SAW2_AVS_CTL_EN_ENABLE_AVS_FVAL                               0x1u

//// Register SAW2_AVS_LIMIT ////

#define HWIO_SAW2_AVS_LIMIT_ADDR(x)                                  (x+0x00000024)
#define HWIO_SAW2_AVS_LIMIT_PHYS(x)                                  (x+0x00000024)
#define HWIO_SAW2_AVS_LIMIT_RMSK                                     0x00ff00ff
#define HWIO_SAW2_AVS_LIMIT_SHFT                                              0
#define HWIO_SAW2_AVS_LIMIT_IN(x)                                    \
	in_dword_masked ( HWIO_SAW2_AVS_LIMIT_ADDR(x), HWIO_SAW2_AVS_LIMIT_RMSK)
#define HWIO_SAW2_AVS_LIMIT_INM(x, mask)                             \
	in_dword_masked ( HWIO_SAW2_AVS_LIMIT_ADDR(x), mask) 
#define HWIO_SAW2_AVS_LIMIT_OUT(x, val)                              \
	out_dword( HWIO_SAW2_AVS_LIMIT_ADDR(x), val)
#define HWIO_SAW2_AVS_LIMIT_OUTM(x, mask, val)                       \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_AVS_LIMIT_ADDR(x), mask, val, HWIO_SAW2_AVS_LIMIT_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_AVS_LIMIT_MAX_VLVL_BMSK                            0x00ff0000
#define HWIO_SAW2_AVS_LIMIT_MAX_VLVL_SHFT                                  0x10

#define HWIO_SAW2_AVS_LIMIT_MIN_VLVL_BMSK                            0x000000ff
#define HWIO_SAW2_AVS_LIMIT_MIN_VLVL_SHFT                                   0x0

//// Register SAW2_AVS_DLY ////

#define HWIO_SAW2_AVS_DLY_ADDR(x)                                    (x+0x00000028)
#define HWIO_SAW2_AVS_DLY_PHYS(x)                                    (x+0x00000028)
#define HWIO_SAW2_AVS_DLY_RMSK                                       0x000003ff
#define HWIO_SAW2_AVS_DLY_SHFT                                                0
#define HWIO_SAW2_AVS_DLY_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_AVS_DLY_ADDR(x), HWIO_SAW2_AVS_DLY_RMSK)
#define HWIO_SAW2_AVS_DLY_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_AVS_DLY_ADDR(x), mask) 
#define HWIO_SAW2_AVS_DLY_OUT(x, val)                                \
	out_dword( HWIO_SAW2_AVS_DLY_ADDR(x), val)
#define HWIO_SAW2_AVS_DLY_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_AVS_DLY_ADDR(x), mask, val, HWIO_SAW2_AVS_DLY_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_AVS_DLY_AVS_DELAY_BMSK                             0x000003ff
#define HWIO_SAW2_AVS_DLY_AVS_DELAY_SHFT                                    0x0

//// Register SAW2_AVS_HYSTERESIS ////

#define HWIO_SAW2_AVS_HYSTERESIS_ADDR(x)                             (x+0x0000002c)
#define HWIO_SAW2_AVS_HYSTERESIS_PHYS(x)                             (x+0x0000002c)
#define HWIO_SAW2_AVS_HYSTERESIS_RMSK                                0x00ff00ff
#define HWIO_SAW2_AVS_HYSTERESIS_SHFT                                         0
#define HWIO_SAW2_AVS_HYSTERESIS_IN(x)                               \
	in_dword_masked ( HWIO_SAW2_AVS_HYSTERESIS_ADDR(x), HWIO_SAW2_AVS_HYSTERESIS_RMSK)
#define HWIO_SAW2_AVS_HYSTERESIS_INM(x, mask)                        \
	in_dword_masked ( HWIO_SAW2_AVS_HYSTERESIS_ADDR(x), mask) 
#define HWIO_SAW2_AVS_HYSTERESIS_OUT(x, val)                         \
	out_dword( HWIO_SAW2_AVS_HYSTERESIS_ADDR(x), val)
#define HWIO_SAW2_AVS_HYSTERESIS_OUTM(x, mask, val)                  \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_AVS_HYSTERESIS_ADDR(x), mask, val, HWIO_SAW2_AVS_HYSTERESIS_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_AVS_HYSTERESIS_DN_COUNT_BMSK                       0x00ff0000
#define HWIO_SAW2_AVS_HYSTERESIS_DN_COUNT_SHFT                             0x10

#define HWIO_SAW2_AVS_HYSTERESIS_UP_COUNT_BMSK                       0x000000ff
#define HWIO_SAW2_AVS_HYSTERESIS_UP_COUNT_SHFT                              0x0

//// Register SAW2_SPM_CTL ////

#define HWIO_SAW2_SPM_CTL_ADDR(x)                                    (x+0x00000030)
#define HWIO_SAW2_SPM_CTL_PHYS(x)                                    (x+0x00000030)
#define HWIO_SAW2_SPM_CTL_RMSK                                       0xff031fff
#define HWIO_SAW2_SPM_CTL_SHFT                                                0
#define HWIO_SAW2_SPM_CTL_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_SPM_CTL_ADDR(x), HWIO_SAW2_SPM_CTL_RMSK)
#define HWIO_SAW2_SPM_CTL_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_SPM_CTL_ADDR(x), mask) 
#define HWIO_SAW2_SPM_CTL_OUT(x, val)                                \
	out_dword( HWIO_SAW2_SPM_CTL_ADDR(x), val)
#define HWIO_SAW2_SPM_CTL_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_CTL_ADDR(x), mask, val, HWIO_SAW2_SPM_CTL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_CTL_EVENT_SYNC_BMSK                            0xff000000
#define HWIO_SAW2_SPM_CTL_EVENT_SYNC_SHFT                                  0x18

#define HWIO_SAW2_SPM_CTL_SLP_CMD_MODE_BMSK                          0x00020000
#define HWIO_SAW2_SPM_CTL_SLP_CMD_MODE_SHFT                                0x11

#define HWIO_SAW2_SPM_CTL_SPM_SYS_PC_MODE_BMSK                       0x00010000
#define HWIO_SAW2_SPM_CTL_SPM_SYS_PC_MODE_SHFT                             0x10

#define HWIO_SAW2_SPM_CTL_SPM_START_ADR_BMSK                         0x00001ff0
#define HWIO_SAW2_SPM_CTL_SPM_START_ADR_SHFT                                0x4

#define HWIO_SAW2_SPM_CTL_ISAR_BMSK                                  0x00000008
#define HWIO_SAW2_SPM_CTL_ISAR_SHFT                                         0x3
#define HWIO_SAW2_SPM_CTL_ISAR_END_OF_PROGRAM_RESET_THE_SPM_START_ADR_TO_ZERO_FVAL       0x0u
#define HWIO_SAW2_SPM_CTL_ISAR_INHIBIT_END_OF_PROGRAM_TO_RESET_SPM_START_ADR_FVAL       0x1u

#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_BMSK                         0x00000006
#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_SHFT                                0x1
#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_SYS_SPM_WAKEUP_FVAL                0x0u
#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_SYS_SPM_WAKEUP_OR_NOT_CPU_SPM_WAIT_REQ_FVAL       0x1u
#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_SYS_SPM_WAKEUP_OR_RISING_EDGE_OF_SYS_SPM_DBG_NOPWRDWN_FVAL       0x2u
#define HWIO_SAW2_SPM_CTL_WAKEUP_CONFIG_SYS_SPM_WAKEUP_OR_NOT_CPU_SPM_WAIT_REQ_OR_RISING_EDGE_OF_SYS_SPM_DBG_NOPWRDWN_FVAL       0x3u

#define HWIO_SAW2_SPM_CTL_SPM_EN_BMSK                                0x00000001
#define HWIO_SAW2_SPM_CTL_SPM_EN_SHFT                                       0x0

//// Register SAW2_SPM_DLY ////

#define HWIO_SAW2_SPM_DLY_ADDR(x)                                    (x+0x00000034)
#define HWIO_SAW2_SPM_DLY_PHYS(x)                                    (x+0x00000034)
#define HWIO_SAW2_SPM_DLY_RMSK                                       0x3fffffff
#define HWIO_SAW2_SPM_DLY_SHFT                                                0
#define HWIO_SAW2_SPM_DLY_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_SPM_DLY_ADDR(x), HWIO_SAW2_SPM_DLY_RMSK)
#define HWIO_SAW2_SPM_DLY_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_SPM_DLY_ADDR(x), mask) 
#define HWIO_SAW2_SPM_DLY_OUT(x, val)                                \
	out_dword( HWIO_SAW2_SPM_DLY_ADDR(x), val)
#define HWIO_SAW2_SPM_DLY_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_DLY_ADDR(x), mask, val, HWIO_SAW2_SPM_DLY_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_DLY_DLY3_BMSK                                  0x3ff00000
#define HWIO_SAW2_SPM_DLY_DLY3_SHFT                                        0x14

#define HWIO_SAW2_SPM_DLY_DLY2_BMSK                                  0x000ffc00
#define HWIO_SAW2_SPM_DLY_DLY2_SHFT                                         0xa

#define HWIO_SAW2_SPM_DLY_DLY1_BMSK                                  0x000003ff
#define HWIO_SAW2_SPM_DLY_DLY1_SHFT                                         0x0

//// Register SAW2_SPM_STS2 ////

#define HWIO_SAW2_SPM_STS2_ADDR(x)                                   (x+0x00000038)
#define HWIO_SAW2_SPM_STS2_PHYS(x)                                   (x+0x00000038)
#define HWIO_SAW2_SPM_STS2_RMSK                                      0x0000000f
#define HWIO_SAW2_SPM_STS2_SHFT                                               0
#define HWIO_SAW2_SPM_STS2_IN(x)                                     \
	in_dword_masked ( HWIO_SAW2_SPM_STS2_ADDR(x), HWIO_SAW2_SPM_STS2_RMSK)
#define HWIO_SAW2_SPM_STS2_INM(x, mask)                              \
	in_dword_masked ( HWIO_SAW2_SPM_STS2_ADDR(x), mask) 
#define HWIO_SAW2_SPM_STS2_OUT(x, val)                               \
	out_dword( HWIO_SAW2_SPM_STS2_ADDR(x), val)
#define HWIO_SAW2_SPM_STS2_OUTM(x, mask, val)                        \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_STS2_ADDR(x), mask, val, HWIO_SAW2_SPM_STS2_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_STS2_CURR_PWR_CTL_BMSK                         0x0000000f
#define HWIO_SAW2_SPM_STS2_CURR_PWR_CTL_SHFT                                0x0

//// Register SAW2_SPM_SLP_SEQ_ENTRY_n ////

#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_ADDR(base, n)                  (base+0x400+0x4*n)
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_PHYS(base, n)                  (base+0x400+0x4*n)
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_RMSK                           0x3f3f3f3f
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_SHFT                                    0
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_MAXn                                    7
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_INI(base, n)                   \
	in_dword_masked ( HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_ADDR(base, n), HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_RMSK)
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_INMI(base, n, mask)            \
	in_dword_masked ( HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_ADDR(base, n), mask) 
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_OUTI(base, n, val)             \
	out_dword( HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_ADDR(base, n), val)
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_OUTMI(base, n, mask, val)      \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_ADDR(base, n), mask, val, HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_INI(base, n)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD3_BMSK                      0x3f000000
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD3_SHFT                            0x18

#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD2_BMSK                      0x003f0000
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD2_SHFT                            0x10

#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD1_BMSK                      0x00003f00
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD1_SHFT                             0x8

#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD0_BMSK                      0x0000003f
#define HWIO_SAW2_SPM_SLP_SEQ_ENTRY_n_CMD0_SHFT                             0x0

//// Register SAW2_SPM_PMIC_DATA_n ////

#define HWIO_SAW2_SPM_PMIC_DATA_n_ADDR(base, n)                      (base+0x40+0x4*n)
#define HWIO_SAW2_SPM_PMIC_DATA_n_PHYS(base, n)                      (base+0x40+0x4*n)
#define HWIO_SAW2_SPM_PMIC_DATA_n_RMSK                               0x071700ff
#define HWIO_SAW2_SPM_PMIC_DATA_n_SHFT                                        0
#define HWIO_SAW2_SPM_PMIC_DATA_n_MAXn                                        0
#define HWIO_SAW2_SPM_PMIC_DATA_n_INI(base, n)                       \
	in_dword_masked ( HWIO_SAW2_SPM_PMIC_DATA_n_ADDR(base, n), HWIO_SAW2_SPM_PMIC_DATA_n_RMSK)
#define HWIO_SAW2_SPM_PMIC_DATA_n_INMI(base, n, mask)                \
	in_dword_masked ( HWIO_SAW2_SPM_PMIC_DATA_n_ADDR(base, n), mask) 
#define HWIO_SAW2_SPM_PMIC_DATA_n_OUTI(base, n, val)                 \
	out_dword( HWIO_SAW2_SPM_PMIC_DATA_n_ADDR(base, n), val)
#define HWIO_SAW2_SPM_PMIC_DATA_n_OUTMI(base, n, mask, val)          \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_SPM_PMIC_DATA_n_ADDR(base, n), mask, val, HWIO_SAW2_SPM_PMIC_DATA_n_INI(base, n)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_SPM_PMIC_DATA_n_DLY_BMSK                           0x07000000
#define HWIO_SAW2_SPM_PMIC_DATA_n_DLY_SHFT                                 0x18

#define HWIO_SAW2_SPM_PMIC_DATA_n_SIZE_BMSK                          0x00100000
#define HWIO_SAW2_SPM_PMIC_DATA_n_SIZE_SHFT                                0x14
#define HWIO_SAW2_SPM_PMIC_DATA_n_SIZE_ENUM_ONE_BYTE_FVAL                  0x0u
#define HWIO_SAW2_SPM_PMIC_DATA_n_SIZE_ENUM_TWO_BYTE_FVAL                  0x1u

#define HWIO_SAW2_SPM_PMIC_DATA_n_ADR_IDX_BMSK                       0x00070000
#define HWIO_SAW2_SPM_PMIC_DATA_n_ADR_IDX_SHFT                             0x10

#define HWIO_SAW2_SPM_PMIC_DATA_n_DATA_BMSK                          0x000000ff
#define HWIO_SAW2_SPM_PMIC_DATA_n_DATA_SHFT                                 0x0

//// Register SAW2_VERSION ////

#define HWIO_SAW2_VERSION_ADDR(x)                                    (x+0x00000fd0)
#define HWIO_SAW2_VERSION_PHYS(x)                                    (x+0x00000fd0)
#define HWIO_SAW2_VERSION_RMSK                                       0xffffffff
#define HWIO_SAW2_VERSION_SHFT                                                0
#define HWIO_SAW2_VERSION_IN(x)                                      \
	in_dword_masked ( HWIO_SAW2_VERSION_ADDR(x), HWIO_SAW2_VERSION_RMSK)
#define HWIO_SAW2_VERSION_INM(x, mask)                               \
	in_dword_masked ( HWIO_SAW2_VERSION_ADDR(x), mask) 
#define HWIO_SAW2_VERSION_OUT(x, val)                                \
	out_dword( HWIO_SAW2_VERSION_ADDR(x), val)
#define HWIO_SAW2_VERSION_OUTM(x, mask, val)                         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_SAW2_VERSION_ADDR(x), mask, val, HWIO_SAW2_VERSION_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_SAW2_VERSION_MAJOR_BMSK                                 0xf0000000
#define HWIO_SAW2_VERSION_MAJOR_SHFT                                       0x1c

#define HWIO_SAW2_VERSION_MINOR_BMSK                                 0x0fff0000
#define HWIO_SAW2_VERSION_MINOR_SHFT                                       0x10

#define HWIO_SAW2_VERSION_STEP_BMSK                                  0x0000ffff
#define HWIO_SAW2_VERSION_STEP_SHFT                                         0x0


#endif

