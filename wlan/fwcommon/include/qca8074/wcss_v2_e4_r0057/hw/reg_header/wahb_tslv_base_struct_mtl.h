//////////////////////////////////////////////////////////////////////////////
// wahb_tslv_base_struct_mtl.h generated by: GenCStruct.pm 
//////////////////////////////////////////////////////////////////////////////
// **** W A R N I N G ****  THIS FILE IS AUTO GENERATED!! PLEASE DO NOT EDIT!!
//////////////////////////////////////////////////////////////////////////////
// QUALCOMM Proprietary
// Copyright Qualcomm Technologies Incorporated.  All rights reserved.
//
// All data and information contained in or disclosed by this document are
// confidential and proprietary information of Qualcomm Technologies Incorporated, and
// all rights therein are expressly reserved. By accepting this material,
// the recipient agrees that this material and the information contained
// therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in
// any manner to others without the express written permission of QUALCOMM
// Incorporated.
//
// This technology was exported from the United States in accordance with
// the Export Administration Regulations. Diversion contrary to U.S. law
// prohibited.
//////////////////////////////////////////////////////////////////////////////
// RCS File        : -USE CVS LOG-
// Revision        : -USE CVS LOG-
// Last Check In   : -USE CVS LOG-
//////////////////////////////////////////////////////////////////////////////
// Description     : Max to Lowest C Struct file
//
//////////////////////////////////////////////////////////////////////////////


#ifndef WAHB_TSLV_BASE_STRUCT_MTL_H
#define WAHB_TSLV_BASE_STRUCT_MTL_H

#ifndef UINT32_TYPE
#define UINT32_TYPE unsigned int
#endif


#ifndef LONG_TYPE
#define LONG_TYPE long int
#endif

typedef struct _wahb_tslv_abt_hw_version_t {
     UINT32_TYPE major     : 4;
     UINT32_TYPE minor     : 12;
     UINT32_TYPE step      : 16;
} wahb_tslv_abt_hw_version_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_hw_version_t f;
} wahb_tslv_abt_hw_version_u;

typedef struct _wahb_tslv_abt_inst_id_t {
     UINT32_TYPE instid    : 32;
} wahb_tslv_abt_inst_id_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_inst_id_t f;
} wahb_tslv_abt_inst_id_u;

typedef struct _wahb_tslv_abt_num_slaves_t {
     UINT32_TYPE _unused0  : 26;
     UINT32_TYPE numslaves : 6;
} wahb_tslv_abt_num_slaves_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_num_slaves_t f;
} wahb_tslv_abt_num_slaves_u;

typedef struct _wahb_tslv_abt_timer_loadval_t {
     UINT32_TYPE _unused0  : 24;
     UINT32_TYPE loadval   : 8;
} wahb_tslv_abt_timer_loadval_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_timer_loadval_t f;
} wahb_tslv_abt_timer_loadval_u;

typedef struct _wahb_tslv_abt_timer_mode_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE mode      : 1;
} wahb_tslv_abt_timer_mode_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_timer_mode_t f;
} wahb_tslv_abt_timer_mode_u;

typedef struct _wahb_tslv_abt_intr_status_t {
     UINT32_TYPE _unused0   : 31;
     UINT32_TYPE intrstatus : 1;
} wahb_tslv_abt_intr_status_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_intr_status_t f;
} wahb_tslv_abt_intr_status_u;

typedef struct _wahb_tslv_abt_intr_clear_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE intrclear : 1;
} wahb_tslv_abt_intr_clear_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_intr_clear_t f;
} wahb_tslv_abt_intr_clear_u;

typedef struct _wahb_tslv_abt_intr_enable_t {
     UINT32_TYPE _unused0   : 31;
     UINT32_TYPE intrenable : 1;
} wahb_tslv_abt_intr_enable_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_intr_enable_t f;
} wahb_tslv_abt_intr_enable_u;

typedef struct _wahb_tslv_abt_synd_valid_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE syndvalid : 1;
} wahb_tslv_abt_synd_valid_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_valid_t f;
} wahb_tslv_abt_synd_valid_u;

typedef struct _wahb_tslv_abt_synd_clear_t {
     UINT32_TYPE _unused0  : 31;
     UINT32_TYPE syndclear : 1;
} wahb_tslv_abt_synd_clear_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_clear_t f;
} wahb_tslv_abt_synd_clear_u;

typedef struct _wahb_tslv_abt_synd_id_t {
     UINT32_TYPE _unused0  : 16;
     UINT32_TYPE bid       : 3;
     UINT32_TYPE pid       : 5;
     UINT32_TYPE mid       : 8;
} wahb_tslv_abt_synd_id_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_id_t f;
} wahb_tslv_abt_synd_id_u;

typedef struct _wahb_tslv_abt_synd_addr0_t {
     UINT32_TYPE syndaddr0 : 32;
} wahb_tslv_abt_synd_addr0_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_addr0_t f;
} wahb_tslv_abt_synd_addr0_u;

typedef struct _wahb_tslv_abt_synd_addr1_t {
     UINT32_TYPE syndaddr1 : 32;
} wahb_tslv_abt_synd_addr1_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_addr1_t f;
} wahb_tslv_abt_synd_addr1_u;

typedef struct _wahb_tslv_abt_synd_hready_t {
     UINT32_TYPE syndhready : 32;
} wahb_tslv_abt_synd_hready_t;

typedef union {
     UINT32_TYPE val : 32;
     wahb_tslv_abt_synd_hready_t f;
} wahb_tslv_abt_synd_hready_u;


#endif

