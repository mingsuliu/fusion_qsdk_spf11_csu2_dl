///////////////////////////////////////////////////////////////////////////////////////////////
//
// QUALCOMM Proprietary Design Data
// Copyright (c) 2011, Qualcomm Technologies Incorporated. All rights reserved.
//
// All data and information contained in or disclosed by this document are confidential and
// proprietary information of Qualcomm Technologies Incorporated, and all rights therein are expressly
// reserved. By accepting this material, the recipient agrees that this material and the
// information contained therein are held in confidence and in trust and will not be used,
// copied, reproduced in whole or in part, nor its contents revealed in any manner to others
// without the express written permission of Qualcomm Technologies Incorporated.
//
// This technology was exported from the United States in accordance with the Export
// Administration Regulations. Diversion contrary to U. S. law prohibited.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// mac_rri_reg_seq_hwioreg.h : automatically generated by Autoseq  3.1 6/24/2018 
// User Name:vakkati
//
// !! WARNING !!  DO NOT MANUALLY EDIT THIS FILE.
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MAC_RRI_REG_SEQ_REG_H__
#define __MAC_RRI_REG_SEQ_REG_H__

#include "seq_hwio.h"
#include "mac_rri_reg_seq_hwiobase.h"
#ifdef SCALE_INCLUDES
	#include "HALhwio.h"
#else
	#include "msmhwio.h"
#endif


///////////////////////////////////////////////////////////////////////////////////////////////
// Register Data for Block MAC_RRI_REG
///////////////////////////////////////////////////////////////////////////////////////////////

//// Register RRI_R0_SW_REG_REINIT_P ////

#define HWIO_RRI_R0_SW_REG_REINIT_P_ADDR(x)                          (x+0x00000000)
#define HWIO_RRI_R0_SW_REG_REINIT_P_PHYS(x)                          (x+0x00000000)
#define HWIO_RRI_R0_SW_REG_REINIT_P_RMSK                             0xffffffff
#define HWIO_RRI_R0_SW_REG_REINIT_P_SHFT                                      0
#define HWIO_RRI_R0_SW_REG_REINIT_P_IN(x)                            \
	in_dword_masked ( HWIO_RRI_R0_SW_REG_REINIT_P_ADDR(x), HWIO_RRI_R0_SW_REG_REINIT_P_RMSK)
#define HWIO_RRI_R0_SW_REG_REINIT_P_INM(x, mask)                     \
	in_dword_masked ( HWIO_RRI_R0_SW_REG_REINIT_P_ADDR(x), mask) 
#define HWIO_RRI_R0_SW_REG_REINIT_P_OUT(x, val)                      \
	out_dword( HWIO_RRI_R0_SW_REG_REINIT_P_ADDR(x), val)
#define HWIO_RRI_R0_SW_REG_REINIT_P_OUTM(x, mask, val)               \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_SW_REG_REINIT_P_ADDR(x), mask, val, HWIO_RRI_R0_SW_REG_REINIT_P_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_SW_REG_REINIT_P_SPARE_BMSK                       0xfffffffe
#define HWIO_RRI_R0_SW_REG_REINIT_P_SPARE_SHFT                              0x1

#define HWIO_RRI_R0_SW_REG_REINIT_P_VAL_BMSK                         0x00000001
#define HWIO_RRI_R0_SW_REG_REINIT_P_VAL_SHFT                                0x0

//// Register RRI_R0_SW_REG_REINIT_ADDR ////

#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_ADDR(x)                       (x+0x00000004)
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_PHYS(x)                       (x+0x00000004)
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_RMSK                          0xfffffffc
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_SHFT                                   2
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_IN(x)                         \
	in_dword_masked ( HWIO_RRI_R0_SW_REG_REINIT_ADDR_ADDR(x), HWIO_RRI_R0_SW_REG_REINIT_ADDR_RMSK)
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_INM(x, mask)                  \
	in_dword_masked ( HWIO_RRI_R0_SW_REG_REINIT_ADDR_ADDR(x), mask) 
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_OUT(x, val)                   \
	out_dword( HWIO_RRI_R0_SW_REG_REINIT_ADDR_ADDR(x), val)
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_OUTM(x, mask, val)            \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_SW_REG_REINIT_ADDR_ADDR(x), mask, val, HWIO_RRI_R0_SW_REG_REINIT_ADDR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_VAL_BMSK                      0xfffffffc
#define HWIO_RRI_R0_SW_REG_REINIT_ADDR_VAL_SHFT                             0x2

//// Register RRI_R0_MISC_CFG ////

#define HWIO_RRI_R0_MISC_CFG_ADDR(x)                                 (x+0x00000008)
#define HWIO_RRI_R0_MISC_CFG_PHYS(x)                                 (x+0x00000008)
#define HWIO_RRI_R0_MISC_CFG_RMSK                                    0xffffffff
#define HWIO_RRI_R0_MISC_CFG_SHFT                                             0
#define HWIO_RRI_R0_MISC_CFG_IN(x)                                   \
	in_dword_masked ( HWIO_RRI_R0_MISC_CFG_ADDR(x), HWIO_RRI_R0_MISC_CFG_RMSK)
#define HWIO_RRI_R0_MISC_CFG_INM(x, mask)                            \
	in_dword_masked ( HWIO_RRI_R0_MISC_CFG_ADDR(x), mask) 
#define HWIO_RRI_R0_MISC_CFG_OUT(x, val)                             \
	out_dword( HWIO_RRI_R0_MISC_CFG_ADDR(x), val)
#define HWIO_RRI_R0_MISC_CFG_OUTM(x, mask, val)                      \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_MISC_CFG_ADDR(x), mask, val, HWIO_RRI_R0_MISC_CFG_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_MISC_CFG_SPARE_BMSK                              0xc0000000
#define HWIO_RRI_R0_MISC_CFG_SPARE_SHFT                                    0x1e

#define HWIO_RRI_R0_MISC_CFG_GOTO_IDLE_FOR_INVALID_CMD_BMSK          0x20000000
#define HWIO_RRI_R0_MISC_CFG_GOTO_IDLE_FOR_INVALID_CMD_SHFT                0x1d

#define HWIO_RRI_R0_MISC_CFG_SW_RRI_DONE_FOR_INVALID_CMD_BMSK        0x10000000
#define HWIO_RRI_R0_MISC_CFG_SW_RRI_DONE_FOR_INVALID_CMD_SHFT              0x1c

#define HWIO_RRI_R0_MISC_CFG_SW_REG_REINIT_CMD_EXT_BASE_ADDR_BMSK    0x0ff00000
#define HWIO_RRI_R0_MISC_CFG_SW_REG_REINIT_CMD_EXT_BASE_ADDR_SHFT          0x14

#define HWIO_RRI_R0_MISC_CFG_ENCODED_FSM_BMSK                        0x000f8000
#define HWIO_RRI_R0_MISC_CFG_ENCODED_FSM_SHFT                               0xf

#define HWIO_RRI_R0_MISC_CFG_SPARE2_BMSK                             0x00007800
#define HWIO_RRI_R0_MISC_CFG_SPARE2_SHFT                                    0xb

#define HWIO_RRI_R0_MISC_CFG_GOTO_IDLE_BMSK                          0x00000400
#define HWIO_RRI_R0_MISC_CFG_GOTO_IDLE_SHFT                                 0xa

#define HWIO_RRI_R0_MISC_CFG_ERR_STATE_CFG_BMSK                      0x00000200
#define HWIO_RRI_R0_MISC_CFG_ERR_STATE_CFG_SHFT                             0x9

#define HWIO_RRI_R0_MISC_CFG_ERR_EXIT_BMSK                           0x00000100
#define HWIO_RRI_R0_MISC_CFG_ERR_EXIT_SHFT                                  0x8

#define HWIO_RRI_R0_MISC_CFG_ONE_USEC_LIMIT_BMSK                     0x000000ff
#define HWIO_RRI_R0_MISC_CFG_ONE_USEC_LIMIT_SHFT                            0x0

//// Register RRI_R0_MEM_RANGE_START ////

#define HWIO_RRI_R0_MEM_RANGE_START_ADDR(x)                          (x+0x0000000c)
#define HWIO_RRI_R0_MEM_RANGE_START_PHYS(x)                          (x+0x0000000c)
#define HWIO_RRI_R0_MEM_RANGE_START_RMSK                             0xffffffff
#define HWIO_RRI_R0_MEM_RANGE_START_SHFT                                      0
#define HWIO_RRI_R0_MEM_RANGE_START_IN(x)                            \
	in_dword_masked ( HWIO_RRI_R0_MEM_RANGE_START_ADDR(x), HWIO_RRI_R0_MEM_RANGE_START_RMSK)
#define HWIO_RRI_R0_MEM_RANGE_START_INM(x, mask)                     \
	in_dword_masked ( HWIO_RRI_R0_MEM_RANGE_START_ADDR(x), mask) 
#define HWIO_RRI_R0_MEM_RANGE_START_OUT(x, val)                      \
	out_dword( HWIO_RRI_R0_MEM_RANGE_START_ADDR(x), val)
#define HWIO_RRI_R0_MEM_RANGE_START_OUTM(x, mask, val)               \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_MEM_RANGE_START_ADDR(x), mask, val, HWIO_RRI_R0_MEM_RANGE_START_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_MEM_RANGE_START_ADDR_BMSK                        0xffffffff
#define HWIO_RRI_R0_MEM_RANGE_START_ADDR_SHFT                               0x0

//// Register RRI_R0_MEM_RANGE_END ////

#define HWIO_RRI_R0_MEM_RANGE_END_ADDR(x)                            (x+0x00000010)
#define HWIO_RRI_R0_MEM_RANGE_END_PHYS(x)                            (x+0x00000010)
#define HWIO_RRI_R0_MEM_RANGE_END_RMSK                               0xffffffff
#define HWIO_RRI_R0_MEM_RANGE_END_SHFT                                        0
#define HWIO_RRI_R0_MEM_RANGE_END_IN(x)                              \
	in_dword_masked ( HWIO_RRI_R0_MEM_RANGE_END_ADDR(x), HWIO_RRI_R0_MEM_RANGE_END_RMSK)
#define HWIO_RRI_R0_MEM_RANGE_END_INM(x, mask)                       \
	in_dword_masked ( HWIO_RRI_R0_MEM_RANGE_END_ADDR(x), mask) 
#define HWIO_RRI_R0_MEM_RANGE_END_OUT(x, val)                        \
	out_dword( HWIO_RRI_R0_MEM_RANGE_END_ADDR(x), val)
#define HWIO_RRI_R0_MEM_RANGE_END_OUTM(x, mask, val)                 \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_MEM_RANGE_END_ADDR(x), mask, val, HWIO_RRI_R0_MEM_RANGE_END_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_MEM_RANGE_END_ADDR_BMSK                          0xffffffff
#define HWIO_RRI_R0_MEM_RANGE_END_ADDR_SHFT                                 0x0

//// Register RRI_R0_PMM_DONE_EXTEND_PULSE ////

#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_ADDR(x)                    (x+0x00000014)
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_PHYS(x)                    (x+0x00000014)
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_RMSK                       0x0000003f
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_SHFT                                0
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_IN(x)                      \
	in_dword_masked ( HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_ADDR(x), HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_RMSK)
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_INM(x, mask)               \
	in_dword_masked ( HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_ADDR(x), mask) 
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_OUT(x, val)                \
	out_dword( HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_ADDR(x), val)
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_OUTM(x, mask, val)         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_ADDR(x), mask, val, HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_VAL_BMSK                   0x0000003f
#define HWIO_RRI_R0_PMM_DONE_EXTEND_PULSE_VAL_SHFT                          0x0

//// Register RRI_R0_TRC_REG_REINIT_ADDR ////

#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_ADDR(x)                      (x+0x00000018)
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_PHYS(x)                      (x+0x00000018)
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_RMSK                         0xfffffffc
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_SHFT                                  2
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_IN(x)                        \
	in_dword_masked ( HWIO_RRI_R0_TRC_REG_REINIT_ADDR_ADDR(x), HWIO_RRI_R0_TRC_REG_REINIT_ADDR_RMSK)
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_INM(x, mask)                 \
	in_dword_masked ( HWIO_RRI_R0_TRC_REG_REINIT_ADDR_ADDR(x), mask) 
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_OUT(x, val)                  \
	out_dword( HWIO_RRI_R0_TRC_REG_REINIT_ADDR_ADDR(x), val)
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_OUTM(x, mask, val)           \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R0_TRC_REG_REINIT_ADDR_ADDR(x), mask, val, HWIO_RRI_R0_TRC_REG_REINIT_ADDR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_VAL_BMSK                     0xfffffffc
#define HWIO_RRI_R0_TRC_REG_REINIT_ADDR_VAL_SHFT                            0x2

//// Register RRI_R1_POLL_CMD_ERR_ADDR ////

#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_ADDR(x)                        (x+0x00001000)
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_PHYS(x)                        (x+0x00001000)
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_RMSK                           0xfffffffc
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_SHFT                                    2
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_IN(x)                          \
	in_dword_masked ( HWIO_RRI_R1_POLL_CMD_ERR_ADDR_ADDR(x), HWIO_RRI_R1_POLL_CMD_ERR_ADDR_RMSK)
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_INM(x, mask)                   \
	in_dword_masked ( HWIO_RRI_R1_POLL_CMD_ERR_ADDR_ADDR(x), mask) 
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_OUT(x, val)                    \
	out_dword( HWIO_RRI_R1_POLL_CMD_ERR_ADDR_ADDR(x), val)
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_OUTM(x, mask, val)             \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_POLL_CMD_ERR_ADDR_ADDR(x), mask, val, HWIO_RRI_R1_POLL_CMD_ERR_ADDR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_VAL_BMSK                       0xfffffffc
#define HWIO_RRI_R1_POLL_CMD_ERR_ADDR_VAL_SHFT                              0x2

//// Register RRI_R1_ERR_INT_P ////

#define HWIO_RRI_R1_ERR_INT_P_ADDR(x)                                (x+0x00001004)
#define HWIO_RRI_R1_ERR_INT_P_PHYS(x)                                (x+0x00001004)
#define HWIO_RRI_R1_ERR_INT_P_RMSK                                   0x00000007
#define HWIO_RRI_R1_ERR_INT_P_SHFT                                            0
#define HWIO_RRI_R1_ERR_INT_P_IN(x)                                  \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_P_ADDR(x), HWIO_RRI_R1_ERR_INT_P_RMSK)
#define HWIO_RRI_R1_ERR_INT_P_INM(x, mask)                           \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_P_ADDR(x), mask) 
#define HWIO_RRI_R1_ERR_INT_P_OUT(x, val)                            \
	out_dword( HWIO_RRI_R1_ERR_INT_P_ADDR(x), val)
#define HWIO_RRI_R1_ERR_INT_P_OUTM(x, mask, val)                     \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_ERR_INT_P_ADDR(x), mask, val, HWIO_RRI_R1_ERR_INT_P_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_ERR_INT_P_SW_TRIG_REININT_DONE_BMSK              0x00000004
#define HWIO_RRI_R1_ERR_INT_P_SW_TRIG_REININT_DONE_SHFT                     0x2

#define HWIO_RRI_R1_ERR_INT_P_POLL_CMD_ERR_BMSK                      0x00000002
#define HWIO_RRI_R1_ERR_INT_P_POLL_CMD_ERR_SHFT                             0x1

#define HWIO_RRI_R1_ERR_INT_P_APB_ERR_BMSK                           0x00000001
#define HWIO_RRI_R1_ERR_INT_P_APB_ERR_SHFT                                  0x0

//// Register RRI_R1_ERR_INT_BITMASK ////

#define HWIO_RRI_R1_ERR_INT_BITMASK_ADDR(x)                          (x+0x00001008)
#define HWIO_RRI_R1_ERR_INT_BITMASK_PHYS(x)                          (x+0x00001008)
#define HWIO_RRI_R1_ERR_INT_BITMASK_RMSK                             0x000001ff
#define HWIO_RRI_R1_ERR_INT_BITMASK_SHFT                                      0
#define HWIO_RRI_R1_ERR_INT_BITMASK_IN(x)                            \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_BITMASK_ADDR(x), HWIO_RRI_R1_ERR_INT_BITMASK_RMSK)
#define HWIO_RRI_R1_ERR_INT_BITMASK_INM(x, mask)                     \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_BITMASK_ADDR(x), mask) 
#define HWIO_RRI_R1_ERR_INT_BITMASK_OUT(x, val)                      \
	out_dword( HWIO_RRI_R1_ERR_INT_BITMASK_ADDR(x), val)
#define HWIO_RRI_R1_ERR_INT_BITMASK_OUTM(x, mask, val)               \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_ERR_INT_BITMASK_ADDR(x), mask, val, HWIO_RRI_R1_ERR_INT_BITMASK_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_ERR_INT_BITMASK_INVALID_CMD_ERR_BMSK             0x00000100
#define HWIO_RRI_R1_ERR_INT_BITMASK_INVALID_CMD_ERR_SHFT                    0x8

#define HWIO_RRI_R1_ERR_INT_BITMASK_TRC_TRIG_REINIT_DONE_BMSK        0x00000080
#define HWIO_RRI_R1_ERR_INT_BITMASK_TRC_TRIG_REINIT_DONE_SHFT               0x7

#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_STATE_TIMEOUT_ERR_BMSK       0x00000040
#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_STATE_TIMEOUT_ERR_SHFT              0x6

#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_RD_INVALID_ADDR_BMSK         0x00000020
#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_RD_INVALID_ADDR_SHFT                0x5

#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_WR_INVALID_ADDR_BMSK         0x00000010
#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_WR_INVALID_ADDR_SHFT                0x4

#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_WR_TO_RD_ONLY_ADDR_BMSK      0x00000008
#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_WR_TO_RD_ONLY_ADDR_SHFT             0x3

#define HWIO_RRI_R1_ERR_INT_BITMASK_SW_TRIG_REININT_DONE_BMSK        0x00000004
#define HWIO_RRI_R1_ERR_INT_BITMASK_SW_TRIG_REININT_DONE_SHFT               0x2

#define HWIO_RRI_R1_ERR_INT_BITMASK_POLL_CMD_ERR_BMSK                0x00000002
#define HWIO_RRI_R1_ERR_INT_BITMASK_POLL_CMD_ERR_SHFT                       0x1

#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_ERR_BMSK                     0x00000001
#define HWIO_RRI_R1_ERR_INT_BITMASK_APB_ERR_SHFT                            0x0

//// Register RRI_R1_ERR_INT_STATUS ////

#define HWIO_RRI_R1_ERR_INT_STATUS_ADDR(x)                           (x+0x0000100c)
#define HWIO_RRI_R1_ERR_INT_STATUS_PHYS(x)                           (x+0x0000100c)
#define HWIO_RRI_R1_ERR_INT_STATUS_RMSK                              0x000001ff
#define HWIO_RRI_R1_ERR_INT_STATUS_SHFT                                       0
#define HWIO_RRI_R1_ERR_INT_STATUS_IN(x)                             \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_STATUS_ADDR(x), HWIO_RRI_R1_ERR_INT_STATUS_RMSK)
#define HWIO_RRI_R1_ERR_INT_STATUS_INM(x, mask)                      \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_STATUS_ADDR(x), mask) 
#define HWIO_RRI_R1_ERR_INT_STATUS_OUT(x, val)                       \
	out_dword( HWIO_RRI_R1_ERR_INT_STATUS_ADDR(x), val)
#define HWIO_RRI_R1_ERR_INT_STATUS_OUTM(x, mask, val)                \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_ERR_INT_STATUS_ADDR(x), mask, val, HWIO_RRI_R1_ERR_INT_STATUS_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_ERR_INT_STATUS_INVALID_CMD_ERR_BMSK              0x00000100
#define HWIO_RRI_R1_ERR_INT_STATUS_INVALID_CMD_ERR_SHFT                     0x8

#define HWIO_RRI_R1_ERR_INT_STATUS_TRC_TRIG_REINIT_DONE_BMSK         0x00000080
#define HWIO_RRI_R1_ERR_INT_STATUS_TRC_TRIG_REINIT_DONE_SHFT                0x7

#define HWIO_RRI_R1_ERR_INT_STATUS_APB_STATE_TIMEOUT_ERR_BMSK        0x00000040
#define HWIO_RRI_R1_ERR_INT_STATUS_APB_STATE_TIMEOUT_ERR_SHFT               0x6

#define HWIO_RRI_R1_ERR_INT_STATUS_APB_RD_INVALID_ADDR_BMSK          0x00000020
#define HWIO_RRI_R1_ERR_INT_STATUS_APB_RD_INVALID_ADDR_SHFT                 0x5

#define HWIO_RRI_R1_ERR_INT_STATUS_APB_WR_INVALID_ADDR_BMSK          0x00000010
#define HWIO_RRI_R1_ERR_INT_STATUS_APB_WR_INVALID_ADDR_SHFT                 0x4

#define HWIO_RRI_R1_ERR_INT_STATUS_APB_WR_TO_RD_ONLY_ADDR_BMSK       0x00000008
#define HWIO_RRI_R1_ERR_INT_STATUS_APB_WR_TO_RD_ONLY_ADDR_SHFT              0x3

#define HWIO_RRI_R1_ERR_INT_STATUS_SW_TRIG_REININT_DONE_BMSK         0x00000004
#define HWIO_RRI_R1_ERR_INT_STATUS_SW_TRIG_REININT_DONE_SHFT                0x2

#define HWIO_RRI_R1_ERR_INT_STATUS_POLL_CMD_ERR_BMSK                 0x00000002
#define HWIO_RRI_R1_ERR_INT_STATUS_POLL_CMD_ERR_SHFT                        0x1

#define HWIO_RRI_R1_ERR_INT_STATUS_APB_ERR_BMSK                      0x00000001
#define HWIO_RRI_R1_ERR_INT_STATUS_APB_ERR_SHFT                             0x0

//// Register RRI_R1_CURRENT_ADDR ////

#define HWIO_RRI_R1_CURRENT_ADDR_ADDR(x)                             (x+0x00001010)
#define HWIO_RRI_R1_CURRENT_ADDR_PHYS(x)                             (x+0x00001010)
#define HWIO_RRI_R1_CURRENT_ADDR_RMSK                                0xffffffff
#define HWIO_RRI_R1_CURRENT_ADDR_SHFT                                         0
#define HWIO_RRI_R1_CURRENT_ADDR_IN(x)                               \
	in_dword_masked ( HWIO_RRI_R1_CURRENT_ADDR_ADDR(x), HWIO_RRI_R1_CURRENT_ADDR_RMSK)
#define HWIO_RRI_R1_CURRENT_ADDR_INM(x, mask)                        \
	in_dword_masked ( HWIO_RRI_R1_CURRENT_ADDR_ADDR(x), mask) 
#define HWIO_RRI_R1_CURRENT_ADDR_OUT(x, val)                         \
	out_dword( HWIO_RRI_R1_CURRENT_ADDR_ADDR(x), val)
#define HWIO_RRI_R1_CURRENT_ADDR_OUTM(x, mask, val)                  \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_CURRENT_ADDR_ADDR(x), mask, val, HWIO_RRI_R1_CURRENT_ADDR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_CURRENT_ADDR_VAL_BMSK                            0xffffffff
#define HWIO_RRI_R1_CURRENT_ADDR_VAL_SHFT                                   0x0

//// Register RRI_R1_EVENTMASK_IX_0 ////

#define HWIO_RRI_R1_EVENTMASK_IX_0_ADDR(x)                           (x+0x00001014)
#define HWIO_RRI_R1_EVENTMASK_IX_0_PHYS(x)                           (x+0x00001014)
#define HWIO_RRI_R1_EVENTMASK_IX_0_RMSK                              0x03ffffff
#define HWIO_RRI_R1_EVENTMASK_IX_0_SHFT                                       0
#define HWIO_RRI_R1_EVENTMASK_IX_0_IN(x)                             \
	in_dword_masked ( HWIO_RRI_R1_EVENTMASK_IX_0_ADDR(x), HWIO_RRI_R1_EVENTMASK_IX_0_RMSK)
#define HWIO_RRI_R1_EVENTMASK_IX_0_INM(x, mask)                      \
	in_dword_masked ( HWIO_RRI_R1_EVENTMASK_IX_0_ADDR(x), mask) 
#define HWIO_RRI_R1_EVENTMASK_IX_0_OUT(x, val)                       \
	out_dword( HWIO_RRI_R1_EVENTMASK_IX_0_ADDR(x), val)
#define HWIO_RRI_R1_EVENTMASK_IX_0_OUTM(x, mask, val)                \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_EVENTMASK_IX_0_ADDR(x), mask, val, HWIO_RRI_R1_EVENTMASK_IX_0_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_EVENTMASK_IX_0_MASK_BMSK                         0x03ffffff
#define HWIO_RRI_R1_EVENTMASK_IX_0_MASK_SHFT                                0x0

//// Register RRI_R1_INVALID_APB_ACC_ADR ////

#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_ADDR(x)                      (x+0x00001018)
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_PHYS(x)                      (x+0x00001018)
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_RMSK                         0x0001ffff
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_SHFT                                  0
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_IN(x)                        \
	in_dword_masked ( HWIO_RRI_R1_INVALID_APB_ACC_ADR_ADDR(x), HWIO_RRI_R1_INVALID_APB_ACC_ADR_RMSK)
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_INM(x, mask)                 \
	in_dword_masked ( HWIO_RRI_R1_INVALID_APB_ACC_ADR_ADDR(x), mask) 
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_OUT(x, val)                  \
	out_dword( HWIO_RRI_R1_INVALID_APB_ACC_ADR_ADDR(x), val)
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_OUTM(x, mask, val)           \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_INVALID_APB_ACC_ADR_ADDR(x), mask, val, HWIO_RRI_R1_INVALID_APB_ACC_ADR_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_VAL_BMSK                     0x0001ffff
#define HWIO_RRI_R1_INVALID_APB_ACC_ADR_VAL_SHFT                            0x0

//// Register RRI_R1_SM_STATES_IX_0 ////

#define HWIO_RRI_R1_SM_STATES_IX_0_ADDR(x)                           (x+0x0000101c)
#define HWIO_RRI_R1_SM_STATES_IX_0_PHYS(x)                           (x+0x0000101c)
#define HWIO_RRI_R1_SM_STATES_IX_0_RMSK                              0x0000ffff
#define HWIO_RRI_R1_SM_STATES_IX_0_SHFT                                       0
#define HWIO_RRI_R1_SM_STATES_IX_0_IN(x)                             \
	in_dword_masked ( HWIO_RRI_R1_SM_STATES_IX_0_ADDR(x), HWIO_RRI_R1_SM_STATES_IX_0_RMSK)
#define HWIO_RRI_R1_SM_STATES_IX_0_INM(x, mask)                      \
	in_dword_masked ( HWIO_RRI_R1_SM_STATES_IX_0_ADDR(x), mask) 
#define HWIO_RRI_R1_SM_STATES_IX_0_OUT(x, val)                       \
	out_dword( HWIO_RRI_R1_SM_STATES_IX_0_ADDR(x), val)
#define HWIO_RRI_R1_SM_STATES_IX_0_OUTM(x, mask, val)                \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_SM_STATES_IX_0_ADDR(x), mask, val, HWIO_RRI_R1_SM_STATES_IX_0_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_MEM_BMSK                 0x0000e000
#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_MEM_SHFT                        0xd

#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_APB_BMSK                 0x00001fe0
#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_APB_SHFT                        0x5

#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_CMD_BMSK                 0x0000001f
#define HWIO_RRI_R1_SM_STATES_IX_0_SM_STATE_CMD_SHFT                        0x0

//// Register RRI_R1_ACTIVE ////

#define HWIO_RRI_R1_ACTIVE_ADDR(x)                                   (x+0x00001020)
#define HWIO_RRI_R1_ACTIVE_PHYS(x)                                   (x+0x00001020)
#define HWIO_RRI_R1_ACTIVE_RMSK                                      0x00000001
#define HWIO_RRI_R1_ACTIVE_SHFT                                               0
#define HWIO_RRI_R1_ACTIVE_IN(x)                                     \
	in_dword_masked ( HWIO_RRI_R1_ACTIVE_ADDR(x), HWIO_RRI_R1_ACTIVE_RMSK)
#define HWIO_RRI_R1_ACTIVE_INM(x, mask)                              \
	in_dword_masked ( HWIO_RRI_R1_ACTIVE_ADDR(x), mask) 
#define HWIO_RRI_R1_ACTIVE_OUT(x, val)                               \
	out_dword( HWIO_RRI_R1_ACTIVE_ADDR(x), val)
#define HWIO_RRI_R1_ACTIVE_OUTM(x, mask, val)                        \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_ACTIVE_ADDR(x), mask, val, HWIO_RRI_R1_ACTIVE_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_ACTIVE_VAL_BMSK                                  0x00000001
#define HWIO_RRI_R1_ACTIVE_VAL_SHFT                                         0x0

//// Register RRI_R1_APB_TIMEOUT ////

#define HWIO_RRI_R1_APB_TIMEOUT_ADDR(x)                              (x+0x00001024)
#define HWIO_RRI_R1_APB_TIMEOUT_PHYS(x)                              (x+0x00001024)
#define HWIO_RRI_R1_APB_TIMEOUT_RMSK                                 0xffffffff
#define HWIO_RRI_R1_APB_TIMEOUT_SHFT                                          0
#define HWIO_RRI_R1_APB_TIMEOUT_IN(x)                                \
	in_dword_masked ( HWIO_RRI_R1_APB_TIMEOUT_ADDR(x), HWIO_RRI_R1_APB_TIMEOUT_RMSK)
#define HWIO_RRI_R1_APB_TIMEOUT_INM(x, mask)                         \
	in_dword_masked ( HWIO_RRI_R1_APB_TIMEOUT_ADDR(x), mask) 
#define HWIO_RRI_R1_APB_TIMEOUT_OUT(x, val)                          \
	out_dword( HWIO_RRI_R1_APB_TIMEOUT_ADDR(x), val)
#define HWIO_RRI_R1_APB_TIMEOUT_OUTM(x, mask, val)                   \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_APB_TIMEOUT_ADDR(x), mask, val, HWIO_RRI_R1_APB_TIMEOUT_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_APB_TIMEOUT_VAL_BMSK                             0xffffffff
#define HWIO_RRI_R1_APB_TIMEOUT_VAL_SHFT                                    0x0

//// Register RRI_R1_INVALID_CMD_WORD ////

#define HWIO_RRI_R1_INVALID_CMD_WORD_ADDR(x)                         (x+0x00001028)
#define HWIO_RRI_R1_INVALID_CMD_WORD_PHYS(x)                         (x+0x00001028)
#define HWIO_RRI_R1_INVALID_CMD_WORD_RMSK                            0xffffffff
#define HWIO_RRI_R1_INVALID_CMD_WORD_SHFT                                     0
#define HWIO_RRI_R1_INVALID_CMD_WORD_IN(x)                           \
	in_dword_masked ( HWIO_RRI_R1_INVALID_CMD_WORD_ADDR(x), HWIO_RRI_R1_INVALID_CMD_WORD_RMSK)
#define HWIO_RRI_R1_INVALID_CMD_WORD_INM(x, mask)                    \
	in_dword_masked ( HWIO_RRI_R1_INVALID_CMD_WORD_ADDR(x), mask) 
#define HWIO_RRI_R1_INVALID_CMD_WORD_OUT(x, val)                     \
	out_dword( HWIO_RRI_R1_INVALID_CMD_WORD_ADDR(x), val)
#define HWIO_RRI_R1_INVALID_CMD_WORD_OUTM(x, mask, val)              \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_INVALID_CMD_WORD_ADDR(x), mask, val, HWIO_RRI_R1_INVALID_CMD_WORD_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_INVALID_CMD_WORD_VAL_BMSK                        0xffffffff
#define HWIO_RRI_R1_INVALID_CMD_WORD_VAL_SHFT                               0x0

//// Register RRI_R1_TESTBUS_LOWER ////

#define HWIO_RRI_R1_TESTBUS_LOWER_ADDR(x)                            (x+0x0000102c)
#define HWIO_RRI_R1_TESTBUS_LOWER_PHYS(x)                            (x+0x0000102c)
#define HWIO_RRI_R1_TESTBUS_LOWER_RMSK                               0xffffffff
#define HWIO_RRI_R1_TESTBUS_LOWER_SHFT                                        0
#define HWIO_RRI_R1_TESTBUS_LOWER_IN(x)                              \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_LOWER_ADDR(x), HWIO_RRI_R1_TESTBUS_LOWER_RMSK)
#define HWIO_RRI_R1_TESTBUS_LOWER_INM(x, mask)                       \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_LOWER_ADDR(x), mask) 
#define HWIO_RRI_R1_TESTBUS_LOWER_OUT(x, val)                        \
	out_dword( HWIO_RRI_R1_TESTBUS_LOWER_ADDR(x), val)
#define HWIO_RRI_R1_TESTBUS_LOWER_OUTM(x, mask, val)                 \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_TESTBUS_LOWER_ADDR(x), mask, val, HWIO_RRI_R1_TESTBUS_LOWER_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_TESTBUS_LOWER_VALUE_BMSK                         0xffffffff
#define HWIO_RRI_R1_TESTBUS_LOWER_VALUE_SHFT                                0x0

//// Register RRI_R1_TESTBUS_UPPER ////

#define HWIO_RRI_R1_TESTBUS_UPPER_ADDR(x)                            (x+0x00001030)
#define HWIO_RRI_R1_TESTBUS_UPPER_PHYS(x)                            (x+0x00001030)
#define HWIO_RRI_R1_TESTBUS_UPPER_RMSK                               0x000000ff
#define HWIO_RRI_R1_TESTBUS_UPPER_SHFT                                        0
#define HWIO_RRI_R1_TESTBUS_UPPER_IN(x)                              \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_UPPER_ADDR(x), HWIO_RRI_R1_TESTBUS_UPPER_RMSK)
#define HWIO_RRI_R1_TESTBUS_UPPER_INM(x, mask)                       \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_UPPER_ADDR(x), mask) 
#define HWIO_RRI_R1_TESTBUS_UPPER_OUT(x, val)                        \
	out_dword( HWIO_RRI_R1_TESTBUS_UPPER_ADDR(x), val)
#define HWIO_RRI_R1_TESTBUS_UPPER_OUTM(x, mask, val)                 \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_TESTBUS_UPPER_ADDR(x), mask, val, HWIO_RRI_R1_TESTBUS_UPPER_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_TESTBUS_UPPER_VALUE_BMSK                         0x000000ff
#define HWIO_RRI_R1_TESTBUS_UPPER_VALUE_SHFT                                0x0

//// Register RRI_R1_ERR_INT_STATUS_MASKED ////

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_ADDR(x)                    (x+0x00001034)
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_PHYS(x)                    (x+0x00001034)
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_RMSK                       0x000001ff
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_SHFT                                0
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_IN(x)                      \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_STATUS_MASKED_ADDR(x), HWIO_RRI_R1_ERR_INT_STATUS_MASKED_RMSK)
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_INM(x, mask)               \
	in_dword_masked ( HWIO_RRI_R1_ERR_INT_STATUS_MASKED_ADDR(x), mask) 
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_OUT(x, val)                \
	out_dword( HWIO_RRI_R1_ERR_INT_STATUS_MASKED_ADDR(x), val)
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_OUTM(x, mask, val)         \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_ERR_INT_STATUS_MASKED_ADDR(x), mask, val, HWIO_RRI_R1_ERR_INT_STATUS_MASKED_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_INVALID_CMD_STATUS_BMSK    0x00000100
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_INVALID_CMD_STATUS_SHFT           0x8

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_TRC_TRIG_REINIT_DONE_P_STATUS_BMSK 0x00000080
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_TRC_TRIG_REINIT_DONE_P_STATUS_SHFT        0x7

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_STATE_TIMEOUT_ERR_P_STATUS_BMSK 0x00000040
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_STATE_TIMEOUT_ERR_P_STATUS_SHFT        0x6

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_RD_INVALID_ADDR_P_STATUS_BMSK 0x00000020
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_RD_INVALID_ADDR_P_STATUS_SHFT        0x5

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_WR_INVALID_ADDR_P_STATUS_BMSK 0x00000010
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_WR_INVALID_ADDR_P_STATUS_SHFT        0x4

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_WR_TO_RD_ONLY_ADDR_P_TEMP_STATUS_BMSK 0x00000008
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_WR_TO_RD_ONLY_ADDR_P_TEMP_STATUS_SHFT        0x3

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_SW_TRIG_REININT_DONE_P_STATUS_BMSK 0x00000004
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_SW_TRIG_REININT_DONE_P_STATUS_SHFT        0x2

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_POLL_CMD_ERR_STATUS_BMSK   0x00000002
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_POLL_CMD_ERR_STATUS_SHFT          0x1

#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_ERR_STATUS_BMSK        0x00000001
#define HWIO_RRI_R1_ERR_INT_STATUS_MASKED_APB_ERR_STATUS_SHFT               0x0

//// Register RRI_R1_TESTBUS_CTRL ////

#define HWIO_RRI_R1_TESTBUS_CTRL_ADDR(x)                             (x+0x00001038)
#define HWIO_RRI_R1_TESTBUS_CTRL_PHYS(x)                             (x+0x00001038)
#define HWIO_RRI_R1_TESTBUS_CTRL_RMSK                                0x0000000f
#define HWIO_RRI_R1_TESTBUS_CTRL_SHFT                                         0
#define HWIO_RRI_R1_TESTBUS_CTRL_IN(x)                               \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_CTRL_ADDR(x), HWIO_RRI_R1_TESTBUS_CTRL_RMSK)
#define HWIO_RRI_R1_TESTBUS_CTRL_INM(x, mask)                        \
	in_dword_masked ( HWIO_RRI_R1_TESTBUS_CTRL_ADDR(x), mask) 
#define HWIO_RRI_R1_TESTBUS_CTRL_OUT(x, val)                         \
	out_dword( HWIO_RRI_R1_TESTBUS_CTRL_ADDR(x), val)
#define HWIO_RRI_R1_TESTBUS_CTRL_OUTM(x, mask, val)                  \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_TESTBUS_CTRL_ADDR(x), mask, val, HWIO_RRI_R1_TESTBUS_CTRL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_TESTBUS_CTRL_TESTBUS_SELECT_BMSK                 0x0000000f
#define HWIO_RRI_R1_TESTBUS_CTRL_TESTBUS_SELECT_SHFT                        0x0

//// Register RRI_R1_REG_ACCESS_EVENT_GEN_CTRL ////

#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDR(x)                (x+0x0000103c)
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_PHYS(x)                (x+0x0000103c)
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_RMSK                   0xffffffff
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_SHFT                            0
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_IN(x)                  \
	in_dword_masked ( HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDR(x), HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_RMSK)
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_INM(x, mask)           \
	in_dword_masked ( HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDR(x), mask) 
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_OUT(x, val)            \
	out_dword( HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDR(x), val)
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_OUTM(x, mask, val)     \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDR(x), mask, val, HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDRESS_RANGE_END_BMSK 0xfffe0000
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDRESS_RANGE_END_SHFT       0x11

#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDRESS_RANGE_START_BMSK 0x0001fffc
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_ADDRESS_RANGE_START_SHFT        0x2

#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_WRITE_ACCESS_REPORT_ENABLE_BMSK 0x00000002
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_WRITE_ACCESS_REPORT_ENABLE_SHFT        0x1

#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_READ_ACCESS_REPORT_ENABLE_BMSK 0x00000001
#define HWIO_RRI_R1_REG_ACCESS_EVENT_GEN_CTRL_READ_ACCESS_REPORT_ENABLE_SHFT        0x0

//// Register RRI_R1_RRI_SPARE_REGISTER ////

#define HWIO_RRI_R1_RRI_SPARE_REGISTER_ADDR(x)                       (x+0x00001040)
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_PHYS(x)                       (x+0x00001040)
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_RMSK                          0x0000ffff
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_SHFT                                   0
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_IN(x)                         \
	in_dword_masked ( HWIO_RRI_R1_RRI_SPARE_REGISTER_ADDR(x), HWIO_RRI_R1_RRI_SPARE_REGISTER_RMSK)
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_INM(x, mask)                  \
	in_dword_masked ( HWIO_RRI_R1_RRI_SPARE_REGISTER_ADDR(x), mask) 
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_OUT(x, val)                   \
	out_dword( HWIO_RRI_R1_RRI_SPARE_REGISTER_ADDR(x), val)
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_OUTM(x, mask, val)            \
	do {\
		HWIO_INTLOCK(); \
		out_dword_masked_ns(HWIO_RRI_R1_RRI_SPARE_REGISTER_ADDR(x), mask, val, HWIO_RRI_R1_RRI_SPARE_REGISTER_IN(x)); \
		HWIO_INTFREE();\
	} while (0) 

#define HWIO_RRI_R1_RRI_SPARE_REGISTER_SPARE_FIELD_BMSK              0x0000ffff
#define HWIO_RRI_R1_RRI_SPARE_REGISTER_SPARE_FIELD_SHFT                     0x0


#endif

