/* This file is specific to Windows and is needed for enabling ETW logging */
/* Keeping this file dummy for other HLOS as ETW changes are present in the */
/* OS agnostic files as well. */

#ifdef WINDOWS_ETW_LOGGING
#include "QCOMWlanEvents.h"
#else
#define Dot11Trace(...) 
#define Dot11Trace0(...) 
#define EventWriteUmacSend80211Auth(...) 0
#define EventWriteUmacSend80211DeAuth(...) 
#define EventWriteUmacSend80211DisAssoc(...) 
#define EventEnabledUmacRecv80211DisAssoc(...) 0
#define EventWriteUmacRecv80211DisAssoc(...) 
#define EventEnabledUmacSend80211Assoc(...) 0
#define EventWriteUmacSend80211Assoc(...) 0
#define EventEnabledUmacSend80211ReAssoc(...) 0
#define EventWriteUmacSend80211ReAssoc(...) 0
#define EventEnabledUmacRecv80211AssocResp(...) 0
#define EventWriteUmacRecv80211AssocResp(...) 0
#define EventEnabledUmacSend80211DisAssoc(...) 0
#define EventWriteUmacRecv80211DeAuth(...) 
#define EventEnabledUmacRecv80211DeAuth(...) 0
#define EventEnabledUmacSend80211DeAuth(...) 0
#define EventEnabledUmacSend80211Auth(...) 0
#define EventWriteUmacRecv80211Auth(...) 
#define EventEnabledUmacRecv80211Auth(...) 0
#define EventEnabledWMIScanRequest(...) 0
#define EventWriteWMIScanRequest(...) 
#define EventEnabledWMIScanRequestSSID(...) 0
#define EventWriteWMIScanRequestSSID(...) 
#define EventEnabledWMIScanEvent(...) 0
#define EventWriteWMIScanEvent(...) 
#define EventWriteDevicePowerStateTransition(...) 
#define EventWriteDeviceWakeReasonIndication(...) 

#endif /* WINDOWS_ETW_LOGGING */


