
/*
 * Copyright (c) 2011-2012 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 * $ATH_LICENSE_TARGET_C$
 */
#if !defined(_TEST_CMD_PARMS_BIN_TEMPLATE)
#define _TEST_CMD_PARMS_BIN_TEMPLATE

typedef struct _parmBinTemplate {
    A_UINT32 offset;
    A_UINT32 len;
} __ATTRIB_PACK _PARM_BIN_TEMPLATE;

#endif // #if !defined(_TEST_CMD_PARMS_BIN_TEMPLATE)

