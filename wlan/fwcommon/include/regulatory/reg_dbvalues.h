/*
 * Copyright (c) 2012, 2016 - 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * 2012, 2016 Qualcomm Atheros, Inc.
 * 2005-2010 Atheros Corporation.
 */
#ifndef __REG_DBVALUE_H__
#define __REG_DBVALUE_H__

#if defined(TEST_FRAMEWORK) // {
#ifdef CTRY_ALBANIA
#undef CTRY_ALBANIA
#endif
#ifdef CTRY_ALGERIA
#undef CTRY_ALGERIA
#endif
#ifdef CTRY_ARGENTINA
#undef CTRY_ARGENTINA
#endif
#ifdef CTRY_ARMENIA
#undef CTRY_ARMENIA
#endif
#ifdef CTRY_ARUBA
#undef CTRY_ARUBA
#endif
#ifdef CTRY_AUSTRALIA
#undef CTRY_AUSTRALIA
#endif
#ifdef CTRY_AUSTRALIA_AP
#undef CTRY_AUSTRALIA_AP
#endif
#ifdef CTRY_AUSTRIA
#undef CTRY_AUSTRIA
#endif
#ifdef CTRY_AZERBAIJAN
#undef CTRY_AZERBAIJAN
#endif
#ifdef CTRY_BAHRAIN
#undef CTRY_BAHRAIN
#endif
#ifdef CTRY_BANGLADESH
#undef CTRY_BANGLADESH
#endif
#ifdef CTRY_BARBADOS
#undef CTRY_BARBADOS
#endif
#ifdef CTRY_BELARUS
#undef CTRY_BELARUS
#endif
#ifdef CTRY_BELGIUM
#undef CTRY_BELGIUM
#endif
#ifdef CTRY_BELIZE
#undef CTRY_BELIZE
#endif
#ifdef CTRY_BOLIVIA
#undef CTRY_BOLIVIA
#endif
#ifdef CTRY_BOSNIA_HERZEGOWANIA
#undef CTRY_BOSNIA_HERZEGOWANIA
#endif
#ifdef CTRY_BRAZIL
#undef CTRY_BRAZIL
#endif
#ifdef CTRY_BRUNEI_DARUSSALAM
#undef CTRY_BRUNEI_DARUSSALAM
#endif
#ifdef CTRY_BULGARIA
#undef CTRY_BULGARIA
#endif
#ifdef CTRY_CAMBODIA
#undef CTRY_CAMBODIA
#endif
#ifdef CTRY_CANADA
#undef CTRY_CANADA
#endif
#ifdef CTRY_CANADA_AP
#undef CTRY_CANADA_AP
#endif
#ifdef CTRY_CHILE
#undef CTRY_CHILE
#endif
#ifdef CTRY_CHINA
#undef CTRY_CHINA
#endif
#ifdef CTRY_COLOMBIA
#undef CTRY_COLOMBIA
#endif
#ifdef CTRY_COSTA_RICA
#undef CTRY_COSTA_RICA
#endif
#ifdef CTRY_CROATIA
#undef CTRY_CROATIA
#endif
#ifdef CTRY_CYPRUS
#undef CTRY_CYPRUS
#endif
#ifdef CTRY_CZECH
#undef CTRY_CZECH
#endif
#ifdef CTRY_DENMARK
#undef CTRY_DENMARK
#endif
#ifdef CTRY_DOMINICAN_REPUBLIC
#undef CTRY_DOMINICAN_REPUBLIC
#endif
#ifdef CTRY_ECUADOR
#undef CTRY_ECUADOR
#endif
#ifdef CTRY_EGYPT
#undef CTRY_EGYPT
#endif
#ifdef CTRY_EL_SALVADOR
#undef CTRY_EL_SALVADOR
#endif
#ifdef CTRY_ESTONIA
#undef CTRY_ESTONIA
#endif
#ifdef CTRY_FAEROE_ISLANDS
#undef CTRY_FAEROE_ISLANDS
#endif
#ifdef CTRY_FINLAND
#undef CTRY_FINLAND
#endif
#ifdef CTRY_FRANCE
#undef CTRY_FRANCE
#endif
#ifdef CTRY_FRANCE2
#undef CTRY_FRANCE2
#endif
#ifdef CTRY_GEORGIA
#undef CTRY_GEORGIA
#endif
#ifdef CTRY_GERMANY
#undef CTRY_GERMANY
#endif
#ifdef CTRY_GREECE
#undef CTRY_GREECE
#endif
#ifdef CTRY_GREENLAND
#undef CTRY_GREENLAND
#endif
#ifdef CTRY_GRENADA
#undef CTRY_GRENADA
#endif
#ifdef CTRY_GUAM
#undef CTRY_GUAM
#endif
#ifdef CTRY_GUATEMALA
#undef CTRY_GUATEMALA
#endif
#ifdef CTRY_HAITI
#undef CTRY_HAITI
#endif
#ifdef CTRY_HONDURAS
#undef CTRY_HONDURAS
#endif
#ifdef CTRY_HONG_KONG
#undef CTRY_HONG_KONG
#endif
#ifdef CTRY_HUNGARY
#undef CTRY_HUNGARY
#endif
#ifdef CTRY_ICELAND
#undef CTRY_ICELAND
#endif
#ifdef CTRY_INDIA
#undef CTRY_INDIA
#endif
#ifdef CTRY_INDONESIA
#undef CTRY_INDONESIA
#endif
#ifdef CTRY_IRAQ
#undef CTRY_IRAQ
#endif
#ifdef CTRY_IRELAND
#undef CTRY_IRELAND
#endif
#ifdef CTRY_ISRAEL
#undef CTRY_ISRAEL
#endif
#ifdef CTRY_ITALY
#undef CTRY_ITALY
#endif
#ifdef CTRY_JAMAICA
#undef CTRY_JAMAICA
#endif
#ifdef CTRY_JAPAN
#undef CTRY_JAPAN
#endif
#ifdef CTRY_JAPAN1
#undef CTRY_JAPAN1
#endif
#ifdef CTRY_JAPAN2
#undef CTRY_JAPAN2
#endif
#ifdef CTRY_JAPAN3
#undef CTRY_JAPAN3
#endif
#ifdef CTRY_JAPAN4
#undef CTRY_JAPAN4
#endif
#ifdef CTRY_JAPAN5
#undef CTRY_JAPAN5
#endif
#ifdef CTRY_JAPAN6
#undef CTRY_JAPAN6
#endif
#ifdef CTRY_JORDAN
#undef CTRY_JORDAN
#endif
#ifdef CTRY_KAZAKHSTAN
#undef CTRY_KAZAKHSTAN
#endif
#ifdef CTRY_KENYA
#undef CTRY_KENYA
#endif
#ifdef CTRY_KOREA_ROC
#undef CTRY_KOREA_ROC
#endif
#ifdef CTRY_KOREA_ROC2
#undef CTRY_KOREA_ROC2
#endif
#ifdef CTRY_KOREA_ROC3
#undef CTRY_KOREA_ROC3
#endif
#ifdef CTRY_KUWAIT
#undef CTRY_KUWAIT
#endif
#ifdef CTRY_LATVIA
#undef CTRY_LATVIA
#endif
#ifdef CTRY_LEBANON
#undef CTRY_LEBANON
#endif
#ifdef CTRY_LIBYA
#undef CTRY_LIBYA
#endif
#ifdef CTRY_LIECHTENSTEIN
#undef CTRY_LIECHTENSTEIN
#endif
#ifdef CTRY_LITHUANIA
#undef CTRY_LITHUANIA
#endif
#ifdef CTRY_LUXEMBOURG
#undef CTRY_LUXEMBOURG
#endif
#ifdef CTRY_MACAU
#undef CTRY_MACAU
#endif
#ifdef CTRY_MACEDONIA
#undef CTRY_MACEDONIA
#endif
#ifdef CTRY_MALAYSIA
#undef CTRY_MALAYSIA
#endif
#ifdef CTRY_MALTA
#undef CTRY_MALTA
#endif
#ifdef CTRY_MEXICO
#undef CTRY_MEXICO
#endif
#ifdef CTRY_MONACO
#undef CTRY_MONACO
#endif
#ifdef CTRY_MOROCCO
#undef CTRY_MOROCCO
#endif
#ifdef CTRY_NEPAL
#undef CTRY_NEPAL
#endif
#ifdef CTRY_NETHERLANDS
#undef CTRY_NETHERLANDS
#endif
#ifdef CTRY_NETHERLAND_ANTILLES
#undef CTRY_NETHERLAND_ANTILLES
#endif
#ifdef CTRY_NEW_ZEALAND
#undef CTRY_NEW_ZEALAND
#endif
#ifdef CTRY_NICARAGUA
#undef CTRY_NICARAGUA
#endif
#ifdef CTRY_NORWAY
#undef CTRY_NORWAY
#endif
#ifdef CTRY_OMAN
#undef CTRY_OMAN
#endif
#ifdef CTRY_PAKISTAN
#undef CTRY_PAKISTAN
#endif
#ifdef CTRY_PANAMA
#undef CTRY_PANAMA
#endif
#ifdef CTRY_PARAGUAY
#undef CTRY_PARAGUAY
#endif
#ifdef CTRY_PERU
#undef CTRY_PERU
#endif
#ifdef CTRY_PHILIPPINES
#undef CTRY_PHILIPPINES
#endif
#ifdef CTRY_POLAND
#undef CTRY_POLAND
#endif
#ifdef CTRY_PORTUGAL
#undef CTRY_PORTUGAL
#endif
#ifdef CTRY_PUERTO_RICO
#undef CTRY_PUERTO_RICO
#endif
#ifdef CTRY_QATAR
#undef CTRY_QATAR
#endif
#ifdef CTRY_ROMANIA
#undef CTRY_ROMANIA
#endif
#ifdef CTRY_RUSSIA
#undef CTRY_RUSSIA
#endif
#ifdef CTRY_RWANDA
#undef CTRY_RWANDA
#endif
#ifdef CTRY_SAUDI_ARABIA
#undef CTRY_SAUDI_ARABIA
#endif
#ifdef CTRY_MONTENEGRO
#undef CTRY_MONTENEGRO
#endif
#ifdef CTRY_SINGAPORE
#undef CTRY_SINGAPORE
#endif
#ifdef CTRY_SLOVAKIA
#undef CTRY_SLOVAKIA
#endif
#ifdef CTRY_SLOVENIA
#undef CTRY_SLOVENIA
#endif
#ifdef CTRY_SOUTH_AFRICA
#undef CTRY_SOUTH_AFRICA
#endif
#ifdef CTRY_SPAIN
#undef CTRY_SPAIN
#endif
#ifdef CTRY_SRILANKA
#undef CTRY_SRILANKA
#endif
#ifdef CTRY_SWEDEN
#undef CTRY_SWEDEN
#endif
#ifdef CTRY_SWITZERLAND
#undef CTRY_SWITZERLAND
#endif
#ifdef CTRY_TAIWAN
#undef CTRY_TAIWAN
#endif
#ifdef CTRY_THAILAND
#undef CTRY_THAILAND
#endif
#ifdef CTRY_TRINIDAD_Y_TOBAGO
#undef CTRY_TRINIDAD_Y_TOBAGO
#endif
#ifdef CTRY_TUNISIA
#undef CTRY_TUNISIA
#endif
#ifdef CTRY_TURKEY
#undef CTRY_TURKEY
#endif
#ifdef CTRY_UAE
#undef CTRY_UAE
#endif
#ifdef CTRY_UKRAINE
#undef CTRY_UKRAINE
#endif
#ifdef CTRY_UNITED_KINGDOM
#undef CTRY_UNITED_KINGDOM
#endif
#ifdef CTRY_UNITED_STATES
#undef CTRY_UNITED_STATES
#endif
#ifdef CTRY_UNITED_STATES_AP
#undef CTRY_UNITED_STATES_AP
#endif
#ifdef CTRY_UNITED_STATES_PS
#undef CTRY_UNITED_STATES_PS
#endif
#ifdef CTRY_URUGUAY
#undef CTRY_URUGUAY
#endif
#ifdef CTRY_UZBEKISTAN
#undef CTRY_UZBEKISTAN
#endif
#ifdef CTRY_VENEZUELA
#undef CTRY_VENEZUELA
#endif
#ifdef CTRY_VIET_NAM
#undef CTRY_VIET_NAM
#endif
#ifdef CTRY_YEMEN
#undef CTRY_YEMEN
#endif
#ifdef CTRY_ZIMBABWE
#undef CTRY_ZIMBABWE
#endif
/* Newly defined country codes from this line for Halphy Regulatory component */
#ifdef CTRY_AFGHANISTAN
#undef CTRY_AFGHANISTAN
#endif
#ifdef CTRY_AMERICAN_SAMOA
#undef CTRY_AMERICAN_SAMOA
#endif
#ifdef CTRY_ANGUILLA
#undef CTRY_ANGUILLA
#endif
#ifdef CTRY_ARGENTINA2
#undef CTRY_ARGENTINA2
#endif
#ifdef CTRY_BAHAMAS
#undef CTRY_BAHAMAS
#endif
#ifdef CTRY_BELGIUM2
#undef CTRY_BELGIUM2
#endif
#ifdef CTRY_BERMUDA
#undef CTRY_BERMUDA
#endif
#ifdef CTRY_BHUTAN
#undef CTRY_BHUTAN
#endif
#ifdef CTRY_BURKINA_FASO
#undef CTRY_BURKINA_FASO
#endif
#ifdef CTRY_CAYMAN_ISLANDS
#undef CTRY_CAYMAN_ISLANDS
#endif
#ifdef CTRY_CENTRAL_AFRICA_REPUBLIC
#undef CTRY_CENTRAL_AFRICA_REPUBLIC
#endif
#ifdef CTRY_CHAD
#undef CTRY_CHAD
#endif
#ifdef CTRY_CHRISTMAS_ISLAND
#undef CTRY_CHRISTMAS_ISLAND
#endif
#ifdef CTRY_COTE_DIVOIRE
#undef CTRY_COTE_DIVOIRE
#endif
#ifdef CTRY_DOMINICA
#undef CTRY_DOMINICA
#endif
#ifdef CTRY_ETHIOPIA
#undef CTRY_ETHIOPIA
#endif
#ifdef CTRY_FRENCH_GUIANA
#undef CTRY_FRENCH_GUIANA
#endif
#ifdef CTRY_FRENCH_POLYNESIA
#undef CTRY_FRENCH_POLYNESIA
#endif
#ifdef CTRY_GHANA
#undef CTRY_GHANA
#endif
#ifdef CTRY_GIBRALTAR
#undef CTRY_GIBRALTAR
#endif
#ifdef CTRY_GUADELOUPE
#undef CTRY_GUADELOUPE
#endif
#ifdef CTRY_GUYANA
#undef CTRY_GUYANA
#endif
#ifdef CTRY_LESOTHO
#undef CTRY_LESOTHO
#endif
#ifdef CTRY_MALAWI
#undef CTRY_MALAWI
#endif
#ifdef CTRY_MALDIVES
#undef CTRY_MALDIVES
#endif
#ifdef CTRY_MARSHALL_ISLANDS
#undef CTRY_MARSHALL_ISLANDS
#endif
#ifdef CTRY_MARTINIQUE
#undef CTRY_MARTINIQUE
#endif
#ifdef CTRY_MAURITANIA
#undef CTRY_MAURITANIA
#endif
#ifdef CTRY_MAURITIUS
#undef CTRY_MAURITIUS
#endif
#ifdef CTRY_MAYOTTE
#undef CTRY_MAYOTTE
#endif
#ifdef CTRY_MICRONESIA
#undef CTRY_MICRONESIA
#endif
#ifdef CTRY_MOLDOVA
#undef CTRY_MOLDOVA
#endif
#ifdef CTRY_MONGOLIA
#undef CTRY_MONGOLIA
#endif
#ifdef CTRY_MYANMAR
#undef CTRY_MYANMAR
#endif
#ifdef CTRY_NIGERIA
#undef CTRY_NIGERIA
#endif
#ifdef CTRY_NORTHERN_MARIANA_ISLANDS
#undef CTRY_NORTHERN_MARIANA_ISLANDS
#endif
#ifdef CTRY_PALAU
#undef CTRY_PALAU
#endif
#ifdef CTRY_PAPUA_NEW_GUINEA
#undef CTRY_PAPUA_NEW_GUINEA
#endif
#ifdef CTRY_REUNION
#undef CTRY_REUNION
#endif
#ifdef CTRY_SAINT_KITTS_AND_NEVIS
#undef CTRY_SAINT_KITTS_AND_NEVIS
#endif
#ifdef CTRY_SAINT_LUCIA
#undef CTRY_SAINT_LUCIA
#endif
#ifdef CTRY_SAINT_PIERRE_AND_MIQUELON
#undef CTRY_SAINT_PIERRE_AND_MIQUELON
#endif
#ifdef CTRY_SAINT_VINCENT_AND_THE_GRENADIENS
#undef CTRY_SAINT_VINCENT_AND_THE_GRENADIENS
#endif
#ifdef CTRY_SAMOA
#undef CTRY_SAMOA
#endif
#ifdef CTRY_SENEGAL
#undef CTRY_SENEGAL
#endif
#ifdef CTRY_SERBIA
#undef CTRY_SERBIA
#endif
#ifdef CTRY_SURINAME
#undef CTRY_SURINAME
#endif
#ifdef CTRY_TANZANIA
#undef CTRY_TANZANIA
#endif
#ifdef CTRY_TURKS_AND_CAICOS
#undef CTRY_TURKS_AND_CAICOS
#endif
#ifdef CTRY_UGANDA
#undef CTRY_UGANDA
#endif
#ifdef CTRY_UNITED_STATES2
#undef CTRY_UNITED_STATES2
#endif
#ifdef CTRY_VANUATU
#undef CTRY_VANUATU
#endif
#ifdef CTRY_VIRGIN_ISLANDS
#undef CTRY_VIRGIN_ISLANDS
#endif
#ifdef CTRY_WALLIS_AND_FUTUNA
#undef CTRY_WALLIS_AND_FUTUNA
#endif
#ifdef CTRY_JAPAN7
#undef CTRY_JAPAN7
#endif
#ifdef CTRY_JAPAN8
#undef CTRY_JAPAN8
#endif
#ifdef CTRY_JAPAN9
#undef CTRY_JAPAN9
#endif
#ifdef CTRY_JAPAN10
#undef CTRY_JAPAN10
#endif
#ifdef CTRY_JAPAN11
#undef CTRY_JAPAN11
#endif
#ifdef CTRY_JAPAN12
#undef CTRY_JAPAN12
#endif
#ifdef CTRY_JAPAN13
#undef CTRY_JAPAN13
#endif
#ifdef CTRY_JAPAN14
#undef CTRY_JAPAN14
#endif
#ifdef CTRY_JAPAN15
#undef CTRY_JAPAN15
#endif
#ifdef CTRY_JAPAN25
#undef CTRY_JAPAN25
#endif
#ifdef CTRY_JAPAN26
#undef CTRY_JAPAN26
#endif
#ifdef CTRY_JAPAN27
#undef CTRY_JAPAN27
#endif
#ifdef CTRY_JAPAN28
#undef CTRY_JAPAN28
#endif
#ifdef CTRY_JAPAN29
#undef CTRY_JAPAN29
#endif
#ifdef CTRY_JAPAN34
#undef CTRY_JAPAN34
#endif
#ifdef CTRY_JAPAN35
#undef CTRY_JAPAN35
#endif
#ifdef CTRY_JAPAN36
#undef CTRY_JAPAN36
#endif
#ifdef CTRY_JAPAN37
#undef CTRY_JAPAN37
#endif
#ifdef CTRY_JAPAN38
#undef CTRY_JAPAN38
#endif
#ifdef CTRY_JAPAN39
#undef CTRY_JAPAN39
#endif
#ifdef CTRY_JAPAN40
#undef CTRY_JAPAN40
#endif
#ifdef CTRY_JAPAN41
#undef CTRY_JAPAN41
#endif
#ifdef CTRY_JAPAN42
#undef CTRY_JAPAN42
#endif
#ifdef CTRY_JAPAN43
#undef CTRY_JAPAN43
#endif
#ifdef CTRY_JAPAN44
#undef CTRY_JAPAN44
#endif
#ifdef CTRY_JAPAN45
#undef CTRY_JAPAN45
#endif
#ifdef CTRY_JAPAN46
#undef CTRY_JAPAN46
#endif
#ifdef CTRY_JAPAN47
#undef CTRY_JAPAN47
#endif
#ifdef CTRY_JAPAN48
#undef CTRY_JAPAN48
#endif
#ifdef CTRY_JAPAN49
#undef CTRY_JAPAN49
#endif
#ifdef CTRY_JAPAN55
#undef CTRY_JAPAN55
#endif
#ifdef CTRY_JAPAN56
#undef CTRY_JAPAN56
#endif
#ifdef CTRY_XA
#undef CTRY_XA
#endif
#endif // }

/*
 * Numbering from ISO 3166
 */
enum CountryCode {
    CTRY_AFGHANISTAN                            = 4,
    CTRY_ALAND_ISLANDS                          = 248,
    CTRY_ALBANIA                                = 8,       /* Albania */
    CTRY_ALGERIA                                = 12,      /* Algeria */
    CTRY_AMERICAN_SAMOA                         = 16,
    CTRY_ANDORRA                                = 20,
	CTRY_ANGUILLA                               = 660,
    CTRY_ANTIGUA_AND_BARBUDA                    = 28,
    CTRY_ARGENTINA                              = 32,      /* Argentina */
    CTRY_ARGENTINA2                             = 5003,
    CTRY_ARMENIA                                = 51,      /* Armenia */
    CTRY_ARUBA                                  = 533,     /* Aruba */
    CTRY_AUSTRALIA                              = 36,      /* Australia (for STA) */
    CTRY_AUSTRALIA_AP                           = 5000,    /* Australia (for AP) */
    CTRY_AUSTRIA                                = 40,      /* Austria */
    CTRY_AZERBAIJAN                             = 31,      /* Azerbaijan */
    CTRY_BAHAMAS                                = 44,
    CTRY_BAHRAIN                                = 48,      /* Bahrain */
    CTRY_BANGLADESH                             = 50,      /* Bangladesh */
    CTRY_BARBADOS                               = 52,      /* Barbados */
    CTRY_BELARUS                                = 112,     /* Belarus */
    CTRY_BELGIUM                                = 56,      /* Belgium */
    CTRY_BELGIUM2                               = 4101,
    CTRY_BELIZE                                 = 84,      /* Belize */
    CTRY_BERMUDA                                = 60,
	CTRY_BHUTAN                                 = 64,
    CTRY_BOLIVIA                                = 68,      /* Bolivia */
    CTRY_BOSNIA_HERZEGOWANIA                    = 70,      /* Bosnia & Herzegowania */
    CTRY_BRAZIL                                 = 76,      /* Brazil */
    CTRY_BRUNEI_DARUSSALAM                      = 96,      /* Brunei Darussalam */
    CTRY_BULGARIA                               = 100,     /* Bulgaria */
    CTRY_BURKINA_FASO                           = 854,
    CTRY_CAMBODIA                               = 116,     /* Cambodia */
    CTRY_CAMEROON                               = 120,
    CTRY_CANADA                                 = 124,     /* Canada (for STA) */
    CTRY_CANADA_AP                              = 5001,    /* Canada (for AP) */
	CTRY_CAYMAN_ISLANDS                         = 136,
	CTRY_CENTRAL_AFRICA_REPUBLIC                = 140,
	CTRY_CHAD                                   = 148,
    CTRY_CHILE                                  = 152,     /* Chile */
    CTRY_CHINA                                  = 156,     /* People's Republic of China */
    CTRY_CHRISTMAS_ISLAND                       = 162,
    CTRY_COLOMBIA                               = 170,     /* Colombia */
    CTRY_COOK_ISLANDS                           = 184,
    CTRY_COSTA_RICA                             = 188,     /* Costa Rica */
    CTRY_COTE_DIVOIRE                           = 384,
    CTRY_CROATIA                                = 191,     /* Croatia */
    CTRY_CYPRUS                                 = 196,
    CTRY_CZECH                                  = 203,     /* Czech Republic */
    CTRY_DENMARK                                = 208,     /* Denmark */
    CTRY_DOMINICA                               = 212,
    CTRY_DOMINICAN_REPUBLIC                     = 214,     /* Dominican Republic */
    CTRY_ECUADOR                                = 218,     /* Ecuador */
    CTRY_EGYPT                                  = 818,     /* Egypt */
    CTRY_EL_SALVADOR                            = 222,     /* El Salvador */
    CTRY_ESTONIA                                = 233,     /* Estonia */
    CTRY_ETHIOPIA                               = 231,
    CTRY_FALKLAND_ISLANDS                       = 238,
    CTRY_FAROE_ISLANDS                          = 234,
    CTRY_FAEROE_ISLANDS                         = 234,     /* Faeroe Islands */
    CTRY_FINLAND                                = 246,     /* Finland */
    CTRY_FRANCE                                 = 250,     /* France */
    CTRY_FRENCH_GUIANA                          = 254,
	CTRY_FRENCH_POLYNESIA                       = 258,
    CTRY_FRENCH_SOUTHERN_TERRITORIES            = 260,
    CTRY_FRANCE2                                = 255,     /* France2 */
    CTRY_GEORGIA                                = 268,     /* Georgia */
    CTRY_GERMANY                                = 276,     /* Germany */
    CTRY_GHANA                                  = 288,
    CTRY_GIBRALTAR                              = 292,
    CTRY_GREECE                                 = 300,     /* Greece */
    CTRY_GREENLAND                              = 304,     /* Greenland */
    CTRY_GRENADA                                = 308,     /* Grenada */
    CTRY_GUADELOUPE                             = 312,
    CTRY_GUAM                                   = 316,     /* Guam */
    CTRY_GUATEMALA                              = 320,     /* Guatemala */
    CTRY_GUERNSEY                               = 831,
    CTRY_GUYANA                                 = 328,
    CTRY_HAITI                                  = 332,     /* Haiti */
    CTRY_HEARD_ISLAND_AND_MCDONALD_ISLANDS      = 334,
    CTRY_HOLY_SEE                               = 336,
    CTRY_HONDURAS                               = 340,     /* Honduras */
    CTRY_HONG_KONG                              = 344,     /* Hong Kong S.A.R., P.R.C. */
    CTRY_HUNGARY                                = 348,     /* Hungary */
    CTRY_ICELAND                                = 352,     /* Iceland */
    CTRY_INDIA                                  = 356,     /* India */
    CTRY_INDIA2                                 = 5006,
    CTRY_INDONESIA                              = 360,     /* Indonesia */
    CTRY_IRAQ                                   = 368,     /* Iraq */
    CTRY_IRELAND                                = 372,     /* Ireland */
    CTRY_ISLE_OF_MAN                            = 833,
    CTRY_ISRAEL                                 = 376,     /* Israel */
    CTRY_ITALY                                  = 380,     /* Italy */
    CTRY_JAMAICA                                = 388,     /* Jamaica */
    CTRY_JERSEY                                 = 832,
    CTRY_JAPAN                                  = 392,     /* Japan */
    CTRY_JAPAN1                                 = 393,     /* Japan (JP1) */
    CTRY_JAPAN2                                 = 394,     /* Japan (JP0) */
    CTRY_JAPAN3                                 = 395,     /* Japan (JP1-1) */
    CTRY_JAPAN4                                 = 396,     /* Japan (JE1) */
    CTRY_JAPAN5                                 = 397,     /* Japan (JE2) */
    CTRY_JAPAN6                                 = 399,     /* Japan (JP6) */
    CTRY_JAPAN7                                 = 4007,
	CTRY_JAPAN8                                 = 4008,
	CTRY_JAPAN9                                 = 4009,
	CTRY_JAPAN10                                = 4010,
	CTRY_JAPAN11                                = 4011,
	CTRY_JAPAN12                                = 4012,
	CTRY_JAPAN13                                = 4013,
	CTRY_JAPAN14                                = 4014,
	CTRY_JAPAN15                                = 4015,
	CTRY_JAPAN25                                = 4025,
	CTRY_JAPAN26                                = 4026,
	CTRY_JAPAN27                                = 4027,
	CTRY_JAPAN28                                = 4028,
	CTRY_JAPAN29                                = 4029,
	CTRY_JAPAN34                                = 4034,
	CTRY_JAPAN35                                = 4035,
	CTRY_JAPAN36                                = 4036,
	CTRY_JAPAN37                                = 4037,
	CTRY_JAPAN38                                = 4038,
	CTRY_JAPAN39                                = 4039,
	CTRY_JAPAN40                                = 4040,
	CTRY_JAPAN41                                = 4041,
	CTRY_JAPAN42                                = 4042,
	CTRY_JAPAN43                                = 4043,
	CTRY_JAPAN44                                = 4044,
	CTRY_JAPAN45                                = 4045,
	CTRY_JAPAN46                                = 4046,
	CTRY_JAPAN47                                = 4047,
	CTRY_JAPAN48                                = 4048,
	CTRY_JAPAN49                                = 4049,
	CTRY_JAPAN55                                = 4055,
	CTRY_JAPAN56                                = 4056,
    CTRY_JAPAN60                                = 4060,
    CTRY_JORDAN                                 = 400,     /* Jordan */
    CTRY_KAZAKHSTAN                             = 398,     /* Kazakhstan */
    CTRY_KENYA                                  = 404,     /* Kenya */
    CTRY_KOREA_ROC                              = 410,     /* South Korea (for STA) */
    CTRY_KOREA_ROC2                             = 411,     /* South Korea */
    CTRY_KOREA_ROC3                             = 412,     /* South Korea (for AP) */
    CTRY_KUWAIT                                 = 414,     /* Kuwait */
    CTRY_LATVIA                                 = 428,     /* Latvia */
    CTRY_LEBANON                                = 422,     /* Lebanon */
    CTRY_LESOTHO                                = 426,
    CTRY_LIBYA                                  = 434,     /* Libya */
    CTRY_LIECHTENSTEIN                          = 438,     /* Liechtenstein */
    CTRY_LITHUANIA                              = 440,     /* Lithuania */
    CTRY_LUXEMBOURG                             = 442,     /* Luxembourg */
    CTRY_MACAU                                  = 446,     /* Macau */
    CTRY_MACEDONIA                              = 807,     /* the Former Yugoslav Republic of Macedonia */
    CTRY_MALAWI                                 = 454,
    CTRY_MALAYSIA                               = 458,     /* Malaysia */
    CTRY_MALDIVES                               = 462,
    CTRY_MALTA                                  = 470,     /* Malta */
	CTRY_MARSHALL_ISLANDS                       = 584,
	CTRY_MARTINIQUE                             = 474,
	CTRY_MAURITANIA                             = 478,
	CTRY_MAURITIUS                              = 480,
	CTRY_MAYOTTE                                = 175,
    CTRY_MEXICO                                 = 484,     /* Mexico */
    CTRY_MICRONESIA                             = 583,
	CTRY_MOLDOVA                                = 498,
    CTRY_MONACO                                 = 492,     /* Principality of Monaco */
	CTRY_MONGOLIA                               = 496,
	CTRY_MONTENEGRO                             = 499,
    CTRY_MONTSERRAT                             = 500,
    CTRY_MOROCCO                                = 504,     /* Morocco */
    CTRY_MYANMAR                                = 104,
	CTRY_NAMIBIA                                = 516,
    CTRY_NEPAL                                  = 524,     /* Nepal */
    CTRY_NETHERLANDS                            = 528,     /* Netherlands */
    CTRY_NETHERLAND_ANTILLES                    = 530,     /* Netherlands-Antilles */
    CTRY_NEW_CALEDONIA                          = 540,
    CTRY_NEW_ZEALAND                            = 554,     /* New Zealand */
	CTRY_NIGERIA                                = 566,
	CTRY_NORTHERN_MARIANA_ISLANDS               = 580,
    CTRY_NICARAGUA                              = 558,     /* Nicaragua */
    CTRY_NIUE                                   = 570,
    CTRY_NORFOLK_ISLAND                         = 574,
    CTRY_NORWAY                                 = 578,     /* Norway */
    CTRY_OMAN                                   = 512,     /* Oman */
    CTRY_PAKISTAN                               = 586,     /* Islamic Republic of Pakistan */
	CTRY_PALAU                                  = 585,
    CTRY_PANAMA                                 = 591,     /* Panama */
	CTRY_PAPUA_NEW_GUINEA                       = 598,
    CTRY_PARAGUAY                               = 600,     /* Paraguay */
    CTRY_PERU                                   = 604,     /* Peru */
    CTRY_PHILIPPINES                            = 608,     /* Republic of the Philippines */
    CTRY_POLAND                                 = 616,     /* Poland */
    CTRY_PORTUGAL                               = 620,     /* Portugal */
    CTRY_PUERTO_RICO                            = 630,     /* Puerto Rico */
    CTRY_QATAR                                  = 634,     /* Qatar */
	CTRY_REUNION                                = 638,
    CTRY_ROMANIA                                = 642,     /* Romania */
    CTRY_RUSSIA                                 = 643,     /* Russia */
    CTRY_RWANDA                                 = 646,     /* Rwanda */
    CTRY_SAINT_BARTHELEMY                       = 652,
    CTRY_SAINT_HELENA_ASCENSION_AND_TRISTAN_DA_CUNHA = 654,
	CTRY_SAINT_KITTS_AND_NEVIS                  = 659,
	CTRY_SAINT_LUCIA                            = 662,
	CTRY_SAINT_MARTIN                           = 663,
	CTRY_SAINT_PIERRE_AND_MIQUELON              = 666,
	CTRY_SAINT_VINCENT_AND_THE_GRENADIENS       = 670,
	CTRY_SAMOA                                  = 882,
    CTRY_SAN_MARINO                             = 674,
    CTRY_SAO_TOME_AND_PRINCIPE                  = 678,
    CTRY_SAUDI_ARABIA                           = 682,     /* Saudi Arabia */
   // CTRY_MONTENEGRO                           = 891,     /* Montenegro */
    CTRY_SENEGAL                                = 686,
	CTRY_SERBIA                                 = 688,
    CTRY_SINGAPORE                              = 702,     /* Singapore */
    CTRY_SINT_MAARTEN                           = 534,
    CTRY_SLOVAKIA                               = 703,     /* Slovak Republic */
    CTRY_SLOVENIA                               = 705,     /* Slovenia */
    CTRY_SOUTH_AFRICA                           = 710,     /* South Africa */
    CTRY_SPAIN                                  = 724,     /* Spain */
	CTRY_SURINAME                               = 740,
    CTRY_SRILANKA                               = 144,     /* Sri Lanka */
    CTRY_SVALBARD_AND_JAN_MAYEN                 = 744,
    CTRY_SWEDEN                                 = 752,     /* Sweden */
    CTRY_SWITZERLAND                            = 756,     /* Switzerland */
    CTRY_TAIWAN                                 = 158,     /* Taiwan */
    CTRY_TANZANIA                               = 834,
    CTRY_THAILAND                               = 764,     /* Thailand */
	CTRY_TOGO                                   = 768,
    CTRY_TRINIDAD_Y_TOBAGO                      = 780,     /* Trinidad y Tobago */
    CTRY_TUNISIA                                = 788,     /* Tunisia */
    CTRY_TURKEY                                 = 792,     /* Turkey */
    CTRY_TURKS_AND_CAICOS                       = 796,
	CTRY_UGANDA                                 = 800,
    CTRY_UAE                                    = 784,     /* U.A.E. */
    CTRY_UKRAINE                                = 804,     /* Ukraine */
    CTRY_UNITED_KINGDOM                         = 826,     /* United Kingdom */
    CTRY_UNITED_STATES                          = 840,     /* United States (for STA) */
    CTRY_UNITED_STATES_MINOR_OUTLYING_ISLANDS   = 581,
    CTRY_UNITED_STATES_AP                       = 841,     /* United States (for AP) */
    CTRY_UNITED_STATES_PS                       = 842,     /* United States - public safety */
    CTRY_UNITED_STATES2                         = 843,     /* United States2 with new country code 843 */
    CTRY_URUGUAY                                = 858,     /* Uruguay */
    CTRY_UZBEKISTAN                             = 860,     /* Uzbekistan */
    CTRY_VANUATU                                = 548,
    CTRY_VENEZUELA                              = 862,     /* Venezuela */
    CTRY_VIET_NAM                               = 704,     /* Viet Nam */
	CTRY_VIRGIN_ISLANDS                         = 850,
    CTRY_VIRGIN_ISLANDS_BRITISH                 = 92,
	CTRY_WALLIS_AND_FUTUNA                      = 876,
	CTRY_XA                                     = 4100,
    CTRY_YEMEN                                  = 887,     /* Yemen */
    CTRY_ZIMBABWE                               = 716,     /* Zimbabwe */
};

#define CTRY_DEBUG      0
#define CTRY_DEFAULT    0x1ff

/*
 * The following regulatory domain definitions are
 * found in the EEPROM. Each regulatory domain
 * can operate in either a 5GHz or 2.4GHz wireless mode or
 * both 5GHz and 2.4GHz wireless modes.
 * In general, the value holds no special
 * meaning and is used to decode into either specific
 * 2.4GHz or 5GHz wireless mode for that particular
 * regulatory domain.
 *
 * Enumerated Regulatory Domain Information 8 bit values indicate that
 * the regdomain is really a pair of unitary regdomains.  12 bit values
 * are the real unitary regdomains and are the only ones which have the
 * frequency bitmasks and flags set.
 */

enum EnumRd {
    NO_ENUMRD   = 0x00,
    NULL1_WORLD = 0x03,     /* For 11b-only countries (no 11a allowed) */
    NULL1_ETSIB = 0x07,     /* Israel */
    NULL1_ETSIC = 0x08,

    FCC1_FCCA   = 0x10,     /* USA */
    FCC1_WORLD  = 0x11,     /* Hong Kong */
    FCC2_FCCA   = 0x20,     /* Canada */
    FCC2_WORLD  = 0x21,     /* Australia & HK */
    FCC2_ETSIC  = 0x22,
    FCC3_FCCA   = 0x3A,     /* USA & Canada w/5470 band, 11h, DFS enabled */
    FCC3_WORLD  = 0x3B,     /* USA & Canada w/5470 band, 11h, DFS enabled */
    FCC3_ETSIC  = 0x3F,
    FCC4_FCCA   = 0x12,     /* FCC public safety plus UNII bands */
    FCC5_FCCA   = 0x13,     /* US with no DFS */
    FCC5_WORLD  = 0x16,     /* US with no DFS */
    FCC6_FCCA   = 0x14,     /* Same as FCC2_FCCA but with 5600-5650MHz channels disabled for US & Canada APs */
    FCC8_FCCA   = 0x16,
    FCC9_FCCA   = 0x17,
    FCC10_FCCA  = 0x18,
	FCC11_WORLD = 0x19,
    FCC6_WORLD  = 0x23,     /* Same as FCC2_FCCA but with 5600-5650MHz channels disabled for Australia APs */
    FCC13_WORLD = 0xE4,
    FCC14_FCCB  = 0xE6,
    FCC15_FCCA  = 0xE7,
    FCC16_FCCA  = 0xE8,
    FCC17_FCCA  = 0xE9,

    ETSI1_WORLD = 0x37,

    ETSI2_WORLD = 0x35,     /* Hungary & others */
    ETSI3_WORLD = 0x36,     /* France & others */
    ETSI4_WORLD = 0x30,
    ETSI4_ETSIC = 0x38,
    ETSI5_WORLD = 0x39,
    ETSI6_WORLD = 0x34,     /* Bulgaria */
    ETSI8_WORLD = 0x3D,     /* Russia */
    ETSI9_WORLD = 0x3E,     /* Ukraine */
    ETSI10_WORLD = 0x24,
    ETSI10_FCCA = 0x25,
    ETSI11_WORLD = 0x26,
    ETSI12_WORLD = 0x28,
    ETSI13_WORLD = 0x27,
    ETSI14_WORLD = 0x29,
    ETSI15_WORLD = 0x31,
    ETSI_RESERVED   = 0x33,     /* Reserved (Do not used) */
    //FRANCE_RES  = 0x31,     /* Legacy France for OEM */

    APL6_WORLD  = 0x5B,     /* Singapore */
    APL4_WORLD  = 0x42,     /* Singapore */
    APL3_FCCA   = 0x50,
    APL_RESERVED    = 0x44,     /* Reserved (Do not used)  */
    APL2_WORLD  = 0x45,     /* Korea */
    APL2_APLC   = 0x46,
    APL3_WORLD  = 0x47,
    APL2_APLD   = 0x49,     /* Korea with 2.3G channels */
    APL2_FCCA   = 0x4D,     /* Specific Mobile Customer */
    APL1_WORLD  = 0x52,     /* Latin America */
    APL1_FCCA   = 0x53,
    APL1_ETSIC  = 0x55,
    APL2_ETSIC  = 0x56,     /* Venezuela */
    APL2_ETSID  = 0x41,     /* Indonesia */
    APL5_WORLD  = 0x58,     /* Chile */
    APL7_FCCA   = 0x5C,
    APL8_WORLD  = 0x5D,
    APL9_WORLD  = 0x5E,
    APL9_MKKC   = 0x48,
    APL9_KRRA   = 0x43,
    APL10_WORLD = 0x5F,     /* Korea 5GHz for STA */
    APL11_FCCA  = 0x4F,
	APL12_WORLD = 0x51,
	APL13_WORLD = 0x5A,
	APL14_WORLD = 0x57,
	APL15_WORLD = 0x59,
    APL16_WORLD = 0x70,
    APL16_ETSIC = 0x6D,
    APL17_ETSIC = 0xE7,     /* Argentina */
    APL17_ETSID = 0xE0,
    APL19_ETSIC = 0x71,
    APL20_WORLD = 0xE5,
    APL23_WORLD = 0xE3,
    APL24_ETSIC = 0xE2,

    MKK3_MKKA   = 0xF0,
	MKK3_MKKB   = 0x80,
	MKK3_MKKC   = 0x82,
	MKK3_FCCA   = 0xF2,
	MKK4_MKKA   = 0xF3,
	MKK4_MKKB   = 0x83,
	MKK4_MKKC   = 0x85,
	MKK4_FCCA   = 0xF5,
    MKK5_MKKA   = 0x99, /* This is a temporary value. MG and DQ have to give official one */
    MKK5_FCCA   = 0x9A, /* This is a temporary value. MG and DQ have to give official one */
    MKK5_MKKB   = 0x86,
    MKK5_MKKC   = 0x88,
    MKK6_MKKC   = 0x89,
    MKK9_MKKA   = 0xF6,
	MKK9_MKKC   = 0xFE,
	MKK9_FCCA   = 0xFC,
	MKK10_MKKA  = 0xF7,
	MKK10_MKKC  = 0xD2,
	MKK10_FCCA  = 0xD0,
    MKK11_MKKA  = 0xD4,
    MKK11_FCCA  = 0xD5,
    MKK11_MKKC  = 0xD7,
    MKK16_MKKC  = 0xDF,
    MKK17_MKKC  = 0xE1,

    /*
     * World mode SKUs
     */
    WOR0_WORLD  = 0x60,     /* World0 (WO0 SKU) */
    WOR1_WORLD  = 0x61,     /* World1 (WO1 SKU) */
    WOR2_WORLD  = 0x62,     /* World2 (WO2 SKU) */
    WOR3_WORLD  = 0x63,     /* World3 (WO3 SKU) */
    WOR4_WORLD  = 0x64,     /* World4 (WO4 SKU) */
    WOR5_ETSIC  = 0x65,     /* World5 (WO5 SKU) */

    WOR01_WORLD = 0x66,     /* World0-1 (WW0-1 SKU) */
    WOR02_WORLD = 0x67,     /* World0-2 (WW0-2 SKU) */
    EU1_WORLD   = 0x68,     /* Same as World0-2 (WW0-2 SKU), except active scan ch1-13. No ch14 */

    WOR9_WORLD  = 0x69,     /* World9 (WO9 SKU) */
    WORA_WORLD  = 0x6A,     /* WorldA (WOA SKU) */
    WORB_WORLD  = 0x6B,     /* WorldB (WOA SKU) */
    WORC_WORLD  = 0x6C,     /* WorldC (WOA SKU) */

    /*
     * Regulator domains ending in a number (e.g. APL1,
     * MK1, ETSI4, etc) apply to 5GHz channel and power
     * information.  Regulator domains ending in a letter
     * (e.g. APLA, FCCA, etc) apply to 2.4GHz channel and
     * power information.
     */
    APL1        = 0x0150,   /* LAT & Asia */
    APL2        = 0x0250,   /* LAT & Asia */
    APL3        = 0x0350,   /* Taiwan */
    APL4        = 0x0450,   /* Jordan */
    APL5        = 0x0550,   /* Chile */
    APL6        = 0x0650,   /* Singapore */
    APL7        = 0x0750,   /* Taiwan */
    APL8        = 0x0850,   /* Malaysia */
    APL9        = 0x0950,   /* Korea */
    APL10       = 0x1050,   /* Korea 5GHz */
    APL12       = 0x1051,
	APL14       = 0x1052,

    ETSI1       = 0x0130,   /* Europe & others */
    ETSI2       = 0x0230,   /* Europe & others */
    ETSI3       = 0x0330,   /* Europe & others */
    ETSI4       = 0x0430,   /* Europe & others */
    ETSI5       = 0x0530,   /* Europe & others */
    ETSI6       = 0x0630,   /* Europe & others */
    ETSI8       = 0x0830,   /* Russia - only by APs */
    ETSI9       = 0x0930,   /* Ukraine - only by APs */
    ETSIB       = 0x0B30,   /* Israel */
    ETSIC       = 0x0C30,   /* Latin America */

    FCC1        = 0x0110,   /* US & others */
    FCC2        = 0x0120,   /* Canada, Australia & New Zealand */
    FCC3        = 0x0160,   /* US w/new middle band & DFS */
    FCC4        = 0x0165,
    FCC5        = 0x0180,
    FCC6        = 0x0610,
    FCC8        = 0x0611,
    FCCA        = 0x0A10,

    APLD        = 0x0D50,   /* South Korea */

    MKK1        = 0x0140,   /* Japan */
    MKK2        = 0x0240,   /* Japan Extended */
    MKK3        = 0x0340,   /* Japan new 5GHz */
    MKK4        = 0x0440,   /* Japan new 5GHz */
    MKK5        = 0x0540,   /* Japan new 5GHz */
    MKK6        = 0x0640,   /* Japan new 5GHz */
    MKK7        = 0x0740,   /* Japan new 5GHz */
    MKK8        = 0x0840,   /* Japan new 5GHz */
    MKK9        = 0x0940,   /* Japan new 5GHz */
    MKK10       = 0x1040,   /* Japan new 5GHz */
    MKK11       = 0x1140,   /* Japan new 5GHz */
    MKK12       = 0x1240,   /* Japan new 5GHz */

    MKKA        = 0x0A40,   /* Japan */
    MKKC        = 0x0A50,

    NULL1       = 0x0198,
    WORLD       = 0x0199,
    WORLD_2G_1  = 0x019A,
	WORLD_2G_2  = 0x019B,
	WORLD_2G_3  = 0x019C,
    WORLD_5G_1  = 0x019D,
	WORLD_5G_2  = 0x019E,
    DEBUG_REG_DMN   = 0x01ff,
    UNINIT_REG_DMN  = 0x0fff
};

enum {                  /* conformance test limits */
    FCC          = 0x10,
    MKK          = 0x40,
    ETSI         = 0x30,
    NO_CTL       = 0xff,
    CTL_11B      = 0x01,
    CTL_11G      = 0x02,
    CTL_KOR      = 0x50,
    CTL_CHN      = 0x60,
    CTL_USER_DEF = 0x70,
};


/*
 * The following are flags for different requirements per reg domain.
 * These requirements are either inhereted from the reg domain pair or
 * from the unitary reg domain if the reg domain pair flags value is
 * 0
 */

enum {
    NO_REQ              = 0x00,
    DISALLOW_ADHOC_11A  = 0x01,
    ADHOC_PER_11D       = 0x02,
    ADHOC_NO_11A        = 0x04,
    DISALLOW_ADHOC_11G  = 0x08
};




/*
 * The following describe the bit masks for different passive scan
 * capability/requirements per regdomain.
 */
#define NO_PSCAN        0x00000000
#define PSCAN_FCC       0x00000001
#define PSCAN_ETSI      0x00000002
#define PSCAN_MKK       0x00000004
#define PSCAN_ETSIB     0x00000008
#define PSCAN_ETSIC     0x00000010
#define PSCAN_WWR       0x00000020
#define PSCAN_DEFER     0xFFFFFFFF

/* Bit masks for DFS per regdomain */

enum {
    NO_DFS   = 0x00,
    DFS_FCC3 = 0x01,
    DFS_ETSI = 0x02,
    DFS_MKK  = 0x04
};


#define DEF_REGDMN      FCC1_FCCA

/*
 * The following table is the master list for all different freqeuncy
 * bands with the complete matrix of all possible flags and settings
 * for each band if it is used in ANY reg domain.
 *
 * The table of frequency bands is indexed by a bitmask.  The ordering
 * must be consistent with the enum below.  When adding a new
 * frequency band, be sure to match the location in the enum with the
 * comments
 */

/*
 * These frequency values are as per channel tags and regulatory domain
 * info. Please update them as database is updated.
 */
#define A_FREQ_MIN              4920
#define A_FREQ_MAX              5925

#define A_CHAN0_FREQ            5000
#define A_CHAN_MAX              ((A_FREQ_MAX - A_CHAN0_FREQ)/5)

#define BG_FREQ_MIN             2412
#define BG_FREQ_MAX             2484

#define BG_CHAN0_FREQ           2407
#define BG_CHAN_MIN             ((BG_FREQ_MIN - BG_CHAN0_FREQ)/5)
#define BG_CHAN_MAX             14      /* corresponding to 2484 MHz */

#define A_20MHZ_BAND_FREQ_MAX   5000


/*
 * 5GHz 11A channel tags
 */

enum {
    F1_4920_4980,
    F1_5040_5080,

    F1_5120_5240,

    F1_5180_5240,
    F2_5180_5240,
    F3_5180_5240,
    F4_5180_5240,
    F5_5180_5240,
    F6_5180_5240,
    F7_5180_5240,

    F1_5260_5280,

    F1_5260_5320,
    F2_5260_5320,
    F3_5260_5320,
    F4_5260_5320,
    F5_5260_5320,
    F6_5260_5320,

    F1_5260_5700,

    F1_5280_5320,

    F1_5500_5580,

    F1_5500_5620,

    F1_5500_5660,

    F1_5500_5700,
    F2_5500_5700,
    F3_5500_5700,
    F4_5500_5700,
    F5_5500_5700,
    F6_5500_5700,
    F7_5500_5700,

    F1_5660_5700,
    F2_5660_5700,

    F1_5745_5805,
    F2_5745_5805,
    F3_5745_5805,

    F1_5745_5825,
    F2_5745_5825,
    F3_5745_5825,
    F4_5745_5825,
    F5_5745_5825,

    W1_4920_4980,
    W1_5040_5080,
    W1_5170_5230,
    W1_5180_5240,
    W1_5260_5320,
    W1_5745_5825,
    W1_5500_5700,
};


/* 2.4 GHz table - for 11b and 11g info */
enum {
    BG1_2312_2372,
    BG2_2312_2372,

    BG1_2412_2472,
    BG2_2412_2472,
    BG3_2412_2472,
    BG4_2412_2472,

    BG1_2412_2462,
    BG2_2412_2462,

    BG1_2432_2442,

    BG1_2457_2472,

    BG1_2467_2472,

    BG1_2484_2484, /* No G */
    BG2_2484_2484, /* No G */

    BG1_2512_2732,

    WBG1_2312_2372,
    WBG1_2412_2412,
    WBG1_2417_2432,
    WBG1_2437_2442,
    WBG1_2447_2457,
    WBG1_2462_2462,
    WBG1_2467_2467,
    WBG2_2467_2467,
    WBG1_2472_2472,
    WBG2_2472_2472,
    WBG1_2484_2484, /* No G */
    WBG2_2484_2484, /* No G */
};

#endif /* __REG_DBVALUE_H__ */
