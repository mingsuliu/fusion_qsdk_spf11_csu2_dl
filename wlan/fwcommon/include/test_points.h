
/*
 * Copyright (c) 2011-2012 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 * $ATH_LICENSE_TARGET_C$
 */
#ifndef _TEST_POINTS_H_
#define _TEST_POINTS_H_

#define TP_CHANGE_CHANNEL   0
#define TP_LOCAL_SEND       1






#if ATH_TARGET

#define TEST_POINT(id, num_args, ...) \
    DBGLOG_RECORD_LOG(WLAN_MODULE_TEST, -1, (id), DBGLOG_INFO, num_args, __VA_ARGS__)

#endif

#endif /* !_TEST_POINTS_H_ */
