/*
 * Copyright (c) 2012,2015,2017 The Linux Foundation. All rights reserved.
 *
 * Previously licensed under the ISC license by Qualcomm Atheros, Inc.
 *
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * This file was originally distributed by Qualcomm Atheros, Inc.
 * under proprietary terms before Copyright ownership was assigned
 * to the Linux Foundation.
 */

#ifndef _PKTLOG_H_IF_
#define _PKTLOG_H_IF_

#include <a_types.h>    /* A_UINT32 */
#include <a_osapi.h>    /* PREPACK, POSTPACK */

/* PKTLOG_MAGIC_NUM_LEGACY:
 * This magic number indicates that the accompanying version number is
 * primarily informational, and only serves to distinguish between
 * architectural levels of the target generating the pktlog trace.
 */
#define PKTLOG_MAGIC_NUM_LEGACY 7735225

/* PKTLOG_MAGIC_NUM_VERSION_IS_CHECKSUM:
 * This magic number indicates that the accompanying version number
 * stores a checksum covering the definitions of the enums and structs
 * used within pktlog traces.
 * This checksum accounts not only for architectural-level differences
 * within the pktlog struct+enum definitions, but also the chip-version,
 * SW image, and FW revision levels of changes to the pktlog defs.
 */
#define PKTLOG_MAGIC_NUM_VERSION_IS_CHECKSUM 2453506

struct ath_pktlog_bufhdr {
    /* magic_num:
     * Used by post processing scripts.
     * Refer to PKTLOG_MAGIC_NUM_xxx defs.
     */
    A_UINT32 magic_num;
    /* version:
     * The interpretation of the version field varies, based on the
     * value specified in the magic_num field.
     * Refer to the PKTLOG_MAGIC_NUM_xxx defs.
     */
    A_UINT32 version;
};

/* DWORD flags__missed_cnt */
#define PKTLOG_HDR_FLAGS_M             0x0000FFFF
#define PKTLOG_HDR_FLAGS_S             0
#define PKTLOG_HDR_MISSED_CNT_M        0xFFFF0000
#define PKTLOG_HDR_MISSED_CNT_S        16

#define PKTLOG_HDR_FLAGS_GET(_var) \
    (((_var) & PKTLOG_HDR_FLAGS_M) >> \
     PKTLOG_HDR_FLAGS_S)

#define PKTLOG_HDR_FLAGS_SET(_var, _val) \
     do { \
         ((_var) |= (((_val) << PKTLOG_HDR_FLAGS_S) & PKTLOG_HDR_FLAGS_M)); \
     } while (0)

#define PKTLOG_HDR_MISSED_CNT_GET(_var) \
    (((_var) & PKTLOG_HDR_MISSED_CNT_M) >> \
     PKTLOG_HDR_MISSED_CNT_S)

#define PKTLOG_HDR_MISSED_CNT_SET(_var, _val) \
     do { \
         ((_var) |= (((_val) << PKTLOG_HDR_MISSED_CNT_S) & PKTLOG_HDR_MISSED_CNT_M)); \
     } while (0)

/* DWORD log_type__pdev_id__size */
#define PKTLOG_HDR_LOG_TYPE_M             0x000000FF
#define PKTLOG_HDR_LOG_TYPE_S             0

#define PKTLOG_HDR_PDEV_ID_M              0x00000300
#define PKTLOG_HDR_PDEV_ID_S              8

/* bits 15:10 are reserved */

#define PKTLOG_HDR_SIZE_M                 0xFFFF0000
#define PKTLOG_HDR_SIZE_S                 16

#define PKTLOG_HDR_LOG_TYPE_GET(_var) \
    (((_var) & PKTLOG_HDR_LOG_TYPE_M) >> \
     PKTLOG_HDR_LOG_TYPE_S)

#define PKTLOG_HDR_LOG_TYPE_SET(_var, _val) \
     do { \
         ((_var) |= (((_val) << PKTLOG_HDR_LOG_TYPE_S) & PKTLOG_HDR_LOG_TYPE_M)); \
     } while (0)

#define PKTLOG_HDR_PDEV_ID_GET(_var) \
    (((_var) & PKTLOG_HDR_PDEV_ID_M) >> \
     PKTLOG_HDR_PDEV_ID_S)

#define PKTLOG_HDR_PDEV_ID_SET(_var, _val) \
     do { \
         ((_var) |= (((_val) << PKTLOG_HDR_PDEV_ID_S) & PKTLOG_HDR_PDEV_ID_M)); \
     } while (0)

#define PKTLOG_HDR_SIZE_GET(_var) \
    (((_var) & PKTLOG_HDR_SIZE_M) >> \
     PKTLOG_HDR_SIZE_S)

#define PKTLOG_HDR_SIZE_SET(_var, _val) \
     do { \
         ((_var) |= (((_val) << PKTLOG_HDR_SIZE_S) & PKTLOG_HDR_SIZE_M)); \
     } while (0)

/*
 * NOTE: The below bit-field defs are not portable (because they require
 * little-endian bit-field ordering, but are provided for convenience.
 * The bit-field defs are used only for script parsing, where the endianness
 * is already controlled.
 */
PREPACK struct pktlog_hdr {
    /* BIT [ 15:   0]   :- flags
     * BIT [ 31:  16]   :- missed_cnt
     */
    union {
        A_UINT32 flags__missed_cnt;
        struct {
            A_UINT32 flags:      16,
                     missed_cnt: 16;
        };
    };

    /* BIT [  7:   0]  :- log_type
     * BIT [  9:   8]  :- pdev_id 0 - SOC, 1/2/3  -> PDEV 0/1/2
     * BIT [ 15:  10]  :- reserved
     * BIT [ 31:  16]  :- size
     */
    union {
        A_UINT32 log_type__pdev_id__size;
        struct {
            A_UINT32 log_type:    8,
                     pdev_id:     2,
                     reserved:    6,
                     size:       16;
        };
    };

    A_UINT32 timestamp;
    /* For HK this field is interpreted as follows
     *  b0-b2  : sequence number for the CE fragments (0 is a valid fragment #)
     *  b3     : set 0 if the it is the last fragment, 1 if more fragments coming.
     *  b4-b5  : MAC # (0, 1, 2)
     *  b6-b15 : reserved
     *  b16-b31: Pktlog event specific
     */
    /* pktlog doesn't interpret this field, host tool does */
    A_UINT32 type_specific_data;
} POSTPACK;

#endif /* _PKTLOG_H_IF_ */
