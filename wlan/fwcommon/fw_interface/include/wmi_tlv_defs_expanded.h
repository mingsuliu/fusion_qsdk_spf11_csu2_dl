# 1 "include/wmi_tlv_defs.h"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "include/wmi_tlv_defs.h"
# 92 "include/wmi_tlv_defs.h"
typedef struct {
    void *tlv_ptr;
    A_UINT32 num_elements;
    A_UINT32 buf_is_allocated;
} wmitlv_cmd_param_info;
# 194 "include/wmi_tlv_defs.h"
typedef enum {

    WMITLV_TAG_LAST_RESERVED = 15,
    WMITLV_TAG_FIRST_ARRAY_ENUM,
    WMITLV_TAG_ARRAY_UINT32 = WMITLV_TAG_FIRST_ARRAY_ENUM,
    WMITLV_TAG_ARRAY_BYTE,
    WMITLV_TAG_ARRAY_STRUC,
    WMITLV_TAG_ARRAY_FIXED_STRUC,
    WMITLV_TAG_LAST_ARRAY_ENUM = 31,
    WMITLV_TAG_STRUC_wmi_service_ready_event_fixed_param,
    WMITLV_TAG_STRUC_HAL_REG_CAPABILITIES,
    WMITLV_TAG_STRUC_wlan_host_mem_req,
    WMITLV_TAG_STRUC_wmi_ready_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_tpc_config_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_comb_phyerr_rx_hdr,
    WMITLV_TAG_STRUC_wmi_vdev_start_response_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_stopped_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_install_key_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_sta_kickout_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_mgmt_rx_hdr,
    WMITLV_TAG_STRUC_wmi_tbtt_offset_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tx_delba_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tx_addba_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_event_fixed_param,
    WMITLV_TAG_STRUC_WOW_EVENT_INFO_fixed_param,
    WMITLV_TAG_STRUC_WOW_EVENT_INFO_SECTION_BITMAP,
    WMITLV_TAG_STRUC_wmi_rtt_event_header,
    WMITLV_TAG_STRUC_wmi_rtt_error_report_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_rtt_meas_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_echo_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ftm_intg_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_keepalive_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_gpio_input_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_csa_event_fixed_param,
    WMITLV_TAG_STRUC_WMI_GTK_OFFLOAD_STATUS_EVENT_fixed_param,
    WMITLV_TAG_STRUC_wmi_igtk_info,
    WMITLV_TAG_STRUC_wmi_dcs_interference_event_fixed_param,
    WMITLV_TAG_STRUC_ath_dcs_cw_int,
    WMITLV_TAG_STRUC_wlan_dcs_cw_int =
        WMITLV_TAG_STRUC_ath_dcs_cw_int,
    WMITLV_TAG_STRUC_ath_dcs_wlan_int_stat,
    WMITLV_TAG_STRUC_wlan_dcs_im_tgt_stats_t =
        WMITLV_TAG_STRUC_ath_dcs_wlan_int_stat,
    WMITLV_TAG_STRUC_wmi_wlan_profile_ctx_t,
    WMITLV_TAG_STRUC_wmi_wlan_profile_t,
    WMITLV_TAG_STRUC_wmi_pdev_qvit_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_host_swba_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tim_info,
    WMITLV_TAG_STRUC_wmi_p2p_noa_info,
    WMITLV_TAG_STRUC_wmi_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_avoid_freq_ranges_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_avoid_freq_range_desc,
    WMITLV_TAG_STRUC_wmi_gtk_rekey_fail_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_init_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_resource_config,
    WMITLV_TAG_STRUC_wlan_host_memory_chunk,
    WMITLV_TAG_STRUC_wmi_start_scan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_stop_scan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_chan_list_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_channel,
    WMITLV_TAG_STRUC_wmi_pdev_set_regdomain_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_wmm_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wmm_params,
    WMITLV_TAG_STRUC_wmi_pdev_set_quiet_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_create_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_delete_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_start_request_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_noa_descriptor,
    WMITLV_TAG_STRUC_wmi_p2p_go_set_beacon_ie_fixed_param,
    WMITLV_TAG_STRUC_WMI_GTK_OFFLOAD_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_up_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_down_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_install_key_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_create_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_delete_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_flush_tids_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_set_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_assoc_complete_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vht_rate_set,
    WMITLV_TAG_STRUC_wmi_bcn_tmpl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_prb_tmpl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bcn_prb_info,
    WMITLV_TAG_STRUC_wmi_peer_tid_addba_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_tid_delba_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_powersave_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_powersave_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_dtim_ps_method_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_mode_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_rssi_threshold_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_period_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_rssi_change_threshold_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_suspend_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_resume_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_add_bcn_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmv_bcn_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wow_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wow_hostwakeup_from_sleep_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_uapsd_auto_trig_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_uapsd_auto_trig_param,
    WMITLV_TAG_STRUC_WMI_SET_ARP_NS_OFFLOAD_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_ARP_OFFLOAD_TUPLE,
    WMITLV_TAG_STRUC_WMI_NS_OFFLOAD_TUPLE,
    WMITLV_TAG_STRUC_wmi_ftm_intg_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_STA_KEEPALIVE_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_STA_KEEPALVE_ARP_RESPONSE,
    WMITLV_TAG_STRUC_wmi_p2p_set_vendor_ie_data_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ap_ps_peer_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_rate_retry_sched_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlan_profile_trigger_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlan_profile_set_hist_intvl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlan_profile_get_prof_data_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlan_profile_enable_profile_id_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_DEL_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_ADD_DEL_EVT_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_rtt_measreq_head,
    WMITLV_TAG_STRUC_wmi_rtt_measreq_body,
    WMITLV_TAG_STRUC_wmi_rtt_tsf_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_spectral_configure_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_spectral_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_nlo_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_nlo_configured_parameters,
    WMITLV_TAG_STRUC_wmi_csa_offload_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_csa_offload_chanswitch_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_chatter_set_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_echo_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_keepalive_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_keepalive_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_FORCE_FW_HANG_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_gpio_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_gpio_output_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_add_wds_entry_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_remove_wds_entry_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bcn_tx_hdr,
    WMITLV_TAG_STRUC_wmi_bcn_send_from_host_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mgmt_tx_hdr,
    WMITLV_TAG_STRUC_wmi_addba_clear_resp_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_addba_send_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_delba_send_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_addba_setresponse_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_send_singleamsdu_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_pktlog_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_pktlog_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_ht_ie_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_vht_ie_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_dscp_tid_map_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_green_ap_ps_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_get_tpc_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_base_macaddr_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_mcast_group_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_ap_profile_fixed_param,
    WMITLV_TAG_STRUC_wmi_ap_profile,
    WMITLV_TAG_STRUC_wmi_scan_sch_priority_table_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_dfs_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_dfs_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_ADD_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_WOW_BITMAP_PATTERN_T,
    WMITLV_TAG_STRUC_WOW_IPV4_SYNC_PATTERN_T,
    WMITLV_TAG_STRUC_WOW_IPV6_SYNC_PATTERN_T,
    WMITLV_TAG_STRUC_WOW_MAGIC_PATTERN_CMD,
    WMITLV_TAG_STRUC_WMI_scan_update_request_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_chatter_pkt_coalescing_filter,
    WMITLV_TAG_STRUC_wmi_chatter_coalescing_add_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_chatter_coalescing_delete_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_chatter_coalescing_query_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_txbf_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_debug_log_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_nlo_event,
    WMITLV_TAG_STRUC_wmi_chatter_query_reply_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_upload_h_hdr,
    WMITLV_TAG_STRUC_wmi_capture_h_event_hdr,
    WMITLV_TAG_STRUC_WMI_VDEV_WNM_SLEEPMODE_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_wmm_addts_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_wmm_delts_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_wmm_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tdls_set_state_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tdls_peer_update_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tdls_peer_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tdls_peer_capabilities,
    WMITLV_TAG_STRUC_wmi_vdev_mcc_set_tbtt_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_chan_list_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_mcc_bcn_intvl_change_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_resmgr_adaptive_ocs_enable_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_resmgr_set_chan_time_quota_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_resmgr_set_chan_latency_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ba_req_ssn_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ba_rsp_ssn_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_smps_force_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_SET_MCASTBCAST_FILTER_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_set_oppps_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_set_noa_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ba_req_ssn_cmd_sub_struct_param,
    WMITLV_TAG_STRUC_wmi_ba_req_ssn_event_sub_struct_param,
    WMITLV_TAG_STRUC_wmi_sta_smps_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_gtx_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mcc_sched_traffic_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mcc_sched_sta_traffic_stats,
    WMITLV_TAG_STRUC_wmi_offload_bcn_tx_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_noa_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_set_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_set_tcp_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_set_tcp_pkt_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_set_udp_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_set_udp_pkt_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hb_ind_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tx_pause_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_rfkill_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_dfs_radar_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_dfs_phyerr_filter_ena_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_dfs_phyerr_filter_dis_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_batch_scan_result_scan_list,
    WMITLV_TAG_STRUC_wmi_batch_scan_result_network_info,
    WMITLV_TAG_STRUC_wmi_batch_scan_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_batch_scan_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_batch_scan_trigger_result_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_batch_scan_enabled_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_batch_scan_result_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_plmreq_start_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_plmreq_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_thermal_mgmt_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_thermal_mgmt_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_info_req_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_info,
    WMITLV_TAG_STRUC_wmi_peer_tx_fail_cnt_thr_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmc_set_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmc_set_action_period_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmc_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mhf_offload_set_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mhf_offload_plumb_routing_table_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_cmd_param,
    WMITLV_TAG_STRUC_wmi_nan_event_hdr,
    WMITLV_TAG_STRUC_wmi_pdev_l1ss_track_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_diag_data_container_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_modem_power_state_cmd_param,
    WMITLV_TAG_STRUC_wmi_peer_get_estimated_linkspeed_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_estimated_linkspeed_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_aggr_state_trig_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_mhf_offload_routing_table_entry,
    WMITLV_TAG_STRUC_wmi_roam_scan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_req_stats_ext_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_stats_ext_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_obss_scan_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_obss_scan_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_offload_prb_rsp_tx_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_led_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_host_auto_shutdown_cfg_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_host_auto_shutdown_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_update_whal_mib_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_avoid_update_cmd_param,
    WMITLV_TAG_STRUC_WOW_IOAC_PKT_PATTERN_T,
    WMITLV_TAG_STRUC_WOW_IOAC_TMR_PATTERN_T,
    WMITLV_TAG_STRUC_WMI_WOW_IOAC_ADD_KEEPALIVE_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_IOAC_DEL_KEEPALIVE_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_IOAC_KEEPALIVE_T,
    WMITLV_TAG_STRUC_WMI_WOW_IOAC_ADD_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_IOAC_DEL_PATTERN_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_start_link_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_clear_link_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_link_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_iface_link_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_radio_link_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_channel_stats,
    WMITLV_TAG_STRUC_wmi_radio_link_stats,
    WMITLV_TAG_STRUC_wmi_rate_stats,
    WMITLV_TAG_STRUC_wmi_peer_link_stats,
    WMITLV_TAG_STRUC_wmi_wmm_ac_stats,
    WMITLV_TAG_STRUC_wmi_iface_link_stats,
    WMITLV_TAG_STRUC_wmi_lpi_mgmt_snooping_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_lpi_start_scan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_lpi_stop_scan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_lpi_result_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_state_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_bucket_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_bucket_channel_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_start_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_configure_wlan_change_monitor_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_wlan_change_bssid_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_configure_hotlist_monitor_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_get_cached_results_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_get_wlan_change_results_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_set_capabilities_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_get_capabilities_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_operation_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_start_stop_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_table_usage_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_wlan_descriptor_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_rssi_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_cached_results_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_wlan_change_results_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_wlan_change_result_bssid_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_hotlist_match_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_capabilities_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_cache_capabilities_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_wlan_change_monitor_capabilities_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_hotlist_monitor_capabilities_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_d0_wow_enable_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_d0_wow_disable_ack_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_unit_test_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_11i_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_11r_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_ese_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_synch_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_synch_complete_fixed_param,
    WMITLV_TAG_STRUC_wmi_extwow_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extwow_set_app_type1_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extwow_set_app_type2_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_lpi_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_lpi_handoff_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_rate_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_rate_ht_info,
    WMITLV_TAG_STRUC_wmi_ric_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_get_temperature_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_temperature_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_set_dhcp_server_offload_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tpc_chainmask_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ric_tspec,
    WMITLV_TAG_STRUC_wmi_tpc_chainmask_config,
    WMITLV_TAG_STRUCT_wmi_ipa_offload_enable_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_prob_req_oui_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_key_material,
    WMITLV_TAG_STRUC_wmi_tdls_set_offchan_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_set_led_flashing_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mdns_offload_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mdns_set_fqdn_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mdns_set_resp_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mdns_get_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mdns_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_invoke_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_resume_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_antenna_diversity_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sap_ofl_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sap_ofl_add_sta_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_sap_ofl_del_sta_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_apfind_cmd_param,
    WMITLV_TAG_STRUC_wmi_apfind_event_hdr,
    WMITLV_TAG_STRUC_wmi_ocb_set_sched_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_set_sched_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_set_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_set_config_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_set_utc_time_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_start_timing_advert_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_stop_timing_advert_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_get_tsf_timer_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_get_tsf_timer_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_get_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_channel_stats_request,
    WMITLV_TAG_STRUC_wmi_dcc_get_stats_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_clear_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_update_ndl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_update_ndl_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_dcc_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ocb_channel,
    WMITLV_TAG_STRUC_wmi_ocb_schedule_element,
    WMITLV_TAG_STRUC_wmi_dcc_ndl_stats_per_channel,
    WMITLV_TAG_STRUC_wmi_dcc_ndl_chan,
    WMITLV_TAG_STRUC_wmi_qos_parameter,
    WMITLV_TAG_STRUC_wmi_dcc_ndl_active_state_config,
    WMITLV_TAG_STRUC_wmi_roam_scan_extended_threshold_param,
    WMITLV_TAG_STRUC_wmi_roam_filter_fixed_param,
    WMITLV_TAG_STRUC_wmi_passpoint_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_passpoint_event_hdr,
    WMITLV_TAG_STRUC_wmi_extscan_configure_hotlist_ssid_monitor_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_hotlist_ssid_match_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_tsf_tstamp_action_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_tsf_report_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_fw_mem_dump_fixed_param,
    WMITLV_TAG_STRUC_wmi_update_fw_mem_dump_fixed_param,
    WMITLV_TAG_STRUC_wmi_fw_mem_dump_params,
    WMITLV_TAG_STRUC_wmi_debug_mesg_flush_fixed_param,
    WMITLV_TAG_STRUC_wmi_debug_mesg_flush_complete_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_set_rate_report_condition_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_subnet_change_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_ie_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rssi_breach_monitor_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_rssi_breach_event_fixed_param,
    WMITLV_TAG_STRUC_WOW_EVENT_INITIAL_WAKEUP_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_set_pcl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_set_hw_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_set_hw_mode_response_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_hw_mode_transition_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_txrx_streams,
    WMITLV_TAG_STRUC_wmi_soc_set_hw_mode_response_vdev_mac_entry,
    WMITLV_TAG_STRUC_wmi_soc_set_dual_mac_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_set_dual_mac_config_response_event_fixed_param,
    WMITLV_TAG_STRUC_WOW_IOAC_SOCK_PATTERN_T,
    WMITLV_TAG_STRUC_wmi_wow_enable_icmpv6_na_flt_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_diag_event_log_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_diag_event_log_supported_event_fixed_params,
    WMITLV_TAG_STRUC_wmi_packet_filter_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_packet_filter_enable_fixed_param,
    WMITLV_TAG_STRUC_wmi_sap_set_blacklist_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mgmt_tx_send_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mgmt_tx_compl_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_soc_set_antenna_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_WOW_UDP_SVC_OFLD_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_lro_info_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_earlystop_rssi_thres_param,
    WMITLV_TAG_STRUC_wmi_service_ready_ext_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_mawc_sensor_report_ind_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mawc_enable_sensor_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_configure_mawc_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_nlo_configure_mawc_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_extscan_configure_mawc_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_assoc_conf_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_wow_hostwakeup_gpio_pin_pattern_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ap_ps_egap_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ap_ps_egap_info_event_fixed_param,
    WMITLV_TAG_STRUC_WMI_PMF_OFFLOAD_SET_SA_QUERY_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_transfer_data_to_flash_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_transfer_data_to_flash_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_scpc_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ap_ps_egap_info_chainmask_list,
    WMITLV_TAG_STRUC_wmi_sta_smps_force_mode_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_get_capability_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_capability_info_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_get_vdev_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_vdev_stats_info_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_set_vdev_instructions_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_del_vdev_instructions_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_delete_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_delete_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_dense_thres_param,
    WMITLV_TAG_STRUC_enlo_candidate_score_param,
    WMITLV_TAG_STRUC_wmi_peer_update_wds_entry_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_config_ratemask_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_fips_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_smart_ant_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_smart_ant_set_rx_antenna_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_smart_ant_set_tx_antenna_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_smart_ant_set_train_antenna_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_smart_ant_set_node_config_ops_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_ant_switch_tbl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_ctl_table_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_mimogain_table_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_fwtest_set_param_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_atf_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_atf_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_get_ani_cck_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_get_ani_ofdm_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_inst_rssi_stats_resp_fixed_param,
    WMITLV_TAG_STRUC_wmi_med_util_report_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_sta_ps_statechange_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_wds_addr_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_ratecode_list_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_nfcal_power_all_channels_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_tpc_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ani_ofdm_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ani_cck_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_channel_hopping_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_fips_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_atf_peer_info,
    WMITLV_TAG_STRUC_wmi_pdev_get_tpc_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_filter_nrp_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_QBOOST_CFG_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_smart_ant_gpio_handle,
    WMITLV_TAG_STRUC_wmi_peer_smart_ant_set_tx_antenna_series,
    WMITLV_TAG_STRUC_wmi_peer_smart_ant_set_train_antenna_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_ant_ctrl_chain,
    WMITLV_TAG_STRUC_wmi_peer_cck_ofdm_rate_info,
    WMITLV_TAG_STRUC_wmi_peer_mcs_rate_info,
    WMITLV_TAG_STRUC_wmi_pdev_nfcal_power_all_channels_nfdBr,
    WMITLV_TAG_STRUC_wmi_pdev_nfcal_power_all_channels_nfdBm,
    WMITLV_TAG_STRUC_wmi_pdev_nfcal_power_all_channels_freqNum,
    WMITLV_TAG_STRUC_wmi_mu_report_total_mu,
    WMITLV_TAG_STRUC_wmi_vdev_set_dscp_tid_map_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_set_mbo_fixed_param,
    WMITLV_TAG_STRUC_wmi_mib_stats_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_disc_iface_created_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_disc_iface_deleted_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_started_cluster_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_joined_cluster_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndi_get_cap_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_initiator_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_responder_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_end_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndi_cap_rsp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_initiator_rsp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_responder_rsp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_end_rsp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_indication_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_confirm_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_end_indication_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_quiet_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_pcl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_hw_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_mac_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_antenna_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_hw_mode_response_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_hw_mode_transition_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_hw_mode_response_vdev_mac_entry,
    WMITLV_TAG_STRUC_wmi_pdev_set_mac_config_response_event_fixed_param,
    WMITLV_TAG_STRUC_WMI_COEX_CONFIG_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_config_enhanced_mcast_filter_fixed_param,
    WMITLV_TAG_STRUC_WMI_CHAN_AVOID_RPT_ALLOW_CMD_fixed_param,
    WMITLV_TAG_STRUC_wmi_set_periodic_channel_stats_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_custom_aggr_size_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_wal_power_debug_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_MAC_PHY_CAPABILITIES,
    WMITLV_TAG_STRUC_WMI_HW_MODE_CAPABILITIES,
    WMITLV_TAG_STRUC_WMI_SOC_MAC_PHY_HW_MODE_CAPS,
    WMITLV_TAG_STRUC_WMI_HAL_REG_CAPABILITIES_EXT,
    WMITLV_TAG_STRUC_WMI_SOC_HAL_REG_CAPABILITIES,
    WMITLV_TAG_STRUC_wmi_vdev_wisa_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tx_power_level_stats_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_adaptive_dwell_parameters_tlv,
    WMITLV_TAG_STRUC_wmi_scan_adaptive_dwell_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_wow_set_action_wake_up_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_ndp_end_rsp_per_ndi,
    WMITLV_TAG_STRUC_wmi_peer_bwf_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_bwf_peer_info,
    WMITLV_TAG_STRUC_wmi_dbglog_time_stamp_sync_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmc_set_leader_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rmc_manual_leader_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_per_chain_rssi_stats,
    WMITLV_TAG_STRUC_wmi_rssi_stats,
    WMITLV_TAG_STRUC_wmi_p2p_lo_start_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_lo_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_p2p_lo_stopped_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_reorder_queue_setup_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_reorder_queue_remove_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_set_multiple_mcast_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_mgmt_tx_compl_bundle_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_read_data_from_flash_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_read_data_from_flash_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_reorder_timeout_val_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_set_rx_blocksize_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_PDEV_SET_WAKEUP_CONFIG_CMDID_fixed_param,
    WMITLV_TAG_STRUC_wmi_tlv_buf_len_param,
    WMITLV_TAG_STRUC_wmi_service_available_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_antdiv_info_req_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_antdiv_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_antdiv_info,
    WMITLV_TAG_STRUC_wmi_pdev_get_antdiv_status_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_antdiv_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_mnt_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_chip_power_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_chip_power_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_coex_get_antenna_isolation_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_coex_report_isolation_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_cca_stats,
    WMITLV_TAG_STRUC_wmi_peer_signal_stats,
    WMITLV_TAG_STRUC_wmi_tx_stats,
    WMITLV_TAG_STRUC_wmi_peer_ac_tx_stats,
    WMITLV_TAG_STRUC_wmi_rx_stats,
    WMITLV_TAG_STRUC_wmi_peer_ac_rx_stats,
    WMITLV_TAG_STRUC_wmi_report_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_cca_stats_thresh,
    WMITLV_TAG_STRUC_wmi_peer_signal_stats_thresh,
    WMITLV_TAG_STRUC_wmi_tx_stats_thresh,
    WMITLV_TAG_STRUC_wmi_rx_stats_thresh,
    WMITLV_TAG_STRUC_wmi_pdev_set_stats_threshold_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_wlan_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_rx_aggr_failure_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_rx_aggr_failure_info,
    WMITLV_TAG_STRUC_wmi_vdev_encrypt_decrypt_data_req_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_encrypt_decrypt_data_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_band_to_mac,
    WMITLV_TAG_STRUC_wmi_tbtt_offset_info,
    WMITLV_TAG_STRUC_wmi_tbtt_offset_ext_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_sar_limits_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sar_limit_cmd_row,
    WMITLV_TAG_STRUC_wmi_pdev_dfs_phyerr_offload_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_dfs_phyerr_offload_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_adfs_ch_cfg_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_adfs_ocac_abort_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_dfs_radar_detection_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_adfs_ocac_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_dfs_cac_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vendor_oui,
    WMITLV_TAG_STRUC_wmi_request_rcpi_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_update_rcpi_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_peer_stats_info_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_stats_info,
    WMITLV_TAG_STRUC_wmi_peer_stats_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pkgid_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_connected_nlo_rssi_params,
    WMITLV_TAG_STRUC_wmi_set_current_country_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_regulatory_rule_struct,
    WMITLV_TAG_STRUC_wmi_reg_chan_list_cc_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_11d_scan_start_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_11d_scan_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_11d_new_country_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_radio_chan_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_radio_chan_stats,
    WMITLV_TAG_STRUC_wmi_radio_chan_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_per_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_add_mac_addr_to_rx_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_add_mac_addr_to_rx_filter_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_set_vdev_active_mode_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_hw_data_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_connected_nlo_bss_band_rssi_pref,
    WMITLV_TAG_STRUC_wmi_peer_oper_mode_change_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chip_power_save_failure_detected_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_multiple_vdev_restart_request_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_csa_switch_count_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_update_pkt_routing_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_check_cal_version_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_check_cal_version_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_diversity_gain_cmd_fixed_param,
    WMITLV_TAG_STRUC_WMI_MAC_PHY_CHAINMASK_COMBO,
    WMITLV_TAG_STRUC_WMI_MAC_PHY_CHAINMASK_CAPABILITY,
    WMITLV_TAG_STRUC_wmi_vdev_set_arp_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_arp_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_arp_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_iface_offload_stats,
    WMITLV_TAG_STRUC_wmi_request_stats_cmd_sub_struc_param,
    WMITLV_TAG_STRUC_rssi_ctl_ext,
    WMITLV_TAG_STRUC_wmi_single_phyerr_ext_rx_hdr,
    WMITLV_TAG_STRUC_wmi_coex_bt_activity_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_tx_power_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_tx_power_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_offchan_data_tx_compl_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_offchan_data_tx_send_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_tx_send_params,
    WMITLV_TAG_STRUC_wmi_he_rate_set,
    WMITLV_TAG_STRUC_wmi_congestion_stats,
    WMITLV_TAG_STRUC_wmi_set_init_country_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_dbs_duty_cycle_fixed_param,
    WMITLV_TAG_STRUC_wmi_scan_dbs_duty_cycle_param_tlv,
    WMITLV_TAG_STRUC_wmi_pdev_div_get_rssi_antid_fixed_param,
    WMITLV_TAG_STRUC_wmi_therm_throt_config_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_therm_throt_level_config_info,
    WMITLV_TAG_STRUC_wmi_therm_throt_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_therm_throt_level_stats_info,
    WMITLV_TAG_STRUC_wmi_pdev_div_rssi_antid_event_fixed_param,
    WMITLV_TAG_STRUC_WMI_OEM_DMA_RING_CAPABILITIES,
    WMITLV_TAG_STRUC_wmi_oem_dma_ring_cfg_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_oem_dma_ring_cfg_rsp_fixed_param,
    WMITLV_TAG_STRUC_wmi_oem_indirect_data,
    WMITLV_TAG_STRUC_wmi_oem_dma_buf_release_fixed_param,
    WMITLV_TAG_STRUC_wmi_oem_dma_buf_release_entry,
    WMITLV_TAG_STRUC_wmi_pdev_bss_chan_info_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_bss_chan_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_lca_disallow_config_tlv_param,
    WMITLV_TAG_STRUC_wmi_vdev_limit_offchan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_rssi_rejection_oce_config_param,
    WMITLV_TAG_STRUC_wmi_roam_rejection_list_config_param =
        WMITLV_TAG_STRUC_wmi_roam_rssi_rejection_oce_config_param,
    WMITLV_TAG_STRUC_wmi_unit_test_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_fils_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_pdev_update_pmk_cache_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pmk_cache,
    WMITLV_TAG_STRUC_wmi_pdev_update_fils_hlp_pkt_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_fils_synch_tlv_param,
    WMITLV_TAG_STRUC_wmi_gtk_offload_extended_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_bg_scan_roaming_param,
    WMITLV_TAG_STRUC_wmi_oic_ping_offload_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_oic_ping_offload_set_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_oic_ping_handoff_event,
    WMITLV_TAG_STRUC_wmi_dhcp_lease_renew_offload_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_dhcp_lease_renew_event,
    WMITLV_TAG_STRUC_wmi_btm_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_debug_mesg_fw_data_stall_param,
    WMITLV_TAG_STRUC_wmi_wlm_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_update_ctltable_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_update_ctltable_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_cnd_scoring_param,
    WMITLV_TAG_STRUC_wmi_pdev_config_vendor_oui_action_fixed_param,
    WMITLV_TAG_STRUC_wmi_vendor_oui_ext,
    WMITLV_TAG_STRUC_wmi_roam_synch_frame_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_fd_send_from_host_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_enable_fils_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_host_swfda_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_bcn_offload_ctrl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_ac_tx_queue_optimized_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_stats_period,
    WMITLV_TAG_STRUC_wmi_ndl_schedule_update_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_tid_msduq_qdepth_thresh_update_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_msduq_qdepth_thresh_update,
    WMITLV_TAG_STRUC_wmi_pdev_set_rx_filter_promiscuous_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sar2_result_event_fixed_param,
    WMITLV_TAG_STRUC_WMI_SAR_CAPABILITIES,
    WMITLV_TAG_STRUC_wmi_sap_obss_detection_cfg_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sap_obss_detection_info_evt_fixed_param,
    WMITLV_TAG_STRUC_WMI_DMA_RING_CAPABILITIES,
    WMITLV_TAG_STRUC_wmi_dma_ring_cfg_req_fixed_param,
    WMITLV_TAG_STRUC_wmi_dma_ring_cfg_rsp_fixed_param,
    WMITLV_TAG_STRUC_wmi_dma_buf_release_fixed_param,
    WMITLV_TAG_STRUC_wmi_dma_buf_release_entry,
    WMITLV_TAG_STRUC_wmi_sar_get_limits_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_sar_get_limits_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_sar_get_limits_event_row,
    WMITLV_TAG_STRUC_wmi_offload_11k_report_fixed_param,
    WMITLV_TAG_STRUC_wmi_invoke_neighbor_report_fixed_param,
    WMITLV_TAG_STRUC_wmi_neighbor_report_offload_tlv_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_connectivity_check_stats,
    WMITLV_TAG_STRUC_wmi_vdev_get_connectivity_check_stats,
    WMITLV_TAG_STRUC_wmi_bpf_set_vdev_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_set_vdev_work_memory_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_get_vdev_work_memory_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_bpf_get_vdev_work_memory_resp_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_get_nfcal_power_fixed_param,
    WMITLV_TAG_STRUC_wmi_bss_color_change_enable_fixed_param,
    WMITLV_TAG_STRUC_wmi_obss_color_collision_det_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_obss_color_collision_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_runtime_dpd_recal_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_disable_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_add_dialog_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_del_dialog_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_pause_dialog_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_resume_dialog_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_enable_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_disable_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_add_dialog_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_del_dialog_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_pause_dialog_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_resume_dialog_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_roam_scan_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_tid_configurations_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_set_custom_sw_retry_th_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_tpc_power_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_tpc_power_evt_fixed_param,
    WMITLV_TAG_STRUC_wmi_dma_buf_release_spectral_meta_data,
    WMITLV_TAG_STRUC_wmi_motion_det_config_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_motion_det_base_line_config_params_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_motion_det_start_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_motion_det_base_line_start_stop_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_motion_det_event,
    WMITLV_TAG_STRUC_wmi_motion_det_base_line_event,
    WMITLV_TAG_STRUC_wmi_ndp_transport_ip_param,
    WMITLV_TAG_STRUC_wmi_obss_spatial_reuse_set_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_esp_estimate_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_host_config_param,
    WMITLV_TAG_STRUC_wmi_spectral_bin_scaling_params,
    WMITLV_TAG_STRUC_wmi_peer_cfr_capture_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_chan_width_switch_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_width_peer_list,
    WMITLV_TAG_STRUC_wmi_obss_spatial_reuse_set_def_obss_thresh_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_he_tb_action_frm_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_extd2_stats,
    WMITLV_TAG_STRUC_wmi_hpcs_pulse_start_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_ctl_failsafe_check_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_chainmask_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_bcn_offload_quiet_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_nan_event_info,
    WMITLV_TAG_STRUC_wmi_ndp_channel_info,
    WMITLV_TAG_STRUC_wmi_ndp_cmd_param,
    WMITLV_TAG_STRUC_wmi_ndp_event_param,
    WMITLV_TAG_STRUC_wmi_pdev_pktlog_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_pktlog_filter_info,
    WMITLV_TAG_STRUC_wmi_quiet_offload_info,
    WMITLV_TAG_STRUC_wmi_get_bcn_recv_stats_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_bcn_recv_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_tx_pn_request_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_tx_pn_response_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_tlv_arrays_len_param,
    WMITLV_TAG_STRUC_wmi_peer_unmap_response_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_csc_switch_count_status_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_bss_load_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_blacklist_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_csc_vdev_list,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_info_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_state_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_dpwb_state_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_tdm_state_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_idrx_state_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_get_mws_coex_antenna_sharing_state_fixed_param,
    WMITLV_TAG_STRUC_wmi_request_wlm_stats_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlm_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_key_material_ext,
    WMITLV_TAG_STRUC_wmi_peer_cfr_capture_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_cold_boot_cal_data_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_rap_config_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_set_rap_config_on_sta_ps_tlv_param,
    WMITLV_TAG_STRUC_wmi_pdev_rap_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_sta_tdcc_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_deauth_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_idle_config_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_idle_trigger_monitor_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_stats_interference,
    WMITLV_TAG_STRUC_wmi_roam_score_delta_param,
    WMITLV_TAG_STRUC_wmi_roam_cnd_min_rssi_param,
    WMITLV_TAG_STRUC_wmi_chan_rf_characterization_info,
    WMITLV_TAG_STRUC_wmi_wlanfw_iface_cmb_ind_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_wlanfw_iface_combination_param,
    WMITLV_TAG_STRUC_wmi_wlanfw_iface_limit_param,
    WMITLV_TAG_STRUC_wmi_pdev_dsm_filter_fixed_param,
    WMITLV_TAG_STRUC_wmi_pdev_bssid_disallow_list_config_param,
    WMITLV_TAG_STRUC_wmi_mgmt_hdr,
    WMITLV_TAG_STRUC_wmi_muedca_params_config_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_btwt_invite_sta_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_btwt_remove_sta_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_btwt_invite_sta_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_twt_btwt_remove_sta_complete_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_delete_all_peer_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_delete_all_peer_resp_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_chan_rf_characterization_info_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_oem_data_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_enable_disable_trigger_reason_fixed_param,
    WMITLV_TAG_STRUC_wmi_service_ready_ext2_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_preauth_status_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_preauth_start_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_set_elna_bypass_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_elna_bypass_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_get_elna_bypass_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_pmkid_request_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_cfr_capture_event_phase_fixed_param,
    WMITLV_TAG_STRUC_wmi_audio_aggr_enable_cmd_fixed_param,
    WMITLV_TAG_STRUC_audio_aggr_rate_set,
    WMITLV_TAG_STRUC_wmi_audio_aggr_add_group,
    WMITLV_TAG_STRUC_wmi_audio_aggr_del_group,
    WMITLV_TAG_STRUC_wmi_audio_aggr_set_group_rate,
    WMITLV_TAG_STRUC_wmi_audio_aggr_set_group_retry,
    WMITLV_TAG_STRUC_wmi_cfr_capture_filter_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_cfr_filter_group_config,
    WMITLV_TAG_STRUC_wmi_fd_tmpl_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_vdev_bss_max_idle_time_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_atf_ssid_grp_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_atf_group_info,
    WMITLV_TAG_STRUC_wmi_atf_grp_wmm_ac_cfg_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_atf_group_wmm_ac_info,
    WMITLV_TAG_STRUC_wmi_peer_atf_ext_request_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_atf_ext_info,
    WMITLV_TAG_STRUC_wmi_get_channel_ani_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_channel_ani_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_get_channel_ani_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_oem_data_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_peer_config_vlan_cmd_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_stats_event_fixed_param,
    WMITLV_TAG_STRUC_wmi_roam_trigger_reason_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_scan_channel_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_ap_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_result_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_neighbor_report_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_roam_neighbor_report_channel_info_tlv_param,
    WMITLV_TAG_STRUC_wmi_set_ocl_cmd_fixed_param,
} WMITLV_TAG_ID;
# 1708 "include/wmi_tlv_defs.h"
typedef enum { WMI_INIT_CMDID_tlv_order_fixed_param, WMI_INIT_CMDID_tlv_order_resource_config, WMI_INIT_CMDID_tlv_order_host_mem_chunks, WMI_INIT_CMDID_tlv_order_hw_mode, WMI_INIT_CMDID_tlv_order_band_to_mac, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_INIT_CMDID } WMI_INIT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_init_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_resource_config *resource_config; A_UINT32 num_resource_config; A_UINT32 is_allocated_resource_config; wlan_host_memory_chunk *host_mem_chunks; A_UINT32 num_host_mem_chunks; A_UINT32 is_allocated_host_mem_chunks; wmi_pdev_set_hw_mode_cmd_fixed_param *hw_mode; A_UINT32 num_hw_mode; A_UINT32 is_allocated_hw_mode; wmi_pdev_band_to_mac *band_to_mac; A_UINT32 num_band_to_mac; A_UINT32 is_allocated_band_to_mac; } WMI_INIT_CMDID_param_tlvs;;





typedef enum { WMI_PEER_CREATE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_CREATE_CMDID } WMI_PEER_CREATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_create_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_CREATE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_DELETE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_DELETE_CMDID } WMI_PEER_DELETE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_delete_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_DELETE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_DELETE_ALL_PEER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DELETE_ALL_PEER_CMDID } WMI_VDEV_DELETE_ALL_PEER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_delete_all_peer_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DELETE_ALL_PEER_CMDID_param_tlvs;;






typedef enum { WMI_PEER_UNMAP_RESPONSE_CMDID_tlv_order_fixed_param, WMI_PEER_UNMAP_RESPONSE_CMDID_tlv_order_peer_ids, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_UNMAP_RESPONSE_CMDID } WMI_PEER_UNMAP_RESPONSE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_unmap_response_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *peer_ids; A_UINT32 num_peer_ids; A_UINT32 is_allocated_peer_ids; } WMI_PEER_UNMAP_RESPONSE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_FLUSH_TIDS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_FLUSH_TIDS_CMDID } WMI_PEER_FLUSH_TIDS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_flush_tids_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_FLUSH_TIDS_CMDID_param_tlvs;;





typedef enum { WMI_PEER_SET_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SET_PARAM_CMDID } WMI_PEER_SET_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_set_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_SET_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_STA_POWERSAVE_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_POWERSAVE_MODE_CMDID } WMI_STA_POWERSAVE_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_powersave_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_POWERSAVE_MODE_CMDID_param_tlvs;;





typedef enum { WMI_STA_POWERSAVE_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_POWERSAVE_PARAM_CMDID } WMI_STA_POWERSAVE_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_powersave_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_POWERSAVE_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_STA_TDCC_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_TDCC_CONFIG_CMDID } WMI_STA_TDCC_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_tdcc_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_TDCC_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_STA_DTIM_PS_METHOD_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_DTIM_PS_METHOD_CMDID } WMI_STA_DTIM_PS_METHOD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_dtim_ps_method_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_DTIM_PS_METHOD_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_REGDOMAIN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_REGDOMAIN_CMDID } WMI_PDEV_SET_REGDOMAIN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_regdomain_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_REGDOMAIN_CMDID_param_tlvs;;






typedef enum { WMI_PEER_TID_ADDBA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TID_ADDBA_CMDID } WMI_PEER_TID_ADDBA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tid_addba_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TID_ADDBA_CMDID_param_tlvs;;





typedef enum { WMI_PEER_TID_DELBA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TID_DELBA_CMDID } WMI_PEER_TID_DELBA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tid_delba_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TID_DELBA_CMDID_param_tlvs;;






typedef enum { WMI_BA_REQ_SSN_CMDID_tlv_order_fixed_param, WMI_BA_REQ_SSN_CMDID_tlv_order_ba_req_ssn_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BA_REQ_SSN_CMDID } WMI_BA_REQ_SSN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ba_req_ssn_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ba_req_ssn *ba_req_ssn_list; A_UINT32 num_ba_req_ssn_list; A_UINT32 is_allocated_ba_req_ssn_list; } WMI_BA_REQ_SSN_CMDID_param_tlvs;;







typedef enum { WMI_PDEV_FTM_INTG_CMDID_tlv_order_fixed_param, WMI_PDEV_FTM_INTG_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_FTM_INTG_CMDID } WMI_PDEV_FTM_INTG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ftm_intg_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_FTM_INTG_CMDID_param_tlvs;;





typedef enum { WMI_WOW_HOSTWAKEUP_FROM_SLEEP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_HOSTWAKEUP_FROM_SLEEP_CMDID } WMI_WOW_HOSTWAKEUP_FROM_SLEEP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wow_hostwakeup_from_sleep_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_HOSTWAKEUP_FROM_SLEEP_CMDID_param_tlvs;;





typedef enum { WMI_WOW_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_ENABLE_CMDID } WMI_WOW_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wow_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_WOW_ENABLE_ICMPV6_NA_FLT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_ENABLE_ICMPV6_NA_FLT_CMDID } WMI_WOW_ENABLE_ICMPV6_NA_FLT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wow_enable_icmpv6_na_flt_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_ENABLE_ICMPV6_NA_FLT_CMDID_param_tlvs;;





typedef enum { WMI_RMV_BCN_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMV_BCN_FILTER_CMDID } WMI_RMV_BCN_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rmv_bcn_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMV_BCN_FILTER_CMDID_param_tlvs;;






typedef enum { WMI_11K_OFFLOAD_REPORT_CMDID_tlv_order_fixed_param, WMI_11K_OFFLOAD_REPORT_CMDID_tlv_order_offload_neighbor_report_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_11K_OFFLOAD_REPORT_CMDID } WMI_11K_OFFLOAD_REPORT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_11k_offload_report_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_neighbor_report_11k_offload_tlv_param *offload_neighbor_report_param; A_UINT32 num_offload_neighbor_report_param; A_UINT32 is_allocated_offload_neighbor_report_param; } WMI_11K_OFFLOAD_REPORT_CMDID_param_tlvs;;





typedef enum { WMI_11K_INVOKE_NEIGHBOR_REPORT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_11K_INVOKE_NEIGHBOR_REPORT_CMDID } WMI_11K_INVOKE_NEIGHBOR_REPORT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_11k_offload_invoke_neighbor_report_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_11K_INVOKE_NEIGHBOR_REPORT_CMDID_param_tlvs;;
# 1854 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_SCAN_MODE_tlv_order_fixed_param, WMI_ROAM_SCAN_MODE_tlv_order_scan_params, WMI_ROAM_SCAN_MODE_tlv_order_offload_param, WMI_ROAM_SCAN_MODE_tlv_order_offload_11i_param, WMI_ROAM_SCAN_MODE_tlv_order_offload_11r_param, WMI_ROAM_SCAN_MODE_tlv_order_offload_ese_param, WMI_ROAM_SCAN_MODE_tlv_order_assoc_ie_len_param, WMI_ROAM_SCAN_MODE_tlv_order_assoc_ie_buf, WMI_ROAM_SCAN_MODE_tlv_order_offload_fils_info_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_MODE } WMI_ROAM_SCAN_MODE_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_mode_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_start_scan_cmd_fixed_param *scan_params; A_UINT32 num_scan_params; A_UINT32 is_allocated_scan_params; wmi_roam_offload_tlv_param *offload_param; A_UINT32 num_offload_param; A_UINT32 is_allocated_offload_param; wmi_roam_11i_offload_tlv_param *offload_11i_param; A_UINT32 num_offload_11i_param; A_UINT32 is_allocated_offload_11i_param; wmi_roam_11r_offload_tlv_param *offload_11r_param; A_UINT32 num_offload_11r_param; A_UINT32 is_allocated_offload_11r_param; wmi_roam_ese_offload_tlv_param *offload_ese_param; A_UINT32 num_offload_ese_param; A_UINT32 is_allocated_offload_ese_param; wmi_tlv_buf_len_param *assoc_ie_len_param; A_UINT32 num_assoc_ie_len_param; A_UINT32 is_allocated_assoc_ie_len_param; A_UINT8 *assoc_ie_buf; A_UINT32 num_assoc_ie_buf; A_UINT32 is_allocated_assoc_ie_buf; wmi_roam_fils_offload_tlv_param *offload_fils_info_param; A_UINT32 num_offload_fils_info_param; A_UINT32 is_allocated_offload_fils_info_param; } WMI_ROAM_SCAN_MODE_param_tlvs;;
# 1864 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_SCAN_RSSI_THRESHOLD_tlv_order_fixed_param, WMI_ROAM_SCAN_RSSI_THRESHOLD_tlv_order_extended_param, WMI_ROAM_SCAN_RSSI_THRESHOLD_tlv_order_earlystop_param, WMI_ROAM_SCAN_RSSI_THRESHOLD_tlv_order_dense_param, WMI_ROAM_SCAN_RSSI_THRESHOLD_tlv_order_bg_scan_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_RSSI_THRESHOLD } WMI_ROAM_SCAN_RSSI_THRESHOLD_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_rssi_threshold_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_roam_scan_extended_threshold_param *extended_param; A_UINT32 num_extended_param; A_UINT32 is_allocated_extended_param; wmi_roam_earlystop_rssi_thres_param *earlystop_param; A_UINT32 num_earlystop_param; A_UINT32 is_allocated_earlystop_param; wmi_roam_dense_thres_param *dense_param; A_UINT32 num_dense_param; A_UINT32 is_allocated_dense_param; wmi_roam_bg_scan_roaming_param *bg_scan_param; A_UINT32 num_bg_scan_param; A_UINT32 is_allocated_bg_scan_param; } WMI_ROAM_SCAN_RSSI_THRESHOLD_param_tlvs;;





typedef enum { WMI_ROAM_SCAN_PERIOD_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_PERIOD } WMI_ROAM_SCAN_PERIOD_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_period_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_SCAN_PERIOD_param_tlvs;;





typedef enum { WMI_ROAM_SCAN_RSSI_CHANGE_THRESHOLD_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_RSSI_CHANGE_THRESHOLD } WMI_ROAM_SCAN_RSSI_CHANGE_THRESHOLD_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_rssi_change_threshold_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_SCAN_RSSI_CHANGE_THRESHOLD_param_tlvs;;






typedef enum { WMI_ROAM_CHAN_LIST_tlv_order_fixed_param, WMI_ROAM_CHAN_LIST_tlv_order_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_CHAN_LIST } WMI_ROAM_CHAN_LIST_TAG_ORDER_enum_type; typedef struct { wmi_roam_chan_list_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; } WMI_ROAM_CHAN_LIST_param_tlvs;;





typedef enum { WMI_ROAM_SCAN_CMD_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_CMD } WMI_ROAM_SCAN_CMD_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_SCAN_CMD_param_tlvs;;
# 1900 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_fixed_param, WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_cellular_cap, WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_supp_op_class_param, WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_supp_op_class_list, WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_non_prefer_ch_param, WMI_ROAM_SET_MBO_PARAM_CMDID_tlv_order_non_prefer_ch_attr, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SET_MBO_PARAM_CMDID } WMI_ROAM_SET_MBO_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_set_mbo_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *cellular_cap; A_UINT32 num_cellular_cap; A_UINT32 is_allocated_cellular_cap; wmi_supported_operating_class_param *supp_op_class_param; A_UINT32 num_supp_op_class_param; A_UINT32 is_allocated_supp_op_class_param; A_UINT32 *supp_op_class_list; A_UINT32 num_supp_op_class_list; A_UINT32 is_allocated_supp_op_class_list; wmi_mbo_non_preferred_channel_report_param *non_prefer_ch_param; A_UINT32 num_non_prefer_ch_param; A_UINT32 is_allocated_non_prefer_ch_param; A_UINT8 *non_prefer_ch_attr; A_UINT32 num_non_prefer_ch_attr; A_UINT32 is_allocated_non_prefer_ch_attr; } WMI_ROAM_SET_MBO_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_ROAM_PER_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_PER_CONFIG_CMDID } WMI_ROAM_PER_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_per_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_PER_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_PLMREQ_START_CMDID_tlv_order_fixed_param, WMI_VDEV_PLMREQ_START_CMDID_tlv_order_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_PLMREQ_START_CMDID } WMI_VDEV_PLMREQ_START_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_plmreq_start_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; } WMI_VDEV_PLMREQ_START_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_PLMREQ_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_PLMREQ_STOP_CMDID } WMI_VDEV_PLMREQ_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_plmreq_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_PLMREQ_STOP_CMDID_param_tlvs;;
# 1929 "include/wmi_tlv_defs.h"
typedef enum { WMI_START_SCAN_CMDID_tlv_order_fixed_param, WMI_START_SCAN_CMDID_tlv_order_channel_list, WMI_START_SCAN_CMDID_tlv_order_ssid_list, WMI_START_SCAN_CMDID_tlv_order_bssid_list, WMI_START_SCAN_CMDID_tlv_order_ie_data, WMI_START_SCAN_CMDID_tlv_order_vendor_oui, WMI_START_SCAN_CMDID_tlv_order_phymode_list, WMI_START_SCAN_CMDID_tlv_order_hint_freq_short_ssid_list, WMI_START_SCAN_CMDID_tlv_order_hint_freq_bssid_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_START_SCAN_CMDID } WMI_START_SCAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_start_scan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; wmi_ssid *ssid_list; A_UINT32 num_ssid_list; A_UINT32 is_allocated_ssid_list; wmi_mac_addr *bssid_list; A_UINT32 num_bssid_list; A_UINT32 is_allocated_bssid_list; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; wmi_vendor_oui *vendor_oui; A_UINT32 num_vendor_oui; A_UINT32 is_allocated_vendor_oui; A_UINT8 *phymode_list; A_UINT32 num_phymode_list; A_UINT32 is_allocated_phymode_list; wmi_hint_freq_short_ssid *hint_freq_short_ssid_list; A_UINT32 num_hint_freq_short_ssid_list; A_UINT32 is_allocated_hint_freq_short_ssid_list; wmi_hint_freq_bssid *hint_freq_bssid_list; A_UINT32 num_hint_freq_bssid_list; A_UINT32 is_allocated_hint_freq_bssid_list; } WMI_START_SCAN_CMDID_param_tlvs;;






typedef enum { WMI_SCAN_ADAPTIVE_DWELL_CONFIG_CMDID_tlv_order_fixed_param, WMI_SCAN_ADAPTIVE_DWELL_CONFIG_CMDID_tlv_order_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_ADAPTIVE_DWELL_CONFIG_CMDID } WMI_SCAN_ADAPTIVE_DWELL_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_adaptive_dwell_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_scan_adaptive_dwell_parameters_tlv *param; A_UINT32 num_param; A_UINT32 is_allocated_param; } WMI_SCAN_ADAPTIVE_DWELL_CONFIG_CMDID_param_tlvs;;






typedef enum { WMI_SET_SCAN_DBS_DUTY_CYCLE_CMDID_tlv_order_fixed_param, WMI_SET_SCAN_DBS_DUTY_CYCLE_CMDID_tlv_order_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_SCAN_DBS_DUTY_CYCLE_CMDID } WMI_SET_SCAN_DBS_DUTY_CYCLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_dbs_duty_cycle_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_scan_dbs_duty_cycle_tlv_param *param; A_UINT32 num_param; A_UINT32 is_allocated_param; } WMI_SET_SCAN_DBS_DUTY_CYCLE_CMDID_param_tlvs;;
# 1954 "include/wmi_tlv_defs.h"
typedef enum { WMI_EXTSCAN_START_CMDID_tlv_order_fixed_param, WMI_EXTSCAN_START_CMDID_tlv_order_ssid_list, WMI_EXTSCAN_START_CMDID_tlv_order_bssid_list, WMI_EXTSCAN_START_CMDID_tlv_order_ie_data, WMI_EXTSCAN_START_CMDID_tlv_order_bucket_list, WMI_EXTSCAN_START_CMDID_tlv_order_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_START_CMDID } WMI_EXTSCAN_START_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_start_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ssid *ssid_list; A_UINT32 num_ssid_list; A_UINT32 is_allocated_ssid_list; wmi_mac_addr *bssid_list; A_UINT32 num_bssid_list; A_UINT32 is_allocated_bssid_list; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; wmi_extscan_bucket *bucket_list; A_UINT32 num_bucket_list; A_UINT32 is_allocated_bucket_list; wmi_extscan_bucket_channel *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; } WMI_EXTSCAN_START_CMDID_param_tlvs;;





typedef enum { WMI_EXTSCAN_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_STOP_CMDID } WMI_EXTSCAN_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_STOP_CMDID_param_tlvs;;






typedef enum { WMI_EXTSCAN_CONFIGURE_WLAN_CHANGE_MONITOR_CMDID_tlv_order_fixed_param, WMI_EXTSCAN_CONFIGURE_WLAN_CHANGE_MONITOR_CMDID_tlv_order_wlan_change_descriptor_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CONFIGURE_WLAN_CHANGE_MONITOR_CMDID } WMI_EXTSCAN_CONFIGURE_WLAN_CHANGE_MONITOR_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_configure_wlan_change_monitor_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_wlan_change_bssid_param *wlan_change_descriptor_list; A_UINT32 num_wlan_change_descriptor_list; A_UINT32 is_allocated_wlan_change_descriptor_list; } WMI_EXTSCAN_CONFIGURE_WLAN_CHANGE_MONITOR_CMDID_param_tlvs;;






typedef enum { WMI_EXTSCAN_CONFIGURE_HOTLIST_MONITOR_CMDID_tlv_order_fixed_param, WMI_EXTSCAN_CONFIGURE_HOTLIST_MONITOR_CMDID_tlv_order_hotlist, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CONFIGURE_HOTLIST_MONITOR_CMDID } WMI_EXTSCAN_CONFIGURE_HOTLIST_MONITOR_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_configure_hotlist_monitor_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_hotlist_entry *hotlist; A_UINT32 num_hotlist; A_UINT32 is_allocated_hotlist; } WMI_EXTSCAN_CONFIGURE_HOTLIST_MONITOR_CMDID_param_tlvs;;





typedef enum { WMI_EXTSCAN_GET_CACHED_RESULTS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_GET_CACHED_RESULTS_CMDID } WMI_EXTSCAN_GET_CACHED_RESULTS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_get_cached_results_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_GET_CACHED_RESULTS_CMDID_param_tlvs;;





typedef enum { WMI_EXTSCAN_GET_WLAN_CHANGE_RESULTS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_GET_WLAN_CHANGE_RESULTS_CMDID } WMI_EXTSCAN_GET_WLAN_CHANGE_RESULTS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_get_wlan_change_results_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_GET_WLAN_CHANGE_RESULTS_CMDID_param_tlvs;;
# 1995 "include/wmi_tlv_defs.h"
typedef enum { WMI_EXTSCAN_SET_CAPABILITIES_CMDID_tlv_order_fixed_param, WMI_EXTSCAN_SET_CAPABILITIES_CMDID_tlv_order_extscan_cache_capabilities, WMI_EXTSCAN_SET_CAPABILITIES_CMDID_tlv_order_wlan_change_capabilities, WMI_EXTSCAN_SET_CAPABILITIES_CMDID_tlv_order_hotlist_capabilities, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_SET_CAPABILITIES_CMDID } WMI_EXTSCAN_SET_CAPABILITIES_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_set_capabilities_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_cache_capabilities *extscan_cache_capabilities; A_UINT32 num_extscan_cache_capabilities; A_UINT32 is_allocated_extscan_cache_capabilities; wmi_extscan_wlan_change_monitor_capabilities *wlan_change_capabilities; A_UINT32 num_wlan_change_capabilities; A_UINT32 is_allocated_wlan_change_capabilities; wmi_extscan_hotlist_monitor_capabilities *hotlist_capabilities; A_UINT32 num_hotlist_capabilities; A_UINT32 is_allocated_hotlist_capabilities; } WMI_EXTSCAN_SET_CAPABILITIES_CMDID_param_tlvs;;





typedef enum { WMI_EXTSCAN_GET_CAPABILITIES_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_GET_CAPABILITIES_CMDID } WMI_EXTSCAN_GET_CAPABILITIES_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_get_capabilities_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_GET_CAPABILITIES_CMDID_param_tlvs;;






typedef enum { WMI_EXTSCAN_CONFIGURE_HOTLIST_SSID_MONITOR_CMDID_tlv_order_fixed_param, WMI_EXTSCAN_CONFIGURE_HOTLIST_SSID_MONITOR_CMDID_tlv_order_hotlist_ssid, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CONFIGURE_HOTLIST_SSID_MONITOR_CMDID } WMI_EXTSCAN_CONFIGURE_HOTLIST_SSID_MONITOR_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_configure_hotlist_ssid_monitor_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_hotlist_ssid_entry *hotlist_ssid; A_UINT32 num_hotlist_ssid; A_UINT32 is_allocated_hotlist_ssid; } WMI_EXTSCAN_CONFIGURE_HOTLIST_SSID_MONITOR_CMDID_param_tlvs;;




typedef enum { WMI_P2P_SET_VENDOR_IE_DATA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_SET_VENDOR_IE_DATA_CMDID } WMI_P2P_SET_VENDOR_IE_DATA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_set_vendor_ie_data_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_P2P_SET_VENDOR_IE_DATA_CMDID_param_tlvs;;




typedef enum { WMI_P2P_SET_OPPPS_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_SET_OPPPS_PARAM_CMDID } WMI_P2P_SET_OPPPS_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_set_oppps_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_P2P_SET_OPPPS_PARAM_CMDID_param_tlvs;;






typedef enum { WMI_P2P_LISTEN_OFFLOAD_START_CMDID_tlv_order_fixed_param, WMI_P2P_LISTEN_OFFLOAD_START_CMDID_tlv_order_device_types_data, WMI_P2P_LISTEN_OFFLOAD_START_CMDID_tlv_order_prob_resp_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_LISTEN_OFFLOAD_START_CMDID } WMI_P2P_LISTEN_OFFLOAD_START_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_lo_start_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *device_types_data; A_UINT32 num_device_types_data; A_UINT32 is_allocated_device_types_data; A_UINT8 *prob_resp_data; A_UINT32 num_prob_resp_data; A_UINT32 is_allocated_prob_resp_data; } WMI_P2P_LISTEN_OFFLOAD_START_CMDID_param_tlvs;;




typedef enum { WMI_P2P_LISTEN_OFFLOAD_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_LISTEN_OFFLOAD_STOP_CMDID } WMI_P2P_LISTEN_OFFLOAD_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_lo_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_P2P_LISTEN_OFFLOAD_STOP_CMDID_param_tlvs;;




typedef enum { WMI_P2P_LISTEN_OFFLOAD_STOPPED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_LISTEN_OFFLOAD_STOPPED_EVENTID } WMI_P2P_LISTEN_OFFLOAD_STOPPED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_lo_stopped_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_P2P_LISTEN_OFFLOAD_STOPPED_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_SET_CHANNEL_CMDID_tlv_order_chan, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_CHANNEL_CMDID } WMI_PDEV_SET_CHANNEL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; } WMI_PDEV_SET_CHANNEL_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_UPDATE_PMK_CACHE_CMDID_tlv_order_fixed_param, WMI_PDEV_UPDATE_PMK_CACHE_CMDID_tlv_order_pmk_cache, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UPDATE_PMK_CACHE_CMDID } WMI_PDEV_UPDATE_PMK_CACHE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_update_pmk_cache_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pmk_cache *pmk_cache; A_UINT32 num_pmk_cache; A_UINT32 is_allocated_pmk_cache; } WMI_PDEV_UPDATE_PMK_CACHE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_UPDATE_FILS_HLP_PKT_CMDID_tlv_order_fixed_param, WMI_PDEV_UPDATE_FILS_HLP_PKT_CMDID_tlv_order_fils_hlp_pkt, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UPDATE_FILS_HLP_PKT_CMDID } WMI_PDEV_UPDATE_FILS_HLP_PKT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_update_fils_hlp_pkt_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *fils_hlp_pkt; A_UINT32 num_fils_hlp_pkt; A_UINT32 is_allocated_fils_hlp_pkt; } WMI_PDEV_UPDATE_FILS_HLP_PKT_CMDID_param_tlvs;;



typedef enum { WMI_ECHO_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ECHO_CMDID } WMI_ECHO_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_echo_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ECHO_CMDID_param_tlvs;;
# 2064 "include/wmi_tlv_defs.h"
typedef enum { WMI_PDEV_SET_WMM_PARAMS_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_WMM_PARAMS_CMDID_tlv_order_wmm_params_ac_be, WMI_PDEV_SET_WMM_PARAMS_CMDID_tlv_order_wmm_params_ac_bk, WMI_PDEV_SET_WMM_PARAMS_CMDID_tlv_order_wmm_params_ac_vi, WMI_PDEV_SET_WMM_PARAMS_CMDID_tlv_order_wmm_params_ac_vo, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_WMM_PARAMS_CMDID } WMI_PDEV_SET_WMM_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_wmm_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_wmm_params *wmm_params_ac_be; A_UINT32 num_wmm_params_ac_be; A_UINT32 is_allocated_wmm_params_ac_be; wmi_wmm_params *wmm_params_ac_bk; A_UINT32 num_wmm_params_ac_bk; A_UINT32 is_allocated_wmm_params_ac_bk; wmi_wmm_params *wmm_params_ac_vi; A_UINT32 num_wmm_params_ac_vi; A_UINT32 is_allocated_wmm_params_ac_vi; wmi_wmm_params *wmm_params_ac_vo; A_UINT32 num_wmm_params_ac_vo; A_UINT32 is_allocated_wmm_params_ac_vo; } WMI_PDEV_SET_WMM_PARAMS_CMDID_param_tlvs;;







typedef enum { WMI_VDEV_START_REQUEST_CMDID_tlv_order_fixed_param, WMI_VDEV_START_REQUEST_CMDID_tlv_order_chan, WMI_VDEV_START_REQUEST_CMDID_tlv_order_noa_descriptors, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_START_REQUEST_CMDID } WMI_VDEV_START_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_start_request_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; wmi_p2p_noa_descriptor *noa_descriptors; A_UINT32 num_noa_descriptors; A_UINT32 is_allocated_noa_descriptors; } WMI_VDEV_START_REQUEST_CMDID_param_tlvs;;







typedef enum { WMI_VDEV_RESTART_REQUEST_CMDID_tlv_order_fixed_param, WMI_VDEV_RESTART_REQUEST_CMDID_tlv_order_chan, WMI_VDEV_RESTART_REQUEST_CMDID_tlv_order_noa_descriptors, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_RESTART_REQUEST_CMDID } WMI_VDEV_RESTART_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_start_request_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; wmi_p2p_noa_descriptor *noa_descriptors; A_UINT32 num_noa_descriptors; A_UINT32 is_allocated_noa_descriptors; } WMI_VDEV_RESTART_REQUEST_CMDID_param_tlvs;;






typedef enum { WMI_P2P_GO_SET_BEACON_IE_tlv_order_fixed_param, WMI_P2P_GO_SET_BEACON_IE_tlv_order_ie_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_GO_SET_BEACON_IE } WMI_P2P_GO_SET_BEACON_IE_TAG_ORDER_enum_type; typedef struct { wmi_p2p_go_set_beacon_ie_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; } WMI_P2P_GO_SET_BEACON_IE_param_tlvs;;






typedef enum { WMI_GTK_OFFLOAD_CMDID_tlv_order_fixed_param, WMI_GTK_OFFLOAD_CMDID_tlv_order_wmi_fils_gtk_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GTK_OFFLOAD_CMDID } WMI_GTK_OFFLOAD_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_GTK_OFFLOAD_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_gtk_offload_fils_tlv_param *wmi_fils_gtk_info; A_UINT32 num_wmi_fils_gtk_info; A_UINT32 is_allocated_wmi_fils_gtk_info; } WMI_GTK_OFFLOAD_CMDID_param_tlvs;;





typedef enum { WMI_PMF_OFFLOAD_SET_SA_QUERY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PMF_OFFLOAD_SET_SA_QUERY_CMDID } WMI_PMF_OFFLOAD_SET_SA_QUERY_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_PMF_OFFLOAD_SET_SA_QUERY_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PMF_OFFLOAD_SET_SA_QUERY_CMDID_param_tlvs;;






typedef enum { WMI_SCAN_CHAN_LIST_CMDID_tlv_order_fixed_param, WMI_SCAN_CHAN_LIST_CMDID_tlv_order_chan_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_CHAN_LIST_CMDID } WMI_SCAN_CHAN_LIST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_chan_list_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *chan_info; A_UINT32 num_chan_info; A_UINT32 is_allocated_chan_info; } WMI_SCAN_CHAN_LIST_CMDID_param_tlvs;;






typedef enum { WMI_STA_UAPSD_AUTO_TRIG_CMDID_tlv_order_fixed_param, WMI_STA_UAPSD_AUTO_TRIG_CMDID_tlv_order_ac_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_UAPSD_AUTO_TRIG_CMDID } WMI_STA_UAPSD_AUTO_TRIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_uapsd_auto_trig_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_sta_uapsd_auto_trig_param *ac_param; A_UINT32 num_ac_param; A_UINT32 is_allocated_ac_param; } WMI_STA_UAPSD_AUTO_TRIG_CMDID_param_tlvs;;







typedef enum { WMI_PRB_TMPL_CMDID_tlv_order_fixed_param, WMI_PRB_TMPL_CMDID_tlv_order_bcn_prb_info, WMI_PRB_TMPL_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PRB_TMPL_CMDID } WMI_PRB_TMPL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_prb_tmpl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_bcn_prb_info *bcn_prb_info; A_UINT32 num_bcn_prb_info; A_UINT32 is_allocated_bcn_prb_info; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_PRB_TMPL_CMDID_param_tlvs;;







typedef enum { WMI_BCN_TMPL_CMDID_tlv_order_fixed_param, WMI_BCN_TMPL_CMDID_tlv_order_bcn_prb_info, WMI_BCN_TMPL_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BCN_TMPL_CMDID } WMI_BCN_TMPL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bcn_tmpl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_bcn_prb_info *bcn_prb_info; A_UINT32 num_bcn_prb_info; A_UINT32 is_allocated_bcn_prb_info; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_BCN_TMPL_CMDID_param_tlvs;;






typedef enum { WMI_FD_TMPL_CMDID_tlv_order_fixed_param, WMI_FD_TMPL_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_FD_TMPL_CMDID } WMI_FD_TMPL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_fd_tmpl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_FD_TMPL_CMDID_param_tlvs;;






typedef enum { WMI_VDEV_INSTALL_KEY_CMDID_tlv_order_fixed_param, WMI_VDEV_INSTALL_KEY_CMDID_tlv_order_key_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_INSTALL_KEY_CMDID } WMI_VDEV_INSTALL_KEY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_install_key_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *key_data; A_UINT32 num_key_data; A_UINT32 is_allocated_key_data; } WMI_VDEV_INSTALL_KEY_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_WNM_SLEEPMODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_WNM_SLEEPMODE_CMDID } WMI_VDEV_WNM_SLEEPMODE_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_VDEV_WNM_SLEEPMODE_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_WNM_SLEEPMODE_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMDID } WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_IPSEC_NATKEEPALIVE_FILTER_CMDID_param_tlvs;;
# 2165 "include/wmi_tlv_defs.h"
typedef enum { WMI_PEER_ASSOC_CMDID_tlv_order_fixed_param, WMI_PEER_ASSOC_CMDID_tlv_order_peer_legacy_rates, WMI_PEER_ASSOC_CMDID_tlv_order_peer_ht_rates, WMI_PEER_ASSOC_CMDID_tlv_order_peer_vht_rates, WMI_PEER_ASSOC_CMDID_tlv_order_peer_he_rates, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ASSOC_CMDID } WMI_PEER_ASSOC_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_assoc_complete_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *peer_legacy_rates; A_UINT32 num_peer_legacy_rates; A_UINT32 is_allocated_peer_legacy_rates; A_UINT8 *peer_ht_rates; A_UINT32 num_peer_ht_rates; A_UINT32 is_allocated_peer_ht_rates; wmi_vht_rate_set *peer_vht_rates; A_UINT32 num_peer_vht_rates; A_UINT32 is_allocated_peer_vht_rates; wmi_he_rate_set *peer_he_rates; A_UINT32 num_peer_he_rates; A_UINT32 is_allocated_peer_he_rates; } WMI_PEER_ASSOC_CMDID_param_tlvs;;





typedef enum { WMI_PEER_SET_RATE_REPORT_CONDITION_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SET_RATE_REPORT_CONDITION_CMDID } WMI_PEER_SET_RATE_REPORT_CONDITION_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_set_rate_report_condition_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_SET_RATE_REPORT_CONDITION_CMDID_param_tlvs;;






typedef enum { WMI_ADD_BCN_FILTER_CMDID_tlv_order_fixed_param, WMI_ADD_BCN_FILTER_CMDID_tlv_order_ie_map, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ADD_BCN_FILTER_CMDID } WMI_ADD_BCN_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_add_bcn_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *ie_map; A_UINT32 num_ie_map; A_UINT32 is_allocated_ie_map; } WMI_ADD_BCN_FILTER_CMDID_param_tlvs;;






typedef enum { WMI_STA_KEEPALIVE_CMDID_tlv_order_fixed_param, WMI_STA_KEEPALIVE_CMDID_tlv_order_arp_resp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_KEEPALIVE_CMDID } WMI_STA_KEEPALIVE_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_STA_KEEPALIVE_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_STA_KEEPALVE_ARP_RESPONSE *arp_resp; A_UINT32 num_arp_resp; A_UINT32 is_allocated_arp_resp; } WMI_STA_KEEPALIVE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_BSS_MAX_IDLE_TIME_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_BSS_MAX_IDLE_TIME_CMDID } WMI_VDEV_BSS_MAX_IDLE_TIME_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_bss_max_idle_time_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_BSS_MAX_IDLE_TIME_CMDID_param_tlvs;;
# 2200 "include/wmi_tlv_defs.h"
typedef enum { WMI_SET_ARP_NS_OFFLOAD_CMDID_tlv_order_fixed_param, WMI_SET_ARP_NS_OFFLOAD_CMDID_tlv_order_ns_tuples, WMI_SET_ARP_NS_OFFLOAD_CMDID_tlv_order_arp_tuples, WMI_SET_ARP_NS_OFFLOAD_CMDID_tlv_order_ns_ext_tuples, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_ARP_NS_OFFLOAD_CMDID } WMI_SET_ARP_NS_OFFLOAD_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_SET_ARP_NS_OFFLOAD_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_NS_OFFLOAD_TUPLE *ns_tuples; A_UINT32 num_ns_tuples; A_UINT32 is_allocated_ns_tuples; WMI_ARP_OFFLOAD_TUPLE *arp_tuples; A_UINT32 num_arp_tuples; A_UINT32 is_allocated_arp_tuples; WMI_NS_OFFLOAD_TUPLE *ns_ext_tuples; A_UINT32 num_ns_ext_tuples; A_UINT32 is_allocated_ns_ext_tuples; } WMI_SET_ARP_NS_OFFLOAD_CMDID_param_tlvs;;




typedef enum { WMI_AP_PS_PEER_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AP_PS_PEER_PARAM_CMDID } WMI_AP_PS_PEER_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ap_ps_peer_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AP_PS_PEER_PARAM_CMDID_param_tlvs;;




typedef enum { WMI_AP_PS_EGAP_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AP_PS_EGAP_PARAM_CMDID } WMI_AP_PS_EGAP_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ap_ps_egap_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AP_PS_EGAP_PARAM_CMDID_param_tlvs;;




typedef enum { WMI_WLAN_PROFILE_TRIGGER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_PROFILE_TRIGGER_CMDID } WMI_WLAN_PROFILE_TRIGGER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wlan_profile_trigger_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLAN_PROFILE_TRIGGER_CMDID_param_tlvs;;




typedef enum { WMI_WLAN_PROFILE_SET_HIST_INTVL_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_PROFILE_SET_HIST_INTVL_CMDID } WMI_WLAN_PROFILE_SET_HIST_INTVL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wlan_profile_set_hist_intvl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLAN_PROFILE_SET_HIST_INTVL_CMDID_param_tlvs;;




typedef enum { WMI_WLAN_PROFILE_GET_PROFILE_DATA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_PROFILE_GET_PROFILE_DATA_CMDID } WMI_WLAN_PROFILE_GET_PROFILE_DATA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wlan_profile_get_prof_data_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLAN_PROFILE_GET_PROFILE_DATA_CMDID_param_tlvs;;




typedef enum { WMI_WLAN_PROFILE_ENABLE_PROFILE_ID_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_PROFILE_ENABLE_PROFILE_ID_CMDID } WMI_WLAN_PROFILE_ENABLE_PROFILE_ID_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wlan_profile_enable_profile_id_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLAN_PROFILE_ENABLE_PROFILE_ID_CMDID_param_tlvs;;




typedef enum { WMI_WOW_DEL_WAKE_PATTERN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_DEL_WAKE_PATTERN_CMDID } WMI_WOW_DEL_WAKE_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_DEL_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_DEL_WAKE_PATTERN_CMDID_param_tlvs;;





typedef enum { WMI_WOW_UDP_SVC_OFLD_CMDID_tlv_order_fixed_param, WMI_WOW_UDP_SVC_OFLD_CMDID_tlv_order_pattern, WMI_WOW_UDP_SVC_OFLD_CMDID_tlv_order_response, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_UDP_SVC_OFLD_CMDID } WMI_WOW_UDP_SVC_OFLD_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_UDP_SVC_OFLD_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *pattern; A_UINT32 num_pattern; A_UINT32 is_allocated_pattern; A_UINT8 *response; A_UINT32 num_response; A_UINT32 is_allocated_response; } WMI_WOW_UDP_SVC_OFLD_CMDID_param_tlvs;;



typedef enum { WMI_WOW_HOSTWAKEUP_GPIO_PIN_PATTERN_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_HOSTWAKEUP_GPIO_PIN_PATTERN_CONFIG_CMDID } WMI_WOW_HOSTWAKEUP_GPIO_PIN_PATTERN_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_HOSTWAKEUP_GPIO_PIN_PATTERN_CONFIG_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_HOSTWAKEUP_GPIO_PIN_PATTERN_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_WOW_SET_ACTION_WAKE_UP_CMDID_tlv_order_fixed_param, WMI_WOW_SET_ACTION_WAKE_UP_CMDID_tlv_order_action_bitmaps_per_category, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_SET_ACTION_WAKE_UP_CMDID } WMI_WOW_SET_ACTION_WAKE_UP_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_SET_ACTION_WAKE_UP_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *action_bitmaps_per_category; A_UINT32 num_action_bitmaps_per_category; A_UINT32 is_allocated_action_bitmaps_per_category; } WMI_WOW_SET_ACTION_WAKE_UP_CMDID_param_tlvs;;





typedef enum { WMI_WOW_ENABLE_DISABLE_WAKE_EVENT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_ENABLE_DISABLE_WAKE_EVENT_CMDID } WMI_WOW_ENABLE_DISABLE_WAKE_EVENT_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_ADD_DEL_EVT_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_ENABLE_DISABLE_WAKE_EVENT_CMDID_param_tlvs;;






typedef enum { WMI_RTT_MEASREQ_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RTT_MEASREQ_CMDID } WMI_RTT_MEASREQ_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RTT_MEASREQ_CMDID_param_tlvs;;





typedef enum { WMI_RTT_TSF_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RTT_TSF_CMDID } WMI_RTT_TSF_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RTT_TSF_CMDID_param_tlvs;;





typedef enum { WMI_OEM_REQ_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_REQ_CMDID } WMI_OEM_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_REQ_CMDID_param_tlvs;;





typedef enum { WMI_OEM_REQUEST_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_REQUEST_CMDID } WMI_OEM_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_REQUEST_CMDID_param_tlvs;;





typedef enum { WMI_LPI_OEM_REQ_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_OEM_REQ_CMDID } WMI_LPI_OEM_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_LPI_OEM_REQ_CMDID_param_tlvs;;




typedef enum { WMI_OEM_DMA_RING_CFG_REQ_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_DMA_RING_CFG_REQ_CMDID } WMI_OEM_DMA_RING_CFG_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_oem_dma_ring_cfg_req_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OEM_DMA_RING_CFG_REQ_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SPECTRAL_SCAN_CONFIGURE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SPECTRAL_SCAN_CONFIGURE_CMDID } WMI_VDEV_SPECTRAL_SCAN_CONFIGURE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_spectral_configure_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SPECTRAL_SCAN_CONFIGURE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SPECTRAL_SCAN_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SPECTRAL_SCAN_ENABLE_CMDID } WMI_VDEV_SPECTRAL_SCAN_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_spectral_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SPECTRAL_SCAN_ENABLE_CMDID_param_tlvs;;






typedef enum { WMI_REQUEST_STATS_CMDID_tlv_order_fixed_param, WMI_REQUEST_STATS_CMDID_tlv_order_inst_rssi_params, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_STATS_CMDID } WMI_REQUEST_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_inst_rssi_stats_params *inst_rssi_params; A_UINT32 num_inst_rssi_params; A_UINT32 is_allocated_inst_rssi_params; } WMI_REQUEST_STATS_CMDID_param_tlvs;;





typedef enum { WMI_PEER_TX_PN_REQUEST_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TX_PN_REQUEST_CMDID } WMI_PEER_TX_PN_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tx_pn_request_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TX_PN_REQUEST_CMDID_param_tlvs;;






typedef enum { WMI_GET_FW_MEM_DUMP_CMDID_tlv_order_fixed_param, WMI_GET_FW_MEM_DUMP_CMDID_tlv_order_fw_mem_dump_params, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_FW_MEM_DUMP_CMDID } WMI_GET_FW_MEM_DUMP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_get_fw_mem_dump_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_fw_mem_dump *fw_mem_dump_params; A_UINT32 num_fw_mem_dump_params; A_UINT32 is_allocated_fw_mem_dump_params; } WMI_GET_FW_MEM_DUMP_CMDID_param_tlvs;;





typedef enum { WMI_DEBUG_MESG_FLUSH_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DEBUG_MESG_FLUSH_CMDID } WMI_DEBUG_MESG_FLUSH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_debug_mesg_flush_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DEBUG_MESG_FLUSH_CMDID_param_tlvs;;






typedef enum { WMI_DIAG_EVENT_LOG_CONFIG_CMDID_tlv_order_fixed_param, WMI_DIAG_EVENT_LOG_CONFIG_CMDID_tlv_order_diag_events_logs_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DIAG_EVENT_LOG_CONFIG_CMDID } WMI_DIAG_EVENT_LOG_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_diag_event_log_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *diag_events_logs_list; A_UINT32 num_diag_events_logs_list; A_UINT32 is_allocated_diag_events_logs_list; } WMI_DIAG_EVENT_LOG_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_START_LINK_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_START_LINK_STATS_CMDID } WMI_START_LINK_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_start_link_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_START_LINK_STATS_CMDID_param_tlvs;;





typedef enum { WMI_CLEAR_LINK_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CLEAR_LINK_STATS_CMDID } WMI_CLEAR_LINK_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_clear_link_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CLEAR_LINK_STATS_CMDID_param_tlvs;;





typedef enum { WMI_REQUEST_LINK_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_LINK_STATS_CMDID } WMI_REQUEST_LINK_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_link_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_LINK_STATS_CMDID_param_tlvs;;





typedef enum { WMI_REQUEST_WLM_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_WLM_STATS_CMDID } WMI_REQUEST_WLM_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_wlm_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_WLM_STATS_CMDID_param_tlvs;;
# 2373 "include/wmi_tlv_defs.h"
typedef enum { WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_fixed_param, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_nlo_list, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_channel_list, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_channel_prediction_param, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_candidate_score_params, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_vendor_oui, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_cnlo_rssi_params, WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_tlv_order_cnlo_bss_band_rssi_pref, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID } WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_nlo_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; nlo_configured_parameters *nlo_list; A_UINT32 num_nlo_list; A_UINT32 is_allocated_nlo_list; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; nlo_channel_prediction_cfg *channel_prediction_param; A_UINT32 num_channel_prediction_param; A_UINT32 is_allocated_channel_prediction_param; enlo_candidate_score_params *candidate_score_params; A_UINT32 num_candidate_score_params; A_UINT32 is_allocated_candidate_score_params; wmi_vendor_oui *vendor_oui; A_UINT32 num_vendor_oui; A_UINT32 is_allocated_vendor_oui; connected_nlo_rssi_params *cnlo_rssi_params; A_UINT32 num_cnlo_rssi_params; A_UINT32 is_allocated_cnlo_rssi_params; connected_nlo_bss_band_rssi_pref *cnlo_bss_band_rssi_pref; A_UINT32 num_cnlo_bss_band_rssi_pref; A_UINT32 is_allocated_cnlo_bss_band_rssi_pref; } WMI_NETWORK_LIST_OFFLOAD_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_PASSPOINT_LIST_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PASSPOINT_LIST_CONFIG_CMDID } WMI_PASSPOINT_LIST_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_passpoint_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PASSPOINT_LIST_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_CSA_OFFLOAD_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CSA_OFFLOAD_ENABLE_CMDID } WMI_CSA_OFFLOAD_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_csa_offload_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CSA_OFFLOAD_ENABLE_CMDID_param_tlvs;;






typedef enum { WMI_CSA_OFFLOAD_CHANSWITCH_CMDID_tlv_order_fixed_param, WMI_CSA_OFFLOAD_CHANSWITCH_CMDID_tlv_order_chan, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CSA_OFFLOAD_CHANSWITCH_CMDID } WMI_CSA_OFFLOAD_CHANSWITCH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_csa_offload_chanswitch_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; } WMI_CSA_OFFLOAD_CHANSWITCH_CMDID_param_tlvs;;





typedef enum { WMI_CHATTER_SET_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHATTER_SET_MODE_CMDID } WMI_CHATTER_SET_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_chatter_set_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHATTER_SET_MODE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_UTF_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UTF_CMDID } WMI_PDEV_UTF_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_UTF_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_QVIT_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_QVIT_CMDID } WMI_PDEV_QVIT_CMDID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_QVIT_CMDID_param_tlvs;;



typedef enum { WMI_PDEV_SET_WAKEUP_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_WAKEUP_CONFIG_CMDID } WMI_PDEV_SET_WAKEUP_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_PDEV_SET_WAKEUP_CONFIG_CMDID_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_WAKEUP_CONFIG_CMDID_param_tlvs;;



typedef enum { WMI_PDEV_DMA_RING_CFG_REQ_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DMA_RING_CFG_REQ_CMDID } WMI_PDEV_DMA_RING_CFG_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dma_ring_cfg_req_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DMA_RING_CFG_REQ_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_SET_KEEPALIVE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_KEEPALIVE_CMDID } WMI_VDEV_SET_KEEPALIVE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_keepalive_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_KEEPALIVE_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_GET_KEEPALIVE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_KEEPALIVE_CMDID } WMI_VDEV_GET_KEEPALIVE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_keepalive_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_KEEPALIVE_CMDID_param_tlvs;;




typedef enum { WMI_BCN_OFFLOAD_CTRL_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BCN_OFFLOAD_CTRL_CMDID } WMI_BCN_OFFLOAD_CTRL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bcn_offload_ctrl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BCN_OFFLOAD_CTRL_CMDID_param_tlvs;;




typedef enum { WMI_FWTEST_VDEV_MCC_SET_TBTT_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_FWTEST_VDEV_MCC_SET_TBTT_MODE_CMDID } WMI_FWTEST_VDEV_MCC_SET_TBTT_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_mcc_set_tbtt_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_FWTEST_VDEV_MCC_SET_TBTT_MODE_CMDID_param_tlvs;;





typedef enum { WMI_FWTEST_P2P_SET_NOA_PARAM_CMDID_tlv_order_fixed_param, WMI_FWTEST_P2P_SET_NOA_PARAM_CMDID_tlv_order_noa_descriptor, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_FWTEST_P2P_SET_NOA_PARAM_CMDID } WMI_FWTEST_P2P_SET_NOA_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_set_noa_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_p2p_noa_descriptor *noa_descriptor; A_UINT32 num_noa_descriptor; A_UINT32 is_allocated_noa_descriptor; } WMI_FWTEST_P2P_SET_NOA_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_UNIT_TEST_CMDID_tlv_order_fixed_param, WMI_UNIT_TEST_CMDID_tlv_order_args, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UNIT_TEST_CMDID } WMI_UNIT_TEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_unit_test_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *args; A_UINT32 num_args; A_UINT32 is_allocated_args; } WMI_UNIT_TEST_CMDID_param_tlvs;;




typedef enum { WMI_FORCE_FW_HANG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_FORCE_FW_HANG_CMDID } WMI_FORCE_FW_HANG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_FORCE_FW_HANG_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_FORCE_FW_HANG_CMDID_param_tlvs;;




typedef enum { WMI_SET_MCASTBCAST_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_MCASTBCAST_FILTER_CMDID } WMI_SET_MCASTBCAST_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_SET_MCASTBCAST_FILTER_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_MCASTBCAST_FILTER_CMDID_param_tlvs;;





typedef enum { WMI_SET_MULTIPLE_MCAST_FILTER_CMDID_tlv_order_fixed_param, WMI_SET_MULTIPLE_MCAST_FILTER_CMDID_tlv_order_mcast_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_MULTIPLE_MCAST_FILTER_CMDID } WMI_SET_MULTIPLE_MCAST_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_SET_MULTIPLE_MCAST_FILTER_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mac_addr *mcast_list; A_UINT32 num_mcast_list; A_UINT32 is_allocated_mcast_list; } WMI_SET_MULTIPLE_MCAST_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_DBGLOG_TIME_STAMP_SYNC_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DBGLOG_TIME_STAMP_SYNC_CMDID } WMI_DBGLOG_TIME_STAMP_SYNC_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_DBGLOG_TIME_STAMP_SYNC_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DBGLOG_TIME_STAMP_SYNC_CMDID_param_tlvs;;




typedef enum { WMI_GPIO_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GPIO_CONFIG_CMDID } WMI_GPIO_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_gpio_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GPIO_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_GPIO_OUTPUT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GPIO_OUTPUT_CMDID } WMI_GPIO_OUTPUT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_gpio_output_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GPIO_OUTPUT_CMDID_param_tlvs;;





typedef enum { WMI_PEER_ADD_WDS_ENTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ADD_WDS_ENTRY_CMDID } WMI_PEER_ADD_WDS_ENTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_add_wds_entry_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_ADD_WDS_ENTRY_CMDID_param_tlvs;;





typedef enum { WMI_PEER_REMOVE_WDS_ENTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_REMOVE_WDS_ENTRY_CMDID } WMI_PEER_REMOVE_WDS_ENTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_remove_wds_entry_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_REMOVE_WDS_ENTRY_CMDID_param_tlvs;;





typedef enum { WMI_BCN_TX_CMDID_tlv_order_hdr, WMI_BCN_TX_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BCN_TX_CMDID } WMI_BCN_TX_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bcn_tx_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_BCN_TX_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SEND_BCN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SEND_BCN_CMDID } WMI_PDEV_SEND_BCN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bcn_send_from_host_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SEND_BCN_CMDID_param_tlvs;;





typedef enum { WMI_MGMT_TX_CMDID_tlv_order_hdr, WMI_MGMT_TX_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MGMT_TX_CMDID } WMI_MGMT_TX_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_tx_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_MGMT_TX_CMDID_param_tlvs;;







typedef enum { WMI_MGMT_TX_SEND_CMDID_tlv_order_fixed_param, WMI_MGMT_TX_SEND_CMDID_tlv_order_bufp, WMI_MGMT_TX_SEND_CMDID_tlv_order_tx_send_params, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MGMT_TX_SEND_CMDID } WMI_MGMT_TX_SEND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_tx_send_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; wmi_tx_send_params *tx_send_params; A_UINT32 num_tx_send_params; A_UINT32 is_allocated_tx_send_params; } WMI_MGMT_TX_SEND_CMDID_param_tlvs;;







typedef enum { WMI_OFFCHAN_DATA_TX_SEND_CMDID_tlv_order_fixed_param, WMI_OFFCHAN_DATA_TX_SEND_CMDID_tlv_order_bufp, WMI_OFFCHAN_DATA_TX_SEND_CMDID_tlv_order_tx_send_params, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OFFCHAN_DATA_TX_SEND_CMDID } WMI_OFFCHAN_DATA_TX_SEND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_offchan_data_tx_send_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; wmi_tx_send_params *tx_send_params; A_UINT32 num_tx_send_params; A_UINT32 is_allocated_tx_send_params; } WMI_OFFCHAN_DATA_TX_SEND_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SEND_FD_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SEND_FD_CMDID } WMI_PDEV_SEND_FD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_fd_send_from_host_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SEND_FD_CMDID_param_tlvs;;





typedef enum { WMI_ENABLE_FILS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ENABLE_FILS_CMDID } WMI_ENABLE_FILS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_enable_fils_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ENABLE_FILS_CMDID_param_tlvs;;





typedef enum { WMI_ADDBA_CLEAR_RESP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ADDBA_CLEAR_RESP_CMDID } WMI_ADDBA_CLEAR_RESP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_addba_clear_resp_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ADDBA_CLEAR_RESP_CMDID_param_tlvs;;





typedef enum { WMI_ADDBA_SEND_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ADDBA_SEND_CMDID } WMI_ADDBA_SEND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_addba_send_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ADDBA_SEND_CMDID_param_tlvs;;





typedef enum { WMI_DELBA_SEND_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DELBA_SEND_CMDID } WMI_DELBA_SEND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_delba_send_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DELBA_SEND_CMDID_param_tlvs;;





typedef enum { WMI_ADDBA_SET_RESP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ADDBA_SET_RESP_CMDID } WMI_ADDBA_SET_RESP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_addba_setresponse_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ADDBA_SET_RESP_CMDID_param_tlvs;;





typedef enum { WMI_SEND_SINGLEAMSDU_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SEND_SINGLEAMSDU_CMDID } WMI_SEND_SINGLEAMSDU_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_send_singleamsdu_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SEND_SINGLEAMSDU_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_PKTLOG_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_PKTLOG_ENABLE_CMDID } WMI_PDEV_PKTLOG_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_pktlog_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_PKTLOG_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_PKTLOG_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_PKTLOG_DISABLE_CMDID } WMI_PDEV_PKTLOG_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_pktlog_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_PKTLOG_DISABLE_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_SET_HT_CAP_IE_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_HT_CAP_IE_CMDID_tlv_order_ie_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_HT_CAP_IE_CMDID } WMI_PDEV_SET_HT_CAP_IE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_ht_ie_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; } WMI_PDEV_SET_HT_CAP_IE_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_SET_VHT_CAP_IE_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_VHT_CAP_IE_CMDID_tlv_order_ie_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_VHT_CAP_IE_CMDID } WMI_PDEV_SET_VHT_CAP_IE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_vht_ie_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; } WMI_PDEV_SET_VHT_CAP_IE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_DSCP_TID_MAP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_DSCP_TID_MAP_CMDID } WMI_PDEV_SET_DSCP_TID_MAP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_dscp_tid_map_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_DSCP_TID_MAP_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_GREEN_AP_PS_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GREEN_AP_PS_ENABLE_CMDID } WMI_PDEV_GREEN_AP_PS_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_green_ap_ps_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GREEN_AP_PS_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_GET_TPC_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_TPC_CONFIG_CMDID } WMI_PDEV_GET_TPC_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_tpc_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_TPC_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DIV_GET_RSSI_ANTID_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DIV_GET_RSSI_ANTID_CMDID } WMI_PDEV_DIV_GET_RSSI_ANTID_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_div_get_rssi_antid_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DIV_GET_RSSI_ANTID_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_BSS_CHAN_INFO_REQUEST_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_BSS_CHAN_INFO_REQUEST_CMDID } WMI_PDEV_BSS_CHAN_INFO_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_bss_chan_info_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_BSS_CHAN_INFO_REQUEST_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_UPDATE_CTLTABLE_REQUEST_CMDID_tlv_order_fixed_param, WMI_PDEV_UPDATE_CTLTABLE_REQUEST_CMDID_tlv_order_ctltable_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UPDATE_CTLTABLE_REQUEST_CMDID } WMI_PDEV_UPDATE_CTLTABLE_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_update_ctltable_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ctltable_data; A_UINT32 num_ctltable_data; A_UINT32 is_allocated_ctltable_data; } WMI_PDEV_UPDATE_CTLTABLE_REQUEST_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_GET_TX_POWER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_TX_POWER_CMDID } WMI_VDEV_GET_TX_POWER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_tx_power_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_TX_POWER_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_LIMIT_OFFCHAN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_LIMIT_OFFCHAN_CMDID } WMI_VDEV_LIMIT_OFFCHAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_limit_offchan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_LIMIT_OFFCHAN_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SET_CUSTOM_SW_RETRY_TH_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_CUSTOM_SW_RETRY_TH_CMDID } WMI_VDEV_SET_CUSTOM_SW_RETRY_TH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_custom_sw_retry_th_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_CUSTOM_SW_RETRY_TH_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_CHAINMASK_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_CHAINMASK_CONFIG_CMDID } WMI_VDEV_CHAINMASK_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_chainmask_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_CHAINMASK_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_BASE_MACADDR_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_BASE_MACADDR_CMDID } WMI_PDEV_SET_BASE_MACADDR_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_base_macaddr_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_BASE_MACADDR_CMDID_param_tlvs;;





typedef enum { WMI_MIB_STATS_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MIB_STATS_ENABLE_CMDID } WMI_MIB_STATS_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mib_stats_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MIB_STATS_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_MCAST_GROUP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_MCAST_GROUP_CMDID } WMI_PEER_MCAST_GROUP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_mcast_group_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_MCAST_GROUP_CMDID_param_tlvs;;
# 2678 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_AP_PROFILE_tlv_order_fixed_param, WMI_ROAM_AP_PROFILE_tlv_order_ap_profile, WMI_ROAM_AP_PROFILE_tlv_order_roam_cnd_scoring_param, WMI_ROAM_AP_PROFILE_tlv_order_roam_score_delta_param_list, WMI_ROAM_AP_PROFILE_tlv_order_roam_cnd_min_rssi_param_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_AP_PROFILE } WMI_ROAM_AP_PROFILE_TAG_ORDER_enum_type; typedef struct { wmi_roam_ap_profile_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ap_profile *ap_profile; A_UINT32 num_ap_profile; A_UINT32 is_allocated_ap_profile; wmi_roam_cnd_scoring_param *roam_cnd_scoring_param; A_UINT32 num_roam_cnd_scoring_param; A_UINT32 is_allocated_roam_cnd_scoring_param; wmi_roam_score_delta_param *roam_score_delta_param_list; A_UINT32 num_roam_score_delta_param_list; A_UINT32 is_allocated_roam_score_delta_param_list; wmi_roam_cnd_min_rssi_param *roam_cnd_min_rssi_param_list; A_UINT32 num_roam_cnd_min_rssi_param_list; A_UINT32 is_allocated_roam_cnd_min_rssi_param_list; } WMI_ROAM_AP_PROFILE_param_tlvs;;





typedef enum { WMI_ROAM_SYNCH_COMPLETE_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SYNCH_COMPLETE } WMI_ROAM_SYNCH_COMPLETE_TAG_ORDER_enum_type; typedef struct { wmi_roam_synch_complete_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_SYNCH_COMPLETE_param_tlvs;;





typedef enum { WMI_ROAM_SET_RIC_REQUEST_CMDID_tlv_order_fixed_param, WMI_ROAM_SET_RIC_REQUEST_CMDID_tlv_order_ric_tspec_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SET_RIC_REQUEST_CMDID } WMI_ROAM_SET_RIC_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ric_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ric_tspec *ric_tspec_list; A_UINT32 num_ric_tspec_list; A_UINT32 is_allocated_ric_tspec_list; } WMI_ROAM_SET_RIC_REQUEST_CMDID_param_tlvs;;






typedef enum { WMI_SCAN_SCH_PRIO_TBL_CMDID_tlv_order_fixed_param, WMI_SCAN_SCH_PRIO_TBL_CMDID_tlv_order_mapping_table, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_SCH_PRIO_TBL_CMDID } WMI_SCAN_SCH_PRIO_TBL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_sch_priority_table_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *mapping_table; A_UINT32 num_mapping_table; A_UINT32 is_allocated_mapping_table; } WMI_SCAN_SCH_PRIO_TBL_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DFS_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DFS_ENABLE_CMDID } WMI_PDEV_DFS_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dfs_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DFS_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DFS_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DFS_DISABLE_CMDID } WMI_PDEV_DFS_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dfs_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DFS_DISABLE_CMDID_param_tlvs;;





typedef enum { WMI_DFS_PHYERR_FILTER_ENA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DFS_PHYERR_FILTER_ENA_CMDID } WMI_DFS_PHYERR_FILTER_ENA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dfs_phyerr_filter_ena_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DFS_PHYERR_FILTER_ENA_CMDID_param_tlvs;;





typedef enum { WMI_DFS_PHYERR_FILTER_DIS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DFS_PHYERR_FILTER_DIS_CMDID } WMI_DFS_PHYERR_FILTER_DIS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dfs_phyerr_filter_dis_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DFS_PHYERR_FILTER_DIS_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DFS_PHYERR_OFFLOAD_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DFS_PHYERR_OFFLOAD_ENABLE_CMDID } WMI_PDEV_DFS_PHYERR_OFFLOAD_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dfs_phyerr_offload_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DFS_PHYERR_OFFLOAD_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DFS_PHYERR_OFFLOAD_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DFS_PHYERR_OFFLOAD_DISABLE_CMDID } WMI_PDEV_DFS_PHYERR_OFFLOAD_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dfs_phyerr_offload_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DFS_PHYERR_OFFLOAD_DISABLE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_ADFS_CH_CFG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ADFS_CH_CFG_CMDID } WMI_VDEV_ADFS_CH_CFG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_adfs_ch_cfg_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ADFS_CH_CFG_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_ADFS_OCAC_ABORT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ADFS_OCAC_ABORT_CMDID } WMI_VDEV_ADFS_OCAC_ABORT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_adfs_ocac_abort_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ADFS_OCAC_ABORT_CMDID_param_tlvs;;
# 2757 "include/wmi_tlv_defs.h"
typedef enum { WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_fixed_param, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_bitmap, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_ipv4, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_ipv6, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_magic_pattern, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_timeout, WMI_WOW_ADD_WAKE_PATTERN_CMDID_tlv_order_ra_ratelimit_interval, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_ADD_WAKE_PATTERN_CMDID } WMI_WOW_ADD_WAKE_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_ADD_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WOW_BITMAP_PATTERN_T *pattern_info_bitmap; A_UINT32 num_pattern_info_bitmap; A_UINT32 is_allocated_pattern_info_bitmap; WOW_IPV4_SYNC_PATTERN_T *pattern_info_ipv4; A_UINT32 num_pattern_info_ipv4; A_UINT32 is_allocated_pattern_info_ipv4; WOW_IPV6_SYNC_PATTERN_T *pattern_info_ipv6; A_UINT32 num_pattern_info_ipv6; A_UINT32 is_allocated_pattern_info_ipv6; WOW_MAGIC_PATTERN_CMD *pattern_info_magic_pattern; A_UINT32 num_pattern_info_magic_pattern; A_UINT32 is_allocated_pattern_info_magic_pattern; A_UINT32 *pattern_info_timeout; A_UINT32 num_pattern_info_timeout; A_UINT32 is_allocated_pattern_info_timeout; A_UINT32 *ra_ratelimit_interval; A_UINT32 num_ra_ratelimit_interval; A_UINT32 is_allocated_ra_ratelimit_interval; } WMI_WOW_ADD_WAKE_PATTERN_CMDID_param_tlvs;;






typedef enum { WMI_WOW_IOAC_ADD_KEEPALIVE_CMDID_tlv_order_fixed_param, WMI_WOW_IOAC_ADD_KEEPALIVE_CMDID_tlv_order_keepalive_set, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_IOAC_ADD_KEEPALIVE_CMDID } WMI_WOW_IOAC_ADD_KEEPALIVE_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_IOAC_ADD_KEEPALIVE_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_WOW_IOAC_KEEPALIVE_T *keepalive_set; A_UINT32 num_keepalive_set; A_UINT32 is_allocated_keepalive_set; } WMI_WOW_IOAC_ADD_KEEPALIVE_CMDID_param_tlvs;;





typedef enum { WMI_WOW_IOAC_DEL_KEEPALIVE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_IOAC_DEL_KEEPALIVE_CMDID } WMI_WOW_IOAC_DEL_KEEPALIVE_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_IOAC_DEL_KEEPALIVE_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_IOAC_DEL_KEEPALIVE_CMDID_param_tlvs;;
# 2779 "include/wmi_tlv_defs.h"
typedef enum { WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_tlv_order_fixed_param, WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_pkt, WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_tmr, WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_tlv_order_pattern_info_sock, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID } WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_IOAC_ADD_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WOW_IOAC_PKT_PATTERN_T *pattern_info_pkt; A_UINT32 num_pattern_info_pkt; A_UINT32 is_allocated_pattern_info_pkt; WOW_IOAC_TMR_PATTERN_T *pattern_info_tmr; A_UINT32 num_pattern_info_tmr; A_UINT32 is_allocated_pattern_info_tmr; WOW_IOAC_SOCK_PATTERN_T *pattern_info_sock; A_UINT32 num_pattern_info_sock; A_UINT32 is_allocated_pattern_info_sock; } WMI_WOW_IOAC_ADD_WAKE_PATTERN_CMDID_param_tlvs;;





typedef enum { WMI_WOW_IOAC_DEL_WAKE_PATTERN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_IOAC_DEL_WAKE_PATTERN_CMDID } WMI_WOW_IOAC_DEL_WAKE_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_WOW_IOAC_DEL_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_IOAC_DEL_WAKE_PATTERN_CMDID_param_tlvs;;





typedef enum { WMI_EXTWOW_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTWOW_ENABLE_CMDID } WMI_EXTWOW_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extwow_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTWOW_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_EXTWOW_SET_APP_TYPE1_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTWOW_SET_APP_TYPE1_PARAMS_CMDID } WMI_EXTWOW_SET_APP_TYPE1_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extwow_set_app_type1_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTWOW_SET_APP_TYPE1_PARAMS_CMDID_param_tlvs;;





typedef enum { WMI_EXTWOW_SET_APP_TYPE2_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTWOW_SET_APP_TYPE2_PARAMS_CMDID } WMI_EXTWOW_SET_APP_TYPE2_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extwow_set_app_type2_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTWOW_SET_APP_TYPE2_PARAMS_CMDID_param_tlvs;;





typedef enum { WMI_STOP_SCAN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STOP_SCAN_CMDID } WMI_STOP_SCAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_stop_scan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STOP_SCAN_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_PARAM_CMDID } WMI_PDEV_SET_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_QUIET_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_QUIET_MODE_CMDID } WMI_PDEV_SET_QUIET_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_quiet_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_QUIET_MODE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SET_QUIET_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_QUIET_MODE_CMDID } WMI_VDEV_SET_QUIET_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_quiet_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_QUIET_MODE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_BCN_OFFLOAD_QUIET_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_BCN_OFFLOAD_QUIET_CONFIG_CMDID } WMI_VDEV_BCN_OFFLOAD_QUIET_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_bcn_offload_quiet_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_BCN_OFFLOAD_QUIET_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SET_CUSTOM_AGGR_SIZE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_CUSTOM_AGGR_SIZE_CMDID } WMI_VDEV_SET_CUSTOM_AGGR_SIZE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_custom_aggr_size_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_CUSTOM_AGGR_SIZE_CMDID_param_tlvs;;






typedef enum { WMI_VDEV_CREATE_CMDID_tlv_order_fixed_param, WMI_VDEV_CREATE_CMDID_tlv_order_cfg_txrx_streams, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_CREATE_CMDID } WMI_VDEV_CREATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_create_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vdev_txrx_streams *cfg_txrx_streams; A_UINT32 num_cfg_txrx_streams; A_UINT32 is_allocated_cfg_txrx_streams; } WMI_VDEV_CREATE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_DELETE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DELETE_CMDID } WMI_VDEV_DELETE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_delete_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DELETE_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_UP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_UP_CMDID } WMI_VDEV_UP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_up_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_UP_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_STOP_CMDID } WMI_VDEV_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_STOP_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_DOWN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DOWN_CMDID } WMI_VDEV_DOWN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_down_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DOWN_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SET_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_PARAM_CMDID } WMI_VDEV_SET_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_PARAM_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SUSPEND_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SUSPEND_CMDID } WMI_PDEV_SUSPEND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_suspend_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SUSPEND_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_RESUME_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_RESUME_CMDID } WMI_PDEV_RESUME_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_resume_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_RESUME_CMDID_param_tlvs;;




typedef enum { WMI_SCAN_UPDATE_REQUEST_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_UPDATE_REQUEST_CMDID } WMI_SCAN_UPDATE_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_update_request_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SCAN_UPDATE_REQUEST_CMDID_param_tlvs;;




typedef enum { WMI_SCAN_PROB_REQ_OUI_CMDID_tlv_order_fixed_param, WMI_SCAN_PROB_REQ_OUI_CMDID_tlv_order_vendor_oui, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_PROB_REQ_OUI_CMDID } WMI_SCAN_PROB_REQ_OUI_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_scan_prob_req_oui_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vendor_oui *vendor_oui; A_UINT32 num_vendor_oui; A_UINT32 is_allocated_vendor_oui; } WMI_SCAN_PROB_REQ_OUI_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID_tlv_order_fixed_param, WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID_tlv_order_vendor_oui_ext, WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID } WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_config_vendor_oui_action_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vendor_oui_ext *vendor_oui_ext; A_UINT32 num_vendor_oui_ext; A_UINT32 is_allocated_vendor_oui_ext; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_CONFIG_VENDOR_OUI_ACTION_CMDID_param_tlvs;;





typedef enum { WMI_CHATTER_ADD_COALESCING_FILTER_CMDID_tlv_order_fixed_param, WMI_CHATTER_ADD_COALESCING_FILTER_CMDID_tlv_order_coalescing_filter, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHATTER_ADD_COALESCING_FILTER_CMDID } WMI_CHATTER_ADD_COALESCING_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_chatter_coalescing_add_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; chatter_pkt_coalescing_filter *coalescing_filter; A_UINT32 num_coalescing_filter; A_UINT32 is_allocated_coalescing_filter; } WMI_CHATTER_ADD_COALESCING_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_CHATTER_DELETE_COALESCING_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHATTER_DELETE_COALESCING_FILTER_CMDID } WMI_CHATTER_DELETE_COALESCING_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_chatter_coalescing_delete_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHATTER_DELETE_COALESCING_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_CHATTER_COALESCING_QUERY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHATTER_COALESCING_QUERY_CMDID } WMI_CHATTER_COALESCING_QUERY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_chatter_coalescing_query_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHATTER_COALESCING_QUERY_CMDID_param_tlvs;;




typedef enum { WMI_TXBF_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TXBF_CMDID } WMI_TXBF_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_txbf_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TXBF_CMDID_param_tlvs;;





typedef enum { WMI_DBGLOG_CFG_CMDID_tlv_order_fixed_param, WMI_DBGLOG_CFG_CMDID_tlv_order_module_id_bitmap, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DBGLOG_CFG_CMDID } WMI_DBGLOG_CFG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_debug_log_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *module_id_bitmap; A_UINT32 num_module_id_bitmap; A_UINT32 is_allocated_module_id_bitmap; } WMI_DBGLOG_CFG_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_WMM_ADDTS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_WMM_ADDTS_CMDID } WMI_VDEV_WMM_ADDTS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_wmm_addts_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_WMM_ADDTS_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_WMM_DELTS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_WMM_DELTS_CMDID } WMI_VDEV_WMM_DELTS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_wmm_delts_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_WMM_DELTS_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_SET_WMM_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_WMM_PARAMS_CMDID } WMI_VDEV_SET_WMM_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_wmm_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_WMM_PARAMS_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_SET_GTX_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_GTX_PARAMS_CMDID } WMI_VDEV_SET_GTX_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_gtx_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_GTX_PARAMS_CMDID_param_tlvs;;






typedef enum { WMI_TDLS_SET_STATE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TDLS_SET_STATE_CMDID } WMI_TDLS_SET_STATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_tdls_set_state_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TDLS_SET_STATE_CMDID_param_tlvs;;







typedef enum { WMI_TDLS_PEER_UPDATE_CMDID_tlv_order_fixed_param, WMI_TDLS_PEER_UPDATE_CMDID_tlv_order_peer_caps, WMI_TDLS_PEER_UPDATE_CMDID_tlv_order_peer_chan_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TDLS_PEER_UPDATE_CMDID } WMI_TDLS_PEER_UPDATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_tdls_peer_update_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_tdls_peer_capabilities *peer_caps; A_UINT32 num_peer_caps; A_UINT32 is_allocated_peer_caps; wmi_channel *peer_chan_list; A_UINT32 num_peer_chan_list; A_UINT32 is_allocated_peer_chan_list; } WMI_TDLS_PEER_UPDATE_CMDID_param_tlvs;;





typedef enum { WMI_TDLS_SET_OFFCHAN_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TDLS_SET_OFFCHAN_MODE_CMDID } WMI_TDLS_SET_OFFCHAN_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_tdls_set_offchan_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TDLS_SET_OFFCHAN_MODE_CMDID_param_tlvs;;







typedef enum { WMI_RESMGR_ADAPTIVE_OCS_ENABLE_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RESMGR_ADAPTIVE_OCS_ENABLE_DISABLE_CMDID } WMI_RESMGR_ADAPTIVE_OCS_ENABLE_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_resmgr_adaptive_ocs_enable_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RESMGR_ADAPTIVE_OCS_ENABLE_DISABLE_CMDID_param_tlvs;;







typedef enum { WMI_RESMGR_SET_CHAN_TIME_QUOTA_CMDID_tlv_order_fixed_param, WMI_RESMGR_SET_CHAN_TIME_QUOTA_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RESMGR_SET_CHAN_TIME_QUOTA_CMDID } WMI_RESMGR_SET_CHAN_TIME_QUOTA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_resmgr_set_chan_time_quota_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RESMGR_SET_CHAN_TIME_QUOTA_CMDID_param_tlvs;;







typedef enum { WMI_RESMGR_SET_CHAN_LATENCY_CMDID_tlv_order_fixed_param, WMI_RESMGR_SET_CHAN_LATENCY_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RESMGR_SET_CHAN_LATENCY_CMDID } WMI_RESMGR_SET_CHAN_LATENCY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_resmgr_set_chan_latency_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RESMGR_SET_CHAN_LATENCY_CMDID_param_tlvs;;






typedef enum { WMI_STA_SMPS_FORCE_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_SMPS_FORCE_MODE_CMDID } WMI_STA_SMPS_FORCE_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_smps_force_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_SMPS_FORCE_MODE_CMDID_param_tlvs;;






typedef enum { WMI_HB_SET_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_SET_ENABLE_CMDID } WMI_HB_SET_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hb_set_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_SET_ENABLE_CMDID_param_tlvs;;






typedef enum { WMI_HB_SET_TCP_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_SET_TCP_PARAMS_CMDID } WMI_HB_SET_TCP_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hb_set_tcp_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_SET_TCP_PARAMS_CMDID_param_tlvs;;






typedef enum { WMI_HB_SET_TCP_PKT_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_SET_TCP_PKT_FILTER_CMDID } WMI_HB_SET_TCP_PKT_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hb_set_tcp_pkt_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_SET_TCP_PKT_FILTER_CMDID_param_tlvs;;






typedef enum { WMI_HB_SET_UDP_PARAMS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_SET_UDP_PARAMS_CMDID } WMI_HB_SET_UDP_PARAMS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hb_set_udp_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_SET_UDP_PARAMS_CMDID_param_tlvs;;






typedef enum { WMI_HB_SET_UDP_PKT_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_SET_UDP_PKT_FILTER_CMDID } WMI_HB_SET_UDP_PKT_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hb_set_udp_pkt_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_SET_UDP_PKT_FILTER_CMDID_param_tlvs;;






typedef enum { WMI_STA_SMPS_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_SMPS_PARAM_CMDID } WMI_STA_SMPS_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sta_smps_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_SMPS_PARAM_CMDID_param_tlvs;;






typedef enum { WMI_MCC_SCHED_TRAFFIC_STATS_CMDID_tlv_order_fixed_param, WMI_MCC_SCHED_TRAFFIC_STATS_CMDID_tlv_order_mcc_sched_sta_traffic_stats_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MCC_SCHED_TRAFFIC_STATS_CMDID } WMI_MCC_SCHED_TRAFFIC_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mcc_sched_traffic_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mcc_sched_sta_traffic_stats *mcc_sched_sta_traffic_stats_list; A_UINT32 num_mcc_sched_sta_traffic_stats_list; A_UINT32 is_allocated_mcc_sched_sta_traffic_stats_list; } WMI_MCC_SCHED_TRAFFIC_STATS_CMDID_param_tlvs;;





typedef enum { WMI_BATCH_SCAN_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BATCH_SCAN_ENABLE_CMDID } WMI_BATCH_SCAN_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_batch_scan_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BATCH_SCAN_ENABLE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_INFO_REQ_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_INFO_REQ_CMDID } WMI_PEER_INFO_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_info_req_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_INFO_REQ_CMDID_param_tlvs;;





typedef enum { WMI_PEER_ANTDIV_INFO_REQ_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ANTDIV_INFO_REQ_CMDID } WMI_PEER_ANTDIV_INFO_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_antdiv_info_req_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_ANTDIV_INFO_REQ_CMDID_param_tlvs;;





typedef enum { WMI_RMC_SET_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMC_SET_MODE_CMDID } WMI_RMC_SET_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rmc_set_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMC_SET_MODE_CMDID_param_tlvs;;





typedef enum { WMI_RMC_SET_ACTION_PERIOD_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMC_SET_ACTION_PERIOD_CMDID } WMI_RMC_SET_ACTION_PERIOD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rmc_set_action_period_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMC_SET_ACTION_PERIOD_CMDID_param_tlvs;;





typedef enum { WMI_RMC_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMC_CONFIG_CMDID } WMI_RMC_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rmc_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMC_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_MHF_OFFLOAD_SET_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MHF_OFFLOAD_SET_MODE_CMDID } WMI_MHF_OFFLOAD_SET_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mhf_offload_set_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MHF_OFFLOAD_SET_MODE_CMDID_param_tlvs;;






typedef enum { WMI_MHF_OFFLOAD_PLUMB_ROUTING_TBL_CMDID_tlv_order_fixed_param, WMI_MHF_OFFLOAD_PLUMB_ROUTING_TBL_CMDID_tlv_order_routing_tbl_entries, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MHF_OFFLOAD_PLUMB_ROUTING_TBL_CMDID } WMI_MHF_OFFLOAD_PLUMB_ROUTING_TBL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mhf_offload_plumb_routing_table_cmd *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mhf_offload_routing_table_entry *routing_tbl_entries; A_UINT32 num_routing_tbl_entries; A_UINT32 is_allocated_routing_tbl_entries; } WMI_MHF_OFFLOAD_PLUMB_ROUTING_TBL_CMDID_param_tlvs;;




typedef enum { WMI_BATCH_SCAN_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BATCH_SCAN_DISABLE_CMDID } WMI_BATCH_SCAN_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_batch_scan_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BATCH_SCAN_DISABLE_CMDID_param_tlvs;;




typedef enum { WMI_BATCH_SCAN_TRIGGER_RESULT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BATCH_SCAN_TRIGGER_RESULT_CMDID } WMI_BATCH_SCAN_TRIGGER_RESULT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_batch_scan_trigger_result_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BATCH_SCAN_TRIGGER_RESULT_CMDID_param_tlvs;;





typedef enum { WMI_LPI_MGMT_SNOOPING_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_MGMT_SNOOPING_CONFIG_CMDID } WMI_LPI_MGMT_SNOOPING_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_mgmt_snooping_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_LPI_MGMT_SNOOPING_CONFIG_CMDID_param_tlvs;;
# 3127 "include/wmi_tlv_defs.h"
typedef enum { WMI_LPI_START_SCAN_CMDID_tlv_order_fixed_param, WMI_LPI_START_SCAN_CMDID_tlv_order_channel_list, WMI_LPI_START_SCAN_CMDID_tlv_order_ssid_list, WMI_LPI_START_SCAN_CMDID_tlv_order_bssid_list, WMI_LPI_START_SCAN_CMDID_tlv_order_ie_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_START_SCAN_CMDID } WMI_LPI_START_SCAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_start_scan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; wmi_ssid *ssid_list; A_UINT32 num_ssid_list; A_UINT32 is_allocated_ssid_list; wmi_mac_addr *bssid_list; A_UINT32 num_bssid_list; A_UINT32 is_allocated_bssid_list; A_UINT8 *ie_data; A_UINT32 num_ie_data; A_UINT32 is_allocated_ie_data; } WMI_LPI_START_SCAN_CMDID_param_tlvs;;





typedef enum { WMI_LPI_STOP_SCAN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_STOP_SCAN_CMDID } WMI_LPI_STOP_SCAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_stop_scan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_LPI_STOP_SCAN_CMDID_param_tlvs;;





typedef enum { WMI_REQUEST_ROAM_SCAN_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_ROAM_SCAN_STATS_CMDID } WMI_REQUEST_ROAM_SCAN_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_roam_scan_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_ROAM_SCAN_STATS_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_BSS_LOAD_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_BSS_LOAD_CONFIG_CMDID } WMI_ROAM_BSS_LOAD_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_bss_load_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_BSS_LOAD_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_DEAUTH_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_DEAUTH_CONFIG_CMDID } WMI_ROAM_DEAUTH_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_deauth_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_DEAUTH_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_IDLE_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_IDLE_CONFIG_CMDID } WMI_ROAM_IDLE_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_idle_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_IDLE_CONFIG_CMDID_param_tlvs;;






typedef enum { WMI_ROAM_PREAUTH_STATUS_CMDID_tlv_order_fixed_param, WMI_ROAM_PREAUTH_STATUS_CMDID_tlv_order_pmkid, WMI_ROAM_PREAUTH_STATUS_CMDID_tlv_order_pmk, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_PREAUTH_STATUS_CMDID } WMI_ROAM_PREAUTH_STATUS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_preauth_status_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *pmkid; A_UINT32 num_pmkid; A_UINT32 is_allocated_pmkid; A_UINT8 *pmk; A_UINT32 num_pmk; A_UINT32 is_allocated_pmk; } WMI_ROAM_PREAUTH_STATUS_CMDID_param_tlvs;;





typedef enum { WMI_ROAM_PMKID_REQUEST_EVENTID_tlv_order_fixed_param, WMI_ROAM_PMKID_REQUEST_EVENTID_tlv_order_pmkid_request, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_PMKID_REQUEST_EVENTID } WMI_ROAM_PMKID_REQUEST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_pmkid_request_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_roam_pmkid_request_tlv_param *pmkid_request; A_UINT32 num_pmkid_request; A_UINT32 is_allocated_pmkid_request; } WMI_ROAM_PMKID_REQUEST_EVENTID_param_tlvs;;




typedef enum { WMI_IDLE_TRIGGER_MONITOR_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_IDLE_TRIGGER_MONITOR_CMDID } WMI_IDLE_TRIGGER_MONITOR_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_idle_trigger_monitor_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_IDLE_TRIGGER_MONITOR_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_ENABLE_DISABLE_TRIGGER_REASON_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_ENABLE_DISABLE_TRIGGER_REASON_CMDID } WMI_ROAM_ENABLE_DISABLE_TRIGGER_REASON_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_enable_disable_trigger_reason_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_ENABLE_DISABLE_TRIGGER_REASON_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_DSM_FILTER_CMDID_tlv_order_fixed_param, WMI_PDEV_DSM_FILTER_CMDID_tlv_order_bssid_disallow_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DSM_FILTER_CMDID } WMI_PDEV_DSM_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dsm_filter_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_bssid_disallow_list_config_param *bssid_disallow_list; A_UINT32 num_bssid_disallow_list; A_UINT32 is_allocated_bssid_disallow_list; } WMI_PDEV_DSM_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_BLACKLIST_EVENTID_tlv_order_fixed_param, WMI_ROAM_BLACKLIST_EVENTID_tlv_order_blacklist_with_timeout, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_BLACKLIST_EVENTID } WMI_ROAM_BLACKLIST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_blacklist_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_roam_blacklist_with_timeout_tlv_param *blacklist_with_timeout; A_UINT32 num_blacklist_with_timeout; A_UINT32 is_allocated_blacklist_with_timeout; } WMI_ROAM_BLACKLIST_EVENTID_param_tlvs;;




typedef enum { WMI_ROAM_PREAUTH_START_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_PREAUTH_START_EVENTID } WMI_ROAM_PREAUTH_START_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_preauth_start_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_PREAUTH_START_EVENTID_param_tlvs;;




typedef enum { WMI_LPI_RESULT_EVENTID_tlv_order_fixed_param, WMI_LPI_RESULT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_RESULT_EVENTID } WMI_LPI_RESULT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_result_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_LPI_RESULT_EVENTID_param_tlvs;;




typedef enum { WMI_LPI_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_STATUS_EVENTID } WMI_LPI_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_LPI_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_LPI_HANDOFF_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LPI_HANDOFF_EVENTID } WMI_LPI_HANDOFF_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_lpi_handoff_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_LPI_HANDOFF_EVENTID_param_tlvs;;





typedef enum { WMI_THERMAL_MGMT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_THERMAL_MGMT_CMDID } WMI_THERMAL_MGMT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_thermal_mgmt_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_THERMAL_MGMT_CMDID_param_tlvs;;





typedef enum { WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMDID_tlv_order_fixed_param, WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMDID_tlv_order_pattern, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMDID } WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *pattern; A_UINT32 num_pattern; A_UINT32 is_allocated_pattern; } WMI_ADD_PROACTIVE_ARP_RSP_PATTERN_CMDID_param_tlvs;;



typedef enum { WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMDID } WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DEL_PROACTIVE_ARP_RSP_PATTERN_CMDID_param_tlvs;;






typedef enum { WMI_NAN_CMDID_tlv_order_fixed_param, WMI_NAN_CMDID_tlv_order_data, WMI_NAN_CMDID_tlv_order_host_config, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_CMDID } WMI_NAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_nan_cmd_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; wmi_nan_host_config_param *host_config; A_UINT32 num_host_config; A_UINT32 is_allocated_host_config; } WMI_NAN_CMDID_param_tlvs;;




typedef enum { WMI_NDI_GET_CAP_REQ_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDI_GET_CAP_REQ_CMDID } WMI_NDI_GET_CAP_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ndi_get_cap_req_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDI_GET_CAP_REQ_CMDID_param_tlvs;;
# 3259 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_fixed_param, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_channel, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_ndp_cfg, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_ndp_app_info, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_ndp_pmk, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_ndp_passphrase, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_nan_servicename, WMI_NDP_INITIATOR_REQ_CMDID_tlv_order_ndp_transport_ip_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_INITIATOR_REQ_CMDID } WMI_NDP_INITIATOR_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_initiator_req_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *channel; A_UINT32 num_channel; A_UINT32 is_allocated_channel; A_UINT8 *ndp_cfg; A_UINT32 num_ndp_cfg; A_UINT32 is_allocated_ndp_cfg; A_UINT8 *ndp_app_info; A_UINT32 num_ndp_app_info; A_UINT32 is_allocated_ndp_app_info; A_UINT8 *ndp_pmk; A_UINT32 num_ndp_pmk; A_UINT32 is_allocated_ndp_pmk; A_INT8 *ndp_passphrase; A_UINT32 num_ndp_passphrase; A_UINT32 is_allocated_ndp_passphrase; A_INT8 *nan_servicename; A_UINT32 num_nan_servicename; A_UINT32 is_allocated_nan_servicename; wmi_ndp_transport_ip_param *ndp_transport_ip_param; A_UINT32 num_ndp_transport_ip_param; A_UINT32 is_allocated_ndp_transport_ip_param; } WMI_NDP_INITIATOR_REQ_CMDID_param_tlvs;;
# 3279 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_fixed_param, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_ndp_cfg, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_ndp_app_info, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_ndp_pmk, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_ndp_passphrase, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_nan_servicename, WMI_NDP_RESPONDER_REQ_CMDID_tlv_order_ndp_transport_ip_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_RESPONDER_REQ_CMDID } WMI_NDP_RESPONDER_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_responder_req_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ndp_cfg; A_UINT32 num_ndp_cfg; A_UINT32 is_allocated_ndp_cfg; A_UINT8 *ndp_app_info; A_UINT32 num_ndp_app_info; A_UINT32 is_allocated_ndp_app_info; A_UINT8 *ndp_pmk; A_UINT32 num_ndp_pmk; A_UINT32 is_allocated_ndp_pmk; A_INT8 *ndp_passphrase; A_UINT32 num_ndp_passphrase; A_UINT32 is_allocated_ndp_passphrase; A_INT8 *nan_servicename; A_UINT32 num_nan_servicename; A_UINT32 is_allocated_nan_servicename; wmi_ndp_transport_ip_param *ndp_transport_ip_param; A_UINT32 num_ndp_transport_ip_param; A_UINT32 is_allocated_ndp_transport_ip_param; } WMI_NDP_RESPONDER_REQ_CMDID_param_tlvs;;
# 3290 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_END_REQ_CMDID_tlv_order_fixed_param, WMI_NDP_END_REQ_CMDID_tlv_order_ndp_end_req_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_END_REQ_CMDID } WMI_NDP_END_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_end_req_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ndp_end_req_PROTOTYPE *ndp_end_req_list; A_UINT32 num_ndp_end_req_list; A_UINT32 is_allocated_ndp_end_req_list; } WMI_NDP_END_REQ_CMDID_param_tlvs;;




typedef enum { WMI_NDP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_CMDID } WMI_NDP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_cmd_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDP_CMDID_param_tlvs;;




typedef enum { WMI_REQUEST_RCPI_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_RCPI_CMDID } WMI_REQUEST_RCPI_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_rcpi_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_RCPI_CMDID_param_tlvs;;




typedef enum { WMI_MODEM_POWER_STATE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MODEM_POWER_STATE_CMDID } WMI_MODEM_POWER_STATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_modem_power_state_cmd_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MODEM_POWER_STATE_CMDID_param_tlvs;;





typedef enum { WMI_SAR_LIMITS_CMDID_tlv_order_fixed_param, WMI_SAR_LIMITS_CMDID_tlv_order_sar_limits, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAR_LIMITS_CMDID } WMI_SAR_LIMITS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sar_limits_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_sar_limit_cmd_row *sar_limits; A_UINT32 num_sar_limits; A_UINT32 is_allocated_sar_limits; } WMI_SAR_LIMITS_CMDID_param_tlvs;;




typedef enum { WMI_SAR_GET_LIMITS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAR_GET_LIMITS_CMDID } WMI_SAR_GET_LIMITS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sar_get_limits_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAR_GET_LIMITS_CMDID_param_tlvs;;



typedef enum { WMI_SAR2_RESULT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAR2_RESULT_EVENTID } WMI_SAR2_RESULT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sar2_result_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAR2_RESULT_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_GET_ESTIMATED_LINKSPEED_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_GET_ESTIMATED_LINKSPEED_CMDID } WMI_PEER_GET_ESTIMATED_LINKSPEED_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_get_estimated_linkspeed_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_GET_ESTIMATED_LINKSPEED_CMDID_param_tlvs;;





typedef enum { WMI_REQUEST_STATS_EXT_CMDID_tlv_order_fixed_param, WMI_REQUEST_STATS_EXT_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_STATS_EXT_CMDID } WMI_REQUEST_STATS_EXT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_req_stats_ext_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_REQUEST_STATS_EXT_CMDID_param_tlvs;;







typedef enum { WMI_OBSS_SCAN_ENABLE_CMDID_tlv_order_fixed_param, WMI_OBSS_SCAN_ENABLE_CMDID_tlv_order_channels, WMI_OBSS_SCAN_ENABLE_CMDID_tlv_order_ie_field, WMI_OBSS_SCAN_ENABLE_CMDID_tlv_order_chan_freqs, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OBSS_SCAN_ENABLE_CMDID } WMI_OBSS_SCAN_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_obss_scan_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *channels; A_UINT32 num_channels; A_UINT32 is_allocated_channels; A_UINT8 *ie_field; A_UINT32 num_ie_field; A_UINT32 is_allocated_ie_field; A_UINT32 *chan_freqs; A_UINT32 num_chan_freqs; A_UINT32 is_allocated_chan_freqs; } WMI_OBSS_SCAN_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_OBSS_SCAN_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OBSS_SCAN_DISABLE_CMDID } WMI_OBSS_SCAN_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_obss_scan_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OBSS_SCAN_DISABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_LED_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_LED_CONFIG_CMDID } WMI_PDEV_SET_LED_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_led_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_LED_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_HOST_AUTO_SHUTDOWN_CFG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HOST_AUTO_SHUTDOWN_CFG_CMDID } WMI_HOST_AUTO_SHUTDOWN_CFG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_host_auto_shutdown_cfg_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HOST_AUTO_SHUTDOWN_CFG_CMDID_param_tlvs;;





typedef enum { WMI_TPC_CHAINMASK_CONFIG_CMDID_tlv_order_fixed_param, WMI_TPC_CHAINMASK_CONFIG_CMDID_tlv_order_config_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TPC_CHAINMASK_CONFIG_CMDID } WMI_TPC_CHAINMASK_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_tpc_chainmask_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_tpc_chainmask_config *config_list; A_UINT32 num_config_list; A_UINT32 is_allocated_config_list; } WMI_TPC_CHAINMASK_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_CHAN_AVOID_UPDATE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHAN_AVOID_UPDATE_CMDID } WMI_CHAN_AVOID_UPDATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_chan_avoid_update_cmd_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHAN_AVOID_UPDATE_CMDID_param_tlvs;;




typedef enum { WMI_CHAN_AVOID_RPT_ALLOW_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHAN_AVOID_RPT_ALLOW_CMDID } WMI_CHAN_AVOID_RPT_ALLOW_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_CHAN_AVOID_RPT_ALLOW_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHAN_AVOID_RPT_ALLOW_CMDID_param_tlvs;;




typedef enum { WMI_D0_WOW_ENABLE_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_D0_WOW_ENABLE_DISABLE_CMDID } WMI_D0_WOW_ENABLE_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_d0_wow_enable_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_D0_WOW_ENABLE_DISABLE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_TEMPERATURE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_TEMPERATURE_CMDID } WMI_PDEV_GET_TEMPERATURE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_temperature_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_TEMPERATURE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_ANTDIV_STATUS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_ANTDIV_STATUS_CMDID } WMI_PDEV_GET_ANTDIV_STATUS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_antdiv_status_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_ANTDIV_STATUS_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_ENCRYPT_DECRYPT_DATA_REQ_CMDID_tlv_order_fixed_param, WMI_VDEV_ENCRYPT_DECRYPT_DATA_REQ_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ENCRYPT_DECRYPT_DATA_REQ_CMDID } WMI_VDEV_ENCRYPT_DECRYPT_DATA_REQ_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_encrypt_decrypt_data_req_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_VDEV_ENCRYPT_DECRYPT_DATA_REQ_CMDID_param_tlvs;;




typedef enum { WMI_SET_ANTENNA_DIVERSITY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_ANTENNA_DIVERSITY_CMDID } WMI_SET_ANTENNA_DIVERSITY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_antenna_diversity_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_ANTENNA_DIVERSITY_CMDID_param_tlvs;;




typedef enum { WMI_RSSI_BREACH_MONITOR_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RSSI_BREACH_MONITOR_CONFIG_CMDID } WMI_RSSI_BREACH_MONITOR_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rssi_breach_monitor_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RSSI_BREACH_MONITOR_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_SET_DHCP_SERVER_OFFLOAD_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_DHCP_SERVER_OFFLOAD_CMDID } WMI_SET_DHCP_SERVER_OFFLOAD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_dhcp_server_offload_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_DHCP_SERVER_OFFLOAD_CMDID_param_tlvs;;




typedef enum { WMI_IPA_OFFLOAD_ENABLE_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_IPA_OFFLOAD_ENABLE_DISABLE_CMDID } WMI_IPA_OFFLOAD_ENABLE_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ipa_offload_enable_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_IPA_OFFLOAD_ENABLE_DISABLE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_LED_FLASHING_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_LED_FLASHING_CMDID } WMI_PDEV_SET_LED_FLASHING_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_led_flashing_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_LED_FLASHING_CMDID_param_tlvs;;




typedef enum { WMI_MDNS_OFFLOAD_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MDNS_OFFLOAD_ENABLE_CMDID } WMI_MDNS_OFFLOAD_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mdns_offload_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MDNS_OFFLOAD_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_MDNS_SET_FQDN_CMDID_tlv_order_fixed_param, WMI_MDNS_SET_FQDN_CMDID_tlv_order_fqdn_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MDNS_SET_FQDN_CMDID } WMI_MDNS_SET_FQDN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mdns_set_fqdn_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *fqdn_data; A_UINT32 num_fqdn_data; A_UINT32 is_allocated_fqdn_data; } WMI_MDNS_SET_FQDN_CMDID_param_tlvs;;




typedef enum { WMI_MDNS_SET_RESPONSE_CMDID_tlv_order_fixed_param, WMI_MDNS_SET_RESPONSE_CMDID_tlv_order_resp_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MDNS_SET_RESPONSE_CMDID } WMI_MDNS_SET_RESPONSE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mdns_set_resp_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *resp_data; A_UINT32 num_resp_data; A_UINT32 is_allocated_resp_data; } WMI_MDNS_SET_RESPONSE_CMDID_param_tlvs;;



typedef enum { WMI_MDNS_GET_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MDNS_GET_STATS_CMDID } WMI_MDNS_GET_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mdns_get_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MDNS_GET_STATS_CMDID_param_tlvs;;
# 3446 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_INVOKE_CMDID_tlv_order_fixed_param, WMI_ROAM_INVOKE_CMDID_tlv_order_channel_list, WMI_ROAM_INVOKE_CMDID_tlv_order_bssid_list, WMI_ROAM_INVOKE_CMDID_tlv_order_bcn_prb_buf_list, WMI_ROAM_INVOKE_CMDID_tlv_order_bcn_prb_frm, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_INVOKE_CMDID } WMI_ROAM_INVOKE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_invoke_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; wmi_mac_addr *bssid_list; A_UINT32 num_bssid_list; A_UINT32 is_allocated_bssid_list; wmi_tlv_buf_len_param *bcn_prb_buf_list; A_UINT32 num_bcn_prb_buf_list; A_UINT32 is_allocated_bcn_prb_buf_list; A_UINT8 *bcn_prb_frm; A_UINT32 num_bcn_prb_frm; A_UINT32 is_allocated_bcn_prb_frm; } WMI_ROAM_INVOKE_CMDID_param_tlvs;;





typedef enum { WMI_SAP_OFL_ENABLE_CMDID_tlv_order_fixed_param, WMI_SAP_OFL_ENABLE_CMDID_tlv_order_psk, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_OFL_ENABLE_CMDID } WMI_SAP_OFL_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sap_ofl_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *psk; A_UINT32 num_psk; A_UINT32 is_allocated_psk; } WMI_SAP_OFL_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_SAP_SET_BLACKLIST_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_SET_BLACKLIST_PARAM_CMDID } WMI_SAP_SET_BLACKLIST_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sap_set_blacklist_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAP_SET_BLACKLIST_PARAM_CMDID_param_tlvs;;




typedef enum { WMI_SAP_OBSS_DETECTION_CFG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_OBSS_DETECTION_CFG_CMDID } WMI_SAP_OBSS_DETECTION_CFG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_sap_obss_detection_cfg_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAP_OBSS_DETECTION_CFG_CMDID_param_tlvs;;




typedef enum { WMI_BSS_COLOR_CHANGE_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BSS_COLOR_CHANGE_ENABLE_CMDID } WMI_BSS_COLOR_CHANGE_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bss_color_change_enable_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BSS_COLOR_CHANGE_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_OBSS_COLOR_COLLISION_DET_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OBSS_COLOR_COLLISION_DET_CONFIG_CMDID } WMI_OBSS_COLOR_COLLISION_DET_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_obss_color_collision_det_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OBSS_COLOR_COLLISION_DET_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_APFIND_CMDID_tlv_order_fixed_param, WMI_APFIND_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_APFIND_CMDID } WMI_APFIND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_apfind_cmd_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_APFIND_CMDID_param_tlvs;;




typedef enum { WMI_OCB_SET_SCHED_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_SET_SCHED_CMDID } WMI_OCB_SET_SCHED_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_set_sched_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_SET_SCHED_CMDID_param_tlvs;;
# 3494 "include/wmi_tlv_defs.h"
typedef enum { WMI_OCB_SET_CONFIG_CMDID_tlv_order_fixed_param, WMI_OCB_SET_CONFIG_CMDID_tlv_order_chan_list, WMI_OCB_SET_CONFIG_CMDID_tlv_order_ocb_chan_list, WMI_OCB_SET_CONFIG_CMDID_tlv_order_qos_parameter_list, WMI_OCB_SET_CONFIG_CMDID_tlv_order_chan_cfg, WMI_OCB_SET_CONFIG_CMDID_tlv_order_ndl_active_state_config_list, WMI_OCB_SET_CONFIG_CMDID_tlv_order_schedule_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_SET_CONFIG_CMDID } WMI_OCB_SET_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_set_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel *chan_list; A_UINT32 num_chan_list; A_UINT32 is_allocated_chan_list; wmi_ocb_channel *ocb_chan_list; A_UINT32 num_ocb_chan_list; A_UINT32 is_allocated_ocb_chan_list; wmi_qos_parameter *qos_parameter_list; A_UINT32 num_qos_parameter_list; A_UINT32 is_allocated_qos_parameter_list; wmi_dcc_ndl_chan *chan_cfg; A_UINT32 num_chan_cfg; A_UINT32 is_allocated_chan_cfg; wmi_dcc_ndl_active_state_config *ndl_active_state_config_list; A_UINT32 num_ndl_active_state_config_list; A_UINT32 is_allocated_ndl_active_state_config_list; wmi_ocb_schedule_element *schedule_list; A_UINT32 num_schedule_list; A_UINT32 is_allocated_schedule_list; } WMI_OCB_SET_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_OCB_SET_UTC_TIME_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_SET_UTC_TIME_CMDID } WMI_OCB_SET_UTC_TIME_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_set_utc_time_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_SET_UTC_TIME_CMDID_param_tlvs;;





typedef enum { WMI_OCB_START_TIMING_ADVERT_CMDID_tlv_order_fixed_param, WMI_OCB_START_TIMING_ADVERT_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_START_TIMING_ADVERT_CMDID } WMI_OCB_START_TIMING_ADVERT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_start_timing_advert_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_OCB_START_TIMING_ADVERT_CMDID_param_tlvs;;




typedef enum { WMI_OCB_STOP_TIMING_ADVERT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_STOP_TIMING_ADVERT_CMDID } WMI_OCB_STOP_TIMING_ADVERT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_stop_timing_advert_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_STOP_TIMING_ADVERT_CMDID_param_tlvs;;




typedef enum { WMI_OCB_GET_TSF_TIMER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_GET_TSF_TIMER_CMDID } WMI_OCB_GET_TSF_TIMER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_get_tsf_timer_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_GET_TSF_TIMER_CMDID_param_tlvs;;





typedef enum { WMI_DCC_GET_STATS_CMDID_tlv_order_fixed_param, WMI_DCC_GET_STATS_CMDID_tlv_order_channel_stats_request, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_GET_STATS_CMDID } WMI_DCC_GET_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_get_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_dcc_channel_stats_request *channel_stats_request; A_UINT32 num_channel_stats_request; A_UINT32 is_allocated_channel_stats_request; } WMI_DCC_GET_STATS_CMDID_param_tlvs;;




typedef enum { WMI_DCC_CLEAR_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_CLEAR_STATS_CMDID } WMI_DCC_CLEAR_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_clear_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DCC_CLEAR_STATS_CMDID_param_tlvs;;






typedef enum { WMI_DCC_UPDATE_NDL_CMDID_tlv_order_fixed_param, WMI_DCC_UPDATE_NDL_CMDID_tlv_order_chan_ndl_list, WMI_DCC_UPDATE_NDL_CMDID_tlv_order_ndl_active_state_config_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_UPDATE_NDL_CMDID } WMI_DCC_UPDATE_NDL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_update_ndl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_dcc_ndl_chan *chan_ndl_list; A_UINT32 num_chan_ndl_list; A_UINT32 is_allocated_chan_ndl_list; wmi_dcc_ndl_active_state_config *ndl_active_state_config_list; A_UINT32 num_ndl_active_state_config_list; A_UINT32 is_allocated_ndl_active_state_config_list; } WMI_DCC_UPDATE_NDL_CMDID_param_tlvs;;
# 3544 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_FILTER_CMDID_tlv_order_fixed_param, WMI_ROAM_FILTER_CMDID_tlv_order_bssid_black_list, WMI_ROAM_FILTER_CMDID_tlv_order_ssid_white_list, WMI_ROAM_FILTER_CMDID_tlv_order_bssid_preferred_list, WMI_ROAM_FILTER_CMDID_tlv_order_bssid_preferred_factor, WMI_ROAM_FILTER_CMDID_tlv_order_lca_disallow_param, WMI_ROAM_FILTER_CMDID_tlv_order_rssi_rejection_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_FILTER_CMDID } WMI_ROAM_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_filter_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mac_addr *bssid_black_list; A_UINT32 num_bssid_black_list; A_UINT32 is_allocated_bssid_black_list; wmi_ssid *ssid_white_list; A_UINT32 num_ssid_white_list; A_UINT32 is_allocated_ssid_white_list; wmi_mac_addr *bssid_preferred_list; A_UINT32 num_bssid_preferred_list; A_UINT32 is_allocated_bssid_preferred_list; A_UINT32 *bssid_preferred_factor; A_UINT32 num_bssid_preferred_factor; A_UINT32 is_allocated_bssid_preferred_factor; wmi_roam_lca_disallow_config_tlv_param *lca_disallow_param; A_UINT32 num_lca_disallow_param; A_UINT32 is_allocated_lca_disallow_param; wmi_roam_rejection_list_config_param *rssi_rejection_list; A_UINT32 num_rssi_rejection_list; A_UINT32 is_allocated_rssi_rejection_list; } WMI_ROAM_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_TSF_TSTAMP_ACTION_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_TSF_TSTAMP_ACTION_CMDID } WMI_VDEV_TSF_TSTAMP_ACTION_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_tsf_tstamp_action_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_TSF_TSTAMP_ACTION_CMDID_param_tlvs;;





typedef enum { WMI_ROAM_SUBNET_CHANGE_CONFIG_CMDID_tlv_order_fixed_param, WMI_ROAM_SUBNET_CHANGE_CONFIG_CMDID_tlv_order_skip_subnet_change_detection_bssid_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SUBNET_CHANGE_CONFIG_CMDID } WMI_ROAM_SUBNET_CHANGE_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_subnet_change_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mac_addr *skip_subnet_change_detection_bssid_list; A_UINT32 num_skip_subnet_change_detection_bssid_list; A_UINT32 is_allocated_skip_subnet_change_detection_bssid_list; } WMI_ROAM_SUBNET_CHANGE_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_SOC_SET_PCL_CMDID_tlv_order_fixed_param, WMI_SOC_SET_PCL_CMDID_tlv_order_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_PCL_CMDID } WMI_SOC_SET_PCL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_pcl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; } WMI_SOC_SET_PCL_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_PCL_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_PCL_CMDID_tlv_order_channel_weight, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_PCL_CMDID } WMI_PDEV_SET_PCL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_pcl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_weight; A_UINT32 num_channel_weight; A_UINT32 is_allocated_channel_weight; } WMI_PDEV_SET_PCL_CMDID_param_tlvs;;




typedef enum { WMI_SOC_SET_HW_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_HW_MODE_CMDID } WMI_SOC_SET_HW_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_hw_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SOC_SET_HW_MODE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_HW_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_HW_MODE_CMDID } WMI_PDEV_SET_HW_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_hw_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_HW_MODE_CMDID_param_tlvs;;




typedef enum { WMI_SOC_SET_DUAL_MAC_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_DUAL_MAC_CONFIG_CMDID } WMI_SOC_SET_DUAL_MAC_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_dual_mac_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SOC_SET_DUAL_MAC_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_MAC_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_MAC_CONFIG_CMDID } WMI_PDEV_SET_MAC_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_mac_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_MAC_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_SOC_SET_ANTENNA_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_ANTENNA_MODE_CMDID } WMI_SOC_SET_ANTENNA_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_antenna_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SOC_SET_ANTENNA_MODE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_ANTENNA_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_ANTENNA_MODE_CMDID } WMI_PDEV_SET_ANTENNA_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_antenna_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_ANTENNA_MODE_CMDID_param_tlvs;;



typedef enum { WMI_LRO_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_LRO_CONFIG_CMDID } WMI_LRO_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_lro_info_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_LRO_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_TRANSFER_DATA_TO_FLASH_CMDID_tlv_order_fixed_param, WMI_TRANSFER_DATA_TO_FLASH_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TRANSFER_DATA_TO_FLASH_CMDID } WMI_TRANSFER_DATA_TO_FLASH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_transfer_data_to_flash_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_TRANSFER_DATA_TO_FLASH_CMDID_param_tlvs;;



typedef enum { WMI_READ_DATA_FROM_FLASH_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_READ_DATA_FROM_FLASH_CMDID } WMI_READ_DATA_FROM_FLASH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_read_data_from_flash_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_READ_DATA_FROM_FLASH_CMDID_param_tlvs;;



typedef enum { WMI_CONFIG_ENHANCED_MCAST_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CONFIG_ENHANCED_MCAST_FILTER_CMDID } WMI_CONFIG_ENHANCED_MCAST_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_config_enhanced_mcast_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CONFIG_ENHANCED_MCAST_FILTER_CMDID_param_tlvs;;



typedef enum { WMI_VDEV_WISA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_WISA_CMDID } WMI_VDEV_WISA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_wisa_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_WISA_CMDID_param_tlvs;;




typedef enum { WMI_MAWC_SENSOR_REPORT_IND_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MAWC_SENSOR_REPORT_IND_CMDID } WMI_MAWC_SENSOR_REPORT_IND_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mawc_sensor_report_ind_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MAWC_SENSOR_REPORT_IND_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_CONFIGURE_MAWC_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_CONFIGURE_MAWC_CMDID } WMI_ROAM_CONFIGURE_MAWC_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_roam_configure_mawc_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_CONFIGURE_MAWC_CMDID_param_tlvs;;




typedef enum { WMI_NLO_CONFIGURE_MAWC_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NLO_CONFIGURE_MAWC_CMDID } WMI_NLO_CONFIGURE_MAWC_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_nlo_configure_mawc_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NLO_CONFIGURE_MAWC_CMDID_param_tlvs;;




typedef enum { WMI_EXTSCAN_CONFIGURE_MAWC_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CONFIGURE_MAWC_CMDID } WMI_EXTSCAN_CONFIGURE_MAWC_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_configure_mawc_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_CONFIGURE_MAWC_CMDID_param_tlvs;;




typedef enum { WMI_COEX_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_COEX_CONFIG_CMDID } WMI_COEX_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_COEX_CONFIG_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_COEX_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_COEX_GET_ANTENNA_ISOLATION_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_COEX_GET_ANTENNA_ISOLATION_CMDID } WMI_COEX_GET_ANTENNA_ISOLATION_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_coex_get_antenna_isolation_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_COEX_GET_ANTENNA_ISOLATION_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_NFCAL_POWER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_NFCAL_POWER_CMDID } WMI_PDEV_GET_NFCAL_POWER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_nfcal_power_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_NFCAL_POWER_CMDID_param_tlvs;;




typedef enum { WMI_BPF_GET_CAPABILITY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_GET_CAPABILITY_CMDID } WMI_BPF_GET_CAPABILITY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_get_capability_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_GET_CAPABILITY_CMDID_param_tlvs;;




typedef enum { WMI_BPF_GET_VDEV_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_GET_VDEV_STATS_CMDID } WMI_BPF_GET_VDEV_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_get_vdev_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_GET_VDEV_STATS_CMDID_param_tlvs;;





typedef enum { WMI_BPF_SET_VDEV_INSTRUCTIONS_CMDID_tlv_order_fixed_param, WMI_BPF_SET_VDEV_INSTRUCTIONS_CMDID_tlv_order_buf_inst, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_SET_VDEV_INSTRUCTIONS_CMDID } WMI_BPF_SET_VDEV_INSTRUCTIONS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_set_vdev_instructions_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *buf_inst; A_UINT32 num_buf_inst; A_UINT32 is_allocated_buf_inst; } WMI_BPF_SET_VDEV_INSTRUCTIONS_CMDID_param_tlvs;;




typedef enum { WMI_BPF_DEL_VDEV_INSTRUCTIONS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_DEL_VDEV_INSTRUCTIONS_CMDID } WMI_BPF_DEL_VDEV_INSTRUCTIONS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_del_vdev_instructions_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_DEL_VDEV_INSTRUCTIONS_CMDID_param_tlvs;;



typedef enum { WMI_BPF_SET_VDEV_ACTIVE_MODE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_SET_VDEV_ACTIVE_MODE_CMDID } WMI_BPF_SET_VDEV_ACTIVE_MODE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_set_vdev_active_mode_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_SET_VDEV_ACTIVE_MODE_CMDID_param_tlvs;;



typedef enum { WMI_BPF_SET_VDEV_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_SET_VDEV_ENABLE_CMDID } WMI_BPF_SET_VDEV_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_set_vdev_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_SET_VDEV_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_BPF_SET_VDEV_WORK_MEMORY_CMDID_tlv_order_fixed_param, WMI_BPF_SET_VDEV_WORK_MEMORY_CMDID_tlv_order_buf_inst, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_SET_VDEV_WORK_MEMORY_CMDID } WMI_BPF_SET_VDEV_WORK_MEMORY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_set_vdev_work_memory_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *buf_inst; A_UINT32 num_buf_inst; A_UINT32 is_allocated_buf_inst; } WMI_BPF_SET_VDEV_WORK_MEMORY_CMDID_param_tlvs;;



typedef enum { WMI_BPF_GET_VDEV_WORK_MEMORY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_GET_VDEV_WORK_MEMORY_CMDID } WMI_BPF_GET_VDEV_WORK_MEMORY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_get_vdev_work_memory_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_GET_VDEV_WORK_MEMORY_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SMART_ANT_ENABLE_CMDID_tlv_order_fixed_param, WMI_PDEV_SMART_ANT_ENABLE_CMDID_tlv_order_gpio_handle, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SMART_ANT_ENABLE_CMDID } WMI_PDEV_SMART_ANT_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_smart_ant_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_smart_ant_gpio_handle *gpio_handle; A_UINT32 num_gpio_handle; A_UINT32 is_allocated_gpio_handle; } WMI_PDEV_SMART_ANT_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SMART_ANT_SET_RX_ANTENNA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SMART_ANT_SET_RX_ANTENNA_CMDID } WMI_PDEV_SMART_ANT_SET_RX_ANTENNA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_smart_ant_set_rx_antenna_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SMART_ANT_SET_RX_ANTENNA_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_ANTENNA_SWITCH_TABLE_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_ANTENNA_SWITCH_TABLE_CMDID_tlv_order_ant_ctrl_chain, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_ANTENNA_SWITCH_TABLE_CMDID } WMI_PDEV_SET_ANTENNA_SWITCH_TABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_ant_switch_tbl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_set_ant_ctrl_chain *ant_ctrl_chain; A_UINT32 num_ant_ctrl_chain; A_UINT32 is_allocated_ant_ctrl_chain; } WMI_PDEV_SET_ANTENNA_SWITCH_TABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_CTL_TABLE_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_CTL_TABLE_CMDID_tlv_order_ctl_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_CTL_TABLE_CMDID } WMI_PDEV_SET_CTL_TABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_ctl_table_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *ctl_info; A_UINT32 num_ctl_info; A_UINT32 is_allocated_ctl_info; } WMI_PDEV_SET_CTL_TABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_MIMOGAIN_TABLE_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_MIMOGAIN_TABLE_CMDID_tlv_order_arraygain_tbl, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_MIMOGAIN_TABLE_CMDID } WMI_PDEV_SET_MIMOGAIN_TABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_mimogain_table_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *arraygain_tbl; A_UINT32 num_arraygain_tbl; A_UINT32 is_allocated_arraygain_tbl; } WMI_PDEV_SET_MIMOGAIN_TABLE_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_FIPS_CMDID_tlv_order_fixed_param, WMI_PDEV_FIPS_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_FIPS_CMDID } WMI_PDEV_FIPS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_fips_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_FIPS_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_ANI_CCK_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_ANI_CCK_CONFIG_CMDID } WMI_PDEV_GET_ANI_CCK_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_ani_cck_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_ANI_CCK_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_ANI_OFDM_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_ANI_OFDM_CONFIG_CMDID } WMI_PDEV_GET_ANI_OFDM_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_ani_ofdm_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_ANI_OFDM_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_TPC_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_TPC_CMDID } WMI_PDEV_GET_TPC_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_tpc_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_TPC_CMDID_param_tlvs;;



typedef enum { WMI_VDEV_RATEMASK_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_RATEMASK_CMDID } WMI_VDEV_RATEMASK_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_config_ratemask_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_RATEMASK_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_ATF_REQUEST_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ATF_REQUEST_CMDID } WMI_VDEV_ATF_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_atf_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ATF_REQUEST_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_SET_DSCP_TID_MAP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_DSCP_TID_MAP_CMDID } WMI_VDEV_SET_DSCP_TID_MAP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_dscp_tid_map_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_SET_DSCP_TID_MAP_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_FILTER_NEIGHBOR_RX_PACKETS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_FILTER_NEIGHBOR_RX_PACKETS_CMDID } WMI_VDEV_FILTER_NEIGHBOR_RX_PACKETS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_filter_nrp_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_FILTER_NEIGHBOR_RX_PACKETS_CMDID_param_tlvs;;




typedef enum { WMI_PEER_UPDATE_WDS_ENTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_UPDATE_WDS_ENTRY_CMDID } WMI_PEER_UPDATE_WDS_ENTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_update_wds_entry_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_UPDATE_WDS_ENTRY_CMDID_param_tlvs;;




typedef enum { WMI_PEER_ADD_PROXY_STA_ENTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ADD_PROXY_STA_ENTRY_CMDID } WMI_PEER_ADD_PROXY_STA_ENTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_create_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_ADD_PROXY_STA_ENTRY_CMDID_param_tlvs;;





typedef enum { WMI_PEER_SMART_ANT_SET_TX_ANTENNA_CMDID_tlv_order_fixed_param, WMI_PEER_SMART_ANT_SET_TX_ANTENNA_CMDID_tlv_order_antenna_series, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SMART_ANT_SET_TX_ANTENNA_CMDID } WMI_PEER_SMART_ANT_SET_TX_ANTENNA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_smart_ant_set_tx_antenna_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_smart_ant_set_tx_antenna_series *antenna_series; A_UINT32 num_antenna_series; A_UINT32 is_allocated_antenna_series; } WMI_PEER_SMART_ANT_SET_TX_ANTENNA_CMDID_param_tlvs;;





typedef enum { WMI_PEER_SMART_ANT_SET_TRAIN_INFO_CMDID_tlv_order_fixed_param, WMI_PEER_SMART_ANT_SET_TRAIN_INFO_CMDID_tlv_order_antenna_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SMART_ANT_SET_TRAIN_INFO_CMDID } WMI_PEER_SMART_ANT_SET_TRAIN_INFO_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_smart_ant_set_train_antenna_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_smart_ant_set_train_antenna_param *antenna_param; A_UINT32 num_antenna_param; A_UINT32 is_allocated_antenna_param; } WMI_PEER_SMART_ANT_SET_TRAIN_INFO_CMDID_param_tlvs;;





typedef enum { WMI_PEER_SMART_ANT_SET_NODE_CONFIG_OPS_CMDID_tlv_order_fixed_param, WMI_PEER_SMART_ANT_SET_NODE_CONFIG_OPS_CMDID_tlv_order_args, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SMART_ANT_SET_NODE_CONFIG_OPS_CMDID } WMI_PEER_SMART_ANT_SET_NODE_CONFIG_OPS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_smart_ant_set_node_config_ops_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *args; A_UINT32 num_args; A_UINT32 is_allocated_args; } WMI_PEER_SMART_ANT_SET_NODE_CONFIG_OPS_CMDID_param_tlvs;;




typedef enum { WMI_QBOOST_CFG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_QBOOST_CFG_CMDID } WMI_QBOOST_CFG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_QBOOST_CFG_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_QBOOST_CFG_CMDID_param_tlvs;;




typedef enum { WMI_FWTEST_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_FWTEST_CMDID } WMI_FWTEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_fwtest_set_param_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_FWTEST_CMDID_param_tlvs;;





typedef enum { WMI_PEER_ATF_REQUEST_CMDID_tlv_order_fixed_param, WMI_PEER_ATF_REQUEST_CMDID_tlv_order_peer_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ATF_REQUEST_CMDID } WMI_PEER_ATF_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_atf_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_atf_peer_info *peer_info; A_UINT32 num_peer_info; A_UINT32 is_allocated_peer_info; } WMI_PEER_ATF_REQUEST_CMDID_param_tlvs;;





typedef enum { WMI_ATF_SSID_GROUPING_REQUEST_CMDID_tlv_order_fixed_param, WMI_ATF_SSID_GROUPING_REQUEST_CMDID_tlv_order_group_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ATF_SSID_GROUPING_REQUEST_CMDID } WMI_ATF_SSID_GROUPING_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_atf_ssid_grp_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_atf_group_info *group_info; A_UINT32 num_group_info; A_UINT32 is_allocated_group_info; } WMI_ATF_SSID_GROUPING_REQUEST_CMDID_param_tlvs;;





typedef enum { WMI_ATF_GROUP_WMM_AC_CONFIG_REQUEST_CMDID_tlv_order_fixed_param, WMI_ATF_GROUP_WMM_AC_CONFIG_REQUEST_CMDID_tlv_order_group_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ATF_GROUP_WMM_AC_CONFIG_REQUEST_CMDID } WMI_ATF_GROUP_WMM_AC_CONFIG_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_atf_grp_wmm_ac_cfg_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_atf_group_wmm_ac_info *group_info; A_UINT32 num_group_info; A_UINT32 is_allocated_group_info; } WMI_ATF_GROUP_WMM_AC_CONFIG_REQUEST_CMDID_param_tlvs;;





typedef enum { WMI_PEER_ATF_EXT_REQUEST_CMDID_tlv_order_fixed_param, WMI_PEER_ATF_EXT_REQUEST_CMDID_tlv_order_peer_ext_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ATF_EXT_REQUEST_CMDID } WMI_PEER_ATF_EXT_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_atf_ext_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_atf_ext_info *peer_ext_info; A_UINT32 num_peer_ext_info; A_UINT32 is_allocated_peer_ext_info; } WMI_PEER_ATF_EXT_REQUEST_CMDID_param_tlvs;;




typedef enum { WMI_SET_PERIODIC_CHANNEL_STATS_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_PERIODIC_CHANNEL_STATS_CONFIG_CMDID } WMI_SET_PERIODIC_CHANNEL_STATS_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_periodic_channel_stats_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_PERIODIC_CHANNEL_STATS_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_WAL_POWER_DEBUG_CMDID_tlv_order_fixed_param, WMI_PDEV_WAL_POWER_DEBUG_CMDID_tlv_order_args, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_WAL_POWER_DEBUG_CMDID } WMI_PDEV_WAL_POWER_DEBUG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_wal_power_debug_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *args; A_UINT32 num_args; A_UINT32 is_allocated_args; } WMI_PDEV_WAL_POWER_DEBUG_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_REORDER_TIMEOUT_VAL_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_REORDER_TIMEOUT_VAL_CMDID } WMI_PDEV_SET_REORDER_TIMEOUT_VAL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_reorder_timeout_val_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_REORDER_TIMEOUT_VAL_CMDID_param_tlvs;;




typedef enum { WMI_PEER_SET_RX_BLOCKSIZE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_SET_RX_BLOCKSIZE_CMDID } WMI_PEER_SET_RX_BLOCKSIZE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_set_rx_blocksize_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_SET_RX_BLOCKSIZE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_BWF_REQUEST_CMDID_tlv_order_fixed_param, WMI_PEER_BWF_REQUEST_CMDID_tlv_order_peer_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_BWF_REQUEST_CMDID } WMI_PEER_BWF_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_bwf_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_bwf_peer_info *peer_info; A_UINT32 num_peer_info; A_UINT32 is_allocated_peer_info; } WMI_PEER_BWF_REQUEST_CMDID_param_tlvs;;



typedef enum { WMI_RMC_SET_MANUAL_LEADER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMC_SET_MANUAL_LEADER_CMDID } WMI_RMC_SET_MANUAL_LEADER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_rmc_set_leader_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMC_SET_MANUAL_LEADER_CMDID_param_tlvs;;




typedef enum { WMI_PEER_REORDER_QUEUE_SETUP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_REORDER_QUEUE_SETUP_CMDID } WMI_PEER_REORDER_QUEUE_SETUP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_reorder_queue_setup_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_REORDER_QUEUE_SETUP_CMDID_param_tlvs;;




typedef enum { WMI_PEER_REORDER_QUEUE_REMOVE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_REORDER_QUEUE_REMOVE_CMDID } WMI_PEER_REORDER_QUEUE_REMOVE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_reorder_queue_remove_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_REORDER_QUEUE_REMOVE_CMDID_param_tlvs;;




typedef enum { WMI_MNT_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MNT_FILTER_CMDID } WMI_MNT_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_mnt_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MNT_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_GET_CHIP_POWER_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_GET_CHIP_POWER_STATS_CMDID } WMI_PDEV_GET_CHIP_POWER_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_get_chip_power_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_GET_CHIP_POWER_STATS_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_INFO_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_INFO_CMDID } WMI_VDEV_GET_MWS_COEX_INFO_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_info_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_INFO_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_GET_BCN_RECEPTION_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_BCN_RECEPTION_STATS_CMDID } WMI_VDEV_GET_BCN_RECEPTION_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_bcn_recv_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_BCN_RECEPTION_STATS_CMDID_param_tlvs;;
# 3892 "include/wmi_tlv_defs.h"
typedef enum { WMI_PDEV_SET_STATS_THRESHOLD_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_STATS_THRESHOLD_CMDID_tlv_order_cca_thresh, WMI_PDEV_SET_STATS_THRESHOLD_CMDID_tlv_order_signal_thresh, WMI_PDEV_SET_STATS_THRESHOLD_CMDID_tlv_order_tx_thresh, WMI_PDEV_SET_STATS_THRESHOLD_CMDID_tlv_order_rx_thresh, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_STATS_THRESHOLD_CMDID } WMI_PDEV_SET_STATS_THRESHOLD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_stats_threshold_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_chan_cca_stats_thresh *cca_thresh; A_UINT32 num_cca_thresh; A_UINT32 is_allocated_cca_thresh; wmi_peer_signal_stats_thresh *signal_thresh; A_UINT32 num_signal_thresh; A_UINT32 is_allocated_signal_thresh; wmi_tx_stats_thresh *tx_thresh; A_UINT32 num_tx_thresh; A_UINT32 is_allocated_tx_thresh; wmi_rx_stats_thresh *rx_thresh; A_UINT32 num_rx_thresh; A_UINT32 is_allocated_rx_thresh; } WMI_PDEV_SET_STATS_THRESHOLD_CMDID_param_tlvs;;




typedef enum { WMI_REQUEST_WLAN_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_WLAN_STATS_CMDID } WMI_REQUEST_WLAN_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_wlan_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_WLAN_STATS_CMDID_param_tlvs;;




typedef enum { WMI_REQUEST_PEER_STATS_INFO_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_PEER_STATS_INFO_CMDID } WMI_REQUEST_PEER_STATS_INFO_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_peer_stats_info_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_PEER_STATS_INFO_CMDID_param_tlvs;;




typedef enum { WMI_SET_CURRENT_COUNTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_CURRENT_COUNTRY_CMDID } WMI_SET_CURRENT_COUNTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_current_country_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_CURRENT_COUNTRY_CMDID_param_tlvs;;




typedef enum { WMI_SET_INIT_COUNTRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_INIT_COUNTRY_CMDID } WMI_SET_INIT_COUNTRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_init_country_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_INIT_COUNTRY_CMDID_param_tlvs;;




typedef enum { WMI_11D_SCAN_START_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_11D_SCAN_START_CMDID } WMI_11D_SCAN_START_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_11d_scan_start_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_11D_SCAN_START_CMDID_param_tlvs;;




typedef enum { WMI_11D_SCAN_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_11D_SCAN_STOP_CMDID } WMI_11D_SCAN_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_11d_scan_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_11D_SCAN_STOP_CMDID_param_tlvs;;




typedef enum { WMI_REQUEST_RADIO_CHAN_STATS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REQUEST_RADIO_CHAN_STATS_CMDID } WMI_REQUEST_RADIO_CHAN_STATS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_request_radio_chan_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_REQUEST_RADIO_CHAN_STATS_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_CMDID } WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_add_mac_addr_to_rx_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_CMDID_param_tlvs;;



typedef enum { WMI_HW_DATA_FILTER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HW_DATA_FILTER_CMDID } WMI_HW_DATA_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hw_data_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HW_DATA_FILTER_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID_tlv_order_fixed_param, WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID_tlv_order_vdev_ids, WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID_tlv_order_chan, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID } WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_multiple_vdev_restart_request_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *vdev_ids; A_UINT32 num_vdev_ids; A_UINT32 is_allocated_vdev_ids; wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; } WMI_PDEV_MULTIPLE_VDEV_RESTART_REQUEST_CMDID_param_tlvs;;



typedef enum { WMI_PDEV_UPDATE_PKT_ROUTING_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UPDATE_PKT_ROUTING_CMDID } WMI_PDEV_UPDATE_PKT_ROUTING_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_update_pkt_routing_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_UPDATE_PKT_ROUTING_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_CHECK_CAL_VERSION_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CHECK_CAL_VERSION_CMDID } WMI_PDEV_CHECK_CAL_VERSION_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_check_cal_version_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_CHECK_CAL_VERSION_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_DIVERSITY_GAIN_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_DIVERSITY_GAIN_CMDID_tlv_order_diversity_gains, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_DIVERSITY_GAIN_CMDID } WMI_PDEV_SET_DIVERSITY_GAIN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_diversity_gain_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *diversity_gains; A_UINT32 num_diversity_gains; A_UINT32 is_allocated_diversity_gains; } WMI_PDEV_SET_DIVERSITY_GAIN_CMDID_param_tlvs;;





typedef enum { WMI_VDEV_SET_ARP_STAT_CMDID_tlv_order_fixed_param, WMI_VDEV_SET_ARP_STAT_CMDID_tlv_order_connectivity_check_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_ARP_STAT_CMDID } WMI_VDEV_SET_ARP_STAT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_arp_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vdev_set_connectivity_check_stats *connectivity_check_stats; A_UINT32 num_connectivity_check_stats; A_UINT32 is_allocated_connectivity_check_stats; } WMI_VDEV_SET_ARP_STAT_CMDID_param_tlvs;;




typedef enum { WMI_VDEV_GET_ARP_STAT_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_ARP_STAT_CMDID } WMI_VDEV_GET_ARP_STAT_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_arp_stats_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_ARP_STAT_CMDID_param_tlvs;;





typedef enum { WMI_THERM_THROT_SET_CONF_CMDID_tlv_order_fixed_param, WMI_THERM_THROT_SET_CONF_CMDID_tlv_order_therm_throt_level_config_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_THERM_THROT_SET_CONF_CMDID } WMI_THERM_THROT_SET_CONF_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_therm_throt_config_request_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_therm_throt_level_config_info *therm_throt_level_config_info; A_UINT32 num_therm_throt_level_config_info; A_UINT32 is_allocated_therm_throt_level_config_info; } WMI_THERM_THROT_SET_CONF_CMDID_param_tlvs;;




typedef enum { WMI_HB_OIC_PING_OFFLOAD_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_OIC_PING_OFFLOAD_PARAM_CMDID } WMI_HB_OIC_PING_OFFLOAD_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_oic_ping_offload_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_OIC_PING_OFFLOAD_PARAM_CMDID_param_tlvs;;



typedef enum { WMI_HB_OIC_PING_OFFLOAD_SET_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_OIC_PING_OFFLOAD_SET_ENABLE_CMDID } WMI_HB_OIC_PING_OFFLOAD_SET_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_oic_ping_offload_set_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_OIC_PING_OFFLOAD_SET_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_HB_DHCP_LEASE_RENEW_OFFLOAD_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HB_DHCP_LEASE_RENEW_OFFLOAD_CMDID } WMI_HB_DHCP_LEASE_RENEW_OFFLOAD_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_dhcp_lease_renew_offload_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HB_DHCP_LEASE_RENEW_OFFLOAD_CMDID_param_tlvs;;




typedef enum { WMI_ROAM_BTM_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_BTM_CONFIG_CMDID } WMI_ROAM_BTM_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_btm_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ROAM_BTM_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_WLM_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLM_CONFIG_CMDID } WMI_WLM_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_wlm_config_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLM_CONFIG_CMDID_param_tlvs;;
# 4010 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_STATS_EVENTID_tlv_order_fixed_param, WMI_ROAM_STATS_EVENTID_tlv_order_roam_trigger_reason, WMI_ROAM_STATS_EVENTID_tlv_order_roam_scan_info, WMI_ROAM_STATS_EVENTID_tlv_order_roam_scan_chan_info, WMI_ROAM_STATS_EVENTID_tlv_order_roam_ap_info, WMI_ROAM_STATS_EVENTID_tlv_order_roam_result, WMI_ROAM_STATS_EVENTID_tlv_order_roam_neighbor_report_info, WMI_ROAM_STATS_EVENTID_tlv_order_roam_neighbor_report_chan_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_STATS_EVENTID } WMI_ROAM_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_roam_trigger_reason *roam_trigger_reason; A_UINT32 num_roam_trigger_reason; A_UINT32 is_allocated_roam_trigger_reason; wmi_roam_scan_info *roam_scan_info; A_UINT32 num_roam_scan_info; A_UINT32 is_allocated_roam_scan_info; wmi_roam_scan_channel_info *roam_scan_chan_info; A_UINT32 num_roam_scan_chan_info; A_UINT32 is_allocated_roam_scan_chan_info; wmi_roam_ap_info *roam_ap_info; A_UINT32 num_roam_ap_info; A_UINT32 is_allocated_roam_ap_info; wmi_roam_result *roam_result; A_UINT32 num_roam_result; A_UINT32 is_allocated_roam_result; wmi_roam_neighbor_report_info *roam_neighbor_report_info; A_UINT32 num_roam_neighbor_report_info; A_UINT32 is_allocated_roam_neighbor_report_info; wmi_roam_neighbor_report_channel_info *roam_neighbor_report_chan_info; A_UINT32 num_roam_neighbor_report_chan_info; A_UINT32 is_allocated_roam_neighbor_report_chan_info; } WMI_ROAM_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_MOTION_DET_CONFIG_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_CONFIG_PARAM_CMDID } WMI_MOTION_DET_CONFIG_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_config_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_CONFIG_PARAM_CMDID_param_tlvs;;



typedef enum { WMI_MOTION_DET_BASE_LINE_CONFIG_PARAM_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_BASE_LINE_CONFIG_PARAM_CMDID } WMI_MOTION_DET_BASE_LINE_CONFIG_PARAM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_base_line_config_params_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_BASE_LINE_CONFIG_PARAM_CMDID_param_tlvs;;



typedef enum { WMI_MOTION_DET_START_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_START_STOP_CMDID } WMI_MOTION_DET_START_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_start_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_START_STOP_CMDID_param_tlvs;;



typedef enum { WMI_MOTION_DET_BASE_LINE_START_STOP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_BASE_LINE_START_STOP_CMDID } WMI_MOTION_DET_BASE_LINE_START_STOP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_base_line_start_stop_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_BASE_LINE_START_STOP_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_AC_TX_QUEUE_OPTIMIZED_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_AC_TX_QUEUE_OPTIMIZED_CMDID } WMI_PDEV_SET_AC_TX_QUEUE_OPTIMIZED_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_ac_tx_queue_optimized_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_AC_TX_QUEUE_OPTIMIZED_CMDID_param_tlvs;;





typedef enum { WMI_PEER_TID_MSDUQ_QDEPTH_THRESH_UPDATE_CMDID_tlv_order_fixed_param, WMI_PEER_TID_MSDUQ_QDEPTH_THRESH_UPDATE_CMDID_tlv_order_msduq_qdepth_thresh_update, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TID_MSDUQ_QDEPTH_THRESH_UPDATE_CMDID } WMI_PEER_TID_MSDUQ_QDEPTH_THRESH_UPDATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tid_msduq_qdepth_thresh_update_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_msduq_qdepth_thresh_update *msduq_qdepth_thresh_update; A_UINT32 num_msduq_qdepth_thresh_update; A_UINT32 is_allocated_msduq_qdepth_thresh_update; } WMI_PEER_TID_MSDUQ_QDEPTH_THRESH_UPDATE_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_SET_RX_FILTER_PROMISCUOUS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_RX_FILTER_PROMISCUOUS_CMDID } WMI_PDEV_SET_RX_FILTER_PROMISCUOUS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_rx_filter_promiscuous_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_RX_FILTER_PROMISCUOUS_CMDID_param_tlvs;;





typedef enum { WMI_RUNTIME_DPD_RECAL_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RUNTIME_DPD_RECAL_CMDID } WMI_RUNTIME_DPD_RECAL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_runtime_dpd_recal_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RUNTIME_DPD_RECAL_CMDID_param_tlvs;;





typedef enum { WMI_GET_TPC_POWER_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_TPC_POWER_CMDID } WMI_GET_TPC_POWER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_get_tpc_power_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GET_TPC_POWER_CMDID_param_tlvs;;




typedef enum { WMI_TWT_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_ENABLE_CMDID } WMI_TWT_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_TWT_DISABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_DISABLE_CMDID } WMI_TWT_DISABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_disable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_DISABLE_CMDID_param_tlvs;;




typedef enum { WMI_TWT_ADD_DIALOG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_ADD_DIALOG_CMDID } WMI_TWT_ADD_DIALOG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_add_dialog_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_ADD_DIALOG_CMDID_param_tlvs;;




typedef enum { WMI_TWT_DEL_DIALOG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_DEL_DIALOG_CMDID } WMI_TWT_DEL_DIALOG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_del_dialog_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_DEL_DIALOG_CMDID_param_tlvs;;




typedef enum { WMI_TWT_PAUSE_DIALOG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_PAUSE_DIALOG_CMDID } WMI_TWT_PAUSE_DIALOG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_pause_dialog_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_PAUSE_DIALOG_CMDID_param_tlvs;;




typedef enum { WMI_TWT_RESUME_DIALOG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_RESUME_DIALOG_CMDID } WMI_TWT_RESUME_DIALOG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_resume_dialog_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_RESUME_DIALOG_CMDID_param_tlvs;;




typedef enum { WMI_TWT_BTWT_INVITE_STA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_BTWT_INVITE_STA_CMDID } WMI_TWT_BTWT_INVITE_STA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_btwt_invite_sta_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_BTWT_INVITE_STA_CMDID_param_tlvs;;




typedef enum { WMI_TWT_BTWT_REMOVE_STA_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_BTWT_REMOVE_STA_CMDID } WMI_TWT_BTWT_REMOVE_STA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_twt_btwt_remove_sta_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_BTWT_REMOVE_STA_CMDID_param_tlvs;;




typedef enum { WMI_PEER_TID_CONFIGURATIONS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TID_CONFIGURATIONS_CMDID } WMI_PEER_TID_CONFIGURATIONS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tid_configurations_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TID_CONFIGURATIONS_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_OBSS_PD_SPATIAL_REUSE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_OBSS_PD_SPATIAL_REUSE_CMDID } WMI_PDEV_OBSS_PD_SPATIAL_REUSE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_obss_spatial_reuse_set_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_OBSS_PD_SPATIAL_REUSE_CMDID_param_tlvs;;
# 4126 "include/wmi_tlv_defs.h"
typedef enum { WMI_PEER_CFR_CAPTURE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_CFR_CAPTURE_CMDID } WMI_PEER_CFR_CAPTURE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_cfr_capture_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_CFR_CAPTURE_CMDID_param_tlvs;;





typedef enum { WMI_PEER_CHAN_WIDTH_SWITCH_CMDID_tlv_order_fixed_param, WMI_PEER_CHAN_WIDTH_SWITCH_CMDID_tlv_order_peer_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_CHAN_WIDTH_SWITCH_CMDID } WMI_PEER_CHAN_WIDTH_SWITCH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_chan_width_switch_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_chan_width_peer_list *peer_info; A_UINT32 num_peer_info; A_UINT32 is_allocated_peer_info; } WMI_PEER_CHAN_WIDTH_SWITCH_CMDID_param_tlvs;;




typedef enum { WMI_PDEV_OBSS_PD_SPATIAL_REUSE_SET_DEF_OBSS_THRESH_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_OBSS_PD_SPATIAL_REUSE_SET_DEF_OBSS_THRESH_CMDID } WMI_PDEV_OBSS_PD_SPATIAL_REUSE_SET_DEF_OBSS_THRESH_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_obss_spatial_reuse_set_def_obss_thresh_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_OBSS_PD_SPATIAL_REUSE_SET_DEF_OBSS_THRESH_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_HE_TB_ACTION_FRM_CMDID_tlv_order_fixed_param, WMI_PDEV_HE_TB_ACTION_FRM_CMDID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_HE_TB_ACTION_FRM_CMDID } WMI_PDEV_HE_TB_ACTION_FRM_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_he_tb_action_frm_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_HE_TB_ACTION_FRM_CMDID_param_tlvs;;




typedef enum { WMI_HPCS_PULSE_START_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HPCS_PULSE_START_CMDID } WMI_HPCS_PULSE_START_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_hpcs_pulse_start_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HPCS_PULSE_START_CMDID_param_tlvs;;






typedef enum { WMI_PDEV_PKTLOG_FILTER_CMDID_tlv_order_fixed_param, WMI_PDEV_PKTLOG_FILTER_CMDID_tlv_order_pdev_pktlog_filter_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_PKTLOG_FILTER_CMDID } WMI_PDEV_PKTLOG_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_pktlog_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_pktlog_filter_info *pdev_pktlog_filter_info; A_UINT32 num_pdev_pktlog_filter_info; A_UINT32 is_allocated_pdev_pktlog_filter_info; } WMI_PDEV_PKTLOG_FILTER_CMDID_param_tlvs;;





typedef enum { WMI_PDEV_SET_RAP_CONFIG_CMDID_tlv_order_fixed_param, WMI_PDEV_SET_RAP_CONFIG_CMDID_tlv_order_rap_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_RAP_CONFIG_CMDID } WMI_PDEV_SET_RAP_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_rap_config_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_set_rap_config_on_sta_ps_tlv_param *rap_param; A_UINT32 num_rap_param; A_UINT32 is_allocated_rap_param; } WMI_PDEV_SET_RAP_CONFIG_CMDID_param_tlvs;;





typedef enum { WMI_OEM_DATA_CMDID_tlv_order_fixed_param, WMI_OEM_DATA_CMDID_tlv_order_oem_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_DATA_CMDID } WMI_OEM_DATA_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_oem_data_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *oem_data; A_UINT32 num_oem_data; A_UINT32 is_allocated_oem_data; } WMI_OEM_DATA_CMDID_param_tlvs;;




typedef enum { WMI_SET_OCL_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_OCL_CMDID } WMI_SET_OCL_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_ocl_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_OCL_CMDID_param_tlvs;;




typedef enum { WMI_SET_ELNA_BYPASS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SET_ELNA_BYPASS_CMDID } WMI_SET_ELNA_BYPASS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_set_elna_bypass_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SET_ELNA_BYPASS_CMDID_param_tlvs;;




typedef enum { WMI_GET_ELNA_BYPASS_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_ELNA_BYPASS_CMDID } WMI_GET_ELNA_BYPASS_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_get_elna_bypass_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GET_ELNA_BYPASS_CMDID_param_tlvs;;





typedef enum { WMI_GET_CHANNEL_ANI_CMDID_tlv_order_fixed_param, WMI_GET_CHANNEL_ANI_CMDID_tlv_order_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_CHANNEL_ANI_CMDID } WMI_GET_CHANNEL_ANI_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_get_channel_ani_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *channel_list; A_UINT32 num_channel_list; A_UINT32 is_allocated_channel_list; } WMI_GET_CHANNEL_ANI_CMDID_param_tlvs;;




typedef enum { WMI_AUDIO_AGGR_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AUDIO_AGGR_ENABLE_CMDID } WMI_AUDIO_AGGR_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_audio_aggr_enable_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AUDIO_AGGR_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_AUDIO_AGGR_ADD_GROUP_CMDID_tlv_order_fixed_param, WMI_AUDIO_AGGR_ADD_GROUP_CMDID_tlv_order_client_addr, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AUDIO_AGGR_ADD_GROUP_CMDID } WMI_AUDIO_AGGR_ADD_GROUP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_audio_aggr_add_group_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mac_addr *client_addr; A_UINT32 num_client_addr; A_UINT32 is_allocated_client_addr; } WMI_AUDIO_AGGR_ADD_GROUP_CMDID_param_tlvs;;



typedef enum { WMI_AUDIO_AGGR_DEL_GROUP_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AUDIO_AGGR_DEL_GROUP_CMDID } WMI_AUDIO_AGGR_DEL_GROUP_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_audio_aggr_del_group_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AUDIO_AGGR_DEL_GROUP_CMDID_param_tlvs;;




typedef enum { WMI_AUDIO_AGGR_SET_GROUP_RATE_CMDID_tlv_order_fixed_param, WMI_AUDIO_AGGR_SET_GROUP_RATE_CMDID_tlv_order_rate_set, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AUDIO_AGGR_SET_GROUP_RATE_CMDID } WMI_AUDIO_AGGR_SET_GROUP_RATE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_audio_aggr_set_group_rate_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_AUDIO_AGGR_RATE_SET_T *rate_set; A_UINT32 num_rate_set; A_UINT32 is_allocated_rate_set; } WMI_AUDIO_AGGR_SET_GROUP_RATE_CMDID_param_tlvs;;



typedef enum { WMI_AUDIO_AGGR_SET_GROUP_RETRY_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AUDIO_AGGR_SET_GROUP_RETRY_CMDID } WMI_AUDIO_AGGR_SET_GROUP_RETRY_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_audio_aggr_set_group_retry_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AUDIO_AGGR_SET_GROUP_RETRY_CMDID_param_tlvs;;





typedef enum { WMI_CFR_CAPTURE_FILTER_CMDID_tlv_order_fixed_param, WMI_CFR_CAPTURE_FILTER_CMDID_tlv_order_filter_group_config, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CFR_CAPTURE_FILTER_CMDID } WMI_CFR_CAPTURE_FILTER_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_cfr_capture_filter_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_cfr_filter_group_config *filter_group_config; A_UINT32 num_filter_group_config; A_UINT32 is_allocated_filter_group_config; } WMI_CFR_CAPTURE_FILTER_CMDID_param_tlvs;;




typedef enum { WMI_PEER_CONFIG_VLAN_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_CONFIG_VLAN_CMDID } WMI_PEER_CONFIG_VLAN_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_peer_config_vlan_cmd_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_CONFIG_VLAN_CMDID_param_tlvs;;
# 4235 "include/wmi_tlv_defs.h"
typedef enum { WMI_SERVICE_READY_EVENTID_tlv_order_fixed_param, WMI_SERVICE_READY_EVENTID_tlv_order_hal_reg_capabilities, WMI_SERVICE_READY_EVENTID_tlv_order_wmi_service_bitmap, WMI_SERVICE_READY_EVENTID_tlv_order_mem_reqs, WMI_SERVICE_READY_EVENTID_tlv_order_wlan_dbs_hw_mode_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SERVICE_READY_EVENTID } WMI_SERVICE_READY_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_service_ready_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; HAL_REG_CAPABILITIES *hal_reg_capabilities; A_UINT32 num_hal_reg_capabilities; A_UINT32 is_allocated_hal_reg_capabilities; A_UINT32 *wmi_service_bitmap; A_UINT32 num_wmi_service_bitmap; A_UINT32 is_allocated_wmi_service_bitmap; wlan_host_mem_req *mem_reqs; A_UINT32 num_mem_reqs; A_UINT32 is_allocated_mem_reqs; A_UINT32 *wlan_dbs_hw_mode_list; A_UINT32 num_wlan_dbs_hw_mode_list; A_UINT32 is_allocated_wlan_dbs_hw_mode_list; } WMI_SERVICE_READY_EVENTID_param_tlvs;;




typedef enum { WMI_SERVICE_AVAILABLE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SERVICE_AVAILABLE_EVENTID } WMI_SERVICE_AVAILABLE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_service_available_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SERVICE_AVAILABLE_EVENTID_param_tlvs;;
# 4257 "include/wmi_tlv_defs.h"
typedef enum { WMI_SERVICE_READY_EXT_EVENTID_tlv_order_fixed_param, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_soc_hw_mode_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_hw_mode_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_mac_phy_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_soc_hal_reg_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_hal_reg_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_mac_phy_chainmask_combo, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_mac_phy_chainmask_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_oem_dma_ring_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_sar_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_dma_ring_caps, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_wmi_bin_scaling_params, WMI_SERVICE_READY_EXT_EVENTID_tlv_order_wmi_chan_rf_characterization_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SERVICE_READY_EXT_EVENTID } WMI_SERVICE_READY_EXT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_service_ready_ext_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_SOC_MAC_PHY_HW_MODE_CAPS *soc_hw_mode_caps; A_UINT32 num_soc_hw_mode_caps; A_UINT32 is_allocated_soc_hw_mode_caps; WMI_HW_MODE_CAPABILITIES *hw_mode_caps; A_UINT32 num_hw_mode_caps; A_UINT32 is_allocated_hw_mode_caps; WMI_MAC_PHY_CAPABILITIES *mac_phy_caps; A_UINT32 num_mac_phy_caps; A_UINT32 is_allocated_mac_phy_caps; WMI_SOC_HAL_REG_CAPABILITIES *soc_hal_reg_caps; A_UINT32 num_soc_hal_reg_caps; A_UINT32 is_allocated_soc_hal_reg_caps; WMI_HAL_REG_CAPABILITIES_EXT *hal_reg_caps; A_UINT32 num_hal_reg_caps; A_UINT32 is_allocated_hal_reg_caps; WMI_MAC_PHY_CHAINMASK_COMBO *mac_phy_chainmask_combo; A_UINT32 num_mac_phy_chainmask_combo; A_UINT32 is_allocated_mac_phy_chainmask_combo; WMI_MAC_PHY_CHAINMASK_CAPABILITY *mac_phy_chainmask_caps; A_UINT32 num_mac_phy_chainmask_caps; A_UINT32 is_allocated_mac_phy_chainmask_caps; WMI_OEM_DMA_RING_CAPABILITIES *oem_dma_ring_caps; A_UINT32 num_oem_dma_ring_caps; A_UINT32 is_allocated_oem_dma_ring_caps; WMI_SAR_CAPABILITIES *sar_caps; A_UINT32 num_sar_caps; A_UINT32 is_allocated_sar_caps; WMI_DMA_RING_CAPABILITIES *dma_ring_caps; A_UINT32 num_dma_ring_caps; A_UINT32 is_allocated_dma_ring_caps; wmi_spectral_bin_scaling_params *wmi_bin_scaling_params; A_UINT32 num_wmi_bin_scaling_params; A_UINT32 is_allocated_wmi_bin_scaling_params; WMI_CHAN_RF_CHARACTERIZATION_INFO *wmi_chan_rf_characterization_info; A_UINT32 num_wmi_chan_rf_characterization_info; A_UINT32 is_allocated_wmi_chan_rf_characterization_info; } WMI_SERVICE_READY_EXT_EVENTID_param_tlvs;;






typedef enum { WMI_SERVICE_READY_EXT2_EVENTID_tlv_order_fixed_param, WMI_SERVICE_READY_EXT2_EVENTID_tlv_order_dma_ring_caps, WMI_SERVICE_READY_EXT2_EVENTID_tlv_order_wmi_bin_scaling_params, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SERVICE_READY_EXT2_EVENTID } WMI_SERVICE_READY_EXT2_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_service_ready_ext2_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_DMA_RING_CAPABILITIES *dma_ring_caps; A_UINT32 num_dma_ring_caps; A_UINT32 is_allocated_dma_ring_caps; wmi_spectral_bin_scaling_params *wmi_bin_scaling_params; A_UINT32 num_wmi_bin_scaling_params; A_UINT32 is_allocated_wmi_bin_scaling_params; } WMI_SERVICE_READY_EXT2_EVENTID_param_tlvs;;




typedef enum { WMI_CHAN_RF_CHARACTERIZATION_INFO_EVENTID_tlv_order_fixed_param, WMI_CHAN_RF_CHARACTERIZATION_INFO_EVENTID_tlv_order_wmi_chan_rf_characterization_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHAN_RF_CHARACTERIZATION_INFO_EVENTID } WMI_CHAN_RF_CHARACTERIZATION_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_chan_rf_characterization_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WMI_CHAN_RF_CHARACTERIZATION_INFO *wmi_chan_rf_characterization_info; A_UINT32 num_wmi_chan_rf_characterization_info; A_UINT32 is_allocated_wmi_chan_rf_characterization_info; } WMI_CHAN_RF_CHARACTERIZATION_INFO_EVENTID_param_tlvs;;






typedef enum { WMI_IFACE_COMBINATION_IND_EVENTID_tlv_order_fixed_param, WMI_IFACE_COMBINATION_IND_EVENTID_tlv_order_combinations, WMI_IFACE_COMBINATION_IND_EVENTID_tlv_order_limits, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_IFACE_COMBINATION_IND_EVENTID } WMI_IFACE_COMBINATION_IND_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_wlanfw_iface_cmb_ind_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wlanfw_iface_combination *combinations; A_UINT32 num_combinations; A_UINT32 is_allocated_combinations; wlanfw_iface_limit *limits; A_UINT32 num_limits; A_UINT32 is_allocated_limits; } WMI_IFACE_COMBINATION_IND_EVENTID_param_tlvs;;





typedef enum { WMI_READY_EVENTID_tlv_order_fixed_param, WMI_READY_EVENTID_tlv_order_mac_addr_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_READY_EVENTID } WMI_READY_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ready_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mac_addr *mac_addr_list; A_UINT32 num_mac_addr_list; A_UINT32 is_allocated_mac_addr_list; } WMI_READY_EVENTID_param_tlvs;;




typedef enum { WMI_SCAN_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SCAN_EVENTID } WMI_SCAN_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_scan_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SCAN_EVENTID_param_tlvs;;





typedef enum { WMI_EXTSCAN_START_STOP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_START_STOP_EVENTID } WMI_EXTSCAN_START_STOP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_start_stop_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_START_STOP_EVENTID_param_tlvs;;






typedef enum { WMI_EXTSCAN_OPERATION_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_OPERATION_EVENTID_tlv_order_bucket_id, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_OPERATION_EVENTID } WMI_EXTSCAN_OPERATION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_operation_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *bucket_id; A_UINT32 num_bucket_id; A_UINT32 is_allocated_bucket_id; } WMI_EXTSCAN_OPERATION_EVENTID_param_tlvs;;





typedef enum { WMI_EXTSCAN_TABLE_USAGE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_TABLE_USAGE_EVENTID } WMI_EXTSCAN_TABLE_USAGE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_table_usage_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_EXTSCAN_TABLE_USAGE_EVENTID_param_tlvs;;
# 4315 "include/wmi_tlv_defs.h"
typedef enum { WMI_EXTSCAN_CACHED_RESULTS_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_CACHED_RESULTS_EVENTID_tlv_order_bssid_list, WMI_EXTSCAN_CACHED_RESULTS_EVENTID_tlv_order_rssi_list, WMI_EXTSCAN_CACHED_RESULTS_EVENTID_tlv_order_ie_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CACHED_RESULTS_EVENTID } WMI_EXTSCAN_CACHED_RESULTS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_cached_results_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_wlan_descriptor *bssid_list; A_UINT32 num_bssid_list; A_UINT32 is_allocated_bssid_list; wmi_extscan_rssi_info *rssi_list; A_UINT32 num_rssi_list; A_UINT32 is_allocated_rssi_list; A_UINT8 *ie_list; A_UINT32 num_ie_list; A_UINT32 is_allocated_ie_list; } WMI_EXTSCAN_CACHED_RESULTS_EVENTID_param_tlvs;;







typedef enum { WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID_tlv_order_bssid_signal_descriptor_list, WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID_tlv_order_rssi_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID } WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_wlan_change_results_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_wlan_change_result_bssid *bssid_signal_descriptor_list; A_UINT32 num_bssid_signal_descriptor_list; A_UINT32 is_allocated_bssid_signal_descriptor_list; A_UINT8 *rssi_list; A_UINT32 num_rssi_list; A_UINT32 is_allocated_rssi_list; } WMI_EXTSCAN_WLAN_CHANGE_RESULTS_EVENTID_param_tlvs;;






typedef enum { WMI_EXTSCAN_HOTLIST_MATCH_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_HOTLIST_MATCH_EVENTID_tlv_order_hotlist_match, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_HOTLIST_MATCH_EVENTID } WMI_EXTSCAN_HOTLIST_MATCH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_hotlist_match_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_wlan_descriptor *hotlist_match; A_UINT32 num_hotlist_match; A_UINT32 is_allocated_hotlist_match; } WMI_EXTSCAN_HOTLIST_MATCH_EVENTID_param_tlvs;;
# 4339 "include/wmi_tlv_defs.h"
typedef enum { WMI_EXTSCAN_CAPABILITIES_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_CAPABILITIES_EVENTID_tlv_order_extscan_cache_capabilities, WMI_EXTSCAN_CAPABILITIES_EVENTID_tlv_order_wlan_change_capabilities, WMI_EXTSCAN_CAPABILITIES_EVENTID_tlv_order_hotlist_capabilities, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_CAPABILITIES_EVENTID } WMI_EXTSCAN_CAPABILITIES_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_capabilities_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_cache_capabilities *extscan_cache_capabilities; A_UINT32 num_extscan_cache_capabilities; A_UINT32 is_allocated_extscan_cache_capabilities; wmi_extscan_wlan_change_monitor_capabilities *wlan_change_capabilities; A_UINT32 num_wlan_change_capabilities; A_UINT32 is_allocated_wlan_change_capabilities; wmi_extscan_hotlist_monitor_capabilities *hotlist_capabilities; A_UINT32 num_hotlist_capabilities; A_UINT32 is_allocated_hotlist_capabilities; } WMI_EXTSCAN_CAPABILITIES_EVENTID_param_tlvs;;






typedef enum { WMI_EXTSCAN_HOTLIST_SSID_MATCH_EVENTID_tlv_order_fixed_param, WMI_EXTSCAN_HOTLIST_SSID_MATCH_EVENTID_tlv_order_hotlist_ssid_match, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_EXTSCAN_HOTLIST_SSID_MATCH_EVENTID } WMI_EXTSCAN_HOTLIST_SSID_MATCH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_extscan_hotlist_ssid_match_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_extscan_wlan_descriptor *hotlist_ssid_match; A_UINT32 num_hotlist_ssid_match; A_UINT32 is_allocated_hotlist_ssid_match; } WMI_EXTSCAN_HOTLIST_SSID_MATCH_EVENTID_param_tlvs;;




typedef enum { WMI_UPDATE_WHAL_MIB_STATS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPDATE_WHAL_MIB_STATS_EVENTID } WMI_UPDATE_WHAL_MIB_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_update_whal_mib_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_UPDATE_WHAL_MIB_STATS_EVENTID_param_tlvs;;







typedef enum { WMI_PDEV_TPC_CONFIG_EVENTID_tlv_order_fixed_param, WMI_PDEV_TPC_CONFIG_EVENTID_tlv_order_ratesArray, WMI_PDEV_TPC_CONFIG_EVENTID_tlv_order_ctlPwrTbl_param, WMI_PDEV_TPC_CONFIG_EVENTID_tlv_order_ctlPwrTbl_buf, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_TPC_CONFIG_EVENTID } WMI_PDEV_TPC_CONFIG_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_tpc_config_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ratesArray; A_UINT32 num_ratesArray; A_UINT32 is_allocated_ratesArray; wmi_tlv_arrays_len_param *ctlPwrTbl_param; A_UINT32 num_ctlPwrTbl_param; A_UINT32 is_allocated_ctlPwrTbl_param; A_UINT8 *ctlPwrTbl_buf; A_UINT32 num_ctlPwrTbl_buf; A_UINT32 is_allocated_ctlPwrTbl_buf; } WMI_PDEV_TPC_CONFIG_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_DIV_RSSI_ANTID_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DIV_RSSI_ANTID_EVENTID } WMI_PDEV_DIV_RSSI_ANTID_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_div_rssi_antid_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DIV_RSSI_ANTID_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_BSS_CHAN_INFO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_BSS_CHAN_INFO_EVENTID } WMI_PDEV_BSS_CHAN_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_bss_chan_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_BSS_CHAN_INFO_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_UPDATE_CTLTABLE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UPDATE_CTLTABLE_EVENTID } WMI_PDEV_UPDATE_CTLTABLE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_update_ctltable_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_UPDATE_CTLTABLE_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_TX_POWER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_TX_POWER_EVENTID } WMI_VDEV_GET_TX_POWER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_tx_power_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_TX_POWER_EVENTID_param_tlvs;;




typedef enum { WMI_CHAN_INFO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHAN_INFO_EVENTID } WMI_CHAN_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_chan_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHAN_INFO_EVENTID_param_tlvs;;






typedef enum { WMI_PHYERR_EVENTID_tlv_order_hdr, WMI_PHYERR_EVENTID_tlv_order_bufp, WMI_PHYERR_EVENTID_tlv_order_single_phyerr_ext, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PHYERR_EVENTID } WMI_PHYERR_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_comb_phyerr_rx_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; wmi_single_phyerr_ext_rx_hdr *single_phyerr_ext; A_UINT32 num_single_phyerr_ext; A_UINT32 is_allocated_single_phyerr_ext; } WMI_PHYERR_EVENTID_param_tlvs;;




typedef enum { WMI_TX_PAUSE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TX_PAUSE_EVENTID } WMI_TX_PAUSE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tx_pause_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TX_PAUSE_EVENTID_param_tlvs;;





typedef enum { WMI_MGMT_TX_COMPLETION_EVENTID_tlv_order_fixed_param, WMI_MGMT_TX_COMPLETION_EVENTID_tlv_order_mgmt_hdr, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MGMT_TX_COMPLETION_EVENTID } WMI_MGMT_TX_COMPLETION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_tx_compl_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_mgmt_hdr *mgmt_hdr; A_UINT32 num_mgmt_hdr; A_UINT32 is_allocated_mgmt_hdr; } WMI_MGMT_TX_COMPLETION_EVENTID_param_tlvs;;




typedef enum { WMI_OFFCHAN_DATA_TX_COMPLETION_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OFFCHAN_DATA_TX_COMPLETION_EVENTID } WMI_OFFCHAN_DATA_TX_COMPLETION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_offchan_data_tx_compl_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OFFCHAN_DATA_TX_COMPLETION_EVENTID_param_tlvs;;
# 4417 "include/wmi_tlv_defs.h"
typedef enum { WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_fixed_param, WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_desc_ids, WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_status, WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_ppdu_id, WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_ack_rssi, WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_tlv_order_mgmt_hdr, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID } WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_tx_compl_bundle_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *desc_ids; A_UINT32 num_desc_ids; A_UINT32 is_allocated_desc_ids; A_UINT32 *status; A_UINT32 num_status; A_UINT32 is_allocated_status; A_UINT32 *ppdu_id; A_UINT32 num_ppdu_id; A_UINT32 is_allocated_ppdu_id; A_UINT32 *ack_rssi; A_UINT32 num_ack_rssi; A_UINT32 is_allocated_ack_rssi; wmi_mgmt_hdr *mgmt_hdr; A_UINT32 num_mgmt_hdr; A_UINT32 is_allocated_mgmt_hdr; } WMI_MGMT_TX_BUNDLE_COMPLETION_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_START_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_START_RESP_EVENTID } WMI_VDEV_START_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_start_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_START_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_STOPPED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_STOPPED_EVENTID } WMI_VDEV_STOPPED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_stopped_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_STOPPED_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_DELETE_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DELETE_RESP_EVENTID } WMI_VDEV_DELETE_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_delete_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DELETE_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_INSTALL_KEY_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_INSTALL_KEY_COMPLETE_EVENTID } WMI_VDEV_INSTALL_KEY_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_install_key_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_INSTALL_KEY_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_STA_KICKOUT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_STA_KICKOUT_EVENTID } WMI_PEER_STA_KICKOUT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_sta_kickout_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_STA_KICKOUT_EVENTID_param_tlvs;;






typedef enum { WMI_MGMT_RX_EVENTID_tlv_order_hdr, WMI_MGMT_RX_EVENTID_tlv_order_bufp, WMI_MGMT_RX_EVENTID_tlv_order_rssi_ctl_ext, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MGMT_RX_EVENTID } WMI_MGMT_RX_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_rx_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; wmi_rssi_ctl_ext *rssi_ctl_ext; A_UINT32 num_rssi_ctl_ext; A_UINT32 is_allocated_rssi_ctl_ext; } WMI_MGMT_RX_EVENTID_param_tlvs;;







typedef enum { WMI_TBTTOFFSET_UPDATE_EVENTID_tlv_order_fixed_param, WMI_TBTTOFFSET_UPDATE_EVENTID_tlv_order_tbttoffset_list, WMI_TBTTOFFSET_UPDATE_EVENTID_tlv_order_tbtt_qtime_low_us_list, WMI_TBTTOFFSET_UPDATE_EVENTID_tlv_order_tbtt_qtime_high_us_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TBTTOFFSET_UPDATE_EVENTID } WMI_TBTTOFFSET_UPDATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tbtt_offset_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *tbttoffset_list; A_UINT32 num_tbttoffset_list; A_UINT32 is_allocated_tbttoffset_list; A_UINT32 *tbtt_qtime_low_us_list; A_UINT32 num_tbtt_qtime_low_us_list; A_UINT32 is_allocated_tbtt_qtime_low_us_list; A_UINT32 *tbtt_qtime_high_us_list; A_UINT32 num_tbtt_qtime_high_us_list; A_UINT32 is_allocated_tbtt_qtime_high_us_list; } WMI_TBTTOFFSET_UPDATE_EVENTID_param_tlvs;;





typedef enum { WMI_TBTTOFFSET_EXT_UPDATE_EVENTID_tlv_order_fixed_param, WMI_TBTTOFFSET_EXT_UPDATE_EVENTID_tlv_order_tbtt_offset_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TBTTOFFSET_EXT_UPDATE_EVENTID } WMI_TBTTOFFSET_EXT_UPDATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tbtt_offset_ext_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_tbtt_offset_info *tbtt_offset_info; A_UINT32 num_tbtt_offset_info; A_UINT32 is_allocated_tbtt_offset_info; } WMI_TBTTOFFSET_EXT_UPDATE_EVENTID_param_tlvs;;




typedef enum { WMI_TX_DELBA_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TX_DELBA_COMPLETE_EVENTID } WMI_TX_DELBA_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tx_delba_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TX_DELBA_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TX_ADDBA_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TX_ADDBA_COMPLETE_EVENTID } WMI_TX_ADDBA_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tx_addba_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TX_ADDBA_COMPLETE_EVENTID_param_tlvs;;






typedef enum { WMI_BA_RSP_SSN_EVENTID_tlv_order_fixed_param, WMI_BA_RSP_SSN_EVENTID_tlv_order_ba_event_ssn_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BA_RSP_SSN_EVENTID } WMI_BA_RSP_SSN_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ba_rsp_ssn_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ba_event_ssn *ba_event_ssn_list; A_UINT32 num_ba_event_ssn_list; A_UINT32 is_allocated_ba_event_ssn_list; } WMI_BA_RSP_SSN_EVENTID_param_tlvs;;




typedef enum { WMI_AGGR_STATE_TRIG_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AGGR_STATE_TRIG_EVENTID } WMI_AGGR_STATE_TRIG_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_aggr_state_trig_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_AGGR_STATE_TRIG_EVENTID_param_tlvs;;







typedef enum { WMI_ROAM_EVENTID_tlv_order_fixed_param, WMI_ROAM_EVENTID_tlv_order_deauth_disassoc_frame, WMI_ROAM_EVENTID_tlv_order_hw_mode_transition_fixed_param, WMI_ROAM_EVENTID_tlv_order_wmi_pdev_set_hw_mode_response_vdev_mac_mapping, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_EVENTID } WMI_ROAM_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *deauth_disassoc_frame; A_UINT32 num_deauth_disassoc_frame; A_UINT32 is_allocated_deauth_disassoc_frame; wmi_pdev_hw_mode_transition_event_fixed_param *hw_mode_transition_fixed_param; A_UINT32 num_hw_mode_transition_fixed_param; A_UINT32 is_allocated_hw_mode_transition_fixed_param; wmi_pdev_set_hw_mode_response_vdev_mac_entry *wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; } WMI_ROAM_EVENTID_param_tlvs;;
# 4508 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_SYNCH_EVENTID_tlv_order_fixed_param, WMI_ROAM_SYNCH_EVENTID_tlv_order_bcn_probe_rsp_frame, WMI_ROAM_SYNCH_EVENTID_tlv_order_reassoc_rsp_frame, WMI_ROAM_SYNCH_EVENTID_tlv_order_chan, WMI_ROAM_SYNCH_EVENTID_tlv_order_key, WMI_ROAM_SYNCH_EVENTID_tlv_order_status, WMI_ROAM_SYNCH_EVENTID_tlv_order_reassoc_req_frame, WMI_ROAM_SYNCH_EVENTID_tlv_order_hw_mode_transition_fixed_param, WMI_ROAM_SYNCH_EVENTID_tlv_order_wmi_pdev_set_hw_mode_response_vdev_mac_mapping, WMI_ROAM_SYNCH_EVENTID_tlv_order_roam_fils_synch_info, WMI_ROAM_SYNCH_EVENTID_tlv_order_key_ext, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SYNCH_EVENTID } WMI_ROAM_SYNCH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_synch_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bcn_probe_rsp_frame; A_UINT32 num_bcn_probe_rsp_frame; A_UINT32 is_allocated_bcn_probe_rsp_frame; A_UINT8 *reassoc_rsp_frame; A_UINT32 num_reassoc_rsp_frame; A_UINT32 is_allocated_reassoc_rsp_frame; wmi_channel *chan; A_UINT32 num_chan; A_UINT32 is_allocated_chan; wmi_key_material *key; A_UINT32 num_key; A_UINT32 is_allocated_key; A_UINT32 *status; A_UINT32 num_status; A_UINT32 is_allocated_status; A_UINT8 *reassoc_req_frame; A_UINT32 num_reassoc_req_frame; A_UINT32 is_allocated_reassoc_req_frame; wmi_pdev_hw_mode_transition_event_fixed_param *hw_mode_transition_fixed_param; A_UINT32 num_hw_mode_transition_fixed_param; A_UINT32 is_allocated_hw_mode_transition_fixed_param; wmi_pdev_set_hw_mode_response_vdev_mac_entry *wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; wmi_roam_fils_synch_tlv_param *roam_fils_synch_info; A_UINT32 num_roam_fils_synch_info; A_UINT32 is_allocated_roam_fils_synch_info; wmi_key_material_ext *key_ext; A_UINT32 num_key_ext; A_UINT32 is_allocated_key_ext; } WMI_ROAM_SYNCH_EVENTID_param_tlvs;;







typedef enum { WMI_ROAM_SYNCH_FRAME_EVENTID_tlv_order_fixed_param, WMI_ROAM_SYNCH_FRAME_EVENTID_tlv_order_bcn_probe_rsp_frame, WMI_ROAM_SYNCH_FRAME_EVENTID_tlv_order_reassoc_rsp_frame, WMI_ROAM_SYNCH_FRAME_EVENTID_tlv_order_reassoc_req_frame, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SYNCH_FRAME_EVENTID } WMI_ROAM_SYNCH_FRAME_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_synch_frame_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bcn_probe_rsp_frame; A_UINT32 num_bcn_probe_rsp_frame; A_UINT32 is_allocated_bcn_probe_rsp_frame; A_UINT8 *reassoc_rsp_frame; A_UINT32 num_reassoc_rsp_frame; A_UINT32 is_allocated_reassoc_rsp_frame; A_UINT8 *reassoc_req_frame; A_UINT32 num_reassoc_req_frame; A_UINT32 is_allocated_reassoc_req_frame; } WMI_ROAM_SYNCH_FRAME_EVENTID_param_tlvs;;
# 4530 "include/wmi_tlv_defs.h"
typedef enum { WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_fixed_param, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_wow_bitmap_info, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_wow_packet_buffer, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_hb_indevt, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_wow_gtkigtk, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_wow_oic_ping_handoff, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_wow_dhcp_lease_renew, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_md_indevt, WMI_WOW_WAKEUP_HOST_EVENTID_tlv_order_bl_indevt, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_WAKEUP_HOST_EVENTID } WMI_WOW_WAKEUP_HOST_EVENTID_TAG_ORDER_enum_type; typedef struct { WOW_EVENT_INFO_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; WOW_EVENT_INFO_SECTION_BITMAP *wow_bitmap_info; A_UINT32 num_wow_bitmap_info; A_UINT32 is_allocated_wow_bitmap_info; A_UINT8 *wow_packet_buffer; A_UINT32 num_wow_packet_buffer; A_UINT32 is_allocated_wow_packet_buffer; wmi_hb_ind_event_fixed_param *hb_indevt; A_UINT32 num_hb_indevt; A_UINT32 is_allocated_hb_indevt; WMI_GTK_OFFLOAD_STATUS_EVENT_fixed_param *wow_gtkigtk; A_UINT32 num_wow_gtkigtk; A_UINT32 is_allocated_wow_gtkigtk; wmi_oic_ping_handoff_event *wow_oic_ping_handoff; A_UINT32 num_wow_oic_ping_handoff; A_UINT32 is_allocated_wow_oic_ping_handoff; wmi_dhcp_lease_renew_event *wow_dhcp_lease_renew; A_UINT32 num_wow_dhcp_lease_renew; A_UINT32 is_allocated_wow_dhcp_lease_renew; wmi_motion_det_event *md_indevt; A_UINT32 num_md_indevt; A_UINT32 is_allocated_md_indevt; wmi_motion_det_base_line_event *bl_indevt; A_UINT32 num_bl_indevt; A_UINT32 is_allocated_bl_indevt; } WMI_WOW_WAKEUP_HOST_EVENTID_param_tlvs;;



typedef enum { WMI_WOW_INITIAL_WAKEUP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WOW_INITIAL_WAKEUP_EVENTID } WMI_WOW_INITIAL_WAKEUP_EVENTID_TAG_ORDER_enum_type; typedef struct { WOW_INITIAL_WAKEUP_EVENT_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WOW_INITIAL_WAKEUP_EVENTID_param_tlvs;;




typedef enum { WMI_RTT_ERROR_REPORT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RTT_ERROR_REPORT_EVENTID } WMI_RTT_ERROR_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RTT_ERROR_REPORT_EVENTID_param_tlvs;;




typedef enum { WMI_ECHO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ECHO_EVENTID } WMI_ECHO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_echo_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ECHO_EVENTID_param_tlvs;;





typedef enum { WMI_PDEV_FTM_INTG_EVENTID_tlv_order_fixed_param, WMI_PDEV_FTM_INTG_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_FTM_INTG_EVENTID } WMI_PDEV_FTM_INTG_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ftm_intg_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_FTM_INTG_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_KEEPALIVE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_KEEPALIVE_EVENTID } WMI_VDEV_GET_KEEPALIVE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_keepalive_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_KEEPALIVE_EVENTID_param_tlvs;;




typedef enum { WMI_GET_TPC_POWER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_TPC_POWER_EVENTID } WMI_GET_TPC_POWER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_get_tpc_power_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GET_TPC_POWER_EVENTID_param_tlvs;;




typedef enum { WMI_MUEDCA_PARAMS_CONFIG_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MUEDCA_PARAMS_CONFIG_EVENTID } WMI_MUEDCA_PARAMS_CONFIG_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_muedca_params_config_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MUEDCA_PARAMS_CONFIG_EVENTID_param_tlvs;;




typedef enum { WMI_GPIO_INPUT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GPIO_INPUT_EVENTID } WMI_GPIO_INPUT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_gpio_input_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GPIO_INPUT_EVENTID_param_tlvs;;




typedef enum { WMI_CSA_HANDLING_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CSA_HANDLING_EVENTID } WMI_CSA_HANDLING_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_csa_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CSA_HANDLING_EVENTID_param_tlvs;;




typedef enum { WMI_RFKILL_STATE_CHANGE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RFKILL_STATE_CHANGE_EVENTID } WMI_RFKILL_STATE_CHANGE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_rfkill_mode_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RFKILL_STATE_CHANGE_EVENTID_param_tlvs;;





typedef enum { WMI_DEBUG_MESG_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DEBUG_MESG_EVENTID } WMI_DEBUG_MESG_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_DEBUG_MESG_EVENTID_param_tlvs;;




typedef enum { WMI_DEBUG_MESG_FLUSH_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_DEBUG_MESG_FLUSH_COMPLETE_EVENTID_tlv_order_data_stall, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DEBUG_MESG_FLUSH_COMPLETE_EVENTID } WMI_DEBUG_MESG_FLUSH_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_debug_mesg_flush_complete_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_debug_mesg_fw_data_stall_param *data_stall; A_UINT32 num_data_stall; A_UINT32 is_allocated_data_stall; } WMI_DEBUG_MESG_FLUSH_COMPLETE_EVENTID_param_tlvs;;



typedef enum { WMI_RSSI_BREACH_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RSSI_BREACH_EVENTID } WMI_RSSI_BREACH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_rssi_breach_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RSSI_BREACH_EVENTID_param_tlvs;;



typedef enum { WMI_TRANSFER_DATA_TO_FLASH_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TRANSFER_DATA_TO_FLASH_COMPLETE_EVENTID } WMI_TRANSFER_DATA_TO_FLASH_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_transfer_data_to_flash_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TRANSFER_DATA_TO_FLASH_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_READ_DATA_FROM_FLASH_EVENTID_tlv_order_fixed_param, WMI_READ_DATA_FROM_FLASH_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_READ_DATA_FROM_FLASH_EVENTID } WMI_READ_DATA_FROM_FLASH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_read_data_from_flash_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_READ_DATA_FROM_FLASH_EVENTID_param_tlvs;;




typedef enum { WMI_DIAG_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DIAG_EVENTID } WMI_DIAG_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_DIAG_EVENTID_param_tlvs;;




typedef enum { WMI_GTK_OFFLOAD_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GTK_OFFLOAD_STATUS_EVENTID } WMI_GTK_OFFLOAD_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { WMI_GTK_OFFLOAD_STATUS_EVENT_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GTK_OFFLOAD_STATUS_EVENTID_param_tlvs;;






typedef enum { WMI_DCS_INTERFERENCE_EVENTID_tlv_order_fixed_param, WMI_DCS_INTERFERENCE_EVENTID_tlv_order_cw_int, WMI_DCS_INTERFERENCE_EVENTID_tlv_order_wlan_stat, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCS_INTERFERENCE_EVENTID } WMI_DCS_INTERFERENCE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dcs_interference_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wlan_dcs_cw_int *cw_int; A_UINT32 num_cw_int; A_UINT32 is_allocated_cw_int; wlan_dcs_im_tgt_stats_t *wlan_stat; A_UINT32 num_wlan_stat; A_UINT32 is_allocated_wlan_stat; } WMI_DCS_INTERFERENCE_EVENTID_param_tlvs;;





typedef enum { WMI_WLAN_PROFILE_DATA_EVENTID_tlv_order_profile_ctx, WMI_WLAN_PROFILE_DATA_EVENTID_tlv_order_profile_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_PROFILE_DATA_EVENTID } WMI_WLAN_PROFILE_DATA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_wlan_profile_ctx_t *profile_ctx; A_UINT32 num_profile_ctx; A_UINT32 is_allocated_profile_ctx; wmi_wlan_profile_t *profile_data; A_UINT32 num_profile_data; A_UINT32 is_allocated_profile_data; } WMI_WLAN_PROFILE_DATA_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_UTF_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UTF_EVENTID } WMI_PDEV_UTF_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_UTF_EVENTID_param_tlvs;;





typedef enum { WMI_PDEV_UTF_SCPC_EVENTID_tlv_order_fixed_param, WMI_PDEV_UTF_SCPC_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_UTF_SCPC_EVENTID } WMI_PDEV_UTF_SCPC_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_scpc_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_UTF_SCPC_EVENTID_param_tlvs;;




typedef enum { WMI_DEBUG_PRINT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DEBUG_PRINT_EVENTID } WMI_DEBUG_PRINT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_DEBUG_PRINT_EVENTID_param_tlvs;;




typedef enum { WMI_RTT_MEASUREMENT_REPORT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RTT_MEASUREMENT_REPORT_EVENTID } WMI_RTT_MEASUREMENT_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_RTT_MEASUREMENT_REPORT_EVENTID_param_tlvs;;




typedef enum { WMI_OEM_MEASUREMENT_REPORT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_MEASUREMENT_REPORT_EVENTID } WMI_OEM_MEASUREMENT_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_MEASUREMENT_REPORT_EVENTID_param_tlvs;;




typedef enum { WMI_OEM_ERROR_REPORT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_ERROR_REPORT_EVENTID } WMI_OEM_ERROR_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_ERROR_REPORT_EVENTID_param_tlvs;;




typedef enum { WMI_OEM_CAPABILITY_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_CAPABILITY_EVENTID } WMI_OEM_CAPABILITY_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_CAPABILITY_EVENTID_param_tlvs;;






typedef enum { WMI_OEM_RESPONSE_EVENTID_tlv_order_data, WMI_OEM_RESPONSE_EVENTID_tlv_order_indirect_data, WMI_OEM_RESPONSE_EVENTID_tlv_order_data2, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_RESPONSE_EVENTID } WMI_OEM_RESPONSE_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; wmi_oem_indirect_data *indirect_data; A_UINT32 num_indirect_data; A_UINT32 is_allocated_indirect_data; A_UINT8 *data2; A_UINT32 num_data2; A_UINT32 is_allocated_data2; } WMI_OEM_RESPONSE_EVENTID_param_tlvs;;





typedef enum { WMI_OEM_DMA_BUF_RELEASE_EVENTID_tlv_order_fixed_param, WMI_OEM_DMA_BUF_RELEASE_EVENTID_tlv_order_entries, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_DMA_BUF_RELEASE_EVENTID } WMI_OEM_DMA_BUF_RELEASE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_oem_dma_buf_release_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_oem_dma_buf_release_entry *entries; A_UINT32 num_entries; A_UINT32 is_allocated_entries; } WMI_OEM_DMA_BUF_RELEASE_EVENTID_param_tlvs;;





typedef enum { WMI_OEM_DATA_EVENTID_tlv_order_fixed_param, WMI_OEM_DATA_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_DATA_EVENTID } WMI_OEM_DATA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_oem_data_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_OEM_DATA_EVENTID_param_tlvs;;
# 4691 "include/wmi_tlv_defs.h"
typedef enum { WMI_HOST_SWBA_EVENTID_tlv_order_fixed_param, WMI_HOST_SWBA_EVENTID_tlv_order_tim_info, WMI_HOST_SWBA_EVENTID_tlv_order_p2p_noa_info, WMI_HOST_SWBA_EVENTID_tlv_order_quiet_offload_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HOST_SWBA_EVENTID } WMI_HOST_SWBA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_host_swba_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_tim_info *tim_info; A_UINT32 num_tim_info; A_UINT32 is_allocated_tim_info; wmi_p2p_noa_info *p2p_noa_info; A_UINT32 num_p2p_noa_info; A_UINT32 is_allocated_p2p_noa_info; wmi_quiet_offload_info *quiet_offload_info; A_UINT32 num_quiet_offload_info; A_UINT32 is_allocated_quiet_offload_info; } WMI_HOST_SWBA_EVENTID_param_tlvs;;
# 4700 "include/wmi_tlv_defs.h"
typedef enum { WMI_HOST_SWBA_V2_EVENTID_tlv_order_fixed_param, WMI_HOST_SWBA_V2_EVENTID_tlv_order_tim_info, WMI_HOST_SWBA_V2_EVENTID_tlv_order_data, WMI_HOST_SWBA_V2_EVENTID_tlv_order_p2p_noa_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HOST_SWBA_V2_EVENTID } WMI_HOST_SWBA_V2_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_host_swba_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_tim_info_v2 *tim_info; A_UINT32 num_tim_info; A_UINT32 is_allocated_tim_info; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; wmi_p2p_noa_info *p2p_noa_info; A_UINT32 num_p2p_noa_info; A_UINT32 is_allocated_p2p_noa_info; } WMI_HOST_SWBA_V2_EVENTID_param_tlvs;;






typedef enum { WMI_HOST_SWFDA_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HOST_SWFDA_EVENTID } WMI_HOST_SWFDA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_host_swfda_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HOST_SWFDA_EVENTID_param_tlvs;;
# 4717 "include/wmi_tlv_defs.h"
typedef enum { WMI_UPDATE_STATS_EVENTID_tlv_order_fixed_param, WMI_UPDATE_STATS_EVENTID_tlv_order_data, WMI_UPDATE_STATS_EVENTID_tlv_order_chain_stats, WMI_UPDATE_STATS_EVENTID_tlv_order_rssi_stats, WMI_UPDATE_STATS_EVENTID_tlv_order_congestion_stats, WMI_UPDATE_STATS_EVENTID_tlv_order_peer_extd2_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPDATE_STATS_EVENTID } WMI_UPDATE_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; wmi_per_chain_rssi_stats *chain_stats; A_UINT32 num_chain_stats; A_UINT32 is_allocated_chain_stats; wmi_rssi_stats *rssi_stats; A_UINT32 num_rssi_stats; A_UINT32 is_allocated_rssi_stats; wmi_congestion_stats *congestion_stats; A_UINT32 num_congestion_stats; A_UINT32 is_allocated_congestion_stats; wmi_peer_extd2_stats *peer_extd2_stats; A_UINT32 num_peer_extd2_stats; A_UINT32 is_allocated_peer_extd2_stats; } WMI_UPDATE_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_TX_PN_RESPONSE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TX_PN_RESPONSE_EVENTID } WMI_PEER_TX_PN_RESPONSE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tx_pn_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TX_PN_RESPONSE_EVENTID_param_tlvs;;





typedef enum { WMI_UPDATE_VDEV_RATE_STATS_EVENTID_tlv_order_fixed_param, WMI_UPDATE_VDEV_RATE_STATS_EVENTID_tlv_order_ht_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPDATE_VDEV_RATE_STATS_EVENTID } WMI_UPDATE_VDEV_RATE_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_rate_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vdev_rate_ht_info *ht_info; A_UINT32 num_ht_info; A_UINT32 is_allocated_ht_info; } WMI_UPDATE_VDEV_RATE_STATS_EVENTID_param_tlvs;;





typedef enum { WMI_REPORT_RX_AGGR_FAILURE_EVENTID_tlv_order_fixed_param, WMI_REPORT_RX_AGGR_FAILURE_EVENTID_tlv_order_failure_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REPORT_RX_AGGR_FAILURE_EVENTID } WMI_REPORT_RX_AGGR_FAILURE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_rx_aggr_failure_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_rx_aggr_failure_info *failure_info; A_UINT32 num_failure_info; A_UINT32 is_allocated_failure_info; } WMI_REPORT_RX_AGGR_FAILURE_EVENTID_param_tlvs;;





typedef enum { WMI_UPDATE_FW_MEM_DUMP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPDATE_FW_MEM_DUMP_EVENTID } WMI_UPDATE_FW_MEM_DUMP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_update_fw_mem_dump_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_UPDATE_FW_MEM_DUMP_EVENTID_param_tlvs;;






typedef enum { WMI_DIAG_EVENT_LOG_SUPPORTED_EVENTID_tlv_order_fixed_param, WMI_DIAG_EVENT_LOG_SUPPORTED_EVENTID_tlv_order_diag_events_logs_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DIAG_EVENT_LOG_SUPPORTED_EVENTID } WMI_DIAG_EVENT_LOG_SUPPORTED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_diag_event_log_supported_event_fixed_params *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *diag_events_logs_list; A_UINT32 num_diag_events_logs_list; A_UINT32 is_allocated_diag_events_logs_list; } WMI_DIAG_EVENT_LOG_SUPPORTED_EVENTID_param_tlvs;;
# 4756 "include/wmi_tlv_defs.h"
typedef enum { WMI_IFACE_LINK_STATS_EVENTID_tlv_order_fixed_param, WMI_IFACE_LINK_STATS_EVENTID_tlv_order_iface_link_stats, WMI_IFACE_LINK_STATS_EVENTID_tlv_order_ac, WMI_IFACE_LINK_STATS_EVENTID_tlv_order_iface_offload_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_IFACE_LINK_STATS_EVENTID } WMI_IFACE_LINK_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_iface_link_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_iface_link_stats *iface_link_stats; A_UINT32 num_iface_link_stats; A_UINT32 is_allocated_iface_link_stats; wmi_wmm_ac_stats *ac; A_UINT32 num_ac; A_UINT32 is_allocated_ac; wmi_iface_offload_stats *iface_offload_stats; A_UINT32 num_iface_offload_stats; A_UINT32 is_allocated_iface_offload_stats; } WMI_IFACE_LINK_STATS_EVENTID_param_tlvs;;







typedef enum { WMI_PEER_LINK_STATS_EVENTID_tlv_order_fixed_param, WMI_PEER_LINK_STATS_EVENTID_tlv_order_peer_stats, WMI_PEER_LINK_STATS_EVENTID_tlv_order_peer_rate_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_LINK_STATS_EVENTID } WMI_PEER_LINK_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_link_stats *peer_stats; A_UINT32 num_peer_stats; A_UINT32 is_allocated_peer_stats; wmi_rate_stats *peer_rate_stats; A_UINT32 num_peer_rate_stats; A_UINT32 is_allocated_peer_rate_stats; } WMI_PEER_LINK_STATS_EVENTID_param_tlvs;;







typedef enum { WMI_RADIO_LINK_STATS_EVENTID_tlv_order_fixed_param, WMI_RADIO_LINK_STATS_EVENTID_tlv_order_radio_stats, WMI_RADIO_LINK_STATS_EVENTID_tlv_order_channel_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RADIO_LINK_STATS_EVENTID } WMI_RADIO_LINK_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_radio_link_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_radio_link_stats *radio_stats; A_UINT32 num_radio_stats; A_UINT32 is_allocated_radio_stats; wmi_channel_stats *channel_stats; A_UINT32 num_channel_stats; A_UINT32 is_allocated_channel_stats; } WMI_RADIO_LINK_STATS_EVENTID_param_tlvs;;





typedef enum { WMI_WLM_STATS_EVENTID_tlv_order_fixed_param, WMI_WLM_STATS_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLM_STATS_EVENTID } WMI_WLM_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_wlm_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_WLM_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_QVIT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_QVIT_EVENTID } WMI_PDEV_QVIT_EVENTID_TAG_ORDER_enum_type; typedef struct { A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_QVIT_EVENTID_param_tlvs;;





typedef enum { WMI_WLAN_FREQ_AVOID_EVENTID_tlv_order_fixed_param, WMI_WLAN_FREQ_AVOID_EVENTID_tlv_order_avd_freq_range, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_FREQ_AVOID_EVENTID } WMI_WLAN_FREQ_AVOID_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_avoid_freq_ranges_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_avoid_freq_range_desc *avd_freq_range; A_UINT32 num_avd_freq_range; A_UINT32 is_allocated_avd_freq_range; } WMI_WLAN_FREQ_AVOID_EVENTID_param_tlvs;;





typedef enum { WMI_SAR_GET_LIMITS_EVENTID_tlv_order_fixed_param, WMI_SAR_GET_LIMITS_EVENTID_tlv_order_sar_get_limits, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAR_GET_LIMITS_EVENTID } WMI_SAR_GET_LIMITS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sar_get_limits_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_sar_get_limit_event_row *sar_get_limits; A_UINT32 num_sar_get_limits; A_UINT32 is_allocated_sar_get_limits; } WMI_SAR_GET_LIMITS_EVENTID_param_tlvs;;




typedef enum { WMI_GTK_REKEY_FAIL_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GTK_REKEY_FAIL_EVENTID } WMI_GTK_REKEY_FAIL_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_gtk_rekey_fail_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GTK_REKEY_FAIL_EVENTID_param_tlvs;;




    typedef enum { WMI_NLO_MATCH_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NLO_MATCH_EVENTID } WMI_NLO_MATCH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nlo_event *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NLO_MATCH_EVENTID_param_tlvs;;




    typedef enum { WMI_NLO_SCAN_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NLO_SCAN_COMPLETE_EVENTID } WMI_NLO_SCAN_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nlo_event *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NLO_SCAN_COMPLETE_EVENTID_param_tlvs;;





    typedef enum { WMI_APFIND_EVENTID_tlv_order_hdr, WMI_APFIND_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_APFIND_EVENTID } WMI_APFIND_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_apfind_event_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_APFIND_EVENTID_param_tlvs;;





    typedef enum { WMI_PASSPOINT_MATCH_EVENTID_tlv_order_fixed_param, WMI_PASSPOINT_MATCH_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PASSPOINT_MATCH_EVENTID } WMI_PASSPOINT_MATCH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_passpoint_event_hdr *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_PASSPOINT_MATCH_EVENTID_param_tlvs;;




    typedef enum { WMI_CHATTER_PC_QUERY_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CHATTER_PC_QUERY_EVENTID } WMI_CHATTER_PC_QUERY_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_chatter_query_reply_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CHATTER_PC_QUERY_EVENTID_param_tlvs;;





    typedef enum { WMI_UPLOADH_EVENTID_tlv_order_hdr, WMI_UPLOADH_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPLOADH_EVENTID } WMI_UPLOADH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_upload_h_hdr *hdr; A_UINT32 num_hdr; A_UINT32 is_allocated_hdr; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_UPLOADH_EVENTID_param_tlvs;;




    typedef enum { WMI_CAPTUREH_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_CAPTUREH_EVENTID } WMI_CAPTUREH_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_capture_h_event_hdr *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_CAPTUREH_EVENTID_param_tlvs;;




    typedef enum { WMI_TDLS_PEER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TDLS_PEER_EVENTID } WMI_TDLS_PEER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tdls_peer_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TDLS_PEER_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_MCC_BCN_INTERVAL_CHANGE_REQ_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_MCC_BCN_INTERVAL_CHANGE_REQ_EVENTID } WMI_VDEV_MCC_BCN_INTERVAL_CHANGE_REQ_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_mcc_bcn_intvl_change_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_MCC_BCN_INTERVAL_CHANGE_REQ_EVENTID_param_tlvs;;



typedef enum { WMI_BATCH_SCAN_ENABLED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BATCH_SCAN_ENABLED_EVENTID } WMI_BATCH_SCAN_ENABLED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_batch_scan_enabled_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BATCH_SCAN_ENABLED_EVENTID_param_tlvs;;





typedef enum { WMI_BATCH_SCAN_RESULT_EVENTID_tlv_order_fixed_param, WMI_BATCH_SCAN_RESULT_EVENTID_tlv_order_scan_list, WMI_BATCH_SCAN_RESULT_EVENTID_tlv_order_network_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BATCH_SCAN_RESULT_EVENTID } WMI_BATCH_SCAN_RESULT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_batch_scan_result_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_batch_scan_result_scan_list *scan_list; A_UINT32 num_scan_list; A_UINT32 is_allocated_scan_list; wmi_batch_scan_result_network_info *network_list; A_UINT32 num_network_list; A_UINT32 is_allocated_network_list; } WMI_BATCH_SCAN_RESULT_EVENTID_param_tlvs;;



typedef enum { WMI_OFFLOAD_BCN_TX_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OFFLOAD_BCN_TX_STATUS_EVENTID } WMI_OFFLOAD_BCN_TX_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_offload_bcn_tx_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OFFLOAD_BCN_TX_STATUS_EVENTID_param_tlvs;;





typedef enum { WMI_P2P_NOA_EVENTID_tlv_order_fixed_param, WMI_P2P_NOA_EVENTID_tlv_order_p2p_noa_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_P2P_NOA_EVENTID } WMI_P2P_NOA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_p2p_noa_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_p2p_noa_info *p2p_noa_info; A_UINT32 num_p2p_noa_info; A_UINT32 is_allocated_p2p_noa_info; } WMI_P2P_NOA_EVENTID_param_tlvs;;





typedef enum { WMI_AP_PS_EGAP_INFO_EVENTID_tlv_order_fixed_param, WMI_AP_PS_EGAP_INFO_EVENTID_tlv_order_chainmask_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_AP_PS_EGAP_INFO_EVENTID } WMI_AP_PS_EGAP_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ap_ps_egap_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ap_ps_egap_info_chainmask_list *chainmask_list; A_UINT32 num_chainmask_list; A_UINT32 is_allocated_chainmask_list; } WMI_AP_PS_EGAP_INFO_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_INFO_EVENTID_tlv_order_fixed_param, WMI_PEER_INFO_EVENTID_tlv_order_peer_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_INFO_EVENTID } WMI_PEER_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_info *peer_info; A_UINT32 num_peer_info; A_UINT32 is_allocated_peer_info; } WMI_PEER_INFO_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_ANTDIV_INFO_EVENTID_tlv_order_fixed_param, WMI_PEER_ANTDIV_INFO_EVENTID_tlv_order_peer_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ANTDIV_INFO_EVENTID } WMI_PEER_ANTDIV_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_antdiv_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_antdiv_info *peer_info; A_UINT32 num_peer_info; A_UINT32 is_allocated_peer_info; } WMI_PEER_ANTDIV_INFO_EVENTID_param_tlvs;;



typedef enum { WMI_PEER_TX_FAIL_CNT_THR_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_TX_FAIL_CNT_THR_EVENTID } WMI_PEER_TX_FAIL_CNT_THR_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_tx_fail_cnt_thr_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_TX_FAIL_CNT_THR_EVENTID_param_tlvs;;



typedef enum { WMI_PEER_OPER_MODE_CHANGE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_OPER_MODE_CHANGE_EVENTID } WMI_PEER_OPER_MODE_CHANGE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_oper_mode_change_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_OPER_MODE_CHANGE_EVENTID_param_tlvs;;




typedef enum { WMI_DFS_RADAR_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DFS_RADAR_EVENTID } WMI_DFS_RADAR_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dfs_radar_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DFS_RADAR_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_DFS_RADAR_DETECTION_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DFS_RADAR_DETECTION_EVENTID } WMI_PDEV_DFS_RADAR_DETECTION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_dfs_radar_detection_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DFS_RADAR_DETECTION_EVENTID_param_tlvs;;



typedef enum { WMI_VDEV_ADFS_OCAC_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ADFS_OCAC_COMPLETE_EVENTID } WMI_VDEV_ADFS_OCAC_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_adfs_ocac_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ADFS_OCAC_COMPLETE_EVENTID_param_tlvs;;



typedef enum { WMI_VDEV_DFS_CAC_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DFS_CAC_COMPLETE_EVENTID } WMI_VDEV_DFS_CAC_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_dfs_cac_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DFS_CAC_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_THERMAL_MGMT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_THERMAL_MGMT_EVENTID } WMI_THERMAL_MGMT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_thermal_mgmt_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_THERMAL_MGMT_EVENTID_param_tlvs;;



typedef enum { WMI_OEM_DMA_RING_CFG_RSP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OEM_DMA_RING_CFG_RSP_EVENTID } WMI_OEM_DMA_RING_CFG_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_oem_dma_ring_cfg_rsp_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OEM_DMA_RING_CFG_RSP_EVENTID_param_tlvs;






typedef enum { WMI_NAN_EVENTID_tlv_order_fixed_param, WMI_NAN_EVENTID_tlv_order_data, WMI_NAN_EVENTID_tlv_order_event_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_EVENTID } WMI_NAN_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nan_event_hdr *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; wmi_nan_event_info *event_info; A_UINT32 num_event_info; A_UINT32 is_allocated_event_info; } WMI_NAN_EVENTID_param_tlvs;;




typedef enum { WMI_NAN_DISC_IFACE_CREATED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_DISC_IFACE_CREATED_EVENTID } WMI_NAN_DISC_IFACE_CREATED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nan_disc_iface_created_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NAN_DISC_IFACE_CREATED_EVENTID_param_tlvs;;




typedef enum { WMI_NAN_DISC_IFACE_DELETED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_DISC_IFACE_DELETED_EVENTID } WMI_NAN_DISC_IFACE_DELETED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nan_disc_iface_deleted_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NAN_DISC_IFACE_DELETED_EVENTID_param_tlvs;;




typedef enum { WMI_NAN_STARTED_CLUSTER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_STARTED_CLUSTER_EVENTID } WMI_NAN_STARTED_CLUSTER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nan_started_cluster_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NAN_STARTED_CLUSTER_EVENTID_param_tlvs;;




typedef enum { WMI_NAN_JOINED_CLUSTER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NAN_JOINED_CLUSTER_EVENTID } WMI_NAN_JOINED_CLUSTER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_nan_joined_cluster_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NAN_JOINED_CLUSTER_EVENTID_param_tlvs;;




typedef enum { WMI_COEX_REPORT_ANTENNA_ISOLATION_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_COEX_REPORT_ANTENNA_ISOLATION_EVENTID } WMI_COEX_REPORT_ANTENNA_ISOLATION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_coex_report_isolation_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_COEX_REPORT_ANTENNA_ISOLATION_EVENTID_param_tlvs;;




typedef enum { WMI_NDI_CAP_RSP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDI_CAP_RSP_EVENTID } WMI_NDI_CAP_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndi_cap_rsp_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDI_CAP_RSP_EVENTID_param_tlvs;;




typedef enum { WMI_NDP_INITIATOR_RSP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_INITIATOR_RSP_EVENTID } WMI_NDP_INITIATOR_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_initiator_rsp_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDP_INITIATOR_RSP_EVENTID_param_tlvs;;




typedef enum { WMI_NDP_RESPONDER_RSP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_RESPONDER_RSP_EVENTID } WMI_NDP_RESPONDER_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_responder_rsp_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDP_RESPONDER_RSP_EVENTID_param_tlvs;;
# 4978 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_END_RSP_EVENTID_tlv_order_fixed_param, WMI_NDP_END_RSP_EVENTID_tlv_order_ndp_end_rsp_per_ndi_list, WMI_NDP_END_RSP_EVENTID_tlv_order_active_ndp_instances_id, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_END_RSP_EVENTID } WMI_NDP_END_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_end_rsp_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_ndp_end_rsp_per_ndi_PROTOTYPE *ndp_end_rsp_per_ndi_list; A_UINT32 num_ndp_end_rsp_per_ndi_list; A_UINT32 is_allocated_ndp_end_rsp_per_ndi_list; wmi_active_ndp_instance_id_PROTOTYPE *active_ndp_instances_id; A_UINT32 num_active_ndp_instances_id; A_UINT32 is_allocated_active_ndp_instances_id; } WMI_NDP_END_RSP_EVENTID_param_tlvs;;
# 4994 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_INDICATION_EVENTID_tlv_order_fixed_param, WMI_NDP_INDICATION_EVENTID_tlv_order_ndp_cfg, WMI_NDP_INDICATION_EVENTID_tlv_order_ndp_app_info, WMI_NDP_INDICATION_EVENTID_tlv_order_ndp_scid, WMI_NDP_INDICATION_EVENTID_tlv_order_ndp_transport_ip_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_INDICATION_EVENTID } WMI_NDP_INDICATION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_indication_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ndp_cfg; A_UINT32 num_ndp_cfg; A_UINT32 is_allocated_ndp_cfg; A_UINT8 *ndp_app_info; A_UINT32 num_ndp_app_info; A_UINT32 is_allocated_ndp_app_info; A_UINT8 *ndp_scid; A_UINT32 num_ndp_scid; A_UINT32 is_allocated_ndp_scid; wmi_ndp_transport_ip_param *ndp_transport_ip_param; A_UINT32 num_ndp_transport_ip_param; A_UINT32 is_allocated_ndp_transport_ip_param; } WMI_NDP_INDICATION_EVENTID_param_tlvs;;
# 5012 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_CONFIRM_EVENTID_tlv_order_fixed_param, WMI_NDP_CONFIRM_EVENTID_tlv_order_ndp_cfg, WMI_NDP_CONFIRM_EVENTID_tlv_order_ndp_app_info, WMI_NDP_CONFIRM_EVENTID_tlv_order_ndp_channel_list, WMI_NDP_CONFIRM_EVENTID_tlv_order_nss_list, WMI_NDP_CONFIRM_EVENTID_tlv_order_ndp_transport_ip_param, WMI_NDP_CONFIRM_EVENTID_tlv_order_ndp_channel_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_CONFIRM_EVENTID } WMI_NDP_CONFIRM_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_confirm_event_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *ndp_cfg; A_UINT32 num_ndp_cfg; A_UINT32 is_allocated_ndp_cfg; A_UINT8 *ndp_app_info; A_UINT32 num_ndp_app_info; A_UINT32 is_allocated_ndp_app_info; wmi_channel *ndp_channel_list; A_UINT32 num_ndp_channel_list; A_UINT32 is_allocated_ndp_channel_list; A_UINT32 *nss_list; A_UINT32 num_nss_list; A_UINT32 is_allocated_nss_list; wmi_ndp_transport_ip_param *ndp_transport_ip_param; A_UINT32 num_ndp_transport_ip_param; A_UINT32 is_allocated_ndp_transport_ip_param; wmi_ndp_channel_info *ndp_channel_info; A_UINT32 num_ndp_channel_info; A_UINT32 is_allocated_ndp_channel_info; } WMI_NDP_CONFIRM_EVENTID_param_tlvs;;
# 5022 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDP_END_INDICATION_EVENTID_tlv_order_ndp_end_indication_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_END_INDICATION_EVENTID } WMI_NDP_END_INDICATION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_end_indication_PROTOTYPE *ndp_end_indication_list; A_UINT32 num_ndp_end_indication_list; A_UINT32 is_allocated_ndp_end_indication_list; } WMI_NDP_END_INDICATION_EVENTID_param_tlvs;;
# 5036 "include/wmi_tlv_defs.h"
typedef enum { WMI_NDL_SCHEDULE_UPDATE_EVENTID_tlv_order_fixed_param, WMI_NDL_SCHEDULE_UPDATE_EVENTID_tlv_order_ndp_instance_list, WMI_NDL_SCHEDULE_UPDATE_EVENTID_tlv_order_ndl_channel_list, WMI_NDL_SCHEDULE_UPDATE_EVENTID_tlv_order_nss_list, WMI_NDL_SCHEDULE_UPDATE_EVENTID_tlv_order_ndp_channel_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDL_SCHEDULE_UPDATE_EVENTID } WMI_NDL_SCHEDULE_UPDATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndl_schedule_update_fixed_param_PROTOTYPE *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *ndp_instance_list; A_UINT32 num_ndp_instance_list; A_UINT32 is_allocated_ndp_instance_list; wmi_channel *ndl_channel_list; A_UINT32 num_ndl_channel_list; A_UINT32 is_allocated_ndl_channel_list; A_UINT32 *nss_list; A_UINT32 num_nss_list; A_UINT32 is_allocated_nss_list; wmi_ndp_channel_info *ndp_channel_info; A_UINT32 num_ndp_channel_info; A_UINT32 is_allocated_ndp_channel_info; } WMI_NDL_SCHEDULE_UPDATE_EVENTID_param_tlvs;;




typedef enum { WMI_NDP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_NDP_EVENTID } WMI_NDP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ndp_event_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_NDP_EVENTID_param_tlvs;;




typedef enum { WMI_UPDATE_RCPI_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UPDATE_RCPI_EVENTID } WMI_UPDATE_RCPI_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_update_rcpi_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_UPDATE_RCPI_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_L1SS_TRACK_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_L1SS_TRACK_EVENTID } WMI_PDEV_L1SS_TRACK_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_l1ss_track_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_L1SS_TRACK_EVENTID_param_tlvs;;




typedef enum { WMI_DIAG_DATA_CONTAINER_EVENTID_tlv_order_fixed_param, WMI_DIAG_DATA_CONTAINER_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DIAG_DATA_CONTAINER_EVENTID } WMI_DIAG_DATA_CONTAINER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_diag_data_container_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_DIAG_DATA_CONTAINER_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_ESTIMATED_LINKSPEED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ESTIMATED_LINKSPEED_EVENTID } WMI_PEER_ESTIMATED_LINKSPEED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_estimated_linkspeed_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_ESTIMATED_LINKSPEED_EVENTID_param_tlvs;;





typedef enum { WMI_STATS_EXT_EVENTID_tlv_order_fixed_param, WMI_STATS_EXT_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STATS_EXT_EVENTID } WMI_STATS_EXT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_stats_ext_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_STATS_EXT_EVENTID_param_tlvs;;



typedef enum { WMI_OFFLOAD_PROB_RESP_TX_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OFFLOAD_PROB_RESP_TX_STATUS_EVENTID } WMI_OFFLOAD_PROB_RESP_TX_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_offload_prb_rsp_tx_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OFFLOAD_PROB_RESP_TX_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_HOST_AUTO_SHUTDOWN_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_HOST_AUTO_SHUTDOWN_EVENTID } WMI_HOST_AUTO_SHUTDOWN_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_host_auto_shutdown_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_HOST_AUTO_SHUTDOWN_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_STATE_EVENTID } WMI_PEER_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_state_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_STATE_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_DELETE_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_DELETE_RESP_EVENTID } WMI_PEER_DELETE_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_delete_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_DELETE_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_DELETE_ALL_PEER_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_DELETE_ALL_PEER_RESP_EVENTID } WMI_VDEV_DELETE_ALL_PEER_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_delete_all_peer_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_DELETE_ALL_PEER_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_ASSOC_CONF_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_ASSOC_CONF_EVENTID } WMI_PEER_ASSOC_CONF_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_assoc_conf_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_ASSOC_CONF_EVENTID_param_tlvs;;




typedef enum { WMI_D0_WOW_DISABLE_ACK_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_D0_WOW_DISABLE_ACK_EVENTID } WMI_D0_WOW_DISABLE_ACK_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_d0_wow_disable_ack_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_D0_WOW_DISABLE_ACK_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_TEMPERATURE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_TEMPERATURE_EVENTID } WMI_PDEV_TEMPERATURE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_temperature_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_TEMPERATURE_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_ANTDIV_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_ANTDIV_STATUS_EVENTID } WMI_PDEV_ANTDIV_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_antdiv_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_ANTDIV_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_MDNS_STATS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MDNS_STATS_EVENTID } WMI_MDNS_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mdns_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MDNS_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_RESUME_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_RESUME_EVENTID } WMI_PDEV_RESUME_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_resume_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_RESUME_EVENTID_param_tlvs;;





typedef enum { WMI_SAP_OFL_ADD_STA_EVENTID_tlv_order_fixed_param, WMI_SAP_OFL_ADD_STA_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_OFL_ADD_STA_EVENTID } WMI_SAP_OFL_ADD_STA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sap_ofl_add_sta_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_SAP_OFL_ADD_STA_EVENTID_param_tlvs;;



typedef enum { WMI_SAP_OFL_DEL_STA_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_OFL_DEL_STA_EVENTID } WMI_SAP_OFL_DEL_STA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sap_ofl_del_sta_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAP_OFL_DEL_STA_EVENTID_param_tlvs;;




typedef enum { WMI_SAP_OBSS_DETECTION_REPORT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SAP_OBSS_DETECTION_REPORT_EVENTID } WMI_SAP_OBSS_DETECTION_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sap_obss_detection_info_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SAP_OBSS_DETECTION_REPORT_EVENTID_param_tlvs;;




typedef enum { WMI_OBSS_COLOR_COLLISION_DETECTION_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OBSS_COLOR_COLLISION_DETECTION_EVENTID } WMI_OBSS_COLOR_COLLISION_DETECTION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_obss_color_collision_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OBSS_COLOR_COLLISION_DETECTION_EVENTID_param_tlvs;;




typedef enum { WMI_OCB_SET_SCHED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_SET_SCHED_EVENTID } WMI_OCB_SET_SCHED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_set_sched_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_SET_SCHED_EVENTID_param_tlvs;;




typedef enum { WMI_OCB_SET_CONFIG_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_SET_CONFIG_RESP_EVENTID } WMI_OCB_SET_CONFIG_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_set_config_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_SET_CONFIG_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_OCB_GET_TSF_TIMER_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_OCB_GET_TSF_TIMER_RESP_EVENTID } WMI_OCB_GET_TSF_TIMER_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ocb_get_tsf_timer_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_OCB_GET_TSF_TIMER_RESP_EVENTID_param_tlvs;;





typedef enum { WMI_DCC_GET_STATS_RESP_EVENTID_tlv_order_fixed_param, WMI_DCC_GET_STATS_RESP_EVENTID_tlv_order_stats_per_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_GET_STATS_RESP_EVENTID } WMI_DCC_GET_STATS_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_get_stats_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_dcc_ndl_stats_per_channel *stats_per_channel_list; A_UINT32 num_stats_per_channel_list; A_UINT32 is_allocated_stats_per_channel_list; } WMI_DCC_GET_STATS_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_DCC_UPDATE_NDL_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_UPDATE_NDL_RESP_EVENTID } WMI_DCC_UPDATE_NDL_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_update_ndl_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_DCC_UPDATE_NDL_RESP_EVENTID_param_tlvs;;





typedef enum { WMI_DCC_STATS_EVENTID_tlv_order_fixed_param, WMI_DCC_STATS_EVENTID_tlv_order_stats_per_channel_list, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_DCC_STATS_EVENTID } WMI_DCC_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dcc_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_dcc_ndl_stats_per_channel *stats_per_channel_list; A_UINT32 num_stats_per_channel_list; A_UINT32 is_allocated_stats_per_channel_list; } WMI_DCC_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_TSF_REPORT_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_TSF_REPORT_EVENTID } WMI_VDEV_TSF_REPORT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_tsf_report_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_TSF_REPORT_EVENTID_param_tlvs;;





typedef enum { WMI_VDEV_SET_IE_CMDID_tlv_order_vdev_ie, WMI_VDEV_SET_IE_CMDID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_SET_IE_CMDID } WMI_VDEV_SET_IE_CMDID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_set_ie_cmd_fixed_param *vdev_ie; A_UINT32 num_vdev_ie; A_UINT32 is_allocated_vdev_ie; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_VDEV_SET_IE_CMDID_param_tlvs;;





typedef enum { WMI_SOC_SET_HW_MODE_RESP_EVENTID_tlv_order_fixed_param, WMI_SOC_SET_HW_MODE_RESP_EVENTID_tlv_order_wmi_soc_set_hw_mode_response_vdev_mac_mapping, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_HW_MODE_RESP_EVENTID } WMI_SOC_SET_HW_MODE_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_hw_mode_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_soc_set_hw_mode_response_vdev_mac_entry *wmi_soc_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_soc_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_soc_set_hw_mode_response_vdev_mac_mapping; } WMI_SOC_SET_HW_MODE_RESP_EVENTID_param_tlvs;;





typedef enum { WMI_PDEV_SET_HW_MODE_RESP_EVENTID_tlv_order_fixed_param, WMI_PDEV_SET_HW_MODE_RESP_EVENTID_tlv_order_wmi_pdev_set_hw_mode_response_vdev_mac_mapping, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_HW_MODE_RESP_EVENTID } WMI_PDEV_SET_HW_MODE_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_hw_mode_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_set_hw_mode_response_vdev_mac_entry *wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; } WMI_PDEV_SET_HW_MODE_RESP_EVENTID_param_tlvs;;





typedef enum { WMI_SOC_HW_MODE_TRANSITION_EVENTID_tlv_order_fixed_param, WMI_SOC_HW_MODE_TRANSITION_EVENTID_tlv_order_wmi_soc_set_hw_mode_response_vdev_mac_mapping, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_HW_MODE_TRANSITION_EVENTID } WMI_SOC_HW_MODE_TRANSITION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_soc_hw_mode_transition_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_soc_set_hw_mode_response_vdev_mac_entry *wmi_soc_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_soc_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_soc_set_hw_mode_response_vdev_mac_mapping; } WMI_SOC_HW_MODE_TRANSITION_EVENTID_param_tlvs;;





typedef enum { WMI_PDEV_HW_MODE_TRANSITION_EVENTID_tlv_order_fixed_param, WMI_PDEV_HW_MODE_TRANSITION_EVENTID_tlv_order_wmi_pdev_set_hw_mode_response_vdev_mac_mapping, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_HW_MODE_TRANSITION_EVENTID } WMI_PDEV_HW_MODE_TRANSITION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_hw_mode_transition_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_set_hw_mode_response_vdev_mac_entry *wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 num_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; A_UINT32 is_allocated_wmi_pdev_set_hw_mode_response_vdev_mac_mapping; } WMI_PDEV_HW_MODE_TRANSITION_EVENTID_param_tlvs;;




typedef enum { WMI_SOC_SET_DUAL_MAC_CONFIG_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_SOC_SET_DUAL_MAC_CONFIG_RESP_EVENTID } WMI_SOC_SET_DUAL_MAC_CONFIG_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_soc_set_dual_mac_config_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_SOC_SET_DUAL_MAC_CONFIG_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_SET_MAC_CONFIG_RESP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_SET_MAC_CONFIG_RESP_EVENTID } WMI_PDEV_SET_MAC_CONFIG_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_set_mac_config_response_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_SET_MAC_CONFIG_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_PACKET_FILTER_CONFIG_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PACKET_FILTER_CONFIG_CMDID } WMI_PACKET_FILTER_CONFIG_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_PACKET_FILTER_CONFIG_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PACKET_FILTER_CONFIG_CMDID_param_tlvs;;




typedef enum { WMI_PACKET_FILTER_ENABLE_CMDID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PACKET_FILTER_ENABLE_CMDID } WMI_PACKET_FILTER_ENABLE_CMDID_TAG_ORDER_enum_type; typedef struct { WMI_PACKET_FILTER_ENABLE_CMD_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PACKET_FILTER_ENABLE_CMDID_param_tlvs;;




typedef enum { WMI_MAWC_ENABLE_SENSOR_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MAWC_ENABLE_SENSOR_EVENTID } WMI_MAWC_ENABLE_SENSOR_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mawc_enable_sensor_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MAWC_ENABLE_SENSOR_EVENTID_param_tlvs;;




typedef enum { WMI_STA_SMPS_FORCE_MODE_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_STA_SMPS_FORCE_MODE_COMPLETE_EVENTID } WMI_STA_SMPS_FORCE_MODE_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_sta_smps_force_mode_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_STA_SMPS_FORCE_MODE_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_BPF_CAPABILIY_INFO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_CAPABILIY_INFO_EVENTID } WMI_BPF_CAPABILIY_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_capability_info_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_CAPABILIY_INFO_EVENTID_param_tlvs;;




typedef enum { WMI_BPF_VDEV_STATS_INFO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_VDEV_STATS_INFO_EVENTID } WMI_BPF_VDEV_STATS_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_vdev_stats_info_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_BPF_VDEV_STATS_INFO_EVENTID_param_tlvs;;





typedef enum { WMI_BPF_GET_VDEV_WORK_MEMORY_RESP_EVENTID_tlv_order_fixed_param, WMI_BPF_GET_VDEV_WORK_MEMORY_RESP_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_BPF_GET_VDEV_WORK_MEMORY_RESP_EVENTID } WMI_BPF_GET_VDEV_WORK_MEMORY_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_bpf_get_vdev_work_memory_resp_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_BPF_GET_VDEV_WORK_MEMORY_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_11D_NEW_COUNTRY_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_11D_NEW_COUNTRY_EVENTID } WMI_11D_NEW_COUNTRY_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_11d_new_country_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_11D_NEW_COUNTRY_EVENTID_param_tlvs;;





typedef enum { WMI_REG_CHAN_LIST_CC_EVENTID_tlv_order_fixed_param, WMI_REG_CHAN_LIST_CC_EVENTID_tlv_order_reg_rule_array, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REG_CHAN_LIST_CC_EVENTID } WMI_REG_CHAN_LIST_CC_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_reg_chan_list_cc_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_regulatory_rule_struct *reg_rule_array; A_UINT32 num_reg_rule_array; A_UINT32 is_allocated_reg_rule_array; } WMI_REG_CHAN_LIST_CC_EVENTID_param_tlvs;;





typedef enum { WMI_PDEV_FIPS_EVENTID_tlv_order_fixed_param, WMI_PDEV_FIPS_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_FIPS_EVENTID } WMI_PDEV_FIPS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_fips_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_FIPS_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_CHANNEL_HOPPING_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CHANNEL_HOPPING_EVENTID } WMI_PDEV_CHANNEL_HOPPING_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_channel_hopping_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_CHANNEL_HOPPING_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_ANI_CCK_LEVEL_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_ANI_CCK_LEVEL_EVENTID } WMI_PDEV_ANI_CCK_LEVEL_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ani_cck_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_ANI_CCK_LEVEL_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_CHIP_POWER_SAVE_FAILURE_DETECTED_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CHIP_POWER_SAVE_FAILURE_DETECTED_EVENTID } WMI_PDEV_CHIP_POWER_SAVE_FAILURE_DETECTED_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_chip_power_save_failure_detected_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_CHIP_POWER_SAVE_FAILURE_DETECTED_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_CHIP_POWER_STATS_EVENTID_tlv_order_fixed_param, WMI_PDEV_CHIP_POWER_STATS_EVENTID_tlv_order_debug_registers, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CHIP_POWER_STATS_EVENTID } WMI_PDEV_CHIP_POWER_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_chip_power_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *debug_registers; A_UINT32 num_debug_registers; A_UINT32 is_allocated_debug_registers; } WMI_PDEV_CHIP_POWER_STATS_EVENTID_param_tlvs;;



typedef enum { WMI_VDEV_BCN_RECEPTION_STATS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_BCN_RECEPTION_STATS_EVENTID } WMI_VDEV_BCN_RECEPTION_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_bcn_recv_stats_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_BCN_RECEPTION_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_STATE_EVENTID } WMI_VDEV_GET_MWS_COEX_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_state_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_STATE_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_DPWB_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_DPWB_STATE_EVENTID } WMI_VDEV_GET_MWS_COEX_DPWB_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_dpwb_state_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_DPWB_STATE_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_TDM_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_TDM_STATE_EVENTID } WMI_VDEV_GET_MWS_COEX_TDM_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_tdm_state_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_TDM_STATE_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_IDRX_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_IDRX_STATE_EVENTID } WMI_VDEV_GET_MWS_COEX_IDRX_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_idrx_state_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_IDRX_STATE_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_GET_MWS_COEX_ANTENNA_SHARING_STATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_MWS_COEX_ANTENNA_SHARING_STATE_EVENTID } WMI_VDEV_GET_MWS_COEX_ANTENNA_SHARING_STATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_mws_coex_antenna_sharing_state_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_GET_MWS_COEX_ANTENNA_SHARING_STATE_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_ANI_OFDM_LEVEL_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_ANI_OFDM_LEVEL_EVENTID } WMI_PDEV_ANI_OFDM_LEVEL_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_ani_ofdm_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_ANI_OFDM_LEVEL_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_TPC_EVENTID_tlv_order_fixed_param, WMI_PDEV_TPC_EVENTID_tlv_order_tpc, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_TPC_EVENTID } WMI_PDEV_TPC_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_tpc_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *tpc; A_UINT32 num_tpc; A_UINT32 is_allocated_tpc; } WMI_PDEV_TPC_EVENTID_param_tlvs;;






typedef enum { WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_tlv_order_fixed_param, WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_tlv_order_nfdbr, WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_tlv_order_nfdbm, WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_tlv_order_freqnum, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID } WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_nfcal_power_all_channels_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_pdev_nfcal_power_all_channels_nfdBr *nfdbr; A_UINT32 num_nfdbr; A_UINT32 is_allocated_nfdbr; wmi_pdev_nfcal_power_all_channels_nfdBm *nfdbm; A_UINT32 num_nfdbm; A_UINT32 is_allocated_nfdbm; wmi_pdev_nfcal_power_all_channels_freqNum *freqnum; A_UINT32 num_freqnum; A_UINT32 is_allocated_freqnum; } WMI_PDEV_NFCAL_POWER_ALL_CHANNELS_EVENTID_param_tlvs;;





typedef enum { WMI_PEER_RATECODE_LIST_EVENTID_tlv_order_fixed_param, WMI_PEER_RATECODE_LIST_EVENTID_tlv_order_ratecode_legacy, WMI_PEER_RATECODE_LIST_EVENTID_tlv_order_ratecode_mcs, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_RATECODE_LIST_EVENTID } WMI_PEER_RATECODE_LIST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_ratecode_list_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_cck_ofdm_rate_info *ratecode_legacy; A_UINT32 num_ratecode_legacy; A_UINT32 is_allocated_ratecode_legacy; wmi_peer_mcs_rate_info *ratecode_mcs; A_UINT32 num_ratecode_mcs; A_UINT32 is_allocated_ratecode_mcs; } WMI_PEER_RATECODE_LIST_EVENTID_param_tlvs;;



typedef enum { WMI_WDS_PEER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WDS_PEER_EVENTID } WMI_WDS_PEER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_wds_addr_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WDS_PEER_EVENTID_param_tlvs;;



typedef enum { WMI_PEER_STA_PS_STATECHG_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_STA_PS_STATECHG_EVENTID } WMI_PEER_STA_PS_STATECHG_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_sta_ps_statechange_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PEER_STA_PS_STATECHG_EVENTID_param_tlvs;;



typedef enum { WMI_INST_RSSI_STATS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_INST_RSSI_STATS_EVENTID } WMI_INST_RSSI_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_inst_rssi_stats_resp_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_INST_RSSI_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_RADIO_TX_POWER_LEVEL_STATS_EVENTID_tlv_order_fixed_param, WMI_RADIO_TX_POWER_LEVEL_STATS_EVENTID_tlv_order_tx_time_per_power_level, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RADIO_TX_POWER_LEVEL_STATS_EVENTID } WMI_RADIO_TX_POWER_LEVEL_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_tx_power_level_stats_evt_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *tx_time_per_power_level; A_UINT32 num_tx_time_per_power_level; A_UINT32 is_allocated_tx_time_per_power_level; } WMI_RADIO_TX_POWER_LEVEL_STATS_EVENTID_param_tlvs;;



typedef enum { WMI_RMC_NEW_LEADER_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RMC_NEW_LEADER_EVENTID } WMI_RMC_NEW_LEADER_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_rmc_manual_leader_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_RMC_NEW_LEADER_EVENTID_param_tlvs;;
# 5495 "include/wmi_tlv_defs.h"
typedef enum { WMI_REPORT_STATS_EVENTID_tlv_order_fixed_param, WMI_REPORT_STATS_EVENTID_tlv_order_chan_cca_stats, WMI_REPORT_STATS_EVENTID_tlv_order_peer_signal_stats, WMI_REPORT_STATS_EVENTID_tlv_order_peer_ac_tx_stats, WMI_REPORT_STATS_EVENTID_tlv_order_tx_stats, WMI_REPORT_STATS_EVENTID_tlv_order_tx_mpdu_aggr, WMI_REPORT_STATS_EVENTID_tlv_order_tx_succ_mcs, WMI_REPORT_STATS_EVENTID_tlv_order_tx_fail_mcs, WMI_REPORT_STATS_EVENTID_tlv_order_tx_ppdu_delay, WMI_REPORT_STATS_EVENTID_tlv_order_peer_ac_rx_stats, WMI_REPORT_STATS_EVENTID_tlv_order_rx_stats, WMI_REPORT_STATS_EVENTID_tlv_order_rx_mpdu_aggr, WMI_REPORT_STATS_EVENTID_tlv_order_rx_mcs, WMI_REPORT_STATS_EVENTID_tlv_order_stats_period, WMI_REPORT_STATS_EVENTID_tlv_order_stats_interference, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_REPORT_STATS_EVENTID } WMI_REPORT_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_report_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_chan_cca_stats *chan_cca_stats; A_UINT32 num_chan_cca_stats; A_UINT32 is_allocated_chan_cca_stats; wmi_peer_signal_stats *peer_signal_stats; A_UINT32 num_peer_signal_stats; A_UINT32 is_allocated_peer_signal_stats; wmi_peer_ac_tx_stats *peer_ac_tx_stats; A_UINT32 num_peer_ac_tx_stats; A_UINT32 is_allocated_peer_ac_tx_stats; wmi_tx_stats *tx_stats; A_UINT32 num_tx_stats; A_UINT32 is_allocated_tx_stats; A_UINT32 *tx_mpdu_aggr; A_UINT32 num_tx_mpdu_aggr; A_UINT32 is_allocated_tx_mpdu_aggr; A_UINT32 *tx_succ_mcs; A_UINT32 num_tx_succ_mcs; A_UINT32 is_allocated_tx_succ_mcs; A_UINT32 *tx_fail_mcs; A_UINT32 num_tx_fail_mcs; A_UINT32 is_allocated_tx_fail_mcs; A_UINT32 *tx_ppdu_delay; A_UINT32 num_tx_ppdu_delay; A_UINT32 is_allocated_tx_ppdu_delay; wmi_peer_ac_rx_stats *peer_ac_rx_stats; A_UINT32 num_peer_ac_rx_stats; A_UINT32 is_allocated_peer_ac_rx_stats; wmi_rx_stats *rx_stats; A_UINT32 num_rx_stats; A_UINT32 is_allocated_rx_stats; A_UINT32 *rx_mpdu_aggr; A_UINT32 num_rx_mpdu_aggr; A_UINT32 is_allocated_rx_mpdu_aggr; A_UINT32 *rx_mcs; A_UINT32 num_rx_mcs; A_UINT32 is_allocated_rx_mcs; wmi_stats_period *stats_period; A_UINT32 num_stats_period; A_UINT32 is_allocated_stats_period; wmi_stats_interference *stats_interference; A_UINT32 num_stats_interference; A_UINT32 is_allocated_stats_interference; } WMI_REPORT_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_ENCRYPT_DECRYPT_DATA_RESP_EVENTID_tlv_order_fixed_param, WMI_VDEV_ENCRYPT_DECRYPT_DATA_RESP_EVENTID_tlv_order_enc80211_frame, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ENCRYPT_DECRYPT_DATA_RESP_EVENTID } WMI_VDEV_ENCRYPT_DECRYPT_DATA_RESP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_encrypt_decrypt_data_resp_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *enc80211_frame; A_UINT32 num_enc80211_frame; A_UINT32 is_allocated_enc80211_frame; } WMI_VDEV_ENCRYPT_DECRYPT_DATA_RESP_EVENTID_param_tlvs;;




typedef enum { WMI_PEER_STATS_INFO_EVENTID_tlv_order_fixed_param, WMI_PEER_STATS_INFO_EVENTID_tlv_order_peer_stats_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_STATS_INFO_EVENTID } WMI_PEER_STATS_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_stats_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_stats_info *peer_stats_info; A_UINT32 num_peer_stats_info; A_UINT32 is_allocated_peer_stats_info; } WMI_PEER_STATS_INFO_EVENTID_param_tlvs;;




typedef enum { WMI_RADIO_CHAN_STATS_EVENTID_tlv_order_fixed_param, WMI_RADIO_CHAN_STATS_EVENTID_tlv_order_radio_chan_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_RADIO_CHAN_STATS_EVENTID } WMI_RADIO_CHAN_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_radio_chan_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_radio_chan_stats *radio_chan_stats; A_UINT32 num_radio_chan_stats; A_UINT32 is_allocated_radio_chan_stats; } WMI_RADIO_CHAN_STATS_EVENTID_param_tlvs;;



typedef enum { WMI_PKGID_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PKGID_EVENTID } WMI_PKGID_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pkgid_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PKGID_EVENTID_param_tlvs;;




typedef enum { WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_STATUS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_STATUS_EVENTID } WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_add_mac_addr_to_rx_filter_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_VDEV_ADD_MAC_ADDR_TO_RX_FILTER_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_CSA_SWITCH_COUNT_STATUS_EVENTID_tlv_order_fixed_param, WMI_PDEV_CSA_SWITCH_COUNT_STATUS_EVENTID_tlv_order_vdev_ids, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CSA_SWITCH_COUNT_STATUS_EVENTID } WMI_PDEV_CSA_SWITCH_COUNT_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_csa_switch_count_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *vdev_ids; A_UINT32 num_vdev_ids; A_UINT32 is_allocated_vdev_ids; } WMI_PDEV_CSA_SWITCH_COUNT_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_CSC_SWITCH_COUNT_STATUS_EVENTID_tlv_order_fixed_param, WMI_PDEV_CSC_SWITCH_COUNT_STATUS_EVENTID_tlv_order_vdev_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CSC_SWITCH_COUNT_STATUS_EVENTID } WMI_PDEV_CSC_SWITCH_COUNT_STATUS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_csc_switch_count_status_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_csc_vdev_list *vdev_info; A_UINT32 num_vdev_info; A_UINT32 is_allocated_vdev_info; } WMI_PDEV_CSC_SWITCH_COUNT_STATUS_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_CHECK_CAL_VERSION_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CHECK_CAL_VERSION_EVENTID } WMI_PDEV_CHECK_CAL_VERSION_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_check_cal_version_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_CHECK_CAL_VERSION_EVENTID_param_tlvs;;





typedef enum { WMI_VDEV_GET_ARP_STAT_EVENTID_tlv_order_fixed_param, WMI_VDEV_GET_ARP_STAT_EVENTID_tlv_order_connectivity_check_stats, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_GET_ARP_STAT_EVENTID } WMI_VDEV_GET_ARP_STAT_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_vdev_get_arp_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_vdev_get_connectivity_check_stats *connectivity_check_stats; A_UINT32 num_connectivity_check_stats; A_UINT32 is_allocated_connectivity_check_stats; } WMI_VDEV_GET_ARP_STAT_EVENTID_param_tlvs;;




typedef enum { WMI_WLAN_COEX_BT_ACTIVITY_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_WLAN_COEX_BT_ACTIVITY_EVENTID } WMI_WLAN_COEX_BT_ACTIVITY_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_coex_bt_activity_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_WLAN_COEX_BT_ACTIVITY_EVENTID_param_tlvs;;





typedef enum { WMI_THERM_THROT_STATS_EVENTID_tlv_order_fixed_param, WMI_THERM_THROT_STATS_EVENTID_tlv_order_therm_throt_level_stats_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_THERM_THROT_STATS_EVENTID } WMI_THERM_THROT_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_therm_throt_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_therm_throt_level_stats_info *therm_throt_level_stats_info; A_UINT32 num_therm_throt_level_stats_info; A_UINT32 is_allocated_therm_throt_level_stats_info; } WMI_THERM_THROT_STATS_EVENTID_param_tlvs;;



typedef enum { WMI_PDEV_DMA_RING_CFG_RSP_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DMA_RING_CFG_RSP_EVENTID } WMI_PDEV_DMA_RING_CFG_RSP_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dma_ring_cfg_rsp_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_DMA_RING_CFG_RSP_EVENTID_param_tlvs;;






typedef enum { WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID_tlv_order_fixed_param, WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID_tlv_order_entries, WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID_tlv_order_meta_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID } WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_dma_buf_release_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_dma_buf_release_entry *entries; A_UINT32 num_entries; A_UINT32 is_allocated_entries; wmi_dma_buf_release_spectral_meta_data *meta_data; A_UINT32 num_meta_data; A_UINT32 is_allocated_meta_data; } WMI_PDEV_DMA_RING_BUF_RELEASE_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_CTL_FAILSAFE_CHECK_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_CTL_FAILSAFE_CHECK_EVENTID } WMI_PDEV_CTL_FAILSAFE_CHECK_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_ctl_failsafe_check_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_CTL_FAILSAFE_CHECK_EVENTID_param_tlvs;;





typedef enum { WMI_UNIT_TEST_EVENTID_tlv_order_fixed_param, WMI_UNIT_TEST_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_UNIT_TEST_EVENTID } WMI_UNIT_TEST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_unit_test_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_UNIT_TEST_EVENTID_param_tlvs;;





typedef enum { WMI_TWT_ENABLE_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_ENABLE_COMPLETE_EVENTID } WMI_TWT_ENABLE_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_enable_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_ENABLE_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_DISABLE_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_DISABLE_COMPLETE_EVENTID } WMI_TWT_DISABLE_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_disable_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_DISABLE_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_ADD_DIALOG_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_ADD_DIALOG_COMPLETE_EVENTID } WMI_TWT_ADD_DIALOG_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_add_dialog_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_ADD_DIALOG_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_DEL_DIALOG_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_DEL_DIALOG_COMPLETE_EVENTID } WMI_TWT_DEL_DIALOG_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_del_dialog_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_DEL_DIALOG_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_PAUSE_DIALOG_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_PAUSE_DIALOG_COMPLETE_EVENTID } WMI_TWT_PAUSE_DIALOG_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_pause_dialog_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_PAUSE_DIALOG_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_RESUME_DIALOG_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_RESUME_DIALOG_COMPLETE_EVENTID } WMI_TWT_RESUME_DIALOG_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_resume_dialog_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_RESUME_DIALOG_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_BTWT_INVITE_STA_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_BTWT_INVITE_STA_COMPLETE_EVENTID } WMI_TWT_BTWT_INVITE_STA_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_btwt_invite_sta_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_BTWT_INVITE_STA_COMPLETE_EVENTID_param_tlvs;;




typedef enum { WMI_TWT_BTWT_REMOVE_STA_COMPLETE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_TWT_BTWT_REMOVE_STA_COMPLETE_EVENTID } WMI_TWT_BTWT_REMOVE_STA_COMPLETE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_twt_btwt_remove_sta_complete_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_TWT_BTWT_REMOVE_STA_COMPLETE_EVENTID_param_tlvs;;
# 5632 "include/wmi_tlv_defs.h"
typedef enum { WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_fixed_param, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_client_id, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_timestamp, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_num_channels, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_chan_info, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_old_bssid, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_is_roaming_success, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_new_bssid, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_num_roam_candidates, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_roam_reason, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_bssid, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_score, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_channel, WMI_ROAM_SCAN_STATS_EVENTID_tlv_order_rssi, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ROAM_SCAN_STATS_EVENTID } WMI_ROAM_SCAN_STATS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_roam_scan_stats_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT32 *client_id; A_UINT32 num_client_id; A_UINT32 is_allocated_client_id; wmi_roaming_timestamp *timestamp; A_UINT32 num_timestamp; A_UINT32 is_allocated_timestamp; A_UINT32 *num_channels; A_UINT32 num_num_channels; A_UINT32 is_allocated_num_channels; A_UINT32 *chan_info; A_UINT32 num_chan_info; A_UINT32 is_allocated_chan_info; wmi_mac_addr *old_bssid; A_UINT32 num_old_bssid; A_UINT32 is_allocated_old_bssid; A_UINT32 *is_roaming_success; A_UINT32 num_is_roaming_success; A_UINT32 is_allocated_is_roaming_success; wmi_mac_addr *new_bssid; A_UINT32 num_new_bssid; A_UINT32 is_allocated_new_bssid; A_UINT32 *num_roam_candidates; A_UINT32 num_num_roam_candidates; A_UINT32 is_allocated_num_roam_candidates; wmi_roam_scan_trigger_reason *roam_reason; A_UINT32 num_roam_reason; A_UINT32 is_allocated_roam_reason; wmi_mac_addr *bssid; A_UINT32 num_bssid; A_UINT32 is_allocated_bssid; A_UINT32 *score; A_UINT32 num_score; A_UINT32 is_allocated_score; A_UINT32 *channel; A_UINT32 num_channel; A_UINT32 is_allocated_channel; A_UINT32 *rssi; A_UINT32 num_rssi; A_UINT32 is_allocated_rssi; } WMI_ROAM_SCAN_STATS_EVENTID_param_tlvs;;




typedef enum { WMI_MOTION_DET_HOST_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_HOST_EVENTID } WMI_MOTION_DET_HOST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_event *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_HOST_EVENTID_param_tlvs;;



typedef enum { WMI_MOTION_DET_BASE_LINE_HOST_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_MOTION_DET_BASE_LINE_HOST_EVENTID } WMI_MOTION_DET_BASE_LINE_HOST_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_motion_det_base_line_event *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_MOTION_DET_BASE_LINE_HOST_EVENTID_param_tlvs;;



typedef enum { WMI_ESP_ESTIMATE_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_ESP_ESTIMATE_EVENTID } WMI_ESP_ESTIMATE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_esp_estimate_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_ESP_ESTIMATE_EVENTID_param_tlvs;;





typedef enum { WMI_PEER_CFR_CAPTURE_EVENTID_tlv_order_fixed_param, WMI_PEER_CFR_CAPTURE_EVENTID_tlv_order_phase_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PEER_CFR_CAPTURE_EVENTID } WMI_PEER_CFR_CAPTURE_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_peer_cfr_capture_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_peer_cfr_capture_event_phase_fixed_param *phase_param; A_UINT32 num_phase_param; A_UINT32 is_allocated_phase_param; } WMI_PEER_CFR_CAPTURE_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_COLD_BOOT_CAL_DATA_EVENTID_tlv_order_fixed_param, WMI_PDEV_COLD_BOOT_CAL_DATA_EVENTID_tlv_order_data, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_COLD_BOOT_CAL_DATA_EVENTID } WMI_PDEV_COLD_BOOT_CAL_DATA_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_cold_boot_cal_data_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *data; A_UINT32 num_data; A_UINT32 is_allocated_data; } WMI_PDEV_COLD_BOOT_CAL_DATA_EVENTID_param_tlvs;;




typedef enum { WMI_PDEV_RAP_INFO_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_PDEV_RAP_INFO_EVENTID } WMI_PDEV_RAP_INFO_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_pdev_rap_info_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_PDEV_RAP_INFO_EVENTID_param_tlvs;;





typedef enum { WMI_VDEV_MGMT_OFFLOAD_EVENTID_tlv_order_fixed_param, WMI_VDEV_MGMT_OFFLOAD_EVENTID_tlv_order_bufp, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_VDEV_MGMT_OFFLOAD_EVENTID } WMI_VDEV_MGMT_OFFLOAD_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_mgmt_hdr *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; A_UINT8 *bufp; A_UINT32 num_bufp; A_UINT32 is_allocated_bufp; } WMI_VDEV_MGMT_OFFLOAD_EVENTID_param_tlvs;;




typedef enum { WMI_GET_ELNA_BYPASS_EVENTID_tlv_order_fixed_param, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_ELNA_BYPASS_EVENTID } WMI_GET_ELNA_BYPASS_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_get_elna_bypass_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; } WMI_GET_ELNA_BYPASS_EVENTID_param_tlvs;;





typedef enum { WMI_GET_CHANNEL_ANI_EVENTID_tlv_order_fixed_param, WMI_GET_CHANNEL_ANI_EVENTID_tlv_order_ani_info, WMI_TLV_HLPR_NUM_TLVS_FOR_WMI_GET_CHANNEL_ANI_EVENTID } WMI_GET_CHANNEL_ANI_EVENTID_TAG_ORDER_enum_type; typedef struct { wmi_get_channel_ani_event_fixed_param *fixed_param; A_UINT32 num_fixed_param; A_UINT32 is_allocated_fixed_param; wmi_channel_ani_info_tlv_param *ani_info; A_UINT32 num_ani_info; A_UINT32 is_allocated_ani_info; } WMI_GET_CHANNEL_ANI_EVENTID_param_tlvs;;
