/*
 * Copyright (c) 2015 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 * $ATH_LICENSE_TARGET_C$
 */

/*
 * rtt_tlv_defs_expanded.h file can be generated using macro preprocessor output of gcc for ease of debugging and reference by developers
 * Following command has to be run from "fw_common\fw_interface\include" directory to create rtt_tlv_defs_expanded.h file
 * gcc -E rtt_tlv_defs.h > rtt_tlv_defs_expanded.h on Linux
 * and
 * gcc -E rtt_tlv_defs.h -o rtt_tlv_defs_expanded.h on Linux/Windows
 *
 */

#ifndef _RTT_TLV_DEFS_H_
#define _RTT_TLV_DEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define RTTTLV_ALL_REQ_LIST(OP) \
    OP(RTT_MSG_SUBTYPE_CAPABILITY_REQ)  \
    OP(RTT_MSG_SUBTYPE_MEASUREMENT_REQ) \
    OP(RTT_MSG_SUBTYPE_GET_CHANNEL_INFO_REQ)  \
    OP(RTT_MSG_SUBTYPE_CFG_RESPONDER_MODE_REQ)  \
    OP(RTT_MSG_SUBTYPE_CONFIGURE_LCR) \
    OP(RTT_MSG_SUBTYPE_CONFIGURE_LCI) \
    OP(RTT_MSG_SUBTYPE_CANCEL_MEASUREMENT_REQ) \
    OP(RTT_MSG_SUBTYPE_CFG_RESPONDER_MEASUREMENT_REQ) \
//    OP(RTT_MSG_SUBTYPE_CLEANUP_REQ)

#define NO_DYNAMIC_MEMORY_ALLOCATION

#ifndef NO_DYNAMIC_MEMORY_ALLOCATION
#define RTTTLV_FIELD_BUF_IS_ALLOCATED(elem_name) \
       is_allocated_##elem_name
#endif

#define RTTTLV_FIELD_NUM_OF(elem_name) \
       num_##elem_name

/* Define the structure typedef for the TLV parameters of each cmd/event */
#define RTTTLV_TYPEDEF_STRUCT_PARAMS_TLVS(rtt_cmd_event_id) \
       rtt_cmd_event_id##_param_tlvs

/*
 * The following macro RTTTLV_OP_* are created by the macro RTTTLV_ELEM().
 */
#define RTTTLV_LOOP_START_OP_TAG_ORDER_macro        RTTTLV_OP_TAG_ORDER_macro
#define RTTTLV_LOOP_START_OP_TAG_ID_macro           RTTTLV_OP_TAG_ID_macro
#define RTTTLV_LOOP_START_OP_TAG_SIZEOF_macro       RTTTLV_OP_TAG_SIZEOF_macro
#define RTTTLV_LOOP_START_OP_TAG_VAR_SIZED_macro    RTTTLV_OP_TAG_VAR_SIZED_macro
#define RTTTLV_LOOP_START_OP_TAG_ARR_SIZE_macro     RTTTLV_OP_TAG_ARR_SIZE_macro

#define RTTTLV_LOOP_END_OP_TAG_ORDER_macro        RTTTLV_OP_TAG_ORDER_macro
#define RTTTLV_LOOP_END_OP_TAG_ID_macro           RTTTLV_OP_TAG_ID_macro
#define RTTTLV_LOOP_END_OP_TAG_SIZEOF_macro       RTTTLV_OP_TAG_SIZEOF_macro
#define RTTTLV_LOOP_END_OP_TAG_VAR_SIZED_macro    RTTTLV_OP_TAG_VAR_SIZED_macro
#define RTTTLV_LOOP_END_OP_TAG_ARR_SIZE_macro     RTTTLV_OP_TAG_ARR_SIZE_macro

/* macro to define the TLV name in the correct order. When (op==TAG_ORDER) */
#define RTTTLV_OP_TAG_ORDER_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      rtt_cmd_event_id##_tlv_order_##elem_name,

/* macro to define the TLV name with the TLV Tag value. When (op==TAG_ID) */
#define RTTTLV_OP_TAG_ID_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      rtt_cmd_event_id##_tlv_tag_##elem_name = elem_tlv_tag,

/* macro to define the TLV name with the TLV structure size. May not be accurate when variable length. When (op==TAG_SIZEOF) */
#define RTTTLV_OP_TAG_SIZEOF_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      rtt_cmd_event_id##_sizeof_##elem_name = sizeof(elem_struc_type),

/* macro to define the TLV name with value indicating whether the TLV is variable length. When (op==TAG_VAR_SIZED) */
#define RTTTLV_OP_TAG_VAR_SIZED_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      rtt_cmd_event_id##_var_sized_##elem_name = var_len,

/* macro to define the TLV name with value indicating the fixed array size. When (op==TAG_ARR_SIZE) */
#define RTTTLV_OP_TAG_ARR_SIZE_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      rtt_cmd_event_id##_arr_size_##elem_name = arr_size,


/*
 * macro to define a few fields associated to a TLV. For example, a structure pointer with the TLV name.
 * This macro is expand from RTTTLV_ELEM(op) when (op==STRUCT_FIELD).
 * NOTE: If this macro is changed, then "mirror" structure rtttlv_cmd_param_info
 * should be updated too.
 */
#ifndef NO_DYNAMIC_MEMORY_ALLOCATION
#define RTTTLV_OP_STRUCT_FIELD_macro(param_ptr, param_len, wmi_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      elem_struc_type *elem_name; \
      A_UINT32 RTTTLV_FIELD_NUM_OF(elem_name); \
      A_UINT32 RTTTLV_FIELD_BUF_IS_ALLOCATED(elem_name);

/*
 * A "mirror" structure that contains the fields that is created by the
 * macro WMITLV_OP_STRUCT_FIELD_macro.
 * NOTE: you should modify this structure and WMITLV_OP_STRUCT_FIELD_macro
 * so that they both has the same kind of fields.
 */
typedef struct {
    void *tlv_ptr;            /* Pointer to the TLV Buffer. But the "real" one will have the right type instead of void. */
    A_UINT32 num_elements;    /* Number of elements. For non-array, this is one. For array, this is the number of elements. */
    A_UINT32 buf_is_allocated;/* Boolean flag to indicate that a new buffer is allocated for this TLV. */
} rtttlv_cmd_param_info;

#else
#define RTTTLV_OP_STRUCT_FIELD_macro(param_ptr, param_len, wmi_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)  \
      elem_struc_type *elem_name; \
      A_UINT32 RTTTLV_FIELD_NUM_OF(elem_name);

/*
 * A "mirror" structure that contains the fields that is created by the
 * macro WMITLV_OP_STRUCT_FIELD_macro.
 * NOTE: you should modify this structure and WMITLV_OP_STRUCT_FIELD_macro
 * so that they both has the same kind of fields.
 */
typedef struct {
    void *tlv_ptr;            /* Pointer to the TLV Buffer. But the "real" one will have the right type instead of void. */
    A_UINT32 num_elements;    /* Number of elements. For non-array, this is one. For array, this is the number of elements. */
} rtttlv_cmd_param_info;

#endif

#define RTTTLV_STRUCT_FIELD_name(rtt_cmd_event_id) \
      rtt_cmd_event_id##_ptr

#define RTTTLV_STRUCT_FIELD_macro(rtt_cmd_event_id)  \
      RTTTLV_TYPEDEF_STRUCT_PARAMS_TLVS(rtt_cmd_event_id) RTTTLV_STRUCT_FIELD_name(rtt_cmd_event_id);

#define RTTTLV_LOOP_START_OP_STRUCT_FIELD_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size) \
              struct elem_struc_type{

#define RTTTLV_LOOP_END_OP_STRUCT_FIELD_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size) \
              }elem_name[arr_size];
/*
 * NOTE TRICKY MACRO:
 *  RTTTLV_ELEM is re-defined to a "op" specific macro.
 *  Eg. RTTTLV_OP_TAG_ORDER_macro is created for the op_type=TAG_ORDER.
 */
#define RTTTLV_ELEM(rtt_cmd_event_id, op_type, param_ptr, param_len, elem_tlv_tag, elem_struc_type, elem_name, var_len) \
    RTTTLV_OP_##op_type##_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, RTTTLV_ARR_SIZE_INVALID)

#define RTTTLV_LOOP_START(rtt_cmd_event_id, op_type, param_ptr, param_len, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size) \
    RTTTLV_LOOP_START_OP_##op_type##_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)

#define RTTTLV_LOOP_END(rtt_cmd_event_id, op_type, param_ptr, param_len, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size) \
    RTTTLV_LOOP_END_OP_##op_type##_macro(param_ptr, param_len, rtt_cmd_event_id, elem_tlv_tag, elem_struc_type, elem_name, var_len, arr_size)

#define RTTTLV_TABLE(id,op,buf,len) RTTTLV_TABLE_##id(id,op,buf,len)

#define RTTTLV_CREATE_PARAM_STRUC(rtt_cmd_event_id)            \
    typedef enum {                                             \
        RTTTLV_TABLE(rtt_cmd_event_id, TAG_ORDER, NULL, 0)     \
        RTT_TLV_HLPR_NUM_TLVS_FOR_##rtt_cmd_event_id           \
    } rtt_cmd_event_id##_TAG_ORDER_enum_type;                  \
                                                               \
    typedef struct {                                           \
        RTTTLV_TABLE(rtt_cmd_event_id, STRUCT_FIELD, NULL, 0)  \
    } RTTTLV_TYPEDEF_STRUCT_PARAMS_TLVS(rtt_cmd_event_id)

#define RTTTLV_CREATE_REQ_UNION(name)                        \
    typedef union {                                                    \
        RTTTLV_ALL_REQ_LIST(RTTTLV_STRUCT_FIELD_macro) \
    } name;

/*
 * Used to fill in the "arr_size" parameter when it is not specified and hence, invalid. Can be used to
 * indicate if the original TLV definition specify this fixed array size.
 */
#define RTTTLV_ARR_SIZE_INVALID  0x1FE

#define RTTTLV_GET_TAG_NUM_TLV_ATTRIB(wmi_cmd_event_id)      \
       RTT_TLV_HLPR_NUM_TLVS_FOR_##wmi_cmd_event_id

/* Indicates whether the TLV is fixed size or variable length */
#define RTTTLV_SIZE_FIX     0
#define RTTTLV_SIZE_VAR     1

typedef struct {
	A_UINT32 tag_order;
	A_UINT32 tag_id;
	A_UINT32 tag_struct_size;
	A_UINT32 tag_varied_size;
	A_UINT32 tag_array_size;
    A_UINT32 cmd_num_tlv;
} rtttlv_attributes_struc;

/* RTT capability request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CAPABILITY_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_cap_req_head, wmi_rtt_oem_cap_req_head, cap_req, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CAPABILITY_REQ);

/* RTT measurement request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_MEASUREMENT_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_measreq_head, wmi_rtt_oem_measreq_head, measreq_head, RTTTLV_SIZE_FIX) \
    RTTTLV_LOOP_START(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_loop_start, wmi_rtt_oem_measreq_body, measreq_body_start, RTTTLV_SIZE_VAR, MAX_CHANNEL_REQ) \
        RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_channel_info, wmi_rtt_oem_channel_info, channel, RTTTLV_SIZE_FIX) \
        RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_measreq_per_channel_info, wmi_rtt_oem_measreq_per_channel_info, measreq_per_channel_info, RTTTLV_SIZE_FIX) \
        RTTTLV_LOOP_START(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_loop_start, wmi_rtt_oem_measreq_peerinfo, peer_info_start, RTTTLV_SIZE_VAR, INITIATOR_REQ_MAX) \
            RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_measreq_peer_info, wmi_rtt_oem_measreq_peer_info, measreq_peer_info, RTTTLV_SIZE_FIX) \
        RTTTLV_LOOP_END(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_loop_end, wmi_rtt_oem_measreq_peerinfo, peer_info, RTTTLV_SIZE_VAR, INITIATOR_REQ_MAX) \
    RTTTLV_LOOP_END(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_loop_end, wmi_rtt_oem_measreq_body, measreq_body, RTTTLV_SIZE_VAR, MAX_CHANNEL_REQ)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_MEASUREMENT_REQ);


/* RTT get_channel_info request*/
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_GET_CHANNEL_INFO_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_get_channel_info_req_head, wmi_rtt_oem_get_channel_info_req_head, get_channel_info_head, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_GET_CHANNEL_INFO_REQ);

/* RTT set responder mode request*/
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CFG_RESPONDER_MODE_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_set_responder_mode_req_head, wmi_rtt_oem_set_responder_mode_req_head, set_responder_mode_head, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CFG_RESPONDER_MODE_REQ);

/* RTT LCR configuration request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CONFIGURE_LCR(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_lcr_cfg_head, wmi_rtt_oem_lcr_cfg_head, lcr_cfg_head, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CONFIGURE_LCR);

/* RTT LCI configuration request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CONFIGURE_LCI(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_lci_cfg_head, wmi_rtt_oem_lci_cfg_head, lci_cfg_head, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CONFIGURE_LCI);

/* RTT configure responder measurement request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CFG_RESPONDER_MEASUREMENT_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_cfg_resp_meas_req_head, wmi_rtt_oem_cfg_resp_meas_req_head, cfg_resp_meas_req_head, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CFG_RESPONDER_MEASUREMENT_REQ);

/* RTT Cleanup request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CLEANUP_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_lci_cfg_head, wmi_rtt_oem_lci_cfg_head, lci_cfg_head, RTTTLV_SIZE_FIX)

//RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CLEANUP_REQ);

/* RTT Measurement cancel request */
#define RTTTLV_TABLE_RTT_MSG_SUBTYPE_CANCEL_MEASUREMENT_REQ(id,op,buf,len) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_req_head, wmi_rtt_oem_req_head, oem_req, RTTTLV_SIZE_FIX) \
    RTTTLV_ELEM(id,op,buf,len, WMIRTT_TLV_TAG_STRUC_wmi_rtt_oem_cancel_measurement_req_info, wmi_rtt_oem_cancel_measurement_req_info, measurement_req_info, RTTTLV_SIZE_FIX)

RTTTLV_CREATE_PARAM_STRUC(RTT_MSG_SUBTYPE_CANCEL_MEASUREMENT_REQ);

RTTTLV_CREATE_REQ_UNION(wlan_rtt_req_param_tlv_union);

#ifdef __cplusplus
}
#endif

#endif /*_RTT_TLV_DEFS_H_*/
