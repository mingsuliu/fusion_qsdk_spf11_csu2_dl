/*
 * Copyright (c) 2013 Atheros Communications Inc.
 *
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */


/* 
 * Every Product Line or chipset or team can have its own Whitelist table. 
 * The following is a list of versions that the present software can support
 * even though its versions are incompatible. Any entry here means that the 
 * indicated version does not break WMI compatibility even though it has 
 * a minor version change. 
 */
wmi_whitelist_version_info version_whitelist[] =
{
    {0, 0, 0x5F414351, 0x00004C4D, 0, 0},    //Placeholder: Major=0, Minor=0, Namespace="QCA_ML" (Dummy entry)
};


