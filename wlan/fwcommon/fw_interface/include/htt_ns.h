/*
 * Copyright (c) 2013 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 */
//------------------------------------------------------------------------------
// Copyright (c) 2013 Atheros Corporation.  All rights reserved.
// $ATH_LICENSE_HOSTSDK0_C$
//------------------------------------------------------------------------------
//==============================================================================
// Author(s): ="Atheros"
//==============================================================================

/**
 * @file htt_ns.h
 *
 * @details
 *  This file defines the HTT_NS message library used by the Northstar
 *  aCPU SW and cCPU FW to exchange data-related messages.
 */

#ifndef _HTT_NS_H_
#define _HTT_NS_H_

#include <a_types.h>    /* A_UINT32 */
#include <a_osapi.h>    /* PREPACK, POSTPACK */

#include <htt_common.h>

/*===========================================================================*/
/* general definitions */


#define HTT_NS_CURRENT_VERSION_MAJOR 1
#define HTT_NS_CURRENT_VERSION_MINOR 0


#ifndef INLINE
#ifdef QCA_SUPPORT_INTEGRATED_SOC
/* host SW */
#define INLINE inline
#else
/* target FW */
#define INLINE __inline
#endif
#define HTT_NS_INLINE_DEF
#endif /* INLINE */

static INLINE void
htt_ns_field_set(
    A_UINT32 *msg_addr32,
    unsigned offset32,
    unsigned mask,
    unsigned shift,
    unsigned value)
{
    /* sanity check: make sure the value fits within the field */
    //adf_os_assert(value << shift == (value << shift) | mask);

    msg_addr32 += offset32;
    /* clear the field */
    *msg_addr32 &= ~mask;
    /* write the new value */
    *msg_addr32 |= (value << shift);
}

#ifdef HTT_NS_INLINE_DEF
#undef HTT_NS_INLINE_DEF
#undef INLINE
#endif


#define HTT_NS_FIELD_GET(msg_addr32, offset32, mask, shift) \
    (((*(msg_addr32 + offset32)) & mask) >> shift)


/*===========================================================================*/
/* host --> target definitions */

enum htt_ns_h2t_msg_type {
    HTT_NS_H2T_MSG_TYPE_VERSION_REQ = 0x0,
    HTT_NS_H2T_MSG_TYPE_TX_FRM      = 0x1,
    HTT_NS_H2T_MSG_TYPE_RX_RING_CFG = 0x2,
    /* keep this last */
    HTT_NS_H2T_NUM_MSGS
};

/*
 * HTT NS host to target message type -
 * stored in bits 7:0 of the first word of the message
 */
#define HTT_NS_H2T_MSG_TYPE_SET(msg_addr, msg_type) \
    (*((A_UINT8 *) msg_addr) = (msg_type))
#define HTT_NS_H2T_MSG_TYPE_GET(msg_addr) \
    (*((A_UINT8 *) msg_addr))

/*=== VERSION_REQ message ===*/

/**
 * @brief host -> target version number request message definition
 *
 *     |31            24|23            16|15             8|7              0|
 *     |----------------+----------------+----------------+----------------|
 *     |                     reserved                     |    msg type    |
 *     |-------------------------------------------------------------------|
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as a version number request message
 *     Value: 0x0 (HTT_NS_H2T_MSG_TYPE_VERSION_REQ)
 */
struct htt_ns_h2t_version_req_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_VERSION_REQ */
        reserved0:    24;
};

#define HTT_NS_VER_REQ_BYTES sizeof(struct htt_ns_h2t_version_req_t)

/*=== TX_FRM message ===*/

/**
 * @brief HTT NS tx MSDU descriptor / TX_FRM message
 *
 * @details
 *  The HTT tx MSDU descriptor is created by the host HTT SW for each
 *  tx MSDU.  The HTT tx MSDU descriptor contains the information that
 *  the target firmware needs for the FW's tx processing, particularly
 *  for pulling the MSDU down to the target through the DXE.
 *
 * |31  29|28 27|26     22|21     16|15  13|12|11        8|7               0|
 * |------------------------------------------------------------------------|
 * | rsvd |cksum| ext TID | vdev ID | type |  pkt subtype |     msg type    |
 * |------------------------------------------------------------------------|
 * |             pkt ID             |num frags|            pkt len          |
 * |------------------------------------------------------------------------|
 * |                            Tx BD bus address                           |
 * |------------------------------------------------------------------------|
 * |           reserved             |              Tx BD length             |
 * |------------------------------------------------------------------------|
 * |                        L2 hdr frag bus address                         |
 * |------------------------------------------------------------------------|
 * |           reserved             |          L2 hdr frag length           |
 * |------------------------------------------------------------------------|
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as a tx completion indication message
 *     Value: 0x1 (HTT_NS_H2T_MSG_TYPE_TX_FRM)
 *   - SUBTYPE
 *     Bits 12:8
 *     Purpose: specify actions required for specific packet types,
 *         particularly "raw" type packets.
 *         Note that this is unrelated to the 802.11 frame subtype.
 *     Value: 0x0 (currently unused)
 *   - TYPE
 *     Bits 15:13
 *     Purpose: identify L2 header type (802.3 vs. 802.11) and 802.11 frame
 *         types (mgmt vs. data)
 *         Note that this is unrelated to the 802.11 frame type.
 *     Value: htt_pkt_type enum
 *   - VDEV_ID
 *     Bits 21:16
 *     Purpose: specify which vdev the frames are sent from (mainly for mcast)
 *     Value: ID of sending vdev
 *   - EXT_TID
 *     Bits 26:22
 *     Purpose: specify which traffic category the tx frame is, if known
 *     Value: 0-15 for QoS TIDs, extension values for mgmt (ucast vs. mcast)
 *         and data (non-QoS and mcast data), or 0x1f if TID is unknown.
 *   - CKSUM
 *     Bits 28:27
 *     Purpose: specify whether the target should add the L3/L4 (IPv4 header,
 *         UDP, TCP) checksums to the MSDU
 *     Value: mask of HTT_TX_L3_CKSUM_OFFLOAD and HTT_TX_L4_CKSUM_OFFLOAD
 *   - MSDU_LEN
 *     Bits 11:0
 *     Purpose: specify the length of the tx frame
 *     Value: 0 to MTU (about 1524 bytes)
 *   - NUM_FRAGS
 *     Bits 15:12
 *     Purpose: specify how many fragments the tx frame is comprised of
 *     Value: 2 (Tx BD + frame) to 12
 *   - MSDU_ID
 *     Bits 31:16
 *     Purpose: temporally unique ID for the tx frame, used for tx completion
 *     Value: 0 to tx desc pool size - 1
 */
PREPACK struct htt_ns_fw_tx_msdu_desc_t
{
    /* DWORD 0: flags and meta-data */
    A_UINT32
        msg_type: 8, /* HTT_NS_H2T_MSG_TYPE_TX_FRM */

        subtype: 5, /* not same as 802.11 frame subtype */

        type: 3, /* not same as 802.11 frame type */

        /* vdev_id -
         * ID for the vdev that is sending this tx frame.
         * For certain non-standard packet types, e.g. pkt_type == raw
         * and (pkt_subtype >> 3) == 1, this field is not relevant/valid.
         * This field is used primarily for determining where to queue
         * broadcast and multicast frames.
         */
        vdev_id: 6,

        /* ext_tid -
         * The extended traffic ID.
         * If the TID is unknown, the extended TID is set to
         * HTT_TX_EXT_TID_INVALID.
         * If the tx frame is QoS data, then the extended TID has the 0-15
         * value of the QoS TID.
         * If the tx frame is non-QoS data, then the extended TID is set to
         * HTT_TX_EXT_TID_NON_QOS.
         * If the tx frame is multicast or broadcast, then the extended TID
         * is set to HTT_TX_EXT_TID_MCAST_BCAST.
         */
        ext_tid: 5,

        /* cksum_offload -
         * This flag indicates whether checksum offload is enabled or not 
         * for this frame. Target FW use this flag to turn on HW checksumming
         *  0x0 - No checksum offload
         *  0x1 - L3 header checksum only
         *  0x2 - L4 checksum only
         *  0x3 - L3 header checksum + L4 checksum
         */
        cksum_offload: 2,

        reserved_dword0_bits28_31: 3; /* unused */

    /* DWORD 1: MSDU length and ID */
    A_UINT32
        len:       12, /* MSDU length, in bytes */
        num_frags:  4, /* number of fragments, including Tx BD */
        msdu_id:   16; /* MSDU ID */

} POSTPACK;

struct htt_ns_tx_frag_desc_t {
    A_UINT32 paddr;
    A_UINT32 len;
};

/* word 0 */
#define HTT_NS_H2T_TX_FRM_SUBTYPE_OFFSET32        0
#define HTT_NS_H2T_TX_FRM_SUBTYPE_M               0x00001f00
#define HTT_NS_H2T_TX_FRM_SUBTYPE_S               8

#define HTT_NS_H2T_TX_FRM_TYPE_OFFSET32           0
#define HTT_NS_H2T_TX_FRM_TYPE_M                  0x0000e000
#define HTT_NS_H2T_TX_FRM_TYPE_S                  13

#define HTT_NS_H2T_TX_FRM_VDEV_ID_OFFSET32        0
#define HTT_NS_H2T_TX_FRM_VDEV_ID_M               0x003f0000
#define HTT_NS_H2T_TX_FRM_VDEV_ID_S               16

#define HTT_NS_H2T_TX_FRM_EXT_TID_OFFSET32        0
#define HTT_NS_H2T_TX_FRM_EXT_TID_M               0x07c00000
#define HTT_NS_H2T_TX_FRM_EXT_TID_S               22

#define HTT_NS_H2T_TX_FRM_CKSUM_OFFLOAD_OFFSET32  0
#define HTT_NS_H2T_TX_FRM_CKSUM_OFFLOAD_M         0x18000000
#define HTT_NS_H2T_TX_FRM_CKSUM_OFFLOAD_S         27

/* word 1 */
#define HTT_NS_H2T_TX_FRM_LEN_OFFSET32            1
#define HTT_NS_H2T_TX_FRM_LEN_M                   0x00000fff
#define HTT_NS_H2T_TX_FRM_LEN_S                   0

#define HTT_NS_H2T_TX_FRM_NUM_FRAGS_OFFSET32      1
#define HTT_NS_H2T_TX_FRM_NUM_FRAGS_M             0x0000f000
#define HTT_NS_H2T_TX_FRM_NUM_FRAGS_S             12

#define HTT_NS_H2T_TX_FRM_MSDU_ID_OFFSET32        1
#define HTT_NS_H2T_TX_FRM_MSDU_ID_M               0xffff0000
#define HTT_NS_H2T_TX_FRM_MSDU_ID_S               16

/* general field access macros */

#define HTT_NS_H2T_TX_FRM_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                            \
        ((A_UINT32 *) msg_addr),                                 \
        HTT_NS_H2T_TX_FRM_ ## field ## _OFFSET32,           \
        HTT_NS_H2T_TX_FRM_ ## field ## _M,                  \
        HTT_NS_H2T_TX_FRM_ ## field ## _S,                  \
        value)

#define HTT_NS_H2T_TX_FRM_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                     \
        ((A_UINT32 *) msg_addr),                          \
        HTT_NS_H2T_TX_FRM_ ## field ## _OFFSET32,    \
        HTT_NS_H2T_TX_FRM_ ## field ## _M,           \
        HTT_NS_H2T_TX_FRM_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_H2T_TX_FRM_SUBTYPE_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(SUBTYPE, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_SUBTYPE_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(SUBTYPE, msg_addr)

#define HTT_NS_H2T_TX_FRM_TYPE_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(TYPE, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_TYPE_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(TYPE, msg_addr)

#define HTT_NS_H2T_TX_FRM_VDEV_ID_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(VDEV_ID, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_VDEV_ID_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(VDEV_ID, msg_addr)

#define HTT_NS_H2T_TX_FRM_EXT_TID_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(EXT_TID, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_EXT_TID_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(EXT_TID, msg_addr)

#define HTT_NS_H2T_TX_FRM_CKSUM_OFFLOAD_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(CKSUM_OFFLOAD, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_CKSUM_OFFLOAD_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(CKSUM_OFFLOAD, msg_addr)

#define HTT_NS_H2T_TX_FRM_LEN_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(LEN, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_LEN_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(LEN, msg_addr)

#define HTT_NS_H2T_TX_FRM_NUM_FRAGS_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(NUM_FRAGS, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_NUM_FRAGS_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(NUM_FRAGS, msg_addr)

#define HTT_NS_H2T_TX_FRM_MSDU_ID_SET(msg_addr, value) \
    HTT_NS_H2T_TX_FRM_FIELD_SET(MSDU_ID, msg_addr, value)
#define HTT_NS_H2T_TX_FRM_MSDU_ID_GET(msg_addr) \
    HTT_NS_H2T_TX_FRM_FIELD_GET(MSDU_ID, msg_addr)


/*=== RX_RING_CFG message ===*/

/**
 * @brief host -> target rx ring configuration message definition
 *
 *     |31                           16|15            8|7             0|
 *     |---------------------------------------------------------------|
 *     |                     reserved                  |    msg type   |
 *     |---------------------------------------------------------------|
 *     |          input index shadow register physical address         |
 *     |---------------------------------------------------------------|
 *     |         output index shadow register physical address         |
 *     |---------------------------------------------------------------|
 *     |                   rx ring base physical address               |
 *     |---------------------------------------------------------------|
 *     |      rx ring buffer size      |        rx ring length         |
 *     |---------------------------------------------------------------|
 *     |      FW_IDX initial value     |         reserved              |
 *     |---------------------------------------------------------------|
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as a rx ring configuration message
 *     Value: 0x2 (HTT_NS_H2T_MSG_TYPE_RX_RING_CFG)
 *   - IN_IDX_SHADOW_REG_PADDR
 *     Bits 31:0
 *     Value: physical address of the index the host updates to show how
 *         many rx buffers have been added to the rx ring
 *   - OUT_IDX_SHADOW_REG_PADDR
 *     Bits 31:0
 *     Value: physical address of the index the target updates to show how
 *         many rx buffers it has removed from the rx ring
 *   - RING_BASE_PADDR
 *     Bits 31:0
 *     Value: physical address of the host's rx ring
 *   - RING_LEN
 *     Bits 15:0
 *     Value: number of elements in the rx ring
 *   - RING_BUF_SZ
 *     Bits 31:16
 *     Value: size of the buffers reference by the rx ring, in byte units
 *   - IDX_INIT_VAL
 *     Bits 31:16
 *     Purpose: Specify the initial value for the FW_IDX.
 *     Value: the number of buffers initially present in the host's rx ring
 */
struct htt_ns_h2t_rx_ring_cfg_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_RX_RING_CFG */
        reserved0:    24;

    /* word 1 */
    A_UINT32 in_idx_shadow_reg_paddr;

    /* word 2 */
    A_UINT32 out_idx_shadow_reg_paddr;

    /* word 3 */
    A_UINT32 base_paddr;

    /* word 4 */
    A_UINT32
        len:          16,
        buf_sz:       16;

    /* word 5 */
    A_UINT32
        reserved4:    16,
        idx_init_val: 16;
};
#define HTT_NS_RX_RING_CFG_BYTES sizeof(struct htt_ns_h2t_rx_ring_cfg_t)

/* word 0 */
#define HTT_NS_H2T_RX_RING_CFG_IN_IDX_SHADOW_REG_PADDR_OFFSET32  1
#define HTT_NS_H2T_RX_RING_CFG_IN_IDX_SHADOW_REG_PADDR_M         0xffffffff
#define HTT_NS_H2T_RX_RING_CFG_IN_IDX_SHADOW_REG_PADDR_S         0

#define HTT_NS_H2T_RX_RING_CFG_OUT_IDX_SHADOW_REG_PADDR_OFFSET32 2
#define HTT_NS_H2T_RX_RING_CFG_OUT_IDX_SHADOW_REG_PADDR_M        0xffffffff
#define HTT_NS_H2T_RX_RING_CFG_OUT_IDX_SHADOW_REG_PADDR_S        0

#define HTT_NS_H2T_RX_RING_CFG_BASE_PADDR_OFFSET32               3
#define HTT_NS_H2T_RX_RING_CFG_BASE_PADDR_M                      0xffffffff
#define HTT_NS_H2T_RX_RING_CFG_BASE_PADDR_S                      0

#define HTT_NS_H2T_RX_RING_CFG_LEN_OFFSET32                      4
#define HTT_NS_H2T_RX_RING_CFG_LEN_M                             0x0000ffff
#define HTT_NS_H2T_RX_RING_CFG_LEN_S                             0

#define HTT_NS_H2T_RX_RING_CFG_BUF_SZ_OFFSET32                   4
#define HTT_NS_H2T_RX_RING_CFG_BUF_SZ_M                          0xffff0000
#define HTT_NS_H2T_RX_RING_CFG_BUF_SZ_S                          16

#define HTT_NS_H2T_RX_RING_CFG_IDX_INIT_VAL_OFFSET32             5
#define HTT_NS_H2T_RX_RING_CFG_IDX_INIT_VAL_M                    0xffff0000
#define HTT_NS_H2T_RX_RING_CFG_IDX_INIT_VAL_S                    16


/* general field access macros */

#define HTT_NS_H2T_RX_RING_CFG_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                            \
        ((A_UINT32 *) msg_addr),                                 \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _OFFSET32,           \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _M,                  \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _S,                  \
        value)

#define HTT_NS_H2T_RX_RING_CFG_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                     \
        ((A_UINT32 *) msg_addr),                          \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _OFFSET32,    \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _M,           \
        HTT_NS_H2T_RX_RING_CFG_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_H2T_RX_RING_CFG_IN_IDX_SHADOW_REG_PADDR_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(IN_IDX_SHADOW_REG_PADDR, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_IN_IDX_SHADOW_REG_PADDR_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(IN_IDX_SHADOW_REG_PADDR, msg_addr)

#define HTT_NS_H2T_RX_RING_CFG_OUT_IDX_SHADOW_REG_PADDR_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(OUT_IDX_SHADOW_REG_PADDR, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_OUT_IDX_SHADOW_REG_PADDR_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(OUT_IDX_SHADOW_REG_PADDR, msg_addr)

#define HTT_NS_H2T_RX_RING_CFG_BASE_PADDR_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(BASE_PADDR, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_BASE_PADDR_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(BASE_PADDR, msg_addr)

#define HTT_NS_H2T_RX_RING_CFG_LEN_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(LEN, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_LEN_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(LEN, msg_addr)

#define HTT_NS_H2T_RX_RING_CFG_BUF_SZ_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(BUF_SZ, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_BUF_SZ_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(BUF_SZ, msg_addr)

#define HTT_NS_H2T_RX_RING_CFG_IDX_INIT_VAL_SET(msg_addr, value) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_SET(IDX_INIT_VAL, msg_addr, value)
#define HTT_NS_H2T_RX_RING_CFG_IDX_INIT_VAL_GET(msg_addr) \
    HTT_NS_H2T_RX_RING_CFG_FIELD_GET(IDX_INIT_VAL, msg_addr)


/*===========================================================================*/
/* target --> host definitions */

enum htt_ns_t2h_msg_type {
    /* setup messages */
    HTT_NS_T2H_MSG_TYPE_VERSION_CONF      = 0x0,
    HTT_NS_T2H_MSG_TYPE_PEER_INFO         = 0x1,
    HTT_NS_T2H_MSG_TYPE_PEER_UNMAP        = 0x2,

    /* tx completion messages */
    HTT_NS_T2H_MSG_TYPE_TX_COMPL_IND      = 0x3,
    HTT_NS_T2H_MSG_TYPE_MGMT_TX_COMPL_IND = 0x4,

    /* rx partion offload messages */
    HTT_NS_T2H_MSG_TYPE_RX_IND            = 0x5,
    HTT_NS_T2H_MSG_TYPE_RX_FLUSH          = 0x6,
    HTT_NS_T2H_MSG_TYPE_RX_ADDBA          = 0x7,
    HTT_NS_T2H_MSG_TYPE_RX_DELBA          = 0x8,

    /* rx full offload messages */
    HTT_NS_T2H_MSG_TYPE_RX_IN_ORDER_IND   = 0x9,
    HTT_NS_T2H_MSG_TYPE_RX_FIFO_LOW_IND   = 0xa,

    /* keep this last */
    HTT_NS_T2H_NUM_MSGS
};

/*
 * HTT NS target to host message type -
 * stored in bits 7:0 of the first word of the message
 */
#define HTT_NS_T2H_MSG_TYPE_SET(msg_addr, msg_type) \
    (*((A_UINT8 *) msg_addr) = (msg_type))
#define HTT_NS_T2H_MSG_TYPE_GET(msg_addr) \
    (*((A_UINT8 *) msg_addr))

/*=== VERSION_CONF message ===*/

/**
 * @brief target -> host version number confirmation message definition
 *
 *     |31            24|23            16|15             8|7              0|
 *     |----------------+----------------+----------------+----------------|
 *     |    reserved    |  major number  |  minor number  |    msg type    |
 *     |-------------------------------------------------------------------|
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as a version number confirmation message
 *     Value: 0x0 (HTT_NS_T2H_MSG_TYPE_VERSION_CONF)
 *   - VER_MINOR
 *     Bits 15:8
 *     Purpose: Specify the minor number of the HTT message library version
 *         in use by the target firmware.
 *         The minor number specifies the specific revision within a range
 *         of fundamentally compatible HTT message definition revisions.
 *         Compatible revisions involve adding new messages or perhaps
 *         adding new fields to existing messages, in a backwards-compatible
 *         manner.
 *         Incompatible revisions involve changing the message type values,
 *         or redefining existing messages.
 *     Value: minor number
 *   - VER_MAJOR
 *     Bits 15:8
 *     Purpose: Specify the major number of the HTT message library version
 *         in use by the target firmware.
 *         The major number specifies the family of minor revisions that are
 *         fundamentally compatible with each other, but not with prior or
 *         later families.
 *     Value: major number
 */
struct htt_ns_t2h_version_conf_t {
    /* word 0 */
    A_UINT32
        msg_type:   8, /* HTT_NS_T2H_MSG_TYPE_VERSION_CONF */
        minor:      8,
        major:      8,
        reserved:   8;
};

/* word 0 */
#define HTT_NS_T2H_VERSION_CONF_MINOR_OFFSET32  0
#define HTT_NS_T2H_VERSION_CONF_MINOR_M         0x0000ff00
#define HTT_NS_T2H_VERSION_CONF_MINOR_S         8

#define HTT_NS_T2H_VERSION_CONF_MAJOR_OFFSET32  0
#define HTT_NS_T2H_VERSION_CONF_MAJOR_M         0x00ff0000
#define HTT_NS_T2H_VERSION_CONF_MAJOR_S         16

/* general field access macros */

#define HTT_NS_T2H_VERSION_CONF_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                         \
        ((A_UINT32 *) msg_addr),                                  \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _M,                  \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_VERSION_CONF_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                               \
        ((A_UINT32 *) msg_addr),                          \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _M,           \
        HTT_NS_T2H_VERSION_CONF_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_VERSION_CONF_MINOR_SET(msg_addr, value) \
    HTT_NS_T2H_VERSION_CONF_FIELD_SET(MINOR, msg_addr, value)
#define HTT_NS_T2H_VERSION_CONF_MINOR_GET(msg_addr) \
    HTT_NS_T2H_VERSION_CONF_FIELD_GET(MINOR, msg_addr)

#define HTT_NS_T2H_VERSION_CONF_MAJOR_SET(msg_addr, value) \
    HTT_NS_T2H_VERSION_CONF_FIELD_SET(MAJOR, msg_addr, value)
#define HTT_NS_T2H_VERSION_CONF_MAJOR_GET(msg_addr) \
    HTT_NS_T2H_VERSION_CONF_FIELD_GET(MAJOR, msg_addr)


/*=== PEER_INFO message ===*/

typedef enum {
    /* ASSOC - "real" peer from STA-AP association */
    HTT_NS_T2H_PEER_TYPE_ASSOC = 0x0,

    /* SELF - self-peer for unicast tx to unassociated peer */
    HTT_NS_T2H_PEER_TYPE_SELF  = 0x1,

    /* BSSID - reserved for FW use for BT-AMP+IBSS */
    HTT_NS_T2H_PEER_TYPE_BSSID = 0x2,

    /* BCAST - self-peer for multicast / broadcast tx */
    HTT_NS_T2H_PEER_TYPE_BCAST = 0x3
} HTT_NS_T2H_PEER_TYPE_ENUM;

enum {
    HTT_NS_RMF_DISABLED = 0,
    HTT_NS_RMF_ENABLED  = 1
};

/**
 * @brief target -> host peer info message definition
 *
 * @details
 * The following diagram shows the format of the peer info message sent
 * from the target to the host.  This layout assumes the target operates
 * as little-endian.
 *
 * |31          25|24|23       18|17|16|15      11|10|9|8|7|6|            0|
 * |-----------------------------------------------------------------------|
 * |   mgmt DPU idx  |  bcast DPU idx  |     DPU idx     |     msg type    |
 * |-----------------------------------------------------------------------|
 * | mgmt DPU sig |bcast DPU sig |     DPU sig    |       peer ID          |
 * |-----------------------------------------------------------------------|
 * |    MAC addr 1   |    MAC addr 0   |     vdev ID     | |R|  peer type  |
 * |-----------------------------------------------------------------------|
 * |    MAC addr 5   |    MAC addr 4   |    MAC addr 3   |    MAC addr 2   |
 * |-----------------------------------------------------------------------|
 *
 *
 * The following field definitions describe the format of the peer info
 * message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as peer info message
 *     Value: 0x1 (HTT_NS_T2H_MSG_TYPE_PEER_INFO)
 *   - DPU_IDX
 *     Bits 15:8
 *     Purpose: specify the DPU index (a.k.a. security key ID) to use for
 *         unicast data frames sent to this peer
 *     Value: key ID
 *   - BCAST_DPU_IDX
 *     Bits 23:16
 *     Purpose: specify the DPU index (a.k.a. security key ID) to use for
 *         broadcast data frames sent by this (self) peer
 *     Value: key ID
 *   - MGMT_DPU_IDX
 *     Bits 31:24
 *     Purpose: specify the DPU index (a.k.a. security key ID) to use for
 *         unicast management frames sent by this (self) peer
 *     Value: key ID
 * WORD 1:
 *   - PEER_ID
 *     Bits 10:0
 *     Purpose: The ID that the target has allocated to refer to the peer
 *   - DPU_SIG
 *     Bits 17:11
 *     Purpose: specify the DPU signature (a.k.a. security key validity
 *         magic number) to specify for unicast data frames sent to this peer
 *   - BCAST_DPU_SIG
 *     Bits 24:18
 *     Purpose: specify the DPU signature (a.k.a. security key validity
 *         magic number) to specify for broadcast data frames sent by this
 *         (self) peer
 *   - MGMT_DPU_SIG
 *     Bits 31:25
 *     Purpose: specify the DPU signature (a.k.a. security key validity
 *         magic number) to specify for unicast management frames sent by this
 *         (self) peer
 * WORD 2:
 *   - PEER_TYPE
 *     Bits 5:0
 *     Purpose: specify whether the peer in question is a real peer or
 *         one of the types of "self-peer" created for the vdev
 *     Value: HTT_NS_T2H_PEER_TYPE enum
 *   - RMF_ENABLED (R)
 *     Bit 6
 *     Purpose: specify whether the peer in question has enable robust
 *         management frames, to encrypt certain managment frames
 *     Value: HTT_NS_RMF enum
 *   - VDEV_ID
 *     Bits 15:8
 *     Purpose: For a real peer, the vdev ID indicates which virtual device
 *         the peer is associated with.  For a self-peer, the vdev ID shows
 *         which virtual device the self-peer represents.
 *   - MAC_ADDR_L16
 *     Bits 31:16
 *     Purpose: Identifies which peer the peer ID is for.
 *     Value: lower 2 bytes of the peer's MAC address
 *         For a self-peer, the peer's MAC address is the MAC address of the
 *         vdev the self-peer represents.
 * WORD 3:
 *   - MAC_ADDR_U32
 *     Bits 31:0
 *     Purpose: Identifies which peer the peer ID is for.
 *     Value: upper 4 bytes of the peer's MAC address
 *         For a self-peer, the peer's MAC address is the MAC address of the
 *         vdev the self-peer represents.
 */
struct htt_ns_t2h_peer_info_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_PEER_INFO */
        dpu_idx:       8,
        bcast_dpu_idx: 8,
        mgmt_dpu_idx:  8;
    /* word 1 */
    A_UINT32
        peer_id:      11,
        dpu_sig:       7,
        bcast_dpu_sig: 7,
        mgmt_dpu_sig:  7;
    /* word 2 */
    A_UINT32
        peer_type:     6,
        rmf_enabled:   1,
        reserved0:     1,
        vdev_id:       8,
        mac_addr_l16: 16;
    /* word 3 */
    A_UINT32 mac_addr_u32;
};

/* word 0 */
#define HTT_NS_T2H_PEER_INFO_DPU_IDX_OFFSET32        0
#define HTT_NS_T2H_PEER_INFO_DPU_IDX_M               0x0000ff00
#define HTT_NS_T2H_PEER_INFO_DPU_IDX_S               8

#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_IDX_OFFSET32  0
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_IDX_M         0x00ff0000
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_IDX_S         16

#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_IDX_OFFSET32   0
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_IDX_M          0xff000000
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_IDX_S          24

/* word 1 */
#define HTT_NS_T2H_PEER_INFO_PEER_ID_OFFSET32        1
#define HTT_NS_T2H_PEER_INFO_PEER_ID_M               0x000007ff
#define HTT_NS_T2H_PEER_INFO_PEER_ID_S               0

#define HTT_NS_T2H_PEER_INFO_DPU_SIG_OFFSET32        1
#define HTT_NS_T2H_PEER_INFO_DPU_SIG_M               0x0003f800
#define HTT_NS_T2H_PEER_INFO_DPU_SIG_S               11

#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_SIG_OFFSET32  1
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_SIG_M         0x01fc0000
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_SIG_S         18

#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_SIG_OFFSET32   1
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_SIG_M          0xfe000000
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_SIG_S          25

/* word 2 */
#define HTT_NS_T2H_PEER_INFO_PEER_TYPE_OFFSET32      2
#define HTT_NS_T2H_PEER_INFO_PEER_TYPE_M             0x0000003f
#define HTT_NS_T2H_PEER_INFO_PEER_TYPE_S             0

#define HTT_NS_T2H_PEER_INFO_RMF_ENABLED_OFFSET32    2
#define HTT_NS_T2H_PEER_INFO_RMF_ENABLED_M           0x00000040
#define HTT_NS_T2H_PEER_INFO_RMF_ENABLED_S           6

#define HTT_NS_T2H_PEER_INFO_VDEV_ID_OFFSET32        2
#define HTT_NS_T2H_PEER_INFO_VDEV_ID_M               0x0000ff00
#define HTT_NS_T2H_PEER_INFO_VDEV_ID_S               8

#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_L16_OFFSET32   2
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_L16_M          0xffff0000
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_L16_S          16

/* word 3 */
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_U32_OFFSET32   3
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_U32_M          0xffffffff
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_U32_S          0

/* general field access macros */

#define HTT_NS_T2H_PEER_INFO_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                          \
        ((A_UINT32 *) msg_addr),                               \
        HTT_NS_T2H_PEER_INFO_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_PEER_INFO_ ## field ## _M,                  \
        HTT_NS_T2H_PEER_INFO_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_PEER_INFO_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                   \
        ((A_UINT32 *) msg_addr),                        \
        HTT_NS_T2H_PEER_INFO_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_PEER_INFO_ ## field ## _M,           \
        HTT_NS_T2H_PEER_INFO_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_PEER_INFO_DPU_IDX_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(DPU_IDX, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_DPU_IDX_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(DPU_IDX, msg_addr)

#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_IDX_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(BCAST_DPU_IDX, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_IDX_GET(msg_addr) \
    (A_UINT8)(HTT_NS_T2H_PEER_INFO_FIELD_GET(BCAST_DPU_IDX, msg_addr))

#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_IDX_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(MGMT_DPU_IDX, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_IDX_GET(msg_addr) \
    (A_UINT8)(HTT_NS_T2H_PEER_INFO_FIELD_GET(MGMT_DPU_IDX, msg_addr))

#define HTT_NS_T2H_PEER_INFO_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(PEER_ID, msg_addr)

#define HTT_NS_T2H_PEER_INFO_DPU_SIG_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(DPU_SIG, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_DPU_SIG_GET(msg_addr) \
    (A_UINT8)(HTT_NS_T2H_PEER_INFO_FIELD_GET(DPU_SIG, msg_addr))

#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_SIG_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(BCAST_DPU_SIG, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_BCAST_DPU_SIG_GET(msg_addr) \
    (A_UINT8)(HTT_NS_T2H_PEER_INFO_FIELD_GET(BCAST_DPU_SIG, msg_addr))

#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_SIG_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(MGMT_DPU_SIG, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_MGMT_DPU_SIG_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(MGMT_DPU_SIG, msg_addr)

#define HTT_NS_T2H_PEER_INFO_PEER_TYPE_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(PEER_TYPE, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_PEER_TYPE_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(PEER_TYPE, msg_addr)

#define HTT_NS_T2H_PEER_INFO_QOS_CAPABLE_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(QOS_CAPABLE, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_QOS_CAPABLE_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(QOS_CAPABLE, msg_addr)

#define HTT_NS_T2H_PEER_INFO_RMF_ENABLED_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(RMF_ENABLED, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_RMF_ENABLED_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(RMF_ENABLED, msg_addr)

#define HTT_NS_T2H_PEER_INFO_VDEV_ID_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(VDEV_ID, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_VDEV_ID_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(VDEV_ID, msg_addr)

#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_L16_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(MAC_ADDR_L16, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_L16_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(MAC_ADDR_L16, msg_addr)

#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_U32_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_INFO_FIELD_SET(MAC_ADDR_U32, msg_addr, value)
#define HTT_NS_T2H_PEER_INFO_MAC_ADDR_U32_GET(msg_addr) \
    HTT_NS_T2H_PEER_INFO_FIELD_GET(MAC_ADDR_U32, msg_addr)


/*=== PEER_UNMAP message ===*/

/**
 * @brief target -> host peer unmap message definition
 *
 * @details
 * The following diagram shows the format of the peer unmap message sent
 * from the target to the host.  This layout assumes the target operates
 * as little-endian.
 *
 * |31                      19|18                       8|7               0|
 * |-----------------------------------------------------------------------|
 * |         reserved         |          peer ID         |     msg type    |
 * |-----------------------------------------------------------------------|
 *
 *
 * The following field definitions describe the format of the peer info
 * message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as peer unmap message
 *     Value: 0x2 (HTT_NS_T2H_MSG_TYPE_PEER_UNMAP)
 *   - PEER_ID
 *     Bits 18:8
 *     Purpose: The ID that the target has allocated to refer to the peer
 */
struct htt_ns_t2h_peer_unmap_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_PEER_UNMAP */
        peer_id:      11,
        reserved0:    13;
};

/* word 0 */
#define HTT_NS_T2H_PEER_UNMAP_PEER_ID_OFFSET32        0
#define HTT_NS_T2H_PEER_UNMAP_PEER_ID_M               0x0007ff00
#define HTT_NS_T2H_PEER_UNMAP_PEER_ID_S               8

/* general field access macros */

#define HTT_NS_T2H_PEER_UNMAP_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                           \
        ((A_UINT32 *) msg_addr),                                \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _M,                  \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_PEER_UNMAP_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                    \
        ((A_UINT32 *) msg_addr),                         \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _M,           \
        HTT_NS_T2H_PEER_UNMAP_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_PEER_UNMAP_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_PEER_UNMAP_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_PEER_UNMAP_PEER_ID_GET(msg_addr) \
    (A_UINT16)(HTT_NS_T2H_PEER_UNMAP_FIELD_GET(PEER_ID, msg_addr))


/*=== TX_COMPL_IND message ===*/

#define HTT_NS_TX_COMPL_IND_STAT_OK          0
#define HTT_NS_TX_COMPL_IND_STAT_DISCARD     1
#define HTT_NS_TX_COMPL_IND_STAT_NO_ACK      2
#define HTT_NS_TX_COMPL_IND_STAT_POSTPONE    3

/**
 * @brief target -> tx complete indicate message
 *
 * @details
 * The following diagram shows the format of the tx complete indication message
 * sent from the target to the host.  This layout assumes the target operates
 * as little-endian.
 *
 * |31                              16|15       11|10   8|7               0|
 * |-----------------------------------------------------------------------|
 * |           num msdus              |  reserved |status|     msg type    |
 * |-----------------------------------------------------------------------|
 *
 *
 * The following field definitions describe the format of the tx completion
 * message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as tx complete indication message
 *     Value: 0x3 (HTT_NS_T2H_MSG_TYPE_TX_COMPL_IND)
 *   - STATUS
 *     Bits 10:8
 *     Purpose: show whether the frame was successfully transmitted
 *     Value: HTT_TX_COMPL_IND_STAT (OK, DISCARD, NO_ACK, POSTPONE)
 *   - NUM_MSDUS
 *     Bits 31:16
 *     Purpose: specify how many MSDUs are referenced in this message
 *     Value: arbitrary (usually 0-63)
 */
struct htt_ns_t2h_tx_compl_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_TX_COMPL_IND */
        status:        3,
        reserved:      5,
        num_msdus:    16;
};

/* word 0 */
#define HTT_NS_T2H_TX_COMPL_IND_STATUS_OFFSET32        0
#define HTT_NS_T2H_TX_COMPL_IND_STATUS_M               0x00000700
#define HTT_NS_T2H_TX_COMPL_IND_STATUS_S               8

#define HTT_NS_T2H_TX_COMPL_IND_NUM_MSDUS_OFFSET32     0
#define HTT_NS_T2H_TX_COMPL_IND_NUM_MSDUS_M            0xffff0000
#define HTT_NS_T2H_TX_COMPL_IND_NUM_MSDUS_S            16

/* general field access macros */

#define HTT_NS_T2H_TX_COMPL_IND_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                         \
        ((A_UINT32 *) msg_addr),                                  \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _M,                  \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_TX_COMPL_IND_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                               \
        ((A_UINT32 *) msg_addr),                          \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _M,           \
        HTT_NS_T2H_TX_COMPL_IND_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_TX_COMPL_IND_STATUS_SET(msg_addr, value) \
    HTT_NS_T2H_TX_COMPL_IND_FIELD_SET(STATUS, msg_addr, value)
#define HTT_NS_T2H_TX_COMPL_IND_STATUS_GET(msg_addr) \
    HTT_NS_T2H_TX_COMPL_IND_FIELD_GET(STATUS, msg_addr)

#define HTT_NS_T2H_TX_COMPL_IND_NUM_MSDUS_SET(msg_addr, value) \
    HTT_NS_T2H_TX_COMPL_IND_FIELD_SET(NUM_MSDUS, msg_addr, value)
#define HTT_NS_T2H_TX_COMPL_IND_NUM_MSDUS_GET(msg_addr) \
    HTT_NS_T2H_TX_COMPL_IND_FIELD_GET(NUM_MSDUS, msg_addr)


/*=== MGMT_TX_COMPL_IND message ===*/

/**
 * @brief target -> management tx complete indicate message
 *
 * @details
 * The following diagram shows the format of the management tx complete
 * indication message sent from the target to the host.
 * This layout assumes the target operates as little-endian.
 *
 * |31                              16|15       11|10   8|7               0|
 * |-----------------------------------------------------------------------|
 * |              MSDU ID             |  reserved |status|     msg type    |
 * |-----------------------------------------------------------------------|
 *
 * The following field definitions describe the format of the mgmt tx
 * completion message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as mgmt tx complete indication message
 *     Value: 0x4 (HTT_NS_T2H_MSG_TYPE_MGMT_TX_COMPL_IND)
 *   - STATUS
 *     Bits 10:8
 *     Purpose: show whether the frame was successfully transmitted
 *     Value: HTT_TX_COMPL_IND_STAT (OK, DISCARD, NO_ACK, POSTPONE)
 *   - MSDU_ID
 *     Bits 31:16
 *     Purpose: specify which frame was transmitted
 *     Value: ID provided in this frame's TX_FRM message
 */
struct htt_ns_t2h_mgmt_tx_compl_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_MGMT_TX_COMPL_IND */
        status:        3,
        reserved:      5,
        msdu_id:      16;
};

/* word 0 */
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_STATUS_OFFSET32   0
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_STATUS_M          0x00000700
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_STATUS_S          8

#define HTT_NS_T2H_MGMT_TX_COMPL_IND_MSDU_ID_OFFSET32  0
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_MSDU_ID_M         0xffff0000
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_MSDU_ID_S         16

/* general field access macros */

#define HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                                  \
        ((A_UINT32 *) msg_addr),                                       \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _M,                  \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                           \
        ((A_UINT32 *) msg_addr),                                \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _M,           \
        HTT_NS_T2H_MGMT_TX_COMPL_IND_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_MGMT_TX_COMPL_IND_STATUS_SET(msg_addr, value) \
    HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_SET(STATUS, msg_addr, value)
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_STATUS_GET(msg_addr) \
    HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_GET(STATUS, msg_addr)

#define HTT_NS_T2H_MGMT_TX_COMPL_IND_MSDU_ID_SET(msg_addr, value) \
    HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_SET(MSDU_ID, msg_addr, value)
#define HTT_NS_T2H_MGMT_TX_COMPL_IND_MSDU_ID_GET(msg_addr) \
    HTT_NS_T2H_MGMT_TX_COMPL_IND_FIELD_GET(MSDU_ID, msg_addr)


/*=== RX_IND message ===*/

/**
 * @brief target -> host rx indication message definition
 *
 * @details
 * The following field definitions describe the format of the rx indication
 * message sent from the target to the host for frames delivered in
 * over-the-air order rather than sequence-number order
 * The message consists of three major sections:
 * 1.  a fixed-length header
 * 2.  a variable-length list of firmware rx MSDU descriptors
 * 3.  one or more 4-octet MPDU range information elements
 *
 * The format of the message is depicted below.
 * in this depiction, the following abbreviations are used for information
 * elements within the message:
 *
 * |31            24|23         18|17|16|15|14|13|12|11|10|9|8|7|6|5|4       0|
 * |----------------+-------------------+---------------------+---------------|
 * |                  peer ID           |  |RV|FV| ext TID    |   msg type    |
 * |--------------------------------------------------------------------------|
 * |      num       |   release   |     release     |    flush    |   flush   |
 * |      MPDU      |     end     |      start      |     end     |   start   |
 * |     ranges     |   seq num   |     seq num     |   seq num   |  seq num  |
 * |==========================================================================|
 * |             reserved               |          FW rx desc bytes           |
 * |--------------------------------------------------------------------------|
 * |     MSDU Rx    |      MSDU Rx      |        MSDU Rx      |    MSDU Rx    |
 * |     desc B3    |      desc B2      |        desc B1      |    desc B0    |
 * |--------------------------------------------------------------------------|
 * :                                    :                                     :
 * |--------------------------------------------------------------------------|
 * |                          alignment                       |    MSDU Rx    |
 * |                           padding                        |    desc Bn    |
 * |--------------------------------------------------------------------------|
 * |              reserved              |  MPDU range status  |   MPDU count  |
 * |--------------------------------------------------------------------------|
 * :              reserved              :  MPDU range status  :   MPDU count  :
 * :- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - :
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as an rx indication message
 *     Value: 0x5 (HTT_NS_T2H_MSG_TYPE_RX_IND)
 *   - EXT_TID
 *     Bits 12:8
 *     Purpose: identify the traffic ID of the rx data, including
 *         special "extended" TID values for multicast, broadcast, and
 *         non-QoS data frames
 *     Value: 0-15 for regular TIDs, or >= 16 for bcast/mcast/non-QoS
 *   - FLUSH_VALID (FV)
 *     Bit 13
 *     Purpose: indicate whether the flush IE (start/end sequence numbers)
 *         is valid
 *     Value:
 *         1 -> flush IE is valid and needs to be processed
 *         0 -> flush IE is not valid and should be ignored
 *   - REL_VALID (RV)
 *     Bit 13
 *     Purpose: indicate whether the release IE (start/end sequence numbers)
 *         is valid
 *     Value:
 *         1 -> release IE is valid and needs to be processed
 *         0 -> release IE is not valid and should be ignored
 *   - PEER_ID
 *     Bits 31:16
 *     Purpose: Identify, by ID, which peer sent the rx data
 *     Value: ID of the peer who sent the rx data
 *   - FLUSH_SEQ_NUM_START
 *     Bits 5:0
 *     Purpose: Indicate the start of a series of MPDUs to flush
 *         Not all MPDUs within this series are necessarily valid - the host
 *         must check each sequence number within this range to see if the
 *         corresponding MPDU is actually present.
 *         This field is only valid if the FV bit is set.
 *     Value:
 *         The sequence number for the first MPDUs to check to flush.
 *         The sequence number is masked by 0x3f.
 *   - FLUSH_SEQ_NUM_END
 *     Bits 11:6
 *     Purpose: Indicate the end of a series of MPDUs to flush
 *     Value:
 *         The sequence number one larger than the sequence number of the
 *         last MPDU to check to flush.
 *         The sequence number is masked by 0x3f.
 *         Not all MPDUs within this series are necessarily valid - the host
 *         must check each sequence number within this range to see if the
 *         corresponding MPDU is actually present.
 *         This field is only valid if the FV bit is set.
 *   - REL_SEQ_NUM_START
 *     Bits 17:12
 *     Purpose: Indicate the start of a series of MPDUs to release.
 *         All MPDUs within this series are present and valid - the host
 *         need not check each sequence number within this range to see if
 *         the corresponding MPDU is actually present.
 *         This field is only valid if the RV bit is set.
 *     Value:
 *         The sequence number for the first MPDUs to check to release.
 *         The sequence number is masked by 0x3f.
 *   - REL_SEQ_NUM_END
 *     Bits 23:18
 *     Purpose: Indicate the end of a series of MPDUs to release.
 *     Value:
 *         The sequence number one larger than the sequence number of the
 *         last MPDU to check to release.
 *         The sequence number is masked by 0x3f.
 *         All MPDUs within this series are present and valid - the host
 *         need not check each sequence number within this range to see if
 *         the corresponding MPDU is actually present.
 *         This field is only valid if the RV bit is set.
 *   - NUM_MPDU_RANGES
 *     Bits 31:24
 *     Purpose: Indicate how many ranges of MPDUs are present.
 *         Each MPDU range consists of a series of contiguous MPDUs within the
 *         rx frame sequence which all have the same MPDU status.
 *     Value: 1-63 (typically a small number, like 1-3)
 *
 * Rx MSDU descriptor fields:
 *   - FW_RX_DESC_BYTES
 *     Bits 15:0
 *     Purpose: Indicate how many bytes in the Rx indication are used for
 *         FW Rx descriptors
 *
 * Payload fields:
 *   - MPDU_COUNT
 *     Bits 7:0
 *     Purpose: Indicate how many sequential MPDUs share the same status.
 *         All MPDUs within the indicated list are from the same RA-TA-TID.
 *   - MPDU_STATUS
 *     Bits 15:8
 *     Purpose: Indicate whether the (group of sequential) MPDU(s) were
 *         received successfully.
 *     Value: HTT_RX_IND_MPDU_STATUS enum
 */
struct htt_ns_t2h_rx_ind_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_RX_IND */
        ext_tid:       5,
        flush_valid:   1,
        rel_valid:     1,
        reserved0:     1,
        peer_id:      16;

    /* word 1 */
    A_UINT32
        flush_start_seq_num: 6,
        flush_end_seq_num:   6,
        rel_start_seq_num:   6,
        rel_end_seq_num:     6,
        num_mpdu_ranges:     8;

    /* word 2 */
    A_UINT32
        fw_rx_desc_bytes: 16,
        reserved2:        16;
};

/* word 0 */
#define HTT_NS_T2H_RX_IND_EXT_TID_OFFSET32              0
#define HTT_NS_T2H_RX_IND_EXT_TID_M                     0x00001f00
#define HTT_NS_T2H_RX_IND_EXT_TID_S                     8

#define HTT_NS_T2H_RX_IND_FLUSH_VALID_OFFSET32          0
#define HTT_NS_T2H_RX_IND_FLUSH_VALID_M                 0x00002000
#define HTT_NS_T2H_RX_IND_FLUSH_VALID_S                 13

#define HTT_NS_T2H_RX_IND_REL_VALID_OFFSET32            0
#define HTT_NS_T2H_RX_IND_REL_VALID_M                   0x00004000
#define HTT_NS_T2H_RX_IND_REL_VALID_S                   14

#define HTT_NS_T2H_RX_IND_PEER_ID_OFFSET32              0
#define HTT_NS_T2H_RX_IND_PEER_ID_M                     0xffff0000
#define HTT_NS_T2H_RX_IND_PEER_ID_S                     16

/* word 1 */
#define HTT_NS_T2H_RX_IND_FLUSH_START_SEQ_NUM_OFFSET32  1
#define HTT_NS_T2H_RX_IND_FLUSH_START_SEQ_NUM_M         0x0000003f
#define HTT_NS_T2H_RX_IND_FLUSH_START_SEQ_NUM_S         0

#define HTT_NS_T2H_RX_IND_FLUSH_END_SEQ_NUM_OFFSET32    1
#define HTT_NS_T2H_RX_IND_FLUSH_END_SEQ_NUM_M           0x00000fc0
#define HTT_NS_T2H_RX_IND_FLUSH_END_SEQ_NUM_S           6

#define HTT_NS_T2H_RX_IND_REL_START_SEQ_NUM_OFFSET32    1
#define HTT_NS_T2H_RX_IND_REL_START_SEQ_NUM_M           0x0003f000
#define HTT_NS_T2H_RX_IND_REL_START_SEQ_NUM_S           12

#define HTT_NS_T2H_RX_IND_REL_END_SEQ_NUM_OFFSET32      1
#define HTT_NS_T2H_RX_IND_REL_END_SEQ_NUM_M             0x00fc0000
#define HTT_NS_T2H_RX_IND_REL_END_SEQ_NUM_S             18

#define HTT_NS_T2H_RX_IND_NUM_MPDU_RANGES_OFFSET32      1
#define HTT_NS_T2H_RX_IND_NUM_MPDU_RANGES_M             0xff000000
#define HTT_NS_T2H_RX_IND_NUM_MPDU_RANGES_S             24

/* word 2 */
#define HTT_NS_T2H_RX_IND_FW_RX_DESC_BYTES_OFFSET32     2
#define HTT_NS_T2H_RX_IND_FW_RX_DESC_BYTES_M            0x0000ffff
#define HTT_NS_T2H_RX_IND_FW_RX_DESC_BYTES_S            0


/* general field access macros */

#define HTT_NS_T2H_RX_IND_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                       \
        ((A_UINT32 *) msg_addr),                            \
        HTT_NS_T2H_RX_IND_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_RX_IND_ ## field ## _M,                  \
        HTT_NS_T2H_RX_IND_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_RX_IND_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                \
        ((A_UINT32 *) msg_addr),                     \
        HTT_NS_T2H_RX_IND_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_RX_IND_ ## field ## _M,           \
        HTT_NS_T2H_RX_IND_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_RX_IND_EXT_TID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(EXT_TID, msg_addr, value)
#define HTT_NS_T2H_RX_IND_EXT_TID_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(EXT_TID, msg_addr)

#define HTT_NS_T2H_RX_IND_FLUSH_VALID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(FLUSH_VALID, msg_addr, value)
#define HTT_NS_T2H_RX_IND_FLUSH_VALID_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(FLUSH_VALID, msg_addr)

#define HTT_NS_T2H_RX_IND_REL_VALID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(REL_VALID, msg_addr, value)
#define HTT_NS_T2H_RX_IND_REL_VALID_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(REL_VALID, msg_addr)

#define HTT_NS_T2H_RX_IND_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_RX_IND_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(PEER_ID, msg_addr)

#define HTT_NS_T2H_RX_IND_FLUSH_START_SEQ_NUM_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(FLUSH_START_SEQ_NUM, msg_addr, value)
#define HTT_NS_T2H_RX_IND_FLUSH_START_SEQ_NUM_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(FLUSH_START_SEQ_NUM, msg_addr)

#define HTT_NS_T2H_RX_IND_FLUSH_END_SEQ_NUM_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(FLUSH_END_SEQ_NUM, msg_addr, value)
#define HTT_NS_T2H_RX_IND_FLUSH_END_SEQ_NUM_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(FLUSH_END_SEQ_NUM, msg_addr)

#define HTT_NS_T2H_RX_IND_REL_START_SEQ_NUM_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(REL_START_SEQ_NUM, msg_addr, value)
#define HTT_NS_T2H_RX_IND_REL_START_SEQ_NUM_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(REL_START_SEQ_NUM, msg_addr)

#define HTT_NS_T2H_RX_IND_REL_END_SEQ_NUM_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(REL_END_SEQ_NUM, msg_addr, value)
#define HTT_NS_T2H_RX_IND_REL_END_SEQ_NUM_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(REL_END_SEQ_NUM, msg_addr)

#define HTT_NS_T2H_RX_IND_NUM_MPDU_RANGES_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(NUM_MPDU_RANGES, msg_addr, value)
#define HTT_NS_T2H_RX_IND_NUM_MPDU_RANGES_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(NUM_MPDU_RANGES, msg_addr)

#define HTT_NS_T2H_RX_IND_FW_RX_DESC_BYTES_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IND_FIELD_SET(FW_RX_DESC_BYTES, msg_addr, value)
#define HTT_NS_T2H_RX_IND_FW_RX_DESC_BYTES_GET(msg_addr) \
    HTT_NS_T2H_RX_IND_FIELD_GET(FW_RX_DESC_BYTES, msg_addr)


/*=== RX_FLUSH message ===*/

/*
 * @brief target -> host rx reorder flush message definition
 *
 * @details
 * The following field definitions describe the format of the rx flush
 * message sent from the target to the host.
 *
 *     |31           24|23           16|15            8|7            0|
 *     |--------------------------------------------------------------|
 *     |       TID     |          peer ID              |   msg type   |
 *     |--------------------------------------------------------------|
 *     |  seq num end  | seq num start |  MPDU status  |   reserved   |
 *     |--------------------------------------------------------------|
 * First DWORD:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as an rx flush message
 *     Value: 0x6 (HTT_NS_T2H_MSG_TYPE_RX_FLUSH)
 *   - PEER_ID
 *     Bits 23:8 (only bits 18:8 actually used)
 *     Purpose: identify which peer's rx data is being flushed
 *     Value: (rx) peer ID
 *   - TID
 *     Bits 31:24 (only bits 27:24 actually used)
 *     Purpose: Specifies which traffic identifier's rx data is being flushed
 *     Value: traffic identifier
 * Second DWORD:
 *   - STATUS
 *     Bits 15:8
 *     Purpose:
 *         Indicate whether the flushed MPDUs should be discarded or processed.
 *     Value:
 *         0x1:   send the MPDUs from the rx reorder buffer to subsequent
 *                stages of rx processing
 *         other: discard the MPDUs
 *         It is anticipated that flush messages will always have
 *         MPDU status == 1, but the status flag is included for
 *         flexibility.
 *   - SEQ_NUM_START
 *     Bits 23:16
 *     Purpose:
 *         Indicate the start of a series of consecutive MPDUs being flushed.
 *         Not all MPDUs within this range are necessarily valid - the host
 *         must check each sequence number within this range to see if the
 *         corresponding MPDU is actually present.
 *     Value:
 *         The sequence number for the first MPDU in the sequence.
 *         This sequence number is the 6 LSBs of the 802.11 sequence number.
 *   - SEQ_NUM_END
 *     Bits 30:24
 *     Purpose:
 *         Indicate the end of a series of consecutive MPDUs being flushed.
 *     Value:
 *         The sequence number one larger than the sequence number of the
 *         last MPDU being flushed.
 *         This sequence number is the 6 LSBs of the 802.11 sequence number.
 *         The range of MPDUs from [SEQ_NUM_START,SEQ_NUM_END-1] inclusive
 *         are to be released for further rx processing.
 *         Not all MPDUs within this range are necessarily valid - the host
 *         must check each sequence number within this range to see if the
 *         corresponding MPDU is actually present.
 */
struct htt_ns_t2h_rx_flush_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_RX_FLUSH */
        peer_id:      16,
        tid:           8;

    /* word 1 */
    A_UINT32
        reserved1:     8,
        status:        8,
        seq_num_start: 8,
        seq_num_end:   8;
};

/* word 0 */
#define HTT_NS_T2H_RX_FLUSH_PEER_ID_OFFSET32          0
#define HTT_NS_T2H_RX_FLUSH_PEER_ID_M                 0x00ffff00
#define HTT_NS_T2H_RX_FLUSH_PEER_ID_S                 8

#define HTT_NS_T2H_RX_FLUSH_TID_OFFSET32              0
#define HTT_NS_T2H_RX_FLUSH_TID_M                     0xff000000
#define HTT_NS_T2H_RX_FLUSH_TID_S                     24

/* word 1 */
#define HTT_NS_T2H_RX_FLUSH_STATUS_OFFSET32           1
#define HTT_NS_T2H_RX_FLUSH_STATUS_M                  0x0000ff00
#define HTT_NS_T2H_RX_FLUSH_STATUS_S                  8

#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_START_OFFSET32    1
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_START_M           0x00ff0000
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_START_S           16

#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_END_OFFSET32      1
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_END_M             0xff000000
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_END_S             24


/* general field access macros */

#define HTT_NS_T2H_RX_FLUSH_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                         \
        ((A_UINT32 *) msg_addr),                              \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _M,                  \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_RX_FLUSH_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                  \
        ((A_UINT32 *) msg_addr),                       \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _M,           \
        HTT_NS_T2H_RX_FLUSH_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_RX_FLUSH_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_FLUSH_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_RX_FLUSH_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_RX_FLUSH_FIELD_GET(PEER_ID, msg_addr)

#define HTT_NS_T2H_RX_FLUSH_TID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_FLUSH_FIELD_SET(TID, msg_addr, value)
#define HTT_NS_T2H_RX_FLUSH_TID_GET(msg_addr) \
    HTT_NS_T2H_RX_FLUSH_FIELD_GET(TID, msg_addr)

#define HTT_NS_T2H_RX_FLUSH_STATUS_SET(msg_addr, value) \
    HTT_NS_T2H_RX_FLUSH_FIELD_SET(STATUS, msg_addr, value)
#define HTT_NS_T2H_RX_FLUSH_STATUS_GET(msg_addr) \
    HTT_NS_T2H_RX_FLUSH_FIELD_GET(STATUS, msg_addr)

#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_START_SET(msg_addr, value) \
    HTT_NS_T2H_RX_FLUSH_FIELD_SET(SEQ_NUM_START, msg_addr, value)
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_START_GET(msg_addr) \
    HTT_NS_T2H_RX_FLUSH_FIELD_GET(SEQ_NUM_START, msg_addr)

#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_END_SET(msg_addr, value) \
    HTT_NS_T2H_RX_FLUSH_FIELD_SET(SEQ_NUM_END, msg_addr, value)
#define HTT_NS_T2H_RX_FLUSH_SEQ_NUM_END_GET(msg_addr) \
    HTT_NS_T2H_RX_FLUSH_FIELD_GET(SEQ_NUM_END, msg_addr)


/*=== ADDBA message ===*/

/**
 * @brief target -> host ADDBA message definition
 *
 * @details
 * The following diagram shows the format of the rx ADDBA message sent
 * from the target to the host:
 *
 * |31                      20|19  16|15     12|11    8|7               0|
 * |---------------------------------------------------------------------|
 * |          peer ID         |  TID |   window size   |     msg type    |
 * |---------------------------------------------------------------------|
 * |                   reserved                |      start seq num      |
 * |---------------------------------------------------------------------|
 *
 * The following field definitions describe the format of the ADDBA
 * message sent from the target to the host.
 *
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as an ADDBA message
 *     Value: 0x7 (HTT_NS_T2H_MSG_TYPE_RX_ADDBA)
 *   - WIN_SIZE
 *     Bits 15:8
 *     Purpose: Specifies the length of the block ack window (max = 64).
 *     Value:
 *         block ack window length specified by the received ADDBA
 *         management message.
 *   - TID
 *     Bits 19:16
 *     Purpose: Specifies which traffic identifier the ADDBA is for.
 *     Value:
 *         TID specified by the received ADDBA management message.
 *   - PEER_ID
 *     Bits 31:20
 *     Purpose: Identifies which peer sent the ADDBA.
 *     Value:
 *         ID (hash value) used by the host for fast, direct lookup of
 *         host SW peer info, including rx reorder states.
 *   - START_SEQ_NUM
 *     Bits 11:0
 *     Purpose: Specifies the initial location of the block ack window
 *     Value: start sequence value specified by the ADDBA-request message
 */
struct htt_ns_t2h_addba_t {
    /* word 0 */
    A_UINT32 msg_type:       8, /* HTT_NS_T2H_MSG_TYPE_RX_ADDBA */
             win_size:       8,
             tid:            4,
             peer_id:       12;
    /* word 1 */
    A_UINT32 start_seq_num: 12,
             reserved0:     20;
};

/* word 0 */
#define HTT_NS_T2H_ADDBA_WIN_SIZE_OFFSET32       0
#define HTT_NS_T2H_ADDBA_WIN_SIZE_M              0x0000ff00
#define HTT_NS_T2H_ADDBA_WIN_SIZE_S              8

#define HTT_NS_T2H_ADDBA_TID_OFFSET32            0
#define HTT_NS_T2H_ADDBA_TID_M                   0x000f0000
#define HTT_NS_T2H_ADDBA_TID_S                   16

#define HTT_NS_T2H_ADDBA_PEER_ID_OFFSET32        0
#define HTT_NS_T2H_ADDBA_PEER_ID_M               0xfff00000
#define HTT_NS_T2H_ADDBA_PEER_ID_S               20

/* word 1 */
#define HTT_NS_T2H_ADDBA_START_SEQ_NUM_OFFSET32  1
#define HTT_NS_T2H_ADDBA_START_SEQ_NUM_M         0x00000fff
#define HTT_NS_T2H_ADDBA_START_SEQ_NUM_S         0

/* general field access macros */
#define HTT_NS_T2H_ADDBA_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                      \
        ((A_UINT32 *) msg_addr),                           \
        HTT_NS_T2H_ADDBA_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_ADDBA_ ## field ## _M,                  \
        HTT_NS_T2H_ADDBA_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_ADDBA_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                               \
        ((A_UINT32 *) msg_addr),                    \
        HTT_NS_T2H_ADDBA_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_ADDBA_ ## field ## _M,           \
        HTT_NS_T2H_ADDBA_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_ADDBA_WIN_SIZE_SET(msg_addr, value) \
    HTT_NS_T2H_ADDBA_FIELD_SET(WIN_SIZE, msg_addr, value)
#define HTT_NS_T2H_ADDBA_WIN_SIZE_GET(msg_addr) \
    HTT_NS_T2H_ADDBA_FIELD_GET(WIN_SIZE, msg_addr)

#define HTT_NS_T2H_ADDBA_TID_SET(msg_addr, value) \
    HTT_NS_T2H_ADDBA_FIELD_SET(TID, msg_addr, value)
#define HTT_NS_T2H_ADDBA_TID_GET(msg_addr) \
    (A_UINT8)(HTT_NS_T2H_ADDBA_FIELD_GET(TID, msg_addr))

#define HTT_NS_T2H_ADDBA_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_ADDBA_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_ADDBA_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_ADDBA_FIELD_GET(PEER_ID, msg_addr)

#define HTT_NS_T2H_ADDBA_START_SEQ_NUM_SET(msg_addr, value) \
    HTT_NS_T2H_ADDBA_FIELD_SET(START_SEQ_NUM, msg_addr, value)
#define HTT_NS_T2H_ADDBA_START_SEQ_NUM_GET(msg_addr) \
    HTT_NS_T2H_ADDBA_FIELD_GET(START_SEQ_NUM, msg_addr)


/*=== DELBA message ===*/

/**
 * @brief target -> host DELBA message definition
 *
 * @details
 * The following diagram shows the format of the rx DELBA message sent
 * from the target to the host:
 *
 * |31                      20|19  16|15     12|11    8|7               0|
 * |---------------------------------------------------------------------|
 * |          peer ID         |  TID |    reserved     |     msg type    |
 * |---------------------------------------------------------------------|
 *
 * The following field definitions describe the format of the ADDBA
 * message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as an DELBA message
 *     Value: 0x8 (HTT_NS_T2H_MSG_TYPE_RX_DELBA)
 *   - TID
 *     Bits 19:16
 *     Purpose: Specifies which traffic identifier the DELBA is for.
 *     Value:
 *         TID specified by the received DELBA management message.
 *   - PEER_ID
 *     Bits 31:20
 *     Purpose: Identifies which peer sent the DELBA.
 *     Value:
 *         ID (hash value) used by the host for fast, direct lookup of
 *         host SW peer info, including rx reorder states.
 */
struct htt_ns_t2h_delba_t {
    /* word 0 */
    A_UINT32
        msg_type:       8, /* HTT_NS_T2H_MSG_TYPE_RX_DELBA */
        reserved0:      8,
        tid:            4,
        peer_id:       12;
};

/* word 0 */
#define HTT_NS_T2H_DELBA_TID_OFFSET32            0
#define HTT_NS_T2H_DELBA_TID_M                   0x000f0000
#define HTT_NS_T2H_DELBA_TID_S                   16

#define HTT_NS_T2H_DELBA_PEER_ID_OFFSET32        0
#define HTT_NS_T2H_DELBA_PEER_ID_M               0xfff00000
#define HTT_NS_T2H_DELBA_PEER_ID_S               20

/* general field access macros */

#define HTT_NS_T2H_DELBA_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                      \
        ((A_UINT32 *) msg_addr),                           \
        HTT_NS_T2H_DELBA_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_DELBA_ ## field ## _M,                  \
        HTT_NS_T2H_DELBA_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_DELBA_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                               \
        ((A_UINT32 *) msg_addr),                    \
        HTT_NS_T2H_DELBA_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_DELBA_ ## field ## _M,           \
        HTT_NS_T2H_DELBA_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_DELBA_TID_SET(msg_addr, value) \
    HTT_NS_T2H_DELBA_FIELD_SET(TID, msg_addr, value)
#define HTT_NS_T2H_DELBA_TID_GET(msg_addr) \
    (A_UINT8)HTT_NS_T2H_DELBA_FIELD_GET(TID, msg_addr)

#define HTT_NS_T2H_DELBA_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_DELBA_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_DELBA_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_DELBA_FIELD_GET(PEER_ID, msg_addr)


/*=== RX_IN_ORDER_IND message ===*/

/**
 * @brief target -> host in-order rx indication message definition
 *
 * @details
 * The following field definitions describe the format of the rx indication
 * message sent from the target to the host for frames delivered in
 * sequence-number order rather than over-the-air order.
 *
 * |31|30                          16|15         10|9 8|7               0|
 * |---------------------------------------------------------------------|
 * |              num MSDUs          |   vdev ID   |RSS|     msg type    |
 * |                                 |             |idx|                 |
 * |---------------------------------------------------------------------|
 * |              reserved           |               peer ID             |
 * |---------------------------------------------------------------------|
 * |X0|         MSDU 0 length        |            MSDU 0 ID              |
 * |---------------------------------------------------------------------|
 * |X1|         MSDU 1 length        |            MSDU 1 ID              |
 * |---------------------------------------------------------------------|
 * :  :                              :                                   :
 *
 * The following field definitions describe the format of the ADDBA
 * message sent from the target to the host.
 *
 * WORD 0:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: identifies this as an DELBA message
 *     Value: 0x9 (HTT_NS_T2H_MSG_TYPE_RX_IN_ORDER_IND)
 *   - RSS_IDX
 *     Bits 9:8
 *     Purpose: Show which group of independent rx flows the frames belong to.
 *         This receive-side steering index assists the host in spreading the
 *         rx processing across multiple cores.
 *     Value: 0-3
 *   - VDEV_ID
 *     Bits 15:10
 *     Purpose: specify which vdev the frames are sent to
 *     Value: ID of the vdev the rx frames are for
 *   - NUM_MSDUS
 *     Bits 31:16
 *     Purpose: Specify how many MSDUs are referenced in this message.
 *     Value: arbitrary
 *   - PEER_ID
 *     Bits 15:0
 *     Purpose: Identifies which peer sent the rx frames.
 *         This indirectly identifies which vdev received the frames.
 *     Value:
 *         ID (hash value) used by the host for fast, direct lookup of
 *         host SW peer info, including rx reorder states.
 */
struct htt_ns_t2h_rx_in_order_ind_t {
    /* word 0 */
    A_UINT32
        msg_type:     8, /* HTT_NS_T2H_MSG_TYPE_RX_IN_ORDER_IND */
        rss_idx:      2,
        vdev_id:      6,
        num_msdus:   16;

    /* word 1 */
    A_UINT32
        peer_id:     16,
        reserved1:   16;
};

struct htt_ns_rx_msdu_spec_t {
    A_UINT32
        msdu_id:     16,
        msdu_len:    15,
        msdu_except:  1; /* exception flag */
};

/* word 0 */
#define HTT_NS_T2H_RX_IN_ORDER_IND_RSS_IDX_OFFSET32    0
#define HTT_NS_T2H_RX_IN_ORDER_IND_RSS_IDX_M           0x00000300
#define HTT_NS_T2H_RX_IN_ORDER_IND_RSS_IDX_S           8

#define HTT_NS_T2H_RX_IN_ORDER_IND_VDEV_ID_OFFSET32    0
#define HTT_NS_T2H_RX_IN_ORDER_IND_VDEV_ID_M           0x0000fc00
#define HTT_NS_T2H_RX_IN_ORDER_IND_VDEV_ID_S           10

#define HTT_NS_T2H_RX_IN_ORDER_IND_NUM_MSDUS_OFFSET32  0
#define HTT_NS_T2H_RX_IN_ORDER_IND_NUM_MSDUS_M         0xffff0000
#define HTT_NS_T2H_RX_IN_ORDER_IND_NUM_MSDUS_S         16

/* word 1 */
#define HTT_NS_T2H_RX_IN_ORDER_IND_PEER_ID_OFFSET32    1
#define HTT_NS_T2H_RX_IN_ORDER_IND_PEER_ID_M           0x0000ffff
#define HTT_NS_T2H_RX_IN_ORDER_IND_PEER_ID_S           0

/* words 2 through N */
#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_ID_M           0x0000ffff
#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_ID_S           0

#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_LEN_M          0x7fff0000
#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_LEN_S          16

#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_EXCEPT_M       0x80000000
#define HTT_NS_T2H_RX_IN_ORDER_IND_MSDU_EXCEPT_S       31

/* general field access macros */

#define HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_SET(field, msg_addr, value) \
    htt_ns_field_set(                                                \
        ((A_UINT32 *) msg_addr),                                     \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _OFFSET32,           \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _M,                  \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _S,                  \
        value)

#define HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_GET(field, msg_addr) \
    HTT_NS_FIELD_GET(                                         \
        ((A_UINT32 *) msg_addr),                              \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _OFFSET32,    \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _M,           \
        HTT_NS_T2H_RX_IN_ORDER_IND_ ## field ## _S)

/* access macros for specific fields */

#define HTT_NS_T2H_RX_IN_ORDER_IND_RSS_IDX_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_SET(RSS_IDX, msg_addr, value)
#define HTT_NS_T2H_RX_IN_ORDER_IND_RSS_IDX_GET(msg_addr) \
    (A_UINT8)HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_GET(RSS_IDX, msg_addr)

#define HTT_NS_T2H_RX_IN_ORDER_IND_VDEV_ID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_SET(VDEV_ID, msg_addr, value)
#define HTT_NS_T2H_RX_IN_ORDER_IND_VDEV_ID_GET(msg_addr) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_GET(VDEV_ID, msg_addr)

#define HTT_NS_T2H_RX_IN_ORDER_IND_NUM_MSDUS_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_SET(NUM_MSDUS, msg_addr, value)
#define HTT_NS_T2H_RX_IN_ORDER_IND_NUM_MSDUS_GET(msg_addr) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_GET(NUM_MSDUS, msg_addr)

#define HTT_NS_T2H_RX_IN_ORDER_IND_PEER_ID_SET(msg_addr, value) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_SET(PEER_ID, msg_addr, value)
#define HTT_NS_T2H_RX_IN_ORDER_IND_PEER_ID_GET(msg_addr) \
    HTT_NS_T2H_RX_IN_ORDER_IND_FIELD_GET(PEER_ID, msg_addr)


/*=== RX_FIFO_LOW_IND message ===*/

/**
 * @brief target -> host input FIFO low margin message
 *
 *     |31            24|23            16|15             8|7              0|
 *     |----------------+----------------+----------------+----------------|
 *     |                     reserved                     |    msg type    |
 *     |-------------------------------------------------------------------|
 *
 * Header fields:
 *   - MSG_TYPE
 *     Bits 7:0
 *     Purpose: Identifies this as a rx FIFO low margin message.
 *         This message notifies the host that the target has few or no
 *         remaining empty rx buffers to use for incoming frames.
 *         In response the host should refill the rx buffer FIFO to the
 *         appropriate depth.
 *     Value: 0xa (HTT_NS_T2H_MSG_TYPE_RX_FIFO_LOW_IND)
 */
struct htt_ns_t2h_rx_fifo_low_ind_t {
    /* word 0 */
    A_UINT32
        msg_type:      8, /* HTT_NS_T2H_MSG_TYPE_RX_FIFO_LOW_IND */
        reserved0:    24;
};



#endif /* _HTT_NS_H_ */
