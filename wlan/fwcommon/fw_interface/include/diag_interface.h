/*
 * Copyright (c) 2014-2010, 2013 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 */

#ifndef _WLAN_DIAG_INTF_H
#define _WLAN_DIAG_INTF_H

#include <wmi_unified.h>

/*
 * FW generated DIAG events and logs are sent to host over WMI with
 * WMI_DIAG_DATA_CONTAINER_EVENTID. This event is a container that
 * includes one or more DIAG event items (see struct wlan_diag_data)
 * where each data is either an event or log.
 *
 * "struct wlan_diag_data" is defined here, rather than wmi_unified.h,
 * because it doesn't not adhere to WMI standards. Specifically, each
 * data item data item may have an optional variable siezd payload.
 *
 * The "code" and optional payload are defined by a seperate DIAG
 * specification.
 */
   
enum wlan_diag_data_type {
    WLAN_DIAG_DATA_TYPE_EVENT,
    WLAN_DIAG_DATA_TYPE_LOG,
};

#define WLAN_DIAG_0_TYPE_S          0
#define WLAN_DIAG_0_TYPE            0x000000ff
#define WLAN_DIAG_0_TYPE_GET(x)     WMI_F_MS(x, WLAN_DIAG_0_TYPE)
#define WLAN_DIAG_0_TYPE_SET(x, y)  WMI_F_RMW(x, y, WLAN_DIAG_0_TYPE)
/* bits 8-15 reserved */

/* length includes the size of wlan_diag_data */
#define WLAN_DIAG_0_LEN_S           16
#define WLAN_DIAG_0_LEN             0xffff0000
#define WLAN_DIAG_0_LEN_GET(x)      WMI_F_MS(x, WLAN_DIAG_0_LEN)
#define WLAN_DIAG_0_LEN_SET(x, y)   WMI_F_RMW(x, y, WLAN_DIAG_0_LEN)

/* log/event are always 32-bit aligned. Padding is inserted after
 * optional payload to satisify this requirement */
struct wlan_diag_data {
    A_UINT32 word0;  /* type, length */
    A_UINT32 target_time;
    A_UINT32 code;  /* DIAG log or event CODE */
    /* optional payload data follows */
};

/****************************/
/* Diagnostics definitions  */
/****************************/
#define WLAN_DIAG_EVENT_LOG_MAX_BUF_SIZE 1200

typedef enum {
    DIAG_VERSION_INFO,
    DIAG_BASE_TIMESTAMP,
} wlan_diag_config_type;

typedef enum {
    WLAN_DIAG_TYPE_CONFIG,
    WLAN_DIAG_TYPE_EVENT,
    WLAN_DIAG_TYPE_LOG,
    WLAN_DIAG_TYPE_MSG,
    WLAN_DIAG_TYPE_LEGACY_MSG,
    WLAN_DIAG_TYPE_EVENT_V2, 
    WLAN_DIAG_TYPE_LOG_V2, 
    WLAN_DIAG_TYPE_MSG_V2, 
} wlan_diag_frame_type;


/* Definitions for Header */
#define DIAG_TYPE_OFFSET               24
#define DIAG_TYPE_MASK                 0xFF000000 /* Bit 0-7 */

#define DIAG_TIMESTAMP_OFFSET          0
#define DIAG_TIMESTAMP_MASK            0x00FFFFFF /* Bit 8-31 */

/* Definitions for diag Message Header */
#define DIAG_ID_OFFSET                 16
#define DIAG_ID_MASK                   0xFFFF0000 /* Bit 0-15 */

#define DIAG_LENGTH_OFFSET             0
#define DIAG_LENGTH_MASK               0x0000FFFF /* Bit 16-31 */

#define DIAG_VDEVID_OFFSET             11
#define DIAG_VDEVID_MASK               0x0000F800 /* Bit 16-20 */
#define DIAG_VDEVID_MAX                31

#define DIAG_LEVEL_OFFSET              8
#define DIAG_LEVEL_MASK                0x00000700 /* Bit 16-20 */
#define DIAG_LEVEL_MAX                 7 

#define DIAG_MSGLENGTH_OFFSET          0
#define DIAG_MSGLENGTH_MASK            0x000000FF /* Bit 24-31 */
#define DIAG_MSGLENGTH_MAX             255

#define DIAG_MODULE_ID_OFFSET          12

#define DIAG_SET_HEADER(type, time_offset) \
    (((type << DIAG_TYPE_OFFSET) & DIAG_TYPE_MASK) | \
     ((time_offset << DIAG_TIMESTAMP_OFFSET) & DIAG_TIMESTAMP_MASK))

#define DIAG_SET_MESSAGE_HEADER(message_id, vdev_id, debug_level, length) \
    (((message_id << DIAG_ID_OFFSET) & DIAG_ID_MASK) | \
     ((vdev_id << DIAG_VDEVID_OFFSET) & DIAG_VDEVID_MASK) | \
     ((debug_level << DIAG_LEVEL_OFFSET) & DIAG_LEVEL_MASK) | \
     ((length << DIAG_MSGLENGTH_OFFSET) & DIAG_MSGLENGTH_MASK))

#define DIAG_SET_EVENT_LOG_HEADER(id,length) \
    (((id << DIAG_ID_OFFSET) & DIAG_ID_MASK) | \
     ((length << DIAG_LENGTH_OFFSET) & DIAG_LENGTH_MASK))

#define DIAG_GET_TYPE(arg) \
    ((arg & DIAG_TYPE_MASK) >> DIAG_TYPE_OFFSET)

#define DIAG_GET_TIME_STAMP(arg) \
    ((arg & DIAG_TIMESTAMP_MASK) >> DIAG_TIMESTAMP_OFFSET)

#define DIAG_GET_ID(arg) \
    ((arg & DIAG_ID_MASK) >> DIAG_ID_OFFSET)

#define DIAG_GET_LENGTH(arg) \
    ((arg & DIAG_LENGTH_MASK) >> DIAG_LENGTH_OFFSET)

#define DIAG_GET_VDEVID(arg) \
    ((arg & DIAG_VDEVID_MASK) >> DIAG_VDEVID_OFFSET)

#define DIAG_GET_LEVEL(arg) \
    ((arg & DIAG_LEVEL_MASK) >> DIAG_LEVEL_OFFSET)

#define DIAG_GET_MSGLENGTH(arg) \
    ((arg & DIAG_MSGLENGTH_MASK) >> DIAG_MSGLENGTH_OFFSET)

#endif /* !_WLAN_DIAG_INTF_H */

